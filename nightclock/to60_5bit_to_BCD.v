module to60_6bit_to_BCD(input [5:0] bcd, output reg [6:0] seven_seg0, output reg [6:0] seven_seg1);

always @(*)
if (bcd == 6'b000000) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000001) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000010) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000011) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000100) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000101) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000110) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b000111) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b001000) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b001001) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b1000000;
end else if (bcd == 6'b001010) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b001011) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b001100) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b001101) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b001110) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b001111) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b010000) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b010001) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b010010) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b010011) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b1111001;
end else if (bcd == 6'b010100) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b010101) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b010110) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b010111) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011000) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011001) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011010) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011011) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011100) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011101) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b0100100;
end else if (bcd == 6'b011110) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b011111) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100000) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100001) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100010) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100011) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100100) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100101) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100110) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b100111) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b0110000;
end else if (bcd == 6'b101000) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101001) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101010) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101011) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101100) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101101) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101110) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b101111) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b110000) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b110001) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b0011001;
end else if (bcd == 6'b110010) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b110011) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b110100) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b110101) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b110110) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b110111) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b111000) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b111001) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b111010) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b0010010;
end else if (bcd == 6'b111011) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b0010010;
end
endmodule