module to6_3bit_upcounter(input clock, input reset, output reg[2:0] number, output reg ripple);

always @(posedge(clock) or posedge(reset)) begin
	if (reset) begin
		number = 0;
		ripple = 0;
	end else if (number < 5) begin
		number = number + 1;
		ripple = 0;
	end else begin
		number = 0;
		ripple = ~ripple;
	end
end
endmodule
	