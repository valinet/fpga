﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;

namespace file_to_image
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sw = new StreamReader(args[1]);
            string[] text = Regex.Split(sw.ReadToEnd(), Environment.NewLine);
            int width = Convert.ToInt32(text[0].Split(' ')[0]);
            int height = Convert.ToInt32(text[0].Split(' ')[1]);
            Bitmap bm = new Bitmap(width, height);
            for (int i = 1; i <= bm.Height; i++)
            {
                for (int j = 0; j < bm.Width; j++)
                {
                    string[] line = text[i].Split('\t');
                    string[] colors = line[j].Split(' ');
                    int R = Convert.ToInt32(colors[0]);
                    int G = Convert.ToInt32(colors[1]);
                    int B = Convert.ToInt32(colors[2]);
                    //  0.21 R + 0.72 G + 0.07 B.
                    //R = (int)(0.21 * R);
                    //G = (int)(0.72 * G);
                    //B = (int)(0.07 * B);
                   // int S = (R + G + B) / 3;
                    bm.SetPixel(j, i - 1, Color.FromArgb(255, R, G, B));
                }
            }
            sw.Close();
            bm.Save(args[0]);
            Environment.Exit(0);

        }
    }
}
