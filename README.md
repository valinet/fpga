Contains Quartus and related projects for work with FPGA, EIE 2016, Imperial College London.

An overview of the projects available in this repository is shown below.

Directories
-----------

* 7_segment_display_verilog_generator - C++ code that generates the Verilog code which creates the component that takes 1 10-bit input and puts its contents on 4 7-bit outputs, which eventually will be mapped to the 7 segment display 

* 24hrclock - does not work and has some bugs, should have been a 24 hrs night clock for the DE0 FPGA board, but it is deprecated and soon will be deleted; check night clock instead

* blinkenlights - up counts the LEDs on the DE0 at a rate of 47 Hz

* blinkenlights - up counts the LEDs on the DE0 at a rate of 6 Hz (I think), and displays it on the 7 digit display as well

* combinationalmultiplier - a working solution for the EIE FPGA Experiment Ex 3.1, all block diagrams

* edge_processing - C++ app which performs Sober filter

* file_to_image - C# app which generates an image based on output from edge_processing

* FPGASelector - a C# app which selects the correct FPGA from the list displayed in the New Project Wizard in Quartus II, based on the arguments list. For example, to select the FPGA corresponding to the DE0 board, namely the EP3C16F484C6, the app should be opened like "FPGASelector 35"; best to be used is by pinning it to the Start menu. Please ensure you select the first item in the FPGAs list prior to launching this app; the app terminates after finishing its job, no UI shown - crashes on school computers for unknown reason tho..

* gluelogic - sticks DE0 switches to DE0 LEDs

* image_to_file - C# app which generates input for edge_processing based on image file

* nightclock - a 24 hrs night watch project, which transforms the DE0 board into a working 24 hrs night watch; pretty self-explanatory

* pipelinedmultiplier - a working solution for the EIE FPGA Experiment Ex 3.2, all block diagrams

* registeredmultiplier - a solution for EIE FPGA Experiment Ex 3.3, believed not be fully working, all block diagrams

* stopwatch - a working solution for EIE FPGA Experiment Ex 4, mixed block diagrames, and Verilog HDL code

Files
-----

* "Digital logic.pptx" - a PowerPoint containing a brief walkthrough over the FPGA Experiment - I used it for orals spring term

* README.md - this bitch

* scan_of_EIE_Project_Booklet.pdf - a scanned copy of the EIE FPGA Experiment booklet from spring term - poor, but usable quality, better than nothing until we manage to find the original PDF version which is burried somewhere in the deep territories of the cluttered Blackboard

More to come...

// Some commands:
// git add *
// git commit -m "comment"
// git push -u origin master