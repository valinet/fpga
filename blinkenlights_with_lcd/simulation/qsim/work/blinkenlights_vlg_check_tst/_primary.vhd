library verilog;
use verilog.vl_types.all;
entity blinkenlights_vlg_check_tst is
    port(
        LEDG            : in     vl_logic_vector(0 to 9);
        sampler_rx      : in     vl_logic
    );
end blinkenlights_vlg_check_tst;
