-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "03/16/2016 05:21:50"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	combinationalmultiplier IS
    PORT (
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	CLOCK : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0);
	OUTPUT : OUT std_logic_vector(9 DOWNTO 0)
	);
END combinationalmultiplier;

-- Design Ports Information
-- D0[6]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[8]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[7]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[6]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[5]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[3]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[1]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[0]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK	=>  Location: PIN_F1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_G5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_G4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_H6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_H7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_H5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_E3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_E4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF combinationalmultiplier IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_CLOCK : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_OUTPUT : std_logic_vector(9 DOWNTO 0);
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \OUTPUT[9]~output_o\ : std_logic;
SIGNAL \OUTPUT[8]~output_o\ : std_logic;
SIGNAL \OUTPUT[7]~output_o\ : std_logic;
SIGNAL \OUTPUT[6]~output_o\ : std_logic;
SIGNAL \OUTPUT[5]~output_o\ : std_logic;
SIGNAL \OUTPUT[4]~output_o\ : std_logic;
SIGNAL \OUTPUT[3]~output_o\ : std_logic;
SIGNAL \OUTPUT[2]~output_o\ : std_logic;
SIGNAL \OUTPUT[1]~output_o\ : std_logic;
SIGNAL \OUTPUT[0]~output_o\ : std_logic;
SIGNAL \CLOCK~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \inst19|inst43~q\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \inst|inst43~q\ : std_logic;
SIGNAL \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst39|inst43~q\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \inst19|inst8~q\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \inst19|inst19~q\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \inst|inst19~q\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \inst|inst8~q\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \inst19|inst41~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \inst19|inst42~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ : std_logic;
SIGNAL \inst15|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst15|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \inst|inst41~q\ : std_logic;
SIGNAL \inst15|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst17|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst15|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst17|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ : std_logic;
SIGNAL \inst15|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst17|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \inst|inst42~q\ : std_logic;
SIGNAL \inst17|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst17|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst25|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst17|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst25|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst57|inst1|inst~combout\ : std_logic;
SIGNAL \inst25|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst32|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst32|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst32|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst5|inst1|inst~combout\ : std_logic;
SIGNAL \inst32|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst41~q\ : std_logic;
SIGNAL \inst32|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst39|inst19~q\ : std_logic;
SIGNAL \inst32|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst39|inst41~q\ : std_logic;
SIGNAL \inst32|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst39|inst8~q\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~300_combout\ : std_logic;
SIGNAL \inst32|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst39|inst42~q\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~301_combout\ : std_logic;
SIGNAL \inst32|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst43~q\ : std_logic;
SIGNAL \inst15|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst25|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst34|inst1|inst~combout\ : std_logic;
SIGNAL \inst32|inst488|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst19~q\ : std_logic;
SIGNAL \inst32|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst42~q\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~66_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~71_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~68_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~294_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~273_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~274_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~275_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~80_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~93_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~94_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~95_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~81_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~82_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~99_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~100_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~89_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~88_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~90_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~91_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~92_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~96_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~276_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~101_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~85_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~84_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~86_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~102_combout\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst~combout\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst8~q\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~83_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~87_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~277_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~278_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~279_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~280_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~103_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~104_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~281_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~98_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~39_combout\ : std_logic;
SIGNAL \inst42|Equal0~1_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~106_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~107_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~108_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~282_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~105_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~109_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~110_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~114_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~115_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~116_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~112_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~111_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~113_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~117_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~118_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~119_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~120_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~121_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~122_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~126_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~125_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~127_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~136_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~139_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~138_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~284_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~285_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~140_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~123_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~124_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~128_combout\ : std_logic;
SIGNAL \inst42|Equal0~3_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~129_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~283_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~130_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~131_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~132_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~133_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~134_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~135_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~141_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~147_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~146_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~148_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~145_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~298_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~299_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~149_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~142_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~143_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~144_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~150_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~151_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~154_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~153_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~158_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~152_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~162_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~163_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~156_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~159_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~160_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~155_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~157_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~161_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~164_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~170_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~171_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~165_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~166_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~169_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~172_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~167_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~168_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~173_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~174_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~40_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~41_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~175_combout\ : std_logic;
SIGNAL \inst42|Equal0~4_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~176_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~286_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~177_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~183_combout\ : std_logic;
SIGNAL \inst42|Equal0~5_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~184_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~97_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~185_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~179_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~180_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~178_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~181_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~182_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~186_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~287_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~188_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~187_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~189_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~192_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~193_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~194_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~190_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~191_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~195_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~196_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~288_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~197_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~198_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~199_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~201_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~200_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~202_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~203_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~208_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~207_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~209_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~206_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~210_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~204_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~205_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~211_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~213_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~214_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~212_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~215_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~216_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~217_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~245_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~246_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~247_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~229_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~232_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~295_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~249_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~248_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~292_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~250_combout\ : std_logic;
SIGNAL \inst42|Equal0~7_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~251_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~252_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~256_combout\ : std_logic;
SIGNAL \inst42|Equal0~8_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~257_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~253_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~254_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~255_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~258_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~259_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~225_combout\ : std_logic;
SIGNAL \inst42|Equal0~6_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~226_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~137_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~220_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~227_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~223_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~221_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~222_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~224_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~219_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~218_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~296_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~297_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~240_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~241_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~242_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~238_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~239_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~289_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~290_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~291_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~243_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~235_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~234_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~236_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~228_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~230_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~231_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~233_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~237_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~244_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~260_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~262_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~263_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~261_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~264_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~265_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~293_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~44_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~42_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~43_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~45_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~49_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~50_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~47_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~46_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~48_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~51_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~54_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~55_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~266_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~56_combout\ : std_logic;
SIGNAL \inst42|Equal0~0_combout\ : std_logic;
SIGNAL \inst42|Equal0~9_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~52_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~53_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~57_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~58_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~71_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~61_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~62_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~63_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~64_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~65_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~66_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~68_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~59_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~250_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~60_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~107_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~108_combout\ : std_logic;
SIGNAL \inst42|Equal0~12_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~109_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~110_combout\ : std_logic;
SIGNAL \inst42|Equal0~11_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~105_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~104_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~252_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~106_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~102_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~103_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~111_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~80_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~81_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~264_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~265_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~82_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~83_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~84_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~85_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~86_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~87_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~89_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~90_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~88_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~91_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~96_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~92_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~93_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~94_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~95_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~97_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~99_combout\ : std_logic;
SIGNAL \inst42|Equal0~10_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~251_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~98_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~100_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~101_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~112_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~148_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~136_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~149_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~147_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~259_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~253_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~150_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~151_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~152_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~142_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~141_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~143_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~144_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~140_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~145_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~113_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~119_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~137_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~138_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~139_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~146_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~134_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~115_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~133_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~135_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~153_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~127_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~128_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~114_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~129_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~130_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~131_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~262_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~263_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~124_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~123_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~125_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~120_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~121_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~122_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~126_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~117_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~116_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~118_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~132_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~154_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~176_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~177_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~272_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~268_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~178_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~271_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~270_combout\ : std_logic;
SIGNAL \inst42|Equal0~2_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~267_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~179_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~180_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~255_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~181_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~157_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~182_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~158_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~183_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~166_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~165_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~167_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~171_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~254_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~184_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~27_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~261_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~170_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~172_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~173_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~163_combout\ : std_logic;
SIGNAL \inst42|Equal0~16_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~162_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~164_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~168_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~269_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~160_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~161_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~169_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~155_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~156_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~159_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~174_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~185_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~257_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~204_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~205_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~203_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~256_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~206_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~188_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~186_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~187_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~175_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~189_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~194_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~193_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~191_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~190_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~192_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~195_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~199_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~198_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~200_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~196_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~197_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~201_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~202_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~207_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~215_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~219_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~217_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~216_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~218_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~220_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~221_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~222_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~223_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~224_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~225_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~226_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~227_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~228_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~210_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~211_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~212_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~213_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~208_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~209_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~214_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~234_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~235_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~258_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~229_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~230_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~231_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~232_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~233_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~236_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~237_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~242_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~260_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~245_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~243_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~244_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~246_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~247_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~239_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~240_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~238_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~241_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~248_combout\ : std_logic;
SIGNAL \inst42|Equal0~17_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~35_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~36_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~38_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~37_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~39_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~40_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~46_combout\ : std_logic;
SIGNAL \inst42|Equal0~13_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~43_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~34_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~44_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~41_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~42_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~45_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~47_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~48_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~56_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~55_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~57_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~51_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~50_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~52_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~53_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~54_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~249_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~49_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~58_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~59_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~60_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~62_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~63_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~61_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~64_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~65_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~66_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~68_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~18_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~15_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~71_combout\ : std_logic;
SIGNAL \inst42|Equal0~14_combout\ : std_logic;
SIGNAL \inst42|Equal0~15_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg3[2]~0_combout\ : std_logic;
SIGNAL \ALT_INV_CLOCK~input_o\ : std_logic;

BEGIN

D0 <= ww_D0;
ww_CLOCK <= CLOCK;
ww_SW <= SW;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
OUTPUT <= ww_OUTPUT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_CLOCK~input_o\ <= NOT \CLOCK~input_o\;

-- Location: IOOBUF_X26_Y29_N16
\D0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[6]~110_combout\,
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\D0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[5]~151_combout\,
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\D0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[4]~176_combout\,
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\D0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[3]~288_combout\,
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\D0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[2]~217_combout\,
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\D0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[1]~260_combout\,
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\D0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[0]~293_combout\,
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\D1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[6]~74_combout\,
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\D1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[5]~112_combout\,
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\D1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[4]~154_combout\,
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\D1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[3]~185_combout\,
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\D1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[2]~207_combout\,
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\D1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[1]~237_combout\,
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\D1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[0]~248_combout\,
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\D2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[6]~40_combout\,
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\D2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[5]~48_combout\,
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\D2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[4]~58_combout\,
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\D2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[3]~64_combout\,
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\D2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[2]~69_combout\,
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\D2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[1]~72_combout\,
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\D2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[0]~74_combout\,
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\D3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\D3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\D3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\D3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\D3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\D3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\D3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOOBUF_X0_Y27_N16
\OUTPUT[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst8~q\,
	devoe => ww_devoe,
	o => \OUTPUT[9]~output_o\);

-- Location: IOOBUF_X0_Y27_N9
\OUTPUT[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst19~q\,
	devoe => ww_devoe,
	o => \OUTPUT[8]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\OUTPUT[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst41~q\,
	devoe => ww_devoe,
	o => \OUTPUT[7]~output_o\);

-- Location: IOOBUF_X0_Y26_N23
\OUTPUT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst42~q\,
	devoe => ww_devoe,
	o => \OUTPUT[6]~output_o\);

-- Location: IOOBUF_X0_Y24_N16
\OUTPUT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst43~q\,
	devoe => ww_devoe,
	o => \OUTPUT[5]~output_o\);

-- Location: IOOBUF_X0_Y24_N23
\OUTPUT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst8~q\,
	devoe => ww_devoe,
	o => \OUTPUT[4]~output_o\);

-- Location: IOOBUF_X0_Y21_N16
\OUTPUT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst19~q\,
	devoe => ww_devoe,
	o => \OUTPUT[3]~output_o\);

-- Location: IOOBUF_X0_Y21_N23
\OUTPUT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst41~q\,
	devoe => ww_devoe,
	o => \OUTPUT[2]~output_o\);

-- Location: IOOBUF_X0_Y20_N2
\OUTPUT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst42~q\,
	devoe => ww_devoe,
	o => \OUTPUT[1]~output_o\);

-- Location: IOOBUF_X0_Y20_N9
\OUTPUT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst43~q\,
	devoe => ww_devoe,
	o => \OUTPUT[0]~output_o\);

-- Location: IOIBUF_X0_Y23_N1
\CLOCK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK,
	o => \CLOCK~input_o\);

-- Location: IOIBUF_X0_Y24_N1
\SW[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: FF_X1_Y22_N25
\inst19|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[0]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst43~q\);

-- Location: IOIBUF_X0_Y22_N15
\SW[5]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: FF_X1_Y22_N17
\inst|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[5]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst43~q\);

-- Location: LCCOMB_X1_Y23_N24
\inst33|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\inst19|inst43~q\ & \inst|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst19|inst43~q\,
	datad => \inst|inst43~q\,
	combout => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: FF_X1_Y23_N25
\inst39|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst43~q\);

-- Location: IOIBUF_X0_Y27_N22
\SW[4]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: FF_X2_Y22_N7
\inst19|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[4]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst8~q\);

-- Location: IOIBUF_X0_Y23_N8
\SW[3]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: FF_X2_Y22_N11
\inst19|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[3]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst19~q\);

-- Location: IOIBUF_X0_Y26_N1
\SW[8]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: FF_X1_Y22_N3
\inst|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[8]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst19~q\);

-- Location: IOIBUF_X0_Y25_N1
\SW[9]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: FF_X1_Y22_N23
\inst|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[9]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8~q\);

-- Location: IOIBUF_X0_Y25_N22
\SW[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: FF_X2_Y22_N29
\inst19|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[2]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst41~q\);

-- Location: LCCOMB_X2_Y22_N30
\inst9|LPM_MUX_component|auto_generated|result_node[2]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ = (\inst|inst8~q\ & \inst19|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst8~q\,
	datad => \inst19|inst41~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\);

-- Location: IOIBUF_X0_Y27_N1
\SW[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: FF_X1_Y22_N11
\inst19|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst42~q\);

-- Location: LCCOMB_X1_Y22_N12
\inst9|LPM_MUX_component|auto_generated|result_node[1]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ = (\inst19|inst42~q\ & \inst|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst19|inst42~q\,
	datad => \inst|inst8~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\);

-- Location: LCCOMB_X1_Y22_N22
\inst15|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst~combout\ = (\inst19|inst43~q\ & (\inst19|inst42~q\ & (\inst|inst8~q\ & \inst|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datab => \inst19|inst42~q\,
	datac => \inst|inst8~q\,
	datad => \inst|inst19~q\,
	combout => \inst15|inst1|inst|inst~combout\);

-- Location: LCCOMB_X1_Y22_N28
\inst15|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst2~0_combout\ = (\inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ & ((\inst15|inst1|inst|inst~combout\) # ((\inst|inst19~q\ & \inst19|inst41~q\)))) # (!\inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ & 
-- (\inst|inst19~q\ & (\inst15|inst1|inst|inst~combout\ & \inst19|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\,
	datab => \inst|inst19~q\,
	datac => \inst15|inst1|inst|inst~combout\,
	datad => \inst19|inst41~q\,
	combout => \inst15|inst2|inst2~0_combout\);

-- Location: LCCOMB_X2_Y22_N0
\inst15|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst1|inst5~combout\ = \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ $ (\inst15|inst2|inst2~0_combout\ $ (((\inst19|inst19~q\ & \inst|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst19~q\,
	datac => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst1|inst5~combout\);

-- Location: IOIBUF_X0_Y26_N8
\SW[7]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: FF_X1_Y22_N9
\inst|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[7]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst41~q\);

-- Location: LCCOMB_X1_Y22_N18
\inst15|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst1|inst5~combout\ = \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ $ (\inst15|inst1|inst|inst~combout\ $ (((\inst|inst19~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\,
	datab => \inst|inst19~q\,
	datac => \inst15|inst1|inst|inst~combout\,
	datad => \inst19|inst41~q\,
	combout => \inst15|inst2|inst1|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N8
\inst17|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst~combout\ = (\inst19|inst43~q\ & (\inst19|inst42~q\ & (\inst|inst41~q\ & \inst|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datab => \inst19|inst42~q\,
	datac => \inst|inst41~q\,
	datad => \inst|inst19~q\,
	combout => \inst17|inst1|inst|inst~combout\);

-- Location: LCCOMB_X1_Y22_N2
\inst15|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst5~combout\ = (\inst19|inst43~q\ & (\inst|inst8~q\ $ (((\inst19|inst42~q\ & \inst|inst19~q\))))) # (!\inst19|inst43~q\ & (\inst19|inst42~q\ & (\inst|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datab => \inst19|inst42~q\,
	datac => \inst|inst19~q\,
	datad => \inst|inst8~q\,
	combout => \inst15|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N20
\inst17|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst2~0_combout\ = (\inst17|inst1|inst|inst~combout\ & ((\inst15|inst1|inst|inst5~combout\) # ((\inst|inst41~q\ & \inst19|inst41~q\)))) # (!\inst17|inst1|inst|inst~combout\ & (\inst|inst41~q\ & (\inst15|inst1|inst|inst5~combout\ & 
-- \inst19|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst17|inst1|inst|inst~combout\,
	datac => \inst15|inst1|inst|inst5~combout\,
	datad => \inst19|inst41~q\,
	combout => \inst17|inst2|inst2~0_combout\);

-- Location: LCCOMB_X1_Y22_N6
\inst17|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst2~0_combout\ = (\inst15|inst2|inst1|inst5~combout\ & ((\inst17|inst2|inst2~0_combout\) # ((\inst|inst41~q\ & \inst19|inst19~q\)))) # (!\inst15|inst2|inst1|inst5~combout\ & (\inst|inst41~q\ & (\inst19|inst19~q\ & 
-- \inst17|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst15|inst2|inst1|inst5~combout\,
	datac => \inst19|inst19~q\,
	datad => \inst17|inst2|inst2~0_combout\,
	combout => \inst17|inst3|inst2~0_combout\);

-- Location: LCCOMB_X2_Y22_N26
\inst17|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst2~0_combout\ = (\inst15|inst3|inst1|inst5~combout\ & ((\inst17|inst3|inst2~0_combout\) # ((\inst19|inst8~q\ & \inst|inst41~q\)))) # (!\inst15|inst3|inst1|inst5~combout\ & (\inst19|inst8~q\ & (\inst17|inst3|inst2~0_combout\ & 
-- \inst|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst8~q\,
	datab => \inst15|inst3|inst1|inst5~combout\,
	datac => \inst17|inst3|inst2~0_combout\,
	datad => \inst|inst41~q\,
	combout => \inst17|inst4|inst2~0_combout\);

-- Location: LCCOMB_X2_Y22_N28
\inst9|LPM_MUX_component|auto_generated|result_node[3]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ = (\inst|inst8~q\ & \inst19|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst8~q\,
	datad => \inst19|inst19~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\);

-- Location: LCCOMB_X2_Y22_N12
\inst15|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst2~0_combout\ = (\inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ & ((\inst15|inst2|inst2~0_combout\) # ((\inst19|inst19~q\ & \inst|inst19~q\)))) # (!\inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ & 
-- (\inst19|inst19~q\ & (\inst|inst19~q\ & \inst15|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst19~q\,
	datac => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst2~0_combout\);

-- Location: LCCOMB_X2_Y22_N18
\inst15|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst1|inst5~combout\ = \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ $ (\inst15|inst3|inst2~0_combout\ $ (((\inst|inst19~q\ & \inst19|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst19~q\,
	datab => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\,
	datac => \inst19|inst8~q\,
	datad => \inst15|inst3|inst2~0_combout\,
	combout => \inst15|inst4|inst1|inst5~combout\);

-- Location: LCCOMB_X2_Y22_N6
\inst17|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst1|inst5~combout\ = \inst17|inst3|inst2~0_combout\ $ (\inst15|inst3|inst1|inst5~combout\ $ (((\inst|inst41~q\ & \inst19|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst17|inst3|inst2~0_combout\,
	datac => \inst19|inst8~q\,
	datad => \inst15|inst3|inst1|inst5~combout\,
	combout => \inst17|inst4|inst1|inst5~combout\);

-- Location: IOIBUF_X0_Y25_N15
\SW[6]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: FF_X1_Y22_N31
\inst|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[6]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst42~q\);

-- Location: LCCOMB_X1_Y22_N26
\inst17|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst1|inst5~combout\ = \inst17|inst2|inst2~0_combout\ $ (\inst15|inst2|inst1|inst5~combout\ $ (((\inst|inst41~q\ & \inst19|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst17|inst2|inst2~0_combout\,
	datac => \inst19|inst19~q\,
	datad => \inst15|inst2|inst1|inst5~combout\,
	combout => \inst17|inst3|inst1|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N0
\inst17|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst1|inst5~0_combout\ = \inst17|inst1|inst|inst~combout\ $ (\inst15|inst1|inst|inst5~combout\ $ (((\inst|inst41~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst17|inst1|inst|inst~combout\,
	datac => \inst15|inst1|inst|inst5~combout\,
	datad => \inst19|inst41~q\,
	combout => \inst17|inst2|inst1|inst5~0_combout\);

-- Location: LCCOMB_X1_Y22_N10
\inst25|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst~combout\ = (\inst19|inst43~q\ & (\inst|inst42~q\ & (\inst19|inst42~q\ & \inst|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datab => \inst|inst42~q\,
	datac => \inst19|inst42~q\,
	datad => \inst|inst41~q\,
	combout => \inst25|inst1|inst|inst~combout\);

-- Location: LCCOMB_X1_Y22_N4
\inst17|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst5~combout\ = (\inst19|inst42~q\ & (\inst|inst41~q\ $ (((\inst|inst19~q\ & \inst19|inst43~q\))))) # (!\inst19|inst42~q\ & (\inst|inst19~q\ & (\inst19|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datab => \inst|inst19~q\,
	datac => \inst19|inst43~q\,
	datad => \inst|inst41~q\,
	combout => \inst17|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N14
\inst25|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst2~0_combout\ = (\inst25|inst1|inst|inst~combout\ & ((\inst17|inst1|inst|inst5~combout\) # ((\inst|inst42~q\ & \inst19|inst41~q\)))) # (!\inst25|inst1|inst|inst~combout\ & (\inst|inst42~q\ & (\inst17|inst1|inst|inst5~combout\ & 
-- \inst19|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst1|inst|inst~combout\,
	datab => \inst|inst42~q\,
	datac => \inst17|inst1|inst|inst5~combout\,
	datad => \inst19|inst41~q\,
	combout => \inst25|inst2|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N28
\inst25|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst2~0_combout\ = (\inst17|inst2|inst1|inst5~0_combout\ & ((\inst25|inst2|inst2~0_combout\) # ((\inst19|inst19~q\ & \inst|inst42~q\)))) # (!\inst17|inst2|inst1|inst5~0_combout\ & (\inst19|inst19~q\ & (\inst|inst42~q\ & 
-- \inst25|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst42~q\,
	datac => \inst17|inst2|inst1|inst5~0_combout\,
	datad => \inst25|inst2|inst2~0_combout\,
	combout => \inst25|inst3|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N0
\inst25|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst2~0_combout\ = (\inst17|inst3|inst1|inst5~combout\ & ((\inst25|inst3|inst2~0_combout\) # ((\inst|inst42~q\ & \inst19|inst8~q\)))) # (!\inst17|inst3|inst1|inst5~combout\ & (\inst|inst42~q\ & (\inst19|inst8~q\ & 
-- \inst25|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst19|inst8~q\,
	datac => \inst17|inst3|inst1|inst5~combout\,
	datad => \inst25|inst3|inst2~0_combout\,
	combout => \inst25|inst4|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N8
\inst25|inst57|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst57|inst1|inst~combout\ = (\inst17|inst4|inst1|inst5~combout\ & \inst25|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst17|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst57|inst1|inst~combout\);

-- Location: LCCOMB_X1_Y22_N30
\inst25|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst5~combout\ = (\inst19|inst43~q\ & (\inst|inst41~q\ $ (((\inst19|inst42~q\ & \inst|inst42~q\))))) # (!\inst19|inst43~q\ & (\inst19|inst42~q\ & (\inst|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datab => \inst19|inst42~q\,
	datac => \inst|inst42~q\,
	datad => \inst|inst41~q\,
	combout => \inst25|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N24
\inst32|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst~combout\ = (\inst|inst43~q\ & (\inst|inst42~q\ & (\inst19|inst43~q\ & \inst19|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst43~q\,
	datab => \inst|inst42~q\,
	datac => \inst19|inst43~q\,
	datad => \inst19|inst42~q\,
	combout => \inst32|inst1|inst|inst~combout\);

-- Location: LCCOMB_X1_Y22_N16
\inst32|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst2~0_combout\ = (\inst25|inst1|inst|inst5~combout\ & ((\inst32|inst1|inst|inst~combout\) # ((\inst|inst43~q\ & \inst19|inst41~q\)))) # (!\inst25|inst1|inst|inst5~combout\ & (\inst32|inst1|inst|inst~combout\ & (\inst|inst43~q\ & 
-- \inst19|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst1|inst|inst5~combout\,
	datab => \inst32|inst1|inst|inst~combout\,
	datac => \inst|inst43~q\,
	datad => \inst19|inst41~q\,
	combout => \inst32|inst2|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N24
\inst25|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst1|inst5~0_combout\ = \inst25|inst1|inst|inst~combout\ $ (\inst17|inst1|inst|inst5~combout\ $ (((\inst|inst42~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst19|inst41~q\,
	datac => \inst25|inst1|inst|inst~combout\,
	datad => \inst17|inst1|inst|inst5~combout\,
	combout => \inst25|inst2|inst1|inst5~0_combout\);

-- Location: LCCOMB_X3_Y22_N14
\inst32|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst2~0_combout\ = (\inst32|inst2|inst2~0_combout\ & ((\inst25|inst2|inst1|inst5~0_combout\) # ((\inst19|inst19~q\ & \inst|inst43~q\)))) # (!\inst32|inst2|inst2~0_combout\ & (\inst19|inst19~q\ & (\inst|inst43~q\ & 
-- \inst25|inst2|inst1|inst5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst43~q\,
	datac => \inst32|inst2|inst2~0_combout\,
	datad => \inst25|inst2|inst1|inst5~0_combout\,
	combout => \inst32|inst3|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N16
\inst25|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst1|inst5~combout\ = \inst17|inst2|inst1|inst5~0_combout\ $ (\inst25|inst2|inst2~0_combout\ $ (((\inst19|inst19~q\ & \inst|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst42~q\,
	datac => \inst17|inst2|inst1|inst5~0_combout\,
	datad => \inst25|inst2|inst2~0_combout\,
	combout => \inst25|inst3|inst1|inst5~combout\);

-- Location: LCCOMB_X3_Y22_N22
\inst32|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst2~0_combout\ = (\inst32|inst3|inst2~0_combout\ & ((\inst25|inst3|inst1|inst5~combout\) # ((\inst19|inst8~q\ & \inst|inst43~q\)))) # (!\inst32|inst3|inst2~0_combout\ & (\inst19|inst8~q\ & (\inst|inst43~q\ & 
-- \inst25|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst8~q\,
	datab => \inst|inst43~q\,
	datac => \inst32|inst3|inst2~0_combout\,
	datad => \inst25|inst3|inst1|inst5~combout\,
	combout => \inst32|inst4|inst2~0_combout\);

-- Location: LCCOMB_X3_Y22_N30
\inst25|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst1|inst5~combout\ = \inst17|inst3|inst1|inst5~combout\ $ (\inst25|inst3|inst2~0_combout\ $ (((\inst|inst42~q\ & \inst19|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst19|inst8~q\,
	datac => \inst17|inst3|inst1|inst5~combout\,
	datad => \inst25|inst3|inst2~0_combout\,
	combout => \inst25|inst4|inst1|inst5~combout\);

-- Location: LCCOMB_X3_Y22_N6
\inst32|inst5|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst5|inst1|inst~combout\ = (\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & (\inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst32|inst5|inst1|inst~combout\);

-- Location: LCCOMB_X3_Y22_N20
\inst32|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst34|inst1|inst5~combout\ = \inst17|inst4|inst2~0_combout\ $ (\inst15|inst4|inst1|inst5~combout\ $ (\inst25|inst57|inst1|inst~combout\ $ (\inst32|inst5|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst15|inst4|inst1|inst5~combout\,
	datac => \inst25|inst57|inst1|inst~combout\,
	datad => \inst32|inst5|inst1|inst~combout\,
	combout => \inst32|inst34|inst1|inst5~combout\);

-- Location: FF_X3_Y22_N21
\inst38|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst34|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst41~q\);

-- Location: LCCOMB_X2_Y22_N8
\inst32|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst1|inst5~combout\ = \inst32|inst2|inst2~0_combout\ $ (\inst25|inst2|inst1|inst5~0_combout\ $ (((\inst19|inst19~q\ & \inst|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst2~0_combout\,
	datab => \inst19|inst19~q\,
	datac => \inst|inst43~q\,
	datad => \inst25|inst2|inst1|inst5~0_combout\,
	combout => \inst32|inst3|inst1|inst5~combout\);

-- Location: FF_X2_Y22_N9
\inst39|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst3|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst19~q\);

-- Location: LCCOMB_X1_Y23_N18
\inst32|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst1|inst5~0_combout\ = \inst32|inst1|inst|inst~combout\ $ (\inst25|inst1|inst|inst5~combout\ $ (((\inst19|inst41~q\ & \inst|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst41~q\,
	datab => \inst|inst43~q\,
	datac => \inst32|inst1|inst|inst~combout\,
	datad => \inst25|inst1|inst|inst5~combout\,
	combout => \inst32|inst2|inst1|inst5~0_combout\);

-- Location: FF_X1_Y23_N19
\inst39|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst2|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst41~q\);

-- Location: LCCOMB_X4_Y22_N20
\inst32|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst1|inst5~combout\ = \inst25|inst3|inst1|inst5~combout\ $ (\inst32|inst3|inst2~0_combout\ $ (((\inst19|inst8~q\ & \inst|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst3|inst1|inst5~combout\,
	datab => \inst19|inst8~q\,
	datac => \inst|inst43~q\,
	datad => \inst32|inst3|inst2~0_combout\,
	combout => \inst32|inst4|inst1|inst5~combout\);

-- Location: FF_X4_Y22_N21
\inst39|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst4|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst8~q\);

-- Location: LCCOMB_X4_Y23_N24
\inst42|seven_seg0[6]~300\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~300_combout\ = (\inst39|inst41~q\ & ((\inst39|inst8~q\) # ((\inst39|inst19~q\ & \inst38|inst41~q\)))) # (!\inst39|inst41~q\ & (((!\inst39|inst8~q\) # (!\inst38|inst41~q\)) # (!\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111110110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~300_combout\);

-- Location: LCCOMB_X1_Y23_N0
\inst32|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst5~combout\ = (\inst19|inst42~q\ & (\inst|inst43~q\ $ (((\inst19|inst43~q\ & \inst|inst42~q\))))) # (!\inst19|inst42~q\ & (((\inst19|inst43~q\ & \inst|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datab => \inst|inst43~q\,
	datac => \inst19|inst43~q\,
	datad => \inst|inst42~q\,
	combout => \inst32|inst1|inst|inst5~combout\);

-- Location: FF_X1_Y23_N1
\inst39|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst1|inst|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst42~q\);

-- Location: LCCOMB_X4_Y23_N26
\inst42|seven_seg0[6]~301\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~301_combout\ = (\inst42|seven_seg0[6]~300_combout\ & (\inst38|inst41~q\ $ (\inst39|inst42~q\ $ (!\inst39|inst19~q\)))) # (!\inst42|seven_seg0[6]~300_combout\ & (!\inst38|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[6]~300_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~301_combout\);

-- Location: LCCOMB_X4_Y22_N30
\inst32|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst57|inst1|inst5~combout\ = \inst25|inst4|inst1|inst5~combout\ $ (\inst32|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst57|inst1|inst5~combout\);

-- Location: FF_X4_Y22_N31
\inst38|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst57|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst43~q\);

-- Location: LCCOMB_X2_Y22_N20
\inst15|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst2~0_combout\ = (\inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ & ((\inst15|inst3|inst2~0_combout\) # ((\inst|inst19~q\ & \inst19|inst8~q\)))) # (!\inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ & 
-- (\inst|inst19~q\ & (\inst19|inst8~q\ & \inst15|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst19~q\,
	datab => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\,
	datac => \inst19|inst8~q\,
	datad => \inst15|inst3|inst2~0_combout\,
	combout => \inst15|inst4|inst2~0_combout\);

-- Location: LCCOMB_X2_Y22_N14
\inst15|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst5~combout\ = \inst15|inst4|inst2~0_combout\ $ (((\inst19|inst8~q\ & \inst|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst8~q\,
	datab => \inst|inst8~q\,
	datad => \inst15|inst4|inst2~0_combout\,
	combout => \inst15|inst57|inst1|inst5~combout\);

-- Location: LCCOMB_X3_Y22_N4
\inst25|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst34|inst1|inst5~combout\ = \inst15|inst57|inst1|inst5~combout\ $ (((\inst17|inst4|inst2~0_combout\ & ((\inst15|inst4|inst1|inst5~combout\) # (\inst25|inst57|inst1|inst~combout\))) # (!\inst17|inst4|inst2~0_combout\ & 
-- (\inst15|inst4|inst1|inst5~combout\ & \inst25|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011111101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst15|inst4|inst1|inst5~combout\,
	datac => \inst25|inst57|inst1|inst~combout\,
	datad => \inst15|inst57|inst1|inst5~combout\,
	combout => \inst25|inst34|inst1|inst5~combout\);

-- Location: LCCOMB_X3_Y22_N18
\inst32|inst34|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst34|inst1|inst~combout\ = (\inst32|inst5|inst1|inst~combout\ & (\inst17|inst4|inst2~0_combout\ $ (\inst15|inst4|inst1|inst5~combout\ $ (\inst25|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst15|inst4|inst1|inst5~combout\,
	datac => \inst25|inst57|inst1|inst~combout\,
	datad => \inst32|inst5|inst1|inst~combout\,
	combout => \inst32|inst34|inst1|inst~combout\);

-- Location: LCCOMB_X3_Y22_N10
\inst32|inst488|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst488|inst1|inst5~combout\ = \inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst25|inst34|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst~combout\,
	combout => \inst32|inst488|inst1|inst5~combout\);

-- Location: FF_X3_Y22_N11
\inst38|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst488|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst19~q\);

-- Location: LCCOMB_X3_Y22_N12
\inst32|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst5|inst1|inst5~combout\ = \inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\ $ (((\inst32|inst4|inst2~0_combout\ & \inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst32|inst5|inst1|inst5~combout\);

-- Location: FF_X3_Y22_N13
\inst38|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst5|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst42~q\);

-- Location: LCCOMB_X14_Y22_N24
\inst42|seven_seg0[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~66_combout\ = \inst38|inst19~q\ $ (\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst19~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~66_combout\);

-- Location: LCCOMB_X8_Y22_N30
\inst42|seven_seg0[6]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~71_combout\ = (\inst39|inst8~q\ & !\inst39|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[6]~71_combout\);

-- Location: LCCOMB_X7_Y22_N20
\inst42|seven_seg0[6]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~68_combout\ = \inst39|inst19~q\ $ (\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~68_combout\);

-- Location: LCCOMB_X8_Y22_N28
\inst42|seven_seg0[6]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~70_combout\ = (\inst38|inst41~q\ & (\inst39|inst19~q\ & (\inst39|inst41~q\ $ (\inst39|inst8~q\)))) # (!\inst38|inst41~q\ & (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~70_combout\);

-- Location: LCCOMB_X8_Y22_N20
\inst42|seven_seg0[6]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~72_combout\ = (\inst42|seven_seg0[6]~71_combout\ & ((\inst39|inst42~q\ & ((\inst42|seven_seg0[6]~70_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[6]~68_combout\)))) # (!\inst42|seven_seg0[6]~71_combout\ & 
-- ((\inst39|inst42~q\ & (\inst42|seven_seg0[6]~68_combout\)) # (!\inst39|inst42~q\ & ((\inst42|seven_seg0[6]~70_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~71_combout\,
	datab => \inst42|seven_seg0[6]~68_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[6]~70_combout\,
	combout => \inst42|seven_seg0[6]~72_combout\);

-- Location: LCCOMB_X15_Y23_N4
\inst42|seven_seg0[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~67_combout\ = \inst39|inst41~q\ $ (\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~67_combout\);

-- Location: LCCOMB_X15_Y23_N18
\inst42|seven_seg0[6]~294\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~294_combout\ = \inst39|inst19~q\ $ (((\inst38|inst41~q\ & (\inst39|inst41~q\ & !\inst39|inst8~q\)) # (!\inst38|inst41~q\ & (!\inst39|inst41~q\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~294_combout\);

-- Location: LCCOMB_X15_Y23_N14
\inst42|seven_seg0[6]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~69_combout\ = (\inst39|inst42~q\ & (!\inst42|seven_seg0[6]~294_combout\ & ((\inst42|seven_seg0[6]~67_combout\) # (!\inst38|inst41~q\)))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[6]~294_combout\ & ((\inst38|inst41~q\) # 
-- (\inst42|seven_seg0[6]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[6]~294_combout\,
	combout => \inst42|seven_seg0[6]~69_combout\);

-- Location: LCCOMB_X14_Y22_N2
\inst42|seven_seg0[6]~273\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~273_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst19~q\ & \inst42|seven_seg0[6]~69_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[6]~72_combout\ & (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~72_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~69_combout\,
	combout => \inst42|seven_seg0[6]~273_combout\);

-- Location: LCCOMB_X14_Y22_N30
\inst42|seven_seg0[6]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~73_combout\ = (!\inst38|inst43~q\ & ((\inst42|seven_seg0[6]~273_combout\) # ((\inst42|seven_seg0[6]~301_combout\ & !\inst42|seven_seg0[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~301_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[6]~66_combout\,
	datad => \inst42|seven_seg0[6]~273_combout\,
	combout => \inst42|seven_seg0[6]~73_combout\);

-- Location: LCCOMB_X4_Y23_N28
\inst42|seven_seg0[6]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~74_combout\ = (\inst39|inst19~q\ & (\inst38|inst41~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (!\inst38|inst41~q\ & (\inst39|inst41~q\ $ (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~74_combout\);

-- Location: LCCOMB_X8_Y22_N26
\inst42|seven_seg0[6]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~75_combout\ = \inst39|inst42~q\ $ (((!\inst39|inst8~q\ & \inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010010110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~75_combout\);

-- Location: LCCOMB_X8_Y22_N24
\inst42|seven_seg0[6]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~76_combout\ = (!\inst42|seven_seg0[6]~66_combout\ & ((\inst42|seven_seg0[6]~75_combout\ & (\inst42|seven_seg0[6]~74_combout\)) # (!\inst42|seven_seg0[6]~75_combout\ & ((\inst42|seven_seg0[6]~68_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~74_combout\,
	datab => \inst42|seven_seg0[6]~68_combout\,
	datac => \inst42|seven_seg0[6]~75_combout\,
	datad => \inst42|seven_seg0[6]~66_combout\,
	combout => \inst42|seven_seg0[6]~76_combout\);

-- Location: LCCOMB_X8_Y22_N22
\inst42|seven_seg0[6]~274\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~274_combout\ = (\inst42|seven_seg0[6]~76_combout\) # ((\inst38|inst42~q\ & (\inst42|seven_seg0[6]~72_combout\ & !\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[6]~72_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~76_combout\,
	combout => \inst42|seven_seg0[6]~274_combout\);

-- Location: LCCOMB_X7_Y22_N14
\inst42|seven_seg0[6]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~77_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ $ (\inst39|inst42~q\ $ (\inst38|inst41~q\)))) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\ & (!\inst39|inst42~q\ & \inst38|inst41~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ & 
-- !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~77_combout\);

-- Location: LCCOMB_X7_Y22_N28
\inst42|seven_seg0[6]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~78_combout\ = (\inst39|inst19~q\ & ((\inst39|inst42~q\ $ (!\inst38|inst41~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst38|inst41~q\)) # (!\inst39|inst42~q\ & ((\inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~78_combout\);

-- Location: LCCOMB_X7_Y22_N30
\inst42|seven_seg0[6]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~79_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[6]~77_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~78_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~77_combout\,
	datad => \inst42|seven_seg0[6]~78_combout\,
	combout => \inst42|seven_seg0[6]~79_combout\);

-- Location: LCCOMB_X6_Y22_N28
\inst42|seven_seg0[6]~275\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~275_combout\ = (\inst42|seven_seg0[6]~274_combout\) # ((\inst38|inst19~q\ & (\inst42|seven_seg0[6]~79_combout\ & !\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[6]~274_combout\,
	datac => \inst42|seven_seg0[6]~79_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~275_combout\);

-- Location: LCCOMB_X6_Y22_N0
\inst42|seven_seg0[6]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~80_combout\ = (\inst42|seven_seg0[6]~73_combout\) # ((\inst38|inst43~q\ & \inst42|seven_seg0[6]~275_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~73_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~275_combout\,
	combout => \inst42|seven_seg0[6]~80_combout\);

-- Location: LCCOMB_X7_Y22_N4
\inst42|seven_seg0[6]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~93_combout\ = (\inst42|seven_seg0[6]~68_combout\ & ((\inst39|inst42~q\ & (!\inst39|inst8~q\ & \inst39|inst41~q\)) # (!\inst39|inst42~q\ & (\inst39|inst8~q\ & !\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg0[6]~68_combout\,
	combout => \inst42|seven_seg0[6]~93_combout\);

-- Location: LCCOMB_X7_Y22_N6
\inst42|seven_seg0[6]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~94_combout\ = (\inst39|inst42~q\ & (!\inst39|inst41~q\ & (\inst39|inst8~q\ & \inst38|inst41~q\))) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & (!\inst39|inst8~q\ & !\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~94_combout\);

-- Location: LCCOMB_X7_Y22_N0
\inst42|seven_seg0[6]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~95_combout\ = (\inst42|seven_seg0[6]~93_combout\) # ((\inst42|seven_seg0[6]~94_combout\ & (\inst39|inst19~q\ $ (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~93_combout\,
	datad => \inst42|seven_seg0[6]~94_combout\,
	combout => \inst42|seven_seg0[6]~95_combout\);

-- Location: LCCOMB_X4_Y23_N22
\inst42|seven_seg0[6]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~81_combout\ = (\inst38|inst41~q\ & ((\inst39|inst41~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\)) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\)))) # (!\inst38|inst41~q\ & (!\inst39|inst41~q\ & (\inst39|inst42~q\ $ 
-- (!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~81_combout\);

-- Location: LCCOMB_X7_Y23_N28
\inst42|seven_seg0[6]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~82_combout\ = (\inst39|inst41~q\ & (!\inst38|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\))) # (!\inst39|inst41~q\ & (\inst38|inst41~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~82_combout\);

-- Location: LCCOMB_X4_Y22_N22
\inst42|seven_seg0[6]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~99_combout\ = (\inst38|inst42~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[6]~81_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~82_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~81_combout\,
	datad => \inst42|seven_seg0[6]~82_combout\,
	combout => \inst42|seven_seg0[6]~99_combout\);

-- Location: LCCOMB_X4_Y22_N24
\inst42|seven_seg0[6]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~100_combout\ = (\inst42|seven_seg0[6]~99_combout\) # ((\inst42|seven_seg0[6]~95_combout\ & !\inst38|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg0[6]~95_combout\,
	datac => \inst42|seven_seg0[6]~99_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~100_combout\);

-- Location: LCCOMB_X12_Y22_N18
\inst42|seven_seg0[6]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~89_combout\ = (!\inst38|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~89_combout\);

-- Location: LCCOMB_X12_Y22_N16
\inst42|seven_seg0[6]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~88_combout\ = (\inst38|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\))) # (!\inst38|inst41~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~88_combout\);

-- Location: LCCOMB_X12_Y22_N8
\inst42|seven_seg0[6]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~90_combout\ = (\inst39|inst41~q\ & (((\inst39|inst8~q\ & \inst42|seven_seg0[6]~88_combout\)))) # (!\inst39|inst41~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[6]~89_combout\)) # (!\inst39|inst8~q\ & 
-- ((\inst42|seven_seg0[6]~88_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[6]~89_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[6]~88_combout\,
	combout => \inst42|seven_seg0[6]~90_combout\);

-- Location: LCCOMB_X4_Y23_N8
\inst42|seven_seg0[6]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~91_combout\ = (\inst38|inst41~q\ & (\inst39|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\)))) # (!\inst38|inst41~q\ & ((\inst39|inst41~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\)) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & 
-- !\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~91_combout\);

-- Location: LCCOMB_X4_Y23_N2
\inst42|seven_seg0[6]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~92_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~82_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~91_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~91_combout\,
	datad => \inst42|seven_seg0[6]~82_combout\,
	combout => \inst42|seven_seg0[6]~92_combout\);

-- Location: LCCOMB_X4_Y22_N28
\inst42|seven_seg0[6]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~96_combout\ = (\inst38|inst42~q\ & (!\inst38|inst19~q\ & (\inst42|seven_seg0[6]~95_combout\))) # (!\inst38|inst42~q\ & (\inst38|inst19~q\ & ((\inst42|seven_seg0[6]~92_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~95_combout\,
	datad => \inst42|seven_seg0[6]~92_combout\,
	combout => \inst42|seven_seg0[6]~96_combout\);

-- Location: LCCOMB_X4_Y22_N26
\inst42|seven_seg0[6]~276\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~276_combout\ = (\inst42|seven_seg0[6]~96_combout\) # ((\inst42|seven_seg0[6]~90_combout\ & (\inst38|inst42~q\ $ (!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~90_combout\,
	datad => \inst42|seven_seg0[6]~96_combout\,
	combout => \inst42|seven_seg0[6]~276_combout\);

-- Location: LCCOMB_X4_Y22_N14
\inst42|seven_seg0[6]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~101_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~276_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[6]~100_combout\ & ((\inst42|seven_seg0[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[6]~100_combout\,
	datac => \inst42|seven_seg0[6]~276_combout\,
	datad => \inst42|seven_seg0[6]~66_combout\,
	combout => \inst42|seven_seg0[6]~101_combout\);

-- Location: LCCOMB_X15_Y23_N6
\inst42|seven_seg0[6]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~85_combout\ = (\inst39|inst42~q\ & (\inst39|inst19~q\ $ (\inst38|inst41~q\))) # (!\inst39|inst42~q\ & (!\inst39|inst19~q\ & !\inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~85_combout\);

-- Location: LCCOMB_X15_Y23_N0
\inst42|seven_seg0[6]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~84_combout\ = (\inst38|inst41~q\ & (\inst39|inst41~q\ & (\inst39|inst19~q\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~84_combout\);

-- Location: LCCOMB_X15_Y23_N20
\inst42|seven_seg0[6]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~86_combout\ = (\inst42|seven_seg0[6]~85_combout\ & (((!\inst39|inst42~q\ & \inst42|seven_seg0[6]~84_combout\)) # (!\inst42|seven_seg0[6]~67_combout\))) # (!\inst42|seven_seg0[6]~85_combout\ & (((!\inst39|inst42~q\ & 
-- \inst42|seven_seg0[6]~84_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~85_combout\,
	datab => \inst42|seven_seg0[6]~67_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[6]~84_combout\,
	combout => \inst42|seven_seg0[6]~86_combout\);

-- Location: LCCOMB_X4_Y22_N0
\inst42|seven_seg0[6]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~102_combout\ = (\inst42|seven_seg0[6]~101_combout\) # ((!\inst38|inst43~q\ & (\inst42|seven_seg0[6]~86_combout\ & !\inst42|seven_seg0[6]~66_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[6]~101_combout\,
	datac => \inst42|seven_seg0[6]~86_combout\,
	datad => \inst42|seven_seg0[6]~66_combout\,
	combout => \inst42|seven_seg0[6]~102_combout\);

-- Location: LCCOMB_X3_Y22_N26
\inst32|inst667|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~0_combout\ = (\inst15|inst57|inst1|inst5~combout\ & ((\inst17|inst4|inst2~0_combout\ & ((\inst15|inst4|inst1|inst5~combout\) # (\inst25|inst57|inst1|inst~combout\))) # (!\inst17|inst4|inst2~0_combout\ & 
-- (\inst15|inst4|inst1|inst5~combout\ & \inst25|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst15|inst4|inst1|inst5~combout\,
	datac => \inst25|inst57|inst1|inst~combout\,
	datad => \inst15|inst57|inst1|inst5~combout\,
	combout => \inst32|inst667|inst1|inst5~0_combout\);

-- Location: LCCOMB_X2_Y22_N4
\inst15|inst57|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst~combout\ = (\inst|inst8~q\ & (\inst19|inst8~q\ & \inst15|inst4|inst2~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst8~q\,
	datac => \inst19|inst8~q\,
	datad => \inst15|inst4|inst2~0_combout\,
	combout => \inst15|inst57|inst1|inst~combout\);

-- Location: LCCOMB_X3_Y22_N2
\inst32|inst667|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~combout\ = \inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\ $ (((\inst25|inst34|inst1|inst5~combout\ & \inst32|inst34|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst25|inst34|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst~combout\,
	combout => \inst32|inst667|inst1|inst5~combout\);

-- Location: FF_X3_Y22_N3
\inst38|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst667|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst8~q\);

-- Location: LCCOMB_X4_Y22_N16
\inst42|seven_seg0[6]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~83_combout\ = (!\inst38|inst19~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~81_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~82_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~82_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~81_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~83_combout\);

-- Location: LCCOMB_X4_Y22_N2
\inst42|seven_seg0[6]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~87_combout\ = (\inst42|seven_seg0[6]~83_combout\) # ((\inst38|inst19~q\ & \inst42|seven_seg0[6]~86_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~86_combout\,
	datad => \inst42|seven_seg0[6]~83_combout\,
	combout => \inst42|seven_seg0[6]~87_combout\);

-- Location: LCCOMB_X4_Y22_N8
\inst42|seven_seg0[6]~277\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~277_combout\ = (\inst38|inst43~q\ & (\inst42|seven_seg0[6]~87_combout\ & ((!\inst38|inst42~q\)))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~276_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[6]~87_combout\,
	datac => \inst42|seven_seg0[6]~276_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~277_combout\);

-- Location: LCCOMB_X4_Y23_N20
\inst42|seven_seg0[3]~278\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~278_combout\ = (\inst39|inst8~q\ & (((\inst42|seven_seg0[6]~81_combout\ & \inst38|inst19~q\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~91_combout\ & ((!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[6]~91_combout\,
	datac => \inst42|seven_seg0[6]~81_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[3]~278_combout\);

-- Location: LCCOMB_X4_Y23_N30
\inst42|seven_seg0[6]~279\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~279_combout\ = (\inst42|seven_seg0[3]~278_combout\) # ((\inst42|seven_seg0[6]~82_combout\ & (\inst39|inst8~q\ $ (\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[3]~278_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~82_combout\,
	combout => \inst42|seven_seg0[6]~279_combout\);

-- Location: LCCOMB_X6_Y22_N10
\inst42|seven_seg0[6]~280\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~280_combout\ = (\inst42|seven_seg0[6]~277_combout\) # ((\inst38|inst42~q\ & (\inst38|inst43~q\ & \inst42|seven_seg0[6]~279_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[6]~277_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~279_combout\,
	combout => \inst42|seven_seg0[6]~280_combout\);

-- Location: LCCOMB_X6_Y22_N26
\inst42|seven_seg0[6]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~103_combout\ = (\inst38|inst8~q\ & (((\inst39|inst43~q\) # (\inst42|seven_seg0[6]~280_combout\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[6]~102_combout\ & (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~102_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[6]~280_combout\,
	combout => \inst42|seven_seg0[6]~103_combout\);

-- Location: LCCOMB_X14_Y22_N16
\inst42|seven_seg0[6]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~104_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg0[6]~301_combout\)) # (!\inst38|inst19~q\ & ((\inst42|seven_seg0[6]~69_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~301_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~69_combout\,
	combout => \inst42|seven_seg0[6]~104_combout\);

-- Location: LCCOMB_X6_Y22_N12
\inst42|seven_seg0[6]~281\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~281_combout\ = (\inst38|inst43~q\ & (!\inst38|inst42~q\ & (\inst42|seven_seg0[6]~104_combout\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~275_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[6]~104_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~275_combout\,
	combout => \inst42|seven_seg0[6]~281_combout\);

-- Location: LCCOMB_X20_Y25_N24
\inst42|seven_seg0[0]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~98_combout\ = (\inst38|inst42~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[0]~98_combout\);

-- Location: LCCOMB_X19_Y23_N28
\inst42|seven_seg1[3]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~39_combout\ = (\inst38|inst19~q\ & (\inst38|inst42~q\ & \inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[3]~39_combout\);

-- Location: LCCOMB_X12_Y22_N26
\inst42|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~1_combout\ = (\inst38|inst8~q\ & (\inst38|inst43~q\ & \inst42|seven_seg1[3]~39_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~39_combout\,
	combout => \inst42|Equal0~1_combout\);

-- Location: LCCOMB_X12_Y22_N24
\inst42|seven_seg0[6]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~106_combout\ = (\inst39|inst41~q\ & (!\inst39|inst42~q\ & (\inst39|inst8~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~106_combout\);

-- Location: LCCOMB_X12_Y22_N2
\inst42|seven_seg0[6]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~107_combout\ = (\inst42|seven_seg0[6]~89_combout\) # ((\inst42|Equal0~1_combout\ & (\inst42|seven_seg0[6]~106_combout\ & \inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~1_combout\,
	datab => \inst42|seven_seg0[6]~106_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[6]~89_combout\,
	combout => \inst42|seven_seg0[6]~107_combout\);

-- Location: LCCOMB_X8_Y22_N6
\inst42|seven_seg0[6]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~108_combout\ = (\inst39|inst41~q\ & (((\inst42|seven_seg0[6]~107_combout\)))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ $ (((!\inst42|seven_seg0[6]~68_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst42|seven_seg0[6]~107_combout\,
	datad => \inst42|seven_seg0[6]~68_combout\,
	combout => \inst42|seven_seg0[6]~108_combout\);

-- Location: LCCOMB_X7_Y22_N10
\inst42|seven_seg0[6]~282\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~282_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg0[6]~108_combout\ & (\inst39|inst8~q\))) # (!\inst38|inst19~q\ & (((!\inst39|inst8~q\ & \inst42|seven_seg0[6]~78_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~108_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[6]~78_combout\,
	combout => \inst42|seven_seg0[6]~282_combout\);

-- Location: LCCOMB_X7_Y22_N18
\inst42|seven_seg0[6]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~105_combout\ = (\inst42|seven_seg0[6]~77_combout\ & (\inst39|inst8~q\ $ (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~77_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~105_combout\);

-- Location: LCCOMB_X6_Y22_N20
\inst42|seven_seg0[6]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~109_combout\ = (\inst42|seven_seg0[6]~281_combout\) # ((\inst42|seven_seg0[0]~98_combout\ & ((\inst42|seven_seg0[6]~282_combout\) # (\inst42|seven_seg0[6]~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~281_combout\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst42|seven_seg0[6]~282_combout\,
	datad => \inst42|seven_seg0[6]~105_combout\,
	combout => \inst42|seven_seg0[6]~109_combout\);

-- Location: LCCOMB_X6_Y22_N18
\inst42|seven_seg0[6]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~110_combout\ = (\inst39|inst43~q\ & ((\inst42|seven_seg0[6]~103_combout\ & ((\inst42|seven_seg0[6]~109_combout\))) # (!\inst42|seven_seg0[6]~103_combout\ & (\inst42|seven_seg0[6]~80_combout\)))) # (!\inst39|inst43~q\ & 
-- (((\inst42|seven_seg0[6]~103_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|seven_seg0[6]~80_combout\,
	datac => \inst42|seven_seg0[6]~103_combout\,
	datad => \inst42|seven_seg0[6]~109_combout\,
	combout => \inst42|seven_seg0[6]~110_combout\);

-- Location: LCCOMB_X11_Y25_N28
\inst42|seven_seg0[5]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~114_combout\ = (\inst39|inst41~q\ & ((\inst38|inst8~q\ & ((\inst38|inst41~q\) # (\inst39|inst19~q\))) # (!\inst38|inst8~q\ & ((!\inst39|inst19~q\) # (!\inst38|inst41~q\))))) # (!\inst39|inst41~q\ & (\inst38|inst8~q\ $ 
-- (\inst38|inst41~q\ $ (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~114_combout\);

-- Location: LCCOMB_X11_Y25_N22
\inst42|seven_seg0[5]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~115_combout\ = (\inst38|inst8~q\ & (\inst39|inst41~q\ & ((!\inst39|inst19~q\) # (!\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & ((\inst38|inst41~q\ & (\inst39|inst19~q\ & !\inst39|inst41~q\)) # (!\inst38|inst41~q\ & 
-- (!\inst39|inst19~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~115_combout\);

-- Location: LCCOMB_X11_Y25_N8
\inst42|seven_seg0[5]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~116_combout\ = (\inst42|seven_seg0[5]~115_combout\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~114_combout\) # (!\inst39|inst42~q\))) # (!\inst38|inst43~q\ & ((\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~114_combout\))))) # 
-- (!\inst42|seven_seg0[5]~115_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[5]~114_combout\ $ (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~114_combout\,
	datac => \inst42|seven_seg0[5]~115_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[5]~116_combout\);

-- Location: LCCOMB_X11_Y25_N4
\inst42|seven_seg0[5]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~112_combout\ = (\inst38|inst8~q\ & ((\inst38|inst41~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\))) # (!\inst38|inst41~q\ & (\inst39|inst19~q\ & \inst39|inst41~q\)))) # (!\inst38|inst8~q\ & ((\inst38|inst41~q\ & (!\inst39|inst19~q\ & 
-- !\inst39|inst41~q\)) # (!\inst38|inst41~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100110010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~112_combout\);

-- Location: LCCOMB_X10_Y25_N24
\inst42|seven_seg0[5]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~111_combout\ = (\inst38|inst8~q\ & ((\inst39|inst41~q\) # ((!\inst39|inst19~q\ & !\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst41~q\ $ (((\inst39|inst19~q\ & \inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~111_combout\);

-- Location: LCCOMB_X11_Y25_N18
\inst42|seven_seg0[5]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~113_combout\ = (\inst42|seven_seg0[5]~111_combout\ & (\inst38|inst43~q\ $ (\inst39|inst42~q\ $ (!\inst42|seven_seg0[5]~112_combout\)))) # (!\inst42|seven_seg0[5]~111_combout\ & ((\inst38|inst43~q\ & 
-- ((\inst42|seven_seg0[5]~112_combout\) # (!\inst39|inst42~q\))) # (!\inst38|inst43~q\ & ((\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~112_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100111100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg0[5]~112_combout\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~113_combout\);

-- Location: LCCOMB_X11_Y25_N26
\inst42|seven_seg0[5]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~117_combout\ = (\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~113_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[5]~116_combout\)))) # (!\inst38|inst42~q\ & (!\inst39|inst8~q\ & 
-- ((\inst42|seven_seg0[5]~113_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[5]~116_combout\,
	datad => \inst42|seven_seg0[5]~113_combout\,
	combout => \inst42|seven_seg0[5]~117_combout\);

-- Location: LCCOMB_X11_Y25_N20
\inst42|seven_seg0[5]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~118_combout\ = (\inst38|inst8~q\ & (\inst39|inst41~q\ $ (((\inst38|inst41~q\) # (\inst39|inst19~q\))))) # (!\inst38|inst8~q\ & (((\inst38|inst41~q\ & \inst39|inst19~q\)) # (!\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001011111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~118_combout\);

-- Location: LCCOMB_X11_Y25_N2
\inst42|seven_seg0[5]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~119_combout\ = (\inst42|seven_seg0[5]~118_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[5]~112_combout\ $ (!\inst39|inst42~q\)))) # (!\inst42|seven_seg0[5]~118_combout\ & ((\inst38|inst43~q\ & ((!\inst39|inst42~q\) # 
-- (!\inst42|seven_seg0[5]~112_combout\))) # (!\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~112_combout\) # (\inst39|inst42~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~118_combout\,
	datac => \inst42|seven_seg0[5]~112_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[5]~119_combout\);

-- Location: LCCOMB_X11_Y25_N0
\inst42|seven_seg0[5]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~120_combout\ = (\inst42|seven_seg0[5]~117_combout\) # ((\inst39|inst8~q\ & (!\inst38|inst42~q\ & \inst42|seven_seg0[5]~119_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~117_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~119_combout\,
	combout => \inst42|seven_seg0[5]~120_combout\);

-- Location: LCCOMB_X10_Y25_N22
\inst42|seven_seg0[5]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~121_combout\ = \inst39|inst41~q\ $ (((\inst38|inst8~q\ & (\inst39|inst19~q\ & \inst38|inst41~q\)) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\) # (\inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110110110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~121_combout\);

-- Location: LCCOMB_X10_Y25_N8
\inst42|seven_seg0[5]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~122_combout\ = (\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~111_combout\ & (\inst42|seven_seg0[5]~121_combout\ $ (!\inst38|inst43~q\)))) # (!\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~121_combout\ & (\inst38|inst43~q\ $ 
-- (\inst42|seven_seg0[5]~111_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~121_combout\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~122_combout\);

-- Location: LCCOMB_X10_Y25_N26
\inst42|seven_seg0[5]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~126_combout\ = (\inst38|inst8~q\ & (\inst39|inst41~q\ & ((!\inst38|inst41~q\) # (!\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\ & (!\inst39|inst41~q\ & \inst38|inst41~q\)) # (!\inst39|inst19~q\ & (\inst39|inst41~q\ 
-- & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~126_combout\);

-- Location: LCCOMB_X10_Y25_N20
\inst42|seven_seg0[5]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~125_combout\ = (\inst38|inst8~q\ & (\inst39|inst41~q\ $ (((\inst39|inst19~q\) # (\inst38|inst41~q\))))) # (!\inst38|inst8~q\ & (((\inst39|inst19~q\ & \inst38|inst41~q\)) # (!\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111100101101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~125_combout\);

-- Location: LCCOMB_X10_Y25_N12
\inst42|seven_seg0[5]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~127_combout\ = (\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~125_combout\ & (\inst42|seven_seg0[5]~126_combout\ $ (\inst38|inst43~q\)))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[5]~126_combout\ & (\inst38|inst43~q\ $ 
-- (\inst42|seven_seg0[5]~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~126_combout\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[5]~125_combout\,
	combout => \inst42|seven_seg0[5]~127_combout\);

-- Location: LCCOMB_X10_Y25_N10
\inst42|seven_seg0[5]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~136_combout\ = (\inst38|inst42~q\ & (\inst39|inst8~q\ & (\inst42|seven_seg0[5]~122_combout\))) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~127_combout\))) # (!\inst39|inst8~q\ & 
-- (\inst42|seven_seg0[5]~122_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[5]~122_combout\,
	datad => \inst42|seven_seg0[5]~127_combout\,
	combout => \inst42|seven_seg0[5]~136_combout\);

-- Location: LCCOMB_X10_Y26_N10
\inst42|seven_seg0[5]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~139_combout\ = (\inst39|inst41~q\ & (\inst38|inst8~q\ & (!\inst39|inst19~q\ & \inst39|inst42~q\))) # (!\inst39|inst41~q\ & (!\inst38|inst8~q\ & (\inst39|inst19~q\ & !\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[5]~139_combout\);

-- Location: LCCOMB_X10_Y26_N4
\inst42|seven_seg0[5]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~138_combout\ = (\inst39|inst42~q\ & ((\inst39|inst19~q\ & (\inst38|inst8~q\)) # (!\inst39|inst19~q\ & ((!\inst38|inst43~q\) # (!\inst38|inst8~q\))))) # (!\inst39|inst42~q\ & ((\inst39|inst19~q\ & (!\inst38|inst8~q\ & 
-- !\inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001010110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~138_combout\);

-- Location: LCCOMB_X10_Y26_N18
\inst42|seven_seg0[5]~284\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~284_combout\ = (\inst42|seven_seg0[5]~138_combout\ & (!\inst38|inst41~q\ & (\inst39|inst41~q\ & \inst38|inst43~q\))) # (!\inst42|seven_seg0[5]~138_combout\ & (\inst38|inst41~q\ & (!\inst39|inst41~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~138_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~284_combout\);

-- Location: LCCOMB_X10_Y26_N28
\inst42|seven_seg0[5]~285\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~285_combout\ = (\inst42|seven_seg0[5]~284_combout\) # ((\inst42|seven_seg0[5]~139_combout\ & (\inst38|inst43~q\ $ (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~139_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[5]~284_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~285_combout\);

-- Location: LCCOMB_X10_Y25_N16
\inst42|seven_seg0[5]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~140_combout\ = (\inst42|seven_seg0[5]~136_combout\) # ((!\inst39|inst8~q\ & (\inst38|inst42~q\ & \inst42|seven_seg0[5]~285_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~136_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~285_combout\,
	combout => \inst42|seven_seg0[5]~140_combout\);

-- Location: LCCOMB_X11_Y25_N6
\inst42|seven_seg0[5]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~123_combout\ = (\inst38|inst8~q\ & ((\inst38|inst41~q\ & (\inst39|inst19~q\ & !\inst39|inst41~q\)) # (!\inst38|inst41~q\ & (!\inst39|inst19~q\ & \inst39|inst41~q\)))) # (!\inst38|inst8~q\ & (!\inst39|inst41~q\ & ((\inst38|inst41~q\) 
-- # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~123_combout\);

-- Location: LCCOMB_X10_Y25_N18
\inst42|seven_seg0[5]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~124_combout\ = (\inst39|inst42~q\ & (\inst42|seven_seg0[5]~123_combout\ & (\inst38|inst43~q\ $ (!\inst42|seven_seg0[5]~111_combout\)))) # (!\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~111_combout\ & 
-- (\inst42|seven_seg0[5]~123_combout\ $ (!\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~123_combout\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~124_combout\);

-- Location: LCCOMB_X10_Y25_N30
\inst42|seven_seg0[5]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~128_combout\ = (\inst39|inst8~q\ & (((\inst38|inst42~q\) # (\inst42|seven_seg0[5]~124_combout\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[5]~127_combout\ & (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~127_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~124_combout\,
	combout => \inst42|seven_seg0[5]~128_combout\);

-- Location: LCCOMB_X19_Y25_N4
\inst42|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~3_combout\ = (\inst39|inst19~q\ & (\inst39|inst41~q\ & \inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|Equal0~3_combout\);

-- Location: LCCOMB_X12_Y22_N4
\inst42|seven_seg0[5]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~129_combout\ = (\inst42|Equal0~1_combout\ & (!\inst39|inst43~q\ & \inst42|Equal0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~1_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|Equal0~3_combout\,
	combout => \inst42|seven_seg0[5]~129_combout\);

-- Location: LCCOMB_X2_Y22_N22
\inst42|seven_seg0[5]~283\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~283_combout\ = (\inst38|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~129_combout\,
	combout => \inst42|seven_seg0[5]~283_combout\);

-- Location: LCCOMB_X2_Y22_N10
\inst42|seven_seg0[5]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~130_combout\ = (\inst39|inst42~q\ & (!\inst39|inst19~q\ & \inst38|inst8~q\)) # (!\inst39|inst42~q\ & (\inst39|inst19~q\ & !\inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~130_combout\);

-- Location: LCCOMB_X2_Y23_N24
\inst42|seven_seg0[5]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~131_combout\ = (\inst39|inst42~q\ & (((!\inst38|inst41~q\ & \inst38|inst8~q\)) # (!\inst39|inst19~q\))) # (!\inst39|inst42~q\ & (!\inst39|inst19~q\ & ((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~131_combout\);

-- Location: LCCOMB_X2_Y22_N24
\inst42|seven_seg0[5]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~132_combout\ = (\inst42|seven_seg0[5]~131_combout\ & (\inst42|seven_seg0[5]~130_combout\ $ (\inst38|inst43~q\ $ (!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[5]~131_combout\ & ((\inst42|seven_seg0[5]~130_combout\ & 
-- (!\inst38|inst43~q\ & \inst38|inst41~q\)) # (!\inst42|seven_seg0[5]~130_combout\ & (\inst38|inst43~q\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~130_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[5]~131_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~132_combout\);

-- Location: LCCOMB_X2_Y22_N2
\inst42|seven_seg0[5]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~133_combout\ = (\inst42|seven_seg0[5]~131_combout\ & (\inst42|seven_seg0[5]~130_combout\ & (\inst38|inst43~q\))) # (!\inst42|seven_seg0[5]~131_combout\ & (\inst38|inst41~q\ & ((\inst42|seven_seg0[5]~130_combout\) # 
-- (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~130_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[5]~131_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~133_combout\);

-- Location: LCCOMB_X2_Y22_N16
\inst42|seven_seg0[5]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~134_combout\ = (\inst42|seven_seg0[5]~132_combout\ & (\inst39|inst41~q\ $ (((\inst42|seven_seg0[5]~133_combout\))))) # (!\inst42|seven_seg0[5]~132_combout\ & (\inst39|inst41~q\ & (\inst42|seven_seg0[5]~283_combout\ & 
-- \inst42|seven_seg0[5]~133_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[5]~283_combout\,
	datac => \inst42|seven_seg0[5]~132_combout\,
	datad => \inst42|seven_seg0[5]~133_combout\,
	combout => \inst42|seven_seg0[5]~134_combout\);

-- Location: LCCOMB_X10_Y25_N28
\inst42|seven_seg0[5]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~135_combout\ = (\inst42|seven_seg0[5]~128_combout\ & (((\inst42|seven_seg0[5]~134_combout\) # (!\inst38|inst42~q\)))) # (!\inst42|seven_seg0[5]~128_combout\ & (\inst42|seven_seg0[5]~122_combout\ & (\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~128_combout\,
	datab => \inst42|seven_seg0[5]~122_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~134_combout\,
	combout => \inst42|seven_seg0[5]~135_combout\);

-- Location: LCCOMB_X10_Y25_N6
\inst42|seven_seg0[5]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~141_combout\ = (\inst38|inst19~q\ & (((\inst39|inst43~q\) # (\inst42|seven_seg0[5]~135_combout\)))) # (!\inst38|inst19~q\ & (\inst42|seven_seg0[5]~140_combout\ & (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[5]~140_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[5]~135_combout\,
	combout => \inst42|seven_seg0[5]~141_combout\);

-- Location: LCCOMB_X10_Y26_N24
\inst42|seven_seg0[5]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~147_combout\ = (\inst39|inst42~q\ & ((\inst39|inst19~q\ & (\inst38|inst8~q\ $ (\inst38|inst43~q\))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & !\inst38|inst43~q\)))) # (!\inst39|inst42~q\ & (\inst39|inst19~q\ $ (\inst38|inst8~q\ $ 
-- (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~147_combout\);

-- Location: LCCOMB_X10_Y26_N2
\inst42|seven_seg0[5]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~146_combout\ = (\inst39|inst42~q\ & ((\inst39|inst19~q\ & (\inst38|inst8~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ (\inst38|inst43~q\))))) # (!\inst39|inst42~q\ & ((\inst39|inst19~q\ & (!\inst38|inst8~q\ & 
-- !\inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ & \inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~146_combout\);

-- Location: LCCOMB_X10_Y26_N26
\inst42|seven_seg0[5]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~148_combout\ = (\inst39|inst41~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg0[5]~146_combout\)))) # (!\inst39|inst41~q\ & (!\inst42|seven_seg0[5]~147_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[5]~147_combout\,
	datac => \inst42|seven_seg0[5]~146_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~148_combout\);

-- Location: LCCOMB_X10_Y26_N0
\inst42|seven_seg0[5]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~145_combout\ = \inst39|inst42~q\ $ (\inst39|inst19~q\ $ (\inst38|inst8~q\ $ (\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~145_combout\);

-- Location: LCCOMB_X10_Y26_N22
\inst42|seven_seg0[5]~298\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~298_combout\ = (\inst38|inst8~q\ & ((\inst39|inst19~q\ & (!\inst42|seven_seg0[5]~129_combout\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & ((!\inst38|inst43~q\))))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\ $ (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~129_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~298_combout\);

-- Location: LCCOMB_X10_Y26_N20
\inst42|seven_seg0[5]~299\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~299_combout\ = (\inst42|seven_seg0[5]~298_combout\ & (((\inst38|inst43~q\) # (!\inst39|inst19~q\)) # (!\inst39|inst42~q\))) # (!\inst42|seven_seg0[5]~298_combout\ & (\inst39|inst42~q\ $ (((!\inst39|inst19~q\ & \inst38|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg0[5]~298_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~299_combout\);

-- Location: LCCOMB_X10_Y26_N8
\inst42|seven_seg0[5]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~149_combout\ = (\inst42|seven_seg0[5]~148_combout\ & (((\inst42|seven_seg0[5]~299_combout\) # (!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[5]~148_combout\ & (\inst42|seven_seg0[5]~145_combout\ & ((\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~148_combout\,
	datab => \inst42|seven_seg0[5]~145_combout\,
	datac => \inst42|seven_seg0[5]~299_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[5]~149_combout\);

-- Location: LCCOMB_X11_Y25_N16
\inst42|seven_seg0[5]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~142_combout\ = (\inst39|inst41~q\ & (\inst38|inst8~q\ $ (\inst38|inst41~q\ $ (!\inst39|inst19~q\)))) # (!\inst39|inst41~q\ & ((\inst38|inst8~q\ & ((\inst38|inst41~q\) # (\inst39|inst19~q\))) # (!\inst38|inst8~q\ & 
-- ((!\inst39|inst19~q\) # (!\inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~142_combout\);

-- Location: LCCOMB_X11_Y25_N30
\inst42|seven_seg0[5]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~143_combout\ = (\inst42|seven_seg0[5]~123_combout\ & ((\inst38|inst43~q\ & ((!\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~142_combout\))) # (!\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~142_combout\) # (\inst39|inst42~q\))))) # 
-- (!\inst42|seven_seg0[5]~123_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[5]~142_combout\ $ (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~142_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~123_combout\,
	combout => \inst42|seven_seg0[5]~143_combout\);

-- Location: LCCOMB_X11_Y25_N12
\inst42|seven_seg0[5]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~144_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~143_combout\) # ((\inst38|inst42~q\)))) # (!\inst39|inst8~q\ & (((!\inst38|inst42~q\ & \inst42|seven_seg0[5]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~143_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~119_combout\,
	combout => \inst42|seven_seg0[5]~144_combout\);

-- Location: LCCOMB_X11_Y25_N10
\inst42|seven_seg0[5]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~150_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg0[5]~144_combout\ & (\inst42|seven_seg0[5]~149_combout\)) # (!\inst42|seven_seg0[5]~144_combout\ & ((\inst42|seven_seg0[5]~113_combout\))))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg0[5]~144_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~149_combout\,
	datab => \inst42|seven_seg0[5]~113_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[5]~144_combout\,
	combout => \inst42|seven_seg0[5]~150_combout\);

-- Location: LCCOMB_X11_Y25_N24
\inst42|seven_seg0[5]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~151_combout\ = (\inst39|inst43~q\ & ((\inst42|seven_seg0[5]~141_combout\ & ((\inst42|seven_seg0[5]~150_combout\))) # (!\inst42|seven_seg0[5]~141_combout\ & (\inst42|seven_seg0[5]~120_combout\)))) # (!\inst39|inst43~q\ & 
-- (((\inst42|seven_seg0[5]~141_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|seven_seg0[5]~120_combout\,
	datac => \inst42|seven_seg0[5]~141_combout\,
	datad => \inst42|seven_seg0[5]~150_combout\,
	combout => \inst42|seven_seg0[5]~151_combout\);

-- Location: LCCOMB_X14_Y21_N28
\inst42|seven_seg0[4]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~154_combout\ = (\inst39|inst41~q\) # (\inst39|inst42~q\ $ (\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111011011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~154_combout\);

-- Location: LCCOMB_X14_Y21_N10
\inst42|seven_seg0[4]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~153_combout\ = (\inst39|inst42~q\) # ((\inst39|inst41~q\) # (!\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~153_combout\);

-- Location: LCCOMB_X14_Y21_N22
\inst42|seven_seg0[4]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~158_combout\ = (\inst39|inst42~q\ & (\inst39|inst41~q\ & !\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~158_combout\);

-- Location: LCCOMB_X14_Y21_N4
\inst42|seven_seg0[4]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~152_combout\ = (\inst39|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~152_combout\);

-- Location: LCCOMB_X12_Y21_N24
\inst42|seven_seg0[4]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~162_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg0[4]~152_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[4]~158_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~162_combout\);

-- Location: LCCOMB_X12_Y21_N26
\inst42|seven_seg0[4]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~163_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~162_combout\ & ((!\inst42|seven_seg0[4]~153_combout\))) # (!\inst42|seven_seg0[4]~162_combout\ & (!\inst42|seven_seg0[4]~154_combout\)))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~162_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~154_combout\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~162_combout\,
	combout => \inst42|seven_seg0[4]~163_combout\);

-- Location: LCCOMB_X14_Y21_N20
\inst42|seven_seg0[4]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~156_combout\ = (\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & \inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~156_combout\);

-- Location: LCCOMB_X12_Y21_N30
\inst42|seven_seg0[4]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~159_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\) # (!\inst42|seven_seg0[4]~154_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[4]~156_combout\ & ((!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~156_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[4]~154_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[4]~159_combout\);

-- Location: LCCOMB_X12_Y21_N16
\inst42|seven_seg0[4]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~160_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~159_combout\ & ((\inst42|seven_seg0[4]~152_combout\))) # (!\inst42|seven_seg0[4]~159_combout\ & (\inst42|seven_seg0[4]~158_combout\)))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~159_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[4]~159_combout\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~160_combout\);

-- Location: LCCOMB_X14_Y21_N18
\inst42|seven_seg0[4]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~155_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & (!\inst42|seven_seg0[4]~153_combout\)) # (!\inst38|inst43~q\ & ((!\inst42|seven_seg0[4]~154_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~154_combout\,
	combout => \inst42|seven_seg0[4]~155_combout\);

-- Location: LCCOMB_X12_Y21_N12
\inst42|seven_seg0[4]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~157_combout\ = (\inst42|seven_seg0[4]~155_combout\ & ((\inst42|seven_seg0[4]~156_combout\) # ((!\inst39|inst8~q\)))) # (!\inst42|seven_seg0[4]~155_combout\ & (((\inst39|inst8~q\ & \inst42|seven_seg0[4]~152_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~156_combout\,
	datab => \inst42|seven_seg0[4]~155_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~157_combout\);

-- Location: LCCOMB_X12_Y21_N2
\inst42|seven_seg0[4]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~161_combout\ = (\inst38|inst19~q\ & (((\inst42|seven_seg0[4]~157_combout\ & !\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & (\inst42|seven_seg0[4]~160_combout\ & ((\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[4]~160_combout\,
	datac => \inst42|seven_seg0[4]~157_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[4]~161_combout\);

-- Location: LCCOMB_X12_Y21_N8
\inst42|seven_seg0[4]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~164_combout\ = (\inst42|seven_seg0[4]~161_combout\) # ((\inst42|seven_seg0[4]~163_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[4]~161_combout\,
	combout => \inst42|seven_seg0[4]~164_combout\);

-- Location: LCCOMB_X12_Y21_N20
\inst42|seven_seg0[4]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~170_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[4]~156_combout\)) # (!\inst38|inst43~q\ & ((\inst42|seven_seg0[4]~152_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~156_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~170_combout\);

-- Location: LCCOMB_X12_Y21_N14
\inst42|seven_seg0[4]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~171_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~170_combout\ & (\inst42|seven_seg0[4]~158_combout\)) # (!\inst42|seven_seg0[4]~170_combout\ & ((!\inst42|seven_seg0[4]~153_combout\))))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~170_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~170_combout\,
	combout => \inst42|seven_seg0[4]~171_combout\);

-- Location: LCCOMB_X12_Y21_N10
\inst42|seven_seg0[4]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~165_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[4]~158_combout\) # ((\inst39|inst8~q\)))) # (!\inst38|inst43~q\ & (((!\inst42|seven_seg0[4]~153_combout\ & !\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[4]~153_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[4]~165_combout\);

-- Location: LCCOMB_X12_Y21_N28
\inst42|seven_seg0[4]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~166_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~165_combout\ & ((!\inst42|seven_seg0[4]~154_combout\))) # (!\inst42|seven_seg0[4]~165_combout\ & (\inst42|seven_seg0[4]~156_combout\)))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~165_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~156_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[4]~154_combout\,
	datad => \inst42|seven_seg0[4]~165_combout\,
	combout => \inst42|seven_seg0[4]~166_combout\);

-- Location: LCCOMB_X12_Y21_N22
\inst42|seven_seg0[4]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~169_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg0[4]~166_combout\ & ((!\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & (((\inst42|seven_seg0[4]~157_combout\ & \inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[4]~166_combout\,
	datac => \inst42|seven_seg0[4]~157_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[4]~169_combout\);

-- Location: LCCOMB_X12_Y21_N0
\inst42|seven_seg0[4]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~172_combout\ = (\inst42|seven_seg0[4]~169_combout\) # ((\inst42|seven_seg0[4]~171_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[4]~171_combout\,
	datac => \inst42|seven_seg0[4]~169_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[4]~172_combout\);

-- Location: LCCOMB_X12_Y21_N18
\inst42|seven_seg0[4]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~167_combout\ = (\inst38|inst19~q\ & (!\inst38|inst42~q\ & (\inst42|seven_seg0[4]~163_combout\))) # (!\inst38|inst19~q\ & (\inst38|inst42~q\ & ((\inst42|seven_seg0[4]~166_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[4]~166_combout\,
	combout => \inst42|seven_seg0[4]~167_combout\);

-- Location: LCCOMB_X12_Y21_N4
\inst42|seven_seg0[4]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~168_combout\ = (\inst42|seven_seg0[4]~167_combout\) # ((\inst42|seven_seg0[4]~160_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[4]~160_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[4]~167_combout\,
	combout => \inst42|seven_seg0[4]~168_combout\);

-- Location: LCCOMB_X12_Y22_N30
\inst42|seven_seg0[4]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~173_combout\ = (\inst38|inst8~q\ & (((\inst38|inst41~q\) # (\inst42|seven_seg0[4]~168_combout\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[4]~172_combout\ & (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg0[4]~172_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[4]~168_combout\,
	combout => \inst42|seven_seg0[4]~173_combout\);

-- Location: LCCOMB_X12_Y22_N12
\inst42|seven_seg0[4]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~174_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg0[4]~173_combout\ & ((\inst42|seven_seg0[4]~172_combout\))) # (!\inst42|seven_seg0[4]~173_combout\ & (\inst42|seven_seg0[4]~164_combout\)))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg0[4]~173_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~164_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg0[4]~173_combout\,
	datad => \inst42|seven_seg0[4]~172_combout\,
	combout => \inst42|seven_seg0[4]~174_combout\);

-- Location: LCCOMB_X16_Y25_N28
\inst42|seven_seg1[5]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~40_combout\ = (\inst39|inst19~q\ & (\inst39|inst41~q\ & \inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~40_combout\);

-- Location: LCCOMB_X12_Y22_N6
\inst42|seven_seg1[3]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~41_combout\ = (\inst38|inst8~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~41_combout\);

-- Location: LCCOMB_X12_Y22_N28
\inst42|seven_seg0[4]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~175_combout\ = (\inst42|seven_seg1[5]~40_combout\ & (\inst42|seven_seg1[3]~39_combout\ & (\inst39|inst8~q\ & \inst42|seven_seg1[3]~41_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~40_combout\,
	datab => \inst42|seven_seg1[3]~39_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~41_combout\,
	combout => \inst42|seven_seg0[4]~175_combout\);

-- Location: LCCOMB_X12_Y22_N10
\inst42|Equal0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~4_combout\ = (\inst42|Equal0~1_combout\ & (\inst39|inst42~q\ & (!\inst39|inst43~q\ & \inst42|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~1_combout\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst43~q\,
	datad => \inst42|Equal0~3_combout\,
	combout => \inst42|Equal0~4_combout\);

-- Location: LCCOMB_X12_Y22_N20
\inst42|seven_seg0[4]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~176_combout\ = (\inst42|seven_seg0[4]~175_combout\ & (((!\inst42|Equal0~4_combout\)))) # (!\inst42|seven_seg0[4]~175_combout\ & ((\inst42|seven_seg0[4]~174_combout\) # ((\inst39|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~174_combout\,
	datab => \inst42|seven_seg0[4]~175_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|Equal0~4_combout\,
	combout => \inst42|seven_seg0[4]~176_combout\);

-- Location: LCCOMB_X4_Y22_N18
\inst42|seven_seg0[6]~286\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~286_combout\ = (\inst38|inst19~q\ & (((\inst42|seven_seg0[6]~90_combout\ & !\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & (\inst42|seven_seg0[6]~86_combout\ & ((\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~86_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~90_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~286_combout\);

-- Location: LCCOMB_X4_Y22_N6
\inst42|seven_seg0[6]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~177_combout\ = (!\inst38|inst8~q\ & ((\inst42|seven_seg0[6]~286_combout\) # ((\inst42|seven_seg0[6]~95_combout\ & !\inst42|seven_seg0[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg0[6]~286_combout\,
	datac => \inst42|seven_seg0[6]~95_combout\,
	datad => \inst42|seven_seg0[6]~66_combout\,
	combout => \inst42|seven_seg0[6]~177_combout\);

-- Location: LCCOMB_X4_Y23_N12
\inst42|seven_seg0[6]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~183_combout\ = (\inst38|inst42~q\ & (\inst38|inst19~q\ $ (\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~183_combout\);

-- Location: LCCOMB_X4_Y23_N14
\inst42|Equal0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~5_combout\ = (!\inst38|inst42~q\ & \inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|Equal0~5_combout\);

-- Location: LCCOMB_X4_Y23_N10
\inst42|seven_seg0[6]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~184_combout\ = (\inst42|seven_seg0[6]~183_combout\ & ((\inst42|seven_seg0[6]~81_combout\) # ((\inst42|seven_seg0[6]~95_combout\ & \inst42|Equal0~5_combout\)))) # (!\inst42|seven_seg0[6]~183_combout\ & 
-- (((\inst42|seven_seg0[6]~95_combout\ & \inst42|Equal0~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~183_combout\,
	datab => \inst42|seven_seg0[6]~81_combout\,
	datac => \inst42|seven_seg0[6]~95_combout\,
	datad => \inst42|Equal0~5_combout\,
	combout => \inst42|seven_seg0[6]~184_combout\);

-- Location: LCCOMB_X4_Y23_N16
\inst42|seven_seg0[6]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~97_combout\ = \inst38|inst19~q\ $ (\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~97_combout\);

-- Location: LCCOMB_X4_Y23_N0
\inst42|seven_seg0[6]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~185_combout\ = (\inst38|inst42~q\ & (!\inst42|seven_seg0[6]~97_combout\ & ((\inst42|seven_seg0[6]~82_combout\)))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg0[6]~86_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[6]~97_combout\,
	datac => \inst42|seven_seg0[6]~86_combout\,
	datad => \inst42|seven_seg0[6]~82_combout\,
	combout => \inst42|seven_seg0[6]~185_combout\);

-- Location: LCCOMB_X12_Y22_N14
\inst42|seven_seg0[6]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~179_combout\ = (\inst42|seven_seg1[3]~41_combout\ & (\inst42|seven_seg1[3]~39_combout\ & (\inst39|inst43~q\ & \inst42|seven_seg0[6]~106_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~41_combout\,
	datab => \inst42|seven_seg1[3]~39_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[6]~106_combout\,
	combout => \inst42|seven_seg0[6]~179_combout\);

-- Location: LCCOMB_X8_Y22_N14
\inst42|seven_seg0[6]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~180_combout\ = (\inst42|seven_seg0[6]~179_combout\) # (\inst38|inst41~q\ $ (\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~179_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~180_combout\);

-- Location: LCCOMB_X8_Y22_N4
\inst42|seven_seg0[6]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~178_combout\ = (\inst38|inst42~q\ & (\inst39|inst41~q\ & (\inst39|inst8~q\ & \inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~178_combout\);

-- Location: LCCOMB_X8_Y22_N16
\inst42|seven_seg0[6]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~181_combout\ = (!\inst39|inst19~q\ & !\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datac => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~181_combout\);

-- Location: LCCOMB_X8_Y22_N2
\inst42|seven_seg0[6]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~182_combout\ = (\inst42|seven_seg0[6]~178_combout\ & ((\inst39|inst42~q\ & (\inst42|seven_seg0[6]~180_combout\)) # (!\inst39|inst42~q\ & ((\inst42|seven_seg0[6]~181_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg0[6]~180_combout\,
	datac => \inst42|seven_seg0[6]~178_combout\,
	datad => \inst42|seven_seg0[6]~181_combout\,
	combout => \inst42|seven_seg0[6]~182_combout\);

-- Location: LCCOMB_X4_Y23_N18
\inst42|seven_seg0[6]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~186_combout\ = (\inst42|seven_seg0[6]~184_combout\) # ((\inst42|seven_seg0[6]~182_combout\) # ((\inst42|seven_seg0[6]~185_combout\ & !\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~184_combout\,
	datab => \inst42|seven_seg0[6]~185_combout\,
	datac => \inst42|seven_seg0[6]~182_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~186_combout\);

-- Location: LCCOMB_X5_Y22_N16
\inst42|seven_seg0[3]~287\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~287_combout\ = (\inst38|inst8~q\ & (\inst38|inst43~q\ & ((\inst42|seven_seg0[6]~177_combout\) # (\inst42|seven_seg0[6]~186_combout\)))) # (!\inst38|inst8~q\ & (!\inst38|inst43~q\ & (\inst42|seven_seg0[6]~177_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[6]~177_combout\,
	datad => \inst42|seven_seg0[6]~186_combout\,
	combout => \inst42|seven_seg0[3]~287_combout\);

-- Location: LCCOMB_X4_Y22_N4
\inst42|seven_seg0[3]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~188_combout\ = (\inst38|inst42~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg0[6]~92_combout\))) # (!\inst38|inst19~q\ & (\inst42|seven_seg0[6]~90_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~90_combout\,
	datad => \inst42|seven_seg0[6]~92_combout\,
	combout => \inst42|seven_seg0[3]~188_combout\);

-- Location: LCCOMB_X6_Y22_N8
\inst42|seven_seg0[6]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~187_combout\ = \inst38|inst43~q\ $ (\inst38|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~187_combout\);

-- Location: LCCOMB_X6_Y22_N14
\inst42|seven_seg0[3]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~189_combout\ = (\inst42|seven_seg0[6]~187_combout\ & ((\inst42|seven_seg0[3]~188_combout\) # ((\inst42|seven_seg0[6]~279_combout\ & !\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~279_combout\,
	datab => \inst42|seven_seg0[3]~188_combout\,
	datac => \inst42|seven_seg0[6]~187_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[3]~189_combout\);

-- Location: LCCOMB_X8_Y22_N12
\inst42|seven_seg0[0]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~192_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[0]~192_combout\);

-- Location: LCCOMB_X8_Y22_N10
\inst42|seven_seg0[3]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~193_combout\ = (\inst42|seven_seg0[6]~71_combout\ & (((!\inst42|seven_seg0[6]~68_combout\ & !\inst42|seven_seg0[0]~192_combout\)))) # (!\inst42|seven_seg0[6]~71_combout\ & ((\inst42|seven_seg0[6]~77_combout\) # 
-- ((\inst42|seven_seg0[0]~192_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~77_combout\,
	datab => \inst42|seven_seg0[6]~68_combout\,
	datac => \inst42|seven_seg0[6]~71_combout\,
	datad => \inst42|seven_seg0[0]~192_combout\,
	combout => \inst42|seven_seg0[3]~193_combout\);

-- Location: LCCOMB_X8_Y22_N0
\inst42|seven_seg0[3]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~194_combout\ = (\inst42|seven_seg0[0]~192_combout\ & ((\inst42|seven_seg0[3]~193_combout\ & (\inst42|seven_seg0[6]~107_combout\)) # (!\inst42|seven_seg0[3]~193_combout\ & ((\inst42|seven_seg0[6]~180_combout\))))) # 
-- (!\inst42|seven_seg0[0]~192_combout\ & (((\inst42|seven_seg0[3]~193_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~192_combout\,
	datab => \inst42|seven_seg0[6]~107_combout\,
	datac => \inst42|seven_seg0[6]~180_combout\,
	datad => \inst42|seven_seg0[3]~193_combout\,
	combout => \inst42|seven_seg0[3]~194_combout\);

-- Location: LCCOMB_X3_Y23_N20
\inst42|seven_seg0[0]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~190_combout\ = (\inst38|inst43~q\ & ((\inst38|inst19~q\) # (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst19~q\,
	datac => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[0]~190_combout\);

-- Location: LCCOMB_X6_Y22_N16
\inst42|seven_seg0[3]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~191_combout\ = (\inst42|seven_seg0[0]~98_combout\ & ((\inst42|seven_seg0[6]~79_combout\) # ((\inst42|seven_seg0[0]~190_combout\)))) # (!\inst42|seven_seg0[0]~98_combout\ & (((!\inst42|seven_seg0[0]~190_combout\ & 
-- \inst42|seven_seg0[6]~275_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~79_combout\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst42|seven_seg0[0]~190_combout\,
	datad => \inst42|seven_seg0[6]~275_combout\,
	combout => \inst42|seven_seg0[3]~191_combout\);

-- Location: LCCOMB_X6_Y22_N22
\inst42|seven_seg0[3]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~195_combout\ = (\inst42|seven_seg0[0]~190_combout\ & ((\inst42|seven_seg0[3]~191_combout\ & (\inst42|seven_seg0[3]~194_combout\)) # (!\inst42|seven_seg0[3]~191_combout\ & ((\inst42|seven_seg0[6]~104_combout\))))) # 
-- (!\inst42|seven_seg0[0]~190_combout\ & (((\inst42|seven_seg0[3]~191_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[3]~194_combout\,
	datab => \inst42|seven_seg0[0]~190_combout\,
	datac => \inst42|seven_seg0[6]~104_combout\,
	datad => \inst42|seven_seg0[3]~191_combout\,
	combout => \inst42|seven_seg0[3]~195_combout\);

-- Location: LCCOMB_X6_Y22_N24
\inst42|seven_seg0[3]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~196_combout\ = (\inst39|inst43~q\ & ((\inst38|inst8~q\ & ((\inst42|seven_seg0[3]~195_combout\))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[6]~80_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|seven_seg0[6]~80_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[3]~195_combout\,
	combout => \inst42|seven_seg0[3]~196_combout\);

-- Location: LCCOMB_X6_Y22_N6
\inst42|seven_seg0[3]~288\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~288_combout\ = (\inst42|seven_seg0[3]~196_combout\) # ((!\inst39|inst43~q\ & ((\inst42|seven_seg0[3]~287_combout\) # (\inst42|seven_seg0[3]~189_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[3]~287_combout\,
	datab => \inst42|seven_seg0[3]~189_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[3]~196_combout\,
	combout => \inst42|seven_seg0[3]~288_combout\);

-- Location: LCCOMB_X14_Y21_N8
\inst42|seven_seg0[2]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~197_combout\ = (\inst39|inst42~q\ & ((\inst39|inst41~q\) # (\inst39|inst19~q\))) # (!\inst39|inst42~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110110111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[2]~197_combout\);

-- Location: LCCOMB_X14_Y21_N6
\inst42|seven_seg0[2]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~198_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)) # (!\inst42|seven_seg0[4]~153_combout\))) # (!\inst39|inst8~q\ & (((!\inst38|inst43~q\ & \inst42|seven_seg0[4]~152_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010011110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[2]~198_combout\);

-- Location: LCCOMB_X14_Y21_N0
\inst42|seven_seg0[2]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~199_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[2]~198_combout\ & (\inst42|seven_seg0[4]~158_combout\)) # (!\inst42|seven_seg0[2]~198_combout\ & ((!\inst42|seven_seg0[2]~197_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[2]~198_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst42|seven_seg0[2]~197_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[2]~198_combout\,
	combout => \inst42|seven_seg0[2]~199_combout\);

-- Location: LCCOMB_X12_Y21_N6
\inst42|seven_seg0[2]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~201_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~165_combout\ & (!\inst42|seven_seg0[4]~154_combout\)) # (!\inst42|seven_seg0[4]~165_combout\ & ((!\inst42|seven_seg0[2]~197_combout\))))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~165_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~154_combout\,
	datab => \inst42|seven_seg0[2]~197_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~165_combout\,
	combout => \inst42|seven_seg0[2]~201_combout\);

-- Location: LCCOMB_X14_Y21_N2
\inst42|seven_seg0[2]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~200_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~155_combout\ & (!\inst42|seven_seg0[2]~197_combout\)) # (!\inst42|seven_seg0[4]~155_combout\ & ((\inst42|seven_seg0[4]~152_combout\))))) # (!\inst39|inst8~q\ & 
-- (\inst42|seven_seg0[4]~155_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110111001001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[4]~155_combout\,
	datac => \inst42|seven_seg0[2]~197_combout\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[2]~200_combout\);

-- Location: LCCOMB_X14_Y22_N22
\inst42|seven_seg0[2]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~202_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~200_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[2]~201_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~201_combout\,
	datab => \inst42|seven_seg0[2]~200_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~202_combout\);

-- Location: LCCOMB_X14_Y22_N4
\inst42|seven_seg0[2]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~203_combout\ = (\inst38|inst19~q\ & ((\inst38|inst42~q\ & (\inst42|seven_seg0[2]~199_combout\)) # (!\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~202_combout\))))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & 
-- ((\inst42|seven_seg0[2]~202_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[2]~199_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~199_combout\,
	datac => \inst42|seven_seg0[2]~202_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~203_combout\);

-- Location: LCCOMB_X14_Y21_N14
\inst42|seven_seg0[2]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~208_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[4]~158_combout\)) # (!\inst39|inst8~q\ & ((!\inst42|seven_seg0[2]~197_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst42|seven_seg0[2]~197_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[2]~208_combout\);

-- Location: LCCOMB_X14_Y21_N12
\inst42|seven_seg0[6]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~207_combout\ = \inst39|inst19~q\ $ (\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~207_combout\);

-- Location: LCCOMB_X14_Y21_N24
\inst42|seven_seg0[2]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~209_combout\ = (\inst38|inst43~q\ & (!\inst42|seven_seg0[6]~207_combout\ & (\inst39|inst41~q\ $ (!\inst42|seven_seg0[2]~208_combout\)))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[2]~208_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[2]~208_combout\,
	datad => \inst42|seven_seg0[6]~207_combout\,
	combout => \inst42|seven_seg0[2]~209_combout\);

-- Location: LCCOMB_X14_Y22_N26
\inst42|seven_seg0[2]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~206_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg0[4]~163_combout\ & ((!\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & (((\inst42|seven_seg0[2]~201_combout\ & \inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[4]~163_combout\,
	datac => \inst42|seven_seg0[2]~201_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~206_combout\);

-- Location: LCCOMB_X14_Y22_N12
\inst42|seven_seg0[2]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~210_combout\ = (\inst42|seven_seg0[2]~206_combout\) # ((\inst42|seven_seg0[2]~209_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~209_combout\,
	datac => \inst42|seven_seg0[2]~206_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~210_combout\);

-- Location: LCCOMB_X14_Y22_N14
\inst42|seven_seg0[2]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~204_combout\ = (\inst38|inst42~q\ & (!\inst38|inst19~q\ & (\inst42|seven_seg0[4]~163_combout\))) # (!\inst38|inst42~q\ & (\inst38|inst19~q\ & ((\inst42|seven_seg0[2]~199_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[2]~199_combout\,
	combout => \inst42|seven_seg0[2]~204_combout\);

-- Location: LCCOMB_X14_Y22_N8
\inst42|seven_seg0[2]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~205_combout\ = (\inst42|seven_seg0[2]~204_combout\) # ((\inst42|seven_seg0[2]~200_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~200_combout\,
	datac => \inst42|seven_seg0[2]~204_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~205_combout\);

-- Location: LCCOMB_X14_Y22_N18
\inst42|seven_seg0[2]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~211_combout\ = (\inst38|inst41~q\ & (((\inst38|inst8~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst8~q\ & ((\inst42|seven_seg0[2]~205_combout\))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[2]~210_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~210_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg0[2]~205_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[2]~211_combout\);

-- Location: LCCOMB_X14_Y21_N30
\inst42|seven_seg0[2]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~213_combout\ = (\inst39|inst42~q\) # ((\inst39|inst19~q\) # (!\inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[2]~213_combout\);

-- Location: LCCOMB_X14_Y21_N16
\inst42|seven_seg0[2]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~214_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[2]~208_combout\ & (!\inst42|seven_seg0[2]~213_combout\)) # (!\inst42|seven_seg0[2]~208_combout\ & ((!\inst42|seven_seg0[4]~154_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[2]~208_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000001111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~213_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[2]~208_combout\,
	datad => \inst42|seven_seg0[4]~154_combout\,
	combout => \inst42|seven_seg0[2]~214_combout\);

-- Location: LCCOMB_X14_Y22_N28
\inst42|seven_seg0[2]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~212_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~201_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[2]~209_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~209_combout\,
	datac => \inst42|seven_seg0[2]~201_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[2]~212_combout\);

-- Location: LCCOMB_X14_Y22_N10
\inst42|seven_seg0[2]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~215_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg0[2]~212_combout\ & (\inst42|seven_seg0[2]~214_combout\)) # (!\inst42|seven_seg0[2]~212_combout\ & ((\inst42|seven_seg0[4]~163_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg0[2]~212_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~214_combout\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[2]~212_combout\,
	combout => \inst42|seven_seg0[2]~215_combout\);

-- Location: LCCOMB_X14_Y22_N20
\inst42|seven_seg0[2]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~216_combout\ = (\inst42|seven_seg0[2]~211_combout\ & (((\inst42|seven_seg0[2]~215_combout\) # (!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[2]~211_combout\ & (\inst42|seven_seg0[2]~203_combout\ & (\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~203_combout\,
	datab => \inst42|seven_seg0[2]~211_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[2]~215_combout\,
	combout => \inst42|seven_seg0[2]~216_combout\);

-- Location: LCCOMB_X12_Y22_N0
\inst42|seven_seg0[2]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~217_combout\ = (\inst42|seven_seg0[4]~175_combout\ & (((\inst42|Equal0~4_combout\)))) # (!\inst42|seven_seg0[4]~175_combout\ & (\inst42|seven_seg0[2]~216_combout\ & (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~216_combout\,
	datab => \inst42|seven_seg0[4]~175_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|Equal0~4_combout\,
	combout => \inst42|seven_seg0[2]~217_combout\);

-- Location: LCCOMB_X15_Y26_N22
\inst42|seven_seg0[1]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~245_combout\ = (\inst39|inst8~q\ & ((\inst39|inst43~q\ & (\inst39|inst19~q\ & !\inst38|inst43~q\)) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\) # (!\inst38|inst43~q\))))) # (!\inst39|inst8~q\ & (\inst39|inst43~q\ $ (\inst39|inst19~q\ 
-- $ (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100111010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~245_combout\);

-- Location: LCCOMB_X15_Y26_N0
\inst42|seven_seg0[1]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~246_combout\ = (\inst39|inst8~q\ & (\inst39|inst43~q\ $ (\inst39|inst19~q\ $ (\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst39|inst43~q\ & (!\inst39|inst19~q\ & \inst38|inst43~q\)) # (!\inst39|inst43~q\ & (\inst39|inst19~q\ & 
-- !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~246_combout\);

-- Location: LCCOMB_X15_Y26_N26
\inst42|seven_seg0[1]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~247_combout\ = (\inst39|inst42~q\ & (\inst42|seven_seg0[1]~245_combout\ & (\inst42|seven_seg0[6]~67_combout\ & !\inst42|seven_seg0[1]~246_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[1]~246_combout\ & 
-- (\inst42|seven_seg0[1]~245_combout\ $ (\inst42|seven_seg0[6]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~245_combout\,
	datab => \inst42|seven_seg0[6]~67_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[1]~246_combout\,
	combout => \inst42|seven_seg0[1]~247_combout\);

-- Location: LCCOMB_X15_Y26_N2
\inst42|seven_seg0[1]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~229_combout\ = \inst39|inst43~q\ $ (\inst39|inst19~q\ $ (\inst39|inst42~q\ $ (\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~229_combout\);

-- Location: LCCOMB_X17_Y22_N26
\inst42|seven_seg0[1]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~232_combout\ = (\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst38|inst43~q\))))) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\ & (!\inst39|inst42~q\ 
-- & !\inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~232_combout\);

-- Location: LCCOMB_X15_Y26_N10
\inst42|seven_seg0[1]~295\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~295_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[1]~232_combout\ & (\inst42|seven_seg0[1]~229_combout\ $ (\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (!\inst42|seven_seg0[1]~229_combout\ & (\inst39|inst41~q\ $ 
-- (\inst42|seven_seg0[1]~232_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[1]~229_combout\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg0[1]~232_combout\,
	combout => \inst42|seven_seg0[1]~295_combout\);

-- Location: LCCOMB_X17_Y22_N14
\inst42|seven_seg0[1]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~249_combout\ = (\inst39|inst43~q\ & (((\inst39|inst42~q\) # (\inst38|inst43~q\)) # (!\inst39|inst19~q\))) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ $ (!\inst38|inst43~q\))) # (!\inst39|inst19~q\ & 
-- ((\inst39|inst42~q\) # (\inst38|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~249_combout\);

-- Location: LCCOMB_X17_Y22_N28
\inst42|seven_seg0[1]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~248_combout\ = (\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst38|inst43~q\))) # (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst38|inst43~q\)))) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\ & 
-- (\inst39|inst42~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst38|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~248_combout\);

-- Location: LCCOMB_X17_Y22_N6
\inst42|seven_seg0[1]~292\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~292_combout\ = (\inst39|inst8~q\ & (!\inst42|seven_seg0[1]~249_combout\ & (!\inst39|inst41~q\))) # (!\inst39|inst8~q\ & (((\inst39|inst41~q\ & \inst42|seven_seg0[1]~248_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[1]~249_combout\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg0[1]~248_combout\,
	combout => \inst42|seven_seg0[1]~292_combout\);

-- Location: LCCOMB_X17_Y22_N24
\inst42|seven_seg0[5]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~250_combout\ = (!\inst39|inst19~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~250_combout\);

-- Location: LCCOMB_X17_Y22_N18
\inst42|Equal0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~7_combout\ = (\inst39|inst43~q\ & \inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|Equal0~7_combout\);

-- Location: LCCOMB_X17_Y22_N12
\inst42|seven_seg0[1]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~251_combout\ = (\inst42|seven_seg0[1]~292_combout\) # ((\inst42|seven_seg0[5]~250_combout\ & (!\inst42|seven_seg0[6]~67_combout\ & \inst42|Equal0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~292_combout\,
	datab => \inst42|seven_seg0[5]~250_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|Equal0~7_combout\,
	combout => \inst42|seven_seg0[1]~251_combout\);

-- Location: LCCOMB_X15_Y26_N16
\inst42|seven_seg0[1]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~252_combout\ = (\inst38|inst41~q\ & (((\inst38|inst8~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg0[1]~295_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg0[1]~251_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~295_combout\,
	datab => \inst42|seven_seg0[1]~251_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[1]~252_combout\);

-- Location: LCCOMB_X16_Y26_N8
\inst42|seven_seg0[1]~256\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~256_combout\ = (\inst39|inst41~q\ & (\inst39|inst43~q\ & !\inst39|inst19~q\)) # (!\inst39|inst41~q\ & (!\inst39|inst43~q\ & \inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~256_combout\);

-- Location: LCCOMB_X16_Y26_N26
\inst42|Equal0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~8_combout\ = (\inst38|inst43~q\ & (\inst42|seven_seg1[3]~39_combout\ & (\inst39|inst8~q\ & \inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[3]~39_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|Equal0~8_combout\);

-- Location: LCCOMB_X16_Y26_N10
\inst42|seven_seg0[1]~257\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~257_combout\ = (\inst42|seven_seg0[1]~256_combout\ & ((\inst39|inst41~q\ & ((\inst39|inst42~q\))) # (!\inst39|inst41~q\ & (\inst42|Equal0~8_combout\ & !\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[1]~256_combout\,
	datac => \inst42|Equal0~8_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~257_combout\);

-- Location: LCCOMB_X16_Y26_N16
\inst42|seven_seg0[1]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~253_combout\ = (\inst39|inst42~q\ & ((\inst39|inst43~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst43~q\ & (\inst39|inst41~q\ & \inst39|inst19~q\)))) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & (\inst39|inst43~q\ $ 
-- (!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~253_combout\);

-- Location: LCCOMB_X16_Y26_N18
\inst42|seven_seg0[1]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~254_combout\ = (\inst39|inst42~q\ & ((\inst39|inst43~q\ & ((\inst39|inst19~q\) # (!\inst39|inst41~q\))) # (!\inst39|inst43~q\ & ((!\inst39|inst19~q\))))) # (!\inst39|inst42~q\ & ((\inst39|inst43~q\ & ((!\inst39|inst19~q\))) # 
-- (!\inst39|inst43~q\ & (!\inst39|inst41~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~254_combout\);

-- Location: LCCOMB_X16_Y26_N12
\inst42|seven_seg0[1]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~255_combout\ = (\inst39|inst8~q\ & ((\inst38|inst43~q\) # ((!\inst39|inst41~q\ & !\inst42|seven_seg0[1]~254_combout\)))) # (!\inst39|inst8~q\ & (!\inst38|inst43~q\ & (\inst39|inst41~q\ & \inst42|seven_seg0[1]~254_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg0[1]~254_combout\,
	combout => \inst42|seven_seg0[1]~255_combout\);

-- Location: LCCOMB_X16_Y26_N4
\inst42|seven_seg0[1]~258\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~258_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[1]~255_combout\ & (\inst42|seven_seg0[1]~257_combout\)) # (!\inst42|seven_seg0[1]~255_combout\ & ((\inst42|seven_seg0[1]~253_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[1]~255_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~257_combout\,
	datab => \inst42|seven_seg0[1]~253_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[1]~255_combout\,
	combout => \inst42|seven_seg0[1]~258_combout\);

-- Location: LCCOMB_X15_Y26_N18
\inst42|seven_seg0[1]~259\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~259_combout\ = (\inst42|seven_seg0[1]~252_combout\ & (((\inst42|seven_seg0[1]~258_combout\) # (!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[1]~252_combout\ & (\inst42|seven_seg0[1]~247_combout\ & (\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~247_combout\,
	datab => \inst42|seven_seg0[1]~252_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[1]~258_combout\,
	combout => \inst42|seven_seg0[1]~259_combout\);

-- Location: LCCOMB_X10_Y26_N30
\inst42|seven_seg0[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~225_combout\ = (\inst39|inst42~q\ & ((\inst38|inst8~q\ & (\inst39|inst19~q\ & !\inst39|inst43~q\)) # (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (!\inst39|inst43~q\))))) # (!\inst39|inst42~q\ & ((\inst38|inst8~q\ & (\inst39|inst19~q\ $ 
-- (!\inst39|inst43~q\))) # (!\inst38|inst8~q\ & (!\inst39|inst19~q\ & \inst39|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000110000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~225_combout\);

-- Location: LCCOMB_X10_Y26_N16
\inst42|Equal0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~6_combout\ = (\inst39|inst42~q\ & (!\inst38|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|Equal0~6_combout\);

-- Location: LCCOMB_X10_Y26_N14
\inst42|seven_seg0[1]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~226_combout\ = (\inst42|seven_seg0[1]~225_combout\ & ((\inst38|inst43~q\) # ((!\inst39|inst19~q\ & \inst42|Equal0~6_combout\)))) # (!\inst42|seven_seg0[1]~225_combout\ & (((!\inst39|inst19~q\ & \inst42|Equal0~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~225_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|Equal0~6_combout\,
	combout => \inst42|seven_seg0[1]~226_combout\);

-- Location: LCCOMB_X11_Y26_N20
\inst42|seven_seg0[5]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~137_combout\ = \inst38|inst41~q\ $ (\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~137_combout\);

-- Location: LCCOMB_X12_Y22_N22
\inst42|seven_seg0[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~220_combout\ = (\inst39|inst19~q\ & (\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst43~q\ & (\inst38|inst8~q\ $ (\inst39|inst42~q\))) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ & 
-- \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~220_combout\);

-- Location: LCCOMB_X11_Y26_N14
\inst42|seven_seg0[1]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~227_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\ & (\inst42|seven_seg0[1]~226_combout\)) # (!\inst42|seven_seg0[5]~137_combout\ & ((\inst42|seven_seg0[1]~220_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~226_combout\,
	datab => \inst42|seven_seg0[5]~137_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~220_combout\,
	combout => \inst42|seven_seg0[1]~227_combout\);

-- Location: LCCOMB_X12_Y26_N26
\inst42|seven_seg0[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~223_combout\ = (\inst38|inst8~q\ & (\inst39|inst43~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~223_combout\);

-- Location: LCCOMB_X12_Y26_N30
\inst42|seven_seg0[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~221_combout\ = (!\inst39|inst43~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~221_combout\);

-- Location: LCCOMB_X12_Y26_N28
\inst42|seven_seg0[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~222_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg0[1]~220_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[1]~221_combout\ & (!\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~221_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[1]~220_combout\,
	combout => \inst42|seven_seg0[1]~222_combout\);

-- Location: LCCOMB_X12_Y26_N20
\inst42|seven_seg0[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~224_combout\ = (!\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\ & ((\inst42|seven_seg0[1]~222_combout\))) # (!\inst42|seven_seg0[5]~137_combout\ & (\inst42|seven_seg0[1]~223_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~223_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[5]~137_combout\,
	datad => \inst42|seven_seg0[1]~222_combout\,
	combout => \inst42|seven_seg0[1]~224_combout\);

-- Location: LCCOMB_X16_Y26_N24
\inst42|seven_seg0[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~219_combout\ = (\inst39|inst42~q\ & ((\inst38|inst8~q\ & (!\inst39|inst43~q\ & \inst39|inst19~q\)) # (!\inst38|inst8~q\ & (\inst39|inst43~q\ $ (!\inst39|inst19~q\))))) # (!\inst39|inst42~q\ & (\inst38|inst8~q\ $ (\inst39|inst43~q\ $ 
-- (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst43~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~219_combout\);

-- Location: LCCOMB_X12_Y26_N0
\inst42|seven_seg0[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~218_combout\ = (\inst38|inst8~q\ & ((\inst39|inst43~q\ & ((\inst39|inst42~q\) # (!\inst39|inst19~q\))) # (!\inst39|inst43~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst39|inst43~q\ & 
-- (\inst39|inst42~q\ & !\inst39|inst19~q\)) # (!\inst39|inst43~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000111101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~218_combout\);

-- Location: LCCOMB_X12_Y26_N24
\inst42|seven_seg0[1]~296\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~296_combout\ = (\inst38|inst41~q\ & (!\inst42|seven_seg0[1]~218_combout\ & (\inst42|seven_seg0[1]~219_combout\ $ (\inst38|inst43~q\)))) # (!\inst38|inst41~q\ & ((\inst42|seven_seg0[1]~219_combout\ & (\inst38|inst43~q\ & 
-- \inst42|seven_seg0[1]~218_combout\)) # (!\inst42|seven_seg0[1]~219_combout\ & (!\inst38|inst43~q\ & !\inst42|seven_seg0[1]~218_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[1]~219_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[1]~218_combout\,
	combout => \inst42|seven_seg0[1]~296_combout\);

-- Location: LCCOMB_X12_Y26_N10
\inst42|seven_seg0[1]~297\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~297_combout\ = (\inst42|seven_seg0[6]~67_combout\ & ((\inst42|seven_seg0[1]~227_combout\) # ((\inst42|seven_seg0[1]~224_combout\)))) # (!\inst42|seven_seg0[6]~67_combout\ & (((\inst42|seven_seg0[1]~296_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~227_combout\,
	datab => \inst42|seven_seg0[1]~224_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[1]~296_combout\,
	combout => \inst42|seven_seg0[1]~297_combout\);

-- Location: LCCOMB_X12_Y26_N2
\inst42|seven_seg0[1]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~240_combout\ = \inst38|inst8~q\ $ (\inst39|inst43~q\ $ (\inst39|inst42~q\ $ (\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~240_combout\);

-- Location: LCCOMB_X12_Y26_N16
\inst42|seven_seg0[1]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~241_combout\ = \inst42|seven_seg0[1]~218_combout\ $ (((\inst38|inst43~q\ & !\inst42|seven_seg0[1]~240_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg0[1]~218_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[1]~240_combout\,
	combout => \inst42|seven_seg0[1]~241_combout\);

-- Location: LCCOMB_X12_Y26_N6
\inst42|seven_seg0[1]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~242_combout\ = (!\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\ & (\inst42|seven_seg0[1]~241_combout\ & !\inst42|seven_seg0[1]~240_combout\)) # (!\inst42|seven_seg0[5]~137_combout\ & (!\inst42|seven_seg0[1]~241_combout\ & 
-- \inst42|seven_seg0[1]~240_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~137_combout\,
	datab => \inst42|seven_seg0[1]~241_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~240_combout\,
	combout => \inst42|seven_seg0[1]~242_combout\);

-- Location: LCCOMB_X12_Y26_N14
\inst42|seven_seg0[1]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~238_combout\ = (\inst38|inst8~q\ & ((\inst39|inst43~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\))) # (!\inst39|inst43~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst39|inst43~q\ & (\inst39|inst42~q\ 
-- & !\inst39|inst19~q\)) # (!\inst39|inst43~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000101101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~238_combout\);

-- Location: LCCOMB_X12_Y26_N12
\inst42|seven_seg0[1]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~239_combout\ = (\inst38|inst8~q\ & (\inst39|inst43~q\ & (\inst39|inst42~q\ & !\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (!\inst39|inst43~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~239_combout\);

-- Location: LCCOMB_X12_Y26_N22
\inst42|seven_seg0[1]~289\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~289_combout\ = (\inst38|inst41~q\ & (\inst42|seven_seg0[1]~239_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[1]~238_combout\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[1]~238_combout\ & 
-- !\inst42|seven_seg0[1]~239_combout\)) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~238_combout\ & \inst42|seven_seg0[1]~239_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[1]~238_combout\,
	datad => \inst42|seven_seg0[1]~239_combout\,
	combout => \inst42|seven_seg0[1]~289_combout\);

-- Location: LCCOMB_X12_Y26_N8
\inst42|seven_seg0[1]~290\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~290_combout\ = (\inst39|inst8~q\ & (((\inst42|seven_seg0[1]~219_combout\)))) # (!\inst39|inst8~q\ & (\inst38|inst41~q\ $ ((!\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100100001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~219_combout\,
	combout => \inst42|seven_seg0[1]~290_combout\);

-- Location: LCCOMB_X12_Y26_N18
\inst42|seven_seg0[1]~291\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~291_combout\ = (\inst38|inst41~q\ & (!\inst42|seven_seg0[1]~218_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[1]~290_combout\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[1]~290_combout\ & 
-- \inst42|seven_seg0[1]~218_combout\)) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~290_combout\ & !\inst42|seven_seg0[1]~218_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[1]~290_combout\,
	datad => \inst42|seven_seg0[1]~218_combout\,
	combout => \inst42|seven_seg0[1]~291_combout\);

-- Location: LCCOMB_X12_Y26_N4
\inst42|seven_seg0[1]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~243_combout\ = (\inst42|seven_seg0[6]~67_combout\ & ((\inst42|seven_seg0[1]~242_combout\) # ((\inst42|seven_seg0[1]~291_combout\)))) # (!\inst42|seven_seg0[6]~67_combout\ & (((\inst42|seven_seg0[1]~289_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~242_combout\,
	datab => \inst42|seven_seg0[6]~67_combout\,
	datac => \inst42|seven_seg0[1]~289_combout\,
	datad => \inst42|seven_seg0[1]~291_combout\,
	combout => \inst42|seven_seg0[1]~243_combout\);

-- Location: LCCOMB_X16_Y26_N28
\inst42|seven_seg0[1]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~235_combout\ = (\inst39|inst41~q\ & (\inst39|inst8~q\ $ (((\inst39|inst43~q\) # (!\inst39|inst19~q\))))) # (!\inst39|inst41~q\ & ((\inst39|inst8~q\) # ((!\inst39|inst43~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~235_combout\);

-- Location: LCCOMB_X16_Y26_N14
\inst42|seven_seg0[1]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~234_combout\ = (\inst39|inst43~q\ & (!\inst39|inst19~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\)))) # (!\inst39|inst43~q\ & (\inst39|inst19~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001111000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~234_combout\);

-- Location: LCCOMB_X16_Y26_N6
\inst42|seven_seg0[1]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~236_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[1]~235_combout\ & (\inst42|seven_seg0[1]~234_combout\ & \inst39|inst42~q\)) # (!\inst42|seven_seg0[1]~235_combout\ & (!\inst42|seven_seg0[1]~234_combout\ & 
-- !\inst39|inst42~q\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~235_combout\ & (\inst42|seven_seg0[1]~234_combout\ $ (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000100010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[1]~235_combout\,
	datac => \inst42|seven_seg0[1]~234_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~236_combout\);

-- Location: LCCOMB_X15_Y26_N4
\inst42|seven_seg0[1]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~228_combout\ = (\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\))))) # (!\inst39|inst43~q\ & (!\inst39|inst19~q\ & 
-- (\inst39|inst42~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~228_combout\);

-- Location: LCCOMB_X15_Y26_N24
\inst42|seven_seg0[1]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~230_combout\ = \inst42|seven_seg0[1]~229_combout\ $ (((\inst42|seven_seg0[1]~228_combout\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg0[1]~228_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~229_combout\,
	combout => \inst42|seven_seg0[1]~230_combout\);

-- Location: LCCOMB_X15_Y26_N30
\inst42|seven_seg0[1]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~231_combout\ = (!\inst38|inst41~q\ & ((\inst42|seven_seg0[1]~228_combout\ & (\inst42|seven_seg0[6]~67_combout\ & \inst42|seven_seg0[1]~230_combout\)) # (!\inst42|seven_seg0[1]~228_combout\ & (!\inst42|seven_seg0[6]~67_combout\ & 
-- !\inst42|seven_seg0[1]~230_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[1]~228_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[1]~230_combout\,
	combout => \inst42|seven_seg0[1]~231_combout\);

-- Location: LCCOMB_X15_Y26_N8
\inst42|seven_seg0[1]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~233_combout\ = (\inst38|inst41~q\ & (!\inst38|inst8~q\ & ((\inst42|seven_seg0[1]~295_combout\) # (\inst42|seven_seg0[1]~231_combout\)))) # (!\inst38|inst41~q\ & (((\inst42|seven_seg0[1]~231_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~295_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg0[1]~231_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[1]~233_combout\);

-- Location: LCCOMB_X15_Y26_N6
\inst42|seven_seg0[1]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~237_combout\ = (\inst42|seven_seg0[1]~233_combout\) # ((\inst42|seven_seg0[1]~236_combout\ & (\inst38|inst41~q\ $ (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~236_combout\,
	datab => \inst42|seven_seg0[1]~233_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[1]~237_combout\);

-- Location: LCCOMB_X15_Y26_N28
\inst42|seven_seg0[1]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~244_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg0[1]~237_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[1]~243_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[1]~243_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[1]~237_combout\,
	combout => \inst42|seven_seg0[1]~244_combout\);

-- Location: LCCOMB_X15_Y26_N12
\inst42|seven_seg0[1]~260\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~260_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg0[1]~244_combout\ & (\inst42|seven_seg0[1]~259_combout\)) # (!\inst42|seven_seg0[1]~244_combout\ & ((\inst42|seven_seg0[1]~297_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg0[1]~244_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[1]~259_combout\,
	datac => \inst42|seven_seg0[1]~297_combout\,
	datad => \inst42|seven_seg0[1]~244_combout\,
	combout => \inst42|seven_seg0[1]~260_combout\);

-- Location: LCCOMB_X8_Y22_N18
\inst42|seven_seg0[0]~262\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~262_combout\ = (\inst42|seven_seg0[0]~192_combout\ & ((\inst42|seven_seg0[6]~180_combout\) # ((!\inst42|seven_seg0[6]~71_combout\)))) # (!\inst42|seven_seg0[0]~192_combout\ & (((\inst42|seven_seg0[6]~71_combout\ & 
-- \inst42|seven_seg0[6]~181_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~192_combout\,
	datab => \inst42|seven_seg0[6]~180_combout\,
	datac => \inst42|seven_seg0[6]~71_combout\,
	datad => \inst42|seven_seg0[6]~181_combout\,
	combout => \inst42|seven_seg0[0]~262_combout\);

-- Location: LCCOMB_X8_Y22_N8
\inst42|seven_seg0[0]~263\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~263_combout\ = (\inst42|seven_seg0[0]~262_combout\ & (((\inst42|seven_seg0[6]~71_combout\) # (\inst42|seven_seg0[6]~179_combout\)))) # (!\inst42|seven_seg0[0]~262_combout\ & (\inst42|seven_seg0[6]~82_combout\ & 
-- (!\inst42|seven_seg0[6]~71_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~82_combout\,
	datab => \inst42|seven_seg0[0]~262_combout\,
	datac => \inst42|seven_seg0[6]~71_combout\,
	datad => \inst42|seven_seg0[6]~179_combout\,
	combout => \inst42|seven_seg0[0]~263_combout\);

-- Location: LCCOMB_X4_Y22_N10
\inst42|seven_seg0[0]~261\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~261_combout\ = (\inst42|seven_seg0[0]~98_combout\ & (((\inst42|seven_seg0[0]~190_combout\) # (\inst42|seven_seg0[6]~92_combout\)))) # (!\inst42|seven_seg0[0]~98_combout\ & (\inst42|seven_seg0[6]~276_combout\ & 
-- (!\inst42|seven_seg0[0]~190_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~276_combout\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst42|seven_seg0[0]~190_combout\,
	datad => \inst42|seven_seg0[6]~92_combout\,
	combout => \inst42|seven_seg0[0]~261_combout\);

-- Location: LCCOMB_X4_Y22_N12
\inst42|seven_seg0[0]~264\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~264_combout\ = (\inst42|seven_seg0[0]~190_combout\ & ((\inst42|seven_seg0[0]~261_combout\ & ((\inst42|seven_seg0[0]~263_combout\))) # (!\inst42|seven_seg0[0]~261_combout\ & (\inst42|seven_seg0[6]~87_combout\)))) # 
-- (!\inst42|seven_seg0[0]~190_combout\ & (((\inst42|seven_seg0[0]~261_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~190_combout\,
	datab => \inst42|seven_seg0[6]~87_combout\,
	datac => \inst42|seven_seg0[0]~263_combout\,
	datad => \inst42|seven_seg0[0]~261_combout\,
	combout => \inst42|seven_seg0[0]~264_combout\);

-- Location: LCCOMB_X6_Y22_N2
\inst42|seven_seg0[0]~265\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~265_combout\ = (\inst39|inst43~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg0[0]~264_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg0[6]~102_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg0[0]~264_combout\,
	datad => \inst42|seven_seg0[6]~102_combout\,
	combout => \inst42|seven_seg0[0]~265_combout\);

-- Location: LCCOMB_X6_Y22_N4
\inst42|seven_seg0[0]~293\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~293_combout\ = (\inst42|seven_seg0[0]~265_combout\) # ((!\inst39|inst43~q\ & ((\inst42|seven_seg0[3]~287_combout\) # (\inst42|seven_seg0[3]~189_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[3]~287_combout\,
	datab => \inst42|seven_seg0[3]~189_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[0]~265_combout\,
	combout => \inst42|seven_seg0[0]~293_combout\);

-- Location: LCCOMB_X14_Y24_N20
\inst42|seven_seg1[6]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~44_combout\ = (\inst39|inst41~q\ & ((\inst38|inst41~q\) # ((!\inst39|inst19~q\)))) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\) # ((!\inst38|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~44_combout\);

-- Location: LCCOMB_X14_Y24_N8
\inst42|seven_seg1[6]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~42_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~42_combout\);

-- Location: LCCOMB_X14_Y24_N2
\inst42|seven_seg1[6]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~43_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\ $ (!\inst38|inst43~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\) # ((!\inst39|inst19~q\ & !\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010100001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~43_combout\);

-- Location: LCCOMB_X14_Y24_N30
\inst42|seven_seg1[6]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~45_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[6]~43_combout\ & (\inst42|seven_seg1[6]~44_combout\)) # (!\inst42|seven_seg1[6]~43_combout\ & ((\inst42|seven_seg1[6]~42_combout\))))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg1[6]~43_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[6]~44_combout\,
	datac => \inst42|seven_seg1[6]~42_combout\,
	datad => \inst42|seven_seg1[6]~43_combout\,
	combout => \inst42|seven_seg1[6]~45_combout\);

-- Location: LCCOMB_X14_Y24_N18
\inst42|seven_seg1[6]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~49_combout\ = (\inst38|inst43~q\ & (!\inst39|inst19~q\ & !\inst38|inst41~q\)) # (!\inst38|inst43~q\ & ((\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101001001010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~49_combout\);

-- Location: LCCOMB_X14_Y24_N12
\inst42|seven_seg1[6]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~50_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg1[6]~49_combout\ & ((!\inst39|inst41~q\) # (!\inst39|inst19~q\)))) # (!\inst38|inst42~q\ & (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & !\inst42|seven_seg1[6]~49_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg1[6]~49_combout\,
	combout => \inst42|seven_seg1[6]~50_combout\);

-- Location: LCCOMB_X14_Y24_N6
\inst42|seven_seg1[6]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~47_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & (\inst38|inst43~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~47_combout\);

-- Location: LCCOMB_X14_Y24_N0
\inst42|seven_seg1[6]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~46_combout\ = (!\inst38|inst42~q\ & (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~46_combout\);

-- Location: LCCOMB_X14_Y24_N24
\inst42|seven_seg1[6]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~48_combout\ = (\inst39|inst19~q\ & ((\inst38|inst41~q\ & ((\inst42|seven_seg1[6]~46_combout\))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[6]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~47_combout\,
	datab => \inst42|seven_seg1[6]~46_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~48_combout\);

-- Location: LCCOMB_X14_Y24_N14
\inst42|seven_seg1[6]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~51_combout\ = (\inst38|inst8~q\ & (((\inst38|inst19~q\) # (\inst42|seven_seg1[6]~48_combout\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg1[6]~50_combout\ & (!\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~50_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~48_combout\,
	combout => \inst42|seven_seg1[6]~51_combout\);

-- Location: LCCOMB_X17_Y23_N16
\inst42|seven_seg1[6]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~54_combout\ = (\inst39|inst19~q\ & (!\inst39|inst41~q\)) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011001110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[6]~54_combout\);

-- Location: LCCOMB_X17_Y23_N30
\inst42|seven_seg1[6]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~55_combout\ = (\inst39|inst8~q\ & (\inst42|Equal0~1_combout\ & \inst42|seven_seg1[6]~54_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~1_combout\,
	datad => \inst42|seven_seg1[6]~54_combout\,
	combout => \inst42|seven_seg1[6]~55_combout\);

-- Location: LCCOMB_X17_Y25_N22
\inst42|seven_seg0[6]~266\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~266_combout\ = (!\inst39|inst41~q\ & !\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~266_combout\);

-- Location: LCCOMB_X17_Y25_N20
\inst42|seven_seg1[6]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~56_combout\ = (\inst42|seven_seg0[0]~98_combout\ & ((\inst42|seven_seg1[6]~55_combout\) # ((!\inst39|inst19~q\ & \inst42|seven_seg0[6]~266_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~55_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg0[6]~266_combout\,
	datad => \inst42|seven_seg0[0]~98_combout\,
	combout => \inst42|seven_seg1[6]~56_combout\);

-- Location: LCCOMB_X17_Y26_N20
\inst42|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~0_combout\ = (!\inst38|inst42~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|Equal0~0_combout\);

-- Location: LCCOMB_X14_Y23_N16
\inst42|Equal0~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~9_combout\ = (!\inst39|inst41~q\ & !\inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|Equal0~9_combout\);

-- Location: LCCOMB_X19_Y25_N26
\inst42|seven_seg1[6]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~52_combout\ = (\inst38|inst43~q\) # ((\inst38|inst42~q\ & \inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~52_combout\);

-- Location: LCCOMB_X17_Y25_N0
\inst42|seven_seg1[6]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~53_combout\ = (\inst38|inst41~q\ & (((\inst42|seven_seg1[6]~52_combout\)))) # (!\inst38|inst41~q\ & (\inst42|Equal0~0_combout\ & (\inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~0_combout\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[6]~52_combout\,
	combout => \inst42|seven_seg1[6]~53_combout\);

-- Location: LCCOMB_X17_Y25_N30
\inst42|seven_seg1[6]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~57_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[6]~56_combout\) # ((!\inst39|inst19~q\ & !\inst42|seven_seg1[6]~53_combout\)))) # (!\inst38|inst41~q\ & (((\inst42|seven_seg1[6]~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[6]~56_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[6]~53_combout\,
	combout => \inst42|seven_seg1[6]~57_combout\);

-- Location: LCCOMB_X14_Y24_N4
\inst42|seven_seg1[6]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~58_combout\ = (\inst42|seven_seg1[6]~51_combout\ & (((\inst42|seven_seg1[6]~57_combout\) # (!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[6]~51_combout\ & (\inst42|seven_seg1[6]~45_combout\ & ((\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~45_combout\,
	datab => \inst42|seven_seg1[6]~51_combout\,
	datac => \inst42|seven_seg1[6]~57_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[6]~58_combout\);

-- Location: LCCOMB_X14_Y26_N24
\inst42|seven_seg1[6]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~69_combout\ = (\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst38|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst38|inst19~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~69_combout\);

-- Location: LCCOMB_X14_Y26_N6
\inst42|seven_seg1[6]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~70_combout\ = (\inst38|inst42~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst19~q\ & (!\inst38|inst43~q\ & \inst42|seven_seg1[6]~69_combout\)) # (!\inst38|inst19~q\ & (\inst38|inst43~q\ & 
-- !\inst42|seven_seg1[6]~69_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[6]~69_combout\,
	combout => \inst42|seven_seg1[6]~70_combout\);

-- Location: LCCOMB_X14_Y26_N4
\inst42|seven_seg1[6]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~71_combout\ = (\inst39|inst19~q\ & ((\inst42|seven_seg1[6]~70_combout\) # ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~70_combout\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~71_combout\);

-- Location: LCCOMB_X14_Y26_N10
\inst42|seven_seg1[6]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~72_combout\ = (\inst38|inst42~q\ & ((\inst38|inst19~q\ & (\inst42|seven_seg1[6]~71_combout\)) # (!\inst38|inst19~q\ & ((!\inst42|seven_seg1[6]~70_combout\))))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg1[6]~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001111000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[6]~71_combout\,
	datad => \inst42|seven_seg1[6]~70_combout\,
	combout => \inst42|seven_seg1[6]~72_combout\);

-- Location: LCCOMB_X14_Y26_N20
\inst42|seven_seg1[6]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~61_combout\ = (\inst38|inst43~q\ & (\inst38|inst42~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst41~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~61_combout\);

-- Location: LCCOMB_X14_Y26_N30
\inst42|seven_seg1[6]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~62_combout\ = \inst38|inst43~q\ $ (((!\inst38|inst19~q\ & !\inst42|seven_seg1[6]~61_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[6]~61_combout\,
	combout => \inst42|seven_seg1[6]~62_combout\);

-- Location: LCCOMB_X14_Y26_N16
\inst42|seven_seg1[6]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~63_combout\ = (\inst38|inst42~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & \inst39|inst41~q\)))) # (!\inst38|inst42~q\ & ((\inst39|inst19~q\ & ((!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # 
-- (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100111011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~63_combout\);

-- Location: LCCOMB_X14_Y26_N26
\inst42|seven_seg1[6]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~64_combout\ = (\inst38|inst19~q\ & ((\inst38|inst42~q\ & (\inst42|seven_seg1[6]~62_combout\ & \inst42|seven_seg1[6]~63_combout\)) # (!\inst38|inst42~q\ & ((\inst42|seven_seg1[6]~62_combout\) # (\inst42|seven_seg1[6]~63_combout\))))) 
-- # (!\inst38|inst19~q\ & (((\inst42|seven_seg1[6]~62_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[6]~62_combout\,
	datad => \inst42|seven_seg1[6]~63_combout\,
	combout => \inst42|seven_seg1[6]~64_combout\);

-- Location: LCCOMB_X14_Y26_N0
\inst42|seven_seg1[6]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~65_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~65_combout\);

-- Location: LCCOMB_X14_Y26_N18
\inst42|seven_seg1[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~66_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst19~q\ & !\inst42|seven_seg1[6]~65_combout\)))) # (!\inst38|inst42~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg1[6]~65_combout\))) # (!\inst38|inst19~q\ & (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~65_combout\,
	combout => \inst42|seven_seg1[6]~66_combout\);

-- Location: LCCOMB_X14_Y26_N12
\inst42|seven_seg1[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~67_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg1[6]~66_combout\ & ((\inst39|inst19~q\)))) # (!\inst38|inst19~q\ & ((\inst39|inst19~q\ & ((!\inst42|Equal0~0_combout\))) # (!\inst39|inst19~q\ & 
-- (!\inst42|seven_seg1[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[6]~66_combout\,
	datac => \inst42|Equal0~0_combout\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~67_combout\);

-- Location: LCCOMB_X14_Y26_N14
\inst42|seven_seg1[6]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~68_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[6]~64_combout\) # ((\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & (((!\inst38|inst41~q\ & \inst42|seven_seg1[6]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~64_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[6]~67_combout\,
	combout => \inst42|seven_seg1[6]~68_combout\);

-- Location: LCCOMB_X14_Y26_N28
\inst42|seven_seg1[6]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~59_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & (\inst38|inst43~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~59_combout\);

-- Location: LCCOMB_X14_Y26_N22
\inst42|seven_seg1[6]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~250_combout\ = (\inst38|inst19~q\ & (!\inst39|inst41~q\ & (\inst42|Equal0~0_combout\ & !\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst42|Equal0~0_combout\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~250_combout\);

-- Location: LCCOMB_X14_Y26_N2
\inst42|seven_seg1[6]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~60_combout\ = (\inst42|seven_seg1[6]~250_combout\) # ((!\inst38|inst19~q\ & (\inst42|seven_seg1[6]~59_combout\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[6]~59_combout\,
	datac => \inst42|seven_seg1[6]~250_combout\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[6]~60_combout\);

-- Location: LCCOMB_X14_Y26_N8
\inst42|seven_seg1[6]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~73_combout\ = (\inst42|seven_seg1[6]~68_combout\ & ((\inst42|seven_seg1[6]~72_combout\) # ((!\inst38|inst41~q\)))) # (!\inst42|seven_seg1[6]~68_combout\ & (((\inst38|inst41~q\ & \inst42|seven_seg1[6]~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~72_combout\,
	datab => \inst42|seven_seg1[6]~68_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[6]~60_combout\,
	combout => \inst42|seven_seg1[6]~73_combout\);

-- Location: LCCOMB_X14_Y24_N26
\inst42|seven_seg1[6]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~74_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg1[6]~58_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[6]~73_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[6]~58_combout\,
	datad => \inst42|seven_seg1[6]~73_combout\,
	combout => \inst42|seven_seg1[6]~74_combout\);

-- Location: LCCOMB_X16_Y25_N26
\inst42|seven_seg1[5]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~107_combout\ = (!\inst39|inst19~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~107_combout\);

-- Location: LCCOMB_X16_Y25_N12
\inst42|seven_seg1[5]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~108_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~40_combout\) # (\inst38|inst8~q\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[5]~107_combout\ & ((!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~107_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[5]~40_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~108_combout\);

-- Location: LCCOMB_X16_Y26_N20
\inst42|Equal0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~12_combout\ = (!\inst39|inst42~q\ & (!\inst39|inst19~q\ & !\inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	combout => \inst42|Equal0~12_combout\);

-- Location: LCCOMB_X16_Y26_N2
\inst42|seven_seg1[5]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~109_combout\ = (\inst42|Equal0~12_combout\ & ((\inst42|Equal0~8_combout\) # (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~8_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|Equal0~12_combout\,
	combout => \inst42|seven_seg1[5]~109_combout\);

-- Location: LCCOMB_X16_Y25_N10
\inst42|seven_seg1[5]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~110_combout\ = (\inst42|seven_seg1[5]~108_combout\ & (((!\inst38|inst8~q\) # (!\inst42|seven_seg1[5]~109_combout\)))) # (!\inst42|seven_seg1[5]~108_combout\ & (\inst42|Equal0~9_combout\ & ((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~108_combout\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst42|seven_seg1[5]~109_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~110_combout\);

-- Location: LCCOMB_X16_Y25_N8
\inst42|Equal0~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~11_combout\ = (\inst39|inst41~q\ & \inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|Equal0~11_combout\);

-- Location: LCCOMB_X16_Y25_N22
\inst42|seven_seg1[5]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~105_combout\ = (\inst38|inst43~q\ & (((!\inst38|inst8~q\)) # (!\inst42|Equal0~11_combout\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~40_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|Equal0~11_combout\,
	datac => \inst42|seven_seg1[5]~40_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~105_combout\);

-- Location: LCCOMB_X16_Y26_N22
\inst42|seven_seg1[5]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~104_combout\ = (\inst38|inst8~q\) # ((\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~104_combout\);

-- Location: LCCOMB_X16_Y26_N30
\inst42|seven_seg1[5]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~252_combout\ = (\inst38|inst43~q\ & (!\inst39|inst19~q\ & ((!\inst38|inst8~q\)))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~104_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg1[5]~104_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~252_combout\);

-- Location: LCCOMB_X16_Y25_N20
\inst42|seven_seg1[5]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~106_combout\ = (\inst38|inst42~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg1[5]~252_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg1[5]~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~105_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~252_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~106_combout\);

-- Location: LCCOMB_X16_Y25_N6
\inst42|seven_seg1[5]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~102_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~102_combout\);

-- Location: LCCOMB_X15_Y25_N24
\inst42|seven_seg1[5]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~103_combout\ = (!\inst38|inst43~q\ & \inst42|seven_seg1[5]~102_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[5]~102_combout\,
	combout => \inst42|seven_seg1[5]~103_combout\);

-- Location: LCCOMB_X16_Y25_N0
\inst42|seven_seg1[5]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~111_combout\ = (\inst42|seven_seg1[5]~106_combout\ & ((\inst42|seven_seg1[5]~110_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[5]~106_combout\ & (((\inst38|inst42~q\ & \inst42|seven_seg1[5]~103_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~110_combout\,
	datab => \inst42|seven_seg1[5]~106_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~103_combout\,
	combout => \inst42|seven_seg1[5]~111_combout\);

-- Location: LCCOMB_X19_Y25_N20
\inst42|seven_seg1[5]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~77_combout\ = (\inst38|inst8~q\ & ((\inst39|inst8~q\) # ((\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~77_combout\);

-- Location: LCCOMB_X19_Y25_N6
\inst42|seven_seg1[5]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~80_combout\ = (\inst38|inst8~q\ & (!\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst8~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~80_combout\);

-- Location: LCCOMB_X19_Y25_N12
\inst42|seven_seg1[5]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~81_combout\ = (\inst38|inst42~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~80_combout\))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[5]~77_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[5]~77_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~80_combout\,
	combout => \inst42|seven_seg1[5]~81_combout\);

-- Location: LCCOMB_X19_Y25_N22
\inst42|seven_seg1[5]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~78_combout\ = (\inst39|inst8~q\ & (!\inst38|inst8~q\ & ((!\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\ & \inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~78_combout\);

-- Location: LCCOMB_X19_Y25_N28
\inst42|seven_seg1[5]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~79_combout\ = (!\inst38|inst42~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~77_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[5]~78_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~78_combout\,
	datad => \inst42|seven_seg1[5]~77_combout\,
	combout => \inst42|seven_seg1[5]~79_combout\);

-- Location: LCCOMB_X19_Y25_N30
\inst42|seven_seg1[5]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~76_combout\ = (\inst38|inst8~q\ & ((\inst39|inst8~q\) # ((\inst38|inst43~q\ & !\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & (\inst38|inst43~q\ $ (((\inst39|inst8~q\ & \inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001110011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~76_combout\);

-- Location: LCCOMB_X19_Y25_N8
\inst42|seven_seg1[5]~264\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~264_combout\ = (\inst38|inst8~q\ & (!\inst38|inst42~q\ & ((\inst39|inst41~q\) # (\inst42|seven_seg1[5]~76_combout\)))) # (!\inst38|inst8~q\ & (\inst38|inst42~q\ & (\inst39|inst41~q\ $ (!\inst42|seven_seg1[5]~76_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg1[5]~76_combout\,
	combout => \inst42|seven_seg1[5]~264_combout\);

-- Location: LCCOMB_X19_Y25_N24
\inst42|seven_seg1[5]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~75_combout\ = (\inst39|inst8~q\ & ((\inst38|inst43~q\) # ((!\inst38|inst8~q\ & !\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (\inst38|inst8~q\ & (!\inst38|inst43~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~75_combout\);

-- Location: LCCOMB_X19_Y25_N2
\inst42|seven_seg1[5]~265\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~265_combout\ = (\inst42|seven_seg1[5]~75_combout\ & (\inst42|seven_seg1[5]~264_combout\ $ (((!\inst42|seven_seg1[5]~76_combout\ & !\inst38|inst42~q\))))) # (!\inst42|seven_seg1[5]~75_combout\ & (!\inst42|seven_seg1[5]~76_combout\ & 
-- ((\inst42|seven_seg1[5]~264_combout\) # (\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100101010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~76_combout\,
	datab => \inst42|seven_seg1[5]~264_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~75_combout\,
	combout => \inst42|seven_seg1[5]~265_combout\);

-- Location: LCCOMB_X19_Y25_N14
\inst42|seven_seg1[5]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~82_combout\ = (\inst39|inst42~q\ & ((\inst42|seven_seg1[5]~81_combout\) # ((\inst42|seven_seg1[5]~79_combout\)))) # (!\inst39|inst42~q\ & (((\inst42|seven_seg1[5]~265_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~81_combout\,
	datab => \inst42|seven_seg1[5]~79_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg1[5]~265_combout\,
	combout => \inst42|seven_seg1[5]~82_combout\);

-- Location: LCCOMB_X17_Y22_N22
\inst42|seven_seg1[5]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~83_combout\ = (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & \inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~83_combout\);

-- Location: LCCOMB_X17_Y22_N16
\inst42|seven_seg1[5]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~84_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ $ (((\inst38|inst43~q\))))) # (!\inst39|inst41~q\ & (\inst38|inst43~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~84_combout\);

-- Location: LCCOMB_X17_Y22_N30
\inst42|seven_seg1[5]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~85_combout\ = (\inst39|inst8~q\ & ((\inst38|inst8~q\) # ((\inst42|seven_seg1[5]~83_combout\)))) # (!\inst39|inst8~q\ & (!\inst38|inst8~q\ & ((!\inst42|seven_seg1[5]~84_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg1[5]~83_combout\,
	datad => \inst42|seven_seg1[5]~84_combout\,
	combout => \inst42|seven_seg1[5]~85_combout\);

-- Location: LCCOMB_X17_Y22_N8
\inst42|seven_seg1[5]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~86_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ & (\inst39|inst42~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~86_combout\);

-- Location: LCCOMB_X17_Y22_N10
\inst42|seven_seg1[5]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~87_combout\ = (\inst42|seven_seg1[5]~85_combout\ & ((\inst42|seven_seg1[5]~86_combout\) # ((!\inst38|inst8~q\)))) # (!\inst42|seven_seg1[5]~85_combout\ & (((\inst42|seven_seg0[5]~250_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~85_combout\,
	datab => \inst42|seven_seg1[5]~86_combout\,
	datac => \inst42|seven_seg0[5]~250_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~87_combout\);

-- Location: LCCOMB_X17_Y22_N20
\inst42|seven_seg1[5]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~89_combout\ = (\inst39|inst19~q\) # ((\inst39|inst42~q\ & ((\inst39|inst41~q\) # (\inst39|inst8~q\))) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~89_combout\);

-- Location: LCCOMB_X20_Y25_N16
\inst42|seven_seg1[5]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~90_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((\inst38|inst8~q\) # (\inst42|seven_seg1[5]~89_combout\))) # (!\inst39|inst8~q\ & (\inst38|inst8~q\ & \inst42|seven_seg1[5]~89_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg1[5]~89_combout\,
	combout => \inst42|seven_seg1[5]~90_combout\);

-- Location: LCCOMB_X20_Y25_N18
\inst42|seven_seg1[5]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~88_combout\ = (!\inst39|inst8~q\ & (\inst38|inst43~q\ & ((\inst42|Equal0~9_combout\) # (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg1[5]~88_combout\);

-- Location: LCCOMB_X20_Y25_N6
\inst42|seven_seg1[5]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~91_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg1[5]~90_combout\) # (\inst42|seven_seg1[5]~88_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[5]~87_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~87_combout\,
	datab => \inst42|seven_seg1[5]~90_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~88_combout\,
	combout => \inst42|seven_seg1[5]~91_combout\);

-- Location: LCCOMB_X16_Y22_N0
\inst42|seven_seg1[5]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~96_combout\ = (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & (!\inst39|inst8~q\ & !\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~96_combout\);

-- Location: LCCOMB_X16_Y22_N28
\inst42|seven_seg1[5]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~92_combout\ = (!\inst39|inst19~q\ & (!\inst39|inst8~q\ & !\inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[5]~92_combout\);

-- Location: LCCOMB_X16_Y22_N14
\inst42|seven_seg1[5]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~93_combout\ = (\inst39|inst8~q\) # ((\inst39|inst19~q\ & (\inst39|inst41~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~93_combout\);

-- Location: LCCOMB_X16_Y22_N20
\inst42|seven_seg1[5]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~94_combout\ = (!\inst39|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~94_combout\);

-- Location: LCCOMB_X16_Y22_N26
\inst42|seven_seg1[5]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~95_combout\ = (\inst38|inst43~q\ & ((\inst38|inst8~q\) # ((\inst42|seven_seg1[5]~93_combout\)))) # (!\inst38|inst43~q\ & (!\inst38|inst8~q\ & ((\inst42|seven_seg1[5]~94_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg1[5]~93_combout\,
	datad => \inst42|seven_seg1[5]~94_combout\,
	combout => \inst42|seven_seg1[5]~95_combout\);

-- Location: LCCOMB_X16_Y22_N2
\inst42|seven_seg1[5]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~97_combout\ = (\inst42|seven_seg1[5]~95_combout\ & (((!\inst38|inst8~q\)) # (!\inst42|seven_seg1[5]~96_combout\))) # (!\inst42|seven_seg1[5]~95_combout\ & (((\inst42|seven_seg1[5]~92_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~96_combout\,
	datab => \inst42|seven_seg1[5]~92_combout\,
	datac => \inst42|seven_seg1[5]~95_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~97_combout\);

-- Location: LCCOMB_X17_Y23_N8
\inst42|seven_seg1[5]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~99_combout\ = (\inst39|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~99_combout\);

-- Location: LCCOMB_X17_Y23_N18
\inst42|Equal0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~10_combout\ = (!\inst39|inst19~q\ & !\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|Equal0~10_combout\);

-- Location: LCCOMB_X17_Y23_N24
\inst42|seven_seg1[5]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~251_combout\ = (\inst38|inst8~q\ & (\inst42|seven_seg1[5]~99_combout\ & ((\inst38|inst43~q\)))) # (!\inst38|inst8~q\ & (((\inst42|Equal0~10_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~99_combout\,
	datab => \inst42|Equal0~10_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~251_combout\);

-- Location: LCCOMB_X19_Y25_N0
\inst42|seven_seg1[5]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~98_combout\ = (!\inst38|inst43~q\ & ((!\inst38|inst8~q\) # (!\inst42|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|Equal0~3_combout\,
	datac => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~98_combout\);

-- Location: LCCOMB_X19_Y25_N18
\inst42|seven_seg1[5]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~100_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg1[5]~97_combout\)) # (!\inst38|inst42~q\ & (((\inst42|seven_seg1[5]~251_combout\) # (\inst42|seven_seg1[5]~98_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~97_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~251_combout\,
	datad => \inst42|seven_seg1[5]~98_combout\,
	combout => \inst42|seven_seg1[5]~100_combout\);

-- Location: LCCOMB_X19_Y25_N16
\inst42|seven_seg1[5]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~101_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[5]~91_combout\) # ((\inst38|inst19~q\)))) # (!\inst38|inst41~q\ & (((!\inst38|inst19~q\ & \inst42|seven_seg1[5]~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~91_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[5]~100_combout\,
	combout => \inst42|seven_seg1[5]~101_combout\);

-- Location: LCCOMB_X19_Y25_N10
\inst42|seven_seg1[5]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~112_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[5]~101_combout\ & (\inst42|seven_seg1[5]~111_combout\)) # (!\inst42|seven_seg1[5]~101_combout\ & ((\inst42|seven_seg1[5]~82_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg1[5]~101_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~111_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[5]~82_combout\,
	datad => \inst42|seven_seg1[5]~101_combout\,
	combout => \inst42|seven_seg1[5]~112_combout\);

-- Location: LCCOMB_X17_Y24_N20
\inst42|seven_seg1[4]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~148_combout\ = (\inst38|inst8~q\ & (((!\inst39|inst8~q\) # (!\inst39|inst41~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & ((\inst39|inst8~q\) # ((\inst39|inst19~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~148_combout\);

-- Location: LCCOMB_X17_Y24_N14
\inst42|seven_seg1[4]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~136_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (!\inst39|inst8~q\))))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\) # ((!\inst39|inst41~q\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~136_combout\);

-- Location: LCCOMB_X17_Y24_N18
\inst42|seven_seg1[4]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~149_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~136_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~148_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~148_combout\,
	datac => \inst42|seven_seg1[4]~136_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~149_combout\);

-- Location: LCCOMB_X17_Y24_N30
\inst42|seven_seg1[4]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~147_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\ & !\inst39|inst8~q\))))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))) # (!\inst38|inst8~q\ & 
-- ((\inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~147_combout\);

-- Location: LCCOMB_X17_Y23_N12
\inst42|seven_seg1[4]~259\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~259_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\) # (\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (((!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[4]~259_combout\);

-- Location: LCCOMB_X17_Y23_N26
\inst42|seven_seg1[4]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~253_combout\ = (\inst42|seven_seg1[4]~259_combout\ & (\inst39|inst8~q\ & (\inst42|Equal0~1_combout\ & \inst42|seven_seg1[6]~54_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~259_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~1_combout\,
	datad => \inst42|seven_seg1[6]~54_combout\,
	combout => \inst42|seven_seg1[4]~253_combout\);

-- Location: LCCOMB_X17_Y23_N0
\inst42|seven_seg1[4]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~150_combout\ = (\inst38|inst8~q\ & (((\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst39|inst8~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst8~q\ & (\inst39|inst41~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[4]~150_combout\);

-- Location: LCCOMB_X17_Y24_N12
\inst42|seven_seg1[4]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~151_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[4]~253_combout\) # ((!\inst42|seven_seg1[4]~150_combout\ & !\inst39|inst8~q\)))) # (!\inst38|inst8~q\ & (((\inst42|seven_seg1[4]~150_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~253_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg1[4]~150_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~151_combout\);

-- Location: LCCOMB_X17_Y24_N22
\inst42|seven_seg1[4]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~152_combout\ = (\inst42|seven_seg1[4]~149_combout\ & (((\inst42|seven_seg1[4]~151_combout\)) # (!\inst38|inst41~q\))) # (!\inst42|seven_seg1[4]~149_combout\ & (\inst38|inst41~q\ & (!\inst42|seven_seg1[4]~147_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~149_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg1[4]~147_combout\,
	datad => \inst42|seven_seg1[4]~151_combout\,
	combout => \inst42|seven_seg1[4]~152_combout\);

-- Location: LCCOMB_X7_Y22_N12
\inst42|seven_seg1[4]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~142_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & ((!\inst38|inst41~q\)))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & (!\inst39|inst41~q\ & \inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~142_combout\);

-- Location: LCCOMB_X7_Y22_N2
\inst42|seven_seg1[4]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~141_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~141_combout\);

-- Location: LCCOMB_X7_Y22_N22
\inst42|seven_seg1[4]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~143_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\) # (!\inst42|seven_seg1[4]~141_combout\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[4]~142_combout\ & (!\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000111001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~142_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[4]~141_combout\,
	combout => \inst42|seven_seg1[4]~143_combout\);

-- Location: LCCOMB_X7_Y22_N8
\inst42|seven_seg1[4]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~144_combout\ = (\inst39|inst19~q\ & ((\inst38|inst8~q\ & (\inst39|inst41~q\ $ (!\inst38|inst41~q\))) # (!\inst38|inst8~q\ & ((\inst39|inst41~q\) # (\inst38|inst41~q\))))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ 
-- (((\inst39|inst41~q\ & \inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011001101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~144_combout\);

-- Location: LCCOMB_X7_Y22_N16
\inst42|seven_seg1[4]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~140_combout\ = (\inst38|inst8~q\ & (\inst38|inst41~q\ $ (((!\inst39|inst19~q\ & !\inst39|inst41~q\))))) # (!\inst38|inst8~q\ & (!\inst38|inst41~q\ & ((\inst39|inst19~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~140_combout\);

-- Location: LCCOMB_X7_Y22_N26
\inst42|seven_seg1[4]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~145_combout\ = (\inst42|seven_seg1[4]~143_combout\ & (((!\inst42|seven_seg1[4]~144_combout\)) # (!\inst39|inst8~q\))) # (!\inst42|seven_seg1[4]~143_combout\ & (\inst39|inst8~q\ & ((!\inst42|seven_seg1[4]~140_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101001101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~143_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[4]~144_combout\,
	datad => \inst42|seven_seg1[4]~140_combout\,
	combout => \inst42|seven_seg1[4]~145_combout\);

-- Location: LCCOMB_X19_Y24_N0
\inst42|seven_seg1[4]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~113_combout\ = (\inst39|inst19~q\ & (((\inst39|inst41~q\ & !\inst39|inst8~q\)) # (!\inst38|inst8~q\))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\ & !\inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100111001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~113_combout\);

-- Location: LCCOMB_X19_Y24_N8
\inst42|seven_seg1[4]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~119_combout\ = (\inst39|inst19~q\ & (!\inst38|inst8~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000110001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~119_combout\);

-- Location: LCCOMB_X19_Y24_N26
\inst42|seven_seg1[4]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~137_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~113_combout\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[4]~119_combout\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~113_combout\,
	datac => \inst42|seven_seg1[4]~119_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~137_combout\);

-- Location: LCCOMB_X17_Y24_N24
\inst42|seven_seg1[4]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~138_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & \inst39|inst8~q\)) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~138_combout\);

-- Location: LCCOMB_X17_Y24_N10
\inst42|seven_seg1[4]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~139_combout\ = (\inst42|seven_seg1[4]~137_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~138_combout\))) # (!\inst42|seven_seg1[4]~137_combout\ & (((!\inst42|seven_seg1[4]~136_combout\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~137_combout\,
	datab => \inst42|seven_seg1[4]~138_combout\,
	datac => \inst42|seven_seg1[4]~136_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~139_combout\);

-- Location: LCCOMB_X17_Y24_N4
\inst42|seven_seg1[4]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~146_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg1[4]~139_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[4]~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[4]~145_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[4]~139_combout\,
	combout => \inst42|seven_seg1[4]~146_combout\);

-- Location: LCCOMB_X19_Y24_N6
\inst42|seven_seg1[4]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~134_combout\ = (!\inst39|inst8~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~134_combout\);

-- Location: LCCOMB_X19_Y24_N4
\inst42|seven_seg1[4]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~115_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst8~q\) # (!\inst39|inst41~q\))))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst39|inst8~q\ & !\inst38|inst8~q\)) # (!\inst39|inst41~q\ & (\inst39|inst8~q\ 
-- & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100011000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~115_combout\);

-- Location: LCCOMB_X19_Y24_N16
\inst42|seven_seg1[4]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~133_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg1[4]~115_combout\) # (\inst38|inst41~q\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[4]~113_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~113_combout\,
	datac => \inst42|seven_seg1[4]~115_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~133_combout\);

-- Location: LCCOMB_X19_Y24_N28
\inst42|seven_seg1[4]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~135_combout\ = (\inst42|seven_seg1[4]~133_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~134_combout\))) # (!\inst42|seven_seg1[4]~133_combout\ & (((\inst42|seven_seg1[4]~119_combout\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~134_combout\,
	datab => \inst42|seven_seg1[4]~133_combout\,
	datac => \inst42|seven_seg1[4]~119_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~135_combout\);

-- Location: LCCOMB_X17_Y24_N8
\inst42|seven_seg1[4]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~153_combout\ = (\inst42|seven_seg1[4]~146_combout\ & ((\inst42|seven_seg1[4]~152_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[4]~146_combout\ & (((\inst38|inst19~q\ & \inst42|seven_seg1[4]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~152_combout\,
	datab => \inst42|seven_seg1[4]~146_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[4]~135_combout\,
	combout => \inst42|seven_seg1[4]~153_combout\);

-- Location: LCCOMB_X19_Y24_N20
\inst42|seven_seg1[4]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~127_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\) # (!\inst39|inst8~q\)) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (((\inst39|inst8~q\) # (\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~127_combout\);

-- Location: LCCOMB_X19_Y24_N2
\inst42|seven_seg1[4]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~128_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~119_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~127_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~127_combout\,
	datac => \inst42|seven_seg1[4]~119_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~128_combout\);

-- Location: LCCOMB_X19_Y24_N10
\inst42|seven_seg1[4]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~114_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst39|inst8~q\ & !\inst38|inst8~q\)) # (!\inst39|inst41~q\ & (\inst39|inst8~q\ & \inst38|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((!\inst39|inst41~q\ & 
-- \inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~114_combout\);

-- Location: LCCOMB_X17_Y24_N2
\inst42|seven_seg1[4]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~129_combout\ = (\inst38|inst8~q\ & (!\inst39|inst19~q\ & ((!\inst39|inst8~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (((!\inst39|inst41~q\ & \inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~129_combout\);

-- Location: LCCOMB_X17_Y24_N28
\inst42|seven_seg1[4]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~130_combout\ = (\inst42|seven_seg1[4]~129_combout\) # ((\inst42|seven_seg1[4]~253_combout\ & \inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~253_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg1[4]~129_combout\,
	combout => \inst42|seven_seg1[4]~130_combout\);

-- Location: LCCOMB_X17_Y24_N6
\inst42|seven_seg1[4]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~131_combout\ = (\inst42|seven_seg1[4]~128_combout\ & (((\inst42|seven_seg1[4]~130_combout\)) # (!\inst38|inst41~q\))) # (!\inst42|seven_seg1[4]~128_combout\ & (\inst38|inst41~q\ & (!\inst42|seven_seg1[4]~114_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~128_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg1[4]~114_combout\,
	datad => \inst42|seven_seg1[4]~130_combout\,
	combout => \inst42|seven_seg1[4]~131_combout\);

-- Location: LCCOMB_X15_Y23_N10
\inst42|seven_seg1[4]~262\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~262_combout\ = (\inst38|inst8~q\ & ((\inst38|inst41~q\ $ (\inst38|inst43~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (((\inst39|inst19~q\) # (!\inst38|inst43~q\)) # (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[4]~262_combout\);

-- Location: LCCOMB_X14_Y24_N10
\inst42|seven_seg1[4]~263\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~263_combout\ = (\inst39|inst8~q\ & (\inst38|inst43~q\ & ((\inst38|inst41~q\) # (\inst42|seven_seg1[4]~262_combout\)))) # (!\inst39|inst8~q\ & (((\inst42|seven_seg1[4]~262_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~262_combout\,
	combout => \inst42|seven_seg1[4]~263_combout\);

-- Location: LCCOMB_X14_Y24_N22
\inst42|seven_seg1[4]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~124_combout\ = (\inst39|inst41~q\ & (\inst38|inst41~q\ $ (\inst39|inst19~q\ $ (\inst38|inst8~q\)))) # (!\inst39|inst41~q\ & ((\inst38|inst41~q\ & (\inst39|inst19~q\ & !\inst38|inst8~q\)) # (!\inst38|inst41~q\ & (!\inst39|inst19~q\ & 
-- \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000101101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~124_combout\);

-- Location: LCCOMB_X14_Y24_N28
\inst42|seven_seg1[4]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~123_combout\ = (\inst38|inst41~q\ & (\inst38|inst8~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\)))) # (!\inst38|inst41~q\ & (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~123_combout\);

-- Location: LCCOMB_X14_Y24_N16
\inst42|seven_seg1[4]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~125_combout\ = (\inst42|seven_seg1[4]~263_combout\ & (((!\inst42|seven_seg1[4]~124_combout\)) # (!\inst39|inst8~q\))) # (!\inst42|seven_seg1[4]~263_combout\ & (\inst39|inst8~q\ & ((!\inst42|seven_seg1[4]~123_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101001101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~263_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[4]~124_combout\,
	datad => \inst42|seven_seg1[4]~123_combout\,
	combout => \inst42|seven_seg1[4]~125_combout\);

-- Location: LCCOMB_X19_Y24_N22
\inst42|seven_seg1[4]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~120_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~115_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~113_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~113_combout\,
	datac => \inst42|seven_seg1[4]~115_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~120_combout\);

-- Location: LCCOMB_X19_Y24_N24
\inst42|seven_seg1[4]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~121_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\) # (!\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst8~q\) # (\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~121_combout\);

-- Location: LCCOMB_X19_Y24_N18
\inst42|seven_seg1[4]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~122_combout\ = (\inst42|seven_seg1[4]~120_combout\ & ((\inst42|seven_seg1[4]~121_combout\) # ((!\inst38|inst41~q\)))) # (!\inst42|seven_seg1[4]~120_combout\ & (((!\inst42|seven_seg1[4]~119_combout\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~120_combout\,
	datab => \inst42|seven_seg1[4]~121_combout\,
	datac => \inst42|seven_seg1[4]~119_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~122_combout\);

-- Location: LCCOMB_X17_Y24_N16
\inst42|seven_seg1[4]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~126_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg1[4]~122_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[4]~125_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[4]~125_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[4]~122_combout\,
	combout => \inst42|seven_seg1[4]~126_combout\);

-- Location: LCCOMB_X19_Y24_N12
\inst42|seven_seg1[4]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~117_combout\ = (!\inst39|inst8~q\ & (!\inst38|inst8~q\ & (\inst39|inst41~q\ $ (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~117_combout\);

-- Location: LCCOMB_X19_Y24_N14
\inst42|seven_seg1[4]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~116_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~114_combout\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[4]~115_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~115_combout\,
	datac => \inst42|seven_seg1[4]~114_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~116_combout\);

-- Location: LCCOMB_X19_Y24_N30
\inst42|seven_seg1[4]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~118_combout\ = (\inst42|seven_seg1[4]~116_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~117_combout\))) # (!\inst42|seven_seg1[4]~116_combout\ & (((\inst42|seven_seg1[4]~113_combout\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~117_combout\,
	datab => \inst42|seven_seg1[4]~113_combout\,
	datac => \inst42|seven_seg1[4]~116_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~118_combout\);

-- Location: LCCOMB_X17_Y24_N0
\inst42|seven_seg1[4]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~132_combout\ = (\inst42|seven_seg1[4]~126_combout\ & ((\inst42|seven_seg1[4]~131_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[4]~126_combout\ & (((\inst38|inst19~q\ & \inst42|seven_seg1[4]~118_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~131_combout\,
	datab => \inst42|seven_seg1[4]~126_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[4]~118_combout\,
	combout => \inst42|seven_seg1[4]~132_combout\);

-- Location: LCCOMB_X17_Y24_N26
\inst42|seven_seg1[4]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~154_combout\ = (\inst39|inst42~q\ & ((\inst42|seven_seg1[4]~132_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg1[4]~153_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg1[4]~153_combout\,
	datad => \inst42|seven_seg1[4]~132_combout\,
	combout => \inst42|seven_seg1[4]~154_combout\);

-- Location: LCCOMB_X16_Y23_N26
\inst42|seven_seg1[3]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~176_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\) # (!\inst39|inst41~q\)) # (!\inst39|inst42~q\))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\) # ((\inst39|inst42~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~176_combout\);

-- Location: LCCOMB_X16_Y23_N28
\inst42|seven_seg1[3]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~177_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~176_combout\) # ((\inst38|inst43~q\ & \inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~176_combout\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~177_combout\);

-- Location: LCCOMB_X16_Y23_N4
\inst42|seven_seg0[6]~272\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~272_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\ & ((!\inst38|inst8~q\))) # (!\inst39|inst41~q\ & (!\inst39|inst42~q\ & \inst38|inst8~q\)))) # (!\inst39|inst19~q\ & (((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~272_combout\);

-- Location: LCCOMB_X16_Y23_N6
\inst42|seven_seg0[6]~268\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~268_combout\ = (\inst39|inst19~q\ & (!\inst38|inst8~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~268_combout\);

-- Location: LCCOMB_X16_Y23_N30
\inst42|seven_seg1[3]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~178_combout\ = (\inst39|inst8~q\ & (!\inst42|seven_seg1[3]~177_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~177_combout\ & ((\inst42|seven_seg0[6]~268_combout\))) # (!\inst42|seven_seg1[3]~177_combout\ & 
-- (\inst42|seven_seg0[6]~272_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[3]~177_combout\,
	datac => \inst42|seven_seg0[6]~272_combout\,
	datad => \inst42|seven_seg0[6]~268_combout\,
	combout => \inst42|seven_seg1[3]~178_combout\);

-- Location: LCCOMB_X16_Y24_N10
\inst42|seven_seg0[6]~271\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~271_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~271_combout\);

-- Location: LCCOMB_X16_Y23_N12
\inst42|seven_seg0[6]~270\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~270_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~270_combout\);

-- Location: LCCOMB_X17_Y22_N4
\inst42|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~2_combout\ = (\inst39|inst19~q\ & \inst38|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|Equal0~2_combout\);

-- Location: LCCOMB_X16_Y23_N0
\inst42|seven_seg0[6]~267\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~267_combout\ = (\inst38|inst8~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst41~q\))) # (!\inst38|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111001111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~267_combout\);

-- Location: LCCOMB_X16_Y23_N20
\inst42|seven_seg1[3]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~179_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((!\inst42|seven_seg0[6]~267_combout\))) # (!\inst39|inst8~q\ & (!\inst42|Equal0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~2_combout\,
	datab => \inst42|seven_seg0[6]~267_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~179_combout\);

-- Location: LCCOMB_X16_Y23_N2
\inst42|seven_seg1[3]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~180_combout\ = (!\inst42|seven_seg1[3]~179_combout\ & (((\inst39|inst8~q\) # (!\inst38|inst43~q\)) # (!\inst42|seven_seg0[6]~270_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~270_combout\,
	datab => \inst42|seven_seg1[3]~179_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~180_combout\);

-- Location: LCCOMB_X16_Y24_N18
\inst42|seven_seg1[3]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~255_combout\ = ((\inst42|seven_seg0[6]~271_combout\ & (\inst38|inst43~q\ & \inst39|inst8~q\))) # (!\inst42|seven_seg1[3]~180_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~271_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~180_combout\,
	combout => \inst42|seven_seg1[3]~255_combout\);

-- Location: LCCOMB_X16_Y24_N26
\inst42|seven_seg1[3]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~181_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\) # ((\inst42|seven_seg1[3]~178_combout\)))) # (!\inst38|inst41~q\ & (!\inst38|inst42~q\ & ((\inst42|seven_seg1[3]~255_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~178_combout\,
	datad => \inst42|seven_seg1[3]~255_combout\,
	combout => \inst42|seven_seg1[3]~181_combout\);

-- Location: LCCOMB_X17_Y25_N4
\inst42|seven_seg1[3]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~157_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\ & (!\inst39|inst19~q\)) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\) # (\inst39|inst42~q\))))) # (!\inst39|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & 
-- \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[3]~157_combout\);

-- Location: LCCOMB_X16_Y25_N16
\inst42|seven_seg1[3]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~182_combout\ = (\inst39|inst19~q\ & ((\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst39|inst8~q\)) # (!\inst39|inst42~q\ & ((!\inst39|inst8~q\) # (!\inst39|inst41~q\))))) # (!\inst39|inst19~q\ & (((\inst39|inst41~q\) # 
-- (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011101111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[3]~182_combout\);

-- Location: LCCOMB_X16_Y25_N24
\inst42|seven_seg1[3]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~158_combout\ = \inst38|inst8~q\ $ (\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[3]~158_combout\);

-- Location: LCCOMB_X16_Y25_N30
\inst42|seven_seg1[3]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~183_combout\ = (\inst42|seven_seg1[3]~158_combout\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~182_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[3]~157_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~157_combout\,
	datab => \inst42|seven_seg1[3]~182_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[3]~158_combout\,
	combout => \inst42|seven_seg1[3]~183_combout\);

-- Location: LCCOMB_X15_Y23_N22
\inst42|seven_seg1[3]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~166_combout\ = (\inst38|inst8~q\ & ((\inst39|inst41~q\ & ((!\inst39|inst19~q\))) # (!\inst39|inst41~q\ & ((\inst39|inst42~q\) # (\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~166_combout\);

-- Location: LCCOMB_X16_Y25_N18
\inst42|seven_seg1[3]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~165_combout\ = (!\inst38|inst8~q\ & ((\inst39|inst8~q\ & (!\inst39|inst19~q\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[5]~40_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[5]~40_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~165_combout\);

-- Location: LCCOMB_X15_Y25_N30
\inst42|seven_seg1[3]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~167_combout\ = (\inst42|seven_seg1[3]~165_combout\) # ((\inst42|seven_seg1[3]~166_combout\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg1[3]~166_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~165_combout\,
	combout => \inst42|seven_seg1[3]~167_combout\);

-- Location: LCCOMB_X14_Y21_N26
\inst42|seven_seg1[3]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~171_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst38|inst8~q\)) # (!\inst39|inst41~q\ & ((\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~171_combout\);

-- Location: LCCOMB_X16_Y24_N16
\inst42|seven_seg1[3]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~254_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg1[3]~167_combout\)))) # (!\inst38|inst43~q\ & (!\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~171_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~167_combout\,
	datad => \inst42|seven_seg1[3]~171_combout\,
	combout => \inst42|seven_seg1[3]~254_combout\);

-- Location: LCCOMB_X16_Y24_N4
\inst42|seven_seg1[3]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~184_combout\ = (\inst42|seven_seg1[3]~181_combout\ & (((\inst42|seven_seg1[3]~183_combout\)) # (!\inst38|inst42~q\))) # (!\inst42|seven_seg1[3]~181_combout\ & (\inst38|inst42~q\ & ((\inst42|seven_seg1[3]~254_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~181_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~183_combout\,
	datad => \inst42|seven_seg1[3]~254_combout\,
	combout => \inst42|seven_seg1[3]~184_combout\);

-- Location: LCCOMB_X16_Y24_N2
\inst42|seven_seg1[3]~27\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~27_combout\ = (((\inst39|inst8~q\) # (\inst39|inst19~q\)) # (!\inst38|inst42~q\)) # (!\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[3]~27_combout\);

-- Location: LCCOMB_X16_Y24_N12
\inst42|seven_seg1[3]~261\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~261_combout\ = (\inst38|inst43~q\ & (\inst38|inst8~q\ & ((\inst42|seven_seg1[3]~27_combout\) # (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[3]~27_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~261_combout\);

-- Location: LCCOMB_X16_Y24_N8
\inst42|seven_seg1[3]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~170_combout\ = (\inst39|inst8~q\ & (!\inst38|inst8~q\ & (\inst38|inst43~q\ & \inst42|seven_seg1[5]~40_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[5]~40_combout\,
	combout => \inst42|seven_seg1[3]~170_combout\);

-- Location: LCCOMB_X16_Y24_N28
\inst42|seven_seg1[3]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~172_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg1[3]~171_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~271_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[3]~171_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~271_combout\,
	combout => \inst42|seven_seg1[3]~172_combout\);

-- Location: LCCOMB_X16_Y24_N6
\inst42|seven_seg1[3]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~173_combout\ = (\inst42|seven_seg1[3]~170_combout\) # ((\inst42|seven_seg1[3]~172_combout\) # ((\inst42|seven_seg1[3]~261_combout\ & \inst42|seven_seg1[4]~253_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~261_combout\,
	datab => \inst42|seven_seg1[3]~170_combout\,
	datac => \inst42|seven_seg1[4]~253_combout\,
	datad => \inst42|seven_seg1[3]~172_combout\,
	combout => \inst42|seven_seg1[3]~173_combout\);

-- Location: LCCOMB_X17_Y23_N20
\inst42|seven_seg1[3]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~163_combout\ = (\inst39|inst8~q\ & (!\inst39|inst41~q\ & ((!\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[3]~163_combout\);

-- Location: LCCOMB_X17_Y23_N10
\inst42|Equal0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~16_combout\ = (\inst39|inst42~q\ & \inst39|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|Equal0~16_combout\);

-- Location: LCCOMB_X17_Y23_N22
\inst42|seven_seg1[3]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~162_combout\ = (!\inst38|inst8~q\ & (\inst39|inst8~q\ & ((\inst39|inst19~q\) # (\inst42|Equal0~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|Equal0~16_combout\,
	combout => \inst42|seven_seg1[3]~162_combout\);

-- Location: LCCOMB_X17_Y23_N2
\inst42|seven_seg1[3]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~164_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~162_combout\) # ((\inst42|seven_seg1[3]~163_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[3]~163_combout\,
	datac => \inst42|seven_seg1[3]~162_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~164_combout\);

-- Location: LCCOMB_X16_Y24_N24
\inst42|seven_seg1[3]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~168_combout\ = (\inst42|seven_seg1[3]~164_combout\) # ((\inst42|seven_seg1[3]~167_combout\ & !\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg1[3]~167_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[3]~164_combout\,
	combout => \inst42|seven_seg1[3]~168_combout\);

-- Location: LCCOMB_X16_Y23_N16
\inst42|seven_seg0[6]~269\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~269_combout\ = (\inst39|inst42~q\ & (\inst39|inst19~q\ & (\inst39|inst41~q\ & \inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[6]~269_combout\);

-- Location: LCCOMB_X16_Y23_N14
\inst42|seven_seg1[3]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~160_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~268_combout\) # ((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & (((\inst42|seven_seg0[6]~269_combout\ & !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~268_combout\,
	datab => \inst42|seven_seg0[6]~269_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~160_combout\);

-- Location: LCCOMB_X16_Y23_N22
\inst42|seven_seg1[3]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~161_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~160_combout\ & (\inst42|seven_seg0[6]~270_combout\)) # (!\inst42|seven_seg1[3]~160_combout\ & ((!\inst42|seven_seg0[6]~267_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg1[3]~160_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~270_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~160_combout\,
	datad => \inst42|seven_seg0[6]~267_combout\,
	combout => \inst42|seven_seg1[3]~161_combout\);

-- Location: LCCOMB_X16_Y24_N14
\inst42|seven_seg1[3]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~169_combout\ = (\inst38|inst41~q\ & (((\inst42|seven_seg1[3]~161_combout\) # (\inst38|inst42~q\)))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[3]~168_combout\ & ((!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[3]~168_combout\,
	datac => \inst42|seven_seg1[3]~161_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[3]~169_combout\);

-- Location: LCCOMB_X10_Y26_N12
\inst42|seven_seg1[3]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~155_combout\ = (\inst39|inst42~q\ & (!\inst38|inst8~q\ & (\inst39|inst41~q\ $ (\inst39|inst19~q\)))) # (!\inst39|inst42~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[3]~155_combout\);

-- Location: LCCOMB_X16_Y25_N14
\inst42|seven_seg1[3]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~156_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg1[3]~155_combout\)) # (!\inst39|inst8~q\ & ((\inst42|Equal0~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~155_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|Equal0~2_combout\,
	combout => \inst42|seven_seg1[3]~156_combout\);

-- Location: LCCOMB_X17_Y25_N14
\inst42|seven_seg1[3]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~159_combout\ = (\inst42|seven_seg1[3]~156_combout\) # ((\inst42|seven_seg1[3]~158_combout\ & (\inst42|seven_seg1[3]~157_combout\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~156_combout\,
	datab => \inst42|seven_seg1[3]~158_combout\,
	datac => \inst42|seven_seg1[3]~157_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~159_combout\);

-- Location: LCCOMB_X16_Y24_N20
\inst42|seven_seg1[3]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~174_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[3]~169_combout\ & (\inst42|seven_seg1[3]~173_combout\)) # (!\inst42|seven_seg1[3]~169_combout\ & ((\inst42|seven_seg1[3]~159_combout\))))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg1[3]~169_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~173_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~169_combout\,
	datad => \inst42|seven_seg1[3]~159_combout\,
	combout => \inst42|seven_seg1[3]~174_combout\);

-- Location: LCCOMB_X16_Y24_N22
\inst42|seven_seg1[3]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~185_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[3]~174_combout\))) # (!\inst38|inst19~q\ & (\inst42|seven_seg1[3]~184_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[3]~184_combout\,
	datad => \inst42|seven_seg1[3]~174_combout\,
	combout => \inst42|seven_seg1[3]~185_combout\);

-- Location: LCCOMB_X15_Y26_N20
\inst42|seven_seg1[2]~257\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~257_combout\ = (\inst39|inst43~q\ & (((!\inst38|inst19~q\) # (!\inst38|inst41~q\)) # (!\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[2]~257_combout\);

-- Location: LCCOMB_X15_Y26_N14
\inst42|seven_seg1[2]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~204_combout\ = (\inst42|seven_seg0[0]~98_combout\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\ & \inst42|seven_seg1[2]~257_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg1[2]~257_combout\,
	combout => \inst42|seven_seg1[2]~204_combout\);

-- Location: LCCOMB_X16_Y26_N0
\inst42|seven_seg1[2]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~205_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[2]~204_combout\) # ((!\inst38|inst42~q\ & !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[2]~204_combout\,
	combout => \inst42|seven_seg1[2]~205_combout\);

-- Location: LCCOMB_X19_Y22_N10
\inst42|seven_seg1[2]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~203_combout\ = (\inst39|inst42~q\ & (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\)))) # (!\inst39|inst42~q\ & (\inst38|inst8~q\ $ (((\inst39|inst19~q\) # (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~203_combout\);

-- Location: LCCOMB_X19_Y22_N2
\inst42|seven_seg1[2]~256\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~256_combout\ = (\inst42|seven_seg1[2]~203_combout\ & (\inst38|inst43~q\ & (!\inst38|inst42~q\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~203_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[2]~256_combout\);

-- Location: LCCOMB_X19_Y22_N20
\inst42|seven_seg1[2]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~206_combout\ = (\inst42|seven_seg1[2]~256_combout\) # ((\inst42|seven_seg1[2]~205_combout\ & (\inst39|inst8~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~205_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[2]~256_combout\,
	combout => \inst42|seven_seg1[2]~206_combout\);

-- Location: LCCOMB_X20_Y25_N2
\inst42|seven_seg1[2]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~188_combout\ = (!\inst38|inst8~q\ & (((!\inst39|inst42~q\ & !\inst39|inst41~q\)) # (!\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[2]~188_combout\);

-- Location: LCCOMB_X19_Y22_N12
\inst42|seven_seg1[2]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~186_combout\ = (\inst39|inst41~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst19~q\))) # (!\inst39|inst41~q\ & (\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~186_combout\);

-- Location: LCCOMB_X19_Y22_N6
\inst42|seven_seg1[2]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~187_combout\ = (\inst42|seven_seg1[2]~186_combout\ & (\inst42|Equal0~0_combout\ & (\inst38|inst8~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~186_combout\,
	datab => \inst42|Equal0~0_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[2]~187_combout\);

-- Location: LCCOMB_X20_Y25_N4
\inst42|seven_seg1[5]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~175_combout\ = (!\inst39|inst8~q\ & !\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~175_combout\);

-- Location: LCCOMB_X20_Y23_N16
\inst42|seven_seg1[2]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~189_combout\ = (\inst42|seven_seg1[2]~187_combout\) # ((\inst38|inst42~q\ & (\inst42|seven_seg1[2]~188_combout\ & \inst42|seven_seg1[5]~175_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[2]~188_combout\,
	datac => \inst42|seven_seg1[2]~187_combout\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg1[2]~189_combout\);

-- Location: LCCOMB_X19_Y22_N16
\inst42|seven_seg1[2]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~194_combout\ = (!\inst38|inst8~q\ & !\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~194_combout\);

-- Location: LCCOMB_X19_Y22_N14
\inst42|seven_seg1[1]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~193_combout\ = (!\inst39|inst41~q\ & (!\inst39|inst8~q\ & (!\inst39|inst19~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~193_combout\);

-- Location: LCCOMB_X19_Y22_N26
\inst42|seven_seg1[2]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~191_combout\ = (\inst39|inst19~q\) # ((\inst38|inst8~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~191_combout\);

-- Location: LCCOMB_X19_Y22_N4
\inst42|seven_seg1[2]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~190_combout\ = (\inst39|inst19~q\ & ((\inst38|inst8~q\ & (!\inst39|inst41~q\ & !\inst39|inst42~q\)) # (!\inst38|inst8~q\ & (\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010001101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~190_combout\);

-- Location: LCCOMB_X19_Y22_N28
\inst42|seven_seg1[2]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~192_combout\ = (\inst38|inst43~q\ & (!\inst42|seven_seg1[2]~191_combout\ & ((!\inst39|inst8~q\)))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[2]~190_combout\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~191_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[2]~190_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[2]~192_combout\);

-- Location: LCCOMB_X19_Y22_N30
\inst42|seven_seg1[2]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~195_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg1[2]~192_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[2]~194_combout\ & (\inst42|seven_seg1[1]~193_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[2]~194_combout\,
	datac => \inst42|seven_seg1[1]~193_combout\,
	datad => \inst42|seven_seg1[2]~192_combout\,
	combout => \inst42|seven_seg1[2]~195_combout\);

-- Location: LCCOMB_X16_Y22_N22
\inst42|seven_seg1[2]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~199_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst39|inst8~q\))) # (!\inst39|inst41~q\ & (!\inst39|inst8~q\ & \inst39|inst42~q\)))) # (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & 
-- (\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~199_combout\);

-- Location: LCCOMB_X16_Y22_N12
\inst42|seven_seg1[2]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~198_combout\ = (\inst39|inst19~q\ & (((!\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & (\inst39|inst8~q\ & !\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~198_combout\);

-- Location: LCCOMB_X16_Y22_N8
\inst42|seven_seg1[2]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~200_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[2]~198_combout\))) # (!\inst38|inst8~q\ & (!\inst42|seven_seg1[2]~199_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg1[2]~199_combout\,
	datad => \inst42|seven_seg1[2]~198_combout\,
	combout => \inst42|seven_seg1[2]~200_combout\);

-- Location: LCCOMB_X16_Y22_N24
\inst42|seven_seg1[2]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~196_combout\ = (\inst38|inst8~q\ & (!\inst39|inst19~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\)))) # (!\inst38|inst8~q\ & (((\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~196_combout\);

-- Location: LCCOMB_X16_Y22_N18
\inst42|seven_seg1[2]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~197_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg1[2]~196_combout\)) # (!\inst39|inst8~q\ & (((\inst38|inst8~q\ & \inst42|Equal0~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[2]~196_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|Equal0~11_combout\,
	combout => \inst42|seven_seg1[2]~197_combout\);

-- Location: LCCOMB_X16_Y22_N30
\inst42|seven_seg1[2]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~201_combout\ = (\inst38|inst43~q\ & (\inst38|inst42~q\ & ((\inst42|seven_seg1[2]~197_combout\)))) # (!\inst38|inst43~q\ & (!\inst38|inst42~q\ & (\inst42|seven_seg1[2]~200_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[2]~200_combout\,
	datad => \inst42|seven_seg1[2]~197_combout\,
	combout => \inst42|seven_seg1[2]~201_combout\);

-- Location: LCCOMB_X19_Y22_N24
\inst42|seven_seg1[2]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~202_combout\ = (\inst38|inst19~q\ & (\inst38|inst41~q\)) # (!\inst38|inst19~q\ & ((\inst38|inst41~q\ & (\inst42|seven_seg1[2]~195_combout\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg1[2]~201_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg1[2]~195_combout\,
	datad => \inst42|seven_seg1[2]~201_combout\,
	combout => \inst42|seven_seg1[2]~202_combout\);

-- Location: LCCOMB_X19_Y22_N22
\inst42|seven_seg1[2]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~207_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[2]~202_combout\ & (\inst42|seven_seg1[2]~206_combout\)) # (!\inst42|seven_seg1[2]~202_combout\ & ((\inst42|seven_seg1[2]~189_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg1[2]~202_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[2]~206_combout\,
	datac => \inst42|seven_seg1[2]~189_combout\,
	datad => \inst42|seven_seg1[2]~202_combout\,
	combout => \inst42|seven_seg1[2]~207_combout\);

-- Location: LCCOMB_X17_Y26_N22
\inst42|seven_seg1[1]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~215_combout\ = (\inst42|seven_seg1[5]~93_combout\ & (\inst38|inst42~q\ & (\inst38|inst41~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~93_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~215_combout\);

-- Location: LCCOMB_X17_Y26_N18
\inst42|seven_seg1[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~219_combout\ = (\inst38|inst41~q\ & (!\inst38|inst43~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\)))) # (!\inst38|inst41~q\ & (!\inst39|inst41~q\ & (!\inst39|inst42~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~219_combout\);

-- Location: LCCOMB_X17_Y26_N14
\inst42|seven_seg1[1]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~217_combout\ = (\inst39|inst41~q\ & (\inst39|inst42~q\ & (!\inst38|inst41~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~217_combout\);

-- Location: LCCOMB_X17_Y26_N0
\inst42|seven_seg1[1]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~216_combout\ = (\inst38|inst41~q\ & (!\inst38|inst43~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\)))) # (!\inst38|inst41~q\ & (((\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~216_combout\);

-- Location: LCCOMB_X17_Y26_N4
\inst42|seven_seg1[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~218_combout\ = (\inst39|inst8~q\ & (((\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & ((\inst39|inst19~q\ & ((\inst42|seven_seg1[1]~216_combout\))) # (!\inst39|inst19~q\ & (\inst42|seven_seg1[1]~217_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[1]~217_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[1]~216_combout\,
	combout => \inst42|seven_seg1[1]~218_combout\);

-- Location: LCCOMB_X17_Y26_N8
\inst42|seven_seg1[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~220_combout\ = (\inst42|seven_seg1[1]~218_combout\ & (((\inst42|seven_seg1[1]~219_combout\) # (!\inst39|inst8~q\)))) # (!\inst42|seven_seg1[1]~218_combout\ & (\inst42|seven_seg0[5]~137_combout\ & ((\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~137_combout\,
	datab => \inst42|seven_seg1[1]~219_combout\,
	datac => \inst42|seven_seg1[1]~218_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~220_combout\);

-- Location: LCCOMB_X17_Y26_N26
\inst42|seven_seg1[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~221_combout\ = (\inst42|seven_seg1[1]~215_combout\) # ((!\inst38|inst42~q\ & \inst42|seven_seg1[1]~220_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~215_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~220_combout\,
	combout => \inst42|seven_seg1[1]~221_combout\);

-- Location: LCCOMB_X17_Y26_N16
\inst42|seven_seg1[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~222_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst38|inst41~q\))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~222_combout\);

-- Location: LCCOMB_X17_Y26_N30
\inst42|seven_seg1[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~223_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[5]~137_combout\ & ((\inst39|inst19~q\) # (\inst42|seven_seg1[1]~222_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[5]~137_combout\,
	datad => \inst42|seven_seg1[1]~222_combout\,
	combout => \inst42|seven_seg1[1]~223_combout\);

-- Location: LCCOMB_X17_Y26_N24
\inst42|seven_seg1[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~224_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst38|inst42~q\))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst38|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~224_combout\);

-- Location: LCCOMB_X17_Y26_N6
\inst42|seven_seg1[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~225_combout\ = (\inst39|inst8~q\) # ((\inst38|inst42~q\ & ((\inst39|inst19~q\) # (\inst42|seven_seg1[1]~224_combout\))) # (!\inst38|inst42~q\ & (\inst39|inst19~q\ & \inst42|seven_seg1[1]~224_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[1]~224_combout\,
	combout => \inst42|seven_seg1[1]~225_combout\);

-- Location: LCCOMB_X17_Y26_N12
\inst42|seven_seg1[1]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~226_combout\ = (\inst38|inst42~q\ & ((\inst38|inst41~q\ & (!\inst42|seven_seg1[5]~99_combout\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg1[1]~225_combout\))))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg1[1]~225_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~99_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[1]~225_combout\,
	combout => \inst42|seven_seg1[1]~226_combout\);

-- Location: LCCOMB_X17_Y26_N2
\inst42|seven_seg1[1]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~227_combout\ = (\inst42|seven_seg0[5]~137_combout\ & (!\inst38|inst42~q\ & (\inst42|seven_seg1[1]~223_combout\))) # (!\inst42|seven_seg0[5]~137_combout\ & (((!\inst38|inst42~q\ & \inst42|seven_seg1[1]~223_combout\)) # 
-- (!\inst42|seven_seg1[1]~226_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~137_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~223_combout\,
	datad => \inst42|seven_seg1[1]~226_combout\,
	combout => \inst42|seven_seg1[1]~227_combout\);

-- Location: LCCOMB_X17_Y26_N28
\inst42|seven_seg1[1]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~228_combout\ = (\inst38|inst19~q\ & (\inst38|inst8~q\)) # (!\inst38|inst19~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg1[1]~221_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg1[1]~227_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg1[1]~221_combout\,
	datad => \inst42|seven_seg1[1]~227_combout\,
	combout => \inst42|seven_seg1[1]~228_combout\);

-- Location: LCCOMB_X15_Y23_N12
\inst42|seven_seg1[1]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~210_combout\ = (!\inst38|inst41~q\ & (\inst38|inst43~q\ & ((\inst42|Equal0~12_combout\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~12_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~210_combout\);

-- Location: LCCOMB_X15_Y23_N30
\inst42|seven_seg1[1]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~211_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst39|inst8~q\))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~211_combout\);

-- Location: LCCOMB_X15_Y23_N8
\inst42|seven_seg1[1]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~212_combout\ = (\inst42|seven_seg1[1]~211_combout\ & ((\inst39|inst19~q\) # (\inst38|inst41~q\))) # (!\inst42|seven_seg1[1]~211_combout\ & (\inst39|inst19~q\ & \inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~211_combout\,
	datab => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~212_combout\);

-- Location: LCCOMB_X15_Y23_N26
\inst42|seven_seg1[1]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~213_combout\ = (!\inst38|inst43~q\ & ((\inst38|inst41~q\ & (\inst39|inst8~q\ $ (\inst42|seven_seg1[1]~212_combout\))) # (!\inst38|inst41~q\ & (\inst39|inst8~q\ & \inst42|seven_seg1[1]~212_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[1]~212_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~213_combout\);

-- Location: LCCOMB_X15_Y23_N16
\inst42|seven_seg1[1]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~208_combout\ = (\inst38|inst43~q\) # ((\inst39|inst41~q\ & (\inst39|inst42~q\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~208_combout\);

-- Location: LCCOMB_X15_Y23_N2
\inst42|seven_seg1[1]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~209_combout\ = (!\inst38|inst41~q\ & (!\inst38|inst42~q\ & (!\inst42|seven_seg1[1]~208_combout\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~208_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~209_combout\);

-- Location: LCCOMB_X15_Y23_N24
\inst42|seven_seg1[1]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~214_combout\ = (\inst42|seven_seg1[1]~209_combout\) # ((\inst38|inst42~q\ & ((\inst42|seven_seg1[1]~210_combout\) # (\inst42|seven_seg1[1]~213_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~210_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~213_combout\,
	datad => \inst42|seven_seg1[1]~209_combout\,
	combout => \inst42|seven_seg1[1]~214_combout\);

-- Location: LCCOMB_X17_Y25_N10
\inst42|seven_seg1[1]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~234_combout\ = (\inst42|seven_seg1[3]~39_combout\ & (\inst39|inst43~q\ & (\inst42|seven_seg0[6]~266_combout\ & \inst42|Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~39_combout\,
	datab => \inst39|inst43~q\,
	datac => \inst42|seven_seg0[6]~266_combout\,
	datad => \inst42|Equal0~2_combout\,
	combout => \inst42|seven_seg1[1]~234_combout\);

-- Location: LCCOMB_X17_Y25_N16
\inst42|seven_seg1[1]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~235_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst43~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[1]~235_combout\);

-- Location: LCCOMB_X17_Y25_N12
\inst42|seven_seg1[1]~258\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~258_combout\ = (!\inst39|inst8~q\ & (!\inst38|inst43~q\ & ((\inst42|seven_seg1[1]~234_combout\) # (!\inst42|seven_seg1[1]~235_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~234_combout\,
	datab => \inst42|seven_seg1[1]~235_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~258_combout\);

-- Location: LCCOMB_X17_Y25_N28
\inst42|seven_seg1[1]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~229_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))) # (!\inst39|inst8~q\ & (\inst39|inst41~q\ & \inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[1]~229_combout\);

-- Location: LCCOMB_X17_Y25_N26
\inst42|seven_seg1[1]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~230_combout\ = (\inst39|inst8~q\ & (!\inst38|inst43~q\ & ((\inst42|seven_seg1[1]~229_combout\) # (\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (!\inst42|seven_seg1[1]~229_combout\ & (!\inst39|inst19~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[1]~229_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~230_combout\);

-- Location: LCCOMB_X17_Y25_N8
\inst42|seven_seg1[1]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~231_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\)))) # (!\inst38|inst41~q\ & (((\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[1]~231_combout\);

-- Location: LCCOMB_X17_Y25_N6
\inst42|seven_seg1[1]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~232_combout\ = (\inst38|inst41~q\ & (\inst39|inst8~q\ & (\inst42|seven_seg1[1]~231_combout\))) # (!\inst38|inst41~q\ & (!\inst39|inst8~q\ & (!\inst42|seven_seg1[1]~231_combout\ & \inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[1]~231_combout\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg1[1]~232_combout\);

-- Location: LCCOMB_X17_Y25_N24
\inst42|seven_seg1[1]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~233_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[1]~232_combout\ & (\inst38|inst41~q\ $ (!\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~232_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~233_combout\);

-- Location: LCCOMB_X17_Y25_N2
\inst42|seven_seg1[1]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~236_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[1]~233_combout\ & (\inst42|seven_seg1[1]~258_combout\)) # (!\inst42|seven_seg1[1]~233_combout\ & ((\inst42|seven_seg1[1]~230_combout\))))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg1[1]~233_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~258_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~230_combout\,
	datad => \inst42|seven_seg1[1]~233_combout\,
	combout => \inst42|seven_seg1[1]~236_combout\);

-- Location: LCCOMB_X17_Y26_N10
\inst42|seven_seg1[1]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~237_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[1]~228_combout\ & ((\inst42|seven_seg1[1]~236_combout\))) # (!\inst42|seven_seg1[1]~228_combout\ & (\inst42|seven_seg1[1]~214_combout\)))) # (!\inst38|inst19~q\ & 
-- (\inst42|seven_seg1[1]~228_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[1]~228_combout\,
	datac => \inst42|seven_seg1[1]~214_combout\,
	datad => \inst42|seven_seg1[1]~236_combout\,
	combout => \inst42|seven_seg1[1]~237_combout\);

-- Location: LCCOMB_X15_Y25_N12
\inst42|seven_seg1[3]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~242_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~165_combout\) # ((!\inst39|inst8~q\ & \inst42|seven_seg1[3]~166_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~166_combout\,
	datad => \inst42|seven_seg1[3]~165_combout\,
	combout => \inst42|seven_seg1[3]~242_combout\);

-- Location: LCCOMB_X15_Y23_N28
\inst42|seven_seg1[3]~260\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~260_combout\ = (\inst39|inst8~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~260_combout\);

-- Location: LCCOMB_X16_Y23_N24
\inst42|seven_seg1[3]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~245_combout\ = ((!\inst38|inst8~q\ & (\inst42|seven_seg1[3]~260_combout\ & \inst42|Equal0~12_combout\))) # (!\inst42|seven_seg1[3]~180_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[3]~260_combout\,
	datac => \inst42|seven_seg1[3]~180_combout\,
	datad => \inst42|Equal0~12_combout\,
	combout => \inst42|seven_seg1[3]~245_combout\);

-- Location: LCCOMB_X16_Y23_N8
\inst42|seven_seg1[3]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~243_combout\ = (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~243_combout\);

-- Location: LCCOMB_X16_Y23_N18
\inst42|seven_seg1[3]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~244_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~243_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~272_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[6]~272_combout\,
	datac => \inst42|seven_seg1[3]~243_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~244_combout\);

-- Location: LCCOMB_X16_Y23_N10
\inst42|seven_seg1[0]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~246_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst41~q\ & ((\inst42|seven_seg1[3]~244_combout\))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[3]~245_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[3]~245_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[3]~244_combout\,
	combout => \inst42|seven_seg1[0]~246_combout\);

-- Location: LCCOMB_X16_Y25_N2
\inst42|seven_seg1[0]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~247_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[0]~246_combout\ & ((\inst42|seven_seg1[3]~183_combout\))) # (!\inst42|seven_seg1[0]~246_combout\ & (\inst42|seven_seg1[3]~242_combout\)))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg1[0]~246_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~242_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~183_combout\,
	datad => \inst42|seven_seg1[0]~246_combout\,
	combout => \inst42|seven_seg1[0]~247_combout\);

-- Location: LCCOMB_X20_Y25_N28
\inst42|seven_seg1[3]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~239_combout\ = (!\inst38|inst8~q\ & (!\inst39|inst42~q\ & (\inst42|seven_seg1[5]~175_combout\ & \inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg1[5]~175_combout\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg1[3]~239_combout\);

-- Location: LCCOMB_X16_Y24_N30
\inst42|seven_seg1[3]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~240_combout\ = (\inst42|seven_seg1[3]~170_combout\) # ((\inst42|seven_seg1[3]~239_combout\) # ((\inst42|seven_seg1[3]~261_combout\ & \inst42|seven_seg1[4]~253_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~261_combout\,
	datab => \inst42|seven_seg1[4]~253_combout\,
	datac => \inst42|seven_seg1[3]~170_combout\,
	datad => \inst42|seven_seg1[3]~239_combout\,
	combout => \inst42|seven_seg1[3]~240_combout\);

-- Location: LCCOMB_X16_Y24_N0
\inst42|seven_seg1[0]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~238_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst41~q\ & ((\inst42|seven_seg1[3]~161_combout\))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[3]~164_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~164_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~161_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[0]~238_combout\);

-- Location: LCCOMB_X16_Y25_N4
\inst42|seven_seg1[0]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~241_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[0]~238_combout\ & (\inst42|seven_seg1[3]~240_combout\)) # (!\inst42|seven_seg1[0]~238_combout\ & ((\inst42|seven_seg1[3]~156_combout\))))) # (!\inst38|inst42~q\ & 
-- (((\inst42|seven_seg1[0]~238_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~240_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~156_combout\,
	datad => \inst42|seven_seg1[0]~238_combout\,
	combout => \inst42|seven_seg1[0]~241_combout\);

-- Location: LCCOMB_X15_Y25_N26
\inst42|seven_seg1[0]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~248_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[0]~241_combout\))) # (!\inst38|inst19~q\ & (\inst42|seven_seg1[0]~247_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[0]~247_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[0]~241_combout\,
	combout => \inst42|seven_seg1[0]~248_combout\);

-- Location: LCCOMB_X17_Y23_N4
\inst42|Equal0~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~17_combout\ = (\inst39|inst43~q\ & (\inst42|Equal0~10_combout\ & (\inst42|Equal0~1_combout\ & \inst42|Equal0~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|Equal0~10_combout\,
	datac => \inst42|Equal0~1_combout\,
	datad => \inst42|Equal0~16_combout\,
	combout => \inst42|Equal0~17_combout\);

-- Location: LCCOMB_X17_Y23_N28
\inst42|seven_seg2[6]~35\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~35_combout\ = (\inst39|inst19~q\) # ((\inst39|inst43~q\ & (\inst39|inst42~q\ & \inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg2[6]~35_combout\);

-- Location: LCCOMB_X17_Y23_N6
\inst42|seven_seg2[6]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~36_combout\ = (\inst38|inst43~q\ & (!\inst42|Equal0~17_combout\ & ((\inst39|inst8~q\) # (\inst42|seven_seg2[6]~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~17_combout\,
	datad => \inst42|seven_seg2[6]~35_combout\,
	combout => \inst42|seven_seg2[6]~36_combout\);

-- Location: LCCOMB_X19_Y23_N12
\inst42|seven_seg2[6]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~75_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg2[6]~36_combout\ & ((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & (((!\inst38|inst43~q\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[6]~36_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[6]~75_combout\);

-- Location: LCCOMB_X10_Y25_N4
\inst42|seven_seg2[6]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~38_combout\ = (\inst38|inst42~q\ & ((\inst38|inst43~q\) # ((\inst39|inst8~q\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg2[6]~38_combout\);

-- Location: LCCOMB_X19_Y23_N30
\inst42|seven_seg2[6]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~37_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\) # ((\inst38|inst43~q\ & \inst42|Equal0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|Equal0~3_combout\,
	combout => \inst42|seven_seg2[6]~37_combout\);

-- Location: LCCOMB_X19_Y23_N16
\inst42|seven_seg2[6]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~39_combout\ = (\inst38|inst8~q\ & (((\inst42|seven_seg2[6]~37_combout\)))) # (!\inst38|inst8~q\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg2[6]~38_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[6]~38_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg2[6]~37_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg2[6]~39_combout\);

-- Location: LCCOMB_X19_Y23_N26
\inst42|seven_seg2[6]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~40_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg2[6]~75_combout\ & (\inst38|inst8~q\))) # (!\inst38|inst19~q\ & (((\inst42|seven_seg2[6]~39_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[6]~75_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg2[6]~39_combout\,
	combout => \inst42|seven_seg2[6]~40_combout\);

-- Location: LCCOMB_X20_Y25_N10
\inst42|seven_seg2[5]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~46_combout\ = (\inst38|inst8~q\) # ((\inst39|inst8~q\ & (\inst42|seven_seg0[0]~98_combout\ & !\inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg2[5]~46_combout\);

-- Location: LCCOMB_X19_Y23_N8
\inst42|Equal0~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~13_combout\ = (\inst42|Equal0~6_combout\ & (\inst42|Equal0~3_combout\ & (\inst42|Equal0~5_combout\ & !\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~6_combout\,
	datab => \inst42|Equal0~3_combout\,
	datac => \inst42|Equal0~5_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|Equal0~13_combout\);

-- Location: LCCOMB_X19_Y23_N20
\inst42|seven_seg2[5]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~43_combout\ = (\inst38|inst19~q\ & (((\inst38|inst8~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst8~q\ & ((\inst42|seven_seg2[6]~37_combout\))) # (!\inst38|inst8~q\ & (\inst42|Equal0~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|Equal0~13_combout\,
	datac => \inst42|seven_seg2[6]~37_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg2[5]~43_combout\);

-- Location: LCCOMB_X20_Y23_N18
\inst42|seven_seg2[4]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~34_combout\ = (!\inst38|inst42~q\ & (!\inst38|inst41~q\ & !\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[4]~34_combout\);

-- Location: LCCOMB_X19_Y23_N22
\inst42|seven_seg2[5]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~44_combout\ = (\inst42|Equal0~13_combout\) # ((\inst42|seven_seg2[4]~34_combout\ & ((!\inst42|Equal0~3_combout\) # (!\inst42|Equal0~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~7_combout\,
	datab => \inst42|Equal0~3_combout\,
	datac => \inst42|Equal0~13_combout\,
	datad => \inst42|seven_seg2[4]~34_combout\,
	combout => \inst42|seven_seg2[5]~44_combout\);

-- Location: LCCOMB_X7_Y22_N24
\inst42|seven_seg2[5]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~41_combout\ = (\inst39|inst8~q\ & (\inst38|inst43~q\ & ((\inst39|inst19~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[5]~41_combout\);

-- Location: LCCOMB_X19_Y23_N6
\inst42|seven_seg2[5]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~42_combout\ = (\inst42|Equal0~13_combout\ & (((!\inst38|inst41~q\) # (!\inst38|inst42~q\)) # (!\inst42|seven_seg2[5]~41_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~41_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|Equal0~13_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[5]~42_combout\);

-- Location: LCCOMB_X19_Y23_N24
\inst42|seven_seg2[5]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~45_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg2[5]~43_combout\ & (\inst42|seven_seg2[5]~44_combout\)) # (!\inst42|seven_seg2[5]~43_combout\ & ((\inst42|seven_seg2[5]~42_combout\))))) # (!\inst38|inst19~q\ & 
-- (\inst42|seven_seg2[5]~43_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[5]~43_combout\,
	datac => \inst42|seven_seg2[5]~44_combout\,
	datad => \inst42|seven_seg2[5]~42_combout\,
	combout => \inst42|seven_seg2[5]~45_combout\);

-- Location: LCCOMB_X20_Y25_N12
\inst42|seven_seg2[5]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~47_combout\ = (((!\inst38|inst42~q\ & \inst42|seven_seg1[5]~175_combout\)) # (!\inst38|inst41~q\)) # (!\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~175_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[5]~47_combout\);

-- Location: LCCOMB_X20_Y25_N14
\inst42|seven_seg2[5]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~48_combout\ = (\inst42|seven_seg2[5]~46_combout\ & ((\inst42|seven_seg2[5]~45_combout\) # ((!\inst38|inst8~q\ & \inst42|seven_seg2[5]~47_combout\)))) # (!\inst42|seven_seg2[5]~46_combout\ & (((!\inst38|inst8~q\ & 
-- \inst42|seven_seg2[5]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~46_combout\,
	datab => \inst42|seven_seg2[5]~45_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg2[5]~47_combout\,
	combout => \inst42|seven_seg2[5]~48_combout\);

-- Location: LCCOMB_X20_Y23_N22
\inst42|seven_seg2[4]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~56_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg2[6]~36_combout\)))) # (!\inst38|inst42~q\ & (\inst42|Equal0~9_combout\ & ((\inst42|seven_seg1[5]~175_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst42|seven_seg2[6]~36_combout\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg2[4]~56_combout\);

-- Location: LCCOMB_X20_Y23_N24
\inst42|seven_seg2[4]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~55_combout\ = (\inst38|inst19~q\ & (\inst38|inst8~q\ & \inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[4]~55_combout\);

-- Location: LCCOMB_X20_Y23_N28
\inst42|seven_seg2[4]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~57_combout\ = (\inst42|seven_seg2[4]~34_combout\) # ((!\inst42|seven_seg2[4]~56_combout\ & (\inst42|seven_seg2[4]~55_combout\ & \inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[4]~56_combout\,
	datab => \inst42|seven_seg2[4]~55_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg2[4]~34_combout\,
	combout => \inst42|seven_seg2[4]~57_combout\);

-- Location: LCCOMB_X19_Y22_N18
\inst42|seven_seg2[4]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~51_combout\ = ((!\inst38|inst43~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst8~q\)))) # (!\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[4]~51_combout\);

-- Location: LCCOMB_X19_Y22_N8
\inst42|seven_seg2[4]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~50_combout\ = (\inst39|inst41~q\ & (\inst39|inst8~q\ & (\inst39|inst19~q\ & \inst42|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|Equal0~0_combout\,
	combout => \inst42|seven_seg2[4]~50_combout\);

-- Location: LCCOMB_X19_Y22_N0
\inst42|seven_seg2[4]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~52_combout\ = (\inst42|seven_seg2[4]~50_combout\) # ((\inst38|inst41~q\ & (\inst38|inst42~q\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg2[4]~51_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg2[4]~51_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg2[4]~50_combout\,
	combout => \inst42|seven_seg2[4]~52_combout\);

-- Location: LCCOMB_X20_Y23_N20
\inst42|seven_seg2[2]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~53_combout\ = (\inst38|inst42~q\ & (\inst38|inst41~q\ & ((\inst39|inst19~q\) # (!\inst42|seven_seg1[5]~175_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg2[2]~53_combout\);

-- Location: LCCOMB_X20_Y23_N26
\inst42|seven_seg2[4]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~54_combout\ = (\inst38|inst19~q\ & (\inst38|inst8~q\)) # (!\inst38|inst19~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg2[4]~52_combout\)) # (!\inst38|inst8~q\ & ((!\inst42|seven_seg2[2]~53_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg2[4]~52_combout\,
	datad => \inst42|seven_seg2[2]~53_combout\,
	combout => \inst42|seven_seg2[4]~54_combout\);

-- Location: LCCOMB_X20_Y23_N4
\inst42|seven_seg1[1]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~249_combout\ = (!\inst38|inst41~q\ & !\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst41~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~249_combout\);

-- Location: LCCOMB_X20_Y23_N2
\inst42|seven_seg2[4]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~49_combout\ = ((\inst38|inst43~q\ & ((\inst42|Equal0~11_combout\) # (\inst39|inst8~q\)))) # (!\inst42|seven_seg1[1]~249_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111110001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~11_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[1]~249_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[4]~49_combout\);

-- Location: LCCOMB_X20_Y23_N30
\inst42|seven_seg2[4]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~58_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg2[4]~54_combout\ & (\inst42|seven_seg2[4]~57_combout\)) # (!\inst42|seven_seg2[4]~54_combout\ & ((\inst42|seven_seg2[4]~49_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg2[4]~54_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[4]~57_combout\,
	datac => \inst42|seven_seg2[4]~54_combout\,
	datad => \inst42|seven_seg2[4]~49_combout\,
	combout => \inst42|seven_seg2[4]~58_combout\);

-- Location: LCCOMB_X20_Y25_N8
\inst42|seven_seg2[3]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~59_combout\ = (!\inst39|inst19~q\ & (!\inst39|inst8~q\ & !\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[3]~59_combout\);

-- Location: LCCOMB_X20_Y25_N26
\inst42|seven_seg2[3]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~60_combout\ = (!\inst38|inst19~q\ & (((\inst42|seven_seg2[3]~59_combout\) # (!\inst38|inst41~q\)) # (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg2[3]~59_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[3]~60_combout\);

-- Location: LCCOMB_X20_Y25_N22
\inst42|seven_seg2[3]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~62_combout\ = (\inst38|inst19~q\ & (\inst38|inst41~q\ & (\inst38|inst43~q\ $ (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[3]~62_combout\);

-- Location: LCCOMB_X20_Y25_N20
\inst42|seven_seg2[3]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~63_combout\ = (\inst38|inst8~q\) # ((\inst42|seven_seg2[3]~62_combout\ & (\inst42|seven_seg0[0]~98_combout\ & !\inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[3]~62_combout\,
	datab => \inst42|seven_seg0[0]~98_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg2[3]~63_combout\);

-- Location: LCCOMB_X20_Y25_N0
\inst42|seven_seg2[3]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~61_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\ & (\inst38|inst19~q\)) # (!\inst38|inst42~q\ & ((!\inst42|seven_seg1[5]~175_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~175_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[3]~61_combout\);

-- Location: LCCOMB_X20_Y25_N30
\inst42|seven_seg2[3]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~64_combout\ = (\inst42|seven_seg2[3]~63_combout\ & (((\inst42|seven_seg2[5]~45_combout\)))) # (!\inst42|seven_seg2[3]~63_combout\ & ((\inst42|seven_seg2[3]~60_combout\) # ((\inst42|seven_seg2[3]~61_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[3]~60_combout\,
	datab => \inst42|seven_seg2[3]~63_combout\,
	datac => \inst42|seven_seg2[5]~45_combout\,
	datad => \inst42|seven_seg2[3]~61_combout\,
	combout => \inst42|seven_seg2[3]~64_combout\);

-- Location: LCCOMB_X20_Y23_N12
\inst42|seven_seg2[2]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~65_combout\ = (\inst38|inst43~q\ & ((\inst39|inst8~q\) # ((\inst39|inst19~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[2]~65_combout\);

-- Location: LCCOMB_X20_Y23_N10
\inst42|seven_seg2[2]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~66_combout\ = (!\inst38|inst19~q\ & ((\inst42|seven_seg2[2]~53_combout\) # ((!\inst38|inst41~q\ & !\inst42|seven_seg2[2]~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[2]~53_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg2[2]~65_combout\,
	combout => \inst42|seven_seg2[2]~66_combout\);

-- Location: LCCOMB_X17_Y23_N14
\inst42|seven_seg2[2]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~78_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst43~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg2[2]~78_combout\);

-- Location: LCCOMB_X20_Y23_N8
\inst42|seven_seg2[2]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~67_combout\ = ((\inst42|Equal0~7_combout\ & (\inst39|inst19~q\ & !\inst39|inst41~q\))) # (!\inst42|seven_seg2[2]~78_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111110001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~7_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg2[2]~78_combout\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg2[2]~67_combout\);

-- Location: LCCOMB_X20_Y23_N14
\inst42|seven_seg2[2]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~68_combout\ = (((\inst42|seven_seg2[2]~67_combout\ & !\inst39|inst8~q\)) # (!\inst38|inst43~q\)) # (!\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg2[2]~67_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[2]~68_combout\);

-- Location: LCCOMB_X20_Y23_N0
\inst42|seven_seg2[2]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~69_combout\ = (!\inst38|inst8~q\ & ((\inst42|seven_seg2[2]~66_combout\) # ((\inst42|seven_seg2[2]~68_combout\ & \inst42|seven_seg1[1]~249_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[2]~66_combout\,
	datab => \inst42|seven_seg2[2]~68_combout\,
	datac => \inst42|seven_seg1[1]~249_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg2[2]~69_combout\);

-- Location: LCCOMB_X17_Y22_N2
\inst42|seven_seg2[1]~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~18_combout\ = (((\inst38|inst8~q\ & !\inst38|inst19~q\)) # (!\inst39|inst43~q\)) # (!\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst43~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[1]~18_combout\);

-- Location: LCCOMB_X17_Y22_N0
\inst42|seven_seg2[1]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~15_combout\ = (((!\inst39|inst41~q\ & \inst42|seven_seg2[1]~18_combout\)) # (!\inst39|inst8~q\)) # (!\inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg2[1]~18_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[1]~15_combout\);

-- Location: LCCOMB_X19_Y23_N10
\inst42|seven_seg2[1]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~79_combout\ = ((!\inst38|inst42~q\ & ((\inst42|seven_seg2[1]~15_combout\) # (!\inst38|inst43~q\)))) # (!\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg2[1]~15_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[1]~79_combout\);

-- Location: LCCOMB_X19_Y23_N18
\inst42|seven_seg2[1]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~70_combout\ = (\inst38|inst42~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg2[5]~41_combout\))) # (!\inst38|inst19~q\ & (\inst42|seven_seg2[2]~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[2]~65_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg2[5]~41_combout\,
	combout => \inst42|seven_seg2[1]~70_combout\);

-- Location: LCCOMB_X19_Y23_N0
\inst42|seven_seg2[1]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~71_combout\ = (\inst38|inst8~q\ & (((\inst38|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst42|seven_seg2[1]~70_combout\ & ((!\inst38|inst41~q\) # (!\inst38|inst19~q\))) # (!\inst42|seven_seg2[1]~70_combout\ & ((\inst38|inst19~q\) # 
-- (\inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001111110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[1]~70_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[1]~71_combout\);

-- Location: LCCOMB_X19_Y23_N2
\inst42|Equal0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~14_combout\ = (!\inst38|inst19~q\ & (\inst42|seven_seg0[6]~71_combout\ & (\inst42|Equal0~2_combout\ & \inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[6]~71_combout\,
	datac => \inst42|Equal0~2_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|Equal0~14_combout\);

-- Location: LCCOMB_X19_Y23_N4
\inst42|Equal0~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~15_combout\ = (\inst39|inst43~q\ & (\inst39|inst42~q\ & (\inst42|Equal0~0_combout\ & \inst42|Equal0~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst42|Equal0~0_combout\,
	datad => \inst42|Equal0~14_combout\,
	combout => \inst42|Equal0~15_combout\);

-- Location: LCCOMB_X19_Y23_N14
\inst42|seven_seg2[1]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~72_combout\ = (\inst42|seven_seg2[1]~71_combout\ & (((\inst42|Equal0~15_combout\ & \inst38|inst8~q\)))) # (!\inst42|seven_seg2[1]~71_combout\ & ((\inst42|seven_seg2[1]~79_combout\) # ((!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[1]~79_combout\,
	datab => \inst42|seven_seg2[1]~71_combout\,
	datac => \inst42|Equal0~15_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg2[1]~72_combout\);

-- Location: LCCOMB_X10_Y25_N2
\inst42|seven_seg2[0]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~77_combout\ = (((!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst8~q\)) # (!\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg2[0]~77_combout\);

-- Location: LCCOMB_X10_Y25_N0
\inst42|seven_seg2[0]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~76_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg2[0]~77_combout\)))) # (!\inst38|inst42~q\ & ((\inst38|inst43~q\) # ((\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg2[0]~77_combout\,
	combout => \inst42|seven_seg2[0]~76_combout\);

-- Location: LCCOMB_X10_Y25_N14
\inst42|seven_seg2[0]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~73_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg2[0]~76_combout\ & ((\inst38|inst41~q\)))) # (!\inst38|inst19~q\ & (((!\inst38|inst41~q\) # (!\inst42|seven_seg2[6]~38_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[0]~76_combout\,
	datac => \inst42|seven_seg2[6]~38_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[0]~73_combout\);

-- Location: LCCOMB_X11_Y25_N14
\inst42|seven_seg2[0]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~74_combout\ = (!\inst38|inst8~q\ & \inst42|seven_seg2[0]~73_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg2[0]~73_combout\,
	combout => \inst42|seven_seg2[0]~74_combout\);

-- Location: LCCOMB_X20_Y23_N6
\inst42|seven_seg3[2]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg3[2]~0_combout\ = ((!\inst42|seven_seg2[4]~55_combout\) # (!\inst42|seven_seg2[6]~36_combout\)) # (!\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg2[6]~36_combout\,
	datad => \inst42|seven_seg2[4]~55_combout\,
	combout => \inst42|seven_seg3[2]~0_combout\);

ww_D0(6) <= \D0[6]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;

ww_OUTPUT(9) <= \OUTPUT[9]~output_o\;

ww_OUTPUT(8) <= \OUTPUT[8]~output_o\;

ww_OUTPUT(7) <= \OUTPUT[7]~output_o\;

ww_OUTPUT(6) <= \OUTPUT[6]~output_o\;

ww_OUTPUT(5) <= \OUTPUT[5]~output_o\;

ww_OUTPUT(4) <= \OUTPUT[4]~output_o\;

ww_OUTPUT(3) <= \OUTPUT[3]~output_o\;

ww_OUTPUT(2) <= \OUTPUT[2]~output_o\;

ww_OUTPUT(1) <= \OUTPUT[1]~output_o\;

ww_OUTPUT(0) <= \OUTPUT[0]~output_o\;
END structure;


