﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace image_to_file
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap bm = new Bitmap(args[0]);
            StreamWriter sw = new StreamWriter(args[1]);
            sw.WriteLine(bm.Width + " " + bm.Height);
            for (int i = 0; i < bm.Height; i++)
            {
                for (int j = 0; j < bm.Width; j++)
                {
                    Color c = bm.GetPixel(j, i);
                    if (j == bm.Width - 1) sw.Write(c.R + " " + c.G + " " + c.B);
                    else sw.Write(c.R + " " + c.G + " " + c.B + "\t");
                }
                sw.Write(Environment.NewLine);
            }
            sw.Close();
            Environment.Exit(0);
        }
    }
}
