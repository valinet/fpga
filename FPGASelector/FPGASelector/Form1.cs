﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FPGASelector
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        public Form1()
        {
            InitializeComponent();
            SetForegroundWindow(FindWindow(null, "New project Wizard"));
            for (int i = 1; i <= Convert.ToInt32(Environment.GetCommandLineArgs()[1]); i++)
                SendKeys.SendWait("{DOWN}");
            Environment.Exit(0);
        }
    }
}