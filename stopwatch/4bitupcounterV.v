module V4bitupcounterV (input clock, input reset, output reg [3:0] number, output reg ripple);

initial ripple = 0;

always @(posedge (clock) or posedge(reset)) begin
	if (reset) begin
		number = 0;
		ripple = 0;
	end else if (number < 9) begin
		number = number + 1;
		ripple = 0;
	end else begin
		number = 0;
		ripple = 1;
	end
end
endmodule