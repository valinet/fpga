// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II 64-Bit"
// VERSION		"Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"
// CREATED		"Thu Mar 17 22:33:18 2016"

module \4bitupcounter (
	CLOCK,
	RIPPLE,
	T
);


input wire	CLOCK;
output wire	RIPPLE;
output wire	[3:0] T;

reg	Q0;
reg	Q1;
reg	Q2;
reg	Q3;
wire	[3:0] T_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_13;

assign	RIPPLE = SYNTHESIZED_WIRE_14;




always@(posedge CLOCK or negedge SYNTHESIZED_WIRE_14)
begin
if (!SYNTHESIZED_WIRE_14)
	begin
	Q0 <= 0;
	end
else
	begin
	Q0 <= SYNTHESIZED_WIRE_15;
	end
end

assign	SYNTHESIZED_WIRE_4 = SYNTHESIZED_WIRE_2 & Q1 & SYNTHESIZED_WIRE_3 & Q3;

assign	SYNTHESIZED_WIRE_2 =  ~Q0;

assign	SYNTHESIZED_WIRE_3 =  ~Q2;

assign	T_ALTERA_SYNTHESIZED[0] = Q0;


assign	T_ALTERA_SYNTHESIZED[1] = Q1;


assign	T_ALTERA_SYNTHESIZED[2] = Q2;


assign	T_ALTERA_SYNTHESIZED[3] = Q3;


assign	SYNTHESIZED_WIRE_14 =  ~SYNTHESIZED_WIRE_4;

assign	SYNTHESIZED_WIRE_15 =  ~Q0;

assign	SYNTHESIZED_WIRE_16 =  ~Q1;

assign	SYNTHESIZED_WIRE_17 =  ~Q2;


always@(posedge SYNTHESIZED_WIRE_15 or negedge SYNTHESIZED_WIRE_14)
begin
if (!SYNTHESIZED_WIRE_14)
	begin
	Q1 <= 0;
	end
else
	begin
	Q1 <= SYNTHESIZED_WIRE_16;
	end
end


always@(posedge SYNTHESIZED_WIRE_16 or negedge SYNTHESIZED_WIRE_14)
begin
if (!SYNTHESIZED_WIRE_14)
	begin
	Q2 <= 0;
	end
else
	begin
	Q2 <= SYNTHESIZED_WIRE_17;
	end
end


always@(posedge SYNTHESIZED_WIRE_17 or negedge SYNTHESIZED_WIRE_14)
begin
if (!SYNTHESIZED_WIRE_14)
	begin
	Q3 <= 0;
	end
else
	begin
	Q3 <= SYNTHESIZED_WIRE_13;
	end
end

assign	SYNTHESIZED_WIRE_13 =  ~Q3;

assign	T = T_ALTERA_SYNTHESIZED;

endmodule
