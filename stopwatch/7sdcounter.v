module bcd_7seg_decoder(input [3:0] bcd, output reg [6:0] seven_seg0);

always @(*)
if (bcd == 4'b0000) begin
		seven_seg0 = 7'b1000000;
end else if (bcd == 4'b0001) begin
		seven_seg0 = 7'b1111001;
end else if (bcd == 4'b0010) begin
		seven_seg0 = 7'b0100100; 
end else if (bcd == 4'b0011) begin
		seven_seg0 = 7'b0110000; 
end else if (bcd == 4'b0100) begin
		seven_seg0 = 7'b0011001; 
end else if (bcd == 4'b0101) begin
		seven_seg0 = 7'b0010010; 
end else if (bcd == 4'b0110) begin
		seven_seg0 = 7'b0000010; 
end else if (bcd == 4'b0111) begin
		seven_seg0 = 7'b1111000; 
end else if (bcd == 4'b1000) begin
		seven_seg0 = 7'b0000000; 
end else if (bcd == 4'b1001) begin
		seven_seg0 = 7'b0010000; 
end
endmodule