-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "03/17/2016 23:34:06"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	stopwatch IS
    PORT (
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	CLOCK : IN std_logic;
	BUTTON1 : IN std_logic;
	BUTTON0 : IN std_logic;
	BUTTON2 : IN std_logic;
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0)
	);
END stopwatch;

-- Design Ports Information
-- D0[6]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BUTTON2	=>  Location: PIN_F1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BUTTON0	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BUTTON1	=>  Location: PIN_G3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF stopwatch IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_CLOCK : std_logic;
SIGNAL ww_BUTTON1 : std_logic;
SIGNAL ww_BUTTON0 : std_logic;
SIGNAL ww_BUTTON2 : std_logic;
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_CLKSELECT_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \inst15|inst|ripple~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst15|inst1|ripple~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst15|inst2|ripple~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst29~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst1|inst18~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst5|inst3~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst7|inst18~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst9|inst18~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst6|inst18~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst|inst8|inst18~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \BUTTON2~input_o\ : std_logic;
SIGNAL \BUTTON1~input_o\ : std_logic;
SIGNAL \inst16~0_combout\ : std_logic;
SIGNAL \inst16~feeder_combout\ : std_logic;
SIGNAL \inst16~q\ : std_logic;
SIGNAL \CLOCK~input_o\ : std_logic;
SIGNAL \inst|inst13|altctrlclk_altclkctrl_uhi_component|wire_clkctrl1_outclk\ : std_logic;
SIGNAL \inst|inst|inst3~0_combout\ : std_logic;
SIGNAL \inst|inst|inst3~feeder_combout\ : std_logic;
SIGNAL \inst|inst|inst3~q\ : std_logic;
SIGNAL \inst|inst2|inst3~0_combout\ : std_logic;
SIGNAL \inst|inst2|inst3~feeder_combout\ : std_logic;
SIGNAL \inst|inst2|inst3~q\ : std_logic;
SIGNAL \inst|inst3|inst3~0_combout\ : std_logic;
SIGNAL \inst|inst3|inst3~q\ : std_logic;
SIGNAL \inst|inst4|inst3~0_combout\ : std_logic;
SIGNAL \inst|inst4|inst3~feeder_combout\ : std_logic;
SIGNAL \inst|inst4|inst3~q\ : std_logic;
SIGNAL \inst|inst5|inst3~0_combout\ : std_logic;
SIGNAL \inst|inst5|inst3~q\ : std_logic;
SIGNAL \inst|inst5|inst3~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst1|inst16~combout\ : std_logic;
SIGNAL \inst|inst1|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst1|inst8~q\ : std_logic;
SIGNAL \inst|inst1|inst10~combout\ : std_logic;
SIGNAL \inst|inst1|inst~q\ : std_logic;
SIGNAL \inst|inst1|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst1|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst1|inst7~q\ : std_logic;
SIGNAL \inst|inst1|inst9~q\ : std_logic;
SIGNAL \inst|inst1|inst18~combout\ : std_logic;
SIGNAL \inst|inst1|inst18~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst6|inst16~combout\ : std_logic;
SIGNAL \inst|inst6|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst6|inst8~q\ : std_logic;
SIGNAL \inst|inst6|inst10~combout\ : std_logic;
SIGNAL \inst|inst6|inst~q\ : std_logic;
SIGNAL \inst|inst6|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst6|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst6|inst7~q\ : std_logic;
SIGNAL \inst|inst6|inst9~q\ : std_logic;
SIGNAL \inst|inst6|inst18~combout\ : std_logic;
SIGNAL \inst|inst6|inst18~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst7|inst16~combout\ : std_logic;
SIGNAL \inst|inst7|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst7|inst8~q\ : std_logic;
SIGNAL \inst|inst7|inst10~combout\ : std_logic;
SIGNAL \inst|inst7|inst~q\ : std_logic;
SIGNAL \inst|inst7|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst7|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst7|inst7~q\ : std_logic;
SIGNAL \inst|inst7|inst9~q\ : std_logic;
SIGNAL \inst|inst7|inst18~combout\ : std_logic;
SIGNAL \inst|inst7|inst18~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst8|inst16~combout\ : std_logic;
SIGNAL \inst|inst8|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst8|inst8~q\ : std_logic;
SIGNAL \inst|inst8|inst10~combout\ : std_logic;
SIGNAL \inst|inst8|inst~q\ : std_logic;
SIGNAL \inst|inst8|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst8|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst8|inst7~q\ : std_logic;
SIGNAL \inst|inst8|inst9~q\ : std_logic;
SIGNAL \inst|inst8|inst18~combout\ : std_logic;
SIGNAL \inst|inst8|inst18~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst9|inst16~combout\ : std_logic;
SIGNAL \inst|inst9|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst9|inst8~q\ : std_logic;
SIGNAL \inst|inst9|inst10~combout\ : std_logic;
SIGNAL \inst|inst9|inst~q\ : std_logic;
SIGNAL \inst|inst9|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst9|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst9|inst7~q\ : std_logic;
SIGNAL \inst|inst9|inst9~q\ : std_logic;
SIGNAL \inst|inst9|inst18~combout\ : std_logic;
SIGNAL \inst|inst9|inst18~clkctrl_outclk\ : std_logic;
SIGNAL \inst|inst10|inst16~combout\ : std_logic;
SIGNAL \inst|inst10|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst10|inst8~q\ : std_logic;
SIGNAL \inst|inst10|inst10~combout\ : std_logic;
SIGNAL \inst|inst10|inst~q\ : std_logic;
SIGNAL \inst|inst10|inst7~0_combout\ : std_logic;
SIGNAL \inst|inst10|inst7~feeder_combout\ : std_logic;
SIGNAL \inst|inst10|inst7~q\ : std_logic;
SIGNAL \inst|inst10|inst9~q\ : std_logic;
SIGNAL \inst29~combout\ : std_logic;
SIGNAL \inst29~clkctrl_outclk\ : std_logic;
SIGNAL \inst15|inst|number~2_combout\ : std_logic;
SIGNAL \BUTTON0~input_o\ : std_logic;
SIGNAL \inst15|inst|number~3_combout\ : std_logic;
SIGNAL \inst15|inst|number~1_combout\ : std_logic;
SIGNAL \inst15|inst|number~0_combout\ : std_logic;
SIGNAL \inst14|comb~9_combout\ : std_logic;
SIGNAL \inst14|comb~8_combout\ : std_logic;
SIGNAL \inst14|comb~10_combout\ : std_logic;
SIGNAL \inst14|comb~11_combout\ : std_logic;
SIGNAL \inst14|comb~12_combout\ : std_logic;
SIGNAL \inst14|comb~13_combout\ : std_logic;
SIGNAL \inst14|comb~18_combout\ : std_logic;
SIGNAL \inst14|comb~17_combout\ : std_logic;
SIGNAL \inst14|comb~14_combout\ : std_logic;
SIGNAL \inst14|Equal0~0_combout\ : std_logic;
SIGNAL \inst14|comb~19_combout\ : std_logic;
SIGNAL \inst14|comb~20_combout\ : std_logic;
SIGNAL \inst14|comb~16_combout\ : std_logic;
SIGNAL \inst14|comb~15_combout\ : std_logic;
SIGNAL \inst15|inst|LessThan0~0_combout\ : std_logic;
SIGNAL \inst15|inst|ripple~q\ : std_logic;
SIGNAL \inst15|inst|ripple~clkctrl_outclk\ : std_logic;
SIGNAL \inst15|inst1|number~0_combout\ : std_logic;
SIGNAL \inst15|inst1|number~3_combout\ : std_logic;
SIGNAL \inst15|inst1|number~2_combout\ : std_logic;
SIGNAL \inst15|inst1|number~1_combout\ : std_logic;
SIGNAL \inst21|comb~8_combout\ : std_logic;
SIGNAL \inst21|comb~9_combout\ : std_logic;
SIGNAL \inst21|comb~10_combout\ : std_logic;
SIGNAL \inst21|comb~11_combout\ : std_logic;
SIGNAL \inst21|comb~13_combout\ : std_logic;
SIGNAL \inst21|comb~12_combout\ : std_logic;
SIGNAL \inst21|comb~17_combout\ : std_logic;
SIGNAL \inst21|comb~18_combout\ : std_logic;
SIGNAL \inst21|Equal0~0_combout\ : std_logic;
SIGNAL \inst21|comb~14_combout\ : std_logic;
SIGNAL \inst21|comb~19_combout\ : std_logic;
SIGNAL \inst21|comb~20_combout\ : std_logic;
SIGNAL \inst21|comb~16_combout\ : std_logic;
SIGNAL \inst21|comb~15_combout\ : std_logic;
SIGNAL \inst15|inst1|LessThan0~0_combout\ : std_logic;
SIGNAL \inst15|inst1|ripple~q\ : std_logic;
SIGNAL \inst15|inst1|ripple~clkctrl_outclk\ : std_logic;
SIGNAL \inst15|inst2|number~0_combout\ : std_logic;
SIGNAL \inst15|inst2|number~3_combout\ : std_logic;
SIGNAL \inst15|inst2|number~2_combout\ : std_logic;
SIGNAL \inst15|inst2|number~1_combout\ : std_logic;
SIGNAL \inst22|comb~9_combout\ : std_logic;
SIGNAL \inst22|comb~8_combout\ : std_logic;
SIGNAL \inst22|comb~10_combout\ : std_logic;
SIGNAL \inst22|comb~11_combout\ : std_logic;
SIGNAL \inst22|comb~12_combout\ : std_logic;
SIGNAL \inst22|comb~13_combout\ : std_logic;
SIGNAL \inst22|comb~17_combout\ : std_logic;
SIGNAL \inst22|comb~18_combout\ : std_logic;
SIGNAL \inst22|comb~14_combout\ : std_logic;
SIGNAL \inst22|Equal0~0_combout\ : std_logic;
SIGNAL \inst22|comb~19_combout\ : std_logic;
SIGNAL \inst22|comb~20_combout\ : std_logic;
SIGNAL \inst22|comb~16_combout\ : std_logic;
SIGNAL \inst22|comb~15_combout\ : std_logic;
SIGNAL \inst15|inst2|LessThan0~0_combout\ : std_logic;
SIGNAL \inst15|inst2|ripple~q\ : std_logic;
SIGNAL \inst15|inst2|ripple~clkctrl_outclk\ : std_logic;
SIGNAL \inst15|inst3|number~2_combout\ : std_logic;
SIGNAL \inst15|inst3|number~1_combout\ : std_logic;
SIGNAL \inst15|inst3|number~0_combout\ : std_logic;
SIGNAL \inst15|inst3|number~3_combout\ : std_logic;
SIGNAL \inst23|comb~9_combout\ : std_logic;
SIGNAL \inst23|comb~8_combout\ : std_logic;
SIGNAL \inst23|comb~11_combout\ : std_logic;
SIGNAL \inst23|comb~10_combout\ : std_logic;
SIGNAL \inst23|comb~13_combout\ : std_logic;
SIGNAL \inst23|comb~12_combout\ : std_logic;
SIGNAL \inst23|comb~17_combout\ : std_logic;
SIGNAL \inst23|comb~18_combout\ : std_logic;
SIGNAL \inst23|Equal0~0_combout\ : std_logic;
SIGNAL \inst23|comb~14_combout\ : std_logic;
SIGNAL \inst23|comb~20_combout\ : std_logic;
SIGNAL \inst23|comb~19_combout\ : std_logic;
SIGNAL \inst23|comb~15_combout\ : std_logic;
SIGNAL \inst23|comb~16_combout\ : std_logic;
SIGNAL \inst15|inst2|number\ : std_logic_vector(3 DOWNTO 0);
SIGNAL inst11 : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst14|seven_seg0\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst22|seven_seg0\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst15|inst1|number\ : std_logic_vector(3 DOWNTO 0);
SIGNAL inst19 : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst15|inst|number\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst15|inst3|number\ : std_logic_vector(3 DOWNTO 0);
SIGNAL inst12 : std_logic_vector(3 DOWNTO 0);
SIGNAL inst17 : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst21|seven_seg0\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst23|seven_seg0\ : std_logic_vector(6 DOWNTO 0);

BEGIN

D0 <= ww_D0;
ww_CLOCK <= CLOCK;
ww_BUTTON1 <= BUTTON1;
ww_BUTTON0 <= BUTTON0;
ww_BUTTON2 <= BUTTON2;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCK~input_o\);

\inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_CLKSELECT_bus\ <= (gnd & gnd);

\inst15|inst|ripple~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst15|inst|ripple~q\);

\inst15|inst1|ripple~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst15|inst1|ripple~q\);

\inst15|inst2|ripple~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst15|inst2|ripple~q\);

\inst29~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst29~combout\);

\inst|inst1|inst18~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst1|inst18~combout\);

\inst|inst5|inst3~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst5|inst3~q\);

\inst|inst7|inst18~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst7|inst18~combout\);

\inst|inst9|inst18~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst9|inst18~combout\);

\inst|inst6|inst18~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst6|inst18~combout\);

\inst|inst8|inst18~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|inst8|inst18~combout\);

-- Location: IOOBUF_X26_Y29_N16
\D0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(6),
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\D0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(5),
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\D0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(4),
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\D0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(3),
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\D0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(2),
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\D0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(1),
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\D0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst14|seven_seg0\(0),
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\D1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(6),
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\D1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(5),
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\D1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(4),
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\D1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(3),
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\D1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(2),
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\D1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(1),
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\D1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst21|seven_seg0\(0),
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\D2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(6),
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\D2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(5),
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\D2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(4),
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\D2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(3),
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\D2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(2),
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\D2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(1),
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\D2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst22|seven_seg0\(0),
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\D3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(6),
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\D3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(5),
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\D3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(4),
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\D3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(3),
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\D3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(2),
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\D3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(1),
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\D3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23|seven_seg0\(0),
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOIBUF_X0_Y23_N1
\BUTTON2~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BUTTON2,
	o => \BUTTON2~input_o\);

-- Location: IOIBUF_X0_Y23_N15
\BUTTON1~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BUTTON1,
	o => \BUTTON1~input_o\);

-- Location: LCCOMB_X1_Y23_N2
\inst16~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst16~0_combout\ = \BUTTON1~input_o\ $ (!\inst16~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON1~input_o\,
	datad => \inst16~q\,
	combout => \inst16~0_combout\);

-- Location: LCCOMB_X1_Y23_N20
\inst16~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst16~feeder_combout\ = \inst16~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst16~0_combout\,
	combout => \inst16~feeder_combout\);

-- Location: FF_X1_Y23_N21
inst16 : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \BUTTON1~input_o\,
	d => \inst16~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst16~q\);

-- Location: IOIBUF_X41_Y15_N1
\CLOCK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK,
	o => \CLOCK~input_o\);

-- Location: CLKCTRL_G9
\inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "falling edge")
-- pragma translate_on
PORT MAP (
	ena => VCC,
	inclk => \inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_INCLK_bus\,
	clkselect => \inst|inst13|altctrlclk_altclkctrl_uhi_component|clkctrl1_CLKSELECT_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst13|altctrlclk_altclkctrl_uhi_component|wire_clkctrl1_outclk\);

-- Location: LCCOMB_X24_Y28_N26
\inst|inst|inst3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst|inst3~0_combout\ = !\inst|inst|inst3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst|inst3~q\,
	combout => \inst|inst|inst3~0_combout\);

-- Location: LCCOMB_X24_Y28_N18
\inst|inst|inst3~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst|inst3~feeder_combout\ = \inst|inst|inst3~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst|inst3~0_combout\,
	combout => \inst|inst|inst3~feeder_combout\);

-- Location: FF_X24_Y28_N19
\inst|inst|inst3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst13|altctrlclk_altclkctrl_uhi_component|wire_clkctrl1_outclk\,
	d => \inst|inst|inst3~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst|inst3~q\);

-- Location: LCCOMB_X23_Y28_N0
\inst|inst2|inst3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst2|inst3~0_combout\ = !\inst|inst2|inst3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst2|inst3~q\,
	combout => \inst|inst2|inst3~0_combout\);

-- Location: LCCOMB_X23_Y28_N10
\inst|inst2|inst3~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst2|inst3~feeder_combout\ = \inst|inst2|inst3~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst2|inst3~0_combout\,
	combout => \inst|inst2|inst3~feeder_combout\);

-- Location: FF_X23_Y28_N11
\inst|inst2|inst3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst|inst3~q\,
	d => \inst|inst2|inst3~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst2|inst3~q\);

-- Location: LCCOMB_X23_Y28_N22
\inst|inst3|inst3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst3|inst3~0_combout\ = !\inst|inst3|inst3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst3|inst3~q\,
	combout => \inst|inst3|inst3~0_combout\);

-- Location: FF_X23_Y28_N5
\inst|inst3|inst3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst2|inst3~q\,
	asdata => \inst|inst3|inst3~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst3|inst3~q\);

-- Location: LCCOMB_X19_Y28_N24
\inst|inst4|inst3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst4|inst3~0_combout\ = !\inst|inst4|inst3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst4|inst3~q\,
	combout => \inst|inst4|inst3~0_combout\);

-- Location: LCCOMB_X19_Y28_N10
\inst|inst4|inst3~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst4|inst3~feeder_combout\ = \inst|inst4|inst3~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst4|inst3~0_combout\,
	combout => \inst|inst4|inst3~feeder_combout\);

-- Location: FF_X19_Y28_N11
\inst|inst4|inst3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst3|inst3~q\,
	d => \inst|inst4|inst3~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst4|inst3~q\);

-- Location: LCCOMB_X19_Y28_N26
\inst|inst5|inst3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst5|inst3~0_combout\ = !\inst|inst5|inst3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst5|inst3~q\,
	combout => \inst|inst5|inst3~0_combout\);

-- Location: FF_X19_Y28_N7
\inst|inst5|inst3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst4|inst3~q\,
	asdata => \inst|inst5|inst3~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst5|inst3~q\);

-- Location: CLKCTRL_G10
\inst|inst5|inst3~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst5|inst3~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst5|inst3~clkctrl_outclk\);

-- Location: LCCOMB_X20_Y28_N6
\inst|inst1|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst16~combout\ = (\inst|inst1|inst7~q\ & \inst|inst1|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst1|inst7~q\,
	datad => \inst|inst1|inst~q\,
	combout => \inst|inst1|inst16~combout\);

-- Location: LCCOMB_X20_Y28_N24
\inst|inst1|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst8~feeder_combout\ = \inst|inst1|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst1|inst16~combout\,
	combout => \inst|inst1|inst8~feeder_combout\);

-- Location: FF_X20_Y28_N25
\inst|inst1|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst5|inst3~clkctrl_outclk\,
	d => \inst|inst1|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst1|inst8~q\);

-- Location: LCCOMB_X20_Y28_N12
\inst|inst1|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst10~combout\ = (!\inst|inst1|inst~q\ & !\inst|inst1|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst1|inst~q\,
	datad => \inst|inst1|inst8~q\,
	combout => \inst|inst1|inst10~combout\);

-- Location: FF_X20_Y28_N13
\inst|inst1|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst5|inst3~clkctrl_outclk\,
	d => \inst|inst1|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst1|inst~q\);

-- Location: LCCOMB_X20_Y28_N28
\inst|inst1|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst7~0_combout\ = \inst|inst1|inst7~q\ $ (\inst|inst1|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst1|inst7~q\,
	datad => \inst|inst1|inst~q\,
	combout => \inst|inst1|inst7~0_combout\);

-- Location: LCCOMB_X20_Y28_N18
\inst|inst1|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst7~feeder_combout\ = \inst|inst1|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst1|inst7~0_combout\,
	combout => \inst|inst1|inst7~feeder_combout\);

-- Location: FF_X20_Y28_N19
\inst|inst1|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst5|inst3~clkctrl_outclk\,
	d => \inst|inst1|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst1|inst7~q\);

-- Location: FF_X20_Y28_N3
\inst|inst1|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst5|inst3~q\,
	asdata => \inst|inst1|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst1|inst9~q\);

-- Location: LCCOMB_X20_Y28_N2
\inst|inst1|inst18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst1|inst18~combout\ = LCELL((\inst|inst1|inst9~q\) # (\inst|inst1|inst7~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst1|inst9~q\,
	datad => \inst|inst1|inst7~q\,
	combout => \inst|inst1|inst18~combout\);

-- Location: CLKCTRL_G13
\inst|inst1|inst18~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst1|inst18~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst1|inst18~clkctrl_outclk\);

-- Location: LCCOMB_X22_Y3_N24
\inst|inst6|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst16~combout\ = (\inst|inst6|inst7~q\ & \inst|inst6|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst6|inst7~q\,
	datad => \inst|inst6|inst~q\,
	combout => \inst|inst6|inst16~combout\);

-- Location: LCCOMB_X22_Y3_N16
\inst|inst6|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst8~feeder_combout\ = \inst|inst6|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst6|inst16~combout\,
	combout => \inst|inst6|inst8~feeder_combout\);

-- Location: FF_X22_Y3_N17
\inst|inst6|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst1|inst18~clkctrl_outclk\,
	d => \inst|inst6|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst6|inst8~q\);

-- Location: LCCOMB_X22_Y3_N2
\inst|inst6|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst10~combout\ = (!\inst|inst6|inst~q\ & !\inst|inst6|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst6|inst~q\,
	datad => \inst|inst6|inst8~q\,
	combout => \inst|inst6|inst10~combout\);

-- Location: FF_X22_Y3_N3
\inst|inst6|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst1|inst18~clkctrl_outclk\,
	d => \inst|inst6|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst6|inst~q\);

-- Location: LCCOMB_X22_Y3_N6
\inst|inst6|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst7~0_combout\ = \inst|inst6|inst7~q\ $ (\inst|inst6|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst6|inst7~q\,
	datad => \inst|inst6|inst~q\,
	combout => \inst|inst6|inst7~0_combout\);

-- Location: LCCOMB_X22_Y3_N18
\inst|inst6|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst7~feeder_combout\ = \inst|inst6|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst6|inst7~0_combout\,
	combout => \inst|inst6|inst7~feeder_combout\);

-- Location: FF_X22_Y3_N19
\inst|inst6|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst1|inst18~clkctrl_outclk\,
	d => \inst|inst6|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst6|inst7~q\);

-- Location: FF_X22_Y3_N29
\inst|inst6|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst1|inst18~combout\,
	asdata => \inst|inst6|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst6|inst9~q\);

-- Location: LCCOMB_X22_Y3_N28
\inst|inst6|inst18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst6|inst18~combout\ = LCELL((\inst|inst6|inst9~q\) # (\inst|inst6|inst7~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst6|inst9~q\,
	datad => \inst|inst6|inst7~q\,
	combout => \inst|inst6|inst18~combout\);

-- Location: CLKCTRL_G17
\inst|inst6|inst18~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst6|inst18~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst6|inst18~clkctrl_outclk\);

-- Location: LCCOMB_X22_Y1_N24
\inst|inst7|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst16~combout\ = (\inst|inst7|inst7~q\ & \inst|inst7|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst7|inst7~q\,
	datad => \inst|inst7|inst~q\,
	combout => \inst|inst7|inst16~combout\);

-- Location: LCCOMB_X22_Y1_N16
\inst|inst7|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst8~feeder_combout\ = \inst|inst7|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst7|inst16~combout\,
	combout => \inst|inst7|inst8~feeder_combout\);

-- Location: FF_X22_Y1_N17
\inst|inst7|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst6|inst18~clkctrl_outclk\,
	d => \inst|inst7|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst7|inst8~q\);

-- Location: LCCOMB_X22_Y1_N28
\inst|inst7|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst10~combout\ = (!\inst|inst7|inst~q\ & !\inst|inst7|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst7|inst~q\,
	datad => \inst|inst7|inst8~q\,
	combout => \inst|inst7|inst10~combout\);

-- Location: FF_X22_Y1_N29
\inst|inst7|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst6|inst18~clkctrl_outclk\,
	d => \inst|inst7|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst7|inst~q\);

-- Location: LCCOMB_X22_Y1_N20
\inst|inst7|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst7~0_combout\ = \inst|inst7|inst7~q\ $ (\inst|inst7|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst7|inst7~q\,
	datad => \inst|inst7|inst~q\,
	combout => \inst|inst7|inst7~0_combout\);

-- Location: LCCOMB_X22_Y1_N18
\inst|inst7|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst7~feeder_combout\ = \inst|inst7|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst7|inst7~0_combout\,
	combout => \inst|inst7|inst7~feeder_combout\);

-- Location: FF_X22_Y1_N19
\inst|inst7|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst6|inst18~clkctrl_outclk\,
	d => \inst|inst7|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst7|inst7~q\);

-- Location: FF_X22_Y1_N31
\inst|inst7|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst6|inst18~combout\,
	asdata => \inst|inst7|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst7|inst9~q\);

-- Location: LCCOMB_X22_Y1_N30
\inst|inst7|inst18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst7|inst18~combout\ = LCELL((\inst|inst7|inst9~q\) # (\inst|inst7|inst7~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst7|inst9~q\,
	datad => \inst|inst7|inst7~q\,
	combout => \inst|inst7|inst18~combout\);

-- Location: CLKCTRL_G16
\inst|inst7|inst18~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst7|inst18~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst7|inst18~clkctrl_outclk\);

-- Location: LCCOMB_X21_Y1_N6
\inst|inst8|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst16~combout\ = (\inst|inst8|inst7~q\ & \inst|inst8|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst8|inst7~q\,
	datad => \inst|inst8|inst~q\,
	combout => \inst|inst8|inst16~combout\);

-- Location: LCCOMB_X21_Y1_N18
\inst|inst8|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst8~feeder_combout\ = \inst|inst8|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst8|inst16~combout\,
	combout => \inst|inst8|inst8~feeder_combout\);

-- Location: FF_X21_Y1_N19
\inst|inst8|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst7|inst18~clkctrl_outclk\,
	d => \inst|inst8|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8|inst8~q\);

-- Location: LCCOMB_X21_Y1_N12
\inst|inst8|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst10~combout\ = (!\inst|inst8|inst~q\ & !\inst|inst8|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst8|inst~q\,
	datad => \inst|inst8|inst8~q\,
	combout => \inst|inst8|inst10~combout\);

-- Location: FF_X21_Y1_N13
\inst|inst8|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst7|inst18~clkctrl_outclk\,
	d => \inst|inst8|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8|inst~q\);

-- Location: LCCOMB_X21_Y1_N24
\inst|inst8|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst7~0_combout\ = \inst|inst8|inst7~q\ $ (\inst|inst8|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|inst8|inst7~q\,
	datad => \inst|inst8|inst~q\,
	combout => \inst|inst8|inst7~0_combout\);

-- Location: LCCOMB_X21_Y1_N28
\inst|inst8|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst7~feeder_combout\ = \inst|inst8|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst8|inst7~0_combout\,
	combout => \inst|inst8|inst7~feeder_combout\);

-- Location: FF_X21_Y1_N29
\inst|inst8|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst7|inst18~clkctrl_outclk\,
	d => \inst|inst8|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8|inst7~q\);

-- Location: FF_X21_Y1_N17
\inst|inst8|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst7|inst18~combout\,
	asdata => \inst|inst8|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8|inst9~q\);

-- Location: LCCOMB_X21_Y1_N16
\inst|inst8|inst18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8|inst18~combout\ = LCELL((\inst|inst8|inst9~q\) # (\inst|inst8|inst7~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst8|inst9~q\,
	datad => \inst|inst8|inst7~q\,
	combout => \inst|inst8|inst18~combout\);

-- Location: CLKCTRL_G15
\inst|inst8|inst18~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst8|inst18~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst8|inst18~clkctrl_outclk\);

-- Location: LCCOMB_X20_Y1_N6
\inst|inst9|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst16~combout\ = (\inst|inst9|inst7~q\ & \inst|inst9|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst9|inst7~q\,
	datad => \inst|inst9|inst~q\,
	combout => \inst|inst9|inst16~combout\);

-- Location: LCCOMB_X20_Y1_N28
\inst|inst9|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst8~feeder_combout\ = \inst|inst9|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst9|inst16~combout\,
	combout => \inst|inst9|inst8~feeder_combout\);

-- Location: FF_X20_Y1_N29
\inst|inst9|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst8|inst18~clkctrl_outclk\,
	d => \inst|inst9|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst9|inst8~q\);

-- Location: LCCOMB_X20_Y1_N2
\inst|inst9|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst10~combout\ = (!\inst|inst9|inst~q\ & !\inst|inst9|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst9|inst~q\,
	datad => \inst|inst9|inst8~q\,
	combout => \inst|inst9|inst10~combout\);

-- Location: FF_X20_Y1_N3
\inst|inst9|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst8|inst18~clkctrl_outclk\,
	d => \inst|inst9|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst9|inst~q\);

-- Location: LCCOMB_X20_Y1_N16
\inst|inst9|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst7~0_combout\ = \inst|inst9|inst7~q\ $ (\inst|inst9|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst9|inst7~q\,
	datad => \inst|inst9|inst~q\,
	combout => \inst|inst9|inst7~0_combout\);

-- Location: LCCOMB_X20_Y1_N10
\inst|inst9|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst7~feeder_combout\ = \inst|inst9|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst9|inst7~0_combout\,
	combout => \inst|inst9|inst7~feeder_combout\);

-- Location: FF_X20_Y1_N11
\inst|inst9|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst8|inst18~clkctrl_outclk\,
	d => \inst|inst9|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst9|inst7~q\);

-- Location: FF_X20_Y1_N25
\inst|inst9|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst8|inst18~combout\,
	asdata => \inst|inst9|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst9|inst9~q\);

-- Location: LCCOMB_X20_Y1_N24
\inst|inst9|inst18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst9|inst18~combout\ = LCELL((\inst|inst9|inst9~q\) # (\inst|inst9|inst7~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst9|inst9~q\,
	datad => \inst|inst9|inst7~q\,
	combout => \inst|inst9|inst18~combout\);

-- Location: CLKCTRL_G18
\inst|inst9|inst18~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|inst9|inst18~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|inst9|inst18~clkctrl_outclk\);

-- Location: LCCOMB_X1_Y14_N2
\inst|inst10|inst16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst10|inst16~combout\ = (\inst|inst10|inst7~q\ & \inst|inst10|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst10|inst7~q\,
	datad => \inst|inst10|inst~q\,
	combout => \inst|inst10|inst16~combout\);

-- Location: LCCOMB_X1_Y14_N24
\inst|inst10|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst10|inst8~feeder_combout\ = \inst|inst10|inst16~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst10|inst16~combout\,
	combout => \inst|inst10|inst8~feeder_combout\);

-- Location: FF_X1_Y14_N25
\inst|inst10|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst9|inst18~clkctrl_outclk\,
	d => \inst|inst10|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst10|inst8~q\);

-- Location: LCCOMB_X1_Y14_N18
\inst|inst10|inst10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst10|inst10~combout\ = (!\inst|inst10|inst~q\ & !\inst|inst10|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst10|inst~q\,
	datad => \inst|inst10|inst8~q\,
	combout => \inst|inst10|inst10~combout\);

-- Location: FF_X1_Y14_N19
\inst|inst10|inst\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst9|inst18~clkctrl_outclk\,
	d => \inst|inst10|inst10~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst10|inst~q\);

-- Location: LCCOMB_X1_Y14_N16
\inst|inst10|inst7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst10|inst7~0_combout\ = \inst|inst10|inst7~q\ $ (\inst|inst10|inst~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst10|inst7~q\,
	datad => \inst|inst10|inst~q\,
	combout => \inst|inst10|inst7~0_combout\);

-- Location: LCCOMB_X1_Y14_N28
\inst|inst10|inst7~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst10|inst7~feeder_combout\ = \inst|inst10|inst7~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst|inst10|inst7~0_combout\,
	combout => \inst|inst10|inst7~feeder_combout\);

-- Location: FF_X1_Y14_N29
\inst|inst10|inst7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst9|inst18~clkctrl_outclk\,
	d => \inst|inst10|inst7~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst10|inst7~q\);

-- Location: FF_X1_Y14_N31
\inst|inst10|inst9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|inst9|inst18~combout\,
	asdata => \inst|inst10|inst7~q\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst10|inst9~q\);

-- Location: LCCOMB_X1_Y14_N30
inst29 : cycloneiii_lcell_comb
-- Equation(s):
-- \inst29~combout\ = LCELL((\inst16~q\ & ((\inst|inst10|inst9~q\) # (\inst|inst10|inst7~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst16~q\,
	datac => \inst|inst10|inst9~q\,
	datad => \inst|inst10|inst7~q\,
	combout => \inst29~combout\);

-- Location: CLKCTRL_G0
\inst29~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst29~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst29~clkctrl_outclk\);

-- Location: LCCOMB_X19_Y27_N20
\inst15|inst|number~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst|number~2_combout\ = (!\inst15|inst|number\(3) & (\inst15|inst|number\(1) $ (\inst15|inst|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst|number\(3),
	datac => \inst15|inst|number\(1),
	datad => \inst15|inst|number\(0),
	combout => \inst15|inst|number~2_combout\);

-- Location: IOIBUF_X0_Y21_N8
\BUTTON0~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_BUTTON0,
	o => \BUTTON0~input_o\);

-- Location: FF_X19_Y27_N21
\inst15|inst|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst29~clkctrl_outclk\,
	d => \inst15|inst|number~2_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst|number\(1));

-- Location: LCCOMB_X19_Y27_N10
\inst15|inst|number~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst|number~3_combout\ = (\inst15|inst|number\(0) & (\inst15|inst|number\(2) & (!\inst15|inst|number\(3) & \inst15|inst|number\(1)))) # (!\inst15|inst|number\(0) & (!\inst15|inst|number\(2) & (\inst15|inst|number\(3) & !\inst15|inst|number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst|number\(0),
	datab => \inst15|inst|number\(2),
	datac => \inst15|inst|number\(3),
	datad => \inst15|inst|number\(1),
	combout => \inst15|inst|number~3_combout\);

-- Location: FF_X19_Y27_N11
\inst15|inst|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst29~clkctrl_outclk\,
	d => \inst15|inst|number~3_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst|number\(3));

-- Location: LCCOMB_X19_Y27_N6
\inst15|inst|number~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst|number~1_combout\ = (!\inst15|inst|number\(3) & (\inst15|inst|number\(2) $ (((\inst15|inst|number\(1) & \inst15|inst|number\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst|number\(3),
	datab => \inst15|inst|number\(1),
	datac => \inst15|inst|number\(2),
	datad => \inst15|inst|number\(0),
	combout => \inst15|inst|number~1_combout\);

-- Location: FF_X19_Y27_N7
\inst15|inst|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst29~clkctrl_outclk\,
	d => \inst15|inst|number~1_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst|number\(2));

-- Location: LCCOMB_X19_Y27_N12
\inst15|inst|number~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst|number~0_combout\ = (!\inst15|inst|number\(0) & (((!\inst15|inst|number\(2) & !\inst15|inst|number\(1))) # (!\inst15|inst|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst|number\(2),
	datab => \inst15|inst|number\(3),
	datac => \inst15|inst|number\(0),
	datad => \inst15|inst|number\(1),
	combout => \inst15|inst|number~0_combout\);

-- Location: FF_X19_Y27_N13
\inst15|inst|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst29~clkctrl_outclk\,
	d => \inst15|inst|number~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst|number\(0));

-- Location: LCCOMB_X19_Y27_N14
\inst11[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst11(0) = (\BUTTON2~input_o\ & ((\inst15|inst|number\(0)))) # (!\BUTTON2~input_o\ & (inst11(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => inst11(0),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst|number\(0),
	combout => inst11(0));

-- Location: LCCOMB_X19_Y27_N24
\inst11[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst11(3) = (\BUTTON2~input_o\ & ((\inst15|inst|number\(3)))) # (!\BUTTON2~input_o\ & (inst11(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(3),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst|number\(3),
	combout => inst11(3));

-- Location: LCCOMB_X19_Y27_N30
\inst11[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst11(1) = (\BUTTON2~input_o\ & ((\inst15|inst|number\(1)))) # (!\BUTTON2~input_o\ & (inst11(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(1),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst|number\(1),
	combout => inst11(1));

-- Location: LCCOMB_X19_Y27_N16
\inst11[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst11(2) = (\BUTTON2~input_o\ & ((\inst15|inst|number\(2)))) # (!\BUTTON2~input_o\ & (inst11(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => inst11(2),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst|number\(2),
	combout => inst11(2));

-- Location: LCCOMB_X19_Y27_N28
\inst14|comb~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~9_combout\ = (!inst11(3) & ((inst11(1) & (inst11(0) & inst11(2))) # (!inst11(1) & ((!inst11(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(3),
	datac => inst11(1),
	datad => inst11(2),
	combout => \inst14|comb~9_combout\);

-- Location: LCCOMB_X19_Y27_N18
\inst14|comb~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~8_combout\ = (inst11(1) & (!inst11(3) & ((!inst11(2)) # (!inst11(0))))) # (!inst11(1) & ((inst11(3) $ (inst11(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(3),
	datac => inst11(1),
	datad => inst11(2),
	combout => \inst14|comb~8_combout\);

-- Location: LCCOMB_X19_Y27_N26
\inst14|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(6) = (!\inst14|comb~8_combout\ & ((\inst14|comb~9_combout\) # (\inst14|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst14|comb~9_combout\,
	datac => \inst14|comb~8_combout\,
	datad => \inst14|seven_seg0\(6),
	combout => \inst14|seven_seg0\(6));

-- Location: LCCOMB_X20_Y27_N28
\inst14|comb~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~10_combout\ = (inst11(0) & (!inst11(1) & (inst11(2) $ (inst11(3))))) # (!inst11(0) & ((inst11(2) & ((!inst11(3)))) # (!inst11(2) & (!inst11(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~10_combout\);

-- Location: LCCOMB_X20_Y27_N6
\inst14|comb~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~11_combout\ = (!inst11(3) & ((inst11(0) & ((inst11(1)) # (!inst11(2)))) # (!inst11(0) & (!inst11(2) & inst11(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~11_combout\);

-- Location: LCCOMB_X28_Y27_N20
\inst14|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(5) = (!\inst14|comb~10_combout\ & ((\inst14|comb~11_combout\) # (\inst14|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst14|comb~10_combout\,
	datac => \inst14|comb~11_combout\,
	datad => \inst14|seven_seg0\(5),
	combout => \inst14|seven_seg0\(5));

-- Location: LCCOMB_X20_Y27_N16
\inst14|comb~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~12_combout\ = (!inst11(0) & ((inst11(1) & ((!inst11(3)))) # (!inst11(1) & (!inst11(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~12_combout\);

-- Location: LCCOMB_X20_Y27_N26
\inst14|comb~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~13_combout\ = (inst11(1) & (inst11(0) & ((!inst11(3))))) # (!inst11(1) & ((inst11(2) & ((!inst11(3)))) # (!inst11(2) & (inst11(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~13_combout\);

-- Location: LCCOMB_X20_Y27_N20
\inst14|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(4) = (!\inst14|comb~12_combout\ & ((\inst14|comb~13_combout\) # (\inst14|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst14|comb~12_combout\,
	datac => \inst14|comb~13_combout\,
	datad => \inst14|seven_seg0\(4),
	combout => \inst14|seven_seg0\(4));

-- Location: LCCOMB_X20_Y27_N2
\inst14|comb~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~18_combout\ = (!inst11(3) & ((inst11(0) & (inst11(2) $ (!inst11(1)))) # (!inst11(0) & (inst11(2) & !inst11(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~18_combout\);

-- Location: LCCOMB_X20_Y27_N8
\inst14|comb~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~17_combout\ = (inst11(1) & (!inst11(3) & ((!inst11(2)) # (!inst11(0))))) # (!inst11(1) & (inst11(2) $ (((inst11(3)) # (!inst11(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~17_combout\);

-- Location: LCCOMB_X20_Y27_N10
\inst14|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(3) = (!\inst14|comb~17_combout\ & ((\inst14|comb~18_combout\) # (\inst14|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst14|comb~18_combout\,
	datac => \inst14|comb~17_combout\,
	datad => \inst14|seven_seg0\(3),
	combout => \inst14|seven_seg0\(3));

-- Location: LCCOMB_X20_Y27_N24
\inst14|comb~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~14_combout\ = (inst11(2) & (((!inst11(3))))) # (!inst11(2) & (((inst11(0) & !inst11(3))) # (!inst11(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~14_combout\);

-- Location: LCCOMB_X20_Y27_N30
\inst14|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|Equal0~0_combout\ = (!inst11(0) & (!inst11(2) & (inst11(1) & !inst11(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|Equal0~0_combout\);

-- Location: LCCOMB_X20_Y27_N0
\inst14|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(2) = (!\inst14|comb~14_combout\ & ((\inst14|Equal0~0_combout\) # (\inst14|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst14|comb~14_combout\,
	datac => \inst14|Equal0~0_combout\,
	datad => \inst14|seven_seg0\(2),
	combout => \inst14|seven_seg0\(2));

-- Location: LCCOMB_X20_Y27_N12
\inst14|comb~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~19_combout\ = (inst11(2) & (!inst11(3) & (inst11(0) $ (!inst11(1))))) # (!inst11(2) & (((!inst11(3)) # (!inst11(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001110110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~19_combout\);

-- Location: LCCOMB_X20_Y27_N22
\inst14|comb~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~20_combout\ = (inst11(2) & (!inst11(3) & (inst11(0) $ (inst11(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~20_combout\);

-- Location: LCCOMB_X21_Y27_N28
\inst14|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(1) = (!\inst14|comb~19_combout\ & ((\inst14|comb~20_combout\) # (\inst14|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst14|comb~19_combout\,
	datab => \inst14|comb~20_combout\,
	datad => \inst14|seven_seg0\(1),
	combout => \inst14|seven_seg0\(1));

-- Location: LCCOMB_X20_Y27_N14
\inst14|comb~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~16_combout\ = (!inst11(1) & (!inst11(3) & (inst11(0) $ (inst11(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~16_combout\);

-- Location: LCCOMB_X20_Y27_N4
\inst14|comb~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|comb~15_combout\ = (inst11(1) & (((!inst11(3))))) # (!inst11(1) & (inst11(2) $ (((inst11(3)) # (!inst11(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001111111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst11(0),
	datab => inst11(2),
	datac => inst11(1),
	datad => inst11(3),
	combout => \inst14|comb~15_combout\);

-- Location: LCCOMB_X20_Y27_N18
\inst14|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst14|seven_seg0\(0) = (!\inst14|comb~15_combout\ & ((\inst14|comb~16_combout\) # (\inst14|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst14|comb~16_combout\,
	datac => \inst14|comb~15_combout\,
	datad => \inst14|seven_seg0\(0),
	combout => \inst14|seven_seg0\(0));

-- Location: LCCOMB_X19_Y27_N22
\inst15|inst|LessThan0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst|LessThan0~0_combout\ = (\inst15|inst|number\(3) & ((\inst15|inst|number\(0)) # ((\inst15|inst|number\(2)) # (\inst15|inst|number\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst|number\(0),
	datab => \inst15|inst|number\(2),
	datac => \inst15|inst|number\(1),
	datad => \inst15|inst|number\(3),
	combout => \inst15|inst|LessThan0~0_combout\);

-- Location: FF_X19_Y27_N23
\inst15|inst|ripple\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst29~clkctrl_outclk\,
	d => \inst15|inst|LessThan0~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst|ripple~q\);

-- Location: CLKCTRL_G14
\inst15|inst|ripple~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst15|inst|ripple~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst15|inst|ripple~clkctrl_outclk\);

-- Location: LCCOMB_X23_Y27_N6
\inst15|inst1|number~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|number~0_combout\ = (!\inst15|inst1|number\(0) & (((!\inst15|inst1|number\(2) & !\inst15|inst1|number\(1))) # (!\inst15|inst1|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst1|number\(3),
	datab => \inst15|inst1|number\(2),
	datac => \inst15|inst1|number\(0),
	datad => \inst15|inst1|number\(1),
	combout => \inst15|inst1|number~0_combout\);

-- Location: FF_X23_Y27_N7
\inst15|inst1|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst|ripple~clkctrl_outclk\,
	d => \inst15|inst1|number~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst1|number\(0));

-- Location: LCCOMB_X23_Y27_N18
\inst15|inst1|number~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|number~3_combout\ = (\inst15|inst1|number\(0) & (\inst15|inst1|number\(2) & (!\inst15|inst1|number\(3) & \inst15|inst1|number\(1)))) # (!\inst15|inst1|number\(0) & (!\inst15|inst1|number\(2) & (\inst15|inst1|number\(3) & 
-- !\inst15|inst1|number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst1|number\(0),
	datab => \inst15|inst1|number\(2),
	datac => \inst15|inst1|number\(3),
	datad => \inst15|inst1|number\(1),
	combout => \inst15|inst1|number~3_combout\);

-- Location: FF_X23_Y27_N19
\inst15|inst1|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst|ripple~clkctrl_outclk\,
	d => \inst15|inst1|number~3_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst1|number\(3));

-- Location: LCCOMB_X23_Y27_N10
\inst15|inst1|number~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|number~2_combout\ = (!\inst15|inst1|number\(3) & (\inst15|inst1|number\(1) $ (\inst15|inst1|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst15|inst1|number\(3),
	datac => \inst15|inst1|number\(1),
	datad => \inst15|inst1|number\(0),
	combout => \inst15|inst1|number~2_combout\);

-- Location: FF_X23_Y27_N11
\inst15|inst1|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst|ripple~clkctrl_outclk\,
	d => \inst15|inst1|number~2_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst1|number\(1));

-- Location: LCCOMB_X23_Y27_N16
\inst15|inst1|number~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|number~1_combout\ = (!\inst15|inst1|number\(3) & (\inst15|inst1|number\(2) $ (((\inst15|inst1|number\(1) & \inst15|inst1|number\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst1|number\(1),
	datab => \inst15|inst1|number\(3),
	datac => \inst15|inst1|number\(2),
	datad => \inst15|inst1|number\(0),
	combout => \inst15|inst1|number~1_combout\);

-- Location: FF_X23_Y27_N17
\inst15|inst1|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst|ripple~clkctrl_outclk\,
	d => \inst15|inst1|number~1_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst1|number\(2));

-- Location: LCCOMB_X23_Y27_N30
\inst12[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst12(2) = (\BUTTON2~input_o\ & ((\inst15|inst1|number\(2)))) # (!\BUTTON2~input_o\ & (inst12(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst12(2),
	datad => \inst15|inst1|number\(2),
	combout => inst12(2));

-- Location: LCCOMB_X23_Y27_N8
\inst12[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst12(3) = (\BUTTON2~input_o\ & ((\inst15|inst1|number\(3)))) # (!\BUTTON2~input_o\ & (inst12(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst12(3),
	datad => \inst15|inst1|number\(3),
	combout => inst12(3));

-- Location: LCCOMB_X23_Y27_N14
\inst12[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst12(1) = (\BUTTON2~input_o\ & ((\inst15|inst1|number\(1)))) # (!\BUTTON2~input_o\ & (inst12(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst12(1),
	datad => \inst15|inst1|number\(1),
	combout => inst12(1));

-- Location: LCCOMB_X23_Y27_N22
\inst12[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst12(0) = (\BUTTON2~input_o\ & ((\inst15|inst1|number\(0)))) # (!\BUTTON2~input_o\ & (inst12(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst12(0),
	datad => \inst15|inst1|number\(0),
	combout => inst12(0));

-- Location: LCCOMB_X24_Y27_N0
\inst21|comb~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~8_combout\ = (inst12(2) & (!inst12(3) & ((!inst12(0)) # (!inst12(1))))) # (!inst12(2) & (inst12(3) $ ((inst12(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~8_combout\);

-- Location: LCCOMB_X24_Y27_N26
\inst21|comb~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~9_combout\ = (!inst12(3) & ((inst12(2) & (inst12(1) & inst12(0))) # (!inst12(2) & (!inst12(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~9_combout\);

-- Location: LCCOMB_X24_Y27_N20
\inst21|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(6) = (!\inst21|comb~8_combout\ & ((\inst21|comb~9_combout\) # (\inst21|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst21|comb~8_combout\,
	datac => \inst21|comb~9_combout\,
	datad => \inst21|seven_seg0\(6),
	combout => \inst21|seven_seg0\(6));

-- Location: LCCOMB_X24_Y27_N4
\inst21|comb~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~10_combout\ = (inst12(2) & (!inst12(3) & ((!inst12(0)) # (!inst12(1))))) # (!inst12(2) & (!inst12(1) & ((inst12(3)) # (!inst12(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011000100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~10_combout\);

-- Location: LCCOMB_X24_Y27_N30
\inst21|comb~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~11_combout\ = (!inst12(3) & ((inst12(2) & (inst12(1) & inst12(0))) # (!inst12(2) & ((inst12(1)) # (inst12(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~11_combout\);

-- Location: LCCOMB_X24_Y27_N18
\inst21|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(5) = (!\inst21|comb~10_combout\ & ((\inst21|comb~11_combout\) # (\inst21|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst21|comb~10_combout\,
	datac => \inst21|comb~11_combout\,
	datad => \inst21|seven_seg0\(5),
	combout => \inst21|seven_seg0\(5));

-- Location: LCCOMB_X23_Y27_N20
\inst21|comb~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~13_combout\ = (inst12(1) & (inst12(0) & ((!inst12(3))))) # (!inst12(1) & ((inst12(2) & ((!inst12(3)))) # (!inst12(2) & (inst12(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(0),
	datab => inst12(2),
	datac => inst12(3),
	datad => inst12(1),
	combout => \inst21|comb~13_combout\);

-- Location: LCCOMB_X23_Y27_N4
\inst21|comb~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~12_combout\ = (!inst12(0) & ((inst12(1) & ((!inst12(3)))) # (!inst12(1) & (!inst12(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(0),
	datab => inst12(2),
	datac => inst12(3),
	datad => inst12(1),
	combout => \inst21|comb~12_combout\);

-- Location: LCCOMB_X23_Y27_N24
\inst21|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(4) = (!\inst21|comb~12_combout\ & ((\inst21|comb~13_combout\) # (\inst21|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|comb~13_combout\,
	datac => \inst21|comb~12_combout\,
	datad => \inst21|seven_seg0\(4),
	combout => \inst21|seven_seg0\(4));

-- Location: LCCOMB_X24_Y27_N24
\inst21|comb~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~17_combout\ = (inst12(1) & (!inst12(3) & ((!inst12(0)) # (!inst12(2))))) # (!inst12(1) & (inst12(2) $ (((inst12(3)) # (!inst12(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011000110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~17_combout\);

-- Location: LCCOMB_X24_Y27_N22
\inst21|comb~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~18_combout\ = (!inst12(3) & ((inst12(2) & (inst12(1) $ (!inst12(0)))) # (!inst12(2) & (!inst12(1) & inst12(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~18_combout\);

-- Location: LCCOMB_X24_Y27_N28
\inst21|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(3) = (!\inst21|comb~17_combout\ & ((\inst21|comb~18_combout\) # (\inst21|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst21|comb~17_combout\,
	datac => \inst21|comb~18_combout\,
	datad => \inst21|seven_seg0\(3),
	combout => \inst21|seven_seg0\(3));

-- Location: LCCOMB_X23_Y27_N2
\inst21|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|Equal0~0_combout\ = (!inst12(0) & (!inst12(2) & (!inst12(3) & inst12(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(0),
	datab => inst12(2),
	datac => inst12(3),
	datad => inst12(1),
	combout => \inst21|Equal0~0_combout\);

-- Location: LCCOMB_X23_Y27_N28
\inst21|comb~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~14_combout\ = (inst12(2) & (((!inst12(3))))) # (!inst12(2) & (((inst12(0) & !inst12(3))) # (!inst12(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(0),
	datab => inst12(2),
	datac => inst12(3),
	datad => inst12(1),
	combout => \inst21|comb~14_combout\);

-- Location: LCCOMB_X23_Y27_N12
\inst21|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(2) = (!\inst21|comb~14_combout\ & ((\inst21|Equal0~0_combout\) # (\inst21|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst21|Equal0~0_combout\,
	datac => \inst21|comb~14_combout\,
	datad => \inst21|seven_seg0\(2),
	combout => \inst21|seven_seg0\(2));

-- Location: LCCOMB_X24_Y27_N16
\inst21|comb~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~19_combout\ = (inst12(2) & (!inst12(3) & (inst12(1) $ (!inst12(0))))) # (!inst12(2) & (((!inst12(1))) # (!inst12(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010100010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~19_combout\);

-- Location: LCCOMB_X24_Y27_N14
\inst21|comb~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~20_combout\ = (inst12(2) & (!inst12(3) & (inst12(1) $ (inst12(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~20_combout\);

-- Location: LCCOMB_X24_Y27_N6
\inst21|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(1) = (!\inst21|comb~19_combout\ & ((\inst21|comb~20_combout\) # (\inst21|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst21|comb~19_combout\,
	datac => \inst21|comb~20_combout\,
	datad => \inst21|seven_seg0\(1),
	combout => \inst21|seven_seg0\(1));

-- Location: LCCOMB_X24_Y27_N10
\inst21|comb~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~16_combout\ = (!inst12(3) & (!inst12(1) & (inst12(2) $ (inst12(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~16_combout\);

-- Location: LCCOMB_X24_Y27_N8
\inst21|comb~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|comb~15_combout\ = (inst12(1) & (((!inst12(3))))) # (!inst12(1) & (inst12(2) $ (((inst12(3)) # (!inst12(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011000110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst12(2),
	datab => inst12(3),
	datac => inst12(1),
	datad => inst12(0),
	combout => \inst21|comb~15_combout\);

-- Location: LCCOMB_X24_Y27_N12
\inst21|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst21|seven_seg0\(0) = (!\inst21|comb~15_combout\ & ((\inst21|comb~16_combout\) # (\inst21|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst21|comb~16_combout\,
	datac => \inst21|comb~15_combout\,
	datad => \inst21|seven_seg0\(0),
	combout => \inst21|seven_seg0\(0));

-- Location: LCCOMB_X23_Y27_N26
\inst15|inst1|LessThan0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|LessThan0~0_combout\ = (\inst15|inst1|number\(3) & ((\inst15|inst1|number\(0)) # ((\inst15|inst1|number\(2)) # (\inst15|inst1|number\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst1|number\(0),
	datab => \inst15|inst1|number\(2),
	datac => \inst15|inst1|number\(3),
	datad => \inst15|inst1|number\(1),
	combout => \inst15|inst1|LessThan0~0_combout\);

-- Location: FF_X23_Y27_N27
\inst15|inst1|ripple\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst|ripple~clkctrl_outclk\,
	d => \inst15|inst1|LessThan0~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst1|ripple~q\);

-- Location: CLKCTRL_G11
\inst15|inst1|ripple~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst15|inst1|ripple~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst15|inst1|ripple~clkctrl_outclk\);

-- Location: LCCOMB_X23_Y26_N16
\inst15|inst2|number~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|number~0_combout\ = (!\inst15|inst2|number\(0) & (((!\inst15|inst2|number\(2) & !\inst15|inst2|number\(1))) # (!\inst15|inst2|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst2|number\(3),
	datab => \inst15|inst2|number\(2),
	datac => \inst15|inst2|number\(0),
	datad => \inst15|inst2|number\(1),
	combout => \inst15|inst2|number~0_combout\);

-- Location: FF_X23_Y26_N17
\inst15|inst2|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst1|ripple~clkctrl_outclk\,
	d => \inst15|inst2|number~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst2|number\(0));

-- Location: LCCOMB_X23_Y26_N2
\inst15|inst2|number~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|number~3_combout\ = (\inst15|inst2|number\(2) & (\inst15|inst2|number\(0) & (!\inst15|inst2|number\(3) & \inst15|inst2|number\(1)))) # (!\inst15|inst2|number\(2) & (!\inst15|inst2|number\(0) & (\inst15|inst2|number\(3) & 
-- !\inst15|inst2|number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst2|number\(2),
	datab => \inst15|inst2|number\(0),
	datac => \inst15|inst2|number\(3),
	datad => \inst15|inst2|number\(1),
	combout => \inst15|inst2|number~3_combout\);

-- Location: FF_X23_Y26_N3
\inst15|inst2|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst1|ripple~clkctrl_outclk\,
	d => \inst15|inst2|number~3_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst2|number\(3));

-- Location: LCCOMB_X23_Y26_N24
\inst15|inst2|number~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|number~2_combout\ = (!\inst15|inst2|number\(3) & (\inst15|inst2|number\(1) $ (\inst15|inst2|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst15|inst2|number\(3),
	datac => \inst15|inst2|number\(1),
	datad => \inst15|inst2|number\(0),
	combout => \inst15|inst2|number~2_combout\);

-- Location: FF_X23_Y26_N25
\inst15|inst2|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst1|ripple~clkctrl_outclk\,
	d => \inst15|inst2|number~2_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst2|number\(1));

-- Location: LCCOMB_X23_Y26_N28
\inst15|inst2|number~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|number~1_combout\ = (!\inst15|inst2|number\(3) & (\inst15|inst2|number\(2) $ (((\inst15|inst2|number\(1) & \inst15|inst2|number\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst2|number\(1),
	datab => \inst15|inst2|number\(3),
	datac => \inst15|inst2|number\(2),
	datad => \inst15|inst2|number\(0),
	combout => \inst15|inst2|number~1_combout\);

-- Location: FF_X23_Y26_N29
\inst15|inst2|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst1|ripple~clkctrl_outclk\,
	d => \inst15|inst2|number~1_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst2|number\(2));

-- Location: LCCOMB_X23_Y26_N22
\inst17[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst17(2) = (\BUTTON2~input_o\ & ((\inst15|inst2|number\(2)))) # (!\BUTTON2~input_o\ & (inst17(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst17(2),
	datad => \inst15|inst2|number\(2),
	combout => inst17(2));

-- Location: LCCOMB_X23_Y26_N4
\inst17[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst17(0) = (\BUTTON2~input_o\ & ((\inst15|inst2|number\(0)))) # (!\BUTTON2~input_o\ & (inst17(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst17(0),
	datad => \inst15|inst2|number\(0),
	combout => inst17(0));

-- Location: LCCOMB_X23_Y26_N14
\inst17[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst17(3) = (\BUTTON2~input_o\ & ((\inst15|inst2|number\(3)))) # (!\BUTTON2~input_o\ & (inst17(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst17(3),
	datad => \inst15|inst2|number\(3),
	combout => inst17(3));

-- Location: LCCOMB_X23_Y26_N30
\inst17[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst17(1) = (\BUTTON2~input_o\ & ((\inst15|inst2|number\(1)))) # (!\BUTTON2~input_o\ & (inst17(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \BUTTON2~input_o\,
	datac => inst17(1),
	datad => \inst15|inst2|number\(1),
	combout => inst17(1));

-- Location: LCCOMB_X23_Y26_N6
\inst22|comb~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~9_combout\ = (!inst17(3) & ((inst17(2) & (inst17(0) & inst17(1))) # (!inst17(2) & ((!inst17(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(2),
	datab => inst17(0),
	datac => inst17(3),
	datad => inst17(1),
	combout => \inst22|comb~9_combout\);

-- Location: LCCOMB_X23_Y26_N12
\inst22|comb~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~8_combout\ = (inst17(2) & (!inst17(3) & ((!inst17(1)) # (!inst17(0))))) # (!inst17(2) & ((inst17(3) $ (inst17(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(2),
	datab => inst17(0),
	datac => inst17(3),
	datad => inst17(1),
	combout => \inst22|comb~8_combout\);

-- Location: LCCOMB_X23_Y26_N18
\inst22|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(6) = (!\inst22|comb~8_combout\ & ((\inst22|comb~9_combout\) # (\inst22|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst22|comb~9_combout\,
	datac => \inst22|comb~8_combout\,
	datad => \inst22|seven_seg0\(6),
	combout => \inst22|seven_seg0\(6));

-- Location: LCCOMB_X23_Y26_N8
\inst22|comb~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~10_combout\ = (inst17(2) & (!inst17(3) & ((!inst17(1)) # (!inst17(0))))) # (!inst17(2) & (!inst17(1) & ((inst17(3)) # (!inst17(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001001011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(2),
	datab => inst17(0),
	datac => inst17(3),
	datad => inst17(1),
	combout => \inst22|comb~10_combout\);

-- Location: LCCOMB_X23_Y26_N10
\inst22|comb~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~11_combout\ = (!inst17(3) & ((inst17(2) & (inst17(0) & inst17(1))) # (!inst17(2) & ((inst17(0)) # (inst17(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(2),
	datab => inst17(0),
	datac => inst17(3),
	datad => inst17(1),
	combout => \inst22|comb~11_combout\);

-- Location: LCCOMB_X23_Y26_N26
\inst22|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(5) = (!\inst22|comb~10_combout\ & ((\inst22|comb~11_combout\) # (\inst22|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst22|comb~10_combout\,
	datac => \inst22|comb~11_combout\,
	datad => \inst22|seven_seg0\(5),
	combout => \inst22|seven_seg0\(5));

-- Location: LCCOMB_X28_Y26_N0
\inst22|comb~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~12_combout\ = (!inst17(0) & ((inst17(1) & (!inst17(3))) # (!inst17(1) & ((!inst17(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~12_combout\);

-- Location: LCCOMB_X28_Y26_N22
\inst22|comb~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~13_combout\ = (inst17(1) & (!inst17(3) & (inst17(0)))) # (!inst17(1) & ((inst17(2) & (!inst17(3))) # (!inst17(2) & ((inst17(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~13_combout\);

-- Location: LCCOMB_X28_Y26_N24
\inst22|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(4) = (!\inst22|comb~12_combout\ & ((\inst22|comb~13_combout\) # (\inst22|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst22|comb~12_combout\,
	datac => \inst22|comb~13_combout\,
	datad => \inst22|seven_seg0\(4),
	combout => \inst22|seven_seg0\(4));

-- Location: LCCOMB_X28_Y26_N4
\inst22|comb~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~17_combout\ = (inst17(1) & (!inst17(3) & ((!inst17(2)) # (!inst17(0))))) # (!inst17(1) & (inst17(2) $ (((inst17(3)) # (!inst17(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010001011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~17_combout\);

-- Location: LCCOMB_X28_Y26_N30
\inst22|comb~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~18_combout\ = (!inst17(3) & ((inst17(0) & (inst17(1) $ (!inst17(2)))) # (!inst17(0) & (!inst17(1) & inst17(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~18_combout\);

-- Location: LCCOMB_X28_Y26_N18
\inst22|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(3) = (!\inst22|comb~17_combout\ & ((\inst22|comb~18_combout\) # (\inst22|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst22|comb~17_combout\,
	datac => \inst22|comb~18_combout\,
	datad => \inst22|seven_seg0\(3),
	combout => \inst22|seven_seg0\(3));

-- Location: LCCOMB_X28_Y26_N16
\inst22|comb~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~14_combout\ = (inst17(2) & (!inst17(3))) # (!inst17(2) & (((!inst17(3) & inst17(0))) # (!inst17(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~14_combout\);

-- Location: LCCOMB_X28_Y26_N26
\inst22|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|Equal0~0_combout\ = (!inst17(3) & (!inst17(0) & (inst17(1) & !inst17(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|Equal0~0_combout\);

-- Location: LCCOMB_X28_Y26_N28
\inst22|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(2) = (!\inst22|comb~14_combout\ & ((\inst22|Equal0~0_combout\) # (\inst22|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst22|comb~14_combout\,
	datac => \inst22|Equal0~0_combout\,
	datad => \inst22|seven_seg0\(2),
	combout => \inst22|seven_seg0\(2));

-- Location: LCCOMB_X28_Y26_N12
\inst22|comb~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~19_combout\ = (inst17(3) & (((!inst17(1) & !inst17(2))))) # (!inst17(3) & ((inst17(0) $ (!inst17(1))) # (!inst17(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~19_combout\);

-- Location: LCCOMB_X28_Y26_N14
\inst22|comb~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~20_combout\ = (!inst17(3) & (inst17(2) & (inst17(0) $ (inst17(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~20_combout\);

-- Location: LCCOMB_X28_Y26_N6
\inst22|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(1) = (!\inst22|comb~19_combout\ & ((\inst22|comb~20_combout\) # (\inst22|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst22|comb~19_combout\,
	datac => \inst22|comb~20_combout\,
	datad => \inst22|seven_seg0\(1),
	combout => \inst22|seven_seg0\(1));

-- Location: LCCOMB_X28_Y26_N10
\inst22|comb~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~16_combout\ = (!inst17(3) & (!inst17(1) & (inst17(0) $ (inst17(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~16_combout\);

-- Location: LCCOMB_X28_Y26_N8
\inst22|comb~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|comb~15_combout\ = (inst17(1) & (!inst17(3))) # (!inst17(1) & (inst17(2) $ (((inst17(3)) # (!inst17(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst17(3),
	datab => inst17(0),
	datac => inst17(1),
	datad => inst17(2),
	combout => \inst22|comb~15_combout\);

-- Location: LCCOMB_X28_Y26_N20
\inst22|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst22|seven_seg0\(0) = (!\inst22|comb~15_combout\ & ((\inst22|comb~16_combout\) # (\inst22|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst22|comb~16_combout\,
	datac => \inst22|comb~15_combout\,
	datad => \inst22|seven_seg0\(0),
	combout => \inst22|seven_seg0\(0));

-- Location: LCCOMB_X23_Y26_N20
\inst15|inst2|LessThan0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|LessThan0~0_combout\ = (\inst15|inst2|number\(3) & ((\inst15|inst2|number\(2)) # ((\inst15|inst2|number\(0)) # (\inst15|inst2|number\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst2|number\(2),
	datab => \inst15|inst2|number\(3),
	datac => \inst15|inst2|number\(0),
	datad => \inst15|inst2|number\(1),
	combout => \inst15|inst2|LessThan0~0_combout\);

-- Location: FF_X23_Y26_N21
\inst15|inst2|ripple\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst1|ripple~clkctrl_outclk\,
	d => \inst15|inst2|LessThan0~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst2|ripple~q\);

-- Location: CLKCTRL_G12
\inst15|inst2|ripple~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst15|inst2|ripple~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst15|inst2|ripple~clkctrl_outclk\);

-- Location: LCCOMB_X24_Y26_N28
\inst15|inst3|number~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|number~2_combout\ = (!\inst15|inst3|number\(3) & (\inst15|inst3|number\(1) $ (\inst15|inst3|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|number\(3),
	datac => \inst15|inst3|number\(1),
	datad => \inst15|inst3|number\(0),
	combout => \inst15|inst3|number~2_combout\);

-- Location: FF_X24_Y26_N29
\inst15|inst3|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst2|ripple~clkctrl_outclk\,
	d => \inst15|inst3|number~2_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst3|number\(1));

-- Location: LCCOMB_X24_Y26_N18
\inst15|inst3|number~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|number~1_combout\ = (!\inst15|inst3|number\(3) & (\inst15|inst3|number\(2) $ (((\inst15|inst3|number\(1) & \inst15|inst3|number\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|number\(3),
	datab => \inst15|inst3|number\(1),
	datac => \inst15|inst3|number\(2),
	datad => \inst15|inst3|number\(0),
	combout => \inst15|inst3|number~1_combout\);

-- Location: FF_X24_Y26_N19
\inst15|inst3|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst2|ripple~clkctrl_outclk\,
	d => \inst15|inst3|number~1_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst3|number\(2));

-- Location: LCCOMB_X24_Y26_N12
\inst15|inst3|number~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|number~0_combout\ = (!\inst15|inst3|number\(0) & (((!\inst15|inst3|number\(2) & !\inst15|inst3|number\(1))) # (!\inst15|inst3|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|number\(3),
	datab => \inst15|inst3|number\(2),
	datac => \inst15|inst3|number\(0),
	datad => \inst15|inst3|number\(1),
	combout => \inst15|inst3|number~0_combout\);

-- Location: FF_X24_Y26_N13
\inst15|inst3|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst2|ripple~clkctrl_outclk\,
	d => \inst15|inst3|number~0_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst3|number\(0));

-- Location: LCCOMB_X24_Y26_N10
\inst15|inst3|number~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|number~3_combout\ = (\inst15|inst3|number\(0) & (\inst15|inst3|number\(2) & (!\inst15|inst3|number\(3) & \inst15|inst3|number\(1)))) # (!\inst15|inst3|number\(0) & (!\inst15|inst3|number\(2) & (\inst15|inst3|number\(3) & 
-- !\inst15|inst3|number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|number\(0),
	datab => \inst15|inst3|number\(2),
	datac => \inst15|inst3|number\(3),
	datad => \inst15|inst3|number\(1),
	combout => \inst15|inst3|number~3_combout\);

-- Location: FF_X24_Y26_N11
\inst15|inst3|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst15|inst2|ripple~clkctrl_outclk\,
	d => \inst15|inst3|number~3_combout\,
	clrn => \BUTTON0~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst15|inst3|number\(3));

-- Location: LCCOMB_X24_Y26_N14
\inst19[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst19(3) = (\BUTTON2~input_o\ & ((\inst15|inst3|number\(3)))) # (!\BUTTON2~input_o\ & (inst19(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => inst19(3),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst3|number\(3),
	combout => inst19(3));

-- Location: LCCOMB_X24_Y26_N8
\inst19[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst19(1) = (\BUTTON2~input_o\ & ((\inst15|inst3|number\(1)))) # (!\BUTTON2~input_o\ & (inst19(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => inst19(1),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst3|number\(1),
	combout => inst19(1));

-- Location: LCCOMB_X24_Y26_N26
\inst19[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst19(2) = (\BUTTON2~input_o\ & ((\inst15|inst3|number\(2)))) # (!\BUTTON2~input_o\ & (inst19(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(2),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst3|number\(2),
	combout => inst19(2));

-- Location: LCCOMB_X24_Y26_N4
\inst19[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- inst19(0) = (\BUTTON2~input_o\ & ((\inst15|inst3|number\(0)))) # (!\BUTTON2~input_o\ & (inst19(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => inst19(0),
	datac => \BUTTON2~input_o\,
	datad => \inst15|inst3|number\(0),
	combout => inst19(0));

-- Location: LCCOMB_X32_Y28_N2
\inst23|comb~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~9_combout\ = (!inst19(3) & ((inst19(1) & (inst19(2) & inst19(0))) # (!inst19(1) & (!inst19(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~9_combout\);

-- Location: LCCOMB_X32_Y28_N4
\inst23|comb~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~8_combout\ = (inst19(1) & (!inst19(3) & ((!inst19(0)) # (!inst19(2))))) # (!inst19(1) & (inst19(3) $ ((inst19(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011001010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~8_combout\);

-- Location: LCCOMB_X32_Y28_N20
\inst23|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(6) = (!\inst23|comb~8_combout\ & ((\inst23|comb~9_combout\) # (\inst23|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst23|comb~9_combout\,
	datac => \inst23|comb~8_combout\,
	datad => \inst23|seven_seg0\(6),
	combout => \inst23|seven_seg0\(6));

-- Location: LCCOMB_X24_Y26_N22
\inst23|comb~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~11_combout\ = (!inst19(3) & ((inst19(1) & ((inst19(0)) # (!inst19(2)))) # (!inst19(1) & (inst19(0) & !inst19(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(1),
	datab => inst19(0),
	datac => inst19(2),
	datad => inst19(3),
	combout => \inst23|comb~11_combout\);

-- Location: LCCOMB_X24_Y26_N24
\inst23|comb~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~10_combout\ = (inst19(0) & (!inst19(1) & (inst19(2) $ (inst19(3))))) # (!inst19(0) & ((inst19(2) & ((!inst19(3)))) # (!inst19(2) & (!inst19(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(1),
	datab => inst19(0),
	datac => inst19(2),
	datad => inst19(3),
	combout => \inst23|comb~10_combout\);

-- Location: LCCOMB_X24_Y26_N16
\inst23|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(5) = (!\inst23|comb~10_combout\ & ((\inst23|comb~11_combout\) # (\inst23|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst23|comb~11_combout\,
	datac => \inst23|comb~10_combout\,
	datad => \inst23|seven_seg0\(5),
	combout => \inst23|seven_seg0\(5));

-- Location: LCCOMB_X32_Y28_N14
\inst23|comb~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~13_combout\ = (inst19(1) & (!inst19(3) & ((inst19(0))))) # (!inst19(1) & ((inst19(2) & (!inst19(3))) # (!inst19(2) & ((inst19(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~13_combout\);

-- Location: LCCOMB_X32_Y28_N24
\inst23|comb~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~12_combout\ = (!inst19(0) & ((inst19(1) & (!inst19(3))) # (!inst19(1) & ((!inst19(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~12_combout\);

-- Location: LCCOMB_X33_Y28_N28
\inst23|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(4) = (!\inst23|comb~12_combout\ & ((\inst23|comb~13_combout\) # (\inst23|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst23|comb~13_combout\,
	datab => \inst23|comb~12_combout\,
	datad => \inst23|seven_seg0\(4),
	combout => \inst23|seven_seg0\(4));

-- Location: LCCOMB_X32_Y28_N16
\inst23|comb~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~17_combout\ = (inst19(1) & (!inst19(3) & ((!inst19(0)) # (!inst19(2))))) # (!inst19(1) & (inst19(2) $ (((inst19(3)) # (!inst19(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011001000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~17_combout\);

-- Location: LCCOMB_X32_Y28_N22
\inst23|comb~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~18_combout\ = (!inst19(3) & ((inst19(1) & (inst19(2) & inst19(0))) # (!inst19(1) & (inst19(2) $ (inst19(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~18_combout\);

-- Location: LCCOMB_X32_Y28_N18
\inst23|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(3) = (!\inst23|comb~17_combout\ & ((\inst23|comb~18_combout\) # (\inst23|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst23|comb~17_combout\,
	datac => \inst23|comb~18_combout\,
	datad => \inst23|seven_seg0\(3),
	combout => \inst23|seven_seg0\(3));

-- Location: LCCOMB_X24_Y26_N30
\inst23|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|Equal0~0_combout\ = (inst19(1) & (!inst19(0) & (!inst19(2) & !inst19(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(1),
	datab => inst19(0),
	datac => inst19(2),
	datad => inst19(3),
	combout => \inst23|Equal0~0_combout\);

-- Location: LCCOMB_X24_Y26_N20
\inst23|comb~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~14_combout\ = (inst19(2) & (((!inst19(3))))) # (!inst19(2) & (((inst19(0) & !inst19(3))) # (!inst19(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(1),
	datab => inst19(0),
	datac => inst19(2),
	datad => inst19(3),
	combout => \inst23|comb~14_combout\);

-- Location: LCCOMB_X24_Y26_N6
\inst23|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(2) = (!\inst23|comb~14_combout\ & ((\inst23|Equal0~0_combout\) # (\inst23|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst23|Equal0~0_combout\,
	datac => \inst23|comb~14_combout\,
	datad => \inst23|seven_seg0\(2),
	combout => \inst23|seven_seg0\(2));

-- Location: LCCOMB_X32_Y28_N30
\inst23|comb~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~20_combout\ = (!inst19(3) & (inst19(2) & (inst19(1) $ (inst19(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~20_combout\);

-- Location: LCCOMB_X32_Y28_N8
\inst23|comb~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~19_combout\ = (inst19(3) & (!inst19(1) & (!inst19(2)))) # (!inst19(3) & ((inst19(1) $ (!inst19(0))) # (!inst19(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011100010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~19_combout\);

-- Location: LCCOMB_X32_Y28_N28
\inst23|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(1) = (!\inst23|comb~19_combout\ & ((\inst23|comb~20_combout\) # (\inst23|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst23|comb~20_combout\,
	datac => \inst23|comb~19_combout\,
	datad => \inst23|seven_seg0\(1),
	combout => \inst23|seven_seg0\(1));

-- Location: LCCOMB_X32_Y28_N12
\inst23|comb~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~15_combout\ = (inst19(1) & (!inst19(3))) # (!inst19(1) & (inst19(2) $ (((inst19(3)) # (!inst19(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011001000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~15_combout\);

-- Location: LCCOMB_X32_Y28_N26
\inst23|comb~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|comb~16_combout\ = (!inst19(3) & (!inst19(1) & (inst19(2) $ (inst19(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => inst19(3),
	datab => inst19(1),
	datac => inst19(2),
	datad => inst19(0),
	combout => \inst23|comb~16_combout\);

-- Location: LCCOMB_X32_Y28_N10
\inst23|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst23|seven_seg0\(0) = (!\inst23|comb~15_combout\ & ((\inst23|comb~16_combout\) # (\inst23|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst23|comb~15_combout\,
	datac => \inst23|comb~16_combout\,
	datad => \inst23|seven_seg0\(0),
	combout => \inst23|seven_seg0\(0));

ww_D0(6) <= \D0[6]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;
END structure;


