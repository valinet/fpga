-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "03/16/2016 05:36:38"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	combinationalmultiplier IS
    PORT (
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	CLOCK : IN std_logic;
	SW : IN std_logic_vector(9 DOWNTO 0);
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0);
	OUTPUT : OUT std_logic_vector(9 DOWNTO 0)
	);
END combinationalmultiplier;

-- Design Ports Information
-- D0[6]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[8]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[7]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[6]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[5]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[3]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[1]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[0]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK	=>  Location: PIN_F1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_G5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_G4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_H6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_H5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_H7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_E3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_E4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF combinationalmultiplier IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_CLOCK : std_logic;
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_OUTPUT : std_logic_vector(9 DOWNTO 0);
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \OUTPUT[9]~output_o\ : std_logic;
SIGNAL \OUTPUT[8]~output_o\ : std_logic;
SIGNAL \OUTPUT[7]~output_o\ : std_logic;
SIGNAL \OUTPUT[6]~output_o\ : std_logic;
SIGNAL \OUTPUT[5]~output_o\ : std_logic;
SIGNAL \OUTPUT[4]~output_o\ : std_logic;
SIGNAL \OUTPUT[3]~output_o\ : std_logic;
SIGNAL \OUTPUT[2]~output_o\ : std_logic;
SIGNAL \OUTPUT[1]~output_o\ : std_logic;
SIGNAL \OUTPUT[0]~output_o\ : std_logic;
SIGNAL \CLOCK~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \inst19|inst19~q\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \inst19|inst41~q\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \inst|inst8~feeder_combout\ : std_logic;
SIGNAL \inst|inst8~q\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \inst19|inst43~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[0]~1_combout\ : std_logic;
SIGNAL \inst45|inst1|inst43~q\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \inst|inst19~feeder_combout\ : std_logic;
SIGNAL \inst|inst19~q\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \inst19|inst42~q\ : std_logic;
SIGNAL \inst15|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst46|inst1|inst42~q\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \inst|inst41~q\ : std_logic;
SIGNAL \inst13|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst46|inst1|inst43~q\ : std_logic;
SIGNAL \inst17|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst17|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst47|inst1|inst41~q\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \inst|inst42~q\ : std_logic;
SIGNAL \inst18|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst47|inst1|inst43~q\ : std_logic;
SIGNAL \inst25|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst17|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst47|inst1|inst42~q\ : std_logic;
SIGNAL \inst25|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst3|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst49|inst1|inst19~q\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \inst19|inst8~feeder_combout\ : std_logic;
SIGNAL \inst19|inst8~q\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \inst|inst43~q\ : std_logic;
SIGNAL \inst25|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst49|inst1|inst42~q\ : std_logic;
SIGNAL \inst26|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst49|inst1|inst43~q\ : std_logic;
SIGNAL \inst32|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst32|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst49|inst1|inst41~q\ : std_logic;
SIGNAL \inst32|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst32|inst4|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst39|inst8~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[2]~0_combout\ : std_logic;
SIGNAL \inst45|inst1|inst41~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[1]~2_combout\ : std_logic;
SIGNAL \inst45|inst1|inst42~q\ : std_logic;
SIGNAL \inst15|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst15|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst3|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst46|inst1|inst19~q\ : std_logic;
SIGNAL \inst15|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst46|inst1|inst41~q\ : std_logic;
SIGNAL \inst17|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst4|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst47|inst1|inst8~q\ : std_logic;
SIGNAL \inst17|inst3|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst47|inst1|inst19~q\ : std_logic;
SIGNAL \inst25|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst49|inst|inst43~q\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[3]~4_combout\ : std_logic;
SIGNAL \inst45|inst1|inst19~q\ : std_logic;
SIGNAL \inst15|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst4|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst46|inst1|inst8~q\ : std_logic;
SIGNAL \inst17|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst47|inst|inst43~q\ : std_logic;
SIGNAL \inst25|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst49|inst|inst42~q\ : std_logic;
SIGNAL \inst25|inst4|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst49|inst1|inst8~q\ : std_logic;
SIGNAL \inst32|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst32|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst41~q\ : std_logic;
SIGNAL \inst32|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst39|inst41~q\ : std_logic;
SIGNAL \inst32|inst3|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst39|inst19~q\ : std_logic;
SIGNAL \inst32|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst39|inst42~q\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~68_combout\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[4]~3_combout\ : std_logic;
SIGNAL \inst45|inst1|inst8~q\ : std_logic;
SIGNAL \inst15|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst46|inst|inst43~q\ : std_logic;
SIGNAL \inst17|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst47|inst|inst42~q\ : std_logic;
SIGNAL \inst25|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst49|inst|inst41~q\ : std_logic;
SIGNAL \inst32|inst488|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst32|inst488|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst19~q\ : std_logic;
SIGNAL \inst32|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst42~q\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~39_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~105_combout\ : std_logic;
SIGNAL \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst39|inst43~q\ : std_logic;
SIGNAL \inst25|inst488|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst~combout\ : std_logic;
SIGNAL \inst46|inst|inst42~q\ : std_logic;
SIGNAL \inst17|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst47|inst|inst41~q\ : std_logic;
SIGNAL \inst25|inst488|inst1|inst5~combout\ : std_logic;
SIGNAL \inst49|inst|inst19~q\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst8~q\ : std_logic;
SIGNAL \inst32|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst38|inst43~q\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~40_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~106_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~281_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~108_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~282_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~97_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~66_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~71_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~273_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~274_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~299_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~300_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~293_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~103_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~280_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~104_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~109_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~272_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~80_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~88_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~275_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~92_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~93_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~82_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~90_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~91_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~94_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~89_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~95_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~81_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~98_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~99_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~100_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~85_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~84_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~86_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~101_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~277_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~278_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~83_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~87_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~276_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~279_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~102_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~110_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~112_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~118_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~119_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~111_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~113_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~115_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~114_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~116_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~117_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~120_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~145_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~147_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~146_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~148_combout\ : std_logic;
SIGNAL \inst42|Equal0~3_combout\ : std_logic;
SIGNAL \inst42|Equal0~2_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~129_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~297_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~298_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~149_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~142_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~123_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~143_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~144_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~150_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~283_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~131_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~130_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~133_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~132_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~134_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~124_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~125_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~126_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~127_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~128_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~121_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~122_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~135_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~138_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~284_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~139_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~285_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~136_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~140_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~141_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~151_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~41_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~175_combout\ : std_logic;
SIGNAL \inst42|Equal0~4_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~154_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~153_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~155_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~156_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~152_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~157_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~158_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~165_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~166_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~169_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~170_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~171_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~172_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~159_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~160_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~161_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~162_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~163_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~164_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~167_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~168_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~173_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~174_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[4]~176_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~189_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~190_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~191_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~192_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~180_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~193_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~194_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~195_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~177_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~178_combout\ : std_logic;
SIGNAL \inst42|Equal0~5_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~182_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~183_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~96_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~184_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~107_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~179_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~181_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~185_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~286_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~186_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~187_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~188_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[3]~287_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~196_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~207_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~206_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~208_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~200_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~205_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~209_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~197_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~198_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~203_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~199_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~204_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~210_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~212_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~213_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~211_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~214_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~201_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~202_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~215_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[2]~216_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~228_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~231_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~294_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~227_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~229_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~230_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~232_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~234_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~233_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~235_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~236_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~217_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~239_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~240_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~137_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~241_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~238_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~237_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~288_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~218_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~289_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~290_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~242_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~243_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~255_combout\ : std_logic;
SIGNAL \inst42|Equal0~8_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~256_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~253_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~254_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~252_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~257_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~244_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~245_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~246_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[5]~249_combout\ : std_logic;
SIGNAL \inst42|Equal0~7_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~247_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~248_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~291_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~250_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~251_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~258_combout\ : std_logic;
SIGNAL \inst42|Equal0~6_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~224_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~225_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~219_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~226_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~220_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~221_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~222_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~223_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~295_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~296_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[1]~259_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~261_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~262_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~260_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~263_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~264_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[0]~292_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~46_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~47_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~48_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~49_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~50_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~51_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~265_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~54_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~55_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~56_combout\ : std_logic;
SIGNAL \inst42|Equal0~0_combout\ : std_logic;
SIGNAL \inst42|Equal0~9_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~52_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~53_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~57_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~44_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~43_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~42_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~45_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~58_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~71_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~59_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~248_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~60_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~65_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~66_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~61_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~62_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~63_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~64_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~68_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[6]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~262_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~263_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~80_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~81_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~82_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~102_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~103_combout\ : std_logic;
SIGNAL \inst42|Equal0~11_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~105_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~104_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~250_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~106_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~107_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~108_combout\ : std_logic;
SIGNAL \inst42|Equal0~12_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~109_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~110_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~111_combout\ : std_logic;
SIGNAL \inst42|Equal0~10_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~99_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~249_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~94_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~93_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~95_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~92_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~96_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~97_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~98_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~100_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~88_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~89_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~90_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~86_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~84_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~83_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~85_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~87_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~91_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~101_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~112_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~119_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~113_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~137_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~138_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~136_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~139_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~140_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~144_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~141_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~142_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~143_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~145_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~146_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~115_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~133_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~134_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~135_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~150_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~257_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~251_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~151_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~148_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~149_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~147_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~152_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~153_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~117_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~114_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~116_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~118_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~120_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~121_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~122_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~260_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~261_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~123_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~124_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~125_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~126_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~127_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~128_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~129_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~130_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~131_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~132_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[4]~154_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~269_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~267_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~268_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~160_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~266_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~161_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~166_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~165_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~167_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~163_combout\ : std_logic;
SIGNAL \inst42|Equal0~16_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~162_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~164_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~168_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~169_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~27_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~259_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~170_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~171_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~270_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~172_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~173_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~158_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~157_combout\ : std_logic;
SIGNAL \inst42|Equal0~1_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~155_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~156_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~159_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~174_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~179_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~180_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~253_combout\ : std_logic;
SIGNAL \inst42|seven_seg0[6]~271_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~176_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~177_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~178_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~181_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~182_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~183_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~252_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~184_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~185_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~194_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~195_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~196_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~197_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~198_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~199_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~190_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~191_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~192_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~193_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~200_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~201_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~254_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~255_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~202_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~203_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~204_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~186_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~187_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[5]~175_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~188_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~189_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[2]~205_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~222_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~223_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~224_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~220_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~221_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~225_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~214_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~215_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~216_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~217_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~218_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~213_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~219_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~226_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~209_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~210_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~211_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~206_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~207_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~208_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~212_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~233_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~232_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~256_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~227_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~228_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~229_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~230_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~231_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~234_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~235_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~241_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~242_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~258_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~243_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~244_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~240_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~245_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~236_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~237_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[3]~238_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~239_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[0]~246_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~58_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~57_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~59_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~18_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~15_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~99_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~95_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[6]~60_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~66_combout\ : std_logic;
SIGNAL \inst42|Equal0~13_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~56_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~64_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~63_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~61_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~62_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~65_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~67_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[5]~68_combout\ : std_logic;
SIGNAL \inst42|seven_seg1[1]~247_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~69_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~70_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~71_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~72_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~73_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~74_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~76_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~75_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~77_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[4]~78_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~82_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~83_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~79_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~80_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~81_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[3]~84_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~98_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~87_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~88_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~85_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~86_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[2]~89_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~40_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~37_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~100_combout\ : std_logic;
SIGNAL \inst42|Equal0~14_combout\ : std_logic;
SIGNAL \inst42|Equal0~15_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~90_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~91_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[1]~92_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~97_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~96_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~93_combout\ : std_logic;
SIGNAL \inst42|seven_seg2[0]~94_combout\ : std_logic;
SIGNAL \inst42|seven_seg3[2]~0_combout\ : std_logic;
SIGNAL \ALT_INV_CLOCK~input_o\ : std_logic;

BEGIN

D0 <= ww_D0;
ww_CLOCK <= CLOCK;
ww_SW <= SW;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
OUTPUT <= ww_OUTPUT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_CLOCK~input_o\ <= NOT \CLOCK~input_o\;

-- Location: IOOBUF_X26_Y29_N16
\D0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[6]~110_combout\,
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\D0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[5]~151_combout\,
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\D0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[4]~176_combout\,
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\D0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[3]~287_combout\,
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\D0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[2]~216_combout\,
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\D0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[1]~259_combout\,
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\D0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg0[0]~292_combout\,
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\D1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[6]~74_combout\,
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\D1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[5]~112_combout\,
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\D1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[4]~154_combout\,
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\D1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[3]~185_combout\,
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\D1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[2]~205_combout\,
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\D1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[1]~235_combout\,
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\D1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg1[0]~246_combout\,
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\D2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[6]~60_combout\,
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\D2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[5]~68_combout\,
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\D2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[4]~78_combout\,
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\D2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[3]~84_combout\,
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\D2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[2]~89_combout\,
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\D2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[1]~92_combout\,
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\D2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg2[0]~94_combout\,
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\D3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\D3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\D3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\D3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\D3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\D3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst42|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\D3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOOBUF_X0_Y27_N16
\OUTPUT[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst8~q\,
	devoe => ww_devoe,
	o => \OUTPUT[9]~output_o\);

-- Location: IOOBUF_X0_Y27_N9
\OUTPUT[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst19~q\,
	devoe => ww_devoe,
	o => \OUTPUT[8]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\OUTPUT[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst41~q\,
	devoe => ww_devoe,
	o => \OUTPUT[7]~output_o\);

-- Location: IOOBUF_X0_Y26_N23
\OUTPUT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst42~q\,
	devoe => ww_devoe,
	o => \OUTPUT[6]~output_o\);

-- Location: IOOBUF_X0_Y24_N16
\OUTPUT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst38|inst43~q\,
	devoe => ww_devoe,
	o => \OUTPUT[5]~output_o\);

-- Location: IOOBUF_X0_Y24_N23
\OUTPUT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst8~q\,
	devoe => ww_devoe,
	o => \OUTPUT[4]~output_o\);

-- Location: IOOBUF_X0_Y21_N16
\OUTPUT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst19~q\,
	devoe => ww_devoe,
	o => \OUTPUT[3]~output_o\);

-- Location: IOOBUF_X0_Y21_N23
\OUTPUT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst41~q\,
	devoe => ww_devoe,
	o => \OUTPUT[2]~output_o\);

-- Location: IOOBUF_X0_Y20_N2
\OUTPUT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst42~q\,
	devoe => ww_devoe,
	o => \OUTPUT[1]~output_o\);

-- Location: IOOBUF_X0_Y20_N9
\OUTPUT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst43~q\,
	devoe => ww_devoe,
	o => \OUTPUT[0]~output_o\);

-- Location: IOIBUF_X0_Y23_N1
\CLOCK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK,
	o => \CLOCK~input_o\);

-- Location: IOIBUF_X0_Y23_N8
\SW[3]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: FF_X2_Y23_N15
\inst19|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[3]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst19~q\);

-- Location: IOIBUF_X0_Y25_N22
\SW[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: FF_X2_Y23_N21
\inst19|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[2]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst41~q\);

-- Location: IOIBUF_X0_Y25_N1
\SW[9]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LCCOMB_X1_Y23_N26
\inst|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst8~feeder_combout\ = \SW[9]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[9]~input_o\,
	combout => \inst|inst8~feeder_combout\);

-- Location: FF_X1_Y23_N27
\inst|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst8~q\);

-- Location: IOIBUF_X0_Y24_N1
\SW[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: FF_X1_Y23_N9
\inst19|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[0]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst43~q\);

-- Location: LCCOMB_X4_Y23_N28
\inst9|LPM_MUX_component|auto_generated|result_node[0]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[0]~1_combout\ = (\inst|inst8~q\ & \inst19|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst8~q\,
	datad => \inst19|inst43~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[0]~1_combout\);

-- Location: FF_X4_Y23_N29
\inst45|inst1|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst9|LPM_MUX_component|auto_generated|result_node[0]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst45|inst1|inst43~q\);

-- Location: IOIBUF_X0_Y26_N1
\SW[8]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: LCCOMB_X4_Y23_N8
\inst|inst19~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|inst19~feeder_combout\ = \SW[8]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[8]~input_o\,
	combout => \inst|inst19~feeder_combout\);

-- Location: FF_X4_Y23_N9
\inst|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst|inst19~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst19~q\);

-- Location: IOIBUF_X0_Y27_N1
\SW[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: FF_X2_Y23_N23
\inst19|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst42~q\);

-- Location: LCCOMB_X1_Y23_N0
\inst15|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst5~combout\ = \inst45|inst1|inst43~q\ $ (((\inst|inst19~q\ & \inst19|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst45|inst1|inst43~q\,
	datab => \inst|inst19~q\,
	datac => \inst19|inst42~q\,
	combout => \inst15|inst1|inst|inst5~combout\);

-- Location: FF_X1_Y23_N1
\inst46|inst1|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst1|inst|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst1|inst42~q\);

-- Location: IOIBUF_X0_Y26_N8
\SW[7]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: FF_X2_Y23_N19
\inst|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[7]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst41~q\);

-- Location: LCCOMB_X1_Y23_N10
\inst13|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\inst|inst19~q\ & \inst19|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst19~q\,
	datad => \inst19|inst43~q\,
	combout => \inst13|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: FF_X1_Y23_N11
\inst46|inst1|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst13|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst1|inst43~q\);

-- Location: LCCOMB_X2_Y23_N18
\inst17|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst~combout\ = (\inst19|inst42~q\ & (\inst|inst41~q\ & \inst46|inst1|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datac => \inst|inst41~q\,
	datad => \inst46|inst1|inst43~q\,
	combout => \inst17|inst1|inst|inst~combout\);

-- Location: LCCOMB_X1_Y23_N6
\inst17|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst1|inst5~combout\ = \inst46|inst1|inst42~q\ $ (\inst17|inst1|inst|inst~combout\ $ (((\inst19|inst41~q\ & \inst|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst41~q\,
	datab => \inst46|inst1|inst42~q\,
	datac => \inst|inst41~q\,
	datad => \inst17|inst1|inst|inst~combout\,
	combout => \inst17|inst2|inst1|inst5~combout\);

-- Location: FF_X1_Y23_N7
\inst47|inst1|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst2|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst1|inst41~q\);

-- Location: IOIBUF_X0_Y25_N15
\SW[6]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: FF_X2_Y23_N7
\inst|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[6]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst42~q\);

-- Location: LCCOMB_X1_Y23_N8
\inst18|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst18|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\inst19|inst43~q\ & \inst|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst19|inst43~q\,
	datad => \inst|inst41~q\,
	combout => \inst18|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: FF_X2_Y23_N29
\inst47|inst1|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \inst18|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst1|inst43~q\);

-- Location: LCCOMB_X2_Y23_N6
\inst25|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst~combout\ = (\inst19|inst42~q\ & (\inst|inst42~q\ & \inst47|inst1|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datac => \inst|inst42~q\,
	datad => \inst47|inst1|inst43~q\,
	combout => \inst25|inst1|inst|inst~combout\);

-- Location: LCCOMB_X2_Y23_N4
\inst17|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst5~combout\ = \inst46|inst1|inst43~q\ $ (((\inst|inst41~q\ & \inst19|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst46|inst1|inst43~q\,
	datac => \inst|inst41~q\,
	datad => \inst19|inst42~q\,
	combout => \inst17|inst1|inst|inst5~combout\);

-- Location: FF_X2_Y23_N5
\inst47|inst1|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst1|inst|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst1|inst42~q\);

-- Location: LCCOMB_X2_Y23_N2
\inst25|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst2~0_combout\ = (\inst25|inst1|inst|inst~combout\ & ((\inst47|inst1|inst42~q\) # ((\inst19|inst41~q\ & \inst|inst42~q\)))) # (!\inst25|inst1|inst|inst~combout\ & (\inst47|inst1|inst42~q\ & (\inst19|inst41~q\ & \inst|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst1|inst|inst~combout\,
	datab => \inst47|inst1|inst42~q\,
	datac => \inst19|inst41~q\,
	datad => \inst|inst42~q\,
	combout => \inst25|inst2|inst2~0_combout\);

-- Location: LCCOMB_X1_Y23_N14
\inst25|inst3|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst1|inst5~0_combout\ = \inst47|inst1|inst41~q\ $ (\inst25|inst2|inst2~0_combout\ $ (((\inst19|inst19~q\ & \inst|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst47|inst1|inst41~q\,
	datac => \inst|inst42~q\,
	datad => \inst25|inst2|inst2~0_combout\,
	combout => \inst25|inst3|inst1|inst5~0_combout\);

-- Location: FF_X1_Y23_N15
\inst49|inst1|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst3|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst1|inst19~q\);

-- Location: IOIBUF_X0_Y27_N22
\SW[4]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: LCCOMB_X1_Y23_N18
\inst19|inst8~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst19|inst8~feeder_combout\ = \SW[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[4]~input_o\,
	combout => \inst19|inst8~feeder_combout\);

-- Location: FF_X1_Y23_N19
\inst19|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst19|inst8~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst19|inst8~q\);

-- Location: IOIBUF_X0_Y22_N15
\SW[5]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: FF_X2_Y23_N25
\inst|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	asdata => \SW[5]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|inst43~q\);

-- Location: LCCOMB_X1_Y23_N30
\inst25|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst5~combout\ = \inst47|inst1|inst43~q\ $ (((\inst19|inst42~q\ & \inst|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110001101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datab => \inst47|inst1|inst43~q\,
	datac => \inst|inst42~q\,
	combout => \inst25|inst1|inst|inst5~combout\);

-- Location: FF_X1_Y23_N31
\inst49|inst1|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst1|inst|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst1|inst42~q\);

-- Location: LCCOMB_X1_Y23_N24
\inst26|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst26|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\inst19|inst43~q\ & \inst|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst43~q\,
	datad => \inst|inst42~q\,
	combout => \inst26|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: FF_X1_Y23_N25
\inst49|inst1|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst26|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst1|inst43~q\);

-- Location: LCCOMB_X2_Y23_N28
\inst32|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst~combout\ = (\inst19|inst42~q\ & (\inst49|inst1|inst43~q\ & \inst|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datab => \inst49|inst1|inst43~q\,
	datad => \inst|inst43~q\,
	combout => \inst32|inst1|inst|inst~combout\);

-- Location: LCCOMB_X2_Y23_N24
\inst32|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst2~0_combout\ = (\inst49|inst1|inst42~q\ & ((\inst32|inst1|inst|inst~combout\) # ((\inst19|inst41~q\ & \inst|inst43~q\)))) # (!\inst49|inst1|inst42~q\ & (\inst19|inst41~q\ & (\inst|inst43~q\ & \inst32|inst1|inst|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst41~q\,
	datab => \inst49|inst1|inst42~q\,
	datac => \inst|inst43~q\,
	datad => \inst32|inst1|inst|inst~combout\,
	combout => \inst32|inst2|inst2~0_combout\);

-- Location: LCCOMB_X1_Y23_N20
\inst25|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst1|inst5~combout\ = \inst47|inst1|inst42~q\ $ (\inst25|inst1|inst|inst~combout\ $ (((\inst|inst42~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst47|inst1|inst42~q\,
	datac => \inst19|inst41~q\,
	datad => \inst25|inst1|inst|inst~combout\,
	combout => \inst25|inst2|inst1|inst5~combout\);

-- Location: FF_X1_Y23_N21
\inst49|inst1|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst2|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst1|inst41~q\);

-- Location: LCCOMB_X2_Y23_N10
\inst32|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst2~0_combout\ = (\inst32|inst2|inst2~0_combout\ & ((\inst49|inst1|inst41~q\) # ((\inst19|inst19~q\ & \inst|inst43~q\)))) # (!\inst32|inst2|inst2~0_combout\ & (\inst19|inst19~q\ & (\inst|inst43~q\ & \inst49|inst1|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst43~q\,
	datac => \inst32|inst2|inst2~0_combout\,
	datad => \inst49|inst1|inst41~q\,
	combout => \inst32|inst3|inst2~0_combout\);

-- Location: LCCOMB_X2_Y23_N26
\inst32|inst4|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst1|inst5~0_combout\ = \inst49|inst1|inst19~q\ $ (\inst32|inst3|inst2~0_combout\ $ (((\inst19|inst8~q\ & \inst|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst1|inst19~q\,
	datab => \inst19|inst8~q\,
	datac => \inst|inst43~q\,
	datad => \inst32|inst3|inst2~0_combout\,
	combout => \inst32|inst4|inst1|inst5~0_combout\);

-- Location: FF_X2_Y23_N27
\inst39|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst4|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst8~q\);

-- Location: LCCOMB_X4_Y23_N26
\inst9|LPM_MUX_component|auto_generated|result_node[2]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[2]~0_combout\ = (\inst|inst8~q\ & \inst19|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst8~q\,
	datac => \inst19|inst41~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[2]~0_combout\);

-- Location: FF_X4_Y23_N27
\inst45|inst1|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst9|LPM_MUX_component|auto_generated|result_node[2]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst45|inst1|inst41~q\);

-- Location: LCCOMB_X4_Y23_N12
\inst9|LPM_MUX_component|auto_generated|result_node[1]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[1]~2_combout\ = (\inst|inst8~q\ & \inst19|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst8~q\,
	datad => \inst19|inst42~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[1]~2_combout\);

-- Location: FF_X4_Y23_N13
\inst45|inst1|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst9|LPM_MUX_component|auto_generated|result_node[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst45|inst1|inst42~q\);

-- Location: LCCOMB_X4_Y23_N2
\inst15|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst~combout\ = (\inst45|inst1|inst43~q\ & (\inst|inst19~q\ & \inst19|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst45|inst1|inst43~q\,
	datac => \inst|inst19~q\,
	datad => \inst19|inst42~q\,
	combout => \inst15|inst1|inst|inst~combout\);

-- Location: LCCOMB_X4_Y23_N18
\inst15|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst2~0_combout\ = (\inst45|inst1|inst42~q\ & ((\inst15|inst1|inst|inst~combout\) # ((\inst|inst19~q\ & \inst19|inst41~q\)))) # (!\inst45|inst1|inst42~q\ & (\inst|inst19~q\ & (\inst19|inst41~q\ & \inst15|inst1|inst|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst45|inst1|inst42~q\,
	datab => \inst|inst19~q\,
	datac => \inst19|inst41~q\,
	datad => \inst15|inst1|inst|inst~combout\,
	combout => \inst15|inst2|inst2~0_combout\);

-- Location: LCCOMB_X4_Y23_N16
\inst15|inst3|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst1|inst5~0_combout\ = \inst45|inst1|inst41~q\ $ (\inst15|inst2|inst2~0_combout\ $ (((\inst|inst19~q\ & \inst19|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst45|inst1|inst41~q\,
	datab => \inst|inst19~q\,
	datac => \inst19|inst19~q\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst1|inst5~0_combout\);

-- Location: FF_X4_Y23_N17
\inst46|inst1|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst3|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst1|inst19~q\);

-- Location: LCCOMB_X3_Y23_N26
\inst15|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst1|inst5~combout\ = \inst45|inst1|inst42~q\ $ (\inst15|inst1|inst|inst~combout\ $ (((\inst|inst19~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst19~q\,
	datab => \inst19|inst41~q\,
	datac => \inst45|inst1|inst42~q\,
	datad => \inst15|inst1|inst|inst~combout\,
	combout => \inst15|inst2|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N27
\inst46|inst1|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst2|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst1|inst41~q\);

-- Location: LCCOMB_X2_Y23_N20
\inst17|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst2~0_combout\ = (\inst46|inst1|inst42~q\ & ((\inst17|inst1|inst|inst~combout\) # ((\inst|inst41~q\ & \inst19|inst41~q\)))) # (!\inst46|inst1|inst42~q\ & (\inst|inst41~q\ & (\inst19|inst41~q\ & \inst17|inst1|inst|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst46|inst1|inst42~q\,
	datac => \inst19|inst41~q\,
	datad => \inst17|inst1|inst|inst~combout\,
	combout => \inst17|inst2|inst2~0_combout\);

-- Location: LCCOMB_X2_Y23_N14
\inst17|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst2~0_combout\ = (\inst46|inst1|inst41~q\ & ((\inst17|inst2|inst2~0_combout\) # ((\inst|inst41~q\ & \inst19|inst19~q\)))) # (!\inst46|inst1|inst41~q\ & (\inst|inst41~q\ & (\inst19|inst19~q\ & \inst17|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst46|inst1|inst41~q\,
	datac => \inst19|inst19~q\,
	datad => \inst17|inst2|inst2~0_combout\,
	combout => \inst17|inst3|inst2~0_combout\);

-- Location: LCCOMB_X3_Y23_N22
\inst17|inst4|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst1|inst5~0_combout\ = \inst46|inst1|inst19~q\ $ (\inst17|inst3|inst2~0_combout\ $ (((\inst|inst41~q\ & \inst19|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst46|inst1|inst19~q\,
	datac => \inst19|inst8~q\,
	datad => \inst17|inst3|inst2~0_combout\,
	combout => \inst17|inst4|inst1|inst5~0_combout\);

-- Location: FF_X3_Y23_N23
\inst47|inst1|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst4|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst1|inst8~q\);

-- Location: LCCOMB_X1_Y23_N4
\inst17|inst3|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst1|inst5~0_combout\ = \inst17|inst2|inst2~0_combout\ $ (\inst46|inst1|inst41~q\ $ (((\inst19|inst19~q\ & \inst|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst41~q\,
	datac => \inst17|inst2|inst2~0_combout\,
	datad => \inst46|inst1|inst41~q\,
	combout => \inst17|inst3|inst1|inst5~0_combout\);

-- Location: FF_X1_Y23_N5
\inst47|inst1|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst3|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst1|inst19~q\);

-- Location: LCCOMB_X2_Y23_N8
\inst25|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst2~0_combout\ = (\inst47|inst1|inst41~q\ & ((\inst25|inst2|inst2~0_combout\) # ((\inst|inst42~q\ & \inst19|inst19~q\)))) # (!\inst47|inst1|inst41~q\ & (\inst|inst42~q\ & (\inst19|inst19~q\ & \inst25|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst19|inst19~q\,
	datac => \inst47|inst1|inst41~q\,
	datad => \inst25|inst2|inst2~0_combout\,
	combout => \inst25|inst3|inst2~0_combout\);

-- Location: LCCOMB_X3_Y23_N16
\inst25|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst2~0_combout\ = (\inst47|inst1|inst19~q\ & ((\inst25|inst3|inst2~0_combout\) # ((\inst19|inst8~q\ & \inst|inst42~q\)))) # (!\inst47|inst1|inst19~q\ & (\inst19|inst8~q\ & (\inst|inst42~q\ & \inst25|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst8~q\,
	datab => \inst47|inst1|inst19~q\,
	datac => \inst|inst42~q\,
	datad => \inst25|inst3|inst2~0_combout\,
	combout => \inst25|inst4|inst2~0_combout\);

-- Location: LCCOMB_X3_Y23_N4
\inst25|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst57|inst1|inst5~combout\ = \inst47|inst1|inst8~q\ $ (\inst25|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst47|inst1|inst8~q\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst57|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N5
\inst49|inst|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst57|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst|inst43~q\);

-- Location: LCCOMB_X4_Y23_N24
\inst9|LPM_MUX_component|auto_generated|result_node[3]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[3]~4_combout\ = (\inst|inst8~q\ & \inst19|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst8~q\,
	datad => \inst19|inst19~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[3]~4_combout\);

-- Location: FF_X4_Y23_N25
\inst45|inst1|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst9|LPM_MUX_component|auto_generated|result_node[3]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst45|inst1|inst19~q\);

-- Location: LCCOMB_X4_Y23_N6
\inst15|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst2~0_combout\ = (\inst45|inst1|inst41~q\ & ((\inst15|inst2|inst2~0_combout\) # ((\inst19|inst19~q\ & \inst|inst19~q\)))) # (!\inst45|inst1|inst41~q\ & (\inst19|inst19~q\ & (\inst|inst19~q\ & \inst15|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst|inst19~q\,
	datac => \inst45|inst1|inst41~q\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst2~0_combout\);

-- Location: LCCOMB_X4_Y23_N20
\inst15|inst4|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst1|inst5~0_combout\ = \inst45|inst1|inst19~q\ $ (\inst15|inst3|inst2~0_combout\ $ (((\inst19|inst8~q\ & \inst|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst8~q\,
	datab => \inst45|inst1|inst19~q\,
	datac => \inst|inst19~q\,
	datad => \inst15|inst3|inst2~0_combout\,
	combout => \inst15|inst4|inst1|inst5~0_combout\);

-- Location: FF_X4_Y23_N21
\inst46|inst1|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst4|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst1|inst8~q\);

-- Location: LCCOMB_X3_Y23_N0
\inst17|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst2~0_combout\ = (\inst46|inst1|inst19~q\ & ((\inst17|inst3|inst2~0_combout\) # ((\inst|inst41~q\ & \inst19|inst8~q\)))) # (!\inst46|inst1|inst19~q\ & (\inst|inst41~q\ & (\inst19|inst8~q\ & \inst17|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst41~q\,
	datab => \inst46|inst1|inst19~q\,
	datac => \inst19|inst8~q\,
	datad => \inst17|inst3|inst2~0_combout\,
	combout => \inst17|inst4|inst2~0_combout\);

-- Location: LCCOMB_X3_Y23_N20
\inst17|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst57|inst1|inst5~combout\ = \inst46|inst1|inst8~q\ $ (\inst17|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst46|inst1|inst8~q\,
	datad => \inst17|inst4|inst2~0_combout\,
	combout => \inst17|inst57|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N21
\inst47|inst|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst57|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst|inst43~q\);

-- Location: LCCOMB_X3_Y23_N28
\inst25|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst5|inst1|inst5~combout\ = \inst47|inst|inst43~q\ $ (((\inst47|inst1|inst8~q\ & \inst25|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst47|inst|inst43~q\,
	datac => \inst47|inst1|inst8~q\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst5|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N29
\inst49|inst|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst5|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst|inst42~q\);

-- Location: LCCOMB_X1_Y23_N28
\inst25|inst4|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst1|inst5~0_combout\ = \inst47|inst1|inst19~q\ $ (\inst25|inst3|inst2~0_combout\ $ (((\inst|inst42~q\ & \inst19|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst42~q\,
	datab => \inst47|inst1|inst19~q\,
	datac => \inst25|inst3|inst2~0_combout\,
	datad => \inst19|inst8~q\,
	combout => \inst25|inst4|inst1|inst5~0_combout\);

-- Location: FF_X1_Y23_N29
\inst49|inst1|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst4|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst1|inst8~q\);

-- Location: LCCOMB_X2_Y23_N12
\inst32|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst2~0_combout\ = (\inst49|inst1|inst19~q\ & ((\inst32|inst3|inst2~0_combout\) # ((\inst19|inst8~q\ & \inst|inst43~q\)))) # (!\inst49|inst1|inst19~q\ & (\inst19|inst8~q\ & (\inst|inst43~q\ & \inst32|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst1|inst19~q\,
	datab => \inst19|inst8~q\,
	datac => \inst|inst43~q\,
	datad => \inst32|inst3|inst2~0_combout\,
	combout => \inst32|inst4|inst2~0_combout\);

-- Location: LCCOMB_X2_Y23_N16
\inst32|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst34|inst1|inst5~combout\ = \inst49|inst|inst42~q\ $ (((\inst49|inst|inst43~q\ & (\inst49|inst1|inst8~q\ & \inst32|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst|inst43~q\,
	datab => \inst49|inst|inst42~q\,
	datac => \inst49|inst1|inst8~q\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst34|inst1|inst5~combout\);

-- Location: FF_X2_Y23_N17
\inst38|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst34|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst41~q\);

-- Location: LCCOMB_X1_Y23_N22
\inst32|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst1|inst5~combout\ = \inst49|inst1|inst42~q\ $ (\inst32|inst1|inst|inst~combout\ $ (((\inst|inst43~q\ & \inst19|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst1|inst42~q\,
	datab => \inst32|inst1|inst|inst~combout\,
	datac => \inst|inst43~q\,
	datad => \inst19|inst41~q\,
	combout => \inst32|inst2|inst1|inst5~combout\);

-- Location: FF_X1_Y23_N23
\inst39|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst2|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst41~q\);

-- Location: LCCOMB_X1_Y23_N16
\inst32|inst3|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst1|inst5~0_combout\ = \inst49|inst1|inst41~q\ $ (\inst32|inst2|inst2~0_combout\ $ (((\inst19|inst19~q\ & \inst|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst19~q\,
	datab => \inst49|inst1|inst41~q\,
	datac => \inst|inst43~q\,
	datad => \inst32|inst2|inst2~0_combout\,
	combout => \inst32|inst3|inst1|inst5~0_combout\);

-- Location: FF_X1_Y23_N17
\inst39|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst3|inst1|inst5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst19~q\);

-- Location: LCCOMB_X1_Y23_N12
\inst32|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst5~combout\ = \inst49|inst1|inst43~q\ $ (((\inst19|inst42~q\ & \inst|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst19|inst42~q\,
	datac => \inst|inst43~q\,
	datad => \inst49|inst1|inst43~q\,
	combout => \inst32|inst1|inst|inst5~combout\);

-- Location: FF_X1_Y23_N13
\inst39|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst1|inst|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst42~q\);

-- Location: LCCOMB_X5_Y25_N16
\inst42|seven_seg0[6]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~78_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\ $ (!\inst39|inst42~q\)))) # (!\inst38|inst41~q\ & ((\inst39|inst19~q\ & ((!\inst39|inst42~q\))) # (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~78_combout\);

-- Location: LCCOMB_X5_Y25_N12
\inst42|seven_seg0[6]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~68_combout\ = \inst39|inst19~q\ $ (\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~68_combout\);

-- Location: LCCOMB_X4_Y23_N4
\inst9|LPM_MUX_component|auto_generated|result_node[4]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[4]~3_combout\ = (\inst19|inst8~q\ & \inst|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst19|inst8~q\,
	datac => \inst|inst8~q\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[4]~3_combout\);

-- Location: FF_X4_Y23_N5
\inst45|inst1|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst9|LPM_MUX_component|auto_generated|result_node[4]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst45|inst1|inst8~q\);

-- Location: LCCOMB_X4_Y23_N10
\inst15|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst2~0_combout\ = (\inst45|inst1|inst19~q\ & ((\inst15|inst3|inst2~0_combout\) # ((\inst|inst19~q\ & \inst19|inst8~q\)))) # (!\inst45|inst1|inst19~q\ & (\inst|inst19~q\ & (\inst19|inst8~q\ & \inst15|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|inst19~q\,
	datab => \inst45|inst1|inst19~q\,
	datac => \inst19|inst8~q\,
	datad => \inst15|inst3|inst2~0_combout\,
	combout => \inst15|inst4|inst2~0_combout\);

-- Location: LCCOMB_X4_Y23_N22
\inst15|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst5~combout\ = \inst45|inst1|inst8~q\ $ (\inst15|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst45|inst1|inst8~q\,
	datad => \inst15|inst4|inst2~0_combout\,
	combout => \inst15|inst57|inst1|inst5~combout\);

-- Location: FF_X4_Y23_N23
\inst46|inst|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst57|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst|inst43~q\);

-- Location: LCCOMB_X3_Y23_N18
\inst17|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst5|inst1|inst5~combout\ = \inst46|inst|inst43~q\ $ (((\inst46|inst1|inst8~q\ & \inst17|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst46|inst|inst43~q\,
	datac => \inst46|inst1|inst8~q\,
	datad => \inst17|inst4|inst2~0_combout\,
	combout => \inst17|inst5|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N19
\inst47|inst|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst5|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst|inst42~q\);

-- Location: LCCOMB_X3_Y23_N2
\inst25|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst34|inst1|inst5~combout\ = \inst47|inst|inst42~q\ $ (((\inst47|inst|inst43~q\ & (\inst47|inst1|inst8~q\ & \inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst47|inst|inst42~q\,
	datab => \inst47|inst|inst43~q\,
	datac => \inst47|inst1|inst8~q\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst34|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N3
\inst49|inst|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst34|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst|inst41~q\);

-- Location: LCCOMB_X2_Y23_N22
\inst32|inst488|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst488|inst1|inst5~0_combout\ = ((!\inst49|inst1|inst8~q\) # (!\inst49|inst|inst43~q\)) # (!\inst49|inst|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst|inst42~q\,
	datab => \inst49|inst|inst43~q\,
	datad => \inst49|inst1|inst8~q\,
	combout => \inst32|inst488|inst1|inst5~0_combout\);

-- Location: LCCOMB_X2_Y23_N30
\inst32|inst488|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst488|inst1|inst5~combout\ = \inst49|inst|inst41~q\ $ (((!\inst32|inst488|inst1|inst5~0_combout\ & \inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst|inst41~q\,
	datab => \inst32|inst488|inst1|inst5~0_combout\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst488|inst1|inst5~combout\);

-- Location: FF_X2_Y23_N31
\inst38|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst488|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst19~q\);

-- Location: LCCOMB_X2_Y23_N0
\inst32|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst5|inst1|inst5~combout\ = \inst49|inst|inst43~q\ $ (((\inst49|inst1|inst8~q\ & \inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst|inst43~q\,
	datab => \inst49|inst1|inst8~q\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst5|inst1|inst5~combout\);

-- Location: FF_X2_Y23_N1
\inst38|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst5|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst42~q\);

-- Location: LCCOMB_X5_Y27_N16
\inst42|seven_seg1[3]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~39_combout\ = (\inst38|inst19~q\ & (\inst38|inst41~q\ & \inst38|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst19~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[3]~39_combout\);

-- Location: LCCOMB_X5_Y23_N12
\inst42|seven_seg0[6]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~105_combout\ = (!\inst39|inst42~q\ & (\inst39|inst41~q\ & (\inst39|inst19~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~105_combout\);

-- Location: LCCOMB_X1_Y23_N2
\inst33|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\inst|inst43~q\ & \inst19|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|inst43~q\,
	datad => \inst19|inst43~q\,
	combout => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: FF_X1_Y23_N3
\inst39|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst39|inst43~q\);

-- Location: LCCOMB_X3_Y23_N24
\inst25|inst488|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst488|inst1|inst5~0_combout\ = ((!\inst47|inst|inst43~q\) # (!\inst47|inst1|inst8~q\)) # (!\inst47|inst|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst47|inst|inst42~q\,
	datac => \inst47|inst1|inst8~q\,
	datad => \inst47|inst|inst43~q\,
	combout => \inst25|inst488|inst1|inst5~0_combout\);

-- Location: LCCOMB_X4_Y23_N30
\inst15|inst57|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst~combout\ = (\inst45|inst1|inst8~q\ & \inst15|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst45|inst1|inst8~q\,
	datad => \inst15|inst4|inst2~0_combout\,
	combout => \inst15|inst57|inst1|inst~combout\);

-- Location: FF_X4_Y23_N31
\inst46|inst|inst42\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst15|inst57|inst1|inst~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst46|inst|inst42~q\);

-- Location: LCCOMB_X3_Y23_N30
\inst17|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst34|inst1|inst5~combout\ = \inst46|inst|inst42~q\ $ (((\inst46|inst|inst43~q\ & (\inst46|inst1|inst8~q\ & \inst17|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst46|inst|inst42~q\,
	datab => \inst46|inst|inst43~q\,
	datac => \inst46|inst1|inst8~q\,
	datad => \inst17|inst4|inst2~0_combout\,
	combout => \inst17|inst34|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N31
\inst47|inst|inst41\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst17|inst34|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst47|inst|inst41~q\);

-- Location: LCCOMB_X3_Y23_N14
\inst25|inst488|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst488|inst1|inst5~combout\ = \inst47|inst|inst41~q\ $ (((!\inst25|inst488|inst1|inst5~0_combout\ & \inst25|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst25|inst488|inst1|inst5~0_combout\,
	datac => \inst47|inst|inst41~q\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst488|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N15
\inst49|inst|inst19\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst25|inst488|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst49|inst|inst19~q\);

-- Location: LCCOMB_X3_Y23_N8
\inst32|inst667|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~0_combout\ = ((!\inst49|inst|inst41~q\) # (!\inst49|inst1|inst8~q\)) # (!\inst49|inst|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst49|inst|inst43~q\,
	datac => \inst49|inst1|inst8~q\,
	datad => \inst49|inst|inst41~q\,
	combout => \inst32|inst667|inst1|inst5~0_combout\);

-- Location: LCCOMB_X3_Y23_N6
\inst32|inst667|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~combout\ = \inst49|inst|inst19~q\ $ (((\inst49|inst|inst42~q\ & (!\inst32|inst667|inst1|inst5~0_combout\ & \inst32|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100011011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst49|inst|inst42~q\,
	datab => \inst49|inst|inst19~q\,
	datac => \inst32|inst667|inst1|inst5~0_combout\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst667|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N7
\inst38|inst8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst667|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst8~q\);

-- Location: LCCOMB_X3_Y23_N12
\inst32|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst57|inst1|inst5~combout\ = \inst49|inst1|inst8~q\ $ (\inst32|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst49|inst1|inst8~q\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst32|inst57|inst1|inst5~combout\);

-- Location: FF_X3_Y23_N13
\inst38|inst43\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \ALT_INV_CLOCK~input_o\,
	d => \inst32|inst57|inst1|inst5~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst38|inst43~q\);

-- Location: LCCOMB_X8_Y27_N12
\inst42|seven_seg1[3]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~40_combout\ = (\inst38|inst8~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~40_combout\);

-- Location: LCCOMB_X4_Y26_N30
\inst42|seven_seg0[6]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~106_combout\ = (\inst42|seven_seg1[3]~39_combout\ & (\inst42|seven_seg0[6]~105_combout\ & (\inst39|inst43~q\ & \inst42|seven_seg1[3]~40_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~39_combout\,
	datab => \inst42|seven_seg0[6]~105_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg1[3]~40_combout\,
	combout => \inst42|seven_seg0[6]~106_combout\);

-- Location: LCCOMB_X5_Y25_N28
\inst42|seven_seg0[6]~281\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~281_combout\ = (\inst42|seven_seg0[6]~106_combout\) # ((!\inst38|inst41~q\ & (!\inst39|inst19~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[6]~106_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~281_combout\);

-- Location: LCCOMB_X5_Y25_N8
\inst42|seven_seg0[6]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~108_combout\ = (\inst39|inst41~q\ & (((\inst42|seven_seg0[6]~281_combout\)))) # (!\inst39|inst41~q\ & (\inst42|seven_seg0[6]~68_combout\ $ ((!\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110100100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~68_combout\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[6]~281_combout\,
	combout => \inst42|seven_seg0[6]~108_combout\);

-- Location: LCCOMB_X5_Y25_N26
\inst42|seven_seg0[6]~282\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~282_combout\ = (\inst39|inst8~q\ & (((\inst42|seven_seg0[6]~108_combout\ & \inst38|inst19~q\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~78_combout\ & ((!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[6]~78_combout\,
	datac => \inst42|seven_seg0[6]~108_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~282_combout\);

-- Location: LCCOMB_X5_Y28_N12
\inst42|seven_seg0[0]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~97_combout\ = (\inst38|inst42~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[0]~97_combout\);

-- Location: LCCOMB_X5_Y24_N28
\inst42|seven_seg0[6]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~75_combout\ = \inst39|inst42~q\ $ (((\inst39|inst41~q\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~75_combout\);

-- Location: LCCOMB_X4_Y26_N24
\inst42|seven_seg0[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~66_combout\ = \inst38|inst19~q\ $ (\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst19~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[6]~66_combout\);

-- Location: LCCOMB_X5_Y24_N2
\inst42|seven_seg0[6]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~74_combout\ = (\inst39|inst19~q\ & (\inst38|inst41~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (!\inst38|inst41~q\ & (\inst39|inst8~q\ $ (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~74_combout\);

-- Location: LCCOMB_X5_Y24_N10
\inst42|seven_seg0[6]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~76_combout\ = (!\inst42|seven_seg0[6]~66_combout\ & ((\inst42|seven_seg0[6]~75_combout\ & ((\inst42|seven_seg0[6]~74_combout\))) # (!\inst42|seven_seg0[6]~75_combout\ & (\inst42|seven_seg0[6]~68_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~68_combout\,
	datab => \inst42|seven_seg0[6]~75_combout\,
	datac => \inst42|seven_seg0[6]~66_combout\,
	datad => \inst42|seven_seg0[6]~74_combout\,
	combout => \inst42|seven_seg0[6]~76_combout\);

-- Location: LCCOMB_X5_Y26_N28
\inst42|seven_seg0[6]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~71_combout\ = (!\inst39|inst41~q\ & \inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~71_combout\);

-- Location: LCCOMB_X5_Y24_N0
\inst42|seven_seg0[6]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~70_combout\ = (\inst39|inst19~q\ & (\inst38|inst41~q\ & (\inst39|inst8~q\ $ (\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (!\inst38|inst41~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~70_combout\);

-- Location: LCCOMB_X5_Y24_N18
\inst42|seven_seg0[6]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~72_combout\ = (\inst39|inst42~q\ & ((\inst42|seven_seg0[6]~71_combout\ & ((\inst42|seven_seg0[6]~70_combout\))) # (!\inst42|seven_seg0[6]~71_combout\ & (\inst42|seven_seg0[6]~68_combout\)))) # (!\inst39|inst42~q\ & 
-- ((\inst42|seven_seg0[6]~71_combout\ & (\inst42|seven_seg0[6]~68_combout\)) # (!\inst42|seven_seg0[6]~71_combout\ & ((\inst42|seven_seg0[6]~70_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg0[6]~71_combout\,
	datac => \inst42|seven_seg0[6]~68_combout\,
	datad => \inst42|seven_seg0[6]~70_combout\,
	combout => \inst42|seven_seg0[6]~72_combout\);

-- Location: LCCOMB_X5_Y24_N26
\inst42|seven_seg0[6]~273\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~273_combout\ = (\inst42|seven_seg0[6]~76_combout\) # ((!\inst38|inst19~q\ & (\inst38|inst42~q\ & \inst42|seven_seg0[6]~72_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~76_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[6]~72_combout\,
	combout => \inst42|seven_seg0[6]~273_combout\);

-- Location: LCCOMB_X5_Y25_N14
\inst42|seven_seg0[6]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~77_combout\ = (\inst39|inst41~q\ & (\inst39|inst42~q\ $ (\inst39|inst19~q\ $ (\inst38|inst41~q\)))) # (!\inst39|inst41~q\ & ((\inst39|inst42~q\ & (!\inst39|inst19~q\ & !\inst38|inst41~q\)) # (!\inst39|inst42~q\ & (\inst39|inst19~q\ & 
-- \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~77_combout\);

-- Location: LCCOMB_X5_Y25_N2
\inst42|seven_seg0[6]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~79_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[6]~77_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~78_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~77_combout\,
	datad => \inst42|seven_seg0[6]~78_combout\,
	combout => \inst42|seven_seg0[6]~79_combout\);

-- Location: LCCOMB_X5_Y24_N16
\inst42|seven_seg0[6]~274\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~274_combout\ = (\inst42|seven_seg0[6]~273_combout\) # ((!\inst38|inst42~q\ & (\inst38|inst19~q\ & \inst42|seven_seg0[6]~79_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~273_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~79_combout\,
	combout => \inst42|seven_seg0[6]~274_combout\);

-- Location: LCCOMB_X5_Y24_N12
\inst42|seven_seg0[6]~299\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~299_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\) # ((!\inst38|inst41~q\) # (!\inst39|inst19~q\)))) # (!\inst39|inst8~q\ & (((\inst39|inst19~q\ & \inst38|inst41~q\)) # (!\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101110111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~299_combout\);

-- Location: LCCOMB_X5_Y24_N6
\inst42|seven_seg0[6]~300\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~300_combout\ = (\inst42|seven_seg0[6]~299_combout\ & (\inst39|inst42~q\ $ (\inst38|inst41~q\ $ (!\inst39|inst19~q\)))) # (!\inst42|seven_seg0[6]~299_combout\ & (\inst39|inst42~q\ & (!\inst38|inst41~q\ & !\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg0[6]~299_combout\,
	combout => \inst42|seven_seg0[6]~300_combout\);

-- Location: LCCOMB_X8_Y24_N14
\inst42|seven_seg0[6]~293\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~293_combout\ = \inst39|inst19~q\ $ (((\inst39|inst8~q\ & (!\inst39|inst41~q\ & !\inst38|inst41~q\)) # (!\inst39|inst8~q\ & (\inst39|inst41~q\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101010100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~293_combout\);

-- Location: LCCOMB_X8_Y24_N28
\inst42|seven_seg0[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~67_combout\ = \inst39|inst41~q\ $ (\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst41~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[6]~67_combout\);

-- Location: LCCOMB_X8_Y24_N10
\inst42|seven_seg0[6]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~69_combout\ = (\inst39|inst42~q\ & (!\inst42|seven_seg0[6]~293_combout\ & ((\inst42|seven_seg0[6]~67_combout\) # (!\inst38|inst41~q\)))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[6]~293_combout\ & ((\inst38|inst41~q\) # 
-- (\inst42|seven_seg0[6]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg0[6]~293_combout\,
	datad => \inst42|seven_seg0[6]~67_combout\,
	combout => \inst42|seven_seg0[6]~69_combout\);

-- Location: LCCOMB_X5_Y24_N14
\inst42|seven_seg0[6]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~103_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg0[6]~300_combout\)) # (!\inst38|inst19~q\ & ((\inst42|seven_seg0[6]~69_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~300_combout\,
	datab => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~69_combout\,
	combout => \inst42|seven_seg0[6]~103_combout\);

-- Location: LCCOMB_X8_Y25_N22
\inst42|seven_seg0[6]~280\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~280_combout\ = (\inst38|inst43~q\ & (!\inst38|inst42~q\ & ((\inst42|seven_seg0[6]~103_combout\)))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~274_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[6]~274_combout\,
	datad => \inst42|seven_seg0[6]~103_combout\,
	combout => \inst42|seven_seg0[6]~280_combout\);

-- Location: LCCOMB_X5_Y25_N24
\inst42|seven_seg0[6]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~104_combout\ = (\inst42|seven_seg0[6]~77_combout\ & (\inst39|inst8~q\ $ (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[6]~77_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~104_combout\);

-- Location: LCCOMB_X8_Y25_N12
\inst42|seven_seg0[6]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~109_combout\ = (\inst42|seven_seg0[6]~280_combout\) # ((\inst42|seven_seg0[0]~97_combout\ & ((\inst42|seven_seg0[6]~282_combout\) # (\inst42|seven_seg0[6]~104_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~282_combout\,
	datab => \inst42|seven_seg0[0]~97_combout\,
	datac => \inst42|seven_seg0[6]~280_combout\,
	datad => \inst42|seven_seg0[6]~104_combout\,
	combout => \inst42|seven_seg0[6]~109_combout\);

-- Location: LCCOMB_X5_Y24_N20
\inst42|seven_seg0[6]~272\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~272_combout\ = (\inst38|inst19~q\ & (((!\inst38|inst42~q\ & \inst42|seven_seg0[6]~72_combout\)))) # (!\inst38|inst19~q\ & (\inst42|seven_seg0[6]~69_combout\ & (\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~69_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[6]~72_combout\,
	combout => \inst42|seven_seg0[6]~272_combout\);

-- Location: LCCOMB_X5_Y24_N24
\inst42|seven_seg0[6]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~73_combout\ = (!\inst38|inst43~q\ & ((\inst42|seven_seg0[6]~272_combout\) # ((\inst42|seven_seg0[6]~300_combout\ & !\inst42|seven_seg0[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~300_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[6]~66_combout\,
	datad => \inst42|seven_seg0[6]~272_combout\,
	combout => \inst42|seven_seg0[6]~73_combout\);

-- Location: LCCOMB_X8_Y25_N24
\inst42|seven_seg0[6]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~80_combout\ = (\inst42|seven_seg0[6]~73_combout\) # ((\inst38|inst43~q\ & \inst42|seven_seg0[6]~274_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[6]~274_combout\,
	datad => \inst42|seven_seg0[6]~73_combout\,
	combout => \inst42|seven_seg0[6]~80_combout\);

-- Location: LCCOMB_X5_Y24_N4
\inst42|seven_seg0[6]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~88_combout\ = (\inst39|inst42~q\ & (!\inst38|inst41~q\ & (!\inst39|inst19~q\ & \inst42|seven_seg0[6]~71_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg0[6]~71_combout\,
	combout => \inst42|seven_seg0[6]~88_combout\);

-- Location: LCCOMB_X5_Y24_N22
\inst42|seven_seg0[6]~275\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~275_combout\ = (\inst42|seven_seg0[6]~68_combout\ & ((\inst39|inst8~q\ & (!\inst39|inst41~q\ & !\inst39|inst42~q\)) # (!\inst39|inst8~q\ & (\inst39|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst42|seven_seg0[6]~68_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~275_combout\);

-- Location: LCCOMB_X5_Y24_N30
\inst42|seven_seg0[6]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~92_combout\ = (\inst39|inst8~q\ & (!\inst39|inst41~q\ & (\inst39|inst42~q\ & \inst38|inst41~q\))) # (!\inst39|inst8~q\ & (\inst39|inst41~q\ & (!\inst39|inst42~q\ & !\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~92_combout\);

-- Location: LCCOMB_X5_Y24_N8
\inst42|seven_seg0[6]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~93_combout\ = (\inst42|seven_seg0[6]~275_combout\) # ((\inst42|seven_seg0[6]~92_combout\ & (\inst39|inst19~q\ $ (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~275_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[6]~92_combout\,
	combout => \inst42|seven_seg0[6]~93_combout\);

-- Location: LCCOMB_X9_Y24_N22
\inst42|seven_seg0[6]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~82_combout\ = (\inst38|inst41~q\ & (!\inst39|inst42~q\ & (!\inst39|inst41~q\ & \inst39|inst19~q\))) # (!\inst38|inst41~q\ & (\inst39|inst42~q\ & (\inst39|inst41~q\ & !\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~82_combout\);

-- Location: LCCOMB_X9_Y24_N24
\inst42|seven_seg0[6]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~90_combout\ = (\inst38|inst41~q\ & (\inst39|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\)))) # (!\inst38|inst41~q\ & ((\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ 
-- & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~90_combout\);

-- Location: LCCOMB_X9_Y24_N14
\inst42|seven_seg0[6]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~91_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[6]~82_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~90_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~82_combout\,
	datad => \inst42|seven_seg0[6]~90_combout\,
	combout => \inst42|seven_seg0[6]~91_combout\);

-- Location: LCCOMB_X8_Y24_N20
\inst42|seven_seg0[6]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~94_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg0[6]~93_combout\ & ((!\inst38|inst19~q\)))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg0[6]~91_combout\ & \inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~93_combout\,
	datab => \inst42|seven_seg0[6]~91_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~94_combout\);

-- Location: LCCOMB_X9_Y24_N6
\inst42|seven_seg0[6]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~89_combout\ = (!\inst42|seven_seg0[6]~67_combout\ & ((\inst38|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\))) # (!\inst38|inst41~q\ & (!\inst39|inst42~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg0[6]~67_combout\,
	combout => \inst42|seven_seg0[6]~89_combout\);

-- Location: LCCOMB_X8_Y24_N30
\inst42|seven_seg0[6]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~95_combout\ = (\inst42|seven_seg0[6]~94_combout\) # ((!\inst42|seven_seg0[6]~66_combout\ & ((\inst42|seven_seg0[6]~88_combout\) # (\inst42|seven_seg0[6]~89_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~88_combout\,
	datab => \inst42|seven_seg0[6]~94_combout\,
	datac => \inst42|seven_seg0[6]~66_combout\,
	datad => \inst42|seven_seg0[6]~89_combout\,
	combout => \inst42|seven_seg0[6]~95_combout\);

-- Location: LCCOMB_X9_Y24_N16
\inst42|seven_seg0[6]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~81_combout\ = (\inst38|inst41~q\ & ((\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & \inst39|inst19~q\)))) # (!\inst38|inst41~q\ & (!\inst39|inst41~q\ & (\inst39|inst42~q\ $ 
-- (!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~81_combout\);

-- Location: LCCOMB_X9_Y24_N18
\inst42|seven_seg0[6]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~98_combout\ = (\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~81_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~82_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~82_combout\,
	datad => \inst42|seven_seg0[6]~81_combout\,
	combout => \inst42|seven_seg0[6]~98_combout\);

-- Location: LCCOMB_X9_Y24_N28
\inst42|seven_seg0[6]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~99_combout\ = (\inst42|seven_seg0[6]~98_combout\) # ((\inst42|seven_seg0[6]~93_combout\ & !\inst38|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg0[6]~93_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[6]~98_combout\,
	combout => \inst42|seven_seg0[6]~99_combout\);

-- Location: LCCOMB_X8_Y24_N8
\inst42|seven_seg0[6]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~100_combout\ = (\inst38|inst43~q\ & (\inst42|seven_seg0[6]~95_combout\)) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~66_combout\ & \inst42|seven_seg0[6]~99_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~95_combout\,
	datab => \inst42|seven_seg0[6]~66_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~99_combout\,
	combout => \inst42|seven_seg0[6]~100_combout\);

-- Location: LCCOMB_X8_Y24_N22
\inst42|seven_seg0[6]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~85_combout\ = (\inst39|inst19~q\ & (\inst39|inst42~q\ & !\inst38|inst41~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~85_combout\);

-- Location: LCCOMB_X8_Y24_N4
\inst42|seven_seg0[6]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~84_combout\ = (\inst39|inst19~q\ & (!\inst39|inst8~q\ & (\inst39|inst41~q\ & \inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~84_combout\);

-- Location: LCCOMB_X8_Y24_N24
\inst42|seven_seg0[6]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~86_combout\ = (\inst42|seven_seg0[6]~85_combout\ & (((\inst42|seven_seg0[6]~84_combout\ & !\inst39|inst42~q\)) # (!\inst42|seven_seg0[6]~67_combout\))) # (!\inst42|seven_seg0[6]~85_combout\ & (((\inst42|seven_seg0[6]~84_combout\ & 
-- !\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~85_combout\,
	datab => \inst42|seven_seg0[6]~67_combout\,
	datac => \inst42|seven_seg0[6]~84_combout\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~86_combout\);

-- Location: LCCOMB_X8_Y24_N6
\inst42|seven_seg0[6]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~101_combout\ = (\inst42|seven_seg0[6]~100_combout\) # ((!\inst42|seven_seg0[6]~66_combout\ & (!\inst38|inst43~q\ & \inst42|seven_seg0[6]~86_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~66_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[6]~100_combout\,
	datad => \inst42|seven_seg0[6]~86_combout\,
	combout => \inst42|seven_seg0[6]~101_combout\);

-- Location: LCCOMB_X9_Y24_N10
\inst42|seven_seg0[3]~277\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~277_combout\ = (\inst39|inst8~q\ & (((\inst38|inst19~q\ & \inst42|seven_seg0[6]~81_combout\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~90_combout\ & (!\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[6]~90_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~81_combout\,
	combout => \inst42|seven_seg0[3]~277_combout\);

-- Location: LCCOMB_X9_Y24_N8
\inst42|seven_seg0[6]~278\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~278_combout\ = (\inst42|seven_seg0[3]~277_combout\) # ((\inst42|seven_seg0[6]~82_combout\ & (\inst39|inst8~q\ $ (\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[3]~277_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~82_combout\,
	combout => \inst42|seven_seg0[6]~278_combout\);

-- Location: LCCOMB_X9_Y24_N0
\inst42|seven_seg0[6]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~83_combout\ = (!\inst38|inst19~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~81_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~82_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~82_combout\,
	datab => \inst42|seven_seg0[6]~81_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~83_combout\);

-- Location: LCCOMB_X8_Y24_N18
\inst42|seven_seg0[6]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~87_combout\ = (\inst42|seven_seg0[6]~83_combout\) # ((\inst42|seven_seg0[6]~86_combout\ & \inst38|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg0[6]~86_combout\,
	datac => \inst42|seven_seg0[6]~83_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~87_combout\);

-- Location: LCCOMB_X8_Y25_N18
\inst42|seven_seg0[6]~276\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~276_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg0[6]~87_combout\ & !\inst38|inst42~q\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[6]~95_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~95_combout\,
	datab => \inst42|seven_seg0[6]~87_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[6]~276_combout\);

-- Location: LCCOMB_X8_Y25_N16
\inst42|seven_seg0[6]~279\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~279_combout\ = (\inst42|seven_seg0[6]~276_combout\) # ((\inst42|seven_seg0[6]~278_combout\ & (\inst38|inst42~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~278_combout\,
	datab => \inst42|seven_seg0[6]~276_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[6]~279_combout\);

-- Location: LCCOMB_X8_Y25_N6
\inst42|seven_seg0[6]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~102_combout\ = (\inst38|inst8~q\ & (((\inst39|inst43~q\) # (\inst42|seven_seg0[6]~279_combout\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[6]~101_combout\ & (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~101_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[6]~279_combout\,
	combout => \inst42|seven_seg0[6]~102_combout\);

-- Location: LCCOMB_X8_Y25_N2
\inst42|seven_seg0[6]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~110_combout\ = (\inst39|inst43~q\ & ((\inst42|seven_seg0[6]~102_combout\ & (\inst42|seven_seg0[6]~109_combout\)) # (!\inst42|seven_seg0[6]~102_combout\ & ((\inst42|seven_seg0[6]~80_combout\))))) # (!\inst39|inst43~q\ & 
-- (((\inst42|seven_seg0[6]~102_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~109_combout\,
	datab => \inst42|seven_seg0[6]~80_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[6]~102_combout\,
	combout => \inst42|seven_seg0[6]~110_combout\);

-- Location: LCCOMB_X4_Y25_N18
\inst42|seven_seg0[5]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~112_combout\ = (\inst39|inst19~q\ & ((\inst38|inst8~q\ & (\inst38|inst41~q\ $ (\inst39|inst41~q\))) # (!\inst38|inst8~q\ & (!\inst38|inst41~q\ & !\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\ & (\inst38|inst41~q\ & 
-- \inst39|inst41~q\)) # (!\inst38|inst8~q\ & (\inst38|inst41~q\ $ (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~112_combout\);

-- Location: LCCOMB_X4_Y25_N20
\inst42|seven_seg0[5]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~118_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\ & \inst38|inst41~q\)) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (\inst39|inst41~q\ $ (((\inst38|inst41~q\) # (!\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010011111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~118_combout\);

-- Location: LCCOMB_X4_Y25_N30
\inst42|seven_seg0[5]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~119_combout\ = (\inst42|seven_seg0[5]~118_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[5]~112_combout\ $ (!\inst39|inst42~q\)))) # (!\inst42|seven_seg0[5]~118_combout\ & ((\inst38|inst43~q\ & ((!\inst39|inst42~q\) # 
-- (!\inst42|seven_seg0[5]~112_combout\))) # (!\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~112_combout\) # (\inst39|inst42~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~112_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~118_combout\,
	combout => \inst42|seven_seg0[5]~119_combout\);

-- Location: LCCOMB_X4_Y25_N4
\inst42|seven_seg0[5]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~111_combout\ = (\inst39|inst19~q\ & (\inst39|inst41~q\ $ (((!\inst38|inst8~q\ & \inst38|inst41~q\))))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst38|inst8~q\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~111_combout\);

-- Location: LCCOMB_X4_Y25_N28
\inst42|seven_seg0[5]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~113_combout\ = (\inst42|seven_seg0[5]~111_combout\ & (\inst39|inst42~q\ $ (\inst42|seven_seg0[5]~112_combout\ $ (!\inst38|inst43~q\)))) # (!\inst42|seven_seg0[5]~111_combout\ & ((\inst39|inst42~q\ & 
-- ((\inst42|seven_seg0[5]~112_combout\) # (!\inst38|inst43~q\))) # (!\inst39|inst42~q\ & ((\inst38|inst43~q\) # (!\inst42|seven_seg0[5]~112_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100111011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg0[5]~112_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~113_combout\);

-- Location: LCCOMB_X4_Y25_N16
\inst42|seven_seg0[5]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~115_combout\ = (\inst39|inst19~q\ & ((\inst38|inst8~q\ & (!\inst38|inst41~q\ & \inst39|inst41~q\)) # (!\inst38|inst8~q\ & (\inst38|inst41~q\ & !\inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (\inst39|inst41~q\ & ((\inst38|inst8~q\) # 
-- (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~115_combout\);

-- Location: LCCOMB_X4_Y25_N10
\inst42|seven_seg0[5]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~114_combout\ = (\inst39|inst41~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\) # (!\inst38|inst41~q\))) # (!\inst39|inst19~q\ & ((\inst38|inst41~q\) # (!\inst38|inst8~q\))))) # (!\inst39|inst41~q\ & (\inst39|inst19~q\ $ 
-- (\inst38|inst8~q\ $ (\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~114_combout\);

-- Location: LCCOMB_X4_Y25_N26
\inst42|seven_seg0[5]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~116_combout\ = (\inst42|seven_seg0[5]~115_combout\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~114_combout\) # (!\inst39|inst42~q\))) # (!\inst38|inst43~q\ & ((\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~114_combout\))))) # 
-- (!\inst42|seven_seg0[5]~115_combout\ & (\inst38|inst43~q\ $ (\inst39|inst42~q\ $ (\inst42|seven_seg0[5]~114_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100101011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~115_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~114_combout\,
	combout => \inst42|seven_seg0[5]~116_combout\);

-- Location: LCCOMB_X4_Y24_N16
\inst42|seven_seg0[5]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~117_combout\ = (\inst38|inst42~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[5]~113_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~116_combout\))))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[5]~113_combout\ & 
-- ((!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~113_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[5]~116_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[5]~117_combout\);

-- Location: LCCOMB_X4_Y24_N22
\inst42|seven_seg0[5]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~120_combout\ = (\inst42|seven_seg0[5]~117_combout\) # ((\inst42|seven_seg0[5]~119_combout\ & (!\inst38|inst42~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~119_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[5]~117_combout\,
	combout => \inst42|seven_seg0[5]~120_combout\);

-- Location: LCCOMB_X3_Y24_N12
\inst42|seven_seg0[5]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~145_combout\ = \inst38|inst43~q\ $ (\inst39|inst42~q\ $ (\inst39|inst19~q\ $ (\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~145_combout\);

-- Location: LCCOMB_X3_Y24_N16
\inst42|seven_seg0[5]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~147_combout\ = (\inst38|inst43~q\ & ((\inst39|inst42~q\ & (\inst39|inst19~q\ & !\inst38|inst8~q\)) # (!\inst39|inst42~q\ & (\inst39|inst19~q\ $ (!\inst38|inst8~q\))))) # (!\inst38|inst43~q\ & (\inst39|inst42~q\ $ (\inst39|inst19~q\ $ 
-- (\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~147_combout\);

-- Location: LCCOMB_X3_Y24_N22
\inst42|seven_seg0[5]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~146_combout\ = (\inst38|inst43~q\ & ((\inst39|inst42~q\ & (\inst39|inst19~q\ $ (!\inst38|inst8~q\))) # (!\inst39|inst42~q\ & (!\inst39|inst19~q\ & \inst38|inst8~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst42~q\ & (!\inst39|inst19~q\ 
-- & \inst38|inst8~q\)) # (!\inst39|inst42~q\ & (\inst39|inst19~q\ & !\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~146_combout\);

-- Location: LCCOMB_X3_Y24_N14
\inst42|seven_seg0[5]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~148_combout\ = (\inst38|inst41~q\ & (((\inst39|inst41~q\)))) # (!\inst38|inst41~q\ & ((\inst39|inst41~q\ & ((!\inst42|seven_seg0[5]~146_combout\))) # (!\inst39|inst41~q\ & (!\inst42|seven_seg0[5]~147_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[5]~147_combout\,
	datac => \inst42|seven_seg0[5]~146_combout\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~148_combout\);

-- Location: LCCOMB_X6_Y27_N30
\inst42|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~3_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ & \inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|Equal0~3_combout\);

-- Location: LCCOMB_X7_Y27_N8
\inst42|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~2_combout\ = (\inst42|seven_seg1[3]~39_combout\ & (\inst38|inst8~q\ & \inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~39_combout\,
	datab => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|Equal0~2_combout\);

-- Location: LCCOMB_X6_Y26_N8
\inst42|seven_seg0[5]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~129_combout\ = (!\inst39|inst43~q\ & (\inst42|Equal0~3_combout\ & \inst42|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst43~q\,
	datac => \inst42|Equal0~3_combout\,
	datad => \inst42|Equal0~2_combout\,
	combout => \inst42|seven_seg0[5]~129_combout\);

-- Location: LCCOMB_X3_Y24_N0
\inst42|seven_seg0[5]~297\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~297_combout\ = (\inst38|inst43~q\ & ((\inst38|inst8~q\ & (\inst39|inst19~q\ & !\inst42|seven_seg0[5]~129_combout\)) # (!\inst38|inst8~q\ & (!\inst39|inst19~q\)))) # (!\inst38|inst43~q\ & (\inst38|inst8~q\ $ ((\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg0[5]~129_combout\,
	combout => \inst42|seven_seg0[5]~297_combout\);

-- Location: LCCOMB_X3_Y24_N6
\inst42|seven_seg0[5]~298\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~298_combout\ = (\inst42|seven_seg0[5]~297_combout\ & ((\inst38|inst43~q\) # ((!\inst39|inst19~q\) # (!\inst39|inst42~q\)))) # (!\inst42|seven_seg0[5]~297_combout\ & (\inst39|inst42~q\ $ (((\inst38|inst43~q\ & !\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg0[5]~297_combout\,
	combout => \inst42|seven_seg0[5]~298_combout\);

-- Location: LCCOMB_X3_Y24_N28
\inst42|seven_seg0[5]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~149_combout\ = (\inst42|seven_seg0[5]~148_combout\ & (((\inst42|seven_seg0[5]~298_combout\) # (!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[5]~148_combout\ & (\inst42|seven_seg0[5]~145_combout\ & (\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~145_combout\,
	datab => \inst42|seven_seg0[5]~148_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[5]~298_combout\,
	combout => \inst42|seven_seg0[5]~149_combout\);

-- Location: LCCOMB_X4_Y24_N6
\inst42|seven_seg0[5]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~142_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ $ (\inst38|inst41~q\ $ (!\inst38|inst8~q\)))) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\) # (!\inst38|inst41~q\))) # (!\inst39|inst19~q\ & 
-- ((\inst38|inst41~q\) # (!\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101010010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~142_combout\);

-- Location: LCCOMB_X4_Y24_N8
\inst42|seven_seg0[5]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~123_combout\ = (\inst39|inst19~q\ & (!\inst39|inst41~q\ & ((\inst38|inst41~q\) # (!\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst38|inst41~q\ & \inst38|inst8~q\)) # (!\inst39|inst41~q\ & (\inst38|inst41~q\ 
-- & !\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~123_combout\);

-- Location: LCCOMB_X4_Y24_N20
\inst42|seven_seg0[5]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~143_combout\ = (\inst42|seven_seg0[5]~123_combout\ & ((\inst42|seven_seg0[5]~142_combout\ & ((!\inst38|inst43~q\) # (!\inst39|inst42~q\))) # (!\inst42|seven_seg0[5]~142_combout\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\))))) # 
-- (!\inst42|seven_seg0[5]~123_combout\ & (\inst42|seven_seg0[5]~142_combout\ $ (\inst39|inst42~q\ $ (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110111011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~142_combout\,
	datab => \inst42|seven_seg0[5]~123_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~143_combout\);

-- Location: LCCOMB_X4_Y24_N30
\inst42|seven_seg0[5]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~144_combout\ = (\inst38|inst42~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~143_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[5]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~119_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[5]~143_combout\,
	combout => \inst42|seven_seg0[5]~144_combout\);

-- Location: LCCOMB_X4_Y24_N24
\inst42|seven_seg0[5]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~150_combout\ = (\inst42|seven_seg0[5]~144_combout\ & (((\inst42|seven_seg0[5]~149_combout\) # (!\inst38|inst42~q\)))) # (!\inst42|seven_seg0[5]~144_combout\ & (\inst42|seven_seg0[5]~113_combout\ & ((\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~113_combout\,
	datab => \inst42|seven_seg0[5]~149_combout\,
	datac => \inst42|seven_seg0[5]~144_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[5]~150_combout\);

-- Location: LCCOMB_X3_Y24_N18
\inst42|seven_seg0[5]~283\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~283_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & ((\inst39|inst42~q\) # (!\inst42|seven_seg0[5]~129_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~129_combout\,
	combout => \inst42|seven_seg0[5]~283_combout\);

-- Location: LCCOMB_X3_Y24_N8
\inst42|seven_seg0[5]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~131_combout\ = (\inst38|inst8~q\ & (((!\inst38|inst41~q\ & \inst39|inst42~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (((\inst39|inst42~q\ & !\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[5]~131_combout\);

-- Location: LCCOMB_X7_Y23_N30
\inst42|seven_seg0[5]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~130_combout\ = (\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst38|inst8~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~130_combout\);

-- Location: LCCOMB_X3_Y24_N4
\inst42|seven_seg0[5]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~133_combout\ = (\inst42|seven_seg0[5]~131_combout\ & (\inst38|inst43~q\ & ((\inst42|seven_seg0[5]~130_combout\)))) # (!\inst42|seven_seg0[5]~131_combout\ & (\inst38|inst41~q\ & ((\inst38|inst43~q\) # 
-- (\inst42|seven_seg0[5]~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~131_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[5]~130_combout\,
	combout => \inst42|seven_seg0[5]~133_combout\);

-- Location: LCCOMB_X3_Y24_N10
\inst42|seven_seg0[5]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~132_combout\ = (\inst42|seven_seg0[5]~131_combout\ & (\inst38|inst43~q\ $ (\inst38|inst41~q\ $ (!\inst42|seven_seg0[5]~130_combout\)))) # (!\inst42|seven_seg0[5]~131_combout\ & ((\inst38|inst43~q\ & (!\inst38|inst41~q\ & 
-- !\inst42|seven_seg0[5]~130_combout\)) # (!\inst38|inst43~q\ & (\inst38|inst41~q\ & \inst42|seven_seg0[5]~130_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~131_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[5]~130_combout\,
	combout => \inst42|seven_seg0[5]~132_combout\);

-- Location: LCCOMB_X3_Y24_N2
\inst42|seven_seg0[5]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~134_combout\ = (\inst42|seven_seg0[5]~132_combout\ & (\inst39|inst41~q\ $ (((\inst42|seven_seg0[5]~133_combout\))))) # (!\inst42|seven_seg0[5]~132_combout\ & (\inst39|inst41~q\ & (\inst42|seven_seg0[5]~283_combout\ & 
-- \inst42|seven_seg0[5]~133_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[5]~283_combout\,
	datac => \inst42|seven_seg0[5]~133_combout\,
	datad => \inst42|seven_seg0[5]~132_combout\,
	combout => \inst42|seven_seg0[5]~134_combout\);

-- Location: LCCOMB_X4_Y24_N18
\inst42|seven_seg0[5]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~124_combout\ = (\inst39|inst42~q\ & (\inst42|seven_seg0[5]~123_combout\ & (\inst38|inst43~q\ $ (!\inst42|seven_seg0[5]~111_combout\)))) # (!\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~111_combout\ & (\inst38|inst43~q\ $ 
-- (!\inst42|seven_seg0[5]~123_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[5]~123_combout\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~124_combout\);

-- Location: LCCOMB_X6_Y27_N0
\inst42|seven_seg0[5]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~125_combout\ = (\inst39|inst19~q\ & (((\inst38|inst41~q\ & !\inst38|inst8~q\)) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (\inst39|inst41~q\ $ (((\inst38|inst41~q\) # (!\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111010001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~125_combout\);

-- Location: LCCOMB_X4_Y24_N28
\inst42|seven_seg0[5]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~126_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst38|inst41~q\ & \inst38|inst8~q\)) # (!\inst39|inst41~q\ & (\inst38|inst41~q\ & !\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst39|inst41~q\ & ((\inst38|inst8~q\) # 
-- (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~126_combout\);

-- Location: LCCOMB_X4_Y24_N2
\inst42|seven_seg0[5]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~127_combout\ = (\inst38|inst43~q\ & (!\inst42|seven_seg0[5]~125_combout\ & (\inst39|inst42~q\ $ (\inst42|seven_seg0[5]~126_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[5]~126_combout\ & (\inst42|seven_seg0[5]~125_combout\ 
-- $ (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~125_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[5]~126_combout\,
	combout => \inst42|seven_seg0[5]~127_combout\);

-- Location: LCCOMB_X4_Y24_N4
\inst42|seven_seg0[5]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~128_combout\ = (\inst38|inst42~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[5]~124_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~127_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[5]~124_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[5]~127_combout\,
	combout => \inst42|seven_seg0[5]~128_combout\);

-- Location: LCCOMB_X4_Y25_N0
\inst42|seven_seg0[5]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~121_combout\ = \inst39|inst41~q\ $ (((\inst39|inst19~q\ & ((\inst38|inst41~q\) # (!\inst38|inst8~q\))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110110110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[5]~121_combout\);

-- Location: LCCOMB_X4_Y25_N14
\inst42|seven_seg0[5]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~122_combout\ = (\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~111_combout\ & (\inst42|seven_seg0[5]~121_combout\ $ (!\inst38|inst43~q\)))) # (!\inst39|inst42~q\ & (!\inst42|seven_seg0[5]~121_combout\ & (\inst38|inst43~q\ $ 
-- (\inst42|seven_seg0[5]~111_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg0[5]~121_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[5]~111_combout\,
	combout => \inst42|seven_seg0[5]~122_combout\);

-- Location: LCCOMB_X4_Y24_N10
\inst42|seven_seg0[5]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~135_combout\ = (\inst42|seven_seg0[5]~128_combout\ & ((\inst42|seven_seg0[5]~134_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg0[5]~128_combout\ & (((\inst42|seven_seg0[5]~122_combout\ & \inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~134_combout\,
	datab => \inst42|seven_seg0[5]~128_combout\,
	datac => \inst42|seven_seg0[5]~122_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg0[5]~135_combout\);

-- Location: LCCOMB_X3_Y24_N24
\inst42|seven_seg0[5]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~138_combout\ = (\inst39|inst42~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\))) # (!\inst39|inst19~q\ & ((!\inst38|inst8~q\) # (!\inst38|inst43~q\))))) # (!\inst39|inst42~q\ & ((\inst39|inst19~q\ & (!\inst38|inst43~q\ & 
-- !\inst38|inst8~q\)) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100011100011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~138_combout\);

-- Location: LCCOMB_X3_Y24_N20
\inst42|seven_seg0[5]~284\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~284_combout\ = (\inst38|inst41~q\ & (!\inst42|seven_seg0[5]~138_combout\ & (!\inst39|inst41~q\ & !\inst38|inst43~q\))) # (!\inst38|inst41~q\ & (\inst42|seven_seg0[5]~138_combout\ & (\inst39|inst41~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[5]~138_combout\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~284_combout\);

-- Location: LCCOMB_X3_Y24_N30
\inst42|seven_seg0[5]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~139_combout\ = (\inst39|inst41~q\ & (\inst39|inst42~q\ & (!\inst39|inst19~q\ & \inst38|inst8~q\))) # (!\inst39|inst41~q\ & (!\inst39|inst42~q\ & (\inst39|inst19~q\ & !\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg0[5]~139_combout\);

-- Location: LCCOMB_X3_Y24_N26
\inst42|seven_seg0[5]~285\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~285_combout\ = (\inst42|seven_seg0[5]~284_combout\) # ((\inst42|seven_seg0[5]~139_combout\ & (\inst38|inst43~q\ $ (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[5]~284_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[5]~139_combout\,
	combout => \inst42|seven_seg0[5]~285_combout\);

-- Location: LCCOMB_X4_Y24_N0
\inst42|seven_seg0[5]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~136_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg0[5]~122_combout\ & (\inst39|inst8~q\))) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~127_combout\))) # (!\inst39|inst8~q\ & 
-- (\inst42|seven_seg0[5]~122_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001010000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~122_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[5]~127_combout\,
	combout => \inst42|seven_seg0[5]~136_combout\);

-- Location: LCCOMB_X4_Y24_N26
\inst42|seven_seg0[5]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~140_combout\ = (\inst42|seven_seg0[5]~136_combout\) # ((\inst42|seven_seg0[5]~285_combout\ & (\inst38|inst42~q\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~285_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[5]~136_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[5]~140_combout\);

-- Location: LCCOMB_X4_Y24_N12
\inst42|seven_seg0[5]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~141_combout\ = (\inst39|inst43~q\ & (((\inst38|inst19~q\)))) # (!\inst39|inst43~q\ & ((\inst38|inst19~q\ & (\inst42|seven_seg0[5]~135_combout\)) # (!\inst38|inst19~q\ & ((\inst42|seven_seg0[5]~140_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~135_combout\,
	datab => \inst39|inst43~q\,
	datac => \inst42|seven_seg0[5]~140_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[5]~141_combout\);

-- Location: LCCOMB_X4_Y24_N14
\inst42|seven_seg0[5]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~151_combout\ = (\inst39|inst43~q\ & ((\inst42|seven_seg0[5]~141_combout\ & ((\inst42|seven_seg0[5]~150_combout\))) # (!\inst42|seven_seg0[5]~141_combout\ & (\inst42|seven_seg0[5]~120_combout\)))) # (!\inst39|inst43~q\ & 
-- (((\inst42|seven_seg0[5]~141_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~120_combout\,
	datab => \inst42|seven_seg0[5]~150_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[5]~141_combout\,
	combout => \inst42|seven_seg0[5]~151_combout\);

-- Location: LCCOMB_X5_Y27_N22
\inst42|seven_seg1[5]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~41_combout\ = (\inst39|inst42~q\ & (\inst39|inst19~q\ & \inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[5]~41_combout\);

-- Location: LCCOMB_X4_Y26_N6
\inst42|seven_seg0[4]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~175_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg1[3]~40_combout\ & (\inst42|seven_seg1[3]~39_combout\ & \inst42|seven_seg1[5]~41_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[3]~40_combout\,
	datac => \inst42|seven_seg1[3]~39_combout\,
	datad => \inst42|seven_seg1[5]~41_combout\,
	combout => \inst42|seven_seg0[4]~175_combout\);

-- Location: LCCOMB_X6_Y26_N2
\inst42|Equal0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~4_combout\ = (\inst39|inst42~q\ & (!\inst39|inst43~q\ & (\inst42|Equal0~3_combout\ & \inst42|Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst43~q\,
	datac => \inst42|Equal0~3_combout\,
	datad => \inst42|Equal0~2_combout\,
	combout => \inst42|Equal0~4_combout\);

-- Location: LCCOMB_X3_Y26_N10
\inst42|seven_seg0[4]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~154_combout\ = (\inst39|inst41~q\) # (\inst39|inst19~q\ $ (\inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111011011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[4]~154_combout\);

-- Location: LCCOMB_X3_Y26_N0
\inst42|seven_seg0[4]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~153_combout\ = ((\inst39|inst41~q\) # (\inst39|inst42~q\)) # (!\inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[4]~153_combout\);

-- Location: LCCOMB_X3_Y26_N8
\inst42|seven_seg0[4]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~155_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\) # (!\inst42|seven_seg0[4]~153_combout\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[4]~154_combout\ & ((!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~154_combout\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[4]~155_combout\);

-- Location: LCCOMB_X3_Y26_N14
\inst42|seven_seg0[4]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~156_combout\ = (\inst39|inst19~q\ & (\inst39|inst41~q\ & !\inst39|inst42~q\)) # (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & \inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[4]~156_combout\);

-- Location: LCCOMB_X6_Y25_N0
\inst42|seven_seg0[4]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~152_combout\ = (\inst39|inst41~q\ & (\inst39|inst42~q\ $ (!\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[4]~152_combout\);

-- Location: LCCOMB_X3_Y26_N28
\inst42|seven_seg0[4]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~157_combout\ = (\inst42|seven_seg0[4]~155_combout\ & (((\inst42|seven_seg0[4]~156_combout\)) # (!\inst39|inst8~q\))) # (!\inst42|seven_seg0[4]~155_combout\ & (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~152_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~155_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[4]~156_combout\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~157_combout\);

-- Location: LCCOMB_X8_Y26_N12
\inst42|seven_seg0[4]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~158_combout\ = (\inst39|inst42~q\ & (!\inst39|inst19~q\ & \inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[4]~158_combout\);

-- Location: LCCOMB_X3_Y26_N2
\inst42|seven_seg0[4]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~165_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[4]~158_combout\)) # (!\inst38|inst43~q\ & ((!\inst42|seven_seg0[4]~153_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~153_combout\,
	combout => \inst42|seven_seg0[4]~165_combout\);

-- Location: LCCOMB_X4_Y26_N28
\inst42|seven_seg0[4]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~166_combout\ = (\inst42|seven_seg0[4]~165_combout\ & (((!\inst39|inst8~q\)) # (!\inst42|seven_seg0[4]~154_combout\))) # (!\inst42|seven_seg0[4]~165_combout\ & (((\inst39|inst8~q\ & \inst42|seven_seg0[4]~156_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~165_combout\,
	datab => \inst42|seven_seg0[4]~154_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~156_combout\,
	combout => \inst42|seven_seg0[4]~166_combout\);

-- Location: LCCOMB_X4_Y26_N22
\inst42|seven_seg0[4]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~169_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg0[4]~157_combout\ & (!\inst38|inst19~q\))) # (!\inst38|inst42~q\ & (((\inst38|inst19~q\ & \inst42|seven_seg0[4]~166_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[4]~157_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[4]~166_combout\,
	combout => \inst42|seven_seg0[4]~169_combout\);

-- Location: LCCOMB_X3_Y26_N4
\inst42|seven_seg0[4]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~170_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[4]~156_combout\)) # (!\inst38|inst43~q\ & ((\inst42|seven_seg0[4]~152_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[4]~156_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~170_combout\);

-- Location: LCCOMB_X3_Y26_N18
\inst42|seven_seg0[4]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~171_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~170_combout\ & (\inst42|seven_seg0[4]~158_combout\)) # (!\inst42|seven_seg0[4]~170_combout\ & ((!\inst42|seven_seg0[4]~153_combout\))))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg0[4]~170_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[4]~170_combout\,
	datad => \inst42|seven_seg0[4]~153_combout\,
	combout => \inst42|seven_seg0[4]~171_combout\);

-- Location: LCCOMB_X4_Y26_N4
\inst42|seven_seg0[4]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~172_combout\ = (\inst42|seven_seg0[4]~169_combout\) # ((\inst42|seven_seg0[4]~171_combout\ & (\inst38|inst42~q\ $ (!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[4]~169_combout\,
	datad => \inst42|seven_seg0[4]~171_combout\,
	combout => \inst42|seven_seg0[4]~172_combout\);

-- Location: LCCOMB_X3_Y26_N30
\inst42|seven_seg0[4]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~159_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\)) # (!\inst42|seven_seg0[4]~154_combout\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg0[4]~156_combout\ & !\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~154_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[4]~156_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[4]~159_combout\);

-- Location: LCCOMB_X3_Y26_N24
\inst42|seven_seg0[4]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~160_combout\ = (\inst42|seven_seg0[4]~159_combout\ & ((\inst42|seven_seg0[4]~152_combout\) # ((!\inst39|inst8~q\)))) # (!\inst42|seven_seg0[4]~159_combout\ & (((\inst42|seven_seg0[4]~158_combout\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~159_combout\,
	datab => \inst42|seven_seg0[4]~152_combout\,
	datac => \inst42|seven_seg0[4]~158_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[4]~160_combout\);

-- Location: LCCOMB_X4_Y26_N0
\inst42|seven_seg0[4]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~161_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst19~q\ & \inst42|seven_seg0[4]~160_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[4]~157_combout\ & (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[4]~157_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[4]~160_combout\,
	combout => \inst42|seven_seg0[4]~161_combout\);

-- Location: LCCOMB_X3_Y26_N6
\inst42|seven_seg0[4]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~162_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg0[4]~152_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[4]~158_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~158_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[4]~162_combout\);

-- Location: LCCOMB_X3_Y26_N12
\inst42|seven_seg0[4]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~163_combout\ = (\inst42|seven_seg0[4]~162_combout\ & (((!\inst39|inst8~q\)) # (!\inst42|seven_seg0[4]~153_combout\))) # (!\inst42|seven_seg0[4]~162_combout\ & (((\inst39|inst8~q\ & !\inst42|seven_seg0[4]~154_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101001111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~162_combout\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[4]~154_combout\,
	combout => \inst42|seven_seg0[4]~163_combout\);

-- Location: LCCOMB_X4_Y26_N14
\inst42|seven_seg0[4]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~164_combout\ = (\inst42|seven_seg0[4]~161_combout\) # ((\inst42|seven_seg0[4]~163_combout\ & (\inst38|inst42~q\ $ (!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[4]~161_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[4]~163_combout\,
	combout => \inst42|seven_seg0[4]~164_combout\);

-- Location: LCCOMB_X4_Y26_N18
\inst42|seven_seg0[4]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~167_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg0[4]~166_combout\ & (!\inst38|inst19~q\))) # (!\inst38|inst42~q\ & (((\inst38|inst19~q\ & \inst42|seven_seg0[4]~163_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[4]~166_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[4]~163_combout\,
	combout => \inst42|seven_seg0[4]~167_combout\);

-- Location: LCCOMB_X4_Y26_N20
\inst42|seven_seg0[4]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~168_combout\ = (\inst42|seven_seg0[4]~167_combout\) # ((\inst42|seven_seg0[4]~160_combout\ & (\inst38|inst42~q\ $ (!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg0[4]~160_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[4]~167_combout\,
	combout => \inst42|seven_seg0[4]~168_combout\);

-- Location: LCCOMB_X4_Y26_N10
\inst42|seven_seg0[4]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~173_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg0[4]~168_combout\) # ((\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & (((\inst42|seven_seg0[4]~172_combout\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg0[4]~168_combout\,
	datac => \inst42|seven_seg0[4]~172_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[4]~173_combout\);

-- Location: LCCOMB_X4_Y26_N12
\inst42|seven_seg0[4]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~174_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg0[4]~173_combout\ & (\inst42|seven_seg0[4]~172_combout\)) # (!\inst42|seven_seg0[4]~173_combout\ & ((\inst42|seven_seg0[4]~164_combout\))))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg0[4]~173_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[4]~172_combout\,
	datac => \inst42|seven_seg0[4]~164_combout\,
	datad => \inst42|seven_seg0[4]~173_combout\,
	combout => \inst42|seven_seg0[4]~174_combout\);

-- Location: LCCOMB_X4_Y26_N8
\inst42|seven_seg0[4]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[4]~176_combout\ = (\inst42|seven_seg0[4]~175_combout\ & (((!\inst42|Equal0~4_combout\)))) # (!\inst42|seven_seg0[4]~175_combout\ & ((\inst39|inst43~q\) # ((\inst42|seven_seg0[4]~174_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~175_combout\,
	datab => \inst39|inst43~q\,
	datac => \inst42|Equal0~4_combout\,
	datad => \inst42|seven_seg0[4]~174_combout\,
	combout => \inst42|seven_seg0[4]~176_combout\);

-- Location: LCCOMB_X9_Y27_N8
\inst42|seven_seg0[0]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~189_combout\ = (\inst38|inst43~q\ & ((\inst38|inst19~q\) # (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[0]~189_combout\);

-- Location: LCCOMB_X8_Y25_N14
\inst42|seven_seg0[3]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~190_combout\ = (\inst42|seven_seg0[0]~97_combout\ & (((\inst42|seven_seg0[6]~79_combout\) # (\inst42|seven_seg0[0]~189_combout\)))) # (!\inst42|seven_seg0[0]~97_combout\ & (\inst42|seven_seg0[6]~274_combout\ & 
-- ((!\inst42|seven_seg0[0]~189_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~274_combout\,
	datab => \inst42|seven_seg0[6]~79_combout\,
	datac => \inst42|seven_seg0[0]~97_combout\,
	datad => \inst42|seven_seg0[0]~189_combout\,
	combout => \inst42|seven_seg0[3]~190_combout\);

-- Location: LCCOMB_X5_Y25_N6
\inst42|seven_seg0[0]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~191_combout\ = (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[0]~191_combout\);

-- Location: LCCOMB_X5_Y25_N4
\inst42|seven_seg0[3]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~192_combout\ = (\inst42|seven_seg0[0]~191_combout\ & (!\inst42|seven_seg0[6]~71_combout\)) # (!\inst42|seven_seg0[0]~191_combout\ & ((\inst42|seven_seg0[6]~71_combout\ & ((!\inst42|seven_seg0[6]~68_combout\))) # 
-- (!\inst42|seven_seg0[6]~71_combout\ & (\inst42|seven_seg0[6]~77_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001001110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~191_combout\,
	datab => \inst42|seven_seg0[6]~71_combout\,
	datac => \inst42|seven_seg0[6]~77_combout\,
	datad => \inst42|seven_seg0[6]~68_combout\,
	combout => \inst42|seven_seg0[3]~192_combout\);

-- Location: LCCOMB_X5_Y25_N18
\inst42|seven_seg0[6]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~180_combout\ = (\inst42|seven_seg0[6]~106_combout\) # (\inst38|inst41~q\ $ (\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datac => \inst42|seven_seg0[6]~106_combout\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~180_combout\);

-- Location: LCCOMB_X5_Y25_N30
\inst42|seven_seg0[3]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~193_combout\ = (\inst42|seven_seg0[0]~191_combout\ & ((\inst42|seven_seg0[3]~192_combout\ & (\inst42|seven_seg0[6]~281_combout\)) # (!\inst42|seven_seg0[3]~192_combout\ & ((\inst42|seven_seg0[6]~180_combout\))))) # 
-- (!\inst42|seven_seg0[0]~191_combout\ & (((\inst42|seven_seg0[3]~192_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~191_combout\,
	datab => \inst42|seven_seg0[6]~281_combout\,
	datac => \inst42|seven_seg0[3]~192_combout\,
	datad => \inst42|seven_seg0[6]~180_combout\,
	combout => \inst42|seven_seg0[3]~193_combout\);

-- Location: LCCOMB_X8_Y25_N20
\inst42|seven_seg0[3]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~194_combout\ = (\inst42|seven_seg0[3]~190_combout\ & (((\inst42|seven_seg0[3]~193_combout\) # (!\inst42|seven_seg0[0]~189_combout\)))) # (!\inst42|seven_seg0[3]~190_combout\ & (\inst42|seven_seg0[6]~103_combout\ & 
-- ((\inst42|seven_seg0[0]~189_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~103_combout\,
	datab => \inst42|seven_seg0[3]~190_combout\,
	datac => \inst42|seven_seg0[3]~193_combout\,
	datad => \inst42|seven_seg0[0]~189_combout\,
	combout => \inst42|seven_seg0[3]~194_combout\);

-- Location: LCCOMB_X8_Y25_N10
\inst42|seven_seg0[3]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~195_combout\ = (\inst39|inst43~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg0[3]~194_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg0[6]~80_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|seven_seg0[3]~194_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[6]~80_combout\,
	combout => \inst42|seven_seg0[3]~195_combout\);

-- Location: LCCOMB_X8_Y24_N16
\inst42|seven_seg0[6]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~177_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg0[6]~86_combout\)))) # (!\inst38|inst42~q\ & ((\inst42|seven_seg0[6]~88_combout\) # ((\inst42|seven_seg0[6]~89_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~88_combout\,
	datab => \inst42|seven_seg0[6]~86_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[6]~89_combout\,
	combout => \inst42|seven_seg0[6]~177_combout\);

-- Location: LCCOMB_X8_Y24_N2
\inst42|seven_seg0[6]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~178_combout\ = (!\inst38|inst8~q\ & ((\inst42|seven_seg0[6]~66_combout\ & ((\inst42|seven_seg0[6]~177_combout\))) # (!\inst42|seven_seg0[6]~66_combout\ & (\inst42|seven_seg0[6]~93_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg0[6]~66_combout\,
	datac => \inst42|seven_seg0[6]~93_combout\,
	datad => \inst42|seven_seg0[6]~177_combout\,
	combout => \inst42|seven_seg0[6]~178_combout\);

-- Location: LCCOMB_X6_Y26_N20
\inst42|Equal0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~5_combout\ = (!\inst38|inst42~q\ & \inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|Equal0~5_combout\);

-- Location: LCCOMB_X9_Y24_N4
\inst42|seven_seg0[6]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~182_combout\ = (\inst38|inst42~q\ & (\inst39|inst8~q\ $ (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~182_combout\);

-- Location: LCCOMB_X9_Y24_N2
\inst42|seven_seg0[6]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~183_combout\ = (\inst42|Equal0~5_combout\ & ((\inst42|seven_seg0[6]~93_combout\) # ((\inst42|seven_seg0[6]~182_combout\ & \inst42|seven_seg0[6]~81_combout\)))) # (!\inst42|Equal0~5_combout\ & (((\inst42|seven_seg0[6]~182_combout\ & 
-- \inst42|seven_seg0[6]~81_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~5_combout\,
	datab => \inst42|seven_seg0[6]~93_combout\,
	datac => \inst42|seven_seg0[6]~182_combout\,
	datad => \inst42|seven_seg0[6]~81_combout\,
	combout => \inst42|seven_seg0[6]~183_combout\);

-- Location: LCCOMB_X9_Y24_N12
\inst42|seven_seg0[6]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~96_combout\ = \inst39|inst8~q\ $ (\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~96_combout\);

-- Location: LCCOMB_X9_Y24_N20
\inst42|seven_seg0[6]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~184_combout\ = (\inst38|inst42~q\ & (!\inst42|seven_seg0[6]~96_combout\ & (\inst42|seven_seg0[6]~82_combout\))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg0[6]~86_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~96_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[6]~82_combout\,
	datad => \inst42|seven_seg0[6]~86_combout\,
	combout => \inst42|seven_seg0[6]~184_combout\);

-- Location: LCCOMB_X5_Y25_N22
\inst42|seven_seg0[6]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~107_combout\ = (!\inst39|inst19~q\ & !\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[6]~107_combout\);

-- Location: LCCOMB_X8_Y26_N30
\inst42|seven_seg0[6]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~179_combout\ = (\inst39|inst8~q\ & (\inst39|inst41~q\ & (\inst38|inst42~q\ & \inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[6]~179_combout\);

-- Location: LCCOMB_X5_Y25_N0
\inst42|seven_seg0[6]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~181_combout\ = (\inst42|seven_seg0[6]~179_combout\ & ((\inst39|inst42~q\ & ((\inst42|seven_seg0[6]~180_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[6]~107_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~107_combout\,
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg0[6]~179_combout\,
	datad => \inst42|seven_seg0[6]~180_combout\,
	combout => \inst42|seven_seg0[6]~181_combout\);

-- Location: LCCOMB_X8_Y24_N12
\inst42|seven_seg0[6]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~185_combout\ = (\inst42|seven_seg0[6]~183_combout\) # ((\inst42|seven_seg0[6]~181_combout\) # ((!\inst38|inst19~q\ & \inst42|seven_seg0[6]~184_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~183_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~184_combout\,
	datad => \inst42|seven_seg0[6]~181_combout\,
	combout => \inst42|seven_seg0[6]~185_combout\);

-- Location: LCCOMB_X8_Y24_N0
\inst42|seven_seg0[3]~286\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~286_combout\ = (\inst38|inst8~q\ & (\inst38|inst43~q\ & ((\inst42|seven_seg0[6]~178_combout\) # (\inst42|seven_seg0[6]~185_combout\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[6]~178_combout\ & (!\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010010000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg0[6]~178_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[6]~185_combout\,
	combout => \inst42|seven_seg0[3]~286_combout\);

-- Location: LCCOMB_X7_Y25_N4
\inst42|seven_seg0[6]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~186_combout\ = \inst38|inst8~q\ $ (\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[6]~186_combout\);

-- Location: LCCOMB_X8_Y24_N26
\inst42|seven_seg0[3]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~187_combout\ = (\inst38|inst19~q\ & (((\inst42|seven_seg0[6]~91_combout\)))) # (!\inst38|inst19~q\ & ((\inst42|seven_seg0[6]~88_combout\) # ((\inst42|seven_seg0[6]~89_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~88_combout\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg0[6]~91_combout\,
	datad => \inst42|seven_seg0[6]~89_combout\,
	combout => \inst42|seven_seg0[3]~187_combout\);

-- Location: LCCOMB_X8_Y25_N28
\inst42|seven_seg0[3]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~188_combout\ = (\inst42|seven_seg0[6]~186_combout\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg0[3]~187_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[6]~278_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~278_combout\,
	datab => \inst42|seven_seg0[6]~186_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[3]~187_combout\,
	combout => \inst42|seven_seg0[3]~188_combout\);

-- Location: LCCOMB_X8_Y25_N0
\inst42|seven_seg0[3]~287\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[3]~287_combout\ = (\inst42|seven_seg0[3]~195_combout\) # ((!\inst39|inst43~q\ & ((\inst42|seven_seg0[3]~286_combout\) # (\inst42|seven_seg0[3]~188_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[3]~195_combout\,
	datab => \inst42|seven_seg0[3]~286_combout\,
	datac => \inst39|inst43~q\,
	datad => \inst42|seven_seg0[3]~188_combout\,
	combout => \inst42|seven_seg0[3]~287_combout\);

-- Location: LCCOMB_X3_Y26_N16
\inst42|seven_seg0[2]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~196_combout\ = (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # (!\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011111100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[2]~196_combout\);

-- Location: LCCOMB_X7_Y26_N0
\inst42|seven_seg0[2]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~207_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg0[4]~158_combout\))) # (!\inst39|inst8~q\ & (!\inst42|seven_seg0[2]~196_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[2]~196_combout\,
	datac => \inst42|seven_seg0[4]~158_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[2]~207_combout\);

-- Location: LCCOMB_X8_Y26_N28
\inst42|seven_seg0[6]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~206_combout\ = \inst39|inst19~q\ $ (\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~206_combout\);

-- Location: LCCOMB_X7_Y26_N26
\inst42|seven_seg0[2]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~208_combout\ = (\inst38|inst43~q\ & (!\inst42|seven_seg0[6]~206_combout\ & (\inst42|seven_seg0[2]~207_combout\ $ (!\inst39|inst41~q\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[2]~207_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010011000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[2]~207_combout\,
	datac => \inst39|inst41~q\,
	datad => \inst42|seven_seg0[6]~206_combout\,
	combout => \inst42|seven_seg0[2]~208_combout\);

-- Location: LCCOMB_X3_Y26_N22
\inst42|seven_seg0[2]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~200_combout\ = (\inst42|seven_seg0[4]~165_combout\ & (((!\inst39|inst8~q\)) # (!\inst42|seven_seg0[4]~154_combout\))) # (!\inst42|seven_seg0[4]~165_combout\ & (((!\inst42|seven_seg0[2]~196_combout\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~154_combout\,
	datab => \inst42|seven_seg0[2]~196_combout\,
	datac => \inst42|seven_seg0[4]~165_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[2]~200_combout\);

-- Location: LCCOMB_X7_Y26_N2
\inst42|seven_seg0[2]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~205_combout\ = (\inst38|inst19~q\ & (!\inst38|inst42~q\ & (\inst42|seven_seg0[4]~163_combout\))) # (!\inst38|inst19~q\ & (\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~200_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[2]~200_combout\,
	combout => \inst42|seven_seg0[2]~205_combout\);

-- Location: LCCOMB_X7_Y26_N12
\inst42|seven_seg0[2]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~209_combout\ = (\inst42|seven_seg0[2]~205_combout\) # ((\inst42|seven_seg0[2]~208_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[2]~208_combout\,
	datad => \inst42|seven_seg0[2]~205_combout\,
	combout => \inst42|seven_seg0[2]~209_combout\);

-- Location: LCCOMB_X3_Y26_N26
\inst42|seven_seg0[2]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~197_combout\ = (\inst39|inst8~q\ & (((\inst38|inst43~q\)) # (!\inst42|seven_seg0[4]~153_combout\))) # (!\inst39|inst8~q\ & (((!\inst38|inst43~q\ & \inst42|seven_seg0[4]~152_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010011110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[4]~153_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg0[4]~152_combout\,
	combout => \inst42|seven_seg0[2]~197_combout\);

-- Location: LCCOMB_X7_Y26_N28
\inst42|seven_seg0[2]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~198_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[2]~197_combout\ & ((\inst42|seven_seg0[4]~158_combout\))) # (!\inst42|seven_seg0[2]~197_combout\ & (!\inst42|seven_seg0[2]~196_combout\)))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[2]~197_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[2]~196_combout\,
	datac => \inst42|seven_seg0[4]~158_combout\,
	datad => \inst42|seven_seg0[2]~197_combout\,
	combout => \inst42|seven_seg0[2]~198_combout\);

-- Location: LCCOMB_X7_Y26_N14
\inst42|seven_seg0[2]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~203_combout\ = (\inst38|inst19~q\ & (!\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~198_combout\)))) # (!\inst38|inst19~q\ & (\inst38|inst42~q\ & (\inst42|seven_seg0[4]~163_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[2]~198_combout\,
	combout => \inst42|seven_seg0[2]~203_combout\);

-- Location: LCCOMB_X3_Y26_N20
\inst42|seven_seg0[2]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~199_combout\ = (\inst42|seven_seg0[4]~155_combout\ & (((!\inst42|seven_seg0[2]~196_combout\) # (!\inst39|inst8~q\)))) # (!\inst42|seven_seg0[4]~155_combout\ & (\inst42|seven_seg0[4]~152_combout\ & (\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~155_combout\,
	datab => \inst42|seven_seg0[4]~152_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[2]~196_combout\,
	combout => \inst42|seven_seg0[2]~199_combout\);

-- Location: LCCOMB_X7_Y26_N16
\inst42|seven_seg0[2]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~204_combout\ = (\inst42|seven_seg0[2]~203_combout\) # ((\inst42|seven_seg0[2]~199_combout\ & (\inst38|inst19~q\ $ (!\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[2]~203_combout\,
	datad => \inst42|seven_seg0[2]~199_combout\,
	combout => \inst42|seven_seg0[2]~204_combout\);

-- Location: LCCOMB_X7_Y26_N6
\inst42|seven_seg0[2]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~210_combout\ = (\inst38|inst8~q\ & (((\inst42|seven_seg0[2]~204_combout\) # (\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg0[2]~209_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~209_combout\,
	datab => \inst42|seven_seg0[2]~204_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[2]~210_combout\);

-- Location: LCCOMB_X6_Y27_N20
\inst42|seven_seg0[2]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~212_combout\ = (\inst39|inst42~q\) # ((\inst39|inst19~q\) # (!\inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[2]~212_combout\);

-- Location: LCCOMB_X7_Y26_N18
\inst42|seven_seg0[2]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~213_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[2]~207_combout\ & ((!\inst42|seven_seg0[2]~212_combout\))) # (!\inst42|seven_seg0[2]~207_combout\ & (!\inst42|seven_seg0[4]~154_combout\)))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[2]~207_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[4]~154_combout\,
	datac => \inst42|seven_seg0[2]~212_combout\,
	datad => \inst42|seven_seg0[2]~207_combout\,
	combout => \inst42|seven_seg0[2]~213_combout\);

-- Location: LCCOMB_X7_Y26_N20
\inst42|seven_seg0[2]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~211_combout\ = (\inst38|inst19~q\ & (\inst38|inst42~q\)) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~200_combout\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg0[2]~208_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg0[2]~208_combout\,
	datad => \inst42|seven_seg0[2]~200_combout\,
	combout => \inst42|seven_seg0[2]~211_combout\);

-- Location: LCCOMB_X7_Y26_N4
\inst42|seven_seg0[2]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~214_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg0[2]~211_combout\ & (\inst42|seven_seg0[2]~213_combout\)) # (!\inst42|seven_seg0[2]~211_combout\ & ((\inst42|seven_seg0[4]~163_combout\))))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg0[2]~211_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg0[2]~213_combout\,
	datac => \inst42|seven_seg0[4]~163_combout\,
	datad => \inst42|seven_seg0[2]~211_combout\,
	combout => \inst42|seven_seg0[2]~214_combout\);

-- Location: LCCOMB_X7_Y26_N10
\inst42|seven_seg0[2]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~201_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg0[2]~199_combout\)) # (!\inst38|inst42~q\ & ((\inst42|seven_seg0[2]~200_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~199_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg0[2]~200_combout\,
	combout => \inst42|seven_seg0[2]~201_combout\);

-- Location: LCCOMB_X7_Y26_N8
\inst42|seven_seg0[2]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~202_combout\ = (\inst42|seven_seg0[2]~201_combout\ & ((\inst42|seven_seg0[2]~198_combout\) # (\inst38|inst42~q\ $ (\inst38|inst19~q\)))) # (!\inst42|seven_seg0[2]~201_combout\ & (\inst42|seven_seg0[2]~198_combout\ & 
-- (\inst38|inst42~q\ $ (!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~201_combout\,
	datab => \inst42|seven_seg0[2]~198_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[2]~202_combout\);

-- Location: LCCOMB_X7_Y26_N22
\inst42|seven_seg0[2]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~215_combout\ = (\inst42|seven_seg0[2]~210_combout\ & ((\inst42|seven_seg0[2]~214_combout\) # ((!\inst38|inst41~q\)))) # (!\inst42|seven_seg0[2]~210_combout\ & (((\inst42|seven_seg0[2]~202_combout\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[2]~210_combout\,
	datab => \inst42|seven_seg0[2]~214_combout\,
	datac => \inst42|seven_seg0[2]~202_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[2]~215_combout\);

-- Location: LCCOMB_X4_Y26_N2
\inst42|seven_seg0[2]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[2]~216_combout\ = (\inst42|seven_seg0[4]~175_combout\ & (((\inst42|Equal0~4_combout\)))) # (!\inst42|seven_seg0[4]~175_combout\ & (!\inst39|inst43~q\ & ((\inst42|seven_seg0[2]~215_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[4]~175_combout\,
	datab => \inst39|inst43~q\,
	datac => \inst42|Equal0~4_combout\,
	datad => \inst42|seven_seg0[2]~215_combout\,
	combout => \inst42|seven_seg0[2]~216_combout\);

-- Location: LCCOMB_X7_Y24_N30
\inst42|seven_seg0[1]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~228_combout\ = \inst39|inst43~q\ $ (\inst39|inst19~q\ $ (\inst39|inst42~q\ $ (\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~228_combout\);

-- Location: LCCOMB_X7_Y24_N4
\inst42|seven_seg0[1]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~231_combout\ = (\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst38|inst43~q\))))) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\ & (!\inst39|inst42~q\ 
-- & !\inst38|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~231_combout\);

-- Location: LCCOMB_X7_Y24_N18
\inst42|seven_seg0[1]~294\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~294_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[1]~231_combout\ & (\inst42|seven_seg0[1]~228_combout\ $ (\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (!\inst42|seven_seg0[1]~228_combout\ & (\inst42|seven_seg0[1]~231_combout\ $ 
-- (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~228_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[1]~231_combout\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[1]~294_combout\);

-- Location: LCCOMB_X7_Y24_N28
\inst42|seven_seg0[1]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~227_combout\ = (\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\))))) # (!\inst39|inst43~q\ & (!\inst39|inst19~q\ & 
-- (\inst39|inst42~q\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~227_combout\);

-- Location: LCCOMB_X7_Y24_N12
\inst42|seven_seg0[1]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~229_combout\ = \inst42|seven_seg0[1]~228_combout\ $ (((\inst39|inst8~q\ & \inst42|seven_seg0[1]~227_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[1]~227_combout\,
	datac => \inst42|seven_seg0[1]~228_combout\,
	combout => \inst42|seven_seg0[1]~229_combout\);

-- Location: LCCOMB_X7_Y24_N2
\inst42|seven_seg0[1]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~230_combout\ = (!\inst38|inst41~q\ & ((\inst42|seven_seg0[1]~227_combout\ & (\inst42|seven_seg0[6]~67_combout\ & \inst42|seven_seg0[1]~229_combout\)) # (!\inst42|seven_seg0[1]~227_combout\ & (!\inst42|seven_seg0[6]~67_combout\ & 
-- !\inst42|seven_seg0[1]~229_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[1]~227_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[1]~229_combout\,
	combout => \inst42|seven_seg0[1]~230_combout\);

-- Location: LCCOMB_X7_Y24_N26
\inst42|seven_seg0[1]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~232_combout\ = (\inst38|inst41~q\ & (!\inst38|inst8~q\ & ((\inst42|seven_seg0[1]~294_combout\) # (\inst42|seven_seg0[1]~230_combout\)))) # (!\inst38|inst41~q\ & (((\inst38|inst8~q\ & \inst42|seven_seg0[1]~230_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[1]~294_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[1]~230_combout\,
	combout => \inst42|seven_seg0[1]~232_combout\);

-- Location: LCCOMB_X7_Y23_N0
\inst42|seven_seg0[1]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~234_combout\ = (\inst39|inst41~q\ & (\inst39|inst8~q\ $ (((\inst39|inst43~q\) # (!\inst39|inst19~q\))))) # (!\inst39|inst41~q\ & ((\inst39|inst8~q\) # ((!\inst39|inst43~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001110011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[1]~234_combout\);

-- Location: LCCOMB_X7_Y24_N24
\inst42|seven_seg0[1]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~233_combout\ = (\inst39|inst43~q\ & (!\inst39|inst19~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\)))) # (!\inst39|inst43~q\ & (\inst39|inst19~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101001010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg0[1]~233_combout\);

-- Location: LCCOMB_X7_Y24_N6
\inst42|seven_seg0[1]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~235_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[1]~234_combout\ & (\inst39|inst42~q\ & \inst42|seven_seg0[1]~233_combout\)) # (!\inst42|seven_seg0[1]~234_combout\ & (!\inst39|inst42~q\ & 
-- !\inst42|seven_seg0[1]~233_combout\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~234_combout\ & (\inst39|inst42~q\ $ (\inst42|seven_seg0[1]~233_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000100010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[1]~234_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[1]~233_combout\,
	combout => \inst42|seven_seg0[1]~235_combout\);

-- Location: LCCOMB_X7_Y24_N16
\inst42|seven_seg0[1]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~236_combout\ = (\inst42|seven_seg0[1]~232_combout\) # ((\inst42|seven_seg0[1]~235_combout\ & (\inst38|inst41~q\ $ (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg0[1]~232_combout\,
	datad => \inst42|seven_seg0[1]~235_combout\,
	combout => \inst42|seven_seg0[1]~236_combout\);

-- Location: LCCOMB_X9_Y25_N16
\inst42|seven_seg0[1]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~217_combout\ = (\inst39|inst19~q\ & ((\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\)) # (!\inst39|inst43~q\ & (!\inst38|inst8~q\ & !\inst39|inst42~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst43~q\ & ((\inst38|inst8~q\) # 
-- (\inst39|inst42~q\))) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~217_combout\);

-- Location: LCCOMB_X9_Y25_N28
\inst42|seven_seg0[1]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~239_combout\ = \inst39|inst19~q\ $ (\inst39|inst43~q\ $ (\inst38|inst8~q\ $ (\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~239_combout\);

-- Location: LCCOMB_X9_Y25_N26
\inst42|seven_seg0[1]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~240_combout\ = \inst42|seven_seg0[1]~217_combout\ $ (((\inst38|inst43~q\ & !\inst42|seven_seg0[1]~239_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[1]~217_combout\,
	datad => \inst42|seven_seg0[1]~239_combout\,
	combout => \inst42|seven_seg0[1]~240_combout\);

-- Location: LCCOMB_X9_Y24_N26
\inst42|seven_seg0[5]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~137_combout\ = \inst38|inst41~q\ $ (\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~137_combout\);

-- Location: LCCOMB_X9_Y25_N12
\inst42|seven_seg0[1]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~241_combout\ = (!\inst39|inst8~q\ & ((\inst42|seven_seg0[1]~240_combout\ & (\inst42|seven_seg0[5]~137_combout\ & !\inst42|seven_seg0[1]~239_combout\)) # (!\inst42|seven_seg0[1]~240_combout\ & (!\inst42|seven_seg0[5]~137_combout\ & 
-- \inst42|seven_seg0[1]~239_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~240_combout\,
	datab => \inst42|seven_seg0[5]~137_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~239_combout\,
	combout => \inst42|seven_seg0[1]~241_combout\);

-- Location: LCCOMB_X7_Y23_N20
\inst42|seven_seg0[1]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~238_combout\ = (\inst38|inst8~q\ & (!\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst39|inst43~q\))) # (!\inst38|inst8~q\ & (\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~238_combout\);

-- Location: LCCOMB_X7_Y23_N22
\inst42|seven_seg0[1]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~237_combout\ = (\inst38|inst8~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst39|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst39|inst43~q\))))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\ & (!\inst39|inst42~q\ & 
-- !\inst39|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst39|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~237_combout\);

-- Location: LCCOMB_X7_Y23_N24
\inst42|seven_seg0[1]~288\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~288_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[1]~238_combout\ & (!\inst42|seven_seg0[1]~237_combout\ & \inst38|inst41~q\)) # (!\inst42|seven_seg0[1]~238_combout\ & (\inst42|seven_seg0[1]~237_combout\ & 
-- !\inst38|inst41~q\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[1]~238_combout\ & (\inst42|seven_seg0[1]~237_combout\ $ (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[1]~238_combout\,
	datac => \inst42|seven_seg0[1]~237_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[1]~288_combout\);

-- Location: LCCOMB_X9_Y25_N30
\inst42|seven_seg0[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~218_combout\ = (\inst39|inst19~q\ & (\inst39|inst43~q\ $ (\inst38|inst8~q\ $ (!\inst39|inst42~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst43~q\ & (!\inst38|inst8~q\ & !\inst39|inst42~q\)) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ $ 
-- (\inst39|inst42~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~218_combout\);

-- Location: LCCOMB_X9_Y25_N4
\inst42|seven_seg0[1]~289\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~289_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg0[1]~218_combout\)) # (!\inst39|inst8~q\ & ((\inst38|inst43~q\ $ (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110010100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~218_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg0[1]~289_combout\);

-- Location: LCCOMB_X9_Y25_N18
\inst42|seven_seg0[1]~290\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~290_combout\ = (\inst38|inst41~q\ & (!\inst42|seven_seg0[1]~217_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[1]~289_combout\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[1]~289_combout\ & 
-- \inst42|seven_seg0[1]~217_combout\)) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~289_combout\ & !\inst42|seven_seg0[1]~217_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[1]~289_combout\,
	datad => \inst42|seven_seg0[1]~217_combout\,
	combout => \inst42|seven_seg0[1]~290_combout\);

-- Location: LCCOMB_X9_Y25_N14
\inst42|seven_seg0[1]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~242_combout\ = (\inst42|seven_seg0[6]~67_combout\ & ((\inst42|seven_seg0[1]~241_combout\) # ((\inst42|seven_seg0[1]~290_combout\)))) # (!\inst42|seven_seg0[6]~67_combout\ & (((\inst42|seven_seg0[1]~288_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~241_combout\,
	datab => \inst42|seven_seg0[1]~288_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[1]~290_combout\,
	combout => \inst42|seven_seg0[1]~242_combout\);

-- Location: LCCOMB_X8_Y26_N10
\inst42|seven_seg0[1]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~243_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg0[1]~236_combout\) # ((\inst38|inst19~q\)))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg0[1]~242_combout\ & !\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~236_combout\,
	datab => \inst42|seven_seg0[1]~242_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[1]~243_combout\);

-- Location: LCCOMB_X6_Y24_N18
\inst42|seven_seg0[1]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~255_combout\ = (\inst39|inst41~q\ & (\inst39|inst43~q\ & !\inst39|inst19~q\)) # (!\inst39|inst41~q\ & (!\inst39|inst43~q\ & \inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[1]~255_combout\);

-- Location: LCCOMB_X7_Y23_N10
\inst42|Equal0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~8_combout\ = (\inst42|seven_seg1[3]~39_combout\ & (\inst38|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~39_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|Equal0~8_combout\);

-- Location: LCCOMB_X6_Y24_N28
\inst42|seven_seg0[1]~256\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~256_combout\ = (\inst42|seven_seg0[1]~255_combout\ & ((\inst39|inst41~q\ & (\inst39|inst42~q\)) # (!\inst39|inst41~q\ & (!\inst39|inst42~q\ & \inst42|Equal0~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|seven_seg0[1]~255_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|Equal0~8_combout\,
	combout => \inst42|seven_seg0[1]~256_combout\);

-- Location: LCCOMB_X6_Y24_N26
\inst42|seven_seg0[1]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~253_combout\ = (\inst39|inst19~q\ & ((\inst39|inst42~q\ & ((\inst39|inst43~q\))) # (!\inst39|inst42~q\ & (!\inst39|inst41~q\ & !\inst39|inst43~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\ & ((!\inst39|inst43~q\) # 
-- (!\inst39|inst41~q\))) # (!\inst39|inst42~q\ & ((\inst39|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001100110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~253_combout\);

-- Location: LCCOMB_X6_Y24_N12
\inst42|seven_seg0[1]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~254_combout\ = (\inst39|inst8~q\ & ((\inst38|inst43~q\) # ((!\inst42|seven_seg0[1]~253_combout\ & !\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[1]~253_combout\ & (\inst39|inst41~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[1]~253_combout\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~254_combout\);

-- Location: LCCOMB_X6_Y24_N0
\inst42|seven_seg0[1]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~252_combout\ = (\inst39|inst41~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst39|inst43~q\))) # (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst39|inst43~q\)))) # (!\inst39|inst41~q\ & (!\inst39|inst19~q\ & 
-- (\inst39|inst42~q\ & \inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100010000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~252_combout\);

-- Location: LCCOMB_X6_Y24_N14
\inst42|seven_seg0[1]~257\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~257_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg0[1]~254_combout\ & (\inst42|seven_seg0[1]~256_combout\)) # (!\inst42|seven_seg0[1]~254_combout\ & ((\inst42|seven_seg0[1]~252_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg0[1]~254_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[1]~256_combout\,
	datac => \inst42|seven_seg0[1]~254_combout\,
	datad => \inst42|seven_seg0[1]~252_combout\,
	combout => \inst42|seven_seg0[1]~257_combout\);

-- Location: LCCOMB_X6_Y24_N24
\inst42|seven_seg0[1]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~244_combout\ = (\inst39|inst8~q\ & ((\inst39|inst43~q\ & (\inst39|inst19~q\ & !\inst38|inst43~q\)) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\) # (!\inst38|inst43~q\))))) # (!\inst39|inst8~q\ & (\inst39|inst43~q\ $ (\inst39|inst19~q\ 
-- $ (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~244_combout\);

-- Location: LCCOMB_X7_Y24_N10
\inst42|seven_seg0[1]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~245_combout\ = (\inst39|inst8~q\ & (\inst39|inst43~q\ $ (\inst39|inst19~q\ $ (\inst38|inst43~q\)))) # (!\inst39|inst8~q\ & ((\inst39|inst43~q\ & (!\inst39|inst19~q\ & \inst38|inst43~q\)) # (!\inst39|inst43~q\ & (\inst39|inst19~q\ & 
-- !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~245_combout\);

-- Location: LCCOMB_X7_Y24_N20
\inst42|seven_seg0[1]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~246_combout\ = (\inst39|inst42~q\ & (\inst42|seven_seg0[6]~67_combout\ & (\inst42|seven_seg0[1]~244_combout\ & !\inst42|seven_seg0[1]~245_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg0[1]~245_combout\ & 
-- (\inst42|seven_seg0[6]~67_combout\ $ (\inst42|seven_seg0[1]~244_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~67_combout\,
	datab => \inst42|seven_seg0[1]~244_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg0[1]~245_combout\,
	combout => \inst42|seven_seg0[1]~246_combout\);

-- Location: LCCOMB_X5_Y23_N10
\inst42|seven_seg0[5]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[5]~249_combout\ = (!\inst39|inst19~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[5]~249_combout\);

-- Location: LCCOMB_X6_Y24_N4
\inst42|Equal0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~7_combout\ = (\inst39|inst43~q\ & \inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|Equal0~7_combout\);

-- Location: LCCOMB_X6_Y24_N6
\inst42|seven_seg0[1]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~247_combout\ = (\inst38|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst39|inst43~q\))) # (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst39|inst43~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst19~q\ & 
-- (\inst39|inst42~q\ & \inst39|inst43~q\)) # (!\inst39|inst19~q\ & (\inst39|inst42~q\ $ (\inst39|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg0[1]~247_combout\);

-- Location: LCCOMB_X7_Y24_N14
\inst42|seven_seg0[1]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~248_combout\ = (\inst39|inst43~q\ & (((\inst39|inst42~q\) # (\inst38|inst43~q\)) # (!\inst39|inst19~q\))) # (!\inst39|inst43~q\ & ((\inst39|inst19~q\ & (\inst39|inst42~q\ $ (!\inst38|inst43~q\))) # (!\inst39|inst19~q\ & 
-- ((\inst39|inst42~q\) # (\inst38|inst43~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~248_combout\);

-- Location: LCCOMB_X7_Y24_N0
\inst42|seven_seg0[1]~291\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~291_combout\ = (\inst39|inst8~q\ & (((!\inst42|seven_seg0[1]~248_combout\ & !\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[1]~247_combout\ & ((\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg0[1]~247_combout\,
	datac => \inst42|seven_seg0[1]~248_combout\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[1]~291_combout\);

-- Location: LCCOMB_X6_Y24_N10
\inst42|seven_seg0[1]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~250_combout\ = (\inst42|seven_seg0[1]~291_combout\) # ((\inst42|seven_seg0[5]~249_combout\ & (\inst42|Equal0~7_combout\ & !\inst42|seven_seg0[6]~67_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~249_combout\,
	datab => \inst42|Equal0~7_combout\,
	datac => \inst42|seven_seg0[1]~291_combout\,
	datad => \inst42|seven_seg0[6]~67_combout\,
	combout => \inst42|seven_seg0[1]~250_combout\);

-- Location: LCCOMB_X7_Y24_N8
\inst42|seven_seg0[1]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~251_combout\ = (\inst38|inst41~q\ & (((\inst38|inst8~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg0[1]~294_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg0[1]~250_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg0[1]~294_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[1]~250_combout\,
	combout => \inst42|seven_seg0[1]~251_combout\);

-- Location: LCCOMB_X7_Y24_N22
\inst42|seven_seg0[1]~258\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~258_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg0[1]~251_combout\ & (\inst42|seven_seg0[1]~257_combout\)) # (!\inst42|seven_seg0[1]~251_combout\ & ((\inst42|seven_seg0[1]~246_combout\))))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg0[1]~251_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~257_combout\,
	datab => \inst42|seven_seg0[1]~246_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg0[1]~251_combout\,
	combout => \inst42|seven_seg0[1]~258_combout\);

-- Location: LCCOMB_X6_Y26_N12
\inst42|Equal0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~6_combout\ = (\inst39|inst42~q\ & (\inst39|inst43~q\ & (\inst38|inst8~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|Equal0~6_combout\);

-- Location: LCCOMB_X6_Y26_N18
\inst42|seven_seg0[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~224_combout\ = (\inst39|inst19~q\ & ((\inst39|inst43~q\ & (\inst38|inst8~q\ $ (\inst39|inst42~q\))) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst43~q\ & (!\inst38|inst8~q\ & 
-- !\inst39|inst42~q\)) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ $ (\inst39|inst42~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100110010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~224_combout\);

-- Location: LCCOMB_X6_Y26_N30
\inst42|seven_seg0[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~225_combout\ = (\inst42|Equal0~6_combout\ & (((\inst42|seven_seg0[1]~224_combout\ & \inst38|inst43~q\)) # (!\inst39|inst19~q\))) # (!\inst42|Equal0~6_combout\ & (\inst42|seven_seg0[1]~224_combout\ & ((\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~6_combout\,
	datab => \inst42|seven_seg0[1]~224_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg0[1]~225_combout\);

-- Location: LCCOMB_X9_Y25_N0
\inst42|seven_seg0[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~219_combout\ = (\inst39|inst19~q\ & (\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst43~q\ & (\inst38|inst8~q\ $ (\inst39|inst42~q\))) # (!\inst39|inst43~q\ & (\inst38|inst8~q\ & 
-- \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~219_combout\);

-- Location: LCCOMB_X9_Y25_N6
\inst42|seven_seg0[1]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~226_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\ & (\inst42|seven_seg0[1]~225_combout\)) # (!\inst42|seven_seg0[5]~137_combout\ & ((\inst42|seven_seg0[1]~219_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~137_combout\,
	datab => \inst42|seven_seg0[1]~225_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~219_combout\,
	combout => \inst42|seven_seg0[1]~226_combout\);

-- Location: LCCOMB_X9_Y25_N10
\inst42|seven_seg0[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~220_combout\ = (\inst39|inst19~q\ & (!\inst39|inst43~q\ & !\inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~220_combout\);

-- Location: LCCOMB_X9_Y25_N8
\inst42|seven_seg0[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~221_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg0[1]~219_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg0[1]~220_combout\ & (!\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~220_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg0[1]~219_combout\,
	combout => \inst42|seven_seg0[1]~221_combout\);

-- Location: LCCOMB_X9_Y25_N2
\inst42|seven_seg0[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~222_combout\ = (!\inst39|inst19~q\ & (\inst39|inst43~q\ & (\inst38|inst8~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[1]~222_combout\);

-- Location: LCCOMB_X9_Y25_N20
\inst42|seven_seg0[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~223_combout\ = (!\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\ & (\inst42|seven_seg0[1]~221_combout\)) # (!\inst42|seven_seg0[5]~137_combout\ & ((\inst42|seven_seg0[1]~222_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[5]~137_combout\,
	datab => \inst42|seven_seg0[1]~221_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[1]~222_combout\,
	combout => \inst42|seven_seg0[1]~223_combout\);

-- Location: LCCOMB_X9_Y25_N24
\inst42|seven_seg0[1]~295\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~295_combout\ = (\inst38|inst41~q\ & (!\inst42|seven_seg0[1]~217_combout\ & (\inst38|inst43~q\ $ (\inst42|seven_seg0[1]~218_combout\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg0[1]~218_combout\ & 
-- \inst42|seven_seg0[1]~217_combout\)) # (!\inst38|inst43~q\ & (!\inst42|seven_seg0[1]~218_combout\ & !\inst42|seven_seg0[1]~217_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg0[1]~218_combout\,
	datad => \inst42|seven_seg0[1]~217_combout\,
	combout => \inst42|seven_seg0[1]~295_combout\);

-- Location: LCCOMB_X9_Y25_N22
\inst42|seven_seg0[1]~296\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~296_combout\ = (\inst42|seven_seg0[6]~67_combout\ & ((\inst42|seven_seg0[1]~226_combout\) # ((\inst42|seven_seg0[1]~223_combout\)))) # (!\inst42|seven_seg0[6]~67_combout\ & (((\inst42|seven_seg0[1]~295_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~226_combout\,
	datab => \inst42|seven_seg0[1]~223_combout\,
	datac => \inst42|seven_seg0[6]~67_combout\,
	datad => \inst42|seven_seg0[1]~295_combout\,
	combout => \inst42|seven_seg0[1]~296_combout\);

-- Location: LCCOMB_X8_Y26_N4
\inst42|seven_seg0[1]~259\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[1]~259_combout\ = (\inst42|seven_seg0[1]~243_combout\ & ((\inst42|seven_seg0[1]~258_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg0[1]~243_combout\ & (((\inst42|seven_seg0[1]~296_combout\ & \inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[1]~243_combout\,
	datab => \inst42|seven_seg0[1]~258_combout\,
	datac => \inst42|seven_seg0[1]~296_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg0[1]~259_combout\);

-- Location: LCCOMB_X5_Y25_N20
\inst42|seven_seg0[0]~261\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~261_combout\ = (\inst42|seven_seg0[0]~191_combout\ & ((\inst42|seven_seg0[6]~180_combout\) # ((!\inst42|seven_seg0[6]~71_combout\)))) # (!\inst42|seven_seg0[0]~191_combout\ & (((\inst42|seven_seg0[6]~107_combout\ & 
-- \inst42|seven_seg0[6]~71_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~191_combout\,
	datab => \inst42|seven_seg0[6]~180_combout\,
	datac => \inst42|seven_seg0[6]~107_combout\,
	datad => \inst42|seven_seg0[6]~71_combout\,
	combout => \inst42|seven_seg0[0]~261_combout\);

-- Location: LCCOMB_X5_Y25_N10
\inst42|seven_seg0[0]~262\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~262_combout\ = (\inst42|seven_seg0[0]~261_combout\ & (((\inst42|seven_seg0[6]~106_combout\) # (\inst42|seven_seg0[6]~71_combout\)))) # (!\inst42|seven_seg0[0]~261_combout\ & (\inst42|seven_seg0[6]~82_combout\ & 
-- ((!\inst42|seven_seg0[6]~71_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~82_combout\,
	datab => \inst42|seven_seg0[0]~261_combout\,
	datac => \inst42|seven_seg0[6]~106_combout\,
	datad => \inst42|seven_seg0[6]~71_combout\,
	combout => \inst42|seven_seg0[0]~262_combout\);

-- Location: LCCOMB_X8_Y25_N8
\inst42|seven_seg0[0]~260\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~260_combout\ = (\inst42|seven_seg0[0]~189_combout\ & (((\inst42|seven_seg0[0]~97_combout\)))) # (!\inst42|seven_seg0[0]~189_combout\ & ((\inst42|seven_seg0[0]~97_combout\ & ((\inst42|seven_seg0[6]~91_combout\))) # 
-- (!\inst42|seven_seg0[0]~97_combout\ & (\inst42|seven_seg0[6]~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~95_combout\,
	datab => \inst42|seven_seg0[0]~189_combout\,
	datac => \inst42|seven_seg0[0]~97_combout\,
	datad => \inst42|seven_seg0[6]~91_combout\,
	combout => \inst42|seven_seg0[0]~260_combout\);

-- Location: LCCOMB_X8_Y25_N26
\inst42|seven_seg0[0]~263\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~263_combout\ = (\inst42|seven_seg0[0]~260_combout\ & ((\inst42|seven_seg0[0]~262_combout\) # ((!\inst42|seven_seg0[0]~189_combout\)))) # (!\inst42|seven_seg0[0]~260_combout\ & (((\inst42|seven_seg0[6]~87_combout\ & 
-- \inst42|seven_seg0[0]~189_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~262_combout\,
	datab => \inst42|seven_seg0[6]~87_combout\,
	datac => \inst42|seven_seg0[0]~260_combout\,
	datad => \inst42|seven_seg0[0]~189_combout\,
	combout => \inst42|seven_seg0[0]~263_combout\);

-- Location: LCCOMB_X8_Y25_N4
\inst42|seven_seg0[0]~264\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~264_combout\ = (\inst39|inst43~q\ & ((\inst38|inst8~q\ & (\inst42|seven_seg0[0]~263_combout\)) # (!\inst38|inst8~q\ & ((\inst42|seven_seg0[6]~101_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg0[0]~263_combout\,
	datad => \inst42|seven_seg0[6]~101_combout\,
	combout => \inst42|seven_seg0[0]~264_combout\);

-- Location: LCCOMB_X8_Y25_N30
\inst42|seven_seg0[0]~292\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[0]~292_combout\ = (\inst42|seven_seg0[0]~264_combout\) # ((!\inst39|inst43~q\ & ((\inst42|seven_seg0[3]~286_combout\) # (\inst42|seven_seg0[3]~188_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|seven_seg0[3]~286_combout\,
	datac => \inst42|seven_seg0[0]~264_combout\,
	datad => \inst42|seven_seg0[3]~188_combout\,
	combout => \inst42|seven_seg0[0]~292_combout\);

-- Location: LCCOMB_X9_Y27_N14
\inst42|seven_seg1[6]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~46_combout\ = (!\inst38|inst42~q\ & (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~46_combout\);

-- Location: LCCOMB_X9_Y27_N28
\inst42|seven_seg1[6]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~47_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & (\inst38|inst43~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~47_combout\);

-- Location: LCCOMB_X9_Y27_N26
\inst42|seven_seg1[6]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~48_combout\ = (\inst39|inst19~q\ & ((\inst38|inst41~q\ & (\inst42|seven_seg1[6]~46_combout\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg1[6]~47_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[6]~46_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[6]~47_combout\,
	combout => \inst42|seven_seg1[6]~48_combout\);

-- Location: LCCOMB_X9_Y27_N20
\inst42|seven_seg1[6]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~49_combout\ = (\inst38|inst43~q\ & (!\inst39|inst19~q\ & !\inst38|inst41~q\)) # (!\inst38|inst43~q\ & ((\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~49_combout\);

-- Location: LCCOMB_X9_Y27_N6
\inst42|seven_seg1[6]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~50_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg1[6]~49_combout\ & ((!\inst39|inst41~q\) # (!\inst39|inst19~q\)))) # (!\inst38|inst42~q\ & (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & !\inst42|seven_seg1[6]~49_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[6]~49_combout\,
	combout => \inst42|seven_seg1[6]~50_combout\);

-- Location: LCCOMB_X9_Y27_N12
\inst42|seven_seg1[6]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~51_combout\ = (\inst38|inst8~q\ & ((\inst38|inst19~q\) # ((\inst42|seven_seg1[6]~48_combout\)))) # (!\inst38|inst8~q\ & (!\inst38|inst19~q\ & ((\inst42|seven_seg1[6]~50_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[6]~48_combout\,
	datad => \inst42|seven_seg1[6]~50_combout\,
	combout => \inst42|seven_seg1[6]~51_combout\);

-- Location: LCCOMB_X7_Y27_N26
\inst42|seven_seg0[6]~265\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~265_combout\ = (!\inst39|inst41~q\ & !\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg0[6]~265_combout\);

-- Location: LCCOMB_X7_Y27_N18
\inst42|seven_seg1[6]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~54_combout\ = (\inst39|inst19~q\ & (!\inst39|inst41~q\)) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[6]~54_combout\);

-- Location: LCCOMB_X7_Y27_N28
\inst42|seven_seg1[6]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~55_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg1[6]~54_combout\ & \inst42|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[6]~54_combout\,
	datac => \inst42|Equal0~2_combout\,
	combout => \inst42|seven_seg1[6]~55_combout\);

-- Location: LCCOMB_X7_Y27_N16
\inst42|seven_seg1[6]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~56_combout\ = (\inst42|seven_seg0[0]~97_combout\ & ((\inst42|seven_seg1[6]~55_combout\) # ((!\inst39|inst19~q\ & \inst42|seven_seg0[6]~265_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~97_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg0[6]~265_combout\,
	datad => \inst42|seven_seg1[6]~55_combout\,
	combout => \inst42|seven_seg1[6]~56_combout\);

-- Location: LCCOMB_X4_Y27_N8
\inst42|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~0_combout\ = (\inst38|inst43~q\ & !\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst43~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|Equal0~0_combout\);

-- Location: LCCOMB_X6_Y27_N18
\inst42|Equal0~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~9_combout\ = (!\inst39|inst19~q\ & !\inst39|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|Equal0~9_combout\);

-- Location: LCCOMB_X9_Y27_N10
\inst42|seven_seg1[6]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~52_combout\ = (\inst38|inst43~q\) # ((\inst38|inst42~q\ & \inst39|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datac => \inst38|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~52_combout\);

-- Location: LCCOMB_X9_Y27_N0
\inst42|seven_seg1[6]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~53_combout\ = (\inst38|inst41~q\ & (((\inst42|seven_seg1[6]~52_combout\)))) # (!\inst38|inst41~q\ & (\inst42|Equal0~0_combout\ & (\inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~0_combout\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst42|seven_seg1[6]~52_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~53_combout\);

-- Location: LCCOMB_X9_Y27_N18
\inst42|seven_seg1[6]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~57_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[6]~56_combout\) # ((!\inst39|inst19~q\ & !\inst42|seven_seg1[6]~53_combout\)))) # (!\inst38|inst41~q\ & (((\inst42|seven_seg1[6]~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[6]~56_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[6]~53_combout\,
	combout => \inst42|seven_seg1[6]~57_combout\);

-- Location: LCCOMB_X9_Y27_N22
\inst42|seven_seg1[6]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~44_combout\ = (\inst39|inst41~q\ & (((\inst38|inst41~q\) # (!\inst39|inst19~q\)))) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & !\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~44_combout\);

-- Location: LCCOMB_X9_Y27_N4
\inst42|seven_seg1[6]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~43_combout\ = (\inst38|inst42~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst43~q\ & ((!\inst38|inst41~q\))) # (!\inst38|inst43~q\ & ((\inst38|inst41~q\) # (!\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~43_combout\);

-- Location: LCCOMB_X9_Y27_N2
\inst42|seven_seg1[6]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~42_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~42_combout\);

-- Location: LCCOMB_X9_Y27_N16
\inst42|seven_seg1[6]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~45_combout\ = (\inst42|seven_seg1[6]~43_combout\ & ((\inst42|seven_seg1[6]~44_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[6]~43_combout\ & (((\inst38|inst42~q\ & \inst42|seven_seg1[6]~42_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~44_combout\,
	datab => \inst42|seven_seg1[6]~43_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[6]~42_combout\,
	combout => \inst42|seven_seg1[6]~45_combout\);

-- Location: LCCOMB_X9_Y27_N24
\inst42|seven_seg1[6]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~58_combout\ = (\inst42|seven_seg1[6]~51_combout\ & ((\inst42|seven_seg1[6]~57_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[6]~51_combout\ & (((\inst38|inst19~q\ & \inst42|seven_seg1[6]~45_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~51_combout\,
	datab => \inst42|seven_seg1[6]~57_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~45_combout\,
	combout => \inst42|seven_seg1[6]~58_combout\);

-- Location: LCCOMB_X6_Y25_N18
\inst42|seven_seg1[6]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~69_combout\ = (\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst38|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[6]~69_combout\);

-- Location: LCCOMB_X6_Y25_N16
\inst42|seven_seg1[6]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~70_combout\ = (\inst38|inst42~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst19~q\ & (!\inst38|inst43~q\ & \inst42|seven_seg1[6]~69_combout\)) # (!\inst38|inst19~q\ & (\inst38|inst43~q\ & 
-- !\inst42|seven_seg1[6]~69_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[6]~69_combout\,
	combout => \inst42|seven_seg1[6]~70_combout\);

-- Location: LCCOMB_X6_Y25_N26
\inst42|seven_seg1[6]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~71_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\) # (\inst42|seven_seg1[6]~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg1[6]~70_combout\,
	combout => \inst42|seven_seg1[6]~71_combout\);

-- Location: LCCOMB_X6_Y25_N24
\inst42|seven_seg1[6]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~72_combout\ = (\inst38|inst42~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg1[6]~71_combout\))) # (!\inst38|inst19~q\ & (!\inst42|seven_seg1[6]~70_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[6]~70_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[6]~70_combout\,
	datac => \inst42|seven_seg1[6]~71_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[6]~72_combout\);

-- Location: LCCOMB_X10_Y27_N16
\inst42|seven_seg1[6]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~59_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & (\inst38|inst43~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~59_combout\);

-- Location: LCCOMB_X10_Y27_N2
\inst42|seven_seg1[6]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~248_combout\ = (!\inst39|inst19~q\ & (\inst42|Equal0~0_combout\ & (\inst38|inst19~q\ & !\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|Equal0~0_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[6]~248_combout\);

-- Location: LCCOMB_X10_Y27_N22
\inst42|seven_seg1[6]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~60_combout\ = (\inst42|seven_seg1[6]~248_combout\) # ((\inst39|inst19~q\ & (\inst42|seven_seg1[6]~59_combout\ & !\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|seven_seg1[6]~59_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~248_combout\,
	combout => \inst42|seven_seg1[6]~60_combout\);

-- Location: LCCOMB_X10_Y27_N24
\inst42|seven_seg1[6]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~65_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[6]~65_combout\);

-- Location: LCCOMB_X10_Y27_N6
\inst42|seven_seg1[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~66_combout\ = (\inst38|inst42~q\ & (((!\inst38|inst19~q\ & !\inst42|seven_seg1[6]~65_combout\)))) # (!\inst38|inst42~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg1[6]~65_combout\))) # (!\inst38|inst19~q\ & (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~65_combout\,
	combout => \inst42|seven_seg1[6]~66_combout\);

-- Location: LCCOMB_X10_Y27_N20
\inst42|seven_seg1[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~67_combout\ = (\inst39|inst19~q\ & ((\inst38|inst19~q\ & ((\inst42|seven_seg1[6]~66_combout\))) # (!\inst38|inst19~q\ & (!\inst42|Equal0~0_combout\)))) # (!\inst39|inst19~q\ & (((!\inst38|inst19~q\ & 
-- !\inst42|seven_seg1[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|Equal0~0_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~66_combout\,
	combout => \inst42|seven_seg1[6]~67_combout\);

-- Location: LCCOMB_X10_Y27_N12
\inst42|seven_seg1[6]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~61_combout\ = (\inst39|inst19~q\ & (((!\inst39|inst41~q\ & \inst38|inst42~q\)) # (!\inst38|inst43~q\))) # (!\inst39|inst19~q\ & (\inst38|inst42~q\ & ((\inst39|inst41~q\) # (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[6]~61_combout\);

-- Location: LCCOMB_X10_Y27_N30
\inst42|seven_seg1[6]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~62_combout\ = \inst38|inst43~q\ $ (((!\inst38|inst19~q\ & !\inst42|seven_seg1[6]~61_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[6]~61_combout\,
	combout => \inst42|seven_seg1[6]~62_combout\);

-- Location: LCCOMB_X10_Y27_N28
\inst42|seven_seg1[6]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~63_combout\ = (\inst39|inst41~q\ & ((\inst39|inst19~q\ & ((\inst38|inst42~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # (!\inst38|inst42~q\))))) # (!\inst39|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & 
-- !\inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[6]~63_combout\);

-- Location: LCCOMB_X10_Y27_N26
\inst42|seven_seg1[6]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~64_combout\ = (\inst42|seven_seg1[6]~62_combout\ & ((\inst42|seven_seg1[6]~63_combout\) # ((!\inst38|inst42~q\) # (!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[6]~62_combout\ & (\inst42|seven_seg1[6]~63_combout\ & (\inst38|inst19~q\ 
-- & !\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[6]~62_combout\,
	datab => \inst42|seven_seg1[6]~63_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[6]~64_combout\);

-- Location: LCCOMB_X10_Y27_N10
\inst42|seven_seg1[6]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~68_combout\ = (\inst38|inst8~q\ & (((\inst42|seven_seg1[6]~64_combout\) # (\inst38|inst41~q\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg1[6]~67_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[6]~67_combout\,
	datac => \inst42|seven_seg1[6]~64_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[6]~68_combout\);

-- Location: LCCOMB_X10_Y27_N0
\inst42|seven_seg1[6]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~73_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[6]~68_combout\ & (\inst42|seven_seg1[6]~72_combout\)) # (!\inst42|seven_seg1[6]~68_combout\ & ((\inst42|seven_seg1[6]~60_combout\))))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg1[6]~68_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[6]~72_combout\,
	datac => \inst42|seven_seg1[6]~60_combout\,
	datad => \inst42|seven_seg1[6]~68_combout\,
	combout => \inst42|seven_seg1[6]~73_combout\);

-- Location: LCCOMB_X10_Y27_N14
\inst42|seven_seg1[6]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[6]~74_combout\ = (\inst39|inst8~q\ & (\inst42|seven_seg1[6]~58_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[6]~73_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg1[6]~58_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[6]~73_combout\,
	combout => \inst42|seven_seg1[6]~74_combout\);

-- Location: LCCOMB_X2_Y27_N6
\inst42|seven_seg1[5]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~76_combout\ = (\inst39|inst19~q\ & (\inst39|inst8~q\ $ (((\inst38|inst43~q\ & !\inst38|inst8~q\))))) # (!\inst39|inst19~q\ & ((\inst38|inst43~q\) # ((\inst38|inst8~q\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011001001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~76_combout\);

-- Location: LCCOMB_X2_Y27_N4
\inst42|seven_seg1[5]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~75_combout\ = (\inst38|inst43~q\ & (((\inst39|inst8~q\)))) # (!\inst38|inst43~q\ & ((\inst39|inst19~q\ & (\inst38|inst8~q\ & !\inst39|inst8~q\)) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~75_combout\);

-- Location: LCCOMB_X2_Y27_N20
\inst42|seven_seg1[5]~262\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~262_combout\ = (\inst38|inst8~q\ & (!\inst38|inst42~q\ & ((\inst39|inst41~q\) # (\inst42|seven_seg1[5]~76_combout\)))) # (!\inst38|inst8~q\ & (\inst38|inst42~q\ & (\inst39|inst41~q\ $ (!\inst42|seven_seg1[5]~76_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~76_combout\,
	combout => \inst42|seven_seg1[5]~262_combout\);

-- Location: LCCOMB_X2_Y27_N10
\inst42|seven_seg1[5]~263\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~263_combout\ = (\inst42|seven_seg1[5]~75_combout\ & (\inst42|seven_seg1[5]~262_combout\ $ (((!\inst42|seven_seg1[5]~76_combout\ & !\inst38|inst42~q\))))) # (!\inst42|seven_seg1[5]~75_combout\ & (!\inst42|seven_seg1[5]~76_combout\ & 
-- ((\inst38|inst42~q\) # (\inst42|seven_seg1[5]~262_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010100010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~76_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~75_combout\,
	datad => \inst42|seven_seg1[5]~262_combout\,
	combout => \inst42|seven_seg1[5]~263_combout\);

-- Location: LCCOMB_X2_Y27_N12
\inst42|seven_seg1[5]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~77_combout\ = (\inst39|inst19~q\ & (((\inst38|inst8~q\) # (\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~77_combout\);

-- Location: LCCOMB_X2_Y27_N30
\inst42|seven_seg1[5]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~80_combout\ = (\inst38|inst8~q\ & (!\inst39|inst8~q\ & ((\inst39|inst19~q\) # (\inst39|inst41~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst8~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~80_combout\);

-- Location: LCCOMB_X2_Y27_N24
\inst42|seven_seg1[5]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~81_combout\ = (\inst38|inst42~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~80_combout\))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[5]~77_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~77_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[5]~80_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[5]~81_combout\);

-- Location: LCCOMB_X2_Y27_N2
\inst42|seven_seg1[5]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~78_combout\ = (\inst39|inst19~q\ & (!\inst39|inst8~q\ & (\inst39|inst41~q\ $ (\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\ $ (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~78_combout\);

-- Location: LCCOMB_X2_Y27_N0
\inst42|seven_seg1[5]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~79_combout\ = (!\inst38|inst42~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg1[5]~77_combout\)) # (!\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~78_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~77_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~78_combout\,
	combout => \inst42|seven_seg1[5]~79_combout\);

-- Location: LCCOMB_X2_Y27_N14
\inst42|seven_seg1[5]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~82_combout\ = (\inst39|inst42~q\ & (((\inst42|seven_seg1[5]~81_combout\) # (\inst42|seven_seg1[5]~79_combout\)))) # (!\inst39|inst42~q\ & (\inst42|seven_seg1[5]~263_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~263_combout\,
	datab => \inst42|seven_seg1[5]~81_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg1[5]~79_combout\,
	combout => \inst42|seven_seg1[5]~82_combout\);

-- Location: LCCOMB_X7_Y27_N2
\inst42|seven_seg1[5]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~102_combout\ = (\inst38|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[5]~102_combout\);

-- Location: LCCOMB_X6_Y27_N4
\inst42|seven_seg1[5]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~103_combout\ = (!\inst38|inst43~q\ & \inst42|seven_seg1[5]~102_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[5]~102_combout\,
	combout => \inst42|seven_seg1[5]~103_combout\);

-- Location: LCCOMB_X8_Y27_N6
\inst42|Equal0~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~11_combout\ = (\inst39|inst41~q\ & \inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|Equal0~11_combout\);

-- Location: LCCOMB_X5_Y27_N24
\inst42|seven_seg1[5]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~105_combout\ = (\inst38|inst43~q\ & (((!\inst38|inst8~q\)) # (!\inst42|Equal0~11_combout\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~41_combout\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|Equal0~11_combout\,
	datac => \inst42|seven_seg1[5]~41_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~105_combout\);

-- Location: LCCOMB_X4_Y27_N6
\inst42|seven_seg1[5]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~104_combout\ = (\inst38|inst8~q\) # ((\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[5]~104_combout\);

-- Location: LCCOMB_X4_Y27_N14
\inst42|seven_seg1[5]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~250_combout\ = (\inst38|inst43~q\ & (!\inst38|inst8~q\ & (!\inst39|inst19~q\))) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~104_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg1[5]~104_combout\,
	combout => \inst42|seven_seg1[5]~250_combout\);

-- Location: LCCOMB_X5_Y27_N18
\inst42|seven_seg1[5]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~106_combout\ = (\inst38|inst42~q\ & (\inst39|inst8~q\)) # (!\inst38|inst42~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg1[5]~250_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg1[5]~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[5]~105_combout\,
	datad => \inst42|seven_seg1[5]~250_combout\,
	combout => \inst42|seven_seg1[5]~106_combout\);

-- Location: LCCOMB_X5_Y27_N8
\inst42|seven_seg1[5]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~107_combout\ = (!\inst39|inst19~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[5]~107_combout\);

-- Location: LCCOMB_X5_Y27_N10
\inst42|seven_seg1[5]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~108_combout\ = (\inst38|inst43~q\ & (((\inst42|seven_seg1[5]~41_combout\) # (\inst38|inst8~q\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[5]~107_combout\ & ((!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[5]~107_combout\,
	datac => \inst42|seven_seg1[5]~41_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~108_combout\);

-- Location: LCCOMB_X6_Y24_N8
\inst42|Equal0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~12_combout\ = (!\inst39|inst41~q\ & (!\inst39|inst19~q\ & !\inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|Equal0~12_combout\);

-- Location: LCCOMB_X6_Y24_N30
\inst42|seven_seg1[5]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~109_combout\ = (\inst42|Equal0~12_combout\ & ((\inst42|Equal0~8_combout\) # (!\inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst43~q\,
	datac => \inst42|Equal0~12_combout\,
	datad => \inst42|Equal0~8_combout\,
	combout => \inst42|seven_seg1[5]~109_combout\);

-- Location: LCCOMB_X5_Y27_N12
\inst42|seven_seg1[5]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~110_combout\ = (\inst42|seven_seg1[5]~108_combout\ & (((!\inst38|inst8~q\) # (!\inst42|seven_seg1[5]~109_combout\)))) # (!\inst42|seven_seg1[5]~108_combout\ & (\inst42|Equal0~9_combout\ & ((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~108_combout\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst42|seven_seg1[5]~109_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~110_combout\);

-- Location: LCCOMB_X5_Y27_N14
\inst42|seven_seg1[5]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~111_combout\ = (\inst42|seven_seg1[5]~106_combout\ & (((\inst42|seven_seg1[5]~110_combout\) # (!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[5]~106_combout\ & (\inst42|seven_seg1[5]~103_combout\ & (\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~103_combout\,
	datab => \inst42|seven_seg1[5]~106_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~110_combout\,
	combout => \inst42|seven_seg1[5]~111_combout\);

-- Location: LCCOMB_X7_Y25_N0
\inst42|Equal0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~10_combout\ = (!\inst39|inst8~q\ & !\inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|Equal0~10_combout\);

-- Location: LCCOMB_X7_Y25_N18
\inst42|seven_seg1[5]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~99_combout\ = (\inst39|inst19~q\ & (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[5]~99_combout\);

-- Location: LCCOMB_X7_Y25_N14
\inst42|seven_seg1[5]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~249_combout\ = (\inst38|inst8~q\ & (((\inst38|inst43~q\ & \inst42|seven_seg1[5]~99_combout\)))) # (!\inst38|inst8~q\ & (\inst42|Equal0~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|Equal0~10_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[5]~99_combout\,
	combout => \inst42|seven_seg1[5]~249_combout\);

-- Location: LCCOMB_X5_Y23_N24
\inst42|seven_seg1[5]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~94_combout\ = (!\inst39|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~94_combout\);

-- Location: LCCOMB_X7_Y23_N28
\inst42|seven_seg1[5]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~93_combout\ = (\inst39|inst8~q\) # ((\inst39|inst41~q\ & (\inst39|inst19~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~93_combout\);

-- Location: LCCOMB_X5_Y23_N30
\inst42|seven_seg1[5]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~95_combout\ = (\inst38|inst8~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst8~q\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~93_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[5]~94_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[5]~94_combout\,
	datac => \inst42|seven_seg1[5]~93_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~95_combout\);

-- Location: LCCOMB_X5_Y23_N2
\inst42|seven_seg1[5]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~92_combout\ = (!\inst39|inst8~q\ & (!\inst39|inst41~q\ & !\inst39|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[5]~92_combout\);

-- Location: LCCOMB_X5_Y23_N20
\inst42|seven_seg1[5]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~96_combout\ = (!\inst39|inst42~q\ & (!\inst39|inst41~q\ & (!\inst39|inst19~q\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~96_combout\);

-- Location: LCCOMB_X5_Y23_N22
\inst42|seven_seg1[5]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~97_combout\ = (\inst42|seven_seg1[5]~95_combout\ & (((!\inst38|inst8~q\) # (!\inst42|seven_seg1[5]~96_combout\)))) # (!\inst42|seven_seg1[5]~95_combout\ & (\inst42|seven_seg1[5]~92_combout\ & ((\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~95_combout\,
	datab => \inst42|seven_seg1[5]~92_combout\,
	datac => \inst42|seven_seg1[5]~96_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~97_combout\);

-- Location: LCCOMB_X6_Y26_N28
\inst42|seven_seg1[5]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~98_combout\ = (!\inst38|inst43~q\ & ((!\inst42|Equal0~3_combout\) # (!\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst8~q\,
	datac => \inst42|Equal0~3_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~98_combout\);

-- Location: LCCOMB_X6_Y26_N22
\inst42|seven_seg1[5]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~100_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg1[5]~97_combout\)))) # (!\inst38|inst42~q\ & ((\inst42|seven_seg1[5]~249_combout\) # ((\inst42|seven_seg1[5]~98_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[5]~249_combout\,
	datac => \inst42|seven_seg1[5]~97_combout\,
	datad => \inst42|seven_seg1[5]~98_combout\,
	combout => \inst42|seven_seg1[5]~100_combout\);

-- Location: LCCOMB_X5_Y23_N6
\inst42|seven_seg1[5]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~88_combout\ = (\inst38|inst43~q\ & (!\inst39|inst8~q\ & ((\inst42|Equal0~9_combout\) # (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|Equal0~9_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~88_combout\);

-- Location: LCCOMB_X5_Y23_N16
\inst42|seven_seg1[5]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~89_combout\ = (\inst39|inst19~q\) # ((\inst39|inst42~q\ & ((\inst39|inst41~q\) # (\inst39|inst8~q\))) # (!\inst39|inst42~q\ & (\inst39|inst41~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[5]~89_combout\);

-- Location: LCCOMB_X5_Y23_N18
\inst42|seven_seg1[5]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~90_combout\ = (!\inst38|inst43~q\ & ((\inst42|seven_seg1[5]~89_combout\ & ((\inst39|inst8~q\) # (\inst38|inst8~q\))) # (!\inst42|seven_seg1[5]~89_combout\ & (\inst39|inst8~q\ & \inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[5]~89_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~90_combout\);

-- Location: LCCOMB_X5_Y23_N14
\inst42|seven_seg1[5]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~86_combout\ = (\inst39|inst42~q\ & (\inst39|inst41~q\ & (\inst39|inst19~q\ & !\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~86_combout\);

-- Location: LCCOMB_X5_Y23_N26
\inst42|seven_seg1[5]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~84_combout\ = (\inst39|inst41~q\ & ((\inst39|inst19~q\ $ (\inst38|inst43~q\)))) # (!\inst39|inst41~q\ & (\inst38|inst43~q\ & ((!\inst39|inst19~q\) # (!\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~84_combout\);

-- Location: LCCOMB_X5_Y23_N8
\inst42|seven_seg1[5]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~83_combout\ = (!\inst39|inst41~q\ & (!\inst39|inst19~q\ & \inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~83_combout\);

-- Location: LCCOMB_X5_Y23_N4
\inst42|seven_seg1[5]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~85_combout\ = (\inst39|inst8~q\ & (((\inst42|seven_seg1[5]~83_combout\) # (\inst38|inst8~q\)))) # (!\inst39|inst8~q\ & (!\inst42|seven_seg1[5]~84_combout\ & ((!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~84_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[5]~83_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[5]~85_combout\);

-- Location: LCCOMB_X5_Y23_N0
\inst42|seven_seg1[5]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~87_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[5]~85_combout\ & (\inst42|seven_seg1[5]~86_combout\)) # (!\inst42|seven_seg1[5]~85_combout\ & ((\inst42|seven_seg0[5]~249_combout\))))) # (!\inst38|inst8~q\ & 
-- (((\inst42|seven_seg1[5]~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[5]~86_combout\,
	datac => \inst42|seven_seg1[5]~85_combout\,
	datad => \inst42|seven_seg0[5]~249_combout\,
	combout => \inst42|seven_seg1[5]~87_combout\);

-- Location: LCCOMB_X5_Y23_N28
\inst42|seven_seg1[5]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~91_combout\ = (\inst38|inst42~q\ & ((\inst42|seven_seg1[5]~88_combout\) # ((\inst42|seven_seg1[5]~90_combout\)))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg1[5]~87_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~88_combout\,
	datab => \inst42|seven_seg1[5]~90_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~87_combout\,
	combout => \inst42|seven_seg1[5]~91_combout\);

-- Location: LCCOMB_X6_Y26_N0
\inst42|seven_seg1[5]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~101_combout\ = (\inst38|inst41~q\ & (((\inst42|seven_seg1[5]~91_combout\) # (\inst38|inst19~q\)))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[5]~100_combout\ & ((!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~100_combout\,
	datab => \inst42|seven_seg1[5]~91_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[5]~101_combout\);

-- Location: LCCOMB_X6_Y27_N2
\inst42|seven_seg1[5]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~112_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[5]~101_combout\ & ((\inst42|seven_seg1[5]~111_combout\))) # (!\inst42|seven_seg1[5]~101_combout\ & (\inst42|seven_seg1[5]~82_combout\)))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg1[5]~101_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[5]~82_combout\,
	datab => \inst42|seven_seg1[5]~111_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[5]~101_combout\,
	combout => \inst42|seven_seg1[5]~112_combout\);

-- Location: LCCOMB_X4_Y28_N16
\inst42|seven_seg1[4]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~119_combout\ = (\inst39|inst19~q\ & (!\inst38|inst8~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ & ((\inst39|inst8~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100000011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~119_combout\);

-- Location: LCCOMB_X4_Y28_N0
\inst42|seven_seg1[4]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~113_combout\ = (\inst39|inst19~q\ & (((\inst39|inst41~q\ & !\inst39|inst8~q\)) # (!\inst38|inst8~q\))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\ & !\inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110010011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~113_combout\);

-- Location: LCCOMB_X4_Y28_N30
\inst42|seven_seg1[4]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~137_combout\ = (\inst38|inst41~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & ((!\inst42|seven_seg1[4]~113_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~119_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[4]~113_combout\,
	combout => \inst42|seven_seg1[4]~137_combout\);

-- Location: LCCOMB_X6_Y27_N14
\inst42|seven_seg1[4]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~138_combout\ = (\inst39|inst8~q\ & (\inst39|inst19~q\ & \inst38|inst8~q\)) # (!\inst39|inst8~q\ & (!\inst39|inst19~q\ & !\inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~138_combout\);

-- Location: LCCOMB_X6_Y27_N24
\inst42|seven_seg1[4]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~136_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (!\inst39|inst8~q\))))) # (!\inst39|inst19~q\ & ((\inst38|inst8~q\) # ((!\inst39|inst41~q\ & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111110110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~136_combout\);

-- Location: LCCOMB_X6_Y27_N28
\inst42|seven_seg1[4]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~139_combout\ = (\inst42|seven_seg1[4]~137_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~138_combout\))) # (!\inst42|seven_seg1[4]~137_combout\ & (((\inst38|inst41~q\ & !\inst42|seven_seg1[4]~136_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101001111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~137_combout\,
	datab => \inst42|seven_seg1[4]~138_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~136_combout\,
	combout => \inst42|seven_seg1[4]~139_combout\);

-- Location: LCCOMB_X4_Y25_N12
\inst42|seven_seg1[4]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~140_combout\ = (\inst38|inst8~q\ & (\inst38|inst41~q\ $ (((!\inst39|inst19~q\ & !\inst39|inst41~q\))))) # (!\inst38|inst8~q\ & (!\inst38|inst41~q\ & ((\inst39|inst19~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001110000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[4]~140_combout\);

-- Location: LCCOMB_X6_Y25_N6
\inst42|seven_seg1[4]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~144_combout\ = (\inst38|inst41~q\ & ((\inst38|inst8~q\ & (\inst39|inst19~q\ $ (!\inst39|inst41~q\))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\) # (\inst39|inst41~q\))))) # (!\inst38|inst41~q\ & (\inst38|inst8~q\ $ 
-- (((\inst39|inst19~q\ & \inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011001101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[4]~144_combout\);

-- Location: LCCOMB_X4_Y25_N22
\inst42|seven_seg1[4]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~141_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[4]~141_combout\);

-- Location: LCCOMB_X4_Y25_N24
\inst42|seven_seg1[4]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~142_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ & (!\inst38|inst41~q\))) # (!\inst39|inst19~q\ & (!\inst38|inst8~q\ & (\inst38|inst41~q\ & !\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[4]~142_combout\);

-- Location: LCCOMB_X4_Y25_N2
\inst42|seven_seg1[4]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~143_combout\ = (\inst38|inst43~q\ & ((\inst39|inst8~q\) # ((!\inst42|seven_seg1[4]~141_combout\)))) # (!\inst38|inst43~q\ & (!\inst39|inst8~q\ & ((!\inst42|seven_seg1[4]~142_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[4]~141_combout\,
	datad => \inst42|seven_seg1[4]~142_combout\,
	combout => \inst42|seven_seg1[4]~143_combout\);

-- Location: LCCOMB_X4_Y25_N8
\inst42|seven_seg1[4]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~145_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg1[4]~143_combout\ & ((!\inst42|seven_seg1[4]~144_combout\))) # (!\inst42|seven_seg1[4]~143_combout\ & (!\inst42|seven_seg1[4]~140_combout\)))) # (!\inst39|inst8~q\ & 
-- (((\inst42|seven_seg1[4]~143_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~140_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[4]~144_combout\,
	datad => \inst42|seven_seg1[4]~143_combout\,
	combout => \inst42|seven_seg1[4]~145_combout\);

-- Location: LCCOMB_X5_Y28_N22
\inst42|seven_seg1[4]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~146_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & (\inst42|seven_seg1[4]~139_combout\)) # (!\inst38|inst42~q\ & ((\inst42|seven_seg1[4]~145_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[4]~139_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[4]~145_combout\,
	combout => \inst42|seven_seg1[4]~146_combout\);

-- Location: LCCOMB_X4_Y28_N8
\inst42|seven_seg1[4]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~115_combout\ = (\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((\inst39|inst8~q\) # (!\inst39|inst41~q\))))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst38|inst8~q\ & !\inst39|inst8~q\)) # (!\inst39|inst41~q\ & (\inst38|inst8~q\ 
-- & \inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001110010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~115_combout\);

-- Location: LCCOMB_X4_Y28_N12
\inst42|seven_seg1[4]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~133_combout\ = (\inst38|inst41~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (\inst42|seven_seg1[4]~115_combout\)) # (!\inst38|inst43~q\ & ((!\inst42|seven_seg1[4]~113_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~115_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[4]~113_combout\,
	combout => \inst42|seven_seg1[4]~133_combout\);

-- Location: LCCOMB_X4_Y28_N2
\inst42|seven_seg1[4]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~134_combout\ = (!\inst39|inst8~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~134_combout\);

-- Location: LCCOMB_X4_Y28_N20
\inst42|seven_seg1[4]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~135_combout\ = (\inst42|seven_seg1[4]~133_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~134_combout\))) # (!\inst42|seven_seg1[4]~133_combout\ & (((\inst38|inst41~q\ & \inst42|seven_seg1[4]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~133_combout\,
	datab => \inst42|seven_seg1[4]~134_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~119_combout\,
	combout => \inst42|seven_seg1[4]~135_combout\);

-- Location: LCCOMB_X6_Y27_N16
\inst42|seven_seg1[4]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~150_combout\ = (\inst39|inst19~q\ & ((\inst38|inst8~q\) # ((\inst39|inst41~q\ & !\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (!\inst39|inst41~q\ & (\inst39|inst8~q\ & !\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~150_combout\);

-- Location: LCCOMB_X7_Y27_N10
\inst42|seven_seg1[4]~257\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~257_combout\ = (\inst39|inst8~q\ & ((\inst39|inst42~q\) # ((\inst39|inst19~q\) # (\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (((!\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[4]~257_combout\);

-- Location: LCCOMB_X7_Y27_N24
\inst42|seven_seg1[4]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~251_combout\ = (\inst42|seven_seg1[4]~257_combout\ & (\inst39|inst8~q\ & (\inst42|Equal0~2_combout\ & \inst42|seven_seg1[6]~54_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~257_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~2_combout\,
	datad => \inst42|seven_seg1[6]~54_combout\,
	combout => \inst42|seven_seg1[4]~251_combout\);

-- Location: LCCOMB_X6_Y27_N10
\inst42|seven_seg1[4]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~151_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[4]~251_combout\) # ((!\inst42|seven_seg1[4]~150_combout\ & !\inst39|inst8~q\)))) # (!\inst38|inst8~q\ & (\inst42|seven_seg1[4]~150_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[4]~150_combout\,
	datac => \inst42|seven_seg1[4]~251_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~151_combout\);

-- Location: LCCOMB_X6_Y27_N8
\inst42|seven_seg1[4]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~148_combout\ = (\inst39|inst8~q\ & (((!\inst38|inst8~q\) # (!\inst39|inst19~q\)) # (!\inst39|inst41~q\))) # (!\inst39|inst8~q\ & ((\inst38|inst8~q\) # ((\inst39|inst41~q\ & \inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~148_combout\);

-- Location: LCCOMB_X6_Y27_N22
\inst42|seven_seg1[4]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~149_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~136_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~148_combout\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~148_combout\,
	datab => \inst42|seven_seg1[4]~136_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~149_combout\);

-- Location: LCCOMB_X6_Y27_N6
\inst42|seven_seg1[4]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~147_combout\ = (\inst39|inst8~q\ & ((\inst39|inst19~q\ & ((\inst38|inst8~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # (!\inst38|inst8~q\))))) # (!\inst39|inst8~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\ & 
-- \inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101100101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~147_combout\);

-- Location: LCCOMB_X6_Y27_N12
\inst42|seven_seg1[4]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~152_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[4]~149_combout\ & (\inst42|seven_seg1[4]~151_combout\)) # (!\inst42|seven_seg1[4]~149_combout\ & ((!\inst42|seven_seg1[4]~147_combout\))))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg1[4]~149_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~151_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg1[4]~149_combout\,
	datad => \inst42|seven_seg1[4]~147_combout\,
	combout => \inst42|seven_seg1[4]~152_combout\);

-- Location: LCCOMB_X5_Y28_N16
\inst42|seven_seg1[4]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~153_combout\ = (\inst42|seven_seg1[4]~146_combout\ & (((\inst42|seven_seg1[4]~152_combout\) # (!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[4]~146_combout\ & (\inst42|seven_seg1[4]~135_combout\ & ((\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~146_combout\,
	datab => \inst42|seven_seg1[4]~135_combout\,
	datac => \inst42|seven_seg1[4]~152_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[4]~153_combout\);

-- Location: LCCOMB_X4_Y28_N28
\inst42|seven_seg1[4]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~117_combout\ = (!\inst38|inst8~q\ & (!\inst39|inst8~q\ & (\inst39|inst41~q\ $ (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~117_combout\);

-- Location: LCCOMB_X4_Y28_N6
\inst42|seven_seg1[4]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~114_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\ & (!\inst38|inst8~q\ & !\inst39|inst8~q\)) # (!\inst39|inst41~q\ & (\inst38|inst8~q\ & \inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ $ (((!\inst39|inst41~q\ & 
-- \inst39|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000100111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~114_combout\);

-- Location: LCCOMB_X4_Y28_N18
\inst42|seven_seg1[4]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~116_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~114_combout\)))) # (!\inst38|inst43~q\ & (!\inst42|seven_seg1[4]~115_combout\ & (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000110101011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[4]~115_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~114_combout\,
	combout => \inst42|seven_seg1[4]~116_combout\);

-- Location: LCCOMB_X4_Y28_N10
\inst42|seven_seg1[4]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~118_combout\ = (\inst42|seven_seg1[4]~116_combout\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg1[4]~117_combout\))) # (!\inst42|seven_seg1[4]~116_combout\ & (((\inst38|inst41~q\ & \inst42|seven_seg1[4]~113_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110001001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~117_combout\,
	datab => \inst42|seven_seg1[4]~116_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~113_combout\,
	combout => \inst42|seven_seg1[4]~118_combout\);

-- Location: LCCOMB_X4_Y28_N22
\inst42|seven_seg1[4]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~120_combout\ = (\inst38|inst41~q\ & (((\inst38|inst43~q\)))) # (!\inst38|inst41~q\ & ((\inst38|inst43~q\ & (!\inst42|seven_seg1[4]~115_combout\)) # (!\inst38|inst43~q\ & ((\inst42|seven_seg1[4]~113_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~115_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[4]~113_combout\,
	combout => \inst42|seven_seg1[4]~120_combout\);

-- Location: LCCOMB_X4_Y28_N24
\inst42|seven_seg1[4]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~121_combout\ = (\inst39|inst19~q\ & (((!\inst39|inst8~q\) # (!\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst38|inst8~q\) # (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~121_combout\);

-- Location: LCCOMB_X4_Y28_N14
\inst42|seven_seg1[4]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~122_combout\ = (\inst42|seven_seg1[4]~120_combout\ & (((\inst42|seven_seg1[4]~121_combout\) # (!\inst38|inst41~q\)))) # (!\inst42|seven_seg1[4]~120_combout\ & (!\inst42|seven_seg1[4]~119_combout\ & (\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~120_combout\,
	datab => \inst42|seven_seg1[4]~119_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~121_combout\,
	combout => \inst42|seven_seg1[4]~122_combout\);

-- Location: LCCOMB_X5_Y28_N8
\inst42|seven_seg1[4]~260\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~260_combout\ = (\inst38|inst8~q\ & ((\inst38|inst41~q\ $ (\inst38|inst43~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (((\inst39|inst19~q\) # (!\inst38|inst43~q\)) # (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst41~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[4]~260_combout\);

-- Location: LCCOMB_X5_Y28_N2
\inst42|seven_seg1[4]~261\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~261_combout\ = (\inst39|inst8~q\ & (\inst38|inst43~q\ & ((\inst38|inst41~q\) # (\inst42|seven_seg1[4]~260_combout\)))) # (!\inst39|inst8~q\ & (((\inst42|seven_seg1[4]~260_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[4]~260_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[4]~261_combout\);

-- Location: LCCOMB_X5_Y28_N14
\inst42|seven_seg1[4]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~123_combout\ = (\inst38|inst8~q\ & (\inst38|inst41~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\)))) # (!\inst38|inst8~q\ & (!\inst38|inst41~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~123_combout\);

-- Location: LCCOMB_X5_Y28_N24
\inst42|seven_seg1[4]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~124_combout\ = (\inst39|inst41~q\ & (\inst38|inst8~q\ $ (\inst39|inst19~q\ $ (\inst38|inst41~q\)))) # (!\inst39|inst41~q\ & ((\inst38|inst8~q\ & (!\inst39|inst19~q\ & !\inst38|inst41~q\)) # (!\inst38|inst8~q\ & (\inst39|inst19~q\ & 
-- \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[4]~124_combout\);

-- Location: LCCOMB_X5_Y28_N6
\inst42|seven_seg1[4]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~125_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg1[4]~261_combout\ & ((!\inst42|seven_seg1[4]~124_combout\))) # (!\inst42|seven_seg1[4]~261_combout\ & (!\inst42|seven_seg1[4]~123_combout\)))) # (!\inst39|inst8~q\ & 
-- (\inst42|seven_seg1[4]~261_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[4]~261_combout\,
	datac => \inst42|seven_seg1[4]~123_combout\,
	datad => \inst42|seven_seg1[4]~124_combout\,
	combout => \inst42|seven_seg1[4]~125_combout\);

-- Location: LCCOMB_X5_Y28_N20
\inst42|seven_seg1[4]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~126_combout\ = (\inst38|inst19~q\ & (((\inst38|inst42~q\)))) # (!\inst38|inst19~q\ & ((\inst38|inst42~q\ & (\inst42|seven_seg1[4]~122_combout\)) # (!\inst38|inst42~q\ & ((\inst42|seven_seg1[4]~125_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[4]~122_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[4]~125_combout\,
	combout => \inst42|seven_seg1[4]~126_combout\);

-- Location: LCCOMB_X4_Y28_N4
\inst42|seven_seg1[4]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~127_combout\ = (\inst39|inst19~q\ & (((!\inst39|inst8~q\) # (!\inst38|inst8~q\)) # (!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & (((\inst38|inst8~q\) # (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[4]~127_combout\);

-- Location: LCCOMB_X4_Y28_N26
\inst42|seven_seg1[4]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~128_combout\ = (\inst38|inst43~q\ & (((\inst38|inst41~q\) # (!\inst42|seven_seg1[4]~119_combout\)))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[4]~127_combout\ & (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~127_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[4]~119_combout\,
	combout => \inst42|seven_seg1[4]~128_combout\);

-- Location: LCCOMB_X5_Y27_N4
\inst42|seven_seg1[4]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~129_combout\ = (\inst39|inst8~q\ & (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (!\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (\inst39|inst19~q\ $ (((\inst38|inst8~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010110011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~129_combout\);

-- Location: LCCOMB_X5_Y27_N2
\inst42|seven_seg1[4]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~130_combout\ = (\inst42|seven_seg1[4]~129_combout\) # ((\inst42|seven_seg1[4]~251_combout\ & \inst38|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~251_combout\,
	datac => \inst42|seven_seg1[4]~129_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[4]~130_combout\);

-- Location: LCCOMB_X5_Y28_N26
\inst42|seven_seg1[4]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~131_combout\ = (\inst38|inst41~q\ & ((\inst42|seven_seg1[4]~128_combout\ & ((\inst42|seven_seg1[4]~130_combout\))) # (!\inst42|seven_seg1[4]~128_combout\ & (!\inst42|seven_seg1[4]~114_combout\)))) # (!\inst38|inst41~q\ & 
-- (((\inst42|seven_seg1[4]~128_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001001010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[4]~114_combout\,
	datac => \inst42|seven_seg1[4]~128_combout\,
	datad => \inst42|seven_seg1[4]~130_combout\,
	combout => \inst42|seven_seg1[4]~131_combout\);

-- Location: LCCOMB_X5_Y28_N0
\inst42|seven_seg1[4]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~132_combout\ = (\inst42|seven_seg1[4]~126_combout\ & (((\inst42|seven_seg1[4]~131_combout\) # (!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[4]~126_combout\ & (\inst42|seven_seg1[4]~118_combout\ & ((\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~118_combout\,
	datab => \inst42|seven_seg1[4]~126_combout\,
	datac => \inst42|seven_seg1[4]~131_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[4]~132_combout\);

-- Location: LCCOMB_X5_Y28_N30
\inst42|seven_seg1[4]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[4]~154_combout\ = (\inst39|inst42~q\ & ((\inst42|seven_seg1[4]~132_combout\))) # (!\inst39|inst42~q\ & (\inst42|seven_seg1[4]~153_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst42|seven_seg1[4]~153_combout\,
	datad => \inst42|seven_seg1[4]~132_combout\,
	combout => \inst42|seven_seg1[4]~154_combout\);

-- Location: LCCOMB_X7_Y27_N4
\inst42|seven_seg0[6]~269\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~269_combout\ = (\inst38|inst8~q\ & (!\inst39|inst19~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\)))) # (!\inst38|inst8~q\ & (((\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[6]~269_combout\);

-- Location: LCCOMB_X7_Y27_N22
\inst42|seven_seg0[6]~267\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~267_combout\ = (\inst38|inst8~q\ & (((!\inst39|inst19~q\)))) # (!\inst38|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[6]~267_combout\);

-- Location: LCCOMB_X7_Y27_N20
\inst42|seven_seg0[6]~268\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~268_combout\ = (\inst39|inst42~q\ & (\inst38|inst8~q\ & (\inst39|inst19~q\ & \inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst38|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[6]~268_combout\);

-- Location: LCCOMB_X7_Y27_N6
\inst42|seven_seg1[3]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~160_combout\ = (\inst38|inst43~q\ & (\inst39|inst8~q\)) # (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg0[6]~267_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~268_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~267_combout\,
	datad => \inst42|seven_seg0[6]~268_combout\,
	combout => \inst42|seven_seg1[3]~160_combout\);

-- Location: LCCOMB_X7_Y27_N0
\inst42|seven_seg0[6]~266\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~266_combout\ = (\inst39|inst41~q\ & ((!\inst39|inst19~q\) # (!\inst38|inst8~q\))) # (!\inst39|inst41~q\ & ((\inst38|inst8~q\) # (\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst39|inst41~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~266_combout\);

-- Location: LCCOMB_X7_Y27_N30
\inst42|seven_seg1[3]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~161_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~160_combout\ & (\inst42|seven_seg0[6]~269_combout\)) # (!\inst42|seven_seg1[3]~160_combout\ & ((!\inst42|seven_seg0[6]~266_combout\))))) # (!\inst38|inst43~q\ & 
-- (((\inst42|seven_seg1[3]~160_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[6]~269_combout\,
	datac => \inst42|seven_seg1[3]~160_combout\,
	datad => \inst42|seven_seg0[6]~266_combout\,
	combout => \inst42|seven_seg1[3]~161_combout\);

-- Location: LCCOMB_X4_Y27_N0
\inst42|seven_seg1[3]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~166_combout\ = (\inst38|inst8~q\ & ((\inst39|inst19~q\ & ((!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~166_combout\);

-- Location: LCCOMB_X4_Y27_N2
\inst42|seven_seg1[3]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~165_combout\ = (!\inst38|inst8~q\ & ((\inst39|inst8~q\ & (!\inst39|inst19~q\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[5]~41_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[5]~41_combout\,
	combout => \inst42|seven_seg1[3]~165_combout\);

-- Location: LCCOMB_X4_Y27_N26
\inst42|seven_seg1[3]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~167_combout\ = (\inst42|seven_seg1[3]~165_combout\) # ((\inst42|seven_seg1[3]~166_combout\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst42|seven_seg1[3]~166_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~165_combout\,
	combout => \inst42|seven_seg1[3]~167_combout\);

-- Location: LCCOMB_X4_Y27_N30
\inst42|seven_seg1[3]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~163_combout\ = (\inst39|inst8~q\ & (((!\inst39|inst19~q\ & !\inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~163_combout\);

-- Location: LCCOMB_X4_Y27_N10
\inst42|Equal0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~16_combout\ = (\inst39|inst42~q\ & \inst39|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|Equal0~16_combout\);

-- Location: LCCOMB_X4_Y27_N20
\inst42|seven_seg1[3]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~162_combout\ = (\inst39|inst8~q\ & (!\inst38|inst8~q\ & ((\inst42|Equal0~16_combout\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~16_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~162_combout\);

-- Location: LCCOMB_X4_Y27_N28
\inst42|seven_seg1[3]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~164_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~162_combout\) # ((\inst38|inst8~q\ & \inst42|seven_seg1[3]~163_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~163_combout\,
	datad => \inst42|seven_seg1[3]~162_combout\,
	combout => \inst42|seven_seg1[3]~164_combout\);

-- Location: LCCOMB_X4_Y27_N24
\inst42|seven_seg1[3]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~168_combout\ = (\inst42|seven_seg1[3]~164_combout\) # ((\inst42|seven_seg1[3]~167_combout\ & !\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~167_combout\,
	datab => \inst42|seven_seg1[3]~164_combout\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~168_combout\);

-- Location: LCCOMB_X4_Y26_N16
\inst42|seven_seg1[3]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~169_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst41~q\ & (\inst42|seven_seg1[3]~161_combout\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg1[3]~168_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[3]~161_combout\,
	datac => \inst42|seven_seg1[3]~168_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[3]~169_combout\);

-- Location: LCCOMB_X5_Y27_N0
\inst42|seven_seg1[3]~27\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~27_combout\ = (((\inst39|inst8~q\) # (\inst39|inst19~q\)) # (!\inst38|inst19~q\)) # (!\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst19~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[3]~27_combout\);

-- Location: LCCOMB_X5_Y27_N26
\inst42|seven_seg1[3]~259\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~259_combout\ = (\inst38|inst43~q\ & (\inst38|inst8~q\ & ((\inst42|seven_seg1[3]~27_combout\) # (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[3]~27_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~259_combout\);

-- Location: LCCOMB_X5_Y27_N20
\inst42|seven_seg1[3]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~170_combout\ = (\inst38|inst43~q\ & (\inst39|inst8~q\ & (\inst42|seven_seg1[5]~41_combout\ & !\inst38|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[5]~41_combout\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~170_combout\);

-- Location: LCCOMB_X4_Y27_N22
\inst42|seven_seg1[3]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~171_combout\ = (\inst38|inst8~q\ & (!\inst39|inst19~q\ & ((!\inst39|inst41~q\)))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~171_combout\);

-- Location: LCCOMB_X4_Y27_N4
\inst42|seven_seg0[6]~270\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~270_combout\ = (\inst38|inst8~q\ & (\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\)))) # (!\inst38|inst8~q\ & (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & !\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg0[6]~270_combout\);

-- Location: LCCOMB_X4_Y27_N18
\inst42|seven_seg1[3]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~172_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (\inst42|seven_seg1[3]~171_combout\)) # (!\inst39|inst8~q\ & ((\inst42|seven_seg0[6]~270_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~171_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg0[6]~270_combout\,
	combout => \inst42|seven_seg1[3]~172_combout\);

-- Location: LCCOMB_X5_Y27_N30
\inst42|seven_seg1[3]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~173_combout\ = (\inst42|seven_seg1[3]~170_combout\) # ((\inst42|seven_seg1[3]~172_combout\) # ((\inst42|seven_seg1[3]~259_combout\ & \inst42|seven_seg1[4]~251_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~259_combout\,
	datab => \inst42|seven_seg1[3]~170_combout\,
	datac => \inst42|seven_seg1[4]~251_combout\,
	datad => \inst42|seven_seg1[3]~172_combout\,
	combout => \inst42|seven_seg1[3]~173_combout\);

-- Location: LCCOMB_X8_Y26_N26
\inst42|seven_seg1[3]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~158_combout\ = \inst38|inst8~q\ $ (\inst39|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[3]~158_combout\);

-- Location: LCCOMB_X8_Y26_N0
\inst42|seven_seg1[3]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~157_combout\ = (\inst39|inst8~q\ & ((\inst39|inst19~q\ & ((!\inst39|inst41~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))) # (!\inst39|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst42~q\ & 
-- \inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~157_combout\);

-- Location: LCCOMB_X7_Y23_N16
\inst42|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~1_combout\ = (\inst39|inst19~q\ & \inst38|inst8~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|Equal0~1_combout\);

-- Location: LCCOMB_X4_Y25_N6
\inst42|seven_seg1[3]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~155_combout\ = (\inst39|inst42~q\ & (!\inst38|inst8~q\ & (\inst39|inst19~q\ $ (\inst39|inst41~q\)))) # (!\inst39|inst42~q\ & (\inst38|inst8~q\ $ (((\inst39|inst19~q\) # (\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[3]~155_combout\);

-- Location: LCCOMB_X8_Y26_N2
\inst42|seven_seg1[3]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~156_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~155_combout\))) # (!\inst39|inst8~q\ & (\inst42|Equal0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|Equal0~1_combout\,
	datad => \inst42|seven_seg1[3]~155_combout\,
	combout => \inst42|seven_seg1[3]~156_combout\);

-- Location: LCCOMB_X8_Y26_N16
\inst42|seven_seg1[3]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~159_combout\ = (\inst42|seven_seg1[3]~156_combout\) # ((\inst42|seven_seg1[3]~158_combout\ & (\inst42|seven_seg1[3]~157_combout\ & \inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~158_combout\,
	datab => \inst42|seven_seg1[3]~157_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[3]~156_combout\,
	combout => \inst42|seven_seg1[3]~159_combout\);

-- Location: LCCOMB_X8_Y26_N22
\inst42|seven_seg1[3]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~174_combout\ = (\inst42|seven_seg1[3]~169_combout\ & ((\inst42|seven_seg1[3]~173_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[3]~169_combout\ & (((\inst38|inst42~q\ & \inst42|seven_seg1[3]~159_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~169_combout\,
	datab => \inst42|seven_seg1[3]~173_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[3]~159_combout\,
	combout => \inst42|seven_seg1[3]~174_combout\);

-- Location: LCCOMB_X7_Y27_N12
\inst42|seven_seg1[3]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~179_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & (!\inst42|seven_seg0[6]~266_combout\)) # (!\inst39|inst8~q\ & ((!\inst42|Equal0~1_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg0[6]~266_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|Equal0~1_combout\,
	combout => \inst42|seven_seg1[3]~179_combout\);

-- Location: LCCOMB_X7_Y27_N14
\inst42|seven_seg1[3]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~180_combout\ = (!\inst42|seven_seg1[3]~179_combout\ & (((\inst39|inst8~q\) # (!\inst42|seven_seg0[6]~269_combout\)) # (!\inst38|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg0[6]~269_combout\,
	datad => \inst42|seven_seg1[3]~179_combout\,
	combout => \inst42|seven_seg1[3]~180_combout\);

-- Location: LCCOMB_X3_Y27_N24
\inst42|seven_seg1[3]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~253_combout\ = ((\inst39|inst8~q\ & (\inst38|inst43~q\ & \inst42|seven_seg0[6]~270_combout\))) # (!\inst42|seven_seg1[3]~180_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~180_combout\,
	datad => \inst42|seven_seg0[6]~270_combout\,
	combout => \inst42|seven_seg1[3]~253_combout\);

-- Location: LCCOMB_X3_Y27_N6
\inst42|seven_seg0[6]~271\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg0[6]~271_combout\ = (\inst38|inst8~q\ & (((!\inst39|inst41~q\ & !\inst39|inst42~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (\inst39|inst41~q\ & ((\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg0[6]~271_combout\);

-- Location: LCCOMB_X3_Y27_N20
\inst42|seven_seg1[3]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~176_combout\ = (\inst38|inst8~q\ & (((!\inst39|inst19~q\) # (!\inst39|inst42~q\)) # (!\inst39|inst41~q\))) # (!\inst38|inst8~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[3]~176_combout\);

-- Location: LCCOMB_X3_Y27_N2
\inst42|seven_seg1[3]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~177_combout\ = (\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~176_combout\) # ((\inst38|inst43~q\ & \inst39|inst41~q\)))) # (!\inst39|inst8~q\ & (((\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[3]~176_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~177_combout\);

-- Location: LCCOMB_X3_Y27_N0
\inst42|seven_seg1[3]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~178_combout\ = (\inst39|inst8~q\ & (((!\inst42|seven_seg1[3]~177_combout\)))) # (!\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~177_combout\ & ((\inst42|seven_seg0[6]~267_combout\))) # (!\inst42|seven_seg1[3]~177_combout\ & 
-- (\inst42|seven_seg0[6]~271_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~271_combout\,
	datab => \inst42|seven_seg0[6]~267_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~177_combout\,
	combout => \inst42|seven_seg1[3]~178_combout\);

-- Location: LCCOMB_X3_Y27_N26
\inst42|seven_seg1[3]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~181_combout\ = (\inst38|inst41~q\ & (((\inst38|inst42~q\) # (\inst42|seven_seg1[3]~178_combout\)))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[3]~253_combout\ & (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg1[3]~253_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[3]~178_combout\,
	combout => \inst42|seven_seg1[3]~181_combout\);

-- Location: LCCOMB_X8_Y26_N24
\inst42|seven_seg1[3]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~182_combout\ = (\inst39|inst8~q\ & (((!\inst39|inst42~q\ & !\inst39|inst41~q\)) # (!\inst39|inst19~q\))) # (!\inst39|inst8~q\ & ((\inst39|inst19~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\))) # (!\inst39|inst19~q\ & 
-- ((\inst39|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[3]~182_combout\);

-- Location: LCCOMB_X8_Y26_N18
\inst42|seven_seg1[3]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~183_combout\ = (\inst42|seven_seg1[3]~158_combout\ & ((\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~182_combout\))) # (!\inst38|inst43~q\ & (\inst42|seven_seg1[3]~157_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~158_combout\,
	datab => \inst42|seven_seg1[3]~157_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[3]~182_combout\,
	combout => \inst42|seven_seg1[3]~183_combout\);

-- Location: LCCOMB_X4_Y27_N16
\inst42|seven_seg1[3]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~252_combout\ = (\inst38|inst43~q\ & (\inst42|seven_seg1[3]~167_combout\)) # (!\inst38|inst43~q\ & (((\inst42|seven_seg1[3]~171_combout\ & !\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~167_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[3]~171_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[3]~252_combout\);

-- Location: LCCOMB_X8_Y26_N20
\inst42|seven_seg1[3]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~184_combout\ = (\inst42|seven_seg1[3]~181_combout\ & ((\inst42|seven_seg1[3]~183_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[3]~181_combout\ & (((\inst38|inst42~q\ & \inst42|seven_seg1[3]~252_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~181_combout\,
	datab => \inst42|seven_seg1[3]~183_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[3]~252_combout\,
	combout => \inst42|seven_seg1[3]~184_combout\);

-- Location: LCCOMB_X8_Y26_N6
\inst42|seven_seg1[3]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~185_combout\ = (\inst38|inst19~q\ & (\inst42|seven_seg1[3]~174_combout\)) # (!\inst38|inst19~q\ & ((\inst42|seven_seg1[3]~184_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[3]~174_combout\,
	datad => \inst42|seven_seg1[3]~184_combout\,
	combout => \inst42|seven_seg1[3]~185_combout\);

-- Location: LCCOMB_X8_Y27_N22
\inst42|seven_seg1[2]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~194_combout\ = (\inst39|inst19~q\ & (((!\inst38|inst8~q\)))) # (!\inst39|inst19~q\ & (\inst38|inst8~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[2]~194_combout\);

-- Location: LCCOMB_X8_Y27_N16
\inst42|seven_seg1[2]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~195_combout\ = (\inst39|inst8~q\ & (((\inst42|seven_seg1[2]~194_combout\)))) # (!\inst39|inst8~q\ & (\inst38|inst8~q\ & ((\inst42|Equal0~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[2]~194_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|Equal0~11_combout\,
	combout => \inst42|seven_seg1[2]~195_combout\);

-- Location: LCCOMB_X8_Y27_N10
\inst42|seven_seg1[2]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~196_combout\ = (\inst39|inst19~q\ & (((!\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (!\inst39|inst42~q\ & (\inst39|inst8~q\ & !\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[2]~196_combout\);

-- Location: LCCOMB_X8_Y27_N0
\inst42|seven_seg1[2]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~197_combout\ = (\inst39|inst19~q\ & ((\inst39|inst42~q\ & ((\inst39|inst41~q\) # (!\inst39|inst8~q\))) # (!\inst39|inst42~q\ & (!\inst39|inst8~q\ & \inst39|inst41~q\)))) # (!\inst39|inst19~q\ & (((\inst39|inst8~q\ & 
-- !\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[2]~197_combout\);

-- Location: LCCOMB_X8_Y27_N2
\inst42|seven_seg1[2]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~198_combout\ = (\inst38|inst8~q\ & (\inst42|seven_seg1[2]~196_combout\)) # (!\inst38|inst8~q\ & ((!\inst42|seven_seg1[2]~197_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~196_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg1[2]~197_combout\,
	combout => \inst42|seven_seg1[2]~198_combout\);

-- Location: LCCOMB_X8_Y27_N8
\inst42|seven_seg1[2]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~199_combout\ = (\inst38|inst42~q\ & (\inst42|seven_seg1[2]~195_combout\ & ((\inst38|inst43~q\)))) # (!\inst38|inst42~q\ & (((\inst42|seven_seg1[2]~198_combout\ & !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[2]~195_combout\,
	datac => \inst42|seven_seg1[2]~198_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[2]~199_combout\);

-- Location: LCCOMB_X8_Y27_N28
\inst42|seven_seg1[2]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~190_combout\ = (\inst39|inst19~q\) # ((\inst38|inst8~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[2]~190_combout\);

-- Location: LCCOMB_X5_Y28_N4
\inst42|seven_seg1[2]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~191_combout\ = (\inst38|inst8~q\ & (((!\inst39|inst41~q\ & !\inst39|inst42~q\)) # (!\inst39|inst19~q\))) # (!\inst38|inst8~q\ & (\inst39|inst41~q\ $ (((!\inst39|inst19~q\ & \inst39|inst42~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001101111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[2]~191_combout\);

-- Location: LCCOMB_X8_Y27_N30
\inst42|seven_seg1[2]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~192_combout\ = (\inst38|inst42~q\ & (!\inst38|inst43~q\ & ((\inst42|seven_seg1[2]~191_combout\) # (!\inst42|seven_seg1[2]~190_combout\)))) # (!\inst38|inst42~q\ & (!\inst42|seven_seg1[2]~190_combout\ & 
-- ((\inst42|seven_seg1[2]~191_combout\) # (\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000110110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[2]~190_combout\,
	datac => \inst42|seven_seg1[2]~191_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[2]~192_combout\);

-- Location: LCCOMB_X8_Y27_N20
\inst42|seven_seg1[2]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~193_combout\ = (\inst42|seven_seg1[2]~192_combout\ & (\inst42|seven_seg1[2]~190_combout\ & \inst39|inst8~q\)) # (!\inst42|seven_seg1[2]~192_combout\ & (!\inst42|seven_seg1[2]~190_combout\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000110000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~192_combout\,
	datab => \inst42|seven_seg1[2]~190_combout\,
	datac => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[2]~193_combout\);

-- Location: LCCOMB_X8_Y27_N14
\inst42|seven_seg1[2]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~200_combout\ = (\inst38|inst41~q\ & ((\inst38|inst19~q\) # ((\inst42|seven_seg1[2]~193_combout\)))) # (!\inst38|inst41~q\ & (!\inst38|inst19~q\ & (\inst42|seven_seg1[2]~199_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[2]~199_combout\,
	datad => \inst42|seven_seg1[2]~193_combout\,
	combout => \inst42|seven_seg1[2]~200_combout\);

-- Location: LCCOMB_X8_Y27_N24
\inst42|seven_seg1[2]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~201_combout\ = (\inst39|inst42~q\ & (!\inst38|inst8~q\ & (\inst39|inst41~q\ $ (\inst39|inst19~q\)))) # (!\inst39|inst42~q\ & (\inst38|inst8~q\ $ (((\inst39|inst41~q\) # (\inst39|inst19~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst39|inst19~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg1[2]~201_combout\);

-- Location: LCCOMB_X8_Y27_N18
\inst42|seven_seg1[2]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~254_combout\ = (\inst38|inst43~q\ & (!\inst38|inst42~q\ & (!\inst39|inst8~q\ & \inst42|seven_seg1[2]~201_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[2]~201_combout\,
	combout => \inst42|seven_seg1[2]~254_combout\);

-- Location: LCCOMB_X5_Y28_N10
\inst42|seven_seg1[2]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~255_combout\ = (\inst39|inst43~q\ & (((!\inst38|inst19~q\) # (!\inst39|inst42~q\)) # (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[2]~255_combout\);

-- Location: LCCOMB_X5_Y28_N18
\inst42|seven_seg1[2]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~202_combout\ = (\inst42|seven_seg0[0]~97_combout\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\ & \inst42|seven_seg1[2]~255_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~97_combout\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg1[2]~255_combout\,
	combout => \inst42|seven_seg1[2]~202_combout\);

-- Location: LCCOMB_X5_Y28_N28
\inst42|seven_seg1[2]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~203_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg1[2]~202_combout\) # ((!\inst38|inst42~q\ & !\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst42|seven_seg1[2]~202_combout\,
	datac => \inst38|inst42~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[2]~203_combout\);

-- Location: LCCOMB_X8_Y27_N26
\inst42|seven_seg1[2]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~204_combout\ = (\inst42|seven_seg1[2]~254_combout\) # ((\inst39|inst19~q\ & (\inst39|inst8~q\ & \inst42|seven_seg1[2]~203_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|seven_seg1[2]~254_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[2]~203_combout\,
	combout => \inst42|seven_seg1[2]~204_combout\);

-- Location: LCCOMB_X2_Y27_N16
\inst42|seven_seg1[2]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~186_combout\ = (\inst39|inst19~q\ & ((!\inst39|inst41~q\) # (!\inst39|inst42~q\))) # (!\inst39|inst19~q\ & ((\inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[2]~186_combout\);

-- Location: LCCOMB_X2_Y27_N22
\inst42|seven_seg1[2]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~187_combout\ = (\inst42|Equal0~0_combout\ & (\inst42|seven_seg1[2]~186_combout\ & (\inst38|inst8~q\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~0_combout\,
	datab => \inst42|seven_seg1[2]~186_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[2]~187_combout\);

-- Location: LCCOMB_X3_Y27_N4
\inst42|seven_seg1[5]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[5]~175_combout\ = (!\inst39|inst8~q\ & !\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[5]~175_combout\);

-- Location: LCCOMB_X2_Y27_N28
\inst42|seven_seg1[2]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~188_combout\ = (!\inst38|inst8~q\ & (((!\inst39|inst42~q\ & !\inst39|inst41~q\)) # (!\inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst8~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[2]~188_combout\);

-- Location: LCCOMB_X2_Y27_N18
\inst42|seven_seg1[2]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~189_combout\ = (\inst42|seven_seg1[2]~187_combout\) # ((\inst38|inst42~q\ & (\inst42|seven_seg1[5]~175_combout\ & \inst42|seven_seg1[2]~188_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~187_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[5]~175_combout\,
	datad => \inst42|seven_seg1[2]~188_combout\,
	combout => \inst42|seven_seg1[2]~189_combout\);

-- Location: LCCOMB_X8_Y27_N4
\inst42|seven_seg1[2]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[2]~205_combout\ = (\inst42|seven_seg1[2]~200_combout\ & ((\inst42|seven_seg1[2]~204_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[2]~200_combout\ & (((\inst42|seven_seg1[2]~189_combout\ & \inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[2]~200_combout\,
	datab => \inst42|seven_seg1[2]~204_combout\,
	datac => \inst42|seven_seg1[2]~189_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg1[2]~205_combout\);

-- Location: LCCOMB_X7_Y25_N26
\inst42|seven_seg1[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~222_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst38|inst42~q\))) # (!\inst39|inst41~q\ & (!\inst38|inst42~q\ & \inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[1]~222_combout\);

-- Location: LCCOMB_X7_Y25_N28
\inst42|seven_seg1[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~223_combout\ = (\inst39|inst8~q\) # ((\inst38|inst42~q\ & ((\inst42|seven_seg1[1]~222_combout\) # (\inst39|inst19~q\))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[1]~222_combout\ & \inst39|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[1]~222_combout\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[1]~223_combout\);

-- Location: LCCOMB_X7_Y25_N22
\inst42|seven_seg1[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~224_combout\ = (\inst38|inst42~q\ & ((\inst38|inst41~q\ & ((!\inst42|seven_seg1[5]~99_combout\))) # (!\inst38|inst41~q\ & (\inst42|seven_seg1[1]~223_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[1]~223_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[1]~223_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[5]~99_combout\,
	combout => \inst42|seven_seg1[1]~224_combout\);

-- Location: LCCOMB_X7_Y25_N2
\inst42|seven_seg1[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~220_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst38|inst41~q\))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst38|inst41~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~220_combout\);

-- Location: LCCOMB_X7_Y25_N24
\inst42|seven_seg1[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~221_combout\ = (\inst42|seven_seg0[5]~137_combout\ & (\inst39|inst8~q\ & ((\inst39|inst19~q\) # (\inst42|seven_seg1[1]~220_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|seven_seg0[5]~137_combout\,
	datac => \inst42|seven_seg1[1]~220_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~221_combout\);

-- Location: LCCOMB_X7_Y25_N20
\inst42|seven_seg1[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~225_combout\ = (\inst42|seven_seg1[1]~224_combout\ & (((\inst42|seven_seg1[1]~221_combout\ & !\inst38|inst42~q\)))) # (!\inst42|seven_seg1[1]~224_combout\ & (((\inst42|seven_seg1[1]~221_combout\ & !\inst38|inst42~q\)) # 
-- (!\inst42|seven_seg0[5]~137_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~224_combout\,
	datab => \inst42|seven_seg0[5]~137_combout\,
	datac => \inst42|seven_seg1[1]~221_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~225_combout\);

-- Location: LCCOMB_X10_Y27_N4
\inst42|seven_seg1[1]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~214_combout\ = (\inst38|inst43~q\ & (((!\inst38|inst41~q\)))) # (!\inst38|inst43~q\ & (\inst38|inst41~q\ & ((\inst39|inst42~q\) # (\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~214_combout\);

-- Location: LCCOMB_X10_Y27_N18
\inst42|seven_seg1[1]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~215_combout\ = (\inst39|inst42~q\ & (\inst39|inst41~q\ & (\inst38|inst43~q\ & !\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst41~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~215_combout\);

-- Location: LCCOMB_X10_Y27_N8
\inst42|seven_seg1[1]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~216_combout\ = (\inst39|inst19~q\ & ((\inst42|seven_seg1[1]~214_combout\) # ((\inst39|inst8~q\)))) # (!\inst39|inst19~q\ & (((\inst42|seven_seg1[1]~215_combout\ & !\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst42|seven_seg1[1]~214_combout\,
	datac => \inst42|seven_seg1[1]~215_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~216_combout\);

-- Location: LCCOMB_X7_Y25_N8
\inst42|seven_seg1[1]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~217_combout\ = (\inst38|inst43~q\ & (!\inst39|inst41~q\ & (!\inst39|inst42~q\ & !\inst38|inst41~q\))) # (!\inst38|inst43~q\ & (\inst38|inst41~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~217_combout\);

-- Location: LCCOMB_X7_Y25_N30
\inst42|seven_seg1[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~218_combout\ = (\inst42|seven_seg1[1]~216_combout\ & (((\inst42|seven_seg1[1]~217_combout\)) # (!\inst39|inst8~q\))) # (!\inst42|seven_seg1[1]~216_combout\ & (\inst39|inst8~q\ & ((\inst42|seven_seg0[5]~137_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~216_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst42|seven_seg1[1]~217_combout\,
	datad => \inst42|seven_seg0[5]~137_combout\,
	combout => \inst42|seven_seg1[1]~218_combout\);

-- Location: LCCOMB_X7_Y25_N6
\inst42|seven_seg1[1]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~213_combout\ = (\inst38|inst41~q\ & (\inst38|inst43~q\ & (\inst42|seven_seg1[5]~93_combout\ & \inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg1[5]~93_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~213_combout\);

-- Location: LCCOMB_X7_Y25_N16
\inst42|seven_seg1[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~219_combout\ = (\inst42|seven_seg1[1]~213_combout\) # ((!\inst38|inst42~q\ & \inst42|seven_seg1[1]~218_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~218_combout\,
	datad => \inst42|seven_seg1[1]~213_combout\,
	combout => \inst42|seven_seg1[1]~219_combout\);

-- Location: LCCOMB_X7_Y25_N10
\inst42|seven_seg1[1]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~226_combout\ = (\inst38|inst8~q\ & ((\inst38|inst19~q\) # ((\inst42|seven_seg1[1]~219_combout\)))) # (!\inst38|inst8~q\ & (!\inst38|inst19~q\ & (\inst42|seven_seg1[1]~225_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst38|inst19~q\,
	datac => \inst42|seven_seg1[1]~225_combout\,
	datad => \inst42|seven_seg1[1]~219_combout\,
	combout => \inst42|seven_seg1[1]~226_combout\);

-- Location: LCCOMB_X6_Y25_N20
\inst42|seven_seg1[1]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~209_combout\ = (\inst39|inst41~q\ & ((\inst39|inst42~q\) # (!\inst39|inst8~q\))) # (!\inst39|inst41~q\ & (\inst39|inst42~q\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~209_combout\);

-- Location: LCCOMB_X2_Y27_N8
\inst42|seven_seg1[1]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~210_combout\ = (\inst39|inst19~q\ & ((\inst38|inst41~q\) # (\inst42|seven_seg1[1]~209_combout\))) # (!\inst39|inst19~q\ & (\inst38|inst41~q\ & \inst42|seven_seg1[1]~209_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[1]~209_combout\,
	combout => \inst42|seven_seg1[1]~210_combout\);

-- Location: LCCOMB_X2_Y27_N26
\inst42|seven_seg1[1]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~211_combout\ = (!\inst38|inst43~q\ & ((\inst42|seven_seg1[1]~210_combout\ & (\inst39|inst8~q\ $ (\inst38|inst41~q\))) # (!\inst42|seven_seg1[1]~210_combout\ & (\inst39|inst8~q\ & \inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~210_combout\,
	datab => \inst39|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[1]~211_combout\);

-- Location: LCCOMB_X3_Y27_N8
\inst42|seven_seg1[1]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~206_combout\ = (\inst38|inst43~q\) # ((\inst39|inst19~q\ & (\inst39|inst42~q\ & \inst39|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst19~q\,
	datab => \inst39|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst41~q\,
	combout => \inst42|seven_seg1[1]~206_combout\);

-- Location: LCCOMB_X3_Y27_N14
\inst42|seven_seg1[1]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~207_combout\ = (!\inst38|inst41~q\ & (!\inst38|inst42~q\ & (!\inst39|inst8~q\ & !\inst42|seven_seg1[1]~206_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[1]~206_combout\,
	combout => \inst42|seven_seg1[1]~207_combout\);

-- Location: LCCOMB_X3_Y27_N28
\inst42|seven_seg1[1]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~208_combout\ = (!\inst38|inst41~q\ & (\inst38|inst43~q\ & ((\inst42|Equal0~12_combout\) # (!\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|Equal0~12_combout\,
	combout => \inst42|seven_seg1[1]~208_combout\);

-- Location: LCCOMB_X3_Y27_N18
\inst42|seven_seg1[1]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~212_combout\ = (\inst42|seven_seg1[1]~207_combout\) # ((\inst38|inst42~q\ & ((\inst42|seven_seg1[1]~211_combout\) # (\inst42|seven_seg1[1]~208_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~211_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[1]~207_combout\,
	datad => \inst42|seven_seg1[1]~208_combout\,
	combout => \inst42|seven_seg1[1]~212_combout\);

-- Location: LCCOMB_X7_Y23_N12
\inst42|seven_seg1[1]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~233_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\) # (\inst39|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg1[1]~233_combout\);

-- Location: LCCOMB_X7_Y23_N14
\inst42|seven_seg1[1]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~232_combout\ = (\inst42|seven_seg0[6]~265_combout\ & (\inst42|Equal0~1_combout\ & (\inst42|seven_seg1[3]~39_combout\ & \inst39|inst43~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~265_combout\,
	datab => \inst42|Equal0~1_combout\,
	datac => \inst42|seven_seg1[3]~39_combout\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg1[1]~232_combout\);

-- Location: LCCOMB_X7_Y23_N26
\inst42|seven_seg1[1]~256\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~256_combout\ = (!\inst38|inst43~q\ & (!\inst39|inst8~q\ & ((\inst42|seven_seg1[1]~232_combout\) # (!\inst42|seven_seg1[1]~233_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg1[1]~233_combout\,
	datac => \inst42|seven_seg1[1]~232_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~256_combout\);

-- Location: LCCOMB_X8_Y23_N4
\inst42|seven_seg1[1]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~227_combout\ = (\inst39|inst41~q\ & ((\inst39|inst8~q\) # (\inst39|inst42~q\))) # (!\inst39|inst41~q\ & (\inst39|inst8~q\ & \inst39|inst42~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datac => \inst39|inst8~q\,
	datad => \inst39|inst42~q\,
	combout => \inst42|seven_seg1[1]~227_combout\);

-- Location: LCCOMB_X7_Y23_N2
\inst42|seven_seg1[1]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~228_combout\ = (\inst38|inst43~q\ & (!\inst42|seven_seg1[1]~227_combout\ & (!\inst39|inst19~q\ & !\inst39|inst8~q\))) # (!\inst38|inst43~q\ & (\inst39|inst8~q\ & ((\inst42|seven_seg1[1]~227_combout\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~227_combout\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~228_combout\);

-- Location: LCCOMB_X7_Y23_N4
\inst42|seven_seg1[1]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~229_combout\ = (\inst38|inst41~q\ & ((\inst39|inst19~q\) # ((\inst39|inst41~q\ & \inst39|inst42~q\)))) # (!\inst38|inst41~q\ & (((\inst39|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~229_combout\);

-- Location: LCCOMB_X7_Y23_N18
\inst42|seven_seg1[1]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~230_combout\ = (\inst38|inst41~q\ & (((\inst42|seven_seg1[1]~229_combout\ & \inst39|inst8~q\)))) # (!\inst38|inst41~q\ & (\inst42|Equal0~9_combout\ & (!\inst42|seven_seg1[1]~229_combout\ & !\inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~9_combout\,
	datab => \inst38|inst41~q\,
	datac => \inst42|seven_seg1[1]~229_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg1[1]~230_combout\);

-- Location: LCCOMB_X7_Y23_N8
\inst42|seven_seg1[1]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~231_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[1]~230_combout\ & (\inst38|inst43~q\ $ (!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[1]~230_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg1[1]~231_combout\);

-- Location: LCCOMB_X7_Y23_N6
\inst42|seven_seg1[1]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~234_combout\ = (\inst42|seven_seg1[1]~231_combout\ & ((\inst42|seven_seg1[1]~256_combout\) # ((!\inst38|inst42~q\)))) # (!\inst42|seven_seg1[1]~231_combout\ & (((\inst42|seven_seg1[1]~228_combout\ & \inst38|inst42~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~256_combout\,
	datab => \inst42|seven_seg1[1]~228_combout\,
	datac => \inst42|seven_seg1[1]~231_combout\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~234_combout\);

-- Location: LCCOMB_X7_Y25_N12
\inst42|seven_seg1[1]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~235_combout\ = (\inst42|seven_seg1[1]~226_combout\ & (((\inst42|seven_seg1[1]~234_combout\) # (!\inst38|inst19~q\)))) # (!\inst42|seven_seg1[1]~226_combout\ & (\inst42|seven_seg1[1]~212_combout\ & (\inst38|inst19~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~226_combout\,
	datab => \inst42|seven_seg1[1]~212_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg1[1]~234_combout\,
	combout => \inst42|seven_seg1[1]~235_combout\);

-- Location: LCCOMB_X3_Y27_N12
\inst42|seven_seg1[3]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~241_combout\ = (!\inst38|inst8~q\ & (!\inst39|inst19~q\ & ((!\inst39|inst42~q\) # (!\inst39|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst41~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg1[3]~241_combout\);

-- Location: LCCOMB_X3_Y27_N22
\inst42|seven_seg1[3]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~242_combout\ = (!\inst38|inst43~q\ & ((\inst39|inst8~q\ & ((\inst42|seven_seg1[3]~241_combout\))) # (!\inst39|inst8~q\ & (\inst42|seven_seg0[6]~271_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[6]~271_combout\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst8~q\,
	datad => \inst42|seven_seg1[3]~241_combout\,
	combout => \inst42|seven_seg1[3]~242_combout\);

-- Location: LCCOMB_X3_Y27_N10
\inst42|seven_seg1[3]~258\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~258_combout\ = (\inst39|inst8~q\ & \inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datac => \inst38|inst43~q\,
	combout => \inst42|seven_seg1[3]~258_combout\);

-- Location: LCCOMB_X3_Y27_N16
\inst42|seven_seg1[3]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~243_combout\ = ((\inst42|seven_seg1[3]~258_combout\ & (!\inst38|inst8~q\ & \inst42|Equal0~12_combout\))) # (!\inst42|seven_seg1[3]~180_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~258_combout\,
	datab => \inst42|seven_seg1[3]~180_combout\,
	datac => \inst38|inst8~q\,
	datad => \inst42|Equal0~12_combout\,
	combout => \inst42|seven_seg1[3]~243_combout\);

-- Location: LCCOMB_X3_Y27_N30
\inst42|seven_seg1[0]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~244_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\) # ((\inst42|seven_seg1[3]~242_combout\)))) # (!\inst38|inst41~q\ & (!\inst38|inst42~q\ & ((\inst42|seven_seg1[3]~243_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~242_combout\,
	datad => \inst42|seven_seg1[3]~243_combout\,
	combout => \inst42|seven_seg1[0]~244_combout\);

-- Location: LCCOMB_X4_Y27_N12
\inst42|seven_seg1[3]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~240_combout\ = (\inst38|inst43~q\ & ((\inst42|seven_seg1[3]~165_combout\) # ((!\inst39|inst8~q\ & \inst42|seven_seg1[3]~166_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst42|seven_seg1[3]~166_combout\,
	datac => \inst38|inst43~q\,
	datad => \inst42|seven_seg1[3]~165_combout\,
	combout => \inst42|seven_seg1[3]~240_combout\);

-- Location: LCCOMB_X7_Y26_N24
\inst42|seven_seg1[0]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~245_combout\ = (\inst42|seven_seg1[0]~244_combout\ & (((\inst42|seven_seg1[3]~183_combout\)) # (!\inst38|inst42~q\))) # (!\inst42|seven_seg1[0]~244_combout\ & (\inst38|inst42~q\ & (\inst42|seven_seg1[3]~240_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[0]~244_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~240_combout\,
	datad => \inst42|seven_seg1[3]~183_combout\,
	combout => \inst42|seven_seg1[0]~245_combout\);

-- Location: LCCOMB_X8_Y26_N8
\inst42|seven_seg1[0]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~236_combout\ = (\inst38|inst42~q\ & (((\inst38|inst41~q\)))) # (!\inst38|inst42~q\ & ((\inst38|inst41~q\ & (\inst42|seven_seg1[3]~161_combout\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg1[3]~164_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[3]~161_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[3]~164_combout\,
	combout => \inst42|seven_seg1[0]~236_combout\);

-- Location: LCCOMB_X6_Y27_N26
\inst42|seven_seg1[3]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~237_combout\ = (!\inst38|inst8~q\ & (!\inst39|inst42~q\ & (\inst42|seven_seg1[5]~175_combout\ & \inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst8~q\,
	datab => \inst39|inst42~q\,
	datac => \inst42|seven_seg1[5]~175_combout\,
	datad => \inst42|Equal0~9_combout\,
	combout => \inst42|seven_seg1[3]~237_combout\);

-- Location: LCCOMB_X5_Y27_N28
\inst42|seven_seg1[3]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[3]~238_combout\ = (\inst42|seven_seg1[3]~170_combout\) # ((\inst42|seven_seg1[3]~237_combout\) # ((\inst42|seven_seg1[4]~251_combout\ & \inst42|seven_seg1[3]~259_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[4]~251_combout\,
	datab => \inst42|seven_seg1[3]~170_combout\,
	datac => \inst42|seven_seg1[3]~259_combout\,
	datad => \inst42|seven_seg1[3]~237_combout\,
	combout => \inst42|seven_seg1[3]~238_combout\);

-- Location: LCCOMB_X8_Y26_N14
\inst42|seven_seg1[0]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~239_combout\ = (\inst42|seven_seg1[0]~236_combout\ & (((\inst42|seven_seg1[3]~238_combout\)) # (!\inst38|inst42~q\))) # (!\inst42|seven_seg1[0]~236_combout\ & (\inst38|inst42~q\ & ((\inst42|seven_seg1[3]~156_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[0]~236_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg1[3]~238_combout\,
	datad => \inst42|seven_seg1[3]~156_combout\,
	combout => \inst42|seven_seg1[0]~239_combout\);

-- Location: LCCOMB_X7_Y26_N30
\inst42|seven_seg1[0]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[0]~246_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg1[0]~239_combout\))) # (!\inst38|inst19~q\ & (\inst42|seven_seg1[0]~245_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg1[0]~245_combout\,
	datac => \inst42|seven_seg1[0]~239_combout\,
	combout => \inst42|seven_seg1[0]~246_combout\);

-- Location: LCCOMB_X6_Y25_N22
\inst42|seven_seg2[6]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~58_combout\ = (\inst38|inst42~q\ & ((\inst39|inst19~q\) # ((\inst38|inst43~q\) # (\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst43~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[6]~58_combout\);

-- Location: LCCOMB_X6_Y26_N10
\inst42|seven_seg2[6]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~57_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\) # ((\inst42|Equal0~3_combout\ & \inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst41~q\,
	datac => \inst42|Equal0~3_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[6]~57_combout\);

-- Location: LCCOMB_X6_Y25_N12
\inst42|seven_seg2[6]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~59_combout\ = (\inst38|inst8~q\ & (((\inst42|seven_seg2[6]~57_combout\)))) # (!\inst38|inst8~q\ & (((!\inst38|inst41~q\)) # (!\inst42|seven_seg2[6]~58_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[6]~58_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg2[6]~57_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[6]~59_combout\);

-- Location: LCCOMB_X9_Y26_N6
\inst42|seven_seg2[6]~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~18_combout\ = (((!\inst38|inst8~q\) # (!\inst38|inst41~q\)) # (!\inst38|inst42~q\)) # (!\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst8~q\,
	combout => \inst42|seven_seg2[6]~18_combout\);

-- Location: LCCOMB_X6_Y24_N22
\inst42|seven_seg2[6]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~15_combout\ = (\inst39|inst41~q\ & (\inst39|inst43~q\ & (\inst39|inst42~q\ & \inst42|seven_seg2[6]~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst43~q\,
	datac => \inst39|inst42~q\,
	datad => \inst42|seven_seg2[6]~18_combout\,
	combout => \inst42|seven_seg2[6]~15_combout\);

-- Location: LCCOMB_X6_Y24_N16
\inst42|seven_seg2[6]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~99_combout\ = (\inst38|inst43~q\ & ((\inst39|inst8~q\) # ((\inst39|inst19~q\) # (\inst42|seven_seg2[6]~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg2[6]~15_combout\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[6]~99_combout\);

-- Location: LCCOMB_X6_Y25_N14
\inst42|seven_seg2[6]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~95_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg2[6]~99_combout\ & \inst38|inst41~q\)))) # (!\inst38|inst42~q\ & (!\inst38|inst43~q\ & ((!\inst38|inst41~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg2[6]~99_combout\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[6]~95_combout\);

-- Location: LCCOMB_X6_Y25_N10
\inst42|seven_seg2[6]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[6]~60_combout\ = (\inst38|inst19~q\ & (((\inst38|inst8~q\ & \inst42|seven_seg2[6]~95_combout\)))) # (!\inst38|inst19~q\ & (\inst42|seven_seg2[6]~59_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[6]~59_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg2[6]~95_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[6]~60_combout\);

-- Location: LCCOMB_X9_Y26_N22
\inst42|seven_seg2[5]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~66_combout\ = (\inst38|inst8~q\) # ((\inst42|seven_seg0[0]~97_combout\ & (!\inst42|Equal0~9_combout\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~97_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|Equal0~9_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[5]~66_combout\);

-- Location: LCCOMB_X6_Y26_N4
\inst42|Equal0~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~13_combout\ = (\inst42|Equal0~3_combout\ & (\inst42|Equal0~5_combout\ & (!\inst38|inst41~q\ & \inst42|Equal0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~3_combout\,
	datab => \inst42|Equal0~5_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|Equal0~6_combout\,
	combout => \inst42|Equal0~13_combout\);

-- Location: LCCOMB_X9_Y26_N4
\inst42|seven_seg2[4]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~56_combout\ = (!\inst38|inst42~q\ & (!\inst38|inst41~q\ & !\inst38|inst43~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[4]~56_combout\);

-- Location: LCCOMB_X6_Y26_N26
\inst42|seven_seg2[5]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~64_combout\ = (\inst42|Equal0~13_combout\) # ((\inst42|seven_seg2[4]~56_combout\ & ((!\inst42|Equal0~7_combout\) # (!\inst42|Equal0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|Equal0~3_combout\,
	datab => \inst42|Equal0~7_combout\,
	datac => \inst42|Equal0~13_combout\,
	datad => \inst42|seven_seg2[4]~56_combout\,
	combout => \inst42|seven_seg2[5]~64_combout\);

-- Location: LCCOMB_X6_Y26_N16
\inst42|seven_seg2[5]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~63_combout\ = (\inst38|inst19~q\ & (\inst38|inst8~q\)) # (!\inst38|inst19~q\ & ((\inst38|inst8~q\ & ((\inst42|seven_seg2[6]~57_combout\))) # (!\inst38|inst8~q\ & (\inst42|Equal0~13_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst8~q\,
	datac => \inst42|Equal0~13_combout\,
	datad => \inst42|seven_seg2[6]~57_combout\,
	combout => \inst42|seven_seg2[5]~63_combout\);

-- Location: LCCOMB_X10_Y26_N24
\inst42|seven_seg2[5]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~61_combout\ = (\inst38|inst43~q\ & (\inst39|inst8~q\ & ((\inst39|inst41~q\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg2[5]~61_combout\);

-- Location: LCCOMB_X6_Y26_N14
\inst42|seven_seg2[5]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~62_combout\ = (\inst42|Equal0~13_combout\ & (((!\inst42|seven_seg2[5]~61_combout\) # (!\inst38|inst41~q\)) # (!\inst38|inst42~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst41~q\,
	datac => \inst42|Equal0~13_combout\,
	datad => \inst42|seven_seg2[5]~61_combout\,
	combout => \inst42|seven_seg2[5]~62_combout\);

-- Location: LCCOMB_X6_Y26_N24
\inst42|seven_seg2[5]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~65_combout\ = (\inst42|seven_seg2[5]~63_combout\ & ((\inst42|seven_seg2[5]~64_combout\) # ((!\inst38|inst19~q\)))) # (!\inst42|seven_seg2[5]~63_combout\ & (((\inst42|seven_seg2[5]~62_combout\ & \inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~64_combout\,
	datab => \inst42|seven_seg2[5]~63_combout\,
	datac => \inst42|seven_seg2[5]~62_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[5]~65_combout\);

-- Location: LCCOMB_X9_Y26_N0
\inst42|seven_seg2[5]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~67_combout\ = (((!\inst38|inst42~q\ & \inst42|seven_seg1[5]~175_combout\)) # (!\inst38|inst41~q\)) # (!\inst38|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg2[5]~67_combout\);

-- Location: LCCOMB_X9_Y26_N14
\inst42|seven_seg2[5]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[5]~68_combout\ = (\inst42|seven_seg2[5]~66_combout\ & ((\inst42|seven_seg2[5]~65_combout\) # ((!\inst38|inst8~q\ & \inst42|seven_seg2[5]~67_combout\)))) # (!\inst42|seven_seg2[5]~66_combout\ & (!\inst38|inst8~q\ & 
-- ((\inst42|seven_seg2[5]~67_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~66_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg2[5]~65_combout\,
	datad => \inst42|seven_seg2[5]~67_combout\,
	combout => \inst42|seven_seg2[5]~68_combout\);

-- Location: LCCOMB_X9_Y26_N12
\inst42|seven_seg1[1]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg1[1]~247_combout\ = (!\inst38|inst41~q\ & !\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst41~q\,
	datad => \inst38|inst42~q\,
	combout => \inst42|seven_seg1[1]~247_combout\);

-- Location: LCCOMB_X9_Y26_N2
\inst42|seven_seg2[4]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~69_combout\ = ((\inst38|inst43~q\ & ((\inst42|Equal0~11_combout\) # (\inst39|inst8~q\)))) # (!\inst42|seven_seg1[1]~247_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~247_combout\,
	datab => \inst42|Equal0~11_combout\,
	datac => \inst39|inst8~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[4]~69_combout\);

-- Location: LCCOMB_X10_Y26_N26
\inst42|seven_seg2[4]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~70_combout\ = (\inst39|inst41~q\ & (\inst39|inst19~q\ & (\inst42|Equal0~0_combout\ & \inst39|inst8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst42|Equal0~0_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[4]~70_combout\);

-- Location: LCCOMB_X10_Y26_N20
\inst42|seven_seg2[4]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~71_combout\ = ((!\inst38|inst43~q\ & ((!\inst39|inst8~q\) # (!\inst39|inst19~q\)))) # (!\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[4]~71_combout\);

-- Location: LCCOMB_X10_Y26_N22
\inst42|seven_seg2[4]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~72_combout\ = (\inst42|seven_seg2[4]~70_combout\) # ((\inst38|inst41~q\ & (\inst38|inst42~q\)) # (!\inst38|inst41~q\ & ((\inst42|seven_seg2[4]~71_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst42|seven_seg2[4]~70_combout\,
	datad => \inst42|seven_seg2[4]~71_combout\,
	combout => \inst42|seven_seg2[4]~72_combout\);

-- Location: LCCOMB_X10_Y26_N28
\inst42|seven_seg2[2]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~73_combout\ = (\inst38|inst41~q\ & (\inst38|inst42~q\ & ((\inst39|inst19~q\) # (!\inst42|seven_seg1[5]~175_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst38|inst42~q\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg2[2]~73_combout\);

-- Location: LCCOMB_X10_Y26_N14
\inst42|seven_seg2[4]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~74_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg2[4]~72_combout\) # ((\inst38|inst19~q\)))) # (!\inst38|inst8~q\ & (((!\inst38|inst19~q\ & !\inst42|seven_seg2[2]~73_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011001011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[4]~72_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg2[2]~73_combout\,
	combout => \inst42|seven_seg2[4]~74_combout\);

-- Location: LCCOMB_X9_Y26_N26
\inst42|seven_seg2[4]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~76_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg2[6]~99_combout\)))) # (!\inst38|inst42~q\ & (\inst42|seven_seg1[5]~175_combout\ & (\inst42|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst42|seven_seg1[5]~175_combout\,
	datac => \inst42|Equal0~9_combout\,
	datad => \inst42|seven_seg2[6]~99_combout\,
	combout => \inst42|seven_seg2[4]~76_combout\);

-- Location: LCCOMB_X9_Y26_N8
\inst42|seven_seg2[4]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~75_combout\ = (\inst38|inst8~q\ & (\inst38|inst41~q\ & \inst38|inst19~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst8~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[4]~75_combout\);

-- Location: LCCOMB_X9_Y26_N20
\inst42|seven_seg2[4]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~77_combout\ = (\inst42|seven_seg2[4]~56_combout\) # ((!\inst42|seven_seg2[4]~76_combout\ & (\inst42|seven_seg2[4]~75_combout\ & \inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[4]~76_combout\,
	datab => \inst42|seven_seg2[4]~75_combout\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg2[4]~56_combout\,
	combout => \inst42|seven_seg2[4]~77_combout\);

-- Location: LCCOMB_X9_Y26_N30
\inst42|seven_seg2[4]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[4]~78_combout\ = (\inst38|inst19~q\ & ((\inst42|seven_seg2[4]~74_combout\ & ((\inst42|seven_seg2[4]~77_combout\))) # (!\inst42|seven_seg2[4]~74_combout\ & (\inst42|seven_seg2[4]~69_combout\)))) # (!\inst38|inst19~q\ & 
-- (((\inst42|seven_seg2[4]~74_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst42|seven_seg2[4]~69_combout\,
	datac => \inst42|seven_seg2[4]~74_combout\,
	datad => \inst42|seven_seg2[4]~77_combout\,
	combout => \inst42|seven_seg2[4]~78_combout\);

-- Location: LCCOMB_X9_Y26_N18
\inst42|seven_seg2[3]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~82_combout\ = (\inst38|inst19~q\ & (\inst38|inst41~q\ & (\inst39|inst8~q\ $ (!\inst38|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst8~q\,
	datab => \inst38|inst19~q\,
	datac => \inst38|inst41~q\,
	datad => \inst38|inst43~q\,
	combout => \inst42|seven_seg2[3]~82_combout\);

-- Location: LCCOMB_X9_Y26_N28
\inst42|seven_seg2[3]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~83_combout\ = (\inst38|inst8~q\) # ((\inst42|seven_seg0[0]~97_combout\ & (!\inst42|Equal0~9_combout\ & \inst42|seven_seg2[3]~82_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg0[0]~97_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|Equal0~9_combout\,
	datad => \inst42|seven_seg2[3]~82_combout\,
	combout => \inst42|seven_seg2[3]~83_combout\);

-- Location: LCCOMB_X6_Y25_N4
\inst42|seven_seg2[3]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~79_combout\ = (!\inst38|inst43~q\ & (!\inst39|inst19~q\ & !\inst39|inst8~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst38|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[3]~79_combout\);

-- Location: LCCOMB_X9_Y27_N30
\inst42|seven_seg2[3]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~80_combout\ = (!\inst38|inst19~q\ & (((\inst42|seven_seg2[3]~79_combout\) # (!\inst38|inst42~q\)) # (!\inst38|inst41~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg2[3]~79_combout\,
	combout => \inst42|seven_seg2[3]~80_combout\);

-- Location: LCCOMB_X9_Y26_N16
\inst42|seven_seg2[3]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~81_combout\ = (\inst38|inst41~q\ & ((\inst38|inst42~q\ & (\inst38|inst19~q\)) # (!\inst38|inst42~q\ & ((!\inst42|seven_seg1[5]~175_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst19~q\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst41~q\,
	datad => \inst42|seven_seg1[5]~175_combout\,
	combout => \inst42|seven_seg2[3]~81_combout\);

-- Location: LCCOMB_X9_Y26_N10
\inst42|seven_seg2[3]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[3]~84_combout\ = (\inst42|seven_seg2[3]~83_combout\ & (\inst42|seven_seg2[5]~65_combout\)) # (!\inst42|seven_seg2[3]~83_combout\ & (((\inst42|seven_seg2[3]~80_combout\) # (\inst42|seven_seg2[3]~81_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~65_combout\,
	datab => \inst42|seven_seg2[3]~83_combout\,
	datac => \inst42|seven_seg2[3]~80_combout\,
	datad => \inst42|seven_seg2[3]~81_combout\,
	combout => \inst42|seven_seg2[3]~84_combout\);

-- Location: LCCOMB_X6_Y24_N2
\inst42|seven_seg2[2]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~98_combout\ = (\inst39|inst19~q\ & ((\inst39|inst41~q\) # ((\inst39|inst42~q\ & \inst39|inst43~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst39|inst42~q\,
	datad => \inst39|inst43~q\,
	combout => \inst42|seven_seg2[2]~98_combout\);

-- Location: LCCOMB_X6_Y24_N20
\inst42|seven_seg2[2]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~87_combout\ = ((!\inst39|inst41~q\ & (\inst42|Equal0~7_combout\ & \inst39|inst19~q\))) # (!\inst42|seven_seg2[2]~98_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst42|Equal0~7_combout\,
	datac => \inst39|inst19~q\,
	datad => \inst42|seven_seg2[2]~98_combout\,
	combout => \inst42|seven_seg2[2]~87_combout\);

-- Location: LCCOMB_X10_Y26_N4
\inst42|seven_seg2[2]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~88_combout\ = (((\inst42|seven_seg2[2]~87_combout\ & !\inst39|inst8~q\)) # (!\inst38|inst19~q\)) # (!\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst42|seven_seg2[2]~87_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[2]~88_combout\);

-- Location: LCCOMB_X10_Y26_N16
\inst42|seven_seg2[2]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~85_combout\ = (\inst38|inst43~q\ & ((\inst39|inst8~q\) # ((\inst39|inst41~q\) # (\inst39|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst43~q\,
	datab => \inst39|inst8~q\,
	datac => \inst39|inst41~q\,
	datad => \inst39|inst19~q\,
	combout => \inst42|seven_seg2[2]~85_combout\);

-- Location: LCCOMB_X10_Y26_N18
\inst42|seven_seg2[2]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~86_combout\ = (!\inst38|inst19~q\ & ((\inst42|seven_seg2[2]~73_combout\) # ((!\inst38|inst41~q\ & !\inst42|seven_seg2[2]~85_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg2[2]~85_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg2[2]~73_combout\,
	combout => \inst42|seven_seg2[2]~86_combout\);

-- Location: LCCOMB_X10_Y26_N6
\inst42|seven_seg2[2]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[2]~89_combout\ = (!\inst38|inst8~q\ & ((\inst42|seven_seg2[2]~86_combout\) # ((\inst42|seven_seg1[1]~247_combout\ & \inst42|seven_seg2[2]~88_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg1[1]~247_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|seven_seg2[2]~88_combout\,
	datad => \inst42|seven_seg2[2]~86_combout\,
	combout => \inst42|seven_seg2[2]~89_combout\);

-- Location: LCCOMB_X6_Y26_N6
\inst42|seven_seg2[1]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~40_combout\ = (((\inst38|inst8~q\ & !\inst38|inst19~q\)) # (!\inst39|inst43~q\)) # (!\inst39|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst42~q\,
	datab => \inst39|inst43~q\,
	datac => \inst38|inst8~q\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[1]~40_combout\);

-- Location: LCCOMB_X10_Y26_N10
\inst42|seven_seg2[1]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~37_combout\ = (((!\inst39|inst41~q\ & \inst42|seven_seg2[1]~40_combout\)) # (!\inst39|inst8~q\)) # (!\inst39|inst19~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst39|inst19~q\,
	datac => \inst42|seven_seg2[1]~40_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[1]~37_combout\);

-- Location: LCCOMB_X10_Y26_N12
\inst42|seven_seg2[1]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~100_combout\ = ((!\inst38|inst42~q\ & ((\inst42|seven_seg2[1]~37_combout\) # (!\inst38|inst43~q\)))) # (!\inst38|inst41~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[1]~37_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst43~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[1]~100_combout\);

-- Location: LCCOMB_X5_Y26_N14
\inst42|Equal0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~14_combout\ = (\inst38|inst41~q\ & (\inst42|Equal0~1_combout\ & (!\inst38|inst19~q\ & \inst42|seven_seg0[6]~71_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|Equal0~1_combout\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg0[6]~71_combout\,
	combout => \inst42|Equal0~14_combout\);

-- Location: LCCOMB_X10_Y26_N8
\inst42|Equal0~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|Equal0~15_combout\ = (\inst39|inst43~q\ & (\inst42|Equal0~0_combout\ & (\inst39|inst42~q\ & \inst42|Equal0~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst43~q\,
	datab => \inst42|Equal0~0_combout\,
	datac => \inst39|inst42~q\,
	datad => \inst42|Equal0~14_combout\,
	combout => \inst42|Equal0~15_combout\);

-- Location: LCCOMB_X10_Y26_N30
\inst42|seven_seg2[1]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~90_combout\ = (\inst38|inst42~q\ & ((\inst38|inst19~q\ & (\inst42|seven_seg2[5]~61_combout\)) # (!\inst38|inst19~q\ & ((\inst42|seven_seg2[2]~85_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[5]~61_combout\,
	datab => \inst38|inst42~q\,
	datac => \inst38|inst19~q\,
	datad => \inst42|seven_seg2[2]~85_combout\,
	combout => \inst42|seven_seg2[1]~90_combout\);

-- Location: LCCOMB_X10_Y26_N0
\inst42|seven_seg2[1]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~91_combout\ = (\inst38|inst8~q\ & (((\inst38|inst19~q\)))) # (!\inst38|inst8~q\ & ((\inst42|seven_seg2[1]~90_combout\ & ((!\inst38|inst41~q\) # (!\inst38|inst19~q\))) # (!\inst42|seven_seg2[1]~90_combout\ & ((\inst38|inst19~q\) # 
-- (\inst38|inst41~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001111110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[1]~90_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst38|inst19~q\,
	datad => \inst38|inst41~q\,
	combout => \inst42|seven_seg2[1]~91_combout\);

-- Location: LCCOMB_X10_Y26_N2
\inst42|seven_seg2[1]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[1]~92_combout\ = (\inst38|inst8~q\ & ((\inst42|seven_seg2[1]~91_combout\ & ((\inst42|Equal0~15_combout\))) # (!\inst42|seven_seg2[1]~91_combout\ & (\inst42|seven_seg2[1]~100_combout\)))) # (!\inst38|inst8~q\ & 
-- (((!\inst42|seven_seg2[1]~91_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst42|seven_seg2[1]~100_combout\,
	datab => \inst38|inst8~q\,
	datac => \inst42|Equal0~15_combout\,
	datad => \inst42|seven_seg2[1]~91_combout\,
	combout => \inst42|seven_seg2[1]~92_combout\);

-- Location: LCCOMB_X6_Y25_N30
\inst42|seven_seg2[0]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~97_combout\ = (((!\inst39|inst41~q\ & !\inst39|inst19~q\)) # (!\inst39|inst8~q\)) # (!\inst38|inst43~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst39|inst41~q\,
	datab => \inst38|inst43~q\,
	datac => \inst39|inst19~q\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[0]~97_combout\);

-- Location: LCCOMB_X6_Y25_N28
\inst42|seven_seg2[0]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~96_combout\ = (\inst38|inst42~q\ & (((\inst42|seven_seg2[0]~97_combout\)))) # (!\inst38|inst42~q\ & ((\inst38|inst43~q\) # ((\inst39|inst8~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datab => \inst38|inst43~q\,
	datac => \inst42|seven_seg2[0]~97_combout\,
	datad => \inst39|inst8~q\,
	combout => \inst42|seven_seg2[0]~96_combout\);

-- Location: LCCOMB_X6_Y25_N2
\inst42|seven_seg2[0]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~93_combout\ = (\inst38|inst41~q\ & ((\inst38|inst19~q\ & (\inst42|seven_seg2[0]~96_combout\)) # (!\inst38|inst19~q\ & ((!\inst42|seven_seg2[6]~58_combout\))))) # (!\inst38|inst41~q\ & (((!\inst38|inst19~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst41~q\,
	datab => \inst42|seven_seg2[0]~96_combout\,
	datac => \inst42|seven_seg2[6]~58_combout\,
	datad => \inst38|inst19~q\,
	combout => \inst42|seven_seg2[0]~93_combout\);

-- Location: LCCOMB_X6_Y25_N8
\inst42|seven_seg2[0]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg2[0]~94_combout\ = (!\inst38|inst8~q\ & \inst42|seven_seg2[0]~93_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst38|inst8~q\,
	datad => \inst42|seven_seg2[0]~93_combout\,
	combout => \inst42|seven_seg2[0]~94_combout\);

-- Location: LCCOMB_X9_Y26_N24
\inst42|seven_seg3[2]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst42|seven_seg3[2]~0_combout\ = ((!\inst42|seven_seg2[6]~99_combout\) # (!\inst42|seven_seg2[4]~75_combout\)) # (!\inst38|inst42~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst38|inst42~q\,
	datac => \inst42|seven_seg2[4]~75_combout\,
	datad => \inst42|seven_seg2[6]~99_combout\,
	combout => \inst42|seven_seg3[2]~0_combout\);

ww_D0(6) <= \D0[6]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;

ww_OUTPUT(9) <= \OUTPUT[9]~output_o\;

ww_OUTPUT(8) <= \OUTPUT[8]~output_o\;

ww_OUTPUT(7) <= \OUTPUT[7]~output_o\;

ww_OUTPUT(6) <= \OUTPUT[6]~output_o\;

ww_OUTPUT(5) <= \OUTPUT[5]~output_o\;

ww_OUTPUT(4) <= \OUTPUT[4]~output_o\;

ww_OUTPUT(3) <= \OUTPUT[3]~output_o\;

ww_OUTPUT(2) <= \OUTPUT[2]~output_o\;

ww_OUTPUT(1) <= \OUTPUT[1]~output_o\;

ww_OUTPUT(0) <= \OUTPUT[0]~output_o\;
END structure;


