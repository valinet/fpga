#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int a[3];
string digit(int n, int t, int special)
{
	for (int y = 0; y < 4; y++) a[y] = 0;
	int i = 0;
	char* x = new char;
	_itoa(n, x, 10);
	string p = x;
	while (n > 0)
	{
		a[i] = n % 10;
		n = n / 10;
		i++;
	}
	switch (a[4 - t])
	{
	case 0:
		if (4 - t < p.length() || !special) return "1000000";
		else return "1111111";
	case 1:
		return "1111001";
	case 2:
		return "0100100";
	case 3:
		return "0110000";
	case 4:
		return "0011001";
	case 5:
		return "0010010";
	case 6:
		return "0000010";
	case 7:
		return "1111000";
	case 8:
		return "0000000";
	case 9:
		return "0010000";
	}
}
string base2(int i, int w)
{
	char* t = new char;
	_itoa(i, t, 2);
	string c = t;
	for (int i = 0; i < w; i++)
	{
		if (c.length() - 1 == i)
		{
			int p = w - i - 1;
			string padding = "";
			for (int j = 0; j < p; j++)
				padding.push_back('0');
			c = padding + c;
			return c;
		}
	}
}
int main()
{
	cout << "This program generates Verilog code which maps an n bit number to the 4 7 bit inputs necessary to display it on the 7 Segment BCD" << endl;
	cout << "Please type the width of your input bus." << endl;
	cout << endl << "Copyright (C) 2006-2016 Democratic Instruments Ltd. All rights reserved." << endl << endl;
	int n;
	cin >> n;
	int p = pow(2, n);
	cout << "What is the maximum value you would like the 7 Segment display to display? Type 0 for maximum value. (It should be smaller than the maximum value allowed by the width of the data bus, which is 2 to the power of length of data bus minus 1)." << endl;
	int max;
	cin >> max;
	if (max != 0) p = max + 1;
	int t[4];
	for (int j = 0; j < 4; j++)
	{
		cout << "Would you like to display on digit number " << j << " ? Type 0 for no, 1 for yes." << endl;
		cin >> t[j];
	}
	cout << "What should be the name of the module?" << endl;
	string name;
	cin >> name;
	cout << "Would you like to write nothing on the display for zeros at the beginning on the number (left) - i.e. instead of 0034, simply __34 ? Type 0 for no, 1 for yes." << endl;
	int answer;
	cin >> answer;
	ofstream g("out.txt");
	g << "module " << name << "(input [" << n - 1 << ":0] bcd"; 
	if (t[0])
		g << ", output reg [6:0] seven_seg0";
	if (t[1])
		g << ", output reg [6:0] seven_seg1";
	if (t[2])
		g << ", output reg [6:0] seven_seg2";
	if (t[3])
		g << ", output reg [6:0] seven_seg3";
	g << ");" << endl << endl << "always @(*)" << endl << "if ";
	for (int i = 0; i < p; i++)
	{
		int b = 0;
		string x3;
		string x2;
		string x1;
		string x0;
		stringstream ss;
		if (t[0])
		{
			ss << "\t\t" << "seven_seg0 = 7'b" << digit(i, 4, answer).c_str() << ";" << endl;
			x3 = ss.str();
		}
		else x3 = "";
		ss.str(std::string());
		if (t[1])
		{
			ss << "\t\t" << "seven_seg1 = 7'b" << digit(i, 3, answer).c_str() << ";" << endl;
			x2 = ss.str();
		}
		else x2 = "";
		ss.str(std::string());
		if (t[2])
		{
			ss << "\t\t" << "seven_seg2 = 7'b" << digit(i, 2, answer).c_str() << ";" << endl;
			x1 = ss.str();
		}
		else x1 = "";
		ss.str(std::string());
		if (t[3])
		{
			ss << "\t\t" << "seven_seg3 = 7'b" << digit(i, 1, answer).c_str() << ";" << endl;
			x0 = ss.str();
		}
		else x0 = "";
		ss.str(std::string());
		if (i == p - 1)
		{
			g << "(bcd == " << n << "'b" << base2(i, n).c_str() << ") begin" << endl << x3 << x2 << x1 << x0 << "end";
		}
		else
		{
			g << "(bcd == " << n << "'b" << base2(i, n).c_str() << ") begin" << endl << x3 << x2 << x1 << x0 << "end else if ";
		}
	}
	g << endl << "endmodule";
	g.close();
	cout << "The file has been created successfully." << endl << endl << "Copyright (C) 2006-2016 Democratic Instruments Ltd. All rights reserved." << endl;
	return 0;
}