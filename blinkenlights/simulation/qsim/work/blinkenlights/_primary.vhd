library verilog;
use verilog.vl_types.all;
entity blinkenlights is
    port(
        LEDG            : out    vl_logic_vector(0 to 9);
        clk             : in     vl_logic
    );
end blinkenlights;
