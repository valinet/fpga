-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "03/12/2016 21:32:06"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	blinkenlights IS
    PORT (
	LEDG : OUT std_logic_vector(0 TO 9);
	clk : IN std_logic
	);
END blinkenlights;

-- Design Ports Information
-- LEDG[0]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[1]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[3]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[5]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[6]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[7]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[8]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF blinkenlights IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_LEDG : std_logic_vector(0 TO 9);
SIGNAL ww_clk : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \LEDG[0]~output_o\ : std_logic;
SIGNAL \LEDG[1]~output_o\ : std_logic;
SIGNAL \LEDG[2]~output_o\ : std_logic;
SIGNAL \LEDG[3]~output_o\ : std_logic;
SIGNAL \LEDG[4]~output_o\ : std_logic;
SIGNAL \LEDG[5]~output_o\ : std_logic;
SIGNAL \LEDG[6]~output_o\ : std_logic;
SIGNAL \LEDG[7]~output_o\ : std_logic;
SIGNAL \LEDG[8]~output_o\ : std_logic;
SIGNAL \LEDG[9]~output_o\ : std_logic;
SIGNAL \inst5|inst|inst~1_combout\ : std_logic;
SIGNAL \inst5|inst1|inst~combout\ : std_logic;
SIGNAL \inst5|inst2|inst~combout\ : std_logic;
SIGNAL \inst5|inst3|inst~combout\ : std_logic;
SIGNAL \inst5|inst3|inst5~combout\ : std_logic;
SIGNAL \inst5|inst4|inst~combout\ : std_logic;
SIGNAL \inst5|inst5|inst~combout\ : std_logic;
SIGNAL \inst5|inst6|inst~combout\ : std_logic;
SIGNAL \inst5|inst6|inst5~combout\ : std_logic;
SIGNAL \inst5|inst7|inst~combout\ : std_logic;
SIGNAL \inst5|inst8|inst~combout\ : std_logic;
SIGNAL \inst5|inst9|inst~combout\ : std_logic;

BEGIN

LEDG <= ww_LEDG;
ww_clk <= clk;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X0_Y20_N9
\LEDG[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst|inst~1_combout\,
	devoe => ww_devoe,
	o => \LEDG[0]~output_o\);

-- Location: IOOBUF_X0_Y20_N2
\LEDG[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst1|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[1]~output_o\);

-- Location: IOOBUF_X0_Y21_N23
\LEDG[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst2|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[2]~output_o\);

-- Location: IOOBUF_X0_Y21_N16
\LEDG[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst3|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[3]~output_o\);

-- Location: IOOBUF_X0_Y24_N23
\LEDG[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst4|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[4]~output_o\);

-- Location: IOOBUF_X0_Y24_N16
\LEDG[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst5|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[5]~output_o\);

-- Location: IOOBUF_X0_Y26_N23
\LEDG[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst6|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[6]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\LEDG[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst7|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[7]~output_o\);

-- Location: IOOBUF_X0_Y27_N9
\LEDG[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst8|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[8]~output_o\);

-- Location: IOOBUF_X0_Y27_N16
\LEDG[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|inst9|inst~combout\,
	devoe => ww_devoe,
	o => \LEDG[9]~output_o\);

-- Location: LCCOMB_X1_Y22_N30
\inst5|inst|inst~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst|inst~1_combout\ = \inst5|inst|inst~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|inst|inst~1_combout\,
	combout => \inst5|inst|inst~1_combout\);

-- Location: LCCOMB_X1_Y22_N24
\inst5|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst1|inst~combout\ = \inst5|inst1|inst~combout\ $ (!\inst5|inst|inst~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|inst1|inst~combout\,
	datac => \inst5|inst|inst~1_combout\,
	combout => \inst5|inst1|inst~combout\);

-- Location: LCCOMB_X1_Y22_N26
\inst5|inst2|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst2|inst~combout\ = \inst5|inst2|inst~combout\ $ (((!\inst5|inst|inst~1_combout\ & \inst5|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010010110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst|inst~1_combout\,
	datab => \inst5|inst1|inst~combout\,
	datac => \inst5|inst2|inst~combout\,
	combout => \inst5|inst2|inst~combout\);

-- Location: LCCOMB_X1_Y22_N4
\inst5|inst3|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst3|inst~combout\ = \inst5|inst3|inst~combout\ $ (((\inst5|inst2|inst~combout\ & (\inst5|inst1|inst~combout\ & !\inst5|inst|inst~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst2|inst~combout\,
	datab => \inst5|inst1|inst~combout\,
	datac => \inst5|inst3|inst~combout\,
	datad => \inst5|inst|inst~1_combout\,
	combout => \inst5|inst3|inst~combout\);

-- Location: LCCOMB_X1_Y22_N22
\inst5|inst3|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst3|inst5~combout\ = (\inst5|inst2|inst~combout\ & (\inst5|inst1|inst~combout\ & (\inst5|inst3|inst~combout\ & !\inst5|inst|inst~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst2|inst~combout\,
	datab => \inst5|inst1|inst~combout\,
	datac => \inst5|inst3|inst~combout\,
	datad => \inst5|inst|inst~1_combout\,
	combout => \inst5|inst3|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N8
\inst5|inst4|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst4|inst~combout\ = \inst5|inst4|inst~combout\ $ (\inst5|inst3|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|inst4|inst~combout\,
	datac => \inst5|inst3|inst5~combout\,
	combout => \inst5|inst4|inst~combout\);

-- Location: LCCOMB_X1_Y22_N18
\inst5|inst5|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst5|inst~combout\ = \inst5|inst5|inst~combout\ $ (((\inst5|inst3|inst5~combout\ & \inst5|inst4|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110001101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst3|inst5~combout\,
	datab => \inst5|inst5|inst~combout\,
	datac => \inst5|inst4|inst~combout\,
	combout => \inst5|inst5|inst~combout\);

-- Location: LCCOMB_X1_Y22_N28
\inst5|inst6|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst6|inst~combout\ = \inst5|inst6|inst~combout\ $ (((\inst5|inst3|inst5~combout\ & (\inst5|inst4|inst~combout\ & \inst5|inst5|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst3|inst5~combout\,
	datab => \inst5|inst6|inst~combout\,
	datac => \inst5|inst4|inst~combout\,
	datad => \inst5|inst5|inst~combout\,
	combout => \inst5|inst6|inst~combout\);

-- Location: LCCOMB_X1_Y22_N14
\inst5|inst6|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst6|inst5~combout\ = (\inst5|inst3|inst5~combout\ & (\inst5|inst6|inst~combout\ & (\inst5|inst4|inst~combout\ & \inst5|inst5|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst3|inst5~combout\,
	datab => \inst5|inst6|inst~combout\,
	datac => \inst5|inst4|inst~combout\,
	datad => \inst5|inst5|inst~combout\,
	combout => \inst5|inst6|inst5~combout\);

-- Location: LCCOMB_X1_Y22_N0
\inst5|inst7|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst7|inst~combout\ = \inst5|inst7|inst~combout\ $ (\inst5|inst6|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst7|inst~combout\,
	datac => \inst5|inst6|inst5~combout\,
	combout => \inst5|inst7|inst~combout\);

-- Location: LCCOMB_X1_Y22_N2
\inst5|inst8|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst8|inst~combout\ = \inst5|inst8|inst~combout\ $ (((\inst5|inst7|inst~combout\ & \inst5|inst6|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst7|inst~combout\,
	datac => \inst5|inst6|inst5~combout\,
	datad => \inst5|inst8|inst~combout\,
	combout => \inst5|inst8|inst~combout\);

-- Location: LCCOMB_X1_Y22_N12
\inst5|inst9|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|inst9|inst~combout\ = \inst5|inst9|inst~combout\ $ (((\inst5|inst6|inst5~combout\ & (\inst5|inst7|inst~combout\ & \inst5|inst8|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|inst9|inst~combout\,
	datab => \inst5|inst6|inst5~combout\,
	datac => \inst5|inst7|inst~combout\,
	datad => \inst5|inst8|inst~combout\,
	combout => \inst5|inst9|inst~combout\);

-- Location: IOIBUF_X41_Y15_N1
\clk~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

ww_LEDG(0) <= \LEDG[0]~output_o\;

ww_LEDG(1) <= \LEDG[1]~output_o\;

ww_LEDG(2) <= \LEDG[2]~output_o\;

ww_LEDG(3) <= \LEDG[3]~output_o\;

ww_LEDG(4) <= \LEDG[4]~output_o\;

ww_LEDG(5) <= \LEDG[5]~output_o\;

ww_LEDG(6) <= \LEDG[6]~output_o\;

ww_LEDG(7) <= \LEDG[7]~output_o\;

ww_LEDG(8) <= \LEDG[8]~output_o\;

ww_LEDG(9) <= \LEDG[9]~output_o\;
END structure;


