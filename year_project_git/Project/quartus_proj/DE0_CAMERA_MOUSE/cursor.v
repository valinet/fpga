module cursor
(
  clk, vga_xx, vga_y, mouse_x, mouse_yy, i_red, i_green, i_blue, o_red, o_green, o_blue, sw_zero, sw_two, lb, mb, rb
);

input  wire    clk;

input    wire   [9:0] vga_xx;
input    wire   [9:0] vga_y;
input    wire   [9:0] mouse_x;
input    wire   [9:0] mouse_yy;
input    wire   [9:0] i_red;
input    wire   [9:0] i_green;
input    wire   [9:0] i_blue;
input    wire   [0:0] sw_zero;
input    wire   [0:0] sw_two;
input    wire   [0:0] lb;
input    wire   [0:0] mb;
input    wire   [0:0] rb;
output reg [9:0] o_red;  
output reg [9:0] o_green;  
output reg [9:0] o_blue;  


integer sim_vga_y = 520;
integer mouse_y;
integer vga_x;
integer MOUSE_BOX_AREA = 11;


always @(posedge clk)
begin
	sim_vga_y = vga_y;
	mouse_y = mouse_yy;
	vga_x = 520 - vga_xx;
    if (sw_zero == 0 && (vga_x >= mouse_x - MOUSE_BOX_AREA) && (vga_x <= mouse_x + MOUSE_BOX_AREA) && (sim_vga_y >= mouse_y - MOUSE_BOX_AREA) 
                && (sim_vga_y <= mouse_y + MOUSE_BOX_AREA))
					 begin
		if ((vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0))
		begin
		    if (sw_two == 1)
            begin
                o_red = 512;
                o_green = 512;
                o_blue = 512;
            end
            else
            begin
                o_red = 512;
                o_green = 512;
                o_blue = 512;
            end
		end
		
		if ((vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1))
		begin
		    if (sw_two == 1)
            begin
                o_red = 0;
                o_green = 0;
                o_blue = 0;
            end
            else
            begin
                o_red = 10'b1111111111;
                o_green = 10'b1111111111;
                o_blue = 10'b1111111111;
            end
				if (lb == 1)
				begin
					 o_red = 0;
                o_green = 0;
                o_blue = 1023;
				end
				if (mb == 1)
				begin
					 o_red = 1023;
                o_green = 1023;
                o_blue = 0;
				end
				if (rb == 1)
				begin
					 o_red = 1023;
                o_green = 0;
                o_blue = 0;
				end
		end
		
		if ((vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 0 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 1 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 2 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 3 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 0) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 4 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 5 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 6 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 7 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 8 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 9 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 10 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 11 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 12 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 13 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 14 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 15 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 16 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 17 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 18 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 19 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 20 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 1) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 2) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 3) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 4) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 5) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 6) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 7) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 8) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 9) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 10) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 11) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 12) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 13) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 14) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 15) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 16) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 17) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 18) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 19) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 20) ||
		(vga_x == mouse_x - MOUSE_BOX_AREA + 21 && sim_vga_y == mouse_y - MOUSE_BOX_AREA + 21))
		begin
		    o_red = i_red;
                o_green = i_green;
                o_blue = i_blue;
		end
		end else begin
		o_red = i_red;
                o_green = i_green;
                o_blue = i_blue;
					 end
end

endmodule