module sanitiser
(
  clk, colour_cam, colour_video, colour_out
);

input  wire    clk;

input    wire   [29:0] colour_cam;
input    wire   [29:0] colour_video;

output reg [29:0] colour_out;  

integer i = 0;

always @(posedge clk)
 begin
  if (colour_cam == 30'b000000000000000000000000000000) begin
   colour_out = 30'b000000000000000000000000000000;
  end else begin
   colour_out = colour_video;
  end
 end

endmodule