module clock_gen ( clk ,reset, clkout,in );

input [4:0] in;
wire [4:0] in;

output clkout;
reg clkout;

input clk ;
wire clk ;

input reset ;
wire reset ;

reg last_in;

always @ (posedge clk) begin
	if(in == last_in) begin
		clkout = 0;
	end else begin
		clkout = 1;
	end
	last_in = in;
end
endmodule
