module accumulator
(
  clk, colour_input, colour_output, x ,y
);

input  wire    clk;

input    wire   [29:0] colour_input;
input    wire   [9:0] x;
input    wire   [9:0] y;

reg [29:0] bufferI [639:0];
reg [29:0] bufferII [639:0];

output reg [29:0] colour_output;  

always @(posedge clk)
begin
	if (colour_input != 0) begin

		if (y % 2 == 0) begin
			bufferI[x] = colour_input;
			colour_output = bufferII[639 - x];
		end else begin
			bufferI[x] = colour_input;
			colour_output = bufferII[639 - x];
		end
	
	end else begin
	
		colour_output = colour_input;
		
	end
end

endmodule