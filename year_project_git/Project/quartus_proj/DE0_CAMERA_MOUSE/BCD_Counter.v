module BCD_Counter ( clk ,dout, clkout );

output [3:0] dout ;
reg [3:0] dout ;

output clkout;
reg clkout;

input clk ;
wire clk ;



initial dout = 0 ;

always @ (posedge (clk)) begin
if(dout < 9) begin
		dout <= dout + 1;
		clkout <= 0;
	end else begin
		dout <= 0;
		clkout <= ~clkout;
	end
end
endmodule
