
# Loop constraints
directive set /vga_mouse_square/core/main CSTEPS_FROM {{. == 2}}

# IO operation constraints
directive set /vga_mouse_square/core/main/io_read(video_in:rsc.d)#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_write(video_out:rsc.d) CSTEPS_FROM {{.. == 1}}

# Real operation constraints
directive set /vga_mouse_square/core/main/acc#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/acc#2 CSTEPS_FROM {{.. == 1}}
