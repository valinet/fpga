
//------> ./rtl_mgc_ioport.v 
//------------------------------------------------------------------
//                M G C _ I O P O R T _ C O M P S
//------------------------------------------------------------------

//------------------------------------------------------------------
//                       M O D U L E S
//------------------------------------------------------------------

//------------------------------------------------------------------
//-- INPUT ENTITIES
//------------------------------------------------------------------

module mgc_in_wire (d, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  output [width-1:0] d;
  input  [width-1:0] z;

  wire   [width-1:0] d;

  assign d = z;

endmodule

//------------------------------------------------------------------

module mgc_in_wire_en (ld, d, lz, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  input              ld;
  output [width-1:0] d;
  output             lz;
  input  [width-1:0] z;

  wire   [width-1:0] d;
  wire               lz;

  assign d = z;
  assign lz = ld;

endmodule

//------------------------------------------------------------------

module mgc_in_wire_wait (ld, vd, d, lz, vz, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  input              ld;
  output             vd;
  output [width-1:0] d;
  output             lz;
  input              vz;
  input  [width-1:0] z;

  wire               vd;
  wire   [width-1:0] d;
  wire               lz;

  assign d = z;
  assign lz = ld;
  assign vd = vz;

endmodule
//------------------------------------------------------------------

module mgc_chan_in (ld, vd, d, lz, vz, z, size, req_size, sizez, sizelz);

  parameter integer rscid = 1;
  parameter integer width = 8;
  parameter integer sz_width = 8;

  input              ld;
  output             vd;
  output [width-1:0] d;
  output             lz;
  input              vz;
  input  [width-1:0] z;
  output [sz_width-1:0] size;
  input              req_size;
  input  [sz_width-1:0] sizez;
  output             sizelz;


  wire               vd;
  wire   [width-1:0] d;
  wire               lz;
  wire   [sz_width-1:0] size;
  wire               sizelz;

  assign d = z;
  assign lz = ld;
  assign vd = vz;
  assign size = sizez;
  assign sizelz = req_size;

endmodule


//------------------------------------------------------------------
//-- OUTPUT ENTITIES
//------------------------------------------------------------------

module mgc_out_stdreg (d, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  input    [width-1:0] d;
  output   [width-1:0] z;

  wire     [width-1:0] z;

  assign z = d;

endmodule

//------------------------------------------------------------------

module mgc_out_stdreg_en (ld, d, lz, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  input              ld;
  input  [width-1:0] d;
  output             lz;
  output [width-1:0] z;

  wire               lz;
  wire   [width-1:0] z;

  assign z = d;
  assign lz = ld;

endmodule

//------------------------------------------------------------------

module mgc_out_stdreg_wait (ld, vd, d, lz, vz, z);

  parameter integer rscid = 1;
  parameter integer width = 8;

  input              ld;
  output             vd;
  input  [width-1:0] d;
  output             lz;
  input              vz;
  output [width-1:0] z;

  wire               vd;
  wire               lz;
  wire   [width-1:0] z;

  assign z = d;
  assign lz = ld;
  assign vd = vz;

endmodule

//------------------------------------------------------------------

module mgc_out_prereg_en (ld, d, lz, z);

    parameter integer rscid = 1;
    parameter integer width = 8;

    input              ld;
    input  [width-1:0] d;
    output             lz;
    output [width-1:0] z;

    wire               lz;
    wire   [width-1:0] z;

    assign z = d;
    assign lz = ld;

endmodule

//------------------------------------------------------------------
//-- INOUT ENTITIES
//------------------------------------------------------------------

module mgc_inout_stdreg_en (ldin, din, ldout, dout, lzin, lzout, z);

    parameter integer rscid = 1;
    parameter integer width = 8;

    input              ldin;
    output [width-1:0] din;
    input              ldout;
    input  [width-1:0] dout;
    output             lzin;
    output             lzout;
    inout  [width-1:0] z;

    wire   [width-1:0] din;
    wire               lzin;
    wire               lzout;
    wire   [width-1:0] z;

    assign lzin = ldin;
    assign din = ldin ? z : {width{1'bz}};
    assign lzout = ldout;
    assign z = ldout ? dout : {width{1'bz}};

endmodule

//------------------------------------------------------------------
module hid_tribuf( I_SIG, ENABLE, O_SIG);
  parameter integer width = 8;

  input [width-1:0] I_SIG;
  input ENABLE;
  inout [width-1:0] O_SIG;

  assign O_SIG = (ENABLE) ? I_SIG : { width{1'bz}};

endmodule
//------------------------------------------------------------------

module mgc_inout_stdreg_wait (ldin, vdin, din, ldout, vdout, dout, lzin, vzin, lzout, vzout, z);

    parameter integer rscid = 1;
    parameter integer width = 8;

    input              ldin;
    output             vdin;
    output [width-1:0] din;
    input              ldout;
    output             vdout;
    input  [width-1:0] dout;
    output             lzin;
    input              vzin;
    output             lzout;
    input              vzout;
    inout  [width-1:0] z;

    wire               vdin;
    wire   [width-1:0] din;
    wire               vdout;
    wire               lzin;
    wire               lzout;
    wire   [width-1:0] z;
    wire   ldout_and_vzout;

    assign lzin = ldin;
    assign vdin = vzin;
    assign din = ldin ? z : {width{1'bz}};
    assign lzout = ldout;
    assign vdout = vzout ;
    assign ldout_and_vzout = ldout && vzout ;

    hid_tribuf #(width) tb( .I_SIG(dout),
                            .ENABLE(ldout_and_vzout),
                            .O_SIG(z) );

endmodule

//------------------------------------------------------------------

module mgc_inout_buf_wait (clk, en, arst, srst, ldin, vdin, din, ldout, vdout, dout, lzin, vzin, lzout, vzout, z);

    parameter integer rscid   = 0; // resource ID
    parameter integer width   = 8; // fifo width
    parameter         ph_clk  =  1'b1; // clock polarity 1=rising edge, 0=falling edge
    parameter         ph_en   =  1'b1; // clock enable polarity
    parameter         ph_arst =  1'b1; // async reset polarity
    parameter         ph_srst =  1'b1; // sync reset polarity

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ldin;
    output             vdin;
    output [width-1:0] din;
    input              ldout;
    output             vdout;
    input  [width-1:0] dout;
    output             lzin;
    input              vzin;
    output             lzout;
    input              vzout;
    inout  [width-1:0] z;

    wire               lzout_buf;
    wire               vzout_buf;
    wire   [width-1:0] z_buf;
    wire               vdin;
    wire   [width-1:0] din;
    wire               vdout;
    wire               lzin;
    wire               lzout;
    wire   [width-1:0] z;

    assign lzin = ldin;
    assign vdin = vzin;
    assign din = ldin ? z : {width{1'bz}};
    assign lzout = lzout_buf & ~ldin;
    assign vzout_buf = vzout & ~ldin;
    hid_tribuf #(width) tb( .I_SIG(z_buf),
                            .ENABLE((lzout_buf && (!ldin) && vzout) ),
                            .O_SIG(z)  );

    mgc_out_buf_wait
    #(
        .rscid   (rscid),
        .width   (width),
        .ph_clk  (ph_clk),
        .ph_en   (ph_en),
        .ph_arst (ph_arst),
        .ph_srst (ph_srst)
    )
    BUFF
    (
        .clk     (clk),
        .en      (en),
        .arst    (arst),
        .srst    (srst),
        .ld      (ldout),
        .vd      (vdout),
        .d       (dout),
        .lz      (lzout_buf),
        .vz      (vzout_buf),
        .z       (z_buf)
    );


endmodule

module mgc_inout_fifo_wait (clk, en, arst, srst, ldin, vdin, din, ldout, vdout, dout, lzin, vzin, lzout, vzout, z);

    parameter integer rscid   = 0; // resource ID
    parameter integer width   = 8; // fifo width
    parameter integer fifo_sz = 8; // fifo depth
    parameter         ph_clk  = 1'b1;  // clock polarity 1=rising edge, 0=falling edge
    parameter         ph_en   = 1'b1;  // clock enable polarity
    parameter         ph_arst = 1'b1;  // async reset polarity
    parameter         ph_srst = 1'b1;  // sync reset polarity
    parameter integer ph_log2 = 3;     // log2(fifo_sz)
    parameter integer fifocg  = 0;     // fifocg

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ldin;
    output             vdin;
    output [width-1:0] din;
    input              ldout;
    output             vdout;
    input  [width-1:0] dout;
    output             lzin;
    input              vzin;
    output             lzout;
    input              vzout;
    inout  [width-1:0] z;

    wire               lzout_buf;
    wire               vzout_buf;
    wire   [width-1:0] z_buf;
    wire               comb;
    wire               vdin;
    wire   [width-1:0] din;
    wire               vdout;
    wire               lzin;
    wire               lzout;
    wire   [width-1:0] z;

    assign lzin = ldin;
    assign vdin = vzin;
    assign din = ldin ? z : {width{1'bz}};
    assign lzout = lzout_buf & ~ldin;
    assign vzout_buf = vzout & ~ldin;
    assign comb = (lzout_buf && (!ldin) && vzout);

    hid_tribuf #(width) tb2( .I_SIG(z_buf), .ENABLE(comb), .O_SIG(z)  );

    mgc_out_fifo_wait
    #(
        .rscid   (rscid),
        .width   (width),
        .fifo_sz (fifo_sz),
        .ph_clk  (ph_clk),
        .ph_en   (ph_en),
        .ph_arst (ph_arst),
        .ph_srst (ph_srst),
        .ph_log2 (ph_log2),
        .fifocg  (fifocg)
    )
    FIFO
    (
        .clk   (clk),
        .en      (en),
        .arst    (arst),
        .srst    (srst),
        .ld      (ldout),
        .vd      (vdout),
        .d       (dout),
        .lz      (lzout_buf),
        .vz      (vzout_buf),
        .z       (z_buf)
    );

endmodule

//------------------------------------------------------------------
//-- I/O SYNCHRONIZATION ENTITIES
//------------------------------------------------------------------

module mgc_io_sync (ld, lz);

    input  ld;
    output lz;

    assign lz = ld;

endmodule

module mgc_bsync_rdy (rd, rz);

    parameter integer rscid   = 0; // resource ID
    parameter ready = 1;
    parameter valid = 0;

    input  rd;
    output rz;

    wire   rz;

    assign rz = rd;

endmodule

module mgc_bsync_vld (vd, vz);

    parameter integer rscid   = 0; // resource ID
    parameter ready = 0;
    parameter valid = 1;

    output vd;
    input  vz;

    wire   vd;

    assign vd = vz;

endmodule

module mgc_bsync_rv (rd, vd, rz, vz);

    parameter integer rscid   = 0; // resource ID
    parameter ready = 1;
    parameter valid = 1;

    input  rd;
    output vd;
    output rz;
    input  vz;

    wire   vd;
    wire   rz;

    assign rz = rd;
    assign vd = vz;

endmodule

//------------------------------------------------------------------

module mgc_sync (ldin, vdin, ldout, vdout);

  input  ldin;
  output vdin;
  input  ldout;
  output vdout;

  wire   vdin;
  wire   vdout;

  assign vdin = ldout;
  assign vdout = ldin;

endmodule




//------> ./rtl_mgc_ioport_v2001.v 
//------------------------------------------------------------------

module mgc_out_reg_pos (clk, en, arst, srst, ld, d, lz, z);

    parameter integer rscid   = 1;
    parameter integer width   = 8;
    parameter         ph_en   =  1'b1;
    parameter         ph_arst =  1'b1;
    parameter         ph_srst =  1'b1;

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ld;
    input  [width-1:0] d;
    output             lz;
    output [width-1:0] z;

    reg                lz;
    reg    [width-1:0] z;

    generate
    if (ph_arst == 1'b0)
    begin: NEG_ARST
        always @(posedge clk or negedge arst)
        if (arst == 1'b0)
        begin: B1
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (srst == ph_srst)
        begin: B2
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (en == ph_en)
        begin: B3
            lz <= ld;
            z  <= (ld) ? d : z;
        end
    end
    else
    begin: POS_ARST
        always @(posedge clk or posedge arst)
        if (arst == 1'b1)
        begin: B1
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (srst == ph_srst)
        begin: B2
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (en == ph_en)
        begin: B3
            lz <= ld;
            z  <= (ld) ? d : z;
        end
    end
    endgenerate

endmodule

//------------------------------------------------------------------

module mgc_out_reg_neg (clk, en, arst, srst, ld, d, lz, z);

    parameter integer rscid   = 1;
    parameter integer width   = 8;
    parameter         ph_en   =  1'b1;
    parameter         ph_arst =  1'b1;
    parameter         ph_srst =  1'b1;

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ld;
    input  [width-1:0] d;
    output             lz;
    output [width-1:0] z;

    reg                lz;
    reg    [width-1:0] z;

    generate
    if (ph_arst == 1'b0)
    begin: NEG_ARST
        always @(negedge clk or negedge arst)
        if (arst == 1'b0)
        begin: B1
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (srst == ph_srst)
        begin: B2
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (en == ph_en)
        begin: B3
            lz <= ld;
            z  <= (ld) ? d : z;
        end
    end
    else
    begin: POS_ARST
        always @(negedge clk or posedge arst)
        if (arst == 1'b1)
        begin: B1
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (srst == ph_srst)
        begin: B2
            lz <= 1'b0;
            z  <= {width{1'b0}};
        end
        else if (en == ph_en)
        begin: B3
            lz <= ld;
            z  <= (ld) ? d : z;
        end
    end
    endgenerate

endmodule

//------------------------------------------------------------------

module mgc_out_reg (clk, en, arst, srst, ld, d, lz, z); // Not Supported

    parameter integer rscid   = 1;
    parameter integer width   = 8;
    parameter         ph_clk  =  1'b1;
    parameter         ph_en   =  1'b1;
    parameter         ph_arst =  1'b1;
    parameter         ph_srst =  1'b1;

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ld;
    input  [width-1:0] d;
    output             lz;
    output [width-1:0] z;


    generate
    if (ph_clk == 1'b0)
    begin: NEG_EDGE

        mgc_out_reg_neg
        #(
            .rscid   (rscid),
            .width   (width),
            .ph_en   (ph_en),
            .ph_arst (ph_arst),
            .ph_srst (ph_srst)
        )
        mgc_out_reg_neg_inst
        (
            .clk     (clk),
            .en      (en),
            .arst    (arst),
            .srst    (srst),
            .ld      (ld),
            .d       (d),
            .lz      (lz),
            .z       (z)
        );

    end
    else
    begin: POS_EDGE

        mgc_out_reg_pos
        #(
            .rscid   (rscid),
            .width   (width),
            .ph_en   (ph_en),
            .ph_arst (ph_arst),
            .ph_srst (ph_srst)
        )
        mgc_out_reg_pos_inst
        (
            .clk     (clk),
            .en      (en),
            .arst    (arst),
            .srst    (srst),
            .ld      (ld),
            .d       (d),
            .lz      (lz),
            .z       (z)
        );

    end
    endgenerate

endmodule




//------------------------------------------------------------------

module mgc_out_buf_wait (clk, en, arst, srst, ld, vd, d, vz, lz, z); // Not supported

    parameter integer rscid   = 1;
    parameter integer width   = 8;
    parameter         ph_clk  =  1'b1;
    parameter         ph_en   =  1'b1;
    parameter         ph_arst =  1'b1;
    parameter         ph_srst =  1'b1;

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ld;
    output             vd;
    input  [width-1:0] d;
    output             lz;
    input              vz;
    output [width-1:0] z;

    wire               filled;
    wire               filled_next;
    wire   [width-1:0] abuf;
    wire               lbuf;


    assign filled_next = (filled & (~vz)) | (filled & ld) | (ld & (~vz));

    assign lbuf = ld & ~(filled ^ vz);

    assign vd = vz | ~filled;

    assign lz = ld | filled;

    assign z = (filled) ? abuf : d;

    wire dummy;
    wire dummy_bufreg_lz;

    // Output registers:
    mgc_out_reg
    #(
        .rscid   (rscid),
        .width   (1'b1),
        .ph_clk  (ph_clk),
        .ph_en   (ph_en),
        .ph_arst (ph_arst),
        .ph_srst (ph_srst)
    )
    STATREG
    (
        .clk     (clk),
        .en      (en),
        .arst    (arst),
        .srst    (srst),
        .ld      (filled_next),
        .d       (1'b0),       // input d is unused
        .lz      (filled),
        .z       (dummy)            // output z is unused
    );

    mgc_out_reg
    #(
        .rscid   (rscid),
        .width   (width),
        .ph_clk  (ph_clk),
        .ph_en   (ph_en),
        .ph_arst (ph_arst),
        .ph_srst (ph_srst)
    )
    BUFREG
    (
        .clk     (clk),
        .en      (en),
        .arst    (arst),
        .srst    (srst),
        .ld      (lbuf),
        .d       (d),
        .lz      (dummy_bufreg_lz),
        .z       (abuf)
    );

endmodule

//------------------------------------------------------------------

module mgc_out_fifo_wait (clk, en, arst, srst, ld, vd, d, lz, vz,  z);

    parameter integer rscid   = 0; // resource ID
    parameter integer width   = 8; // fifo width
    parameter integer fifo_sz = 8; // fifo depth
    parameter         ph_clk  = 1'b1; // clock polarity 1=rising edge, 0=falling edge
    parameter         ph_en   = 1'b1; // clock enable polarity
    parameter         ph_arst = 1'b1; // async reset polarity
    parameter         ph_srst = 1'b1; // sync reset polarity
    parameter integer ph_log2 = 3; // log2(fifo_sz)
    parameter integer fifocg  = 0; // fifocg


    input                 clk;
    input                 en;
    input                 arst;
    input                 srst;
    input                 ld;    // load data
    output                vd;    // fifo full active low
    input     [width-1:0] d;
    output                lz;    // fifo ready to send
    input                 vz;    // dest ready for data
    output    [width-1:0] z;

    wire    [31:0]      size;


      // Output registers:
 mgc_out_fifo_wait_core#(
        .rscid   (rscid),
        .width   (width),
        .sz_width (32),
        .fifo_sz (fifo_sz),
        .ph_clk  (ph_clk),
        .ph_en   (ph_en),
        .ph_arst (ph_arst),
        .ph_srst (ph_srst),
        .ph_log2 (ph_log2),
        .fifocg  (fifocg)
        ) CORE (
        .clk (clk),
        .en (en),
        .arst (arst),
        .srst (srst),
        .ld (ld),
        .vd (vd),
        .d (d),
        .lz (lz),
        .vz (vz),
        .z (z),
        .size (size)
        );

endmodule



module mgc_out_fifo_wait_core (clk, en, arst, srst, ld, vd, d, lz, vz,  z, size);

    parameter integer rscid   = 0; // resource ID
    parameter integer width   = 8; // fifo width
    parameter integer sz_width = 8; // size of port for elements in fifo
    parameter integer fifo_sz = 8; // fifo depth
    parameter         ph_clk  =  1'b1; // clock polarity 1=rising edge, 0=falling edge
    parameter         ph_en   =  1'b1; // clock enable polarity
    parameter         ph_arst =  1'b1; // async reset polarity
    parameter         ph_srst =  1'b1; // sync reset polarity
    parameter integer ph_log2 = 3; // log2(fifo_sz)
    parameter integer fifocg  = 0; // fifocg

   localparam integer  fifo_b = width * fifo_sz;

    input                 clk;
    input                 en;
    input                 arst;
    input                 srst;
    input                 ld;    // load data
    output                vd;    // fifo full active low
    input     [width-1:0] d;
    output                lz;    // fifo ready to send
    input                 vz;    // dest ready for data
    output    [width-1:0] z;
    output    [sz_width-1:0]      size;

    reg      [( (fifo_sz > 0) ? fifo_sz : 1)-1:0] stat_pre;
    wire     [( (fifo_sz > 0) ? fifo_sz : 1)-1:0] stat;
    reg      [( (fifo_b > 0) ? fifo_b : 1)-1:0] buff_pre;
    wire     [( (fifo_b > 0) ? fifo_b : 1)-1:0] buff;
    reg      [( (fifo_sz > 0) ? fifo_sz : 1)-1:0] en_l;

    reg       [width-1:0] buff_nxt;

    reg                   stat_nxt;
    reg                   stat_before;
    reg                   stat_after;
    reg                   en_l_var;

    integer               i;
    genvar                eni;

    integer               count;
    integer               count_t;
    integer               n_elem;
// pragma translate_off
    integer               peak;
// pragma translate_on

    wire dummy_statreg_lz;
    wire dummy_bufreg_lz;

    generate
    if ( fifo_sz > 0 )
    begin: FIFO_REG
      assign vd = vz | ~stat[0];
      assign lz = ld | stat[fifo_sz-1];
      assign size = (count - (vz && stat[fifo_sz-1])) + ld;
      assign z = (stat[fifo_sz-1]) ? buff[fifo_b-1:width*(fifo_sz-1)] : d;

      always @(*)
      begin: FIFOPROC
        n_elem = 0;
        for (i = fifo_sz-1; i >= 0; i = i - 1)
        begin
          if (i != 0)
            stat_before = stat[i-1];
          else
            stat_before = 1'b0;

          if (i != (fifo_sz-1))
            stat_after = stat[i+1];
          else
            stat_after = 1'b1;

          stat_nxt = stat_after &
                    (stat_before | (stat[i] & (~vz)) | (stat[i] & ld) | (ld & (~vz)));

          stat_pre[i] = stat_nxt;
          en_l_var = 1'b1;
          if (!stat_nxt)
            begin
              buff_nxt = {width{1'bx}};
              en_l_var = 1'b0;
            end
          else if (vz && stat_before)
            buff_nxt[0+:width] = buff[width*(i-1)+:width];
          else if (ld && !((vz && stat_before) || ((!vz) && stat[i])))
            buff_nxt = d;
          else
            begin
              if (fifocg == 0)
                buff_nxt[0+:width] = buff[width*i+:width];
              else
                buff_nxt = {width{1'bx}};
              en_l_var = 1'b0;
            end

          if (ph_en != 0)
            en_l[i] = en & en_l_var;
          else
            en_l[i] = en | ~en_l_var;

          buff_pre[width*i+:width] = buff_nxt[0+:width];

          if ((stat_after == 1'b1) && (stat[i] == 1'b0))
            n_elem = (fifo_sz - 1) - i;
        end

        if ( stat[fifo_sz-1] == 1'b0 )
          count_t = 0;
        else if ( stat[0] == 1'b1 )
          count_t = fifo_sz;
        else
          count_t = n_elem;
        count = count_t;
// pragma translate_off
        if ( peak < count )
          peak = count;
// pragma translate_on
      end

      if (fifocg == 0)
      begin: NOCGFIFO
        // Output registers:
        mgc_out_reg
        #(
            .rscid   (rscid),
            .width   (fifo_sz),
            .ph_clk  (ph_clk),
            .ph_en   (ph_en),
            .ph_arst (ph_arst),
            .ph_srst (ph_srst)
        )
        STATREG
        (
            .clk     (clk),
            .en      (en),
            .arst    (arst),
            .srst    (srst),
            .ld      (1'b1),
            .d       (stat_pre),
            .lz      (dummy_statreg_lz),
            .z       (stat)
        );
        mgc_out_reg
        #(
            .rscid   (rscid),
            .width   (fifo_b),
            .ph_clk  (ph_clk),
            .ph_en   (ph_en),
            .ph_arst (ph_arst),
            .ph_srst (ph_srst)
        )
        BUFREG
        (
            .clk     (clk),
            .en      (en),
            .arst    (arst),
            .srst    (srst),
            .ld      (1'b1),
            .d       (buff_pre),
            .lz      (dummy_bufreg_lz),
            .z       (buff)
        );
      end
      else
      begin: CGFIFO
        // Output registers:
        mgc_out_reg
        #(
          .rscid   (rscid),
          .width   (fifo_sz),
          .ph_clk  (ph_clk),
          .ph_en   (ph_en),
          .ph_arst (ph_arst),
          .ph_srst (ph_srst)
        )
        STATREG
        (
          .clk     (clk),
          .en      (en),
          .arst    (arst),
          .srst    (srst),
          .ld      (1'b1),
          .d       (stat_pre),
          .lz      (dummy_statreg_lz),
          .z       (stat)
        );
        for (eni = fifo_sz-1; eni >= 0; eni = eni - 1)
        begin: fifocgGEN2
          mgc_out_reg
          #(
            .rscid   (rscid),
            .width   (width),
            .ph_clk  (ph_clk),
            .ph_en   (ph_en),
            .ph_arst (ph_arst),
            .ph_srst (ph_srst)
          )
          BUFREG
          (
            .clk     (clk),
            .en      (en_l[eni]),
            .arst    (arst),
            .srst    (srst),
            .ld      (1'b1),
            .d       (buff_pre[width*eni+:width]),
            .lz      (dummy_bufreg_lz),
            .z       (buff[width*eni+:width])
          );
        end
      end
    end
    else
    begin: FEED_THRU
      assign vd = vz;
      assign lz = ld;
      assign z = d;
      assign size = ld && !vz;
    end
    endgenerate

endmodule

//------------------------------------------------------------------
//-- PIPE ENTITIES
//------------------------------------------------------------------
/*
 *
 *             _______________________________________________
 * WRITER    |                                               |          READER
 *           |           MGC_PIPE                            |
 *           |           __________________________          |
 *        --<| vdout  --<| vd ---------------  vz<|-----ldin<|---
 *           |           |      FIFO              |          |
 *        ---|>ldout  ---|>ld ---------------- lz |> ---vdin |>--
 *        ---|>dout -----|>d  ---------------- dz |> ----din |>--
 *           |           |________________________|          |
 *           |_______________________________________________|
 */
// two clock pipe
module mgc_pipe (clk, en, arst, srst, ldin, vdin, din, ldout, vdout, dout, size, req_size);

    parameter integer rscid   = 0; // resource ID
    parameter integer width   = 8; // fifo width
    parameter integer sz_width = 8; // width of size of elements in fifo
    parameter integer fifo_sz = 8; // fifo depth
    parameter integer log2_sz = 3; // log2(fifo_sz)
    parameter         ph_clk  = 1'b1;  // clock polarity 1=rising edge, 0=falling edge
    parameter         ph_en   = 1'b1;  // clock enable polarity
    parameter         ph_arst = 1'b1;  // async reset polarity
    parameter         ph_srst = 1'b1;  // sync reset polarity
    parameter integer fifocg  = 0; // fifocg

    input              clk;
    input              en;
    input              arst;
    input              srst;
    input              ldin;
    output             vdin;
    output [width-1:0] din;
    input              ldout;
    output             vdout;
    input  [width-1:0] dout;
    output [sz_width-1:0]      size;
    input              req_size;


    mgc_out_fifo_wait_core
    #(
        .rscid    (rscid),
        .width    (width),
        .sz_width (sz_width),
        .fifo_sz  (fifo_sz),
        .ph_clk   (ph_clk),
        .ph_en    (ph_en),
        .ph_arst  (ph_arst),
        .ph_srst  (ph_srst),
        .ph_log2  (log2_sz),
        .fifocg   (fifocg)
    )
    FIFO
    (
        .clk     (clk),
        .en      (en),
        .arst    (arst),
        .srst    (srst),
        .ld      (ldout),
        .vd      (vdout),
        .d       (dout),
        .lz      (vdin),
        .vz      (ldin),
        .z       (din),
        .size    (size)
    );

endmodule


//------> ./rtl.v 
// ----------------------------------------------------------------------
//  HLS HDL:        Verilog Netlister
//  HLS Version:    2010a.198 Production Release
//  HLS Date:       Tue Nov  2 16:03:18 PST 2010
// 
//  Generated by:   Valentin@THINKPAD
//  Generated date: Thu May 05 22:56:55 2016
// ----------------------------------------------------------------------

// 
// ------------------------------------------------------------------
//  Design Unit:    vga_mouse_square_core
// ------------------------------------------------------------------


module vga_mouse_square_core (
  clk, en, arst_n, vga_xy_rsc_mgc_in_wire_d, video_in_rsc_mgc_in_wire_d, video_out_rsc_mgc_out_stdreg_d,
      levels_in_rsc_mgc_in_wire_d, levels_out_rsc_mgc_inout_stdreg_en_ldout, levels_out_rsc_mgc_inout_stdreg_en_dout,
      last_xy_input_rsc_mgc_in_wire_d, last_xy_output_rsc_mgc_out_stdreg_d, onTrack_input_rsc_mgc_in_wire_d,
      onTrack_output_rsc_mgc_out_stdreg_d
);
  input clk;
  input en;
  input arst_n;
  input [19:0] vga_xy_rsc_mgc_in_wire_d;
  input [29:0] video_in_rsc_mgc_in_wire_d;
  output [29:0] video_out_rsc_mgc_out_stdreg_d;
  reg [29:0] video_out_rsc_mgc_out_stdreg_d;
  input [4:0] levels_in_rsc_mgc_in_wire_d;
  output levels_out_rsc_mgc_inout_stdreg_en_ldout;
  reg levels_out_rsc_mgc_inout_stdreg_en_ldout;
  output [4:0] levels_out_rsc_mgc_inout_stdreg_en_dout;
  reg [4:0] levels_out_rsc_mgc_inout_stdreg_en_dout;
  input [19:0] last_xy_input_rsc_mgc_in_wire_d;
  output [19:0] last_xy_output_rsc_mgc_out_stdreg_d;
  reg [19:0] last_xy_output_rsc_mgc_out_stdreg_d;
  input onTrack_input_rsc_mgc_in_wire_d;
  output onTrack_output_rsc_mgc_out_stdreg_d;
  reg onTrack_output_rsc_mgc_out_stdreg_d;


  // Interconnect Declarations
  reg [9:0] o_red_lpi;
  reg [9:0] o_green_lpi;
  reg o_blue_1_lpi;
  reg exit_for_lpi;
  reg [19:0] io_read_vga_xy_rsc_d_cse_lpi_dfm;
  reg [4:0] C1_5_dfmergedata_lpi_dfm_1;
  reg [19:0] io_read_video_in_rsc_d_cse_sg1_lpi_dfm;
  reg [1:0] for_i_1_sva_1;
  wire [1:0] mgc_isOnSegment_acc_tmp_sva_1;
  wire [4:0] C1_5_dfmergedata_lpi_dfm_2;
  wire [6:0] for_aif_1_slc_points_y_psp_sva_1;
  wire [3:0] for_if_1_slc_points_x_rom_1_psp_sva_1;
  wire [1:0] for_if_1_slc_points_x_rom_1_1_psp_sva_1;
  wire or_tmp_1;
  wire [1:0] for_i_1_lpi_dfm;
  wire [5:0] for_aif_3_acc_sdt_sva;
  wire [19:0] io_read_vga_xy_rsc_d_cse_lpi_dfm_1;
  wire [19:0] io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1;
  wire [1:0] for_acc_itm;
  wire exit_isOnSegment_lpi_dfm;
  wire [9:0] o_green_lpi_dfm_2;
  wire [9:0] o_red_lpi_dfm_1;
  wire o_blue_1_lpi_dfm_1;
  wire [9:0] o_green_lpi_dfm_1;
  wire and_cse;
  wire [1:0] for_i_1_sva_2;
  wire for_land_lpi_dfm;
  wire [7:0] mgc_isOnSegment_if_acc_18_itm;
  wire mgc_isOnSegment_and_1_itm;

  wire[3:0] if_2_if_mux_nl;
  wire[1:0] if_2_if_mux_1_nl;
  wire[3:0] if_2_if_mux_2_nl;
  wire[1:0] if_2_if_mux_3_nl;
  assign io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1 = MUX_v_20_2_1({io_read_video_in_rsc_d_cse_sg1_lpi_dfm
      , (video_in_rsc_mgc_in_wire_d[29:10])}, exit_for_lpi);
  assign for_acc_itm = for_i_1_sva_2 + 2'b1;
  assign for_i_1_lpi_dfm = for_i_1_sva_1 & (signext_2_1(~ exit_for_lpi));
  assign C1_5_dfmergedata_lpi_dfm_2 = MUX1HOT_v_5_3_1({C1_5_dfmergedata_lpi_dfm_1
      , 5'b1 , levels_in_rsc_mgc_in_wire_d}, {(~ exit_for_lpi) , ((~ or_tmp_1) &
      exit_for_lpi) , (or_tmp_1 & exit_for_lpi)});
  assign for_aif_3_acc_sdt_sva = conv_u2s_5_6(C1_5_dfmergedata_lpi_dfm_2) + 6'b111111;
  assign io_read_vga_xy_rsc_d_cse_lpi_dfm_1 = MUX_v_20_2_1({io_read_vga_xy_rsc_d_cse_lpi_dfm
      , vga_xy_rsc_mgc_in_wire_d}, exit_for_lpi);
  assign for_aif_1_slc_points_y_psp_sva_1 = MUX_v_7_4_1({7'b110010 , 7'b1001011 ,
      7'b1100100 , 7'bX}, for_i_1_lpi_dfm);
  assign for_if_1_slc_points_x_rom_1_psp_sva_1 = MUX_v_4_4_1({4'b11 , 4'b1001 , 4'b110
      , 4'bX}, for_i_1_lpi_dfm);
  assign for_if_1_slc_points_x_rom_1_1_psp_sva_1 = MUX_v_2_4_1({2'b1 , 2'b11 , 2'b10
      , 2'bX}, for_i_1_lpi_dfm);
  assign exit_isOnSegment_lpi_dfm = (mgc_isOnSegment_if_acc_18_itm[7]) | (mgc_isOnSegment_if_acc_18_itm[6])
      | (mgc_isOnSegment_if_acc_18_itm[5]) | (mgc_isOnSegment_if_acc_18_itm[4]) |
      (mgc_isOnSegment_if_acc_18_itm[3]) | (mgc_isOnSegment_if_acc_18_itm[2]) | (mgc_isOnSegment_if_acc_18_itm[1]);
  assign o_green_lpi_dfm_2 = o_green_lpi_dfm_1 & (signext_10_1(~ exit_isOnSegment_lpi_dfm));
  assign o_red_lpi_dfm_1 = ~((~(o_red_lpi | ({{9{exit_for_lpi}}, exit_for_lpi})))
      | ({{9{for_land_lpi_dfm}}, for_land_lpi_dfm}));
  assign o_blue_1_lpi_dfm_1 = ~((~(o_blue_1_lpi | exit_for_lpi)) | for_land_lpi_dfm);
  assign o_green_lpi_dfm_1 = MUX_v_10_2_1({(o_green_lpi | ({{9{exit_for_lpi}}, exit_for_lpi}))
      , (io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1[9:0])}, for_land_lpi_dfm);
  assign if_2_if_mux_nl = MUX_v_4_4_1({4'b11 , 4'b1001 , 4'b110 , 4'bX}, mgc_isOnSegment_acc_tmp_sva_1);
  assign if_2_if_mux_1_nl = MUX_v_2_4_1({2'b1 , 2'b11 , 2'b10 , 2'bX}, mgc_isOnSegment_acc_tmp_sva_1);
  assign if_2_if_mux_2_nl = MUX_v_4_4_1({4'b1100 , 4'b110 , 4'b1001 , 4'bX}, C1_5_dfmergedata_lpi_dfm_2[1:0]);
  assign if_2_if_mux_3_nl = MUX_v_2_4_1({2'b10 , 2'b0 , 2'b1 , 2'bX}, C1_5_dfmergedata_lpi_dfm_2[1:0]);
  assign mgc_isOnSegment_if_acc_18_itm = ({(if_2_if_mux_nl) , 1'b0 , (if_2_if_mux_1_nl)
      , 1'b1}) + ({(if_2_if_mux_2_nl) , 1'b1 , (if_2_if_mux_3_nl) , 1'b1});
  assign mgc_isOnSegment_acc_tmp_sva_1 = (C1_5_dfmergedata_lpi_dfm_2[1:0]) + 2'b11;
  assign and_cse = exit_isOnSegment_lpi_dfm & onTrack_input_rsc_mgc_in_wire_d;
  assign for_i_1_sva_2 = for_i_1_lpi_dfm + 2'b1;
  assign for_land_lpi_dfm = ((((for_i_1_lpi_dfm) == (C1_5_dfmergedata_lpi_dfm_2[1:0]))
      & (~((C1_5_dfmergedata_lpi_dfm_2[4]) | (C1_5_dfmergedata_lpi_dfm_2[3]) | (C1_5_dfmergedata_lpi_dfm_2[2]))))
      | (((for_i_1_lpi_dfm) == (for_aif_3_acc_sdt_sva[1:0])) & (~((for_aif_3_acc_sdt_sva[5])
      | (for_aif_3_acc_sdt_sva[4]) | (for_aif_3_acc_sdt_sva[3]) | (for_aif_3_acc_sdt_sva[2])))))
      & (~((readslicef_10_1_9((({2'b10 , for_aif_1_slc_points_y_psp_sva_1 , 1'b1})
      + conv_u2u_9_10({(~ (io_read_vga_xy_rsc_d_cse_lpi_dfm_1[19:12])) , 1'b1}))))
      | (readslicef_9_1_8(((readslicef_11_9_2((conv_u2u_10_11(io_read_vga_xy_rsc_d_cse_lpi_dfm_1[19:10])
      + conv_u2u_10_11({1'b1 , (~ for_aif_1_slc_points_y_psp_sva_1) , 2'b11}))))
      + 9'b100000001))) | (readslicef_10_1_9((({1'b1 , for_if_1_slc_points_x_rom_1_psp_sva_1
      , 1'b0 , for_if_1_slc_points_x_rom_1_1_psp_sva_1 , 2'b1}) + conv_u2u_9_10({(~
      (io_read_vga_xy_rsc_d_cse_lpi_dfm_1[9:2])) , 1'b1})))) | (readslicef_9_1_8(((readslicef_11_9_2((conv_u2u_10_11(io_read_vga_xy_rsc_d_cse_lpi_dfm_1[9:0])
      + conv_u2u_10_11({(~ for_if_1_slc_points_x_rom_1_psp_sva_1) , 1'b1 , (~ for_if_1_slc_points_x_rom_1_1_psp_sva_1)
      , 3'b111})))) + 9'b100000001)))));
  assign or_tmp_1 = (levels_in_rsc_mgc_in_wire_d[4]) | (levels_in_rsc_mgc_in_wire_d[3])
      | (levels_in_rsc_mgc_in_wire_d[2]) | (levels_in_rsc_mgc_in_wire_d[1]) | (levels_in_rsc_mgc_in_wire_d[0]);
  assign mgc_isOnSegment_and_1_itm = o_blue_1_lpi_dfm_1 & (~ exit_isOnSegment_lpi_dfm);
  always @(posedge clk or negedge arst_n) begin
    if ( ~ arst_n ) begin
      io_read_video_in_rsc_d_cse_sg1_lpi_dfm <= 20'b0;
      exit_for_lpi <= 1'b1;
      o_green_lpi <= 10'b0;
      C1_5_dfmergedata_lpi_dfm_1 <= 5'b0;
      io_read_vga_xy_rsc_d_cse_lpi_dfm <= 20'b0;
      video_out_rsc_mgc_out_stdreg_d <= 30'b0;
      last_xy_output_rsc_mgc_out_stdreg_d <= 20'b0;
      levels_out_rsc_mgc_inout_stdreg_en_dout <= 5'b0;
      levels_out_rsc_mgc_inout_stdreg_en_ldout <= 1'b0;
      onTrack_output_rsc_mgc_out_stdreg_d <= 1'b0;
      for_i_1_sva_1 <= 2'b0;
      o_blue_1_lpi <= 1'b0;
      o_red_lpi <= 10'b0;
    end
    else begin
      if ( en ) begin
        io_read_video_in_rsc_d_cse_sg1_lpi_dfm <= io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1;
        exit_for_lpi <= ~ (for_acc_itm[1]);
        o_green_lpi <= MUX_v_10_2_1({o_green_lpi_dfm_1 , o_green_lpi_dfm_2}, onTrack_input_rsc_mgc_in_wire_d
            & (~ (for_acc_itm[1])));
        C1_5_dfmergedata_lpi_dfm_1 <= C1_5_dfmergedata_lpi_dfm_2;
        io_read_vga_xy_rsc_d_cse_lpi_dfm <= io_read_vga_xy_rsc_d_cse_lpi_dfm_1;
        video_out_rsc_mgc_out_stdreg_d <= MUX_v_30_2_1({({(MUX_v_10_2_1({o_red_lpi_dfm_1
            , (io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1[19:10])}, and_cse)) , (MUX_v_10_2_1({o_green_lpi_dfm_1
            , o_green_lpi_dfm_2}, onTrack_input_rsc_mgc_in_wire_d)) , (signext_10_1(MUX_s_1_2_1({o_blue_1_lpi_dfm_1
            , mgc_isOnSegment_and_1_itm}, onTrack_input_rsc_mgc_in_wire_d)))}) ,
            video_out_rsc_mgc_out_stdreg_d}, for_acc_itm[1]);
        last_xy_output_rsc_mgc_out_stdreg_d <= MUX_v_20_2_1({last_xy_input_rsc_mgc_in_wire_d
            , last_xy_output_rsc_mgc_out_stdreg_d}, for_acc_itm[1]);
        levels_out_rsc_mgc_inout_stdreg_en_dout <= C1_5_dfmergedata_lpi_dfm_2 + 5'b11111;
        levels_out_rsc_mgc_inout_stdreg_en_ldout <= ~ (for_acc_itm[1]);
        onTrack_output_rsc_mgc_out_stdreg_d <= MUX_s_1_2_1({and_cse , onTrack_output_rsc_mgc_out_stdreg_d},
            for_acc_itm[1]);
        for_i_1_sva_1 <= for_i_1_sva_2;
        o_blue_1_lpi <= MUX_s_1_2_1({(MUX_s_1_2_1({o_blue_1_lpi_dfm_1 , mgc_isOnSegment_and_1_itm},
            onTrack_input_rsc_mgc_in_wire_d)) , o_blue_1_lpi_dfm_1}, for_acc_itm[1]);
        o_red_lpi <= MUX_v_10_2_1({o_red_lpi_dfm_1 , (io_read_video_in_rsc_d_cse_sg1_lpi_dfm_1[19:10])},
            exit_isOnSegment_lpi_dfm & onTrack_input_rsc_mgc_in_wire_d & (~ (for_acc_itm[1])));
      end
    end
  end

  function [19:0] MUX_v_20_2_1;
    input [39:0] inputs;
    input [0:0] sel;
    reg [19:0] result;
    reg [39:0] or_inputs;
  begin
    case (sel)
      1'b0 : begin
        result = inputs[39:20];
      end
      1'b1 : begin
        result = inputs[19:0];
      end
      default : begin
        result = resolve_20_2(inputs);
      end
    endcase
    MUX_v_20_2_1 = result;
  end
  endfunction


  function [1:0] signext_2_1;
    input [0:0] vector;
  begin
    signext_2_1= {{1{vector[0]}}, vector};
  end
  endfunction


  function [4:0] MUX1HOT_v_5_3_1;
    input [14:0] inputs;
    input [2:0] sel;
    reg [4:0] result;
    reg [14:0] or_inputs;
    integer i;
  begin
    result = inputs[0+:5] & {5{sel[0]}};
    for( i = 1; i < 3; i = i + 1 )
      result = result | (inputs[i*5+:5] & {5{sel[i]}});
    MUX1HOT_v_5_3_1 = result;
  end
  endfunction


  function [6:0] MUX_v_7_4_1;
    input [27:0] inputs;
    input [1:0] sel;
    reg [6:0] result;
    reg [27:0] or_inputs;
  begin
    case (sel)
      2'b00 : begin
        result = inputs[27:21];
      end
      2'b01 : begin
        result = inputs[20:14];
      end
      2'b10 : begin
        result = inputs[13:7];
      end
      2'b11 : begin
        result = inputs[6:0];
      end
      default : begin
        result = resolve_7_4(inputs);
      end
    endcase
    MUX_v_7_4_1 = result;
  end
  endfunction


  function [3:0] MUX_v_4_4_1;
    input [15:0] inputs;
    input [1:0] sel;
    reg [3:0] result;
    reg [15:0] or_inputs;
  begin
    case (sel)
      2'b00 : begin
        result = inputs[15:12];
      end
      2'b01 : begin
        result = inputs[11:8];
      end
      2'b10 : begin
        result = inputs[7:4];
      end
      2'b11 : begin
        result = inputs[3:0];
      end
      default : begin
        result = resolve_4_4(inputs);
      end
    endcase
    MUX_v_4_4_1 = result;
  end
  endfunction


  function [1:0] MUX_v_2_4_1;
    input [7:0] inputs;
    input [1:0] sel;
    reg [1:0] result;
    reg [7:0] or_inputs;
  begin
    case (sel)
      2'b00 : begin
        result = inputs[7:6];
      end
      2'b01 : begin
        result = inputs[5:4];
      end
      2'b10 : begin
        result = inputs[3:2];
      end
      2'b11 : begin
        result = inputs[1:0];
      end
      default : begin
        result = resolve_2_4(inputs);
      end
    endcase
    MUX_v_2_4_1 = result;
  end
  endfunction


  function [9:0] signext_10_1;
    input [0:0] vector;
  begin
    signext_10_1= {{9{vector[0]}}, vector};
  end
  endfunction


  function [9:0] MUX_v_10_2_1;
    input [19:0] inputs;
    input [0:0] sel;
    reg [9:0] result;
    reg [19:0] or_inputs;
  begin
    case (sel)
      1'b0 : begin
        result = inputs[19:10];
      end
      1'b1 : begin
        result = inputs[9:0];
      end
      default : begin
        result = resolve_10_2(inputs);
      end
    endcase
    MUX_v_10_2_1 = result;
  end
  endfunction


  function [0:0] readslicef_10_1_9;
    input [9:0] vector;
    reg [9:0] tmp;
  begin
    tmp = vector >> 9;
    readslicef_10_1_9 = tmp[0:0];
  end
  endfunction


  function [0:0] readslicef_9_1_8;
    input [8:0] vector;
    reg [8:0] tmp;
  begin
    tmp = vector >> 8;
    readslicef_9_1_8 = tmp[0:0];
  end
  endfunction


  function [8:0] readslicef_11_9_2;
    input [10:0] vector;
    reg [10:0] tmp;
  begin
    tmp = vector >> 2;
    readslicef_11_9_2 = tmp[8:0];
  end
  endfunction


  function [29:0] MUX_v_30_2_1;
    input [59:0] inputs;
    input [0:0] sel;
    reg [29:0] result;
    reg [59:0] or_inputs;
  begin
    case (sel)
      1'b0 : begin
        result = inputs[59:30];
      end
      1'b1 : begin
        result = inputs[29:0];
      end
      default : begin
        result = resolve_30_2(inputs);
      end
    endcase
    MUX_v_30_2_1 = result;
  end
  endfunction


  function [0:0] MUX_s_1_2_1;
    input [1:0] inputs;
    input [0:0] sel;
    reg [0:0] result;
    reg [1:0] or_inputs;
  begin
    case (sel)
      1'b0 : begin
        result = inputs[1:1];
      end
      1'b1 : begin
        result = inputs[0:0];
      end
      default : begin
        result = resolve_1_2(inputs);
      end
    endcase
    MUX_s_1_2_1 = result;
  end
  endfunction


  function [19:0] resolve_20_2;
    input [39:0] inputs;
    integer i, j;
    reg [19:0] rvalue;
    reg [19:0] temp1;
  begin
    resolve_20_2 = 20'bX;
    // pragma translate_off
    rvalue = inputs[19:0];
    for ( i = 1; i < 2; i = i+1 ) begin
      temp1 = inputs[( i  * 20 )-1 -: 20 ];
      rvalue = ((rvalue ~^ temp1) | 20'bX) ~^ rvalue;
    end
    resolve_20_2 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [6:0] resolve_7_4;
    input [27:0] inputs;
    integer i, j;
    reg [6:0] rvalue;
    reg [6:0] temp1;
  begin
    resolve_7_4 = 7'bX;
    // pragma translate_off
    rvalue = inputs[6:0];
    for ( i = 1; i < 4; i = i+1 ) begin
      temp1 = inputs[( i  * 7 )-1 -: 7 ];
      rvalue = ((rvalue ~^ temp1) | 7'bX) ~^ rvalue;
    end
    resolve_7_4 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [3:0] resolve_4_4;
    input [15:0] inputs;
    integer i, j;
    reg [3:0] rvalue;
    reg [3:0] temp1;
  begin
    resolve_4_4 = 4'bX;
    // pragma translate_off
    rvalue = inputs[3:0];
    for ( i = 1; i < 4; i = i+1 ) begin
      temp1 = inputs[( i  * 4 )-1 -: 4 ];
      rvalue = ((rvalue ~^ temp1) | 4'bX) ~^ rvalue;
    end
    resolve_4_4 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [1:0] resolve_2_4;
    input [7:0] inputs;
    integer i, j;
    reg [1:0] rvalue;
    reg [1:0] temp1;
  begin
    resolve_2_4 = 2'bX;
    // pragma translate_off
    rvalue = inputs[1:0];
    for ( i = 1; i < 4; i = i+1 ) begin
      temp1 = inputs[( i  * 2 )-1 -: 2 ];
      rvalue = ((rvalue ~^ temp1) | 2'bX) ~^ rvalue;
    end
    resolve_2_4 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [9:0] resolve_10_2;
    input [19:0] inputs;
    integer i, j;
    reg [9:0] rvalue;
    reg [9:0] temp1;
  begin
    resolve_10_2 = 10'bX;
    // pragma translate_off
    rvalue = inputs[9:0];
    for ( i = 1; i < 2; i = i+1 ) begin
      temp1 = inputs[( i  * 10 )-1 -: 10 ];
      rvalue = ((rvalue ~^ temp1) | 10'bX) ~^ rvalue;
    end
    resolve_10_2 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [29:0] resolve_30_2;
    input [59:0] inputs;
    integer i, j;
    reg [29:0] rvalue;
    reg [29:0] temp1;
  begin
    resolve_30_2 = 30'bX;
    // pragma translate_off
    rvalue = inputs[29:0];
    for ( i = 1; i < 2; i = i+1 ) begin
      temp1 = inputs[( i  * 30 )-1 -: 30 ];
      rvalue = ((rvalue ~^ temp1) | 30'bX) ~^ rvalue;
    end
    resolve_30_2 = rvalue;
    // pragma translate_on
  end
  endfunction


  function [0:0] resolve_1_2;
    input [1:0] inputs;
    integer i, j;
    reg [0:0] rvalue;
    reg [0:0] temp1;
  begin
    resolve_1_2 = 1'bX;
    // pragma translate_off
    rvalue = inputs[0:0];
    for ( i = 1; i < 2; i = i+1 ) begin
      temp1 = inputs[( i  * 1 )-1 -: 1 ];
      rvalue = ((rvalue ~^ temp1) | 1'bX) ~^ rvalue;
    end
    resolve_1_2 = rvalue;
    // pragma translate_on
  end
  endfunction


  function signed [5:0] conv_u2s_5_6 ;
    input [4:0]  vector ;
  begin
    conv_u2s_5_6 = {1'b0, vector};
  end
  endfunction


  function  [9:0] conv_u2u_9_10 ;
    input [8:0]  vector ;
  begin
    conv_u2u_9_10 = {1'b0, vector};
  end
  endfunction


  function  [10:0] conv_u2u_10_11 ;
    input [9:0]  vector ;
  begin
    conv_u2u_10_11 = {1'b0, vector};
  end
  endfunction

endmodule

// ------------------------------------------------------------------
//  Design Unit:    vga_mouse_square
//  Generated from file(s):
//    2) $PROJECT_HOME/ip/vga_mouse_square.c
// ------------------------------------------------------------------


module vga_mouse_square (
  vga_xy_rsc_z, video_in_rsc_z, video_out_rsc_z, levels_in_rsc_z, levels_out_rsc_z,
      levels_out_rsc_lzout, levels_out_rsc_lzin, last_xy_input_rsc_z, last_xy_output_rsc_z,
      onTrack_input_rsc_z, onTrack_output_rsc_z, clk, en, arst_n
);
  input [19:0] vga_xy_rsc_z;
  input [29:0] video_in_rsc_z;
  output [29:0] video_out_rsc_z;
  input [4:0] levels_in_rsc_z;
  inout [4:0] levels_out_rsc_z;
  output levels_out_rsc_lzout;
  output levels_out_rsc_lzin;
  input [19:0] last_xy_input_rsc_z;
  output [19:0] last_xy_output_rsc_z;
  input onTrack_input_rsc_z;
  output onTrack_output_rsc_z;
  input clk;
  input en;
  input arst_n;


  // Interconnect Declarations
  wire [19:0] vga_xy_rsc_mgc_in_wire_d;
  wire [29:0] video_in_rsc_mgc_in_wire_d;
  wire [29:0] video_out_rsc_mgc_out_stdreg_d;
  wire [4:0] levels_in_rsc_mgc_in_wire_d;
  wire [4:0] levels_out_rsc_mgc_inout_stdreg_en_din;
  wire levels_out_rsc_mgc_inout_stdreg_en_ldout;
  wire [4:0] levels_out_rsc_mgc_inout_stdreg_en_dout;
  wire [19:0] last_xy_input_rsc_mgc_in_wire_d;
  wire [19:0] last_xy_output_rsc_mgc_out_stdreg_d;
  wire onTrack_input_rsc_mgc_in_wire_d;
  wire onTrack_output_rsc_mgc_out_stdreg_d;

  mgc_in_wire #(.rscid(1),
  .width(20)) vga_xy_rsc_mgc_in_wire (
      .d(vga_xy_rsc_mgc_in_wire_d),
      .z(vga_xy_rsc_z)
    );
  mgc_in_wire #(.rscid(4),
  .width(30)) video_in_rsc_mgc_in_wire (
      .d(video_in_rsc_mgc_in_wire_d),
      .z(video_in_rsc_z)
    );
  mgc_out_stdreg #(.rscid(5),
  .width(30)) video_out_rsc_mgc_out_stdreg (
      .d(video_out_rsc_mgc_out_stdreg_d),
      .z(video_out_rsc_z)
    );
  mgc_in_wire #(.rscid(6),
  .width(5)) levels_in_rsc_mgc_in_wire (
      .d(levels_in_rsc_mgc_in_wire_d),
      .z(levels_in_rsc_z)
    );
  mgc_inout_stdreg_en #(.rscid(7),
  .width(5)) levels_out_rsc_mgc_inout_stdreg_en (
      .ldin(1'b0),
      .din(levels_out_rsc_mgc_inout_stdreg_en_din),
      .ldout(levels_out_rsc_mgc_inout_stdreg_en_ldout),
      .dout(levels_out_rsc_mgc_inout_stdreg_en_dout),
      .lzin(levels_out_rsc_lzin),
      .lzout(levels_out_rsc_lzout),
      .z(levels_out_rsc_z)
    );
  mgc_in_wire #(.rscid(8),
  .width(20)) last_xy_input_rsc_mgc_in_wire (
      .d(last_xy_input_rsc_mgc_in_wire_d),
      .z(last_xy_input_rsc_z)
    );
  mgc_out_stdreg #(.rscid(9),
  .width(20)) last_xy_output_rsc_mgc_out_stdreg (
      .d(last_xy_output_rsc_mgc_out_stdreg_d),
      .z(last_xy_output_rsc_z)
    );
  mgc_in_wire #(.rscid(10),
  .width(1)) onTrack_input_rsc_mgc_in_wire (
      .d(onTrack_input_rsc_mgc_in_wire_d),
      .z(onTrack_input_rsc_z)
    );
  mgc_out_stdreg #(.rscid(11),
  .width(1)) onTrack_output_rsc_mgc_out_stdreg (
      .d(onTrack_output_rsc_mgc_out_stdreg_d),
      .z(onTrack_output_rsc_z)
    );
  vga_mouse_square_core vga_mouse_square_core_inst (
      .clk(clk),
      .en(en),
      .arst_n(arst_n),
      .vga_xy_rsc_mgc_in_wire_d(vga_xy_rsc_mgc_in_wire_d),
      .video_in_rsc_mgc_in_wire_d(video_in_rsc_mgc_in_wire_d),
      .video_out_rsc_mgc_out_stdreg_d(video_out_rsc_mgc_out_stdreg_d),
      .levels_in_rsc_mgc_in_wire_d(levels_in_rsc_mgc_in_wire_d),
      .levels_out_rsc_mgc_inout_stdreg_en_ldout(levels_out_rsc_mgc_inout_stdreg_en_ldout),
      .levels_out_rsc_mgc_inout_stdreg_en_dout(levels_out_rsc_mgc_inout_stdreg_en_dout),
      .last_xy_input_rsc_mgc_in_wire_d(last_xy_input_rsc_mgc_in_wire_d),
      .last_xy_output_rsc_mgc_out_stdreg_d(last_xy_output_rsc_mgc_out_stdreg_d),
      .onTrack_input_rsc_mgc_in_wire_d(onTrack_input_rsc_mgc_in_wire_d),
      .onTrack_output_rsc_mgc_out_stdreg_d(onTrack_output_rsc_mgc_out_stdreg_d)
    );
endmodule



