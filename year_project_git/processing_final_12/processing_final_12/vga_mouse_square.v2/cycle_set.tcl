
# Loop constraints
directive set /vga_mouse_square/core/main CSTEPS_FROM {{. == 2}}

# IO operation constraints
directive set /vga_mouse_square/core/main/io_read(video_in:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_read(vga_xy:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if:io_read(levels_in:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_read(onTrack_input:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if#2:if:io_write(onTrack_output:rsc.d)#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_write(levels_out:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_read(last_xy_input:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_write(last_xy_output:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/io_write(video_out:rsc.d) CSTEPS_FROM {{.. == 1}}

# Real operation constraints
directive set /vga_mouse_square/core/main/for:and CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#5 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux1h CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#7 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:mux CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:mux#8 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc#6 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:mux#9 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc#8 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc#2 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:mux#2 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:acc#9 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:aif#3:acc CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:if#1:equal CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:aif#3:oelse:equal CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:nor#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:nor CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:or#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#9 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:acc#3 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:acc CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/_isOnSegment:acc CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if#2:if:mux CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if#2:if:mux#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if#2:if:mux#2 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/if#2:if:mux#3 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/_isOnSegment:if:acc#18 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/_isOnSegment:and CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/acc#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/mux#1 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/mux#2 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/mux#3 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#11 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#12 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/mux#9 CSTEPS_FROM {{.. == 1}}
directive set /vga_mouse_square/core/main/for:mux#13 CSTEPS_FROM {{.. == 1}}
