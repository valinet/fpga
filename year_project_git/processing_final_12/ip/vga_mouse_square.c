///////////////////////////////////////////////////////////////////////////////////
//  _____                           _       _    _____      _ _                  //
// |_   _|                         (_)     | |  / ____|    | | |                 //
//   | |  _ __ ___  _ __   ___ _ __ _  __ _| | | |     ___ | | | ___  __ _  ___  //
//   | | | '_ ` _ \| '_ \ / _ \ '__| |/ _` | | | |    / _ \| | |/ _ \/ _` |/ _ \ //
//  _| |_| | | | | | |_) |  __/ |  | | (_| | | | |___| (_) | | |  __/ (_| |  __/ //
// |_____|_| |_| |_| .__/ \___|_|  |_|\__,_|_|  \_____\___/|_|_|\___|\__, |\___| //
//                 | |                                                __/ |      //
//                 |_|                                               |___/       //
///////////////////////////////////////////////////////////////////////////////////
//  File:           vga_mouse_square.cpp                                         //
//  Description:    video to vga with mouse pointer - real-time processing       //
//  By:             rad09 (Original design)                                      //
//                  vr1015 (Changes to work with Dottie)                         //
//                  al3415                                                       //
///////////////////////////////////////////////////////////////////////////////////
// this hardware displays a game for bored people to play in the electronics lab //
///////////////////////////////////////////////////////////////////////////////////
// Catapult Project options                                                      //
// Constraint Editor:                                                            //
//  Frequency: 27 MHz                                                            //
//  Top design: vga_mouse_square                                                 //
//  clk>reset sync: disable; reset async: enable; enable: enable                 //
// Architecture Constraint:                                                      //
//  core>main: enable pipeline + loop can be merged                              //
///////////////////////////////////////////////////////////////////////////////////
//   _______________________________________                                     //
//  / Look for my W ;)                      \                                    //
//  \ (No, seriously, don't look...)        /                                    //
//   ---------------------------------------                                     //
//          \   ^__^                                                             //
//           \  (oo)\_______                                                     //
//              (__)\       )\/\                                                 //
//                  ||----w |                                                    //
//                  ||     ||                                                    //
///////////////////////////////////////////////////////////////////////////////////

#include "stdio.h"                            // whatever
#include "ac_int.h"                           // contains prototypes for the Algortihmic Datatype ac_int
#include "ac_fixed.h"                         // contains prototypes for the Algorithmic Datatype ac_fixed (which is a fixed-point type)

#define COLOR_WL          10                  // width of colour (?)
#define PIXEL_WL          (3*COLOR_WL)        // width of colours' bus (?)

#define DOT_SIZE 30                           // the area around a dot that when light source is detected into, it should be interpreted
                                              // as touching the dot
#define DOT_DRAW_AREA 20                       // the area around a dot to actually paint in solid colour

#define MOUSE_BOX_AREA 11

#define LIGHT_SOURCE_SIZE 20                  // the size of the highlighter that shows the light source on screen

#define LEVELS 12                             // number of levels currently available
#define MAXLEVELS 5                           // maximum number of levels which can be stored
                                              // max number of levels is 2 to power of MAXLEVELS

#define THRESHOLD 10                          // see method _isOnSegment(...)

#define WIDTH 770                             // used to compute symetrical of vga_x
                                              // should be 640, but apparently vga_x takes sometimes values bigger than that, so I added
                                              // an offset to it (640 + offset 130 from aprox. 138)
#define HEIGHT 520                            // used for the mouse, same story as width

#define  COORD_WL          10                 // again, whatever

typedef ac_fixed<14, 11, true> acpoint;       // typedef so I can check the number of digits, and floating point position easily
typedef ac_fixed<14, 11, false> acpoint_u;    // same as above, this time unsigned tho, so larger capacity towards relevant positive values

#pragma hls_design top                        // probably indicates that this is the top design to Catapult BLS

// helper struct in which we store the game's levels
struct image_point
{
    ac_int<10, false>  x, y;
};

// helper method which extracts the minimum out of 3 numbers
acpoint_u MIN(acpoint_u r, acpoint_u g, acpoint_u b) 
{
    acpoint_u min;

    if (r < g) {
        if (r < b) {
            min = r;
        }
        else {
            min = b;
        }
    }
    else if (g < b) {
        min = g;
    }
    else {
        min = b;
    }
    return min;
}

// helper method which extracts the maximum out of 3 numbers
acpoint_u MAX(acpoint_u r, acpoint_u g, acpoint_u b) 
{
    acpoint_u max;

    if (r > g) {
        if (r > b) {
            max = r;
        }
        else {
            max = b;
        }
    }
    else if (g > b) {
        max = g;
    }
    else {
        max = b;
    }
    return max;
}

// computers H,S,V values from R,G,B values - returns values by passing by reference
void RGB2HSV(ac_int<10, false> rr, ac_int<10, false> gg, ac_int<10, false> bb, acpoint_u &h, acpoint_u &s, acpoint_u &v) 
{	
    acpoint_u min, max, delta, r, g, b;
    rr = rr << 2;
    rr = rr >> 2;
    gg = gg << 2;
    gg = gg >> 2;
    bb = bb << 2;
    bb = bb >> 2;
    r = rr;
    g = gg;
    b = bb;
    r /= 255;
    g /= 255;
    b /= 255;

    min = MIN(r, g, b);
    max = MAX(r, g, b);

    v = max;

    delta = max - min;

    if (max != 0)
        s = delta / max;		// s
    else {
        // r = g = b = 0		// s = 0, v is undefined
        s = 0;
        h = -1;
        return;
    }
    if (r == max)
        h = (g - b) / delta;		// between yellow & magenta
    else if (g == max)
        h = 2 + (b - r) / delta;	// between cyan & yellow
    else
        h = 4 + (r - g) / delta;	// between magenta & cyan
    h *= 60;				// degrees
    if (h < 0)
        h += 360;
}

// an implementation of absolute value
acpoint absolute(acpoint x)
{
    if (x < 0) return 0 - x;
    else return x;
}

/*
    This function checks if (vga_x, vga_y) is on the line (or in proximity of the line) defined by start and finish. 
    If it is, then it returns 1. In all other cases, it returns a 0.
    Parameter "threshold" is a value that indicates the tolerance of the check - i.e. how far the point can be from the line,
        but actually still be considered on the line (in proximity)
*/
int _isOnSegment(image_point start, image_point finish, ac_int<10, false> vga_x, ac_int<10, false> vga_y, int threshold)
{
    acpoint sfx = start.x - finish.x;
    acpoint sfy = start.y - finish.y;
    if (sfx != 0 && sfy != 0)
    {
 		acpoint m = (sfy / sfx);

		acpoint c = vga_x - start.x + ((acpoint)start.y / m) - ((acpoint)vga_y / m);
        
        if (absolute(c) <= threshold)
            return 1;
    }
    else if (sfx == 0)
    {
        if (start.y < finish.y)
        {
            if ((vga_y >= (start.y - threshold)) && (vga_y <= (finish.y + threshold)))
                return 1;
        }
        else 
        {
            if ((vga_y >= (finish.y - threshold)) && (vga_y <= (start.y + threshold)))
                return 1;
        }
    }
    else if (sfy == 0)
    {
        if (start.x < finish.x)
        {
            if ((vga_x >= (start.x - threshold)) && (vga_x <= (finish.x + threshold)))
                return 1;
        }
        else
        {
            if ((vga_x >= (finish.x - threshold)) && (vga_x <= (start.x + threshold)))
                return 1;
        }
    }
    return 0;
}

void vga_mouse_square(ac_int<(COORD_WL+COORD_WL), false> * vga_xy, ac_int<(COORD_WL+COORD_WL), false> * mouse_xy, ac_int<(8), false> cursor_size, 
    ac_int<PIXEL_WL, false> * video_in, ac_int<PIXEL_WL, false> * video_out, ac_int<MAXLEVELS, false> * levels_in, ac_int<MAXLEVELS, false> * levelz_out,
    ac_int<(COORD_WL+COORD_WL), false> * last_xy_input, ac_int<(COORD_WL+COORD_WL), false> * last_xy_output,
    ac_int<(1), false> * onTrack_input, ac_int<(1), false> * onTrack_output,
    ac_int<(10), false> * lastLight_input, ac_int<(10), false> * lastLight_output,
    ac_int<1, false> * sw_zero, ac_int<1, false> * sw_one, ac_int<1, false> * sw_two, ac_int<1, false> * level_pulse,
    ac_int<1, false> * history)
{
  
    ac_int<10, false> i_red, i_green, i_blue; // current pixel
    ac_int<10, false> o_red, o_green, o_blue; // output pixel
    ac_int<10, false> mouse_x, mouse_y, vga_x, vga_y, last_x, last_y; // mouse and screen coordinates

    i_red = (*video_in).slc<COLOR_WL>(20);
    i_green = (*video_in).slc<COLOR_WL>(10);
    i_blue = (*video_in).slc<COLOR_WL>(0);

    // extract mouse X-Y coordinates -- NOT NEEDED
    mouse_x = (*mouse_xy).slc<COORD_WL>(0);
    mouse_y = (*mouse_xy).slc<COORD_WL>(10);
    
    // extract VGA pixel X-Y coordinates, and coordinates of last light point on the line
    vga_x = (*vga_xy).slc<COORD_WL>(0);
    vga_y = (*vga_xy).slc<COORD_WL>(10);
    last_x = (*last_xy_input).slc<COORD_WL>(0);
    last_y = (*last_xy_input).slc<COORD_WL>(10);
    
    // levels_in is an n-bit number (n is definition LEVELS, which represents the number of levels in this game)
    // each bit of levels_in shows whether that level has been completed (0 - not completed, 1 - completed)
    // by default, level 0 is always completed - i.e. the game starts with 2 dots on screen
    
    ac_int<MAXLEVELS, false> nolevels = *levelz_out;
    
    // this starts the game
    if (!*levels_in) 
    {
        nolevels = 1;
        *last_xy_output = 0;
    }
    else nolevels = *levels_in;
    
    // define sim_vga_x as the symetric of vga_x - that is because the image from the camera is mirrored of what the player expects
    // same for sim_vga_y, used for the mouse
    
    ac_int<10, false> sim_vga_x = WIDTH - vga_x;
    ac_int<10, false> sim_vga_y = HEIGHT - vga_y;
    
    // points[] holds n levels for the game (where n is LEVELS)
    image_point points[] =  { {350, 200}, {450, 350}, {225, 250}, {475, 250}, {250, 350}, {350, 200}, 
                              {200, 300}, {500, 350}, {450, 225}, {300, 275}, {400, 350}, {350, 200}, {0, 0} };
    
    //{400, 100}, {300, 200}, {200, 200}, {300, 300}, {200, 500}, {300, 400}, {600, 500}, {500, 300}, 
    //                        {600, 200}, {500, 200}, {400, 100}, {0, 0} }; // LEVELS -> 11
                            //{ {200, 200}, {500, 300}, {600, 400}, {400, 500}, {200, 300}, {0, 0} }; // LEVELS -> 5
    
    // if the player finishes all levels, the game starts all over again
    if (nolevels == LEVELS)
    {
        nolevels = 1;
    }

    // set pixel to default colour
    if (*sw_two == 1)
    {
        o_red = 1023;
        o_green = 1023;
        o_blue = 1023;
    }
    else 
    {
        o_red = 0;
        o_green = 0;
        o_blue = 0;
    }
    
    // draw on screen the current level, and the last completed level
    if ((vga_x >= points[nolevels - 1].x - DOT_DRAW_AREA) && (vga_x <= points[nolevels - 1].x + DOT_DRAW_AREA) && (vga_y >= points[nolevels - 1].y - DOT_DRAW_AREA) 
                && (vga_y <= points[nolevels - 1].y + DOT_DRAW_AREA))
    {
        o_red = 0;
        o_green = 0;
        o_blue = 1023;
    }
    if ((vga_x >= points[nolevels].x - DOT_DRAW_AREA) && (vga_x <= points[nolevels].x + DOT_DRAW_AREA) && (vga_y >= points[nolevels].y - DOT_DRAW_AREA) 
                && (vga_y <= points[nolevels].y + DOT_DRAW_AREA))
    {
        o_red = 0;
        o_green = 0;
        o_blue = 1023;
    }
    
    ac_int<MAXLEVELS, false> _history;
    if (*history == 0) _history = 1;
    else _history = nolevels - 3;
    // draw on screen lines connecting all the previously completed levels
    for (int i = _history; i < nolevels; i++)
    {
        if (_isOnSegment(points[i - 1], points[i], vga_x, vga_y, THRESHOLD))
        {
            if (points[i - 1].x < points[i].x)
            {
                if (points[i - 1].y < points[i].y)
                {
                    if (vga_x <= points[i].x && vga_y <= points[i].y && vga_x >= points[i - 1].x && vga_y >= points[i - 1].y)
                    {
                        o_red = 0;
                        o_green = 0;
                        o_blue = 1023;
                    }
                }
                else
                {
                    if (vga_x <= points[i].x && vga_y <= points[i - 1].y && vga_x >= points[i - 1].x && vga_y >= points[i].y)
                    {
                        o_red = 0;
                        o_green = 0;
                        o_blue = 1023;
                    }
                }
            }
            else
            {
                if (points[i - 1].y < points[i].y)
                {
                    if (vga_x <= points[i - 1].x && vga_y <= points[i].y && vga_x >= points[i].x && vga_y >= points[i - 1].y)
                    {
                        o_red = 0;
                        o_green = 0;
                        o_blue = 1023;
                    }
                }
                else
                {
                    if (vga_x <= points[i - 1].x && vga_y <= points[i - 1].y && vga_x >= points[i].x && vga_y >= points[i].y)
                    {
                        o_red = 0;
                        o_green = 0;
                        o_blue = 1023;
                    }
                }
            }
        }
    }
    
    // draw on screen the line currently in progress, if any
    if (*onTrack_input)
    {
        if (_isOnSegment(points[nolevels - 1], points[nolevels], vga_x, vga_y, THRESHOLD))
        {
            if (points[nolevels - 1].x < points[nolevels].x)
            {
                if (points[nolevels - 1].y < points[nolevels].y)
                {
                    if (vga_x <= last_x && vga_y <= last_y && vga_x >= points[nolevels - 1].x && vga_y >= points[nolevels - 1].y)
                    {
                        o_red = 1023;
                        o_green = 0;
                        o_blue = 0;
                    }
                }
                else
                {
                    if (vga_x <= last_x && vga_y >= last_y && vga_x >= points[nolevels - 1].x && vga_y <= points[nolevels - 1].y)
                    {
                        o_red = 1023;
                        o_green = 0;
                        o_blue = 0;
                    }
                }
            }
            else
            {
                if (points[nolevels - 1].y < points[nolevels].y)
                {
                    if (vga_x >= last_x && vga_y <= last_y && vga_x <= points[nolevels - 1].x && vga_y >= points[nolevels - 1].y)
                    {
                        o_red = 1023;
                        o_green = 0;
                        o_blue = 0;
                    }
                }
                else
                {
                    if (vga_x >= last_x && vga_y >= last_y && vga_x <= points[nolevels - 1].x && vga_y <= points[nolevels - 1].y)
                    {
                        o_red = 1023;
                        o_green = 0;
                        o_blue = 0;
                    }
                }
            }
        }
    }
    
    // draws the light source on screen, symetrical to the physical one
    *lastLight_output = *lastLight_input;  
    if (*sw_zero == 1 && vga_x >= *lastLight_output - LIGHT_SOURCE_SIZE && vga_x <= *lastLight_output + LIGHT_SOURCE_SIZE) 
    {
        if (*sw_two == 1)
        {
            o_red = 0;
            o_green = 0;
            o_blue = 0;
        }
        else
        {
            o_red = 512;
            o_green = 512;
            o_blue = 512;
        }
        *lastLight_output = 0;
    }    
    *level_pulse = 0;
    // check to see if current point is the light source; if it is, store it in last_x, and last_y
    acpoint_u h, s, v;
    RGB2HSV(i_red, i_green, i_blue, h, s, v);
    s = s * 100;
    v = v * 100;
    
    // checks which mode the game is into:
    // 0 - mouse is the controller
    // 1 - the light received from camera is the controller
    if ((*sw_zero == 1 && s <= 1 && v >= 99) || (*sw_zero == 0 && (vga_x >= mouse_x - MOUSE_BOX_AREA) && (vga_x <= mouse_x + MOUSE_BOX_AREA) && (vga_y >= mouse_y - MOUSE_BOX_AREA) 
                && (vga_y <= mouse_y + MOUSE_BOX_AREA)))
    {
        *lastLight_output = sim_vga_x;
        // current pixel is bright
        if ((*sw_zero == 1 && _isOnSegment(points[nolevels - 1], points[nolevels], sim_vga_x, vga_y, THRESHOLD * 20)) ||
            (*sw_zero == 0 && _isOnSegment(points[nolevels - 1], points[nolevels], vga_x, sim_vga_y, THRESHOLD * 20)))
        {
            if (*sw_zero == 1) *last_xy_output = ((ac_int<PIXEL_WL, false>)vga_y) << 10 | (ac_int<PIXEL_WL, false>)sim_vga_x;
            else *last_xy_output = ((ac_int<PIXEL_WL, false>)sim_vga_y) << 10 | (ac_int<PIXEL_WL, false>)vga_x;
            *onTrack_output = *onTrack_input;
        }
        else
        {
            *last_xy_output = 0;
            *onTrack_output = 0;
        }
        if (((*sw_zero == 1 && (sim_vga_x >= points[nolevels - 1].x - DOT_SIZE) && (sim_vga_x <= points[nolevels - 1].x + DOT_SIZE) && (vga_y >= points[nolevels - 1].y - DOT_SIZE) 
                && (vga_y <= points[nolevels - 1].y + DOT_SIZE))) ||
            ((*sw_zero == 0 && (vga_x >= points[nolevels - 1].x - DOT_SIZE) && (vga_x <= points[nolevels - 1].x + DOT_SIZE) && (sim_vga_y >= points[nolevels - 1].y - DOT_SIZE) 
                && (sim_vga_y <= points[nolevels - 1].y + DOT_SIZE))))
        {
            *onTrack_output = 1;
        }
        if ((((*sw_zero == 1 && (sim_vga_x >= points[nolevels].x - DOT_SIZE) && (sim_vga_x <= points[nolevels].x + DOT_SIZE) && (vga_y >= points[nolevels].y - DOT_SIZE) 
                && (vga_y <= points[nolevels].y + DOT_SIZE))) || 
            ((*sw_zero == 0 && (vga_x >= points[nolevels].x - DOT_SIZE) && (vga_x <= points[nolevels].x + DOT_SIZE) && (sim_vga_y >= points[nolevels].y - DOT_SIZE) 
                && (sim_vga_y <= points[nolevels].y + DOT_SIZE)))) && *onTrack_input)
        {
             *level_pulse = 1;
             nolevels += 1;
             *onTrack_output = 0;
             *last_xy_output = 0;
        }
            
    }
    else
    {
        *last_xy_output = *last_xy_input;
        *onTrack_output = *onTrack_input;
    }
    
    // draw on screen the grid, useful for easier pointing, and debugging
    if (*sw_one == 1)
    {
		if (vga_x == 100)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 200)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 300)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 400)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 500)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 600)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 640)
		{
			o_red = 0;
			o_green = 1023;
			o_blue = 1023;
		}
		if (vga_x == 700)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_x == 800)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_y == 100)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_y == 200)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_y == 300)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
		if (vga_y == 400)
		{
			o_red = 1023;
			o_green = 1023;
			o_blue = 0;
		}
        if (vga_y == 480)
        {
            o_red = 0;
            o_green = 1023;
            o_blue = 1023;
        }
        if (vga_y == 500)
        {
            o_red = 1023;
            o_green = 1023;
            o_blue = 0;
        }
    }
    // keep "in memory" the current level
    *levelz_out = nolevels;
    
    //*light_output =  ((((((((((((((((((((buffer[19] << 190) | (buffer[18] << 180)) | (buffer[17] << 170)) | (buffer[16] << 160)) | (buffer[15] << 150)) | (buffer[14] << 140)) | (buffer[13] << 130)) | (buffer[12] << 120)) | (buffer[11] << 110)) | (buffer[10] << 100)) | (buffer[9] << 90)) | (buffer[8] << 80)) | (buffer[7] << 70)) | (buffer[6] << 60)) | (buffer[5] << 50)) | (buffer[4] << 40)) | (buffer[3] << 30)) | (buffer[2] << 20)) | (buffer[1] << 10)) | (buffer[0]));                             
    
    // combine the 3 color components into 1 signal only
    *video_out = ((((ac_int<PIXEL_WL, false>)o_red) << 20) | (((ac_int<PIXEL_WL, false>)o_green) << 10) | (ac_int<PIXEL_WL, false>)o_blue);

}