#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main(int argc, char *argv[])
{
	ifstream f;
	f.open(argv[1]);
	if (f.is_open())
	{
		// matrix that contains initial image
		int w, h;
		f >> w >> h;
		int** a = new int*[h];
		for (int i = 0; i < h; ++i)
		{
			a[i] = new int[w];
		}

		// convert image to gray scale
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				int R, G, B;
				f >> R >> G >> B;
				R = (int)(0.11 * R);
				G = (int)(0.59 * G);
				B = (int)(0.30 * B);
				a[i][j] = R + G + B;
			}
		}

		// create the kernel
		int k[3][3] = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };

		// matrix which contains horizontal component
		int** x = new int*[h];
		for (int i = 0; i < h; ++i)
		{
			x[i] = new int[w];
		}

		// matrix that contains vertical component
		int** y = new int*[h];
		for (int i = 0; i < h; ++i)
		{
			y[i] = new int[w];
		}

		// compute new values, horizontal, main part, ohne edges
		for (int i = 1; i < h - 1; i++)
		{
			for (int j = 1; j < w - 1; j++)
			{
				x[i][j] = a[i - 1][j - 1] * k[0][0] +
					a[i - 1][j] * k[0][1] +
					a[i - 1][j + 1] * k[0][2] +
					a[i][j - 1] * k[1][0] +
					a[i][j] * k[1][1] +
					a[i][j + 1] * k[1][2] +
					a[i + 1][j - 1] * k[2][0] +
					a[i + 1][j] * k[2][1] +
					a[i + 1][j + 1] * k[2][2];
			}
		}

		// remake the kernal for vertical
		k[0][0] = 1;
		k[0][1] = 2;
		k[0][2] = 1;
		k[1][0] = 0;
		k[1][1] = 0;
		k[1][2] = 0;
		k[2][0] = -1;
		k[2][1] = -2;
		k[2][2] = -1;

		// compute new values, vertical, main part, ohne edges
		for (int i = 1; i < h - 1; i++)
		{
			for (int j = 1; j < w - 1; j++)
			{
				y[i][j] = a[i - 1][j - 1] * k[0][0] +
					a[i - 1][j] * k[0][1] +
					a[i - 1][j + 1] * k[0][2] +
					a[i][j - 1] * k[1][0] +
					a[i][j] * k[1][1] +
					a[i][j + 1] * k[1][2] +
					a[i + 1][j - 1] * k[2][0] +
					a[i + 1][j] * k[2][1] +
					a[i + 1][j + 1] * k[2][2];
			}
		}

		// normalization
		for (int i = 1; i < h - 1; i++)
		{
			for (int j = 1; j < w - 1; j++)
			{
				x[i][j] = sqrt(x[i][j] * x[i][j] + y[i][j] * y[i][j]);
				if (x[i][j] > 255) x[i][j] = 255;
			}
		}

		// takeing care of edges
		for (int i = 1; i < w - 1; i++)
		{
			x[0][i] = x[1][i];
		}
		for (int i = 1; i < w - 1; i++)
		{
			x[h - 1][i] = x[h - 2][i];
		}
		for (int i = 1; i < h - 1; i++)
		{
			x[i][0] = x[i][1];
		}
		for (int i = 1; i < h - 1; i++)
		{
			x[i][w - 1] = x[i][w - 2];
		}

		// takeing care of corners
		x[0][0] = x[0][1];
		x[h - 1][0] = x[h - 1][1];
		x[0][w - 1] = x[1][w - 1];
		x[h - 1][w - 1] = x[h - 1][w - 2];

		// closing file
		f.close();

		// deleting file, so we can output in place
		remove(argv[1]);

		// save matrix to output file
		ofstream g(argv[1]);
		g << w << " " << h << endl;
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
			
				if (j == w - 1)
				{
					g << x[i][j] << " " << x[i][j] << " " << x[i][j];
				}
				else
				{
					g << x[i][j] << " " << x[i][j] << " " << x[i][j] << "\t";
				}
			}
			g << endl;
		}

		// done, kill it
		return 0;
	}
}