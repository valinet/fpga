-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "03/16/2016 04:48:08"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	combinationalmultiplier IS
    PORT (
	D0 : OUT std_logic_vector(6 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	D1 : OUT std_logic_vector(6 DOWNTO 0);
	D2 : OUT std_logic_vector(6 DOWNTO 0);
	D3 : OUT std_logic_vector(6 DOWNTO 0);
	OUTPUT : OUT std_logic_vector(9 DOWNTO 0)
	);
END combinationalmultiplier;

-- Design Ports Information
-- D0[6]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[4]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[1]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D0[0]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[6]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[5]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[4]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[2]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[1]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D1[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[2]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D2[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[4]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[2]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D3[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[8]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[7]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[6]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[5]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[3]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[1]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[0]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_G5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_H7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_G4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_H6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_H5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_E3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_E4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF combinationalmultiplier IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_D0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_D1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_D3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_OUTPUT : std_logic_vector(9 DOWNTO 0);
SIGNAL \D0[6]~output_o\ : std_logic;
SIGNAL \D0[5]~output_o\ : std_logic;
SIGNAL \D0[4]~output_o\ : std_logic;
SIGNAL \D0[3]~output_o\ : std_logic;
SIGNAL \D0[2]~output_o\ : std_logic;
SIGNAL \D0[1]~output_o\ : std_logic;
SIGNAL \D0[0]~output_o\ : std_logic;
SIGNAL \D1[6]~output_o\ : std_logic;
SIGNAL \D1[5]~output_o\ : std_logic;
SIGNAL \D1[4]~output_o\ : std_logic;
SIGNAL \D1[3]~output_o\ : std_logic;
SIGNAL \D1[2]~output_o\ : std_logic;
SIGNAL \D1[1]~output_o\ : std_logic;
SIGNAL \D1[0]~output_o\ : std_logic;
SIGNAL \D2[6]~output_o\ : std_logic;
SIGNAL \D2[5]~output_o\ : std_logic;
SIGNAL \D2[4]~output_o\ : std_logic;
SIGNAL \D2[3]~output_o\ : std_logic;
SIGNAL \D2[2]~output_o\ : std_logic;
SIGNAL \D2[1]~output_o\ : std_logic;
SIGNAL \D2[0]~output_o\ : std_logic;
SIGNAL \D3[6]~output_o\ : std_logic;
SIGNAL \D3[5]~output_o\ : std_logic;
SIGNAL \D3[4]~output_o\ : std_logic;
SIGNAL \D3[3]~output_o\ : std_logic;
SIGNAL \D3[2]~output_o\ : std_logic;
SIGNAL \D3[1]~output_o\ : std_logic;
SIGNAL \D3[0]~output_o\ : std_logic;
SIGNAL \OUTPUT[9]~output_o\ : std_logic;
SIGNAL \OUTPUT[8]~output_o\ : std_logic;
SIGNAL \OUTPUT[7]~output_o\ : std_logic;
SIGNAL \OUTPUT[6]~output_o\ : std_logic;
SIGNAL \OUTPUT[5]~output_o\ : std_logic;
SIGNAL \OUTPUT[4]~output_o\ : std_logic;
SIGNAL \OUTPUT[3]~output_o\ : std_logic;
SIGNAL \OUTPUT[2]~output_o\ : std_logic;
SIGNAL \OUTPUT[1]~output_o\ : std_logic;
SIGNAL \OUTPUT[0]~output_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \inst17|inst1|inst|inst~combout\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \inst15|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst17|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ : std_logic;
SIGNAL \inst15|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst15|inst2|inst1|inst5~combout\ : std_logic;
SIGNAL \inst17|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \inst25|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst17|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst25|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst25|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \inst32|inst1|inst|inst~combout\ : std_logic;
SIGNAL \inst25|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst32|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst32|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst32|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ : std_logic;
SIGNAL \inst15|inst2|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst17|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst17|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst15|inst3|inst2~0_combout\ : std_logic;
SIGNAL \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ : std_logic;
SIGNAL \inst15|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst25|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst25|inst5|inst1|inst~combout\ : std_logic;
SIGNAL \inst17|inst57|inst1|inst~combout\ : std_logic;
SIGNAL \inst15|inst4|inst2~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst25|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst25|inst57|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst34|inst1|inst~combout\ : std_logic;
SIGNAL \inst32|inst488|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst1|inst|inst5~combout\ : std_logic;
SIGNAL \inst32|inst4|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst2|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst32|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst32|inst3|inst1|inst5~combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~46_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~32_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~47_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~48_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~49_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~50_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~241_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~75_combout\ : std_logic;
SIGNAL \inst32|inst5|inst1|inst5~combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~41_combout\ : std_logic;
SIGNAL \inst|Equal0~8_combout\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~0_combout\ : std_logic;
SIGNAL \inst15|inst57|inst1|inst~combout\ : std_logic;
SIGNAL \inst25|inst34|inst1|inst5~combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~24_combout\ : std_logic;
SIGNAL \inst|Equal0~9_combout\ : std_logic;
SIGNAL \inst|Equal0~27_combout\ : std_logic;
SIGNAL \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~71_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~248_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~72_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~42_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~73_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~74_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~76_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~43_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~33_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~34_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~38_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~39_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~36_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~35_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~240_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~37_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~40_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~44_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~45_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~249_combout\ : std_logic;
SIGNAL \inst32|inst667|inst1|inst5~combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~59_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~243_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~64_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~63_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~65_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~245_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~62_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~66_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~53_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~54_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~68_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~55_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~56_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~244_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~67_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~69_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~247_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~58_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~57_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~60_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~61_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~246_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~70_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~51_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~52_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~242_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~77_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~118_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~119_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~121_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~120_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~25_combout\ : std_logic;
SIGNAL \inst|Equal0~28_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~250_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~251_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~122_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~82_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~81_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~83_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~86_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~85_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~87_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~117_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~115_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~114_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~116_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~123_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~78_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~79_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~80_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~84_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~88_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~99_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~95_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~97_combout\ : std_logic;
SIGNAL \inst|Equal0~10_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~93_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~109_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~108_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~110_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~94_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~96_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~98_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~111_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~112_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~102_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~103_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~105_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~104_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~101_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~106_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~91_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~89_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~90_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~92_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~100_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~107_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~113_combout\ : std_logic;
SIGNAL \inst|seven_seg0[5]~124_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~129_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~125_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~126_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~127_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~128_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~130_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~136_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~131_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~138_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~139_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~140_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~141_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~142_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~143_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~144_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~146_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~145_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~134_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~135_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~132_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~133_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~137_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~147_combout\ : std_logic;
SIGNAL \inst|Equal0~11_combout\ : std_logic;
SIGNAL \inst|Equal0~12_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~148_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~149_combout\ : std_logic;
SIGNAL \inst|Equal0~13_combout\ : std_logic;
SIGNAL \inst|Equal0~14_combout\ : std_logic;
SIGNAL \inst|seven_seg0[4]~150_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~161_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~162_combout\ : std_logic;
SIGNAL \inst|Equal0~29_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~154_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~163_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~164_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~165_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~166_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~167_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~168_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~169_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~170_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~252_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~171_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~151_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~152_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~153_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~155_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~156_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~157_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~158_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~159_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~160_combout\ : std_logic;
SIGNAL \inst|seven_seg0[3]~172_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~177_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~178_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~175_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~176_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~179_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~173_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~174_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~180_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~181_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~182_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~183_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~184_combout\ : std_logic;
SIGNAL \inst|seven_seg0[2]~185_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~186_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~189_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~198_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~203_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~199_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~204_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~201_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~200_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~202_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~205_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~196_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~195_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~197_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~206_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~187_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~188_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~190_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~191_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~192_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~193_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~194_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~207_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~208_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~212_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~213_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~214_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~216_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~215_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~217_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~218_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~210_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~209_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~211_combout\ : std_logic;
SIGNAL \inst|Equal0~15_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~220_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~219_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~222_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~221_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~223_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~224_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~253_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~254_combout\ : std_logic;
SIGNAL \inst|seven_seg0[1]~225_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~226_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~227_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~228_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~229_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~230_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~231_combout\ : std_logic;
SIGNAL \inst|seven_seg0[0]~232_combout\ : std_logic;
SIGNAL \inst|Equal0~18_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~62_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~63_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~61_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~64_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~65_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~57_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~58_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~32_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~59_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~53_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~52_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~54_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~55_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~51_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~56_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~60_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~253_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~49_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~50_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~66_combout\ : std_logic;
SIGNAL \inst|Equal0~17_combout\ : std_logic;
SIGNAL \inst|Equal0~16_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~42_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~43_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~40_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~252_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~44_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~45_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~46_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~47_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~29_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~28_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~30_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~31_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~37_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~36_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~35_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~38_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~33_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~34_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~39_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~48_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~67_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~75_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~74_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~268_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~269_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~70_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~69_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~71_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~68_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~72_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~73_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~76_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~79_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~97_combout\ : std_logic;
SIGNAL \inst|Equal0~19_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~95_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~96_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~98_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~90_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~255_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~99_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~100_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~101_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~102_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~103_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~77_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~78_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~254_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~80_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~84_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~81_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~82_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~83_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~85_combout\ : std_logic;
SIGNAL \inst|Equal0~20_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~86_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~91_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~92_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~87_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~88_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~89_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~93_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~94_combout\ : std_logic;
SIGNAL \inst|seven_seg1[5]~104_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~266_combout\ : std_logic;
SIGNAL \inst|seven_seg1[6]~41_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~125_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~126_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~127_combout\ : std_logic;
SIGNAL \inst|Equal0~21_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~128_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~267_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~122_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~110_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~123_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~124_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~129_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~111_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~112_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~113_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~114_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~115_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~117_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~118_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~119_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~116_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~120_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~121_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~106_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~105_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~108_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~107_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~109_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~130_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~139_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~138_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~140_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~141_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~137_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~142_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~143_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~147_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~144_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~145_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~146_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~148_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~149_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~150_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~153_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~154_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~151_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~152_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~155_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~133_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~132_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~134_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~131_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~135_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~136_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~156_combout\ : std_logic;
SIGNAL \inst|seven_seg1[4]~157_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~159_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~165_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~166_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~178_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~179_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~180_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~256_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~184_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~233_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~237_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~183_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~257_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~255_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~236_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~235_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~181_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~234_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~182_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~185_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~238_combout\ : std_logic;
SIGNAL \inst|seven_seg0[6]~239_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~186_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~187_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~188_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~26_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~164_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~167_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~158_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~162_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~160_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~163_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~168_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~169_combout\ : std_logic;
SIGNAL \inst|Equal0~30_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~170_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~171_combout\ : std_logic;
SIGNAL \inst|Equal0~22_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~172_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~173_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~174_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~175_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~176_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~264_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~265_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~161_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~177_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~189_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~199_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~198_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~200_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~192_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~258_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~197_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~201_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~202_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~203_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~204_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~205_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~206_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~207_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~208_combout\ : std_logic;
SIGNAL \inst|Equal0~23_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~209_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~210_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~211_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~212_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~213_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~214_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~194_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~195_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~190_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~191_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~193_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~196_combout\ : std_logic;
SIGNAL \inst|seven_seg1[2]~215_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~227_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~228_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~229_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~230_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~231_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~224_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~225_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~222_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~221_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~223_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~226_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~232_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~234_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~235_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~236_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~237_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~238_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~233_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~260_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~239_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~218_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~219_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~216_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~217_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~259_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~220_combout\ : std_logic;
SIGNAL \inst|seven_seg1[1]~240_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~248_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~263_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~246_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~247_combout\ : std_logic;
SIGNAL \inst|seven_seg1[0]~249_combout\ : std_logic;
SIGNAL \inst|seven_seg1[0]~250_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~243_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~242_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~244_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~262_combout\ : std_logic;
SIGNAL \inst|seven_seg1[0]~241_combout\ : std_logic;
SIGNAL \inst|seven_seg1[3]~261_combout\ : std_logic;
SIGNAL \inst|seven_seg1[0]~245_combout\ : std_logic;
SIGNAL \inst|seven_seg1[0]~251_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~27_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~28_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~29_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~31_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~30_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~32_combout\ : std_logic;
SIGNAL \inst|seven_seg2[6]~33_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~39_combout\ : std_logic;
SIGNAL \inst|Equal0~24_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~71_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~37_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~36_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~34_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~35_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~38_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~72_combout\ : std_logic;
SIGNAL \inst|seven_seg2[5]~40_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~46_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~44_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~43_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~45_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~47_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~41_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~42_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~14_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~11_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~75_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~49_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~50_combout\ : std_logic;
SIGNAL \inst|seven_seg2[3]~52_combout\ : std_logic;
SIGNAL \inst|seven_seg2[3]~51_combout\ : std_logic;
SIGNAL \inst|seven_seg2[3]~53_combout\ : std_logic;
SIGNAL \inst|seven_seg2[3]~54_combout\ : std_logic;
SIGNAL \inst|seven_seg2[3]~55_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~58_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~59_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~60_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~56_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~57_combout\ : std_logic;
SIGNAL \inst|seven_seg2[2]~61_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~64_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~74_combout\ : std_logic;
SIGNAL \inst|Equal0~25_combout\ : std_logic;
SIGNAL \inst|Equal0~26_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~62_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~63_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~65_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~73_combout\ : std_logic;
SIGNAL \inst|seven_seg2[1]~66_combout\ : std_logic;
SIGNAL \inst|seven_seg2[0]~68_combout\ : std_logic;
SIGNAL \inst|seven_seg2[0]~69_combout\ : std_logic;
SIGNAL \inst|seven_seg2[0]~67_combout\ : std_logic;
SIGNAL \inst|seven_seg2[0]~70_combout\ : std_logic;
SIGNAL \inst|seven_seg2[4]~48_combout\ : std_logic;
SIGNAL \inst|seven_seg3[2]~0_combout\ : std_logic;

BEGIN

D0 <= ww_D0;
ww_SW <= SW;
D1 <= ww_D1;
D2 <= ww_D2;
D3 <= ww_D3;
OUTPUT <= ww_OUTPUT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X26_Y29_N16
\D0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[6]~77_combout\,
	devoe => ww_devoe,
	o => \D0[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\D0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[5]~124_combout\,
	devoe => ww_devoe,
	o => \D0[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\D0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[4]~150_combout\,
	devoe => ww_devoe,
	o => \D0[4]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\D0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[3]~172_combout\,
	devoe => ww_devoe,
	o => \D0[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\D0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[2]~185_combout\,
	devoe => ww_devoe,
	o => \D0[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\D0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[1]~225_combout\,
	devoe => ww_devoe,
	o => \D0[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\D0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg0[0]~232_combout\,
	devoe => ww_devoe,
	o => \D0[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\D1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[6]~67_combout\,
	devoe => ww_devoe,
	o => \D1[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\D1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[5]~104_combout\,
	devoe => ww_devoe,
	o => \D1[5]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\D1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[4]~157_combout\,
	devoe => ww_devoe,
	o => \D1[4]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\D1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[3]~189_combout\,
	devoe => ww_devoe,
	o => \D1[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\D1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[2]~215_combout\,
	devoe => ww_devoe,
	o => \D1[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\D1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[1]~240_combout\,
	devoe => ww_devoe,
	o => \D1[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\D1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg1[0]~251_combout\,
	devoe => ww_devoe,
	o => \D1[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\D2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[6]~33_combout\,
	devoe => ww_devoe,
	o => \D2[6]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\D2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[5]~40_combout\,
	devoe => ww_devoe,
	o => \D2[5]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\D2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[4]~50_combout\,
	devoe => ww_devoe,
	o => \D2[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\D2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[3]~55_combout\,
	devoe => ww_devoe,
	o => \D2[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\D2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[2]~61_combout\,
	devoe => ww_devoe,
	o => \D2[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\D2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[1]~66_combout\,
	devoe => ww_devoe,
	o => \D2[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\D2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg2[0]~70_combout\,
	devoe => ww_devoe,
	o => \D2[0]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\D3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\D3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[5]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\D3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\D3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\D3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\D3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|seven_seg3[2]~0_combout\,
	devoe => ww_devoe,
	o => \D3[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\D3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \D3[0]~output_o\);

-- Location: IOOBUF_X0_Y27_N16
\OUTPUT[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst667|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[9]~output_o\);

-- Location: IOOBUF_X0_Y27_N9
\OUTPUT[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst488|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[8]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\OUTPUT[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst34|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[7]~output_o\);

-- Location: IOOBUF_X0_Y26_N23
\OUTPUT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst5|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[6]~output_o\);

-- Location: IOOBUF_X0_Y24_N16
\OUTPUT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst57|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[5]~output_o\);

-- Location: IOOBUF_X0_Y24_N23
\OUTPUT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst4|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[4]~output_o\);

-- Location: IOOBUF_X0_Y21_N16
\OUTPUT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst3|inst1|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[3]~output_o\);

-- Location: IOOBUF_X0_Y21_N23
\OUTPUT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst2|inst1|inst5~0_combout\,
	devoe => ww_devoe,
	o => \OUTPUT[2]~output_o\);

-- Location: IOOBUF_X0_Y20_N2
\OUTPUT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst32|inst1|inst|inst5~combout\,
	devoe => ww_devoe,
	o => \OUTPUT[1]~output_o\);

-- Location: IOOBUF_X0_Y20_N9
\OUTPUT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	devoe => ww_devoe,
	o => \OUTPUT[0]~output_o\);

-- Location: IOIBUF_X0_Y23_N8
\SW[3]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X0_Y26_N8
\SW[7]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X0_Y25_N22
\SW[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X0_Y24_N1
\SW[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: IOIBUF_X0_Y27_N1
\SW[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X0_Y26_N1
\SW[8]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: LCCOMB_X5_Y24_N22
\inst17|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst~combout\ = (\SW[7]~input_o\ & (\SW[0]~input_o\ & (\SW[1]~input_o\ & \SW[8]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[1]~input_o\,
	datad => \SW[8]~input_o\,
	combout => \inst17|inst1|inst|inst~combout\);

-- Location: IOIBUF_X0_Y25_N1
\SW[9]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LCCOMB_X5_Y24_N16
\inst15|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst5~combout\ = (\SW[1]~input_o\ & (\SW[8]~input_o\ $ (((\SW[0]~input_o\ & \SW[9]~input_o\))))) # (!\SW[1]~input_o\ & (\SW[0]~input_o\ & (\SW[9]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[9]~input_o\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X5_Y24_N26
\inst17|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst2~0_combout\ = (\inst17|inst1|inst|inst~combout\ & ((\inst15|inst1|inst|inst5~combout\) # ((\SW[7]~input_o\ & \SW[2]~input_o\)))) # (!\inst17|inst1|inst|inst~combout\ & (\SW[7]~input_o\ & (\SW[2]~input_o\ & 
-- \inst15|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \inst17|inst1|inst|inst~combout\,
	datad => \inst15|inst1|inst|inst5~combout\,
	combout => \inst17|inst2|inst2~0_combout\);

-- Location: LCCOMB_X5_Y24_N30
\inst9|LPM_MUX_component|auto_generated|result_node[1]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ = (\SW[9]~input_o\ & \SW[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[9]~input_o\,
	datac => \SW[1]~input_o\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\);

-- Location: LCCOMB_X5_Y24_N4
\inst15|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst1|inst|inst~combout\ = (\SW[1]~input_o\ & (\SW[0]~input_o\ & (\SW[9]~input_o\ & \SW[8]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[9]~input_o\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst1|inst|inst~combout\);

-- Location: LCCOMB_X5_Y24_N8
\inst15|inst2|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst1|inst5~combout\ = \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ $ (\inst15|inst1|inst|inst~combout\ $ (((\SW[2]~input_o\ & \SW[8]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\,
	datab => \SW[2]~input_o\,
	datac => \inst15|inst1|inst|inst~combout\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst2|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y24_N10
\inst17|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst1|inst5~combout\ = \inst17|inst2|inst2~0_combout\ $ (\inst15|inst2|inst1|inst5~combout\ $ (((\SW[3]~input_o\ & \SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[3]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \inst17|inst2|inst2~0_combout\,
	datad => \inst15|inst2|inst1|inst5~combout\,
	combout => \inst17|inst3|inst1|inst5~combout\);

-- Location: IOIBUF_X0_Y27_N22
\SW[4]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X0_Y25_N15
\SW[6]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: LCCOMB_X5_Y24_N0
\inst25|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst~combout\ = (\SW[6]~input_o\ & (\SW[0]~input_o\ & (\SW[1]~input_o\ & \SW[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[1]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \inst25|inst1|inst|inst~combout\);

-- Location: LCCOMB_X5_Y24_N18
\inst17|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst1|inst|inst5~combout\ = (\SW[7]~input_o\ & (\SW[1]~input_o\ $ (((\SW[0]~input_o\ & \SW[8]~input_o\))))) # (!\SW[7]~input_o\ & (\SW[0]~input_o\ & ((\SW[8]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[1]~input_o\,
	datad => \SW[8]~input_o\,
	combout => \inst17|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X5_Y24_N12
\inst25|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst2~0_combout\ = (\inst25|inst1|inst|inst~combout\ & ((\inst17|inst1|inst|inst5~combout\) # ((\SW[6]~input_o\ & \SW[2]~input_o\)))) # (!\inst25|inst1|inst|inst~combout\ & (\SW[6]~input_o\ & (\SW[2]~input_o\ & 
-- \inst17|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \inst25|inst1|inst|inst~combout\,
	datac => \SW[2]~input_o\,
	datad => \inst17|inst1|inst|inst5~combout\,
	combout => \inst25|inst2|inst2~0_combout\);

-- Location: LCCOMB_X5_Y24_N2
\inst17|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst2|inst1|inst5~0_combout\ = \inst17|inst1|inst|inst~combout\ $ (\inst15|inst1|inst|inst5~combout\ $ (((\SW[7]~input_o\ & \SW[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[7]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \inst17|inst1|inst|inst~combout\,
	datad => \inst15|inst1|inst|inst5~combout\,
	combout => \inst17|inst2|inst1|inst5~0_combout\);

-- Location: LCCOMB_X6_Y24_N0
\inst25|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst2~0_combout\ = (\inst25|inst2|inst2~0_combout\ & ((\inst17|inst2|inst1|inst5~0_combout\) # ((\SW[6]~input_o\ & \SW[3]~input_o\)))) # (!\inst25|inst2|inst2~0_combout\ & (\SW[6]~input_o\ & (\SW[3]~input_o\ & 
-- \inst17|inst2|inst1|inst5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst2|inst2~0_combout\,
	datab => \SW[6]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \inst17|inst2|inst1|inst5~0_combout\,
	combout => \inst25|inst3|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N16
\inst25|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst1|inst5~combout\ = \inst17|inst3|inst1|inst5~combout\ $ (\inst25|inst3|inst2~0_combout\ $ (((\SW[4]~input_o\ & \SW[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst3|inst1|inst5~combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[6]~input_o\,
	datad => \inst25|inst3|inst2~0_combout\,
	combout => \inst25|inst4|inst1|inst5~combout\);

-- Location: IOIBUF_X0_Y22_N15
\SW[5]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: LCCOMB_X4_Y24_N8
\inst32|inst1|inst|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst~combout\ = (\SW[5]~input_o\ & (\SW[1]~input_o\ & (\SW[0]~input_o\ & \SW[6]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[0]~input_o\,
	datad => \SW[6]~input_o\,
	combout => \inst32|inst1|inst|inst~combout\);

-- Location: LCCOMB_X5_Y24_N24
\inst25|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst1|inst|inst5~combout\ = (\SW[6]~input_o\ & (\SW[1]~input_o\ $ (((\SW[0]~input_o\ & \SW[7]~input_o\))))) # (!\SW[6]~input_o\ & (\SW[0]~input_o\ & ((\SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[0]~input_o\,
	datac => \SW[1]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \inst25|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N26
\inst32|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst2~0_combout\ = (\inst32|inst1|inst|inst~combout\ & ((\inst25|inst1|inst|inst5~combout\) # ((\SW[5]~input_o\ & \SW[2]~input_o\)))) # (!\inst32|inst1|inst|inst~combout\ & (\SW[5]~input_o\ & (\SW[2]~input_o\ & 
-- \inst25|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \inst32|inst1|inst|inst~combout\,
	datac => \SW[2]~input_o\,
	datad => \inst25|inst1|inst|inst5~combout\,
	combout => \inst32|inst2|inst2~0_combout\);

-- Location: LCCOMB_X5_Y24_N10
\inst25|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst2|inst1|inst5~0_combout\ = \inst25|inst1|inst|inst~combout\ $ (\inst17|inst1|inst|inst5~combout\ $ (((\SW[6]~input_o\ & \SW[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \inst25|inst1|inst|inst~combout\,
	datac => \SW[2]~input_o\,
	datad => \inst17|inst1|inst|inst5~combout\,
	combout => \inst25|inst2|inst1|inst5~0_combout\);

-- Location: LCCOMB_X4_Y24_N20
\inst32|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst2~0_combout\ = (\inst32|inst2|inst2~0_combout\ & ((\inst25|inst2|inst1|inst5~0_combout\) # ((\SW[3]~input_o\ & \SW[5]~input_o\)))) # (!\inst32|inst2|inst2~0_combout\ & (\SW[3]~input_o\ & (\inst25|inst2|inst1|inst5~0_combout\ & 
-- \SW[5]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst2~0_combout\,
	datab => \SW[3]~input_o\,
	datac => \inst25|inst2|inst1|inst5~0_combout\,
	datad => \SW[5]~input_o\,
	combout => \inst32|inst3|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N14
\inst25|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst3|inst1|inst5~combout\ = \inst25|inst2|inst2~0_combout\ $ (\inst17|inst2|inst1|inst5~0_combout\ $ (((\SW[6]~input_o\ & \SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst2|inst2~0_combout\,
	datab => \SW[6]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \inst17|inst2|inst1|inst5~0_combout\,
	combout => \inst25|inst3|inst1|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N30
\inst32|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst2~0_combout\ = (\inst32|inst3|inst2~0_combout\ & ((\inst25|inst3|inst1|inst5~combout\) # ((\SW[5]~input_o\ & \SW[4]~input_o\)))) # (!\inst32|inst3|inst2~0_combout\ & (\SW[5]~input_o\ & (\SW[4]~input_o\ & 
-- \inst25|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \inst32|inst3|inst2~0_combout\,
	datac => \SW[4]~input_o\,
	datad => \inst25|inst3|inst1|inst5~combout\,
	combout => \inst32|inst4|inst2~0_combout\);

-- Location: LCCOMB_X10_Y21_N26
\inst32|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst57|inst1|inst5~combout\ = \inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst32|inst57|inst1|inst5~combout\);

-- Location: LCCOMB_X5_Y24_N14
\inst9|LPM_MUX_component|auto_generated|result_node[2]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ = (\SW[2]~input_o\ & \SW[9]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[2]~input_o\,
	datac => \SW[9]~input_o\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\);

-- Location: LCCOMB_X5_Y24_N20
\inst15|inst2|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst2|inst2~0_combout\ = (\inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ & ((\inst15|inst1|inst|inst~combout\) # ((\SW[2]~input_o\ & \SW[8]~input_o\)))) # (!\inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\ & 
-- (\SW[2]~input_o\ & (\inst15|inst1|inst|inst~combout\ & \SW[8]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[1]~0_combout\,
	datab => \SW[2]~input_o\,
	datac => \inst15|inst1|inst|inst~combout\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst2|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N6
\inst15|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst1|inst5~combout\ = \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ $ (\inst15|inst2|inst2~0_combout\ $ (((\SW[8]~input_o\ & \SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\,
	datab => \SW[8]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y24_N24
\inst17|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst3|inst2~0_combout\ = (\inst17|inst2|inst2~0_combout\ & ((\inst15|inst2|inst1|inst5~combout\) # ((\SW[3]~input_o\ & \SW[7]~input_o\)))) # (!\inst17|inst2|inst2~0_combout\ & (\SW[3]~input_o\ & (\SW[7]~input_o\ & 
-- \inst15|inst2|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[3]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \inst17|inst2|inst2~0_combout\,
	datad => \inst15|inst2|inst1|inst5~combout\,
	combout => \inst17|inst3|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N12
\inst17|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst2~0_combout\ = (\inst15|inst3|inst1|inst5~combout\ & ((\inst17|inst3|inst2~0_combout\) # ((\SW[4]~input_o\ & \SW[7]~input_o\)))) # (!\inst15|inst3|inst1|inst5~combout\ & (\SW[4]~input_o\ & (\SW[7]~input_o\ & 
-- \inst17|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|inst1|inst5~combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[7]~input_o\,
	datad => \inst17|inst3|inst2~0_combout\,
	combout => \inst17|inst4|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N18
\inst17|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst4|inst1|inst5~combout\ = \inst15|inst3|inst1|inst5~combout\ $ (\inst17|inst3|inst2~0_combout\ $ (((\SW[4]~input_o\ & \SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|inst1|inst5~combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[7]~input_o\,
	datad => \inst17|inst3|inst2~0_combout\,
	combout => \inst17|inst4|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y24_N22
\inst15|inst3|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst3|inst2~0_combout\ = (\inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ & ((\inst15|inst2|inst2~0_combout\) # ((\SW[8]~input_o\ & \SW[3]~input_o\)))) # (!\inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\ & 
-- (\SW[8]~input_o\ & (\SW[3]~input_o\ & \inst15|inst2|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|LPM_MUX_component|auto_generated|result_node[2]~1_combout\,
	datab => \SW[8]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \inst15|inst2|inst2~0_combout\,
	combout => \inst15|inst3|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N8
\inst9|LPM_MUX_component|auto_generated|result_node[3]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ = (\SW[9]~input_o\ & \SW[3]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[9]~input_o\,
	datac => \SW[3]~input_o\,
	combout => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\);

-- Location: LCCOMB_X6_Y24_N26
\inst15|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst1|inst5~combout\ = \inst15|inst3|inst2~0_combout\ $ (\inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ $ (((\SW[4]~input_o\ & \SW[8]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|inst2~0_combout\,
	datab => \SW[4]~input_o\,
	datac => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst4|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y24_N4
\inst25|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst4|inst2~0_combout\ = (\inst17|inst3|inst1|inst5~combout\ & ((\inst25|inst3|inst2~0_combout\) # ((\SW[4]~input_o\ & \SW[6]~input_o\)))) # (!\inst17|inst3|inst1|inst5~combout\ & (\SW[4]~input_o\ & (\SW[6]~input_o\ & 
-- \inst25|inst3|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst3|inst1|inst5~combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[6]~input_o\,
	datad => \inst25|inst3|inst2~0_combout\,
	combout => \inst25|inst4|inst2~0_combout\);

-- Location: LCCOMB_X6_Y24_N2
\inst25|inst5|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst5|inst1|inst~combout\ = (\inst17|inst4|inst1|inst5~combout\ & (\inst25|inst4|inst2~0_combout\ & (\inst17|inst4|inst2~0_combout\ $ (\inst15|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst15|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst5|inst1|inst~combout\);

-- Location: LCCOMB_X6_Y24_N20
\inst17|inst57|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst17|inst57|inst1|inst~combout\ = (\inst15|inst4|inst1|inst5~combout\ & \inst17|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst15|inst4|inst1|inst5~combout\,
	datad => \inst17|inst4|inst2~0_combout\,
	combout => \inst17|inst57|inst1|inst~combout\);

-- Location: LCCOMB_X6_Y24_N30
\inst15|inst4|inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst4|inst2~0_combout\ = (\inst15|inst3|inst2~0_combout\ & ((\inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\) # ((\SW[4]~input_o\ & \SW[8]~input_o\)))) # (!\inst15|inst3|inst2~0_combout\ & (\SW[4]~input_o\ & 
-- (\inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\ & \SW[8]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst3|inst2~0_combout\,
	datab => \SW[4]~input_o\,
	datac => \inst9|LPM_MUX_component|auto_generated|result_node[3]~2_combout\,
	datad => \SW[8]~input_o\,
	combout => \inst15|inst4|inst2~0_combout\);

-- Location: LCCOMB_X10_Y24_N24
\inst15|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst5~combout\ = \inst15|inst4|inst2~0_combout\ $ (((\SW[4]~input_o\ & \SW[9]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst4|inst2~0_combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[9]~input_o\,
	combout => \inst15|inst57|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y24_N28
\inst25|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst5|inst1|inst5~combout\ = \inst17|inst4|inst2~0_combout\ $ (\inst15|inst4|inst1|inst5~combout\ $ (((\inst17|inst4|inst1|inst5~combout\ & \inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst17|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst15|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst5|inst1|inst5~combout\);

-- Location: LCCOMB_X11_Y23_N24
\inst25|inst57|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst57|inst1|inst5~combout\ = \inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst17|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst25|inst57|inst1|inst5~combout\);

-- Location: LCCOMB_X11_Y23_N30
\inst32|inst34|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst34|inst1|inst~combout\ = (\inst25|inst5|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & \inst25|inst57|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst57|inst1|inst5~combout\,
	combout => \inst32|inst34|inst1|inst~combout\);

-- Location: LCCOMB_X7_Y24_N16
\inst32|inst488|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst488|inst1|inst5~combout\ = \inst25|inst5|inst1|inst~combout\ $ (\inst17|inst57|inst1|inst~combout\ $ (\inst15|inst57|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst~combout\,
	datab => \inst17|inst57|inst1|inst~combout\,
	datac => \inst15|inst57|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst~combout\,
	combout => \inst32|inst488|inst1|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N2
\inst32|inst1|inst|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst1|inst|inst5~combout\ = (\SW[5]~input_o\ & (\SW[1]~input_o\ $ (((\SW[0]~input_o\ & \SW[6]~input_o\))))) # (!\SW[5]~input_o\ & (((\SW[0]~input_o\ & \SW[6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \SW[1]~input_o\,
	datac => \SW[0]~input_o\,
	datad => \SW[6]~input_o\,
	combout => \inst32|inst1|inst|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N12
\inst32|inst4|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst4|inst1|inst5~combout\ = \inst32|inst3|inst2~0_combout\ $ (\inst25|inst3|inst1|inst5~combout\ $ (((\SW[5]~input_o\ & \SW[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \inst32|inst3|inst2~0_combout\,
	datac => \SW[4]~input_o\,
	datad => \inst25|inst3|inst1|inst5~combout\,
	combout => \inst32|inst4|inst1|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N6
\inst32|inst2|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst2|inst1|inst5~0_combout\ = \inst32|inst1|inst|inst~combout\ $ (\inst25|inst1|inst|inst5~combout\ $ (((\SW[5]~input_o\ & \SW[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[5]~input_o\,
	datab => \inst32|inst1|inst|inst~combout\,
	datac => \SW[2]~input_o\,
	datad => \inst25|inst1|inst|inst5~combout\,
	combout => \inst32|inst2|inst1|inst5~0_combout\);

-- Location: LCCOMB_X11_Y23_N26
\inst32|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst34|inst1|inst5~combout\ = \inst25|inst5|inst1|inst5~combout\ $ (((\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & \inst25|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst57|inst1|inst5~combout\,
	combout => \inst32|inst34|inst1|inst5~combout\);

-- Location: LCCOMB_X4_Y24_N24
\inst32|inst3|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst3|inst1|inst5~combout\ = \inst32|inst2|inst2~0_combout\ $ (\inst25|inst2|inst1|inst5~0_combout\ $ (((\SW[3]~input_o\ & \SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst2~0_combout\,
	datab => \SW[3]~input_o\,
	datac => \inst25|inst2|inst1|inst5~0_combout\,
	datad => \SW[5]~input_o\,
	combout => \inst32|inst3|inst1|inst5~combout\);

-- Location: LCCOMB_X12_Y21_N16
\inst|seven_seg0[6]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~46_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((!\inst32|inst3|inst1|inst5~combout\) # (!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~46_combout\);

-- Location: LCCOMB_X6_Y22_N24
\inst|seven_seg0[6]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~32_combout\ = \inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~32_combout\);

-- Location: LCCOMB_X9_Y21_N10
\inst|seven_seg0[6]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~47_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (((!\inst32|inst34|inst1|inst5~combout\))))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~47_combout\);

-- Location: LCCOMB_X9_Y21_N28
\inst|seven_seg0[6]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~48_combout\ = (\inst|seven_seg0[6]~46_combout\ & (\inst32|inst1|inst|inst5~combout\ $ (((\inst|seven_seg0[6]~47_combout\))))) # (!\inst|seven_seg0[6]~46_combout\ & (!\inst|seven_seg0[6]~47_combout\ & (\inst32|inst1|inst|inst5~combout\ 
-- $ (!\inst|seven_seg0[6]~32_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst|seven_seg0[6]~46_combout\,
	datac => \inst|seven_seg0[6]~32_combout\,
	datad => \inst|seven_seg0[6]~47_combout\,
	combout => \inst|seven_seg0[6]~48_combout\);

-- Location: LCCOMB_X10_Y21_N8
\inst|seven_seg0[6]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~49_combout\ = \inst32|inst4|inst1|inst5~combout\ $ (\inst32|inst2|inst1|inst5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[6]~49_combout\);

-- Location: LCCOMB_X9_Y21_N14
\inst|seven_seg0[6]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~50_combout\ = (!\inst|seven_seg0[6]~49_combout\ & ((\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (\inst32|inst3|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst|seven_seg0[6]~49_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~50_combout\);

-- Location: LCCOMB_X9_Y21_N2
\inst|seven_seg0[6]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~241_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ $ (\inst|seven_seg0[6]~32_combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ $ (!\inst|seven_seg0[6]~32_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst|seven_seg0[6]~32_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~241_combout\);

-- Location: LCCOMB_X9_Y21_N20
\inst|seven_seg0[6]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~75_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~48_combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~50_combout\) # (\inst|seven_seg0[6]~241_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~48_combout\,
	datac => \inst|seven_seg0[6]~50_combout\,
	datad => \inst|seven_seg0[6]~241_combout\,
	combout => \inst|seven_seg0[6]~75_combout\);

-- Location: LCCOMB_X11_Y23_N12
\inst32|inst5|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst5|inst1|inst5~combout\ = \inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\ $ (((\inst32|inst4|inst2~0_combout\ & \inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst32|inst5|inst1|inst5~combout\);

-- Location: LCCOMB_X5_Y21_N16
\inst|seven_seg0[6]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~41_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[6]~41_combout\);

-- Location: LCCOMB_X11_Y22_N8
\inst|Equal0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~8_combout\ = (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst3|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|Equal0~8_combout\);

-- Location: LCCOMB_X7_Y24_N4
\inst32|inst667|inst1|inst5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~0_combout\ = (\inst25|inst5|inst1|inst~combout\ & ((\inst17|inst57|inst1|inst~combout\ & ((!\inst32|inst34|inst1|inst~combout\) # (!\inst15|inst57|inst1|inst5~combout\))) # (!\inst17|inst57|inst1|inst~combout\ & 
-- ((\inst15|inst57|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst~combout\))))) # (!\inst25|inst5|inst1|inst~combout\ & ((\inst17|inst57|inst1|inst~combout\ & ((\inst15|inst57|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst~combout\))) # 
-- (!\inst17|inst57|inst1|inst~combout\ & (\inst15|inst57|inst1|inst5~combout\ & \inst32|inst34|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst~combout\,
	datab => \inst17|inst57|inst1|inst~combout\,
	datac => \inst15|inst57|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst~combout\,
	combout => \inst32|inst667|inst1|inst5~0_combout\);

-- Location: LCCOMB_X10_Y24_N12
\inst15|inst57|inst1|inst\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst15|inst57|inst1|inst~combout\ = (\inst15|inst4|inst2~0_combout\ & (\SW[4]~input_o\ & \SW[9]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst4|inst2~0_combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[9]~input_o\,
	combout => \inst15|inst57|inst1|inst~combout\);

-- Location: LCCOMB_X7_Y24_N26
\inst25|inst34|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst25|inst34|inst1|inst5~combout\ = \inst15|inst57|inst1|inst5~combout\ $ (\inst25|inst5|inst1|inst~combout\ $ (((\inst15|inst4|inst1|inst5~combout\ & \inst17|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000011101111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst4|inst1|inst5~combout\,
	datab => \inst17|inst4|inst2~0_combout\,
	datac => \inst15|inst57|inst1|inst5~combout\,
	datad => \inst25|inst5|inst1|inst~combout\,
	combout => \inst25|inst34|inst1|inst5~combout\);

-- Location: LCCOMB_X11_Y21_N8
\inst|seven_seg2[4]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~24_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst~combout\,
	combout => \inst|seven_seg2[4]~24_combout\);

-- Location: LCCOMB_X15_Y23_N0
\inst|Equal0~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~9_combout\ = (\inst|seven_seg2[4]~24_combout\ & (\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst|seven_seg2[4]~24_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|Equal0~9_combout\);

-- Location: LCCOMB_X8_Y23_N4
\inst|Equal0~27\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~27_combout\ = (\inst|Equal0~9_combout\ & (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~9_combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|Equal0~27_combout\);

-- Location: LCCOMB_X12_Y23_N16
\inst33|LPM_MUX_component|auto_generated|result_node[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ = (\SW[5]~input_o\ & \SW[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[5]~input_o\,
	datac => \SW[0]~input_o\,
	combout => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\);

-- Location: LCCOMB_X5_Y21_N0
\inst|seven_seg0[6]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~71_combout\ = (\inst|Equal0~8_combout\ & (\inst|Equal0~27_combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~8_combout\,
	datab => \inst|Equal0~27_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[6]~71_combout\);

-- Location: LCCOMB_X5_Y21_N6
\inst|seven_seg0[6]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~248_combout\ = (\inst|seven_seg0[6]~71_combout\) # ((!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~71_combout\,
	combout => \inst|seven_seg0[6]~248_combout\);

-- Location: LCCOMB_X5_Y21_N18
\inst|seven_seg0[6]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~72_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((\inst|seven_seg0[6]~248_combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst|seven_seg0[6]~32_combout\ $ ((!\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101101000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst|seven_seg0[6]~32_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[6]~248_combout\,
	combout => \inst|seven_seg0[6]~72_combout\);

-- Location: LCCOMB_X5_Y21_N26
\inst|seven_seg0[6]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~42_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ $ ((!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\ & 
-- (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)) # (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[6]~42_combout\);

-- Location: LCCOMB_X5_Y21_N28
\inst|seven_seg0[6]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~73_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~72_combout\ & ((\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~42_combout\ & 
-- !\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~72_combout\,
	datac => \inst|seven_seg0[6]~42_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~73_combout\);

-- Location: LCCOMB_X5_Y21_N22
\inst|seven_seg0[6]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~74_combout\ = (\inst|seven_seg0[6]~73_combout\) # ((\inst|seven_seg0[6]~41_combout\ & (\inst32|inst4|inst1|inst5~combout\ $ (\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~41_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~73_combout\,
	combout => \inst|seven_seg0[6]~74_combout\);

-- Location: LCCOMB_X9_Y21_N30
\inst|seven_seg0[6]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~76_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~74_combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg0[6]~75_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~75_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~74_combout\,
	combout => \inst|seven_seg0[6]~76_combout\);

-- Location: LCCOMB_X5_Y21_N12
\inst|seven_seg0[6]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~43_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~41_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~42_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~41_combout\,
	datac => \inst|seven_seg0[6]~42_combout\,
	combout => \inst|seven_seg0[6]~43_combout\);

-- Location: LCCOMB_X9_Y21_N16
\inst|seven_seg0[6]~33\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~33_combout\ = \inst32|inst1|inst|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[6]~33_combout\);

-- Location: LCCOMB_X9_Y21_N18
\inst|seven_seg0[6]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~34_combout\ = (\inst|seven_seg0[6]~32_combout\ & (!\inst|seven_seg0[6]~33_combout\ & (\inst32|inst5|inst1|inst5~combout\ $ (!\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~32_combout\,
	datad => \inst|seven_seg0[6]~33_combout\,
	combout => \inst|seven_seg0[6]~34_combout\);

-- Location: LCCOMB_X4_Y24_N16
\inst|seven_seg0[6]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~38_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst32|inst4|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[6]~38_combout\);

-- Location: LCCOMB_X4_Y24_N10
\inst|seven_seg0[6]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~39_combout\ = (\inst|seven_seg0[6]~38_combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg0[6]~38_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~39_combout\);

-- Location: LCCOMB_X6_Y22_N20
\inst|seven_seg0[6]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~36_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~36_combout\);

-- Location: LCCOMB_X6_Y22_N2
\inst|seven_seg0[6]~35\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~35_combout\ = \inst32|inst1|inst|inst5~combout\ $ (((!\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[6]~35_combout\);

-- Location: LCCOMB_X6_Y22_N26
\inst|seven_seg0[6]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~240_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~240_combout\);

-- Location: LCCOMB_X6_Y22_N30
\inst|seven_seg0[6]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~37_combout\ = (\inst|seven_seg0[6]~35_combout\ & (((\inst|seven_seg0[6]~32_combout\)))) # (!\inst|seven_seg0[6]~35_combout\ & (!\inst|seven_seg0[6]~36_combout\ & (!\inst|seven_seg0[6]~240_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~36_combout\,
	datab => \inst|seven_seg0[6]~35_combout\,
	datac => \inst|seven_seg0[6]~240_combout\,
	datad => \inst|seven_seg0[6]~32_combout\,
	combout => \inst|seven_seg0[6]~37_combout\);

-- Location: LCCOMB_X9_Y21_N4
\inst|seven_seg0[6]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~40_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~39_combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~37_combout\))))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg0[6]~39_combout\ & (!\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111010000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~39_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~37_combout\,
	combout => \inst|seven_seg0[6]~40_combout\);

-- Location: LCCOMB_X9_Y21_N22
\inst|seven_seg0[6]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~44_combout\ = (!\inst32|inst5|inst1|inst5~combout\ & (\inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~44_combout\);

-- Location: LCCOMB_X9_Y21_N24
\inst|seven_seg0[6]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~45_combout\ = (\inst|seven_seg0[6]~34_combout\) # ((\inst|seven_seg0[6]~40_combout\) # ((\inst|seven_seg0[6]~43_combout\ & \inst|seven_seg0[6]~44_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~43_combout\,
	datab => \inst|seven_seg0[6]~34_combout\,
	datac => \inst|seven_seg0[6]~40_combout\,
	datad => \inst|seven_seg0[6]~44_combout\,
	combout => \inst|seven_seg0[6]~45_combout\);

-- Location: LCCOMB_X9_Y21_N6
\inst|seven_seg0[6]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~249_combout\ = (\inst|seven_seg0[6]~76_combout\) # ((\inst|seven_seg0[6]~45_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst|seven_seg0[6]~76_combout\,
	datad => \inst|seven_seg0[6]~45_combout\,
	combout => \inst|seven_seg0[6]~249_combout\);

-- Location: LCCOMB_X10_Y24_N26
\inst32|inst667|inst1|inst5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst32|inst667|inst1|inst5~combout\ = \inst32|inst667|inst1|inst5~0_combout\ $ (((\inst15|inst4|inst2~0_combout\ & (\SW[4]~input_o\ & \SW[9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst4|inst2~0_combout\,
	datab => \SW[4]~input_o\,
	datac => \SW[9]~input_o\,
	datad => \inst32|inst667|inst1|inst5~0_combout\,
	combout => \inst32|inst667|inst1|inst5~combout\);

-- Location: LCCOMB_X6_Y21_N18
\inst|seven_seg0[6]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~59_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst32|inst3|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~59_combout\);

-- Location: LCCOMB_X6_Y21_N26
\inst|seven_seg0[6]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~243_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~243_combout\);

-- Location: LCCOMB_X6_Y21_N14
\inst|seven_seg0[6]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~64_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~243_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~59_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~59_combout\,
	datac => \inst|seven_seg0[6]~243_combout\,
	combout => \inst|seven_seg0[6]~64_combout\);

-- Location: LCCOMB_X4_Y24_N4
\inst|seven_seg0[6]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~63_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst1|inst|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[6]~63_combout\);

-- Location: LCCOMB_X7_Y21_N10
\inst|seven_seg0[6]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~65_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~49_combout\ & \inst|seven_seg0[6]~63_combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg0[6]~64_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~64_combout\,
	datab => \inst|seven_seg0[6]~49_combout\,
	datac => \inst|seven_seg0[6]~63_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~65_combout\);

-- Location: LCCOMB_X6_Y21_N22
\inst|seven_seg0[6]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~245_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst1|inst5~combout\ $ (!\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~245_combout\);

-- Location: LCCOMB_X7_Y21_N0
\inst|seven_seg0[6]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~62_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst|seven_seg0[6]~245_combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & (\inst|seven_seg0[6]~32_combout\ & ((!\inst|seven_seg0[6]~49_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~32_combout\,
	datab => \inst|seven_seg0[6]~245_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[6]~49_combout\,
	combout => \inst|seven_seg0[6]~62_combout\);

-- Location: LCCOMB_X7_Y21_N20
\inst|seven_seg0[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~66_combout\ = (\inst|seven_seg0[6]~65_combout\ & ((\inst|seven_seg0[6]~62_combout\) # (\inst32|inst488|inst1|inst5~combout\ $ (\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[6]~65_combout\ & 
-- (\inst|seven_seg0[6]~62_combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (!\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~65_combout\,
	datab => \inst|seven_seg0[6]~62_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~66_combout\);

-- Location: LCCOMB_X6_Y21_N0
\inst|seven_seg0[6]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~53_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst32|inst3|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~53_combout\);

-- Location: LCCOMB_X6_Y21_N10
\inst|seven_seg0[6]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~54_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~53_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~243_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~243_combout\,
	datad => \inst|seven_seg0[6]~53_combout\,
	combout => \inst|seven_seg0[6]~54_combout\);

-- Location: LCCOMB_X7_Y21_N30
\inst|seven_seg0[6]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~68_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg0[6]~54_combout\)) # (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~49_combout\ & \inst|seven_seg0[6]~63_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~54_combout\,
	datab => \inst|seven_seg0[6]~49_combout\,
	datac => \inst|seven_seg0[6]~63_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~68_combout\);

-- Location: LCCOMB_X5_Y21_N14
\inst|seven_seg0[6]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~55_combout\ = (!\inst|seven_seg0[6]~49_combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst1|inst|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~49_combout\,
	combout => \inst|seven_seg0[6]~55_combout\);

-- Location: LCCOMB_X6_Y21_N20
\inst|seven_seg0[6]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~56_combout\ = (!\inst32|inst1|inst|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~56_combout\);

-- Location: LCCOMB_X6_Y21_N4
\inst|seven_seg0[6]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~244_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst|seven_seg0[6]~56_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst|seven_seg0[6]~56_combout\,
	combout => \inst|seven_seg0[6]~244_combout\);

-- Location: LCCOMB_X6_Y21_N8
\inst|seven_seg0[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~67_combout\ = (\inst|seven_seg0[6]~55_combout\) # (\inst|seven_seg0[6]~244_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~55_combout\,
	datac => \inst|seven_seg0[6]~244_combout\,
	combout => \inst|seven_seg0[6]~67_combout\);

-- Location: LCCOMB_X7_Y21_N8
\inst|seven_seg0[6]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~69_combout\ = (\inst|seven_seg0[6]~68_combout\ & ((\inst|seven_seg0[6]~67_combout\) # (\inst32|inst488|inst1|inst5~combout\ $ (\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[6]~68_combout\ & 
-- (\inst|seven_seg0[6]~67_combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (!\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~68_combout\,
	datab => \inst|seven_seg0[6]~67_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~69_combout\);

-- Location: LCCOMB_X8_Y21_N28
\inst|seven_seg0[6]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~247_combout\ = (\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~69_combout\))) # (!\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~66_combout\)))) # 
-- (!\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~66_combout\)) # (!\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~69_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~66_combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|seven_seg0[6]~69_combout\,
	combout => \inst|seven_seg0[6]~247_combout\);

-- Location: LCCOMB_X6_Y21_N16
\inst|seven_seg0[6]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~58_combout\ = (\inst|seven_seg0[6]~243_combout\ & (\inst32|inst4|inst1|inst5~combout\ $ (\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~243_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~58_combout\);

-- Location: LCCOMB_X6_Y21_N6
\inst|seven_seg0[6]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~57_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~55_combout\) # ((\inst|seven_seg0[6]~244_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~54_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~55_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~244_combout\,
	datad => \inst|seven_seg0[6]~54_combout\,
	combout => \inst|seven_seg0[6]~57_combout\);

-- Location: LCCOMB_X6_Y21_N28
\inst|seven_seg0[6]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~60_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~53_combout\ & \inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~59_combout\ & 
-- ((!\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~59_combout\,
	datac => \inst|seven_seg0[6]~53_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~60_combout\);

-- Location: LCCOMB_X8_Y21_N0
\inst|seven_seg0[6]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~61_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~58_combout\) # ((\inst|seven_seg0[6]~60_combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~57_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~58_combout\,
	datab => \inst|seven_seg0[6]~57_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~60_combout\,
	combout => \inst|seven_seg0[6]~61_combout\);

-- Location: LCCOMB_X8_Y21_N26
\inst|seven_seg0[6]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~246_combout\ = (\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~66_combout\)) # (!\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~61_combout\))))) # 
-- (!\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~61_combout\))) # (!\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~66_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111010000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~66_combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|seven_seg0[6]~61_combout\,
	combout => \inst|seven_seg0[6]~246_combout\);

-- Location: LCCOMB_X8_Y21_N18
\inst|seven_seg0[6]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~70_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~246_combout\) # (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (\inst|seven_seg0[6]~247_combout\ & ((!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~247_combout\,
	datac => \inst|seven_seg0[6]~246_combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[6]~70_combout\);

-- Location: LCCOMB_X9_Y21_N8
\inst|seven_seg0[6]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~51_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~50_combout\) # ((\inst|seven_seg0[6]~241_combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~37_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~50_combout\,
	datac => \inst|seven_seg0[6]~37_combout\,
	datad => \inst|seven_seg0[6]~241_combout\,
	combout => \inst|seven_seg0[6]~51_combout\);

-- Location: LCCOMB_X9_Y21_N26
\inst|seven_seg0[6]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~52_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~48_combout\))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~51_combout\)))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~51_combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~48_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~51_combout\,
	datad => \inst|seven_seg0[6]~48_combout\,
	combout => \inst|seven_seg0[6]~52_combout\);

-- Location: LCCOMB_X9_Y21_N12
\inst|seven_seg0[6]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~242_combout\ = (\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~52_combout\)) # (!\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~45_combout\))))) # 
-- (!\inst25|inst4|inst1|inst5~combout\ & ((\inst32|inst4|inst2~0_combout\ & ((\inst|seven_seg0[6]~45_combout\))) # (!\inst32|inst4|inst2~0_combout\ & (\inst|seven_seg0[6]~52_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst|seven_seg0[6]~52_combout\,
	datad => \inst|seven_seg0[6]~45_combout\,
	combout => \inst|seven_seg0[6]~242_combout\);

-- Location: LCCOMB_X8_Y21_N4
\inst|seven_seg0[6]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~77_combout\ = (\inst|seven_seg0[6]~70_combout\ & ((\inst|seven_seg0[6]~249_combout\) # ((!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))) # (!\inst|seven_seg0[6]~70_combout\ & (((\inst|seven_seg0[6]~242_combout\ 
-- & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~249_combout\,
	datab => \inst|seven_seg0[6]~70_combout\,
	datac => \inst|seven_seg0[6]~242_combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[6]~77_combout\);

-- Location: LCCOMB_X11_Y22_N6
\inst|seven_seg0[5]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~118_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & \inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ & 
-- !\inst32|inst667|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~118_combout\);

-- Location: LCCOMB_X11_Y22_N0
\inst|seven_seg0[5]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~119_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & 
-- \inst32|inst667|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~119_combout\);

-- Location: LCCOMB_X11_Y22_N28
\inst|seven_seg0[5]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~121_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (((!\inst32|inst488|inst1|inst5~combout\ & !\inst|seven_seg0[5]~119_combout\))))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\) # (\inst|seven_seg0[5]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~119_combout\,
	combout => \inst|seven_seg0[5]~121_combout\);

-- Location: LCCOMB_X11_Y22_N26
\inst|seven_seg0[5]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~120_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (!\inst|seven_seg0[5]~119_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (!\inst|seven_seg0[5]~119_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~119_combout\,
	combout => \inst|seven_seg0[5]~120_combout\);

-- Location: LCCOMB_X8_Y23_N8
\inst|seven_seg2[1]~25\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~25_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg2[1]~25_combout\);

-- Location: LCCOMB_X12_Y23_N26
\inst|Equal0~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~28_combout\ = (\inst|Equal0~9_combout\ & (\inst|seven_seg2[1]~25_combout\ & ((!\SW[5]~input_o\) # (!\SW[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[5]~input_o\,
	datac => \inst|Equal0~9_combout\,
	datad => \inst|seven_seg2[1]~25_combout\,
	combout => \inst|Equal0~28_combout\);

-- Location: LCCOMB_X11_Y22_N18
\inst|seven_seg0[5]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~250_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (((\inst|Equal0~8_combout\ & \inst|Equal0~28_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst|Equal0~8_combout\,
	datad => \inst|Equal0~28_combout\,
	combout => \inst|seven_seg0[5]~250_combout\);

-- Location: LCCOMB_X11_Y22_N12
\inst|seven_seg0[5]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~251_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[5]~250_combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~250_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~251_combout\);

-- Location: LCCOMB_X11_Y22_N14
\inst|seven_seg0[5]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~122_combout\ = (\inst|seven_seg0[5]~120_combout\ & (((!\inst|seven_seg0[5]~251_combout\) # (!\inst|seven_seg0[5]~121_combout\)))) # (!\inst|seven_seg0[5]~120_combout\ & (\inst|seven_seg0[5]~118_combout\ $ 
-- ((\inst|seven_seg0[5]~121_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011011110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~118_combout\,
	datab => \inst|seven_seg0[5]~121_combout\,
	datac => \inst|seven_seg0[5]~120_combout\,
	datad => \inst|seven_seg0[5]~251_combout\,
	combout => \inst|seven_seg0[5]~122_combout\);

-- Location: LCCOMB_X14_Y22_N22
\inst|seven_seg0[5]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~82_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ $ (((\inst32|inst3|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~82_combout\);

-- Location: LCCOMB_X14_Y22_N20
\inst|seven_seg0[5]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~81_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ ((!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst488|inst1|inst5~combout\) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~81_combout\);

-- Location: LCCOMB_X14_Y22_N0
\inst|seven_seg0[5]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~83_combout\ = (\inst|seven_seg0[5]~82_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~81_combout\) # (!\inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst32|inst57|inst1|inst5~combout\) # (!\inst|seven_seg0[5]~81_combout\))))) # (!\inst|seven_seg0[5]~82_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst57|inst1|inst5~combout\ $ (\inst|seven_seg0[5]~81_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100101110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~82_combout\,
	datad => \inst|seven_seg0[5]~81_combout\,
	combout => \inst|seven_seg0[5]~83_combout\);

-- Location: LCCOMB_X15_Y22_N28
\inst|seven_seg0[5]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~86_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst5~combout\))))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~86_combout\);

-- Location: LCCOMB_X15_Y22_N26
\inst|seven_seg0[5]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~85_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\) # 
-- (!\inst32|inst488|inst1|inst5~combout\))))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~85_combout\);

-- Location: LCCOMB_X15_Y22_N6
\inst|seven_seg0[5]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~87_combout\ = (\inst|seven_seg0[5]~86_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((!\inst32|inst57|inst1|inst5~combout\) # (!\inst|seven_seg0[5]~85_combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[5]~85_combout\) # (\inst32|inst57|inst1|inst5~combout\))))) # (!\inst|seven_seg0[5]~86_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst|seven_seg0[5]~85_combout\ $ (\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110110111011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~86_combout\,
	datac => \inst|seven_seg0[5]~85_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~87_combout\);

-- Location: LCCOMB_X14_Y22_N18
\inst|seven_seg0[5]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~117_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst5|inst1|inst5~combout\) # (\inst|seven_seg0[5]~87_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[5]~83_combout\ & 
-- (!\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~83_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~87_combout\,
	combout => \inst|seven_seg0[5]~117_combout\);

-- Location: LCCOMB_X15_Y22_N18
\inst|seven_seg0[5]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~115_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101010100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~115_combout\);

-- Location: LCCOMB_X15_Y22_N24
\inst|seven_seg0[5]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~114_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- ((\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001000100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~114_combout\);

-- Location: LCCOMB_X15_Y22_N4
\inst|seven_seg0[5]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~116_combout\ = (\inst|seven_seg0[5]~114_combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~115_combout\) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- ((\inst32|inst667|inst1|inst5~combout\) # (!\inst|seven_seg0[5]~115_combout\))))) # (!\inst|seven_seg0[5]~114_combout\ & (\inst32|inst57|inst1|inst5~combout\ $ (\inst|seven_seg0[5]~115_combout\ $ (!\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101101101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~115_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~114_combout\,
	combout => \inst|seven_seg0[5]~116_combout\);

-- Location: LCCOMB_X14_Y22_N28
\inst|seven_seg0[5]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~123_combout\ = (\inst|seven_seg0[5]~117_combout\ & ((\inst|seven_seg0[5]~122_combout\) # ((!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[5]~117_combout\ & (((\inst32|inst5|inst1|inst5~combout\ & 
-- \inst|seven_seg0[5]~116_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~122_combout\,
	datab => \inst|seven_seg0[5]~117_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~116_combout\,
	combout => \inst|seven_seg0[5]~123_combout\);

-- Location: LCCOMB_X14_Y22_N8
\inst|seven_seg0[5]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~78_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst32|inst488|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~78_combout\);

-- Location: LCCOMB_X15_Y22_N16
\inst|seven_seg0[5]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~79_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & 
-- !\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~79_combout\);

-- Location: LCCOMB_X14_Y22_N2
\inst|seven_seg0[5]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~80_combout\ = (\inst|seven_seg0[5]~78_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((!\inst|seven_seg0[5]~79_combout\) # (!\inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst32|inst57|inst1|inst5~combout\) # (\inst|seven_seg0[5]~79_combout\))))) # (!\inst|seven_seg0[5]~78_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst57|inst1|inst5~combout\ $ (!\inst|seven_seg0[5]~79_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~78_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~79_combout\,
	combout => \inst|seven_seg0[5]~80_combout\);

-- Location: LCCOMB_X14_Y22_N26
\inst|seven_seg0[5]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~84_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst5|inst1|inst5~combout\ & \inst|seven_seg0[5]~80_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[5]~83_combout\ & 
-- (\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~83_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~80_combout\,
	combout => \inst|seven_seg0[5]~84_combout\);

-- Location: LCCOMB_X14_Y22_N4
\inst|seven_seg0[5]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~88_combout\ = (\inst|seven_seg0[5]~84_combout\) # ((\inst|seven_seg0[5]~87_combout\ & (\inst32|inst5|inst1|inst5~combout\ $ (!\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~84_combout\,
	datad => \inst|seven_seg0[5]~87_combout\,
	combout => \inst|seven_seg0[5]~88_combout\);

-- Location: LCCOMB_X14_Y22_N6
\inst|seven_seg0[5]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~99_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[5]~82_combout\ & (\inst32|inst57|inst1|inst5~combout\ $ (!\inst|seven_seg0[5]~79_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg0[5]~82_combout\ & !\inst|seven_seg0[5]~79_combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst|seven_seg0[5]~82_combout\ & \inst|seven_seg0[5]~79_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~82_combout\,
	datad => \inst|seven_seg0[5]~79_combout\,
	combout => \inst|seven_seg0[5]~99_combout\);

-- Location: LCCOMB_X15_Y22_N10
\inst|seven_seg0[5]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~95_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- (!\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~95_combout\);

-- Location: LCCOMB_X15_Y22_N22
\inst|seven_seg0[5]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~97_combout\ = (!\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~97_combout\);

-- Location: LCCOMB_X15_Y23_N2
\inst|Equal0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~10_combout\ = (\inst32|inst1|inst|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|Equal0~10_combout\);

-- Location: LCCOMB_X15_Y23_N12
\inst|seven_seg0[5]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~93_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst|Equal0~10_combout\ & (\inst15|inst57|inst1|inst~combout\ $ (\inst32|inst667|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst|Equal0~10_combout\,
	datad => \inst32|inst667|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[5]~93_combout\);

-- Location: LCCOMB_X15_Y22_N20
\inst|seven_seg0[5]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~109_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\) # ((\inst|seven_seg0[5]~97_combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[5]~93_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~97_combout\,
	datad => \inst|seven_seg0[5]~93_combout\,
	combout => \inst|seven_seg0[5]~109_combout\);

-- Location: LCCOMB_X15_Y22_N2
\inst|seven_seg0[5]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~108_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\) # (\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # (\inst32|inst1|inst|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~108_combout\);

-- Location: LCCOMB_X15_Y22_N14
\inst|seven_seg0[5]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~110_combout\ = (\inst|seven_seg0[5]~109_combout\ & ((\inst|seven_seg0[5]~95_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg0[5]~109_combout\ & (((!\inst|seven_seg0[5]~108_combout\ & 
-- \inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~95_combout\,
	datab => \inst|seven_seg0[5]~109_combout\,
	datac => \inst|seven_seg0[5]~108_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~110_combout\);

-- Location: LCCOMB_X15_Y22_N8
\inst|seven_seg0[5]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~94_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\))))) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~94_combout\);

-- Location: LCCOMB_X15_Y22_N12
\inst|seven_seg0[5]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~96_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~94_combout\))) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & (\inst|seven_seg0[5]~95_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~95_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~94_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~96_combout\);

-- Location: LCCOMB_X15_Y22_N0
\inst|seven_seg0[5]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~98_combout\ = (\inst|seven_seg0[5]~96_combout\ & (((\inst|seven_seg0[5]~97_combout\)) # (!\inst32|inst488|inst1|inst5~combout\))) # (!\inst|seven_seg0[5]~96_combout\ & (\inst32|inst488|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[5]~93_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~96_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[5]~97_combout\,
	datad => \inst|seven_seg0[5]~93_combout\,
	combout => \inst|seven_seg0[5]~98_combout\);

-- Location: LCCOMB_X14_Y22_N12
\inst|seven_seg0[5]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~111_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[5]~98_combout\ & \inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst|seven_seg0[5]~110_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~98_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~110_combout\,
	datac => \inst|seven_seg0[5]~98_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~111_combout\);

-- Location: LCCOMB_X14_Y22_N30
\inst|seven_seg0[5]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~112_combout\ = (\inst|seven_seg0[5]~111_combout\) # ((\inst|seven_seg0[5]~99_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & \inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~99_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~111_combout\,
	combout => \inst|seven_seg0[5]~112_combout\);

-- Location: LCCOMB_X11_Y22_N4
\inst|seven_seg0[5]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~102_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & 
-- ((!\inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & \inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~102_combout\);

-- Location: LCCOMB_X11_Y22_N22
\inst|seven_seg0[5]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~103_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (((\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst57|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~103_combout\);

-- Location: LCCOMB_X11_Y22_N10
\inst|seven_seg0[5]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~105_combout\ = (\inst|seven_seg0[5]~103_combout\ & ((\inst|seven_seg0[5]~102_combout\ & ((\inst32|inst57|inst1|inst5~combout\) # (\inst32|inst1|inst|inst5~combout\))) # (!\inst|seven_seg0[5]~102_combout\ & 
-- ((!\inst32|inst1|inst|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~102_combout\,
	datac => \inst|seven_seg0[5]~103_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~105_combout\);

-- Location: LCCOMB_X11_Y22_N16
\inst|seven_seg0[5]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~104_combout\ = (\inst|seven_seg0[5]~102_combout\ & ((\inst|seven_seg0[5]~103_combout\ & (\inst32|inst57|inst1|inst5~combout\)) # (!\inst|seven_seg0[5]~103_combout\ & ((!\inst32|inst1|inst|inst5~combout\))))) # 
-- (!\inst|seven_seg0[5]~102_combout\ & (((\inst|seven_seg0[5]~103_combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~102_combout\,
	datac => \inst|seven_seg0[5]~103_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[5]~104_combout\);

-- Location: LCCOMB_X11_Y22_N2
\inst|seven_seg0[5]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~101_combout\ = (\inst|Equal0~8_combout\ & (!\inst|Equal0~28_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))) # (!\inst|Equal0~8_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ 
-- ((\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst|Equal0~8_combout\,
	datad => \inst|Equal0~28_combout\,
	combout => \inst|seven_seg0[5]~101_combout\);

-- Location: LCCOMB_X11_Y22_N20
\inst|seven_seg0[5]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~106_combout\ = (\inst|seven_seg0[5]~105_combout\ & ((\inst|seven_seg0[5]~104_combout\ & ((\inst|seven_seg0[5]~101_combout\))) # (!\inst|seven_seg0[5]~104_combout\ & (\inst32|inst667|inst1|inst5~combout\)))) # 
-- (!\inst|seven_seg0[5]~105_combout\ & (\inst|seven_seg0[5]~104_combout\ & (!\inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~105_combout\,
	datab => \inst|seven_seg0[5]~104_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~101_combout\,
	combout => \inst|seven_seg0[5]~106_combout\);

-- Location: LCCOMB_X12_Y22_N20
\inst|seven_seg0[5]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~91_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~91_combout\);

-- Location: LCCOMB_X12_Y22_N16
\inst|seven_seg0[5]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~89_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\) # (\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((!\inst32|inst34|inst1|inst5~combout\))))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & 
-- \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[5]~89_combout\);

-- Location: LCCOMB_X12_Y22_N10
\inst|seven_seg0[5]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~90_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & \inst|seven_seg0[5]~89_combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & !\inst|seven_seg0[5]~89_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~89_combout\,
	combout => \inst|seven_seg0[5]~90_combout\);

-- Location: LCCOMB_X12_Y22_N22
\inst|seven_seg0[5]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~92_combout\ = (\inst|seven_seg0[5]~90_combout\) # ((\inst|seven_seg0[5]~91_combout\ & (\inst32|inst57|inst1|inst5~combout\ $ (!\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~91_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~90_combout\,
	combout => \inst|seven_seg0[5]~92_combout\);

-- Location: LCCOMB_X14_Y22_N16
\inst|seven_seg0[5]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~100_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~98_combout\) # ((\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst5|inst1|inst5~combout\ & 
-- \inst|seven_seg0[5]~99_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[5]~98_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~99_combout\,
	combout => \inst|seven_seg0[5]~100_combout\);

-- Location: LCCOMB_X14_Y22_N10
\inst|seven_seg0[5]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~107_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[5]~100_combout\ & (\inst|seven_seg0[5]~106_combout\)) # (!\inst|seven_seg0[5]~100_combout\ & ((\inst|seven_seg0[5]~92_combout\))))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[5]~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~106_combout\,
	datab => \inst|seven_seg0[5]~92_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[5]~100_combout\,
	combout => \inst|seven_seg0[5]~107_combout\);

-- Location: LCCOMB_X14_Y22_N24
\inst|seven_seg0[5]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~113_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (\inst|seven_seg0[5]~107_combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (\inst|seven_seg0[5]~112_combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[5]~112_combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst|seven_seg0[5]~107_combout\,
	combout => \inst|seven_seg0[5]~113_combout\);

-- Location: LCCOMB_X14_Y22_N14
\inst|seven_seg0[5]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[5]~124_combout\ = (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst|seven_seg0[5]~113_combout\ & (\inst|seven_seg0[5]~123_combout\)) # (!\inst|seven_seg0[5]~113_combout\ & ((\inst|seven_seg0[5]~88_combout\))))) 
-- # (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & (((\inst|seven_seg0[5]~113_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst|seven_seg0[5]~123_combout\,
	datac => \inst|seven_seg0[5]~88_combout\,
	datad => \inst|seven_seg0[5]~113_combout\,
	combout => \inst|seven_seg0[5]~124_combout\);

-- Location: LCCOMB_X12_Y24_N30
\inst|seven_seg0[4]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~129_combout\ = (\inst32|inst488|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\ $ (\inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~129_combout\);

-- Location: LCCOMB_X12_Y24_N8
\inst|seven_seg0[4]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~125_combout\ = (!\inst32|inst488|inst1|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~125_combout\);

-- Location: LCCOMB_X15_Y23_N22
\inst|seven_seg0[4]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~126_combout\ = (\inst|seven_seg2[4]~24_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst|seven_seg2[4]~24_combout\,
	combout => \inst|seven_seg0[4]~126_combout\);

-- Location: LCCOMB_X12_Y24_N18
\inst|seven_seg0[4]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~127_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\ $ (!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100010000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~127_combout\);

-- Location: LCCOMB_X12_Y24_N28
\inst|seven_seg0[4]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~128_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg0[4]~126_combout\)) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~127_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~126_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~127_combout\,
	combout => \inst|seven_seg0[4]~128_combout\);

-- Location: LCCOMB_X12_Y24_N24
\inst|seven_seg0[4]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~130_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~128_combout\ & (!\inst|seven_seg0[4]~129_combout\)) # (!\inst|seven_seg0[4]~128_combout\ & ((\inst|seven_seg0[4]~125_combout\))))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[4]~128_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~129_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst|seven_seg0[4]~125_combout\,
	datad => \inst|seven_seg0[4]~128_combout\,
	combout => \inst|seven_seg0[4]~130_combout\);

-- Location: LCCOMB_X8_Y24_N8
\inst|seven_seg0[4]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~136_combout\ = \inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[4]~136_combout\);

-- Location: LCCOMB_X12_Y24_N10
\inst|seven_seg0[4]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~131_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & 
-- ((\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)) # (!\inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111011010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~131_combout\);

-- Location: LCCOMB_X12_Y24_N20
\inst|seven_seg0[4]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~138_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg0[4]~125_combout\) # (\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg0[4]~126_combout\ & 
-- ((!\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~126_combout\,
	datab => \inst|seven_seg0[4]~125_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~138_combout\);

-- Location: LCCOMB_X12_Y24_N22
\inst|seven_seg0[4]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~139_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~138_combout\ & (!\inst|seven_seg0[4]~131_combout\)) # (!\inst|seven_seg0[4]~138_combout\ & ((!\inst|seven_seg0[4]~129_combout\))))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[4]~138_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~131_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst|seven_seg0[4]~129_combout\,
	datad => \inst|seven_seg0[4]~138_combout\,
	combout => \inst|seven_seg0[4]~139_combout\);

-- Location: LCCOMB_X8_Y24_N10
\inst|seven_seg0[4]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~140_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~139_combout\) # (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & 
-- \inst|seven_seg0[4]~139_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[4]~139_combout\,
	combout => \inst|seven_seg0[4]~140_combout\);

-- Location: LCCOMB_X12_Y24_N16
\inst|seven_seg0[4]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~141_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst5|inst1|inst5~combout\)) # (!\inst|seven_seg0[4]~129_combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg0[4]~125_combout\ & 
-- !\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~129_combout\,
	datab => \inst|seven_seg0[4]~125_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~141_combout\);

-- Location: LCCOMB_X12_Y24_N26
\inst|seven_seg0[4]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~142_combout\ = (\inst|seven_seg0[4]~141_combout\ & (((\inst|seven_seg0[4]~127_combout\) # (!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[4]~141_combout\ & (!\inst|seven_seg0[4]~131_combout\ & 
-- (\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110000011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~131_combout\,
	datab => \inst|seven_seg0[4]~141_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~127_combout\,
	combout => \inst|seven_seg0[4]~142_combout\);

-- Location: LCCOMB_X11_Y24_N26
\inst|seven_seg0[4]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~143_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~142_combout\) # (!\inst|seven_seg0[4]~140_combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (((\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst|seven_seg0[4]~140_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~142_combout\,
	combout => \inst|seven_seg0[4]~143_combout\);

-- Location: LCCOMB_X11_Y24_N20
\inst|seven_seg0[4]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~144_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((!\inst|seven_seg0[4]~140_combout\ & \inst|seven_seg0[4]~142_combout\)) # (!\inst32|inst4|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (!\inst|seven_seg0[4]~140_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & \inst|seven_seg0[4]~142_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst|seven_seg0[4]~140_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~142_combout\,
	combout => \inst|seven_seg0[4]~144_combout\);

-- Location: LCCOMB_X11_Y24_N24
\inst|seven_seg0[4]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~146_combout\ = (\inst|seven_seg0[4]~140_combout\ & ((\inst|seven_seg0[4]~136_combout\ & ((!\inst|seven_seg0[4]~144_combout\))) # (!\inst|seven_seg0[4]~136_combout\ & ((\inst|seven_seg0[4]~143_combout\) # 
-- (\inst|seven_seg0[4]~144_combout\))))) # (!\inst|seven_seg0[4]~140_combout\ & (((\inst|seven_seg0[4]~143_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~136_combout\,
	datab => \inst|seven_seg0[4]~140_combout\,
	datac => \inst|seven_seg0[4]~143_combout\,
	datad => \inst|seven_seg0[4]~144_combout\,
	combout => \inst|seven_seg0[4]~146_combout\);

-- Location: LCCOMB_X11_Y24_N14
\inst|seven_seg0[4]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~145_combout\ = (\inst|seven_seg0[4]~136_combout\ & ((\inst|seven_seg0[4]~140_combout\ & (\inst|seven_seg0[4]~143_combout\)) # (!\inst|seven_seg0[4]~140_combout\ & ((\inst|seven_seg0[4]~144_combout\))))) # 
-- (!\inst|seven_seg0[4]~136_combout\ & ((\inst|seven_seg0[4]~140_combout\) # ((\inst|seven_seg0[4]~143_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~136_combout\,
	datab => \inst|seven_seg0[4]~140_combout\,
	datac => \inst|seven_seg0[4]~143_combout\,
	datad => \inst|seven_seg0[4]~144_combout\,
	combout => \inst|seven_seg0[4]~145_combout\);

-- Location: LCCOMB_X12_Y24_N0
\inst|seven_seg0[4]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~134_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~127_combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (!\inst|seven_seg0[4]~131_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000111000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~131_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~127_combout\,
	combout => \inst|seven_seg0[4]~134_combout\);

-- Location: LCCOMB_X12_Y24_N2
\inst|seven_seg0[4]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~135_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~134_combout\ & ((\inst|seven_seg0[4]~125_combout\))) # (!\inst|seven_seg0[4]~134_combout\ & (\inst|seven_seg0[4]~126_combout\)))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[4]~134_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~126_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst|seven_seg0[4]~125_combout\,
	datad => \inst|seven_seg0[4]~134_combout\,
	combout => \inst|seven_seg0[4]~135_combout\);

-- Location: LCCOMB_X12_Y24_N4
\inst|seven_seg0[4]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~132_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\) # ((!\inst|seven_seg0[4]~131_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst5|inst1|inst5~combout\ & 
-- (!\inst|seven_seg0[4]~129_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100110101011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst|seven_seg0[4]~129_combout\,
	datad => \inst|seven_seg0[4]~131_combout\,
	combout => \inst|seven_seg0[4]~132_combout\);

-- Location: LCCOMB_X12_Y24_N6
\inst|seven_seg0[4]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~133_combout\ = (\inst|seven_seg0[4]~132_combout\ & ((\inst|seven_seg0[4]~126_combout\) # ((!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[4]~132_combout\ & (((\inst32|inst5|inst1|inst5~combout\ & 
-- \inst|seven_seg0[4]~127_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~126_combout\,
	datab => \inst|seven_seg0[4]~132_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~127_combout\,
	combout => \inst|seven_seg0[4]~133_combout\);

-- Location: LCCOMB_X11_Y24_N0
\inst|seven_seg0[4]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~137_combout\ = (\inst|seven_seg0[4]~135_combout\ & ((\inst|seven_seg0[4]~133_combout\) # (\inst|seven_seg0[4]~136_combout\ $ (!\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst|seven_seg0[4]~135_combout\ & 
-- (\inst|seven_seg0[4]~133_combout\ & (\inst|seven_seg0[4]~136_combout\ $ (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111010000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~135_combout\,
	datab => \inst|seven_seg0[4]~136_combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst|seven_seg0[4]~133_combout\,
	combout => \inst|seven_seg0[4]~137_combout\);

-- Location: LCCOMB_X11_Y24_N18
\inst|seven_seg0[4]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~147_combout\ = (\inst|seven_seg0[4]~146_combout\ & ((\inst|seven_seg0[4]~145_combout\ & ((\inst|seven_seg0[4]~137_combout\))) # (!\inst|seven_seg0[4]~145_combout\ & (\inst|seven_seg0[4]~130_combout\)))) # 
-- (!\inst|seven_seg0[4]~146_combout\ & (((\inst|seven_seg0[4]~145_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~130_combout\,
	datab => \inst|seven_seg0[4]~146_combout\,
	datac => \inst|seven_seg0[4]~145_combout\,
	datad => \inst|seven_seg0[4]~137_combout\,
	combout => \inst|seven_seg0[4]~147_combout\);

-- Location: LCCOMB_X9_Y23_N16
\inst|Equal0~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~11_combout\ = (\inst32|inst3|inst1|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|Equal0~11_combout\);

-- Location: LCCOMB_X9_Y24_N8
\inst|Equal0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~12_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & (\inst|Equal0~11_combout\ & \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|Equal0~11_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|Equal0~12_combout\);

-- Location: LCCOMB_X11_Y23_N8
\inst|seven_seg0[4]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~148_combout\ = (\inst25|inst5|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst57|inst1|inst5~combout\ & \inst25|inst57|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst25|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[4]~148_combout\);

-- Location: LCCOMB_X12_Y23_N2
\inst|seven_seg0[4]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~149_combout\ = (\inst|Equal0~12_combout\ & \inst|seven_seg0[4]~148_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~12_combout\,
	datad => \inst|seven_seg0[4]~148_combout\,
	combout => \inst|seven_seg0[4]~149_combout\);

-- Location: LCCOMB_X10_Y22_N0
\inst|Equal0~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~13_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst3|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|Equal0~13_combout\);

-- Location: LCCOMB_X12_Y23_N28
\inst|Equal0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~14_combout\ = (\inst|seven_seg2[1]~25_combout\ & (\inst|Equal0~13_combout\ & (\inst|Equal0~9_combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[1]~25_combout\,
	datab => \inst|Equal0~13_combout\,
	datac => \inst|Equal0~9_combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|Equal0~14_combout\);

-- Location: LCCOMB_X12_Y23_N22
\inst|seven_seg0[4]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[4]~150_combout\ = (\inst|seven_seg0[4]~149_combout\ & (((!\inst|Equal0~14_combout\)))) # (!\inst|seven_seg0[4]~149_combout\ & ((\inst|seven_seg0[4]~147_combout\) # ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~147_combout\,
	datab => \inst|seven_seg0[4]~149_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst|Equal0~14_combout\,
	combout => \inst|seven_seg0[4]~150_combout\);

-- Location: LCCOMB_X7_Y21_N2
\inst|seven_seg0[3]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~161_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~64_combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~62_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~64_combout\,
	datab => \inst|seven_seg0[6]~62_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[3]~161_combout\);

-- Location: LCCOMB_X8_Y21_N12
\inst|seven_seg0[3]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~162_combout\ = (\inst|seven_seg0[3]~161_combout\) # ((!\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~58_combout\) # (\inst|seven_seg0[6]~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~58_combout\,
	datab => \inst|seven_seg0[6]~60_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[3]~161_combout\,
	combout => \inst|seven_seg0[3]~162_combout\);

-- Location: LCCOMB_X12_Y23_N12
\inst|Equal0~29\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~29_combout\ = (\SW[0]~input_o\ & (\SW[5]~input_o\ & (\inst|Equal0~9_combout\ & \inst|seven_seg2[1]~25_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[0]~input_o\,
	datab => \SW[5]~input_o\,
	datac => \inst|Equal0~9_combout\,
	datad => \inst|seven_seg2[1]~25_combout\,
	combout => \inst|Equal0~29_combout\);

-- Location: LCCOMB_X6_Y21_N2
\inst|seven_seg0[6]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~154_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((\inst|Equal0~29_combout\ & \inst|Equal0~8_combout\)) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~29_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|Equal0~8_combout\,
	combout => \inst|seven_seg0[6]~154_combout\);

-- Location: LCCOMB_X6_Y21_N12
\inst|seven_seg0[6]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~163_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst|seven_seg0[6]~154_combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~154_combout\,
	combout => \inst|seven_seg0[6]~163_combout\);

-- Location: LCCOMB_X6_Y21_N30
\inst|seven_seg0[6]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~164_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~163_combout\ & (\inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~243_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~163_combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst|seven_seg0[6]~243_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~164_combout\);

-- Location: LCCOMB_X6_Y21_N24
\inst|seven_seg0[6]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~165_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~164_combout\))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~53_combout\)))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~53_combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~164_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~53_combout\,
	datac => \inst|seven_seg0[6]~164_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~165_combout\);

-- Location: LCCOMB_X7_Y21_N4
\inst|seven_seg0[6]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~166_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~63_combout\ & \inst|seven_seg0[6]~49_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~67_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~67_combout\,
	datab => \inst|seven_seg0[6]~63_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~49_combout\,
	combout => \inst|seven_seg0[6]~166_combout\);

-- Location: LCCOMB_X7_Y21_N6
\inst|seven_seg0[6]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~167_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg0[6]~165_combout\)) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~166_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~165_combout\,
	datac => \inst|seven_seg0[6]~166_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~167_combout\);

-- Location: LCCOMB_X7_Y21_N16
\inst|seven_seg0[6]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~168_combout\ = (\inst|seven_seg0[6]~49_combout\ & (\inst|seven_seg0[6]~63_combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (!\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~49_combout\,
	datac => \inst|seven_seg0[6]~63_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~168_combout\);

-- Location: LCCOMB_X7_Y21_N26
\inst|seven_seg0[6]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~169_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[6]~62_combout\ & !\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[6]~67_combout\ & 
-- ((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~67_combout\,
	datab => \inst|seven_seg0[6]~62_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~169_combout\);

-- Location: LCCOMB_X7_Y21_N28
\inst|seven_seg0[6]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~170_combout\ = (\inst|seven_seg0[6]~167_combout\) # ((!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~168_combout\) # (\inst|seven_seg0[6]~169_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~167_combout\,
	datab => \inst|seven_seg0[6]~168_combout\,
	datac => \inst|seven_seg0[6]~169_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~170_combout\);

-- Location: LCCOMB_X8_Y21_N22
\inst|seven_seg0[6]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~252_combout\ = \inst25|inst4|inst1|inst5~combout\ $ (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst32|inst4|inst2~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg0[6]~252_combout\);

-- Location: LCCOMB_X8_Y21_N14
\inst|seven_seg0[3]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~171_combout\ = (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst|seven_seg0[6]~252_combout\ & (\inst|seven_seg0[3]~162_combout\)) # (!\inst|seven_seg0[6]~252_combout\ & 
-- ((\inst|seven_seg0[6]~170_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[3]~162_combout\,
	datab => \inst|seven_seg0[6]~170_combout\,
	datac => \inst|seven_seg0[6]~252_combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[3]~171_combout\);

-- Location: LCCOMB_X11_Y21_N2
\inst|seven_seg0[0]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~151_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst~combout\ $ (\inst25|inst34|inst1|inst5~combout\)) # (!\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg0[0]~151_combout\);

-- Location: LCCOMB_X11_Y23_N10
\inst|seven_seg0[0]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~152_combout\ = (\inst32|inst4|inst2~0_combout\ & (!\inst25|inst4|inst1|inst5~combout\ & (\inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\)))) # (!\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ 
-- & (\inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst|seven_seg0[0]~152_combout\);

-- Location: LCCOMB_X8_Y21_N30
\inst|seven_seg0[3]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~153_combout\ = (\inst|seven_seg0[0]~151_combout\ & (((\inst|seven_seg0[0]~152_combout\)))) # (!\inst|seven_seg0[0]~151_combout\ & ((\inst|seven_seg0[0]~152_combout\ & (\inst|seven_seg0[6]~43_combout\)) # 
-- (!\inst|seven_seg0[0]~152_combout\ & ((\inst|seven_seg0[6]~45_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~43_combout\,
	datab => \inst|seven_seg0[6]~45_combout\,
	datac => \inst|seven_seg0[0]~151_combout\,
	datad => \inst|seven_seg0[0]~152_combout\,
	combout => \inst|seven_seg0[3]~153_combout\);

-- Location: LCCOMB_X5_Y21_N24
\inst|seven_seg0[0]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~155_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[0]~155_combout\);

-- Location: LCCOMB_X5_Y21_N10
\inst|seven_seg0[0]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~156_combout\ = (\inst32|inst4|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[0]~156_combout\);

-- Location: LCCOMB_X5_Y21_N4
\inst|seven_seg0[3]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~157_combout\ = (\inst|seven_seg0[0]~156_combout\ & (((!\inst|seven_seg0[6]~32_combout\ & !\inst|seven_seg0[0]~155_combout\)))) # (!\inst|seven_seg0[0]~156_combout\ & ((\inst|seven_seg0[6]~41_combout\) # 
-- ((\inst|seven_seg0[0]~155_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[0]~156_combout\,
	datab => \inst|seven_seg0[6]~41_combout\,
	datac => \inst|seven_seg0[6]~32_combout\,
	datad => \inst|seven_seg0[0]~155_combout\,
	combout => \inst|seven_seg0[3]~157_combout\);

-- Location: LCCOMB_X5_Y21_N30
\inst|seven_seg0[3]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~158_combout\ = (\inst|seven_seg0[0]~155_combout\ & ((\inst|seven_seg0[3]~157_combout\ & (\inst|seven_seg0[6]~248_combout\)) # (!\inst|seven_seg0[3]~157_combout\ & ((\inst|seven_seg0[6]~154_combout\))))) # 
-- (!\inst|seven_seg0[0]~155_combout\ & (((\inst|seven_seg0[3]~157_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~248_combout\,
	datab => \inst|seven_seg0[0]~155_combout\,
	datac => \inst|seven_seg0[3]~157_combout\,
	datad => \inst|seven_seg0[6]~154_combout\,
	combout => \inst|seven_seg0[3]~158_combout\);

-- Location: LCCOMB_X8_Y21_N24
\inst|seven_seg0[3]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~159_combout\ = (\inst|seven_seg0[0]~151_combout\ & ((\inst|seven_seg0[3]~153_combout\ & ((\inst|seven_seg0[3]~158_combout\))) # (!\inst|seven_seg0[3]~153_combout\ & (\inst|seven_seg0[6]~75_combout\)))) # 
-- (!\inst|seven_seg0[0]~151_combout\ & (((\inst|seven_seg0[3]~153_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~75_combout\,
	datab => \inst|seven_seg0[0]~151_combout\,
	datac => \inst|seven_seg0[3]~153_combout\,
	datad => \inst|seven_seg0[3]~158_combout\,
	combout => \inst|seven_seg0[3]~159_combout\);

-- Location: LCCOMB_X8_Y21_N2
\inst|seven_seg0[3]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~160_combout\ = (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[3]~159_combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[6]~242_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[3]~159_combout\,
	datac => \inst|seven_seg0[6]~242_combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[3]~160_combout\);

-- Location: LCCOMB_X8_Y21_N8
\inst|seven_seg0[3]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[3]~172_combout\ = (\inst|seven_seg0[3]~171_combout\) # (\inst|seven_seg0[3]~160_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|seven_seg0[3]~171_combout\,
	datad => \inst|seven_seg0[3]~160_combout\,
	combout => \inst|seven_seg0[3]~172_combout\);

-- Location: LCCOMB_X11_Y24_N4
\inst|seven_seg0[2]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~177_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[4]~130_combout\)) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~142_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~130_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~142_combout\,
	combout => \inst|seven_seg0[2]~177_combout\);

-- Location: LCCOMB_X11_Y24_N30
\inst|seven_seg0[2]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~178_combout\ = (\inst|seven_seg0[2]~177_combout\ & (((\inst|seven_seg0[4]~139_combout\) # (!\inst32|inst1|inst|inst5~combout\)))) # (!\inst|seven_seg0[2]~177_combout\ & (\inst|seven_seg0[4]~133_combout\ & 
-- (\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~133_combout\,
	datab => \inst|seven_seg0[2]~177_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[4]~139_combout\,
	combout => \inst|seven_seg0[2]~178_combout\);

-- Location: LCCOMB_X11_Y24_N16
\inst|seven_seg0[2]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~175_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[4]~135_combout\)) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~139_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~135_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~139_combout\,
	combout => \inst|seven_seg0[2]~175_combout\);

-- Location: LCCOMB_X11_Y24_N2
\inst|seven_seg0[2]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~176_combout\ = (\inst|seven_seg0[2]~175_combout\ & ((\inst|seven_seg0[4]~130_combout\) # ((!\inst32|inst1|inst|inst5~combout\)))) # (!\inst|seven_seg0[2]~175_combout\ & (((\inst32|inst1|inst|inst5~combout\ & 
-- \inst|seven_seg0[4]~142_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~130_combout\,
	datab => \inst|seven_seg0[2]~175_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[4]~142_combout\,
	combout => \inst|seven_seg0[2]~176_combout\);

-- Location: LCCOMB_X11_Y24_N8
\inst|seven_seg0[2]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~179_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg0[2]~176_combout\))) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & (\inst|seven_seg0[2]~178_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst|seven_seg0[2]~178_combout\,
	datad => \inst|seven_seg0[2]~176_combout\,
	combout => \inst|seven_seg0[2]~179_combout\);

-- Location: LCCOMB_X11_Y24_N12
\inst|seven_seg0[2]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~173_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~142_combout\))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[4]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~135_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~142_combout\,
	combout => \inst|seven_seg0[2]~173_combout\);

-- Location: LCCOMB_X11_Y24_N22
\inst|seven_seg0[2]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~174_combout\ = (\inst|seven_seg0[2]~173_combout\ & (((\inst|seven_seg0[4]~133_combout\)) # (!\inst32|inst1|inst|inst5~combout\))) # (!\inst|seven_seg0[2]~173_combout\ & (\inst32|inst1|inst|inst5~combout\ & 
-- (\inst|seven_seg0[4]~130_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[2]~173_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst|seven_seg0[4]~130_combout\,
	datad => \inst|seven_seg0[4]~133_combout\,
	combout => \inst|seven_seg0[2]~174_combout\);

-- Location: LCCOMB_X11_Y24_N10
\inst|seven_seg0[2]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~180_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[4]~139_combout\))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[4]~133_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[4]~133_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~139_combout\,
	combout => \inst|seven_seg0[2]~180_combout\);

-- Location: LCCOMB_X12_Y24_N12
\inst|seven_seg0[2]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~181_combout\ = ((\inst32|inst34|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))) # (!\inst32|inst488|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[2]~181_combout\);

-- Location: LCCOMB_X12_Y24_N14
\inst|seven_seg0[2]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~182_combout\ = (\inst|seven_seg0[4]~141_combout\ & (((!\inst32|inst5|inst1|inst5~combout\)) # (!\inst|seven_seg0[2]~181_combout\))) # (!\inst|seven_seg0[4]~141_combout\ & (((\inst32|inst5|inst1|inst5~combout\ & 
-- !\inst|seven_seg0[4]~131_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110001111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[2]~181_combout\,
	datab => \inst|seven_seg0[4]~141_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[4]~131_combout\,
	combout => \inst|seven_seg0[2]~182_combout\);

-- Location: LCCOMB_X11_Y24_N28
\inst|seven_seg0[2]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~183_combout\ = (\inst|seven_seg0[2]~180_combout\ & ((\inst|seven_seg0[2]~182_combout\) # ((!\inst32|inst1|inst|inst5~combout\)))) # (!\inst|seven_seg0[2]~180_combout\ & (((\inst32|inst1|inst|inst5~combout\ & 
-- \inst|seven_seg0[4]~135_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[2]~180_combout\,
	datab => \inst|seven_seg0[2]~182_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg0[4]~135_combout\,
	combout => \inst|seven_seg0[2]~183_combout\);

-- Location: LCCOMB_X11_Y24_N6
\inst|seven_seg0[2]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~184_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & ((\inst|seven_seg0[2]~179_combout\ & ((\inst|seven_seg0[2]~183_combout\))) # (!\inst|seven_seg0[2]~179_combout\ & (\inst|seven_seg0[2]~174_combout\)))) # 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst|seven_seg0[2]~179_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst|seven_seg0[2]~179_combout\,
	datac => \inst|seven_seg0[2]~174_combout\,
	datad => \inst|seven_seg0[2]~183_combout\,
	combout => \inst|seven_seg0[2]~184_combout\);

-- Location: LCCOMB_X12_Y23_N24
\inst|seven_seg0[2]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[2]~185_combout\ = (\inst|seven_seg0[4]~149_combout\ & (((\inst|Equal0~14_combout\)))) # (!\inst|seven_seg0[4]~149_combout\ & (\inst|seven_seg0[2]~184_combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[2]~184_combout\,
	datab => \inst|seven_seg0[4]~149_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst|Equal0~14_combout\,
	combout => \inst|seven_seg0[2]~185_combout\);

-- Location: LCCOMB_X7_Y24_N22
\inst|seven_seg0[1]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~186_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ (\inst32|inst667|inst1|inst5~combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & 
-- (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ 
-- (\inst32|inst667|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100110010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~186_combout\);

-- Location: LCCOMB_X7_Y24_N12
\inst|seven_seg0[1]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~189_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (\inst32|inst667|inst1|inst5~combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ (!\inst32|inst667|inst1|inst5~combout\))))) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\) # 
-- ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~189_combout\);

-- Location: LCCOMB_X7_Y24_N8
\inst|seven_seg0[1]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~198_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (!\inst32|inst57|inst1|inst5~combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~198_combout\);

-- Location: LCCOMB_X7_Y24_N2
\inst|seven_seg0[1]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~203_combout\ = (\inst32|inst1|inst|inst5~combout\ & (((\inst32|inst488|inst1|inst5~combout\) # (\inst|seven_seg0[1]~198_combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & (!\inst|seven_seg0[1]~189_combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110111000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~189_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~198_combout\,
	combout => \inst|seven_seg0[1]~203_combout\);

-- Location: LCCOMB_X7_Y24_N10
\inst|seven_seg0[1]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~199_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst667|inst1|inst5~combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ (\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~199_combout\);

-- Location: LCCOMB_X7_Y24_N20
\inst|seven_seg0[1]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~204_combout\ = (\inst|seven_seg0[1]~203_combout\ & ((\inst|seven_seg0[1]~186_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg0[1]~203_combout\ & (((\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg0[1]~199_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~186_combout\,
	datab => \inst|seven_seg0[1]~203_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~199_combout\,
	combout => \inst|seven_seg0[1]~204_combout\);

-- Location: LCCOMB_X7_Y24_N14
\inst|seven_seg0[1]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~201_combout\ = (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst57|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~201_combout\);

-- Location: LCCOMB_X7_Y24_N28
\inst|seven_seg0[1]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~200_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\ & (\inst|seven_seg0[1]~186_combout\)) # 
-- (!\inst32|inst1|inst|inst5~combout\ & ((\inst|seven_seg0[1]~199_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst|seven_seg0[1]~186_combout\,
	datad => \inst|seven_seg0[1]~199_combout\,
	combout => \inst|seven_seg0[1]~200_combout\);

-- Location: LCCOMB_X7_Y24_N24
\inst|seven_seg0[1]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~202_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~200_combout\ & ((\inst|seven_seg0[1]~201_combout\))) # (!\inst|seven_seg0[1]~200_combout\ & (\inst|seven_seg0[1]~198_combout\)))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg0[1]~200_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg0[1]~198_combout\,
	datac => \inst|seven_seg0[1]~201_combout\,
	datad => \inst|seven_seg0[1]~200_combout\,
	combout => \inst|seven_seg0[1]~202_combout\);

-- Location: LCCOMB_X10_Y21_N20
\inst|seven_seg0[1]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~205_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~202_combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst|seven_seg0[1]~204_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|seven_seg0[1]~204_combout\,
	datac => \inst|seven_seg0[1]~202_combout\,
	combout => \inst|seven_seg0[1]~205_combout\);

-- Location: LCCOMB_X12_Y21_N18
\inst|seven_seg0[1]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~196_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst57|inst1|inst5~combout\))))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\) # (!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100100100011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~196_combout\);

-- Location: LCCOMB_X12_Y21_N24
\inst|seven_seg0[1]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~195_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ $ (!\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\ & !\inst32|inst57|inst1|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~195_combout\);

-- Location: LCCOMB_X12_Y21_N28
\inst|seven_seg0[1]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~197_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~196_combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst|seven_seg0[1]~195_combout\)) # (!\inst|seven_seg0[1]~196_combout\ 
-- & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst|seven_seg0[1]~195_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[1]~196_combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ 
-- $ (\inst|seven_seg0[1]~195_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg0[1]~196_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst|seven_seg0[1]~195_combout\,
	combout => \inst|seven_seg0[1]~197_combout\);

-- Location: LCCOMB_X10_Y21_N30
\inst|seven_seg0[1]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~206_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst|seven_seg0[1]~205_combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst|seven_seg0[1]~197_combout\))))) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((\inst|seven_seg0[1]~197_combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst|seven_seg0[1]~205_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|seven_seg0[1]~205_combout\,
	datac => \inst|seven_seg0[1]~197_combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[1]~206_combout\);

-- Location: LCCOMB_X7_Y24_N0
\inst|seven_seg0[1]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~187_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst57|inst1|inst5~combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst57|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~187_combout\);

-- Location: LCCOMB_X7_Y24_N18
\inst|seven_seg0[1]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~188_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[1]~187_combout\ & (\inst32|inst1|inst|inst5~combout\ $ 
-- (!\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~187_combout\,
	combout => \inst|seven_seg0[1]~188_combout\);

-- Location: LCCOMB_X7_Y24_N6
\inst|seven_seg0[1]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~190_combout\ = (\inst|seven_seg0[1]~188_combout\ & (((!\inst|seven_seg0[1]~189_combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg0[1]~188_combout\ & (\inst|seven_seg0[1]~186_combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~186_combout\,
	datab => \inst|seven_seg0[1]~188_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~189_combout\,
	combout => \inst|seven_seg0[1]~190_combout\);

-- Location: LCCOMB_X11_Y22_N24
\inst|seven_seg0[1]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~191_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- ((\inst32|inst488|inst1|inst5~combout\) # (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))) # (!\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & 
-- (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst488|inst1|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~191_combout\);

-- Location: LCCOMB_X12_Y21_N26
\inst|seven_seg0[1]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~192_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg0[1]~191_combout\ & \inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ & (!\inst|seven_seg0[1]~191_combout\ & !\inst32|inst57|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg0[1]~191_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~192_combout\);

-- Location: LCCOMB_X12_Y21_N4
\inst|seven_seg0[1]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~193_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst667|inst1|inst5~combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~193_combout\);

-- Location: LCCOMB_X12_Y21_N22
\inst|seven_seg0[1]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~194_combout\ = (\inst|seven_seg0[1]~192_combout\) # ((\inst|seven_seg0[1]~193_combout\ & (\inst32|inst57|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~192_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg0[1]~193_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[1]~194_combout\);

-- Location: LCCOMB_X10_Y21_N24
\inst|seven_seg0[1]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~207_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & (\inst|seven_seg0[1]~190_combout\)) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~194_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg0[1]~190_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~194_combout\,
	combout => \inst|seven_seg0[1]~207_combout\);

-- Location: LCCOMB_X10_Y21_N10
\inst|seven_seg0[1]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~208_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg0[1]~207_combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~49_combout\ & (\inst|seven_seg0[1]~206_combout\)) # 
-- (!\inst|seven_seg0[6]~49_combout\ & ((\inst|seven_seg0[1]~207_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~206_combout\,
	datab => \inst|seven_seg0[1]~207_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~49_combout\,
	combout => \inst|seven_seg0[1]~208_combout\);

-- Location: LCCOMB_X7_Y23_N30
\inst|seven_seg0[1]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~212_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst32|inst488|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~212_combout\);

-- Location: LCCOMB_X7_Y23_N24
\inst|seven_seg0[1]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~213_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst488|inst1|inst5~combout\ $ (((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (!\inst32|inst34|inst1|inst5~combout\))))) # 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst488|inst1|inst5~combout\) # ((\inst32|inst34|inst1|inst5~combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110010110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~213_combout\);

-- Location: LCCOMB_X7_Y23_N10
\inst|seven_seg0[1]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~214_combout\ = (\inst|seven_seg0[1]~212_combout\ & ((\inst|seven_seg0[1]~213_combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\)) # (!\inst|seven_seg0[1]~213_combout\ & 
-- (!\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))) # (!\inst|seven_seg0[1]~212_combout\ & (!\inst|seven_seg0[1]~213_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000100010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~212_combout\,
	datab => \inst|seven_seg0[1]~213_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[1]~214_combout\);

-- Location: LCCOMB_X7_Y23_N6
\inst|seven_seg0[1]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~216_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)) # 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst1|inst|inst5~combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ $ 
-- (((\inst32|inst1|inst|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010001000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~216_combout\);

-- Location: LCCOMB_X7_Y23_N4
\inst|seven_seg0[1]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~215_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # 
-- (!\inst32|inst1|inst|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ (((\inst32|inst488|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110111010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~215_combout\);

-- Location: LCCOMB_X7_Y23_N0
\inst|seven_seg0[1]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~217_combout\ = (\inst|seven_seg0[1]~216_combout\ & ((\inst|seven_seg0[1]~215_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)) # (!\inst|seven_seg0[1]~215_combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\))))) # (!\inst|seven_seg0[1]~216_combout\ & (\inst|seven_seg0[1]~215_combout\ & (\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~216_combout\,
	datab => \inst|seven_seg0[1]~215_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~217_combout\);

-- Location: LCCOMB_X7_Y23_N26
\inst|seven_seg0[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~218_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg0[1]~214_combout\)) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~217_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~214_combout\,
	datab => \inst|seven_seg0[1]~217_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~218_combout\);

-- Location: LCCOMB_X7_Y23_N18
\inst|seven_seg0[1]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~210_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst488|inst1|inst5~combout\ & ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (!\inst32|inst34|inst1|inst5~combout\)))) # 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110000110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~210_combout\);

-- Location: LCCOMB_X7_Y23_N16
\inst|seven_seg0[1]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~209_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ $ (((\inst32|inst488|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst488|inst1|inst5~combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (\inst32|inst488|inst1|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100010100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~209_combout\);

-- Location: LCCOMB_X7_Y23_N20
\inst|seven_seg0[1]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~211_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst|seven_seg0[1]~210_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (!\inst|seven_seg0[1]~209_combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- ((\inst|seven_seg0[1]~210_combout\ & (\inst32|inst667|inst1|inst5~combout\ & !\inst|seven_seg0[1]~209_combout\)) # (!\inst|seven_seg0[1]~210_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & \inst|seven_seg0[1]~209_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000101001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst|seven_seg0[1]~210_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~209_combout\,
	combout => \inst|seven_seg0[1]~211_combout\);

-- Location: LCCOMB_X8_Y23_N2
\inst|Equal0~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~15_combout\ = (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & (\inst|seven_seg2[1]~25_combout\ & (\inst32|inst3|inst1|inst5~combout\ & \inst|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst|seven_seg2[1]~25_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|Equal0~15_combout\);

-- Location: LCCOMB_X7_Y23_N22
\inst|seven_seg0[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~220_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst1|inst|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~220_combout\);

-- Location: LCCOMB_X7_Y23_N28
\inst|seven_seg0[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~219_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (!\inst32|inst1|inst|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst1|inst|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg0[1]~219_combout\);

-- Location: LCCOMB_X7_Y23_N2
\inst|seven_seg0[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~222_combout\ = (\inst|seven_seg0[1]~220_combout\ & ((\inst|seven_seg0[1]~219_combout\) # ((!\inst32|inst667|inst1|inst5~combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))) # 
-- (!\inst|seven_seg0[1]~220_combout\ & (\inst|seven_seg0[1]~219_combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~220_combout\,
	datab => \inst|seven_seg0[1]~219_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~222_combout\);

-- Location: LCCOMB_X7_Y23_N8
\inst|seven_seg0[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~221_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[1]~220_combout\ $ (((!\inst|seven_seg0[1]~219_combout\ & !\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg0[1]~219_combout\ & ((!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))) # (!\inst|seven_seg0[1]~219_combout\ & (!\inst|seven_seg0[1]~220_combout\ & 
-- \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000110011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~220_combout\,
	datab => \inst|seven_seg0[1]~219_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	combout => \inst|seven_seg0[1]~221_combout\);

-- Location: LCCOMB_X7_Y23_N12
\inst|seven_seg0[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~223_combout\ = (\inst|seven_seg0[1]~222_combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst|Equal0~15_combout\) # (!\inst|seven_seg0[1]~221_combout\)))) # (!\inst|seven_seg0[1]~222_combout\ & (((\inst|seven_seg0[1]~221_combout\ 
-- & \inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~15_combout\,
	datab => \inst|seven_seg0[1]~222_combout\,
	datac => \inst|seven_seg0[1]~221_combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg0[1]~223_combout\);

-- Location: LCCOMB_X7_Y23_N14
\inst|seven_seg0[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~224_combout\ = (\inst|seven_seg0[1]~218_combout\ & (((\inst|seven_seg0[1]~223_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst|seven_seg0[1]~218_combout\ & (\inst|seven_seg0[1]~211_combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~218_combout\,
	datab => \inst|seven_seg0[1]~211_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~223_combout\,
	combout => \inst|seven_seg0[1]~224_combout\);

-- Location: LCCOMB_X10_Y21_N18
\inst|seven_seg0[1]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~253_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst|seven_seg0[1]~190_combout\ & (!\inst32|inst4|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (((\inst32|inst4|inst1|inst5~combout\ & 
-- \inst|seven_seg0[1]~194_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst|seven_seg0[1]~190_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~194_combout\,
	combout => \inst|seven_seg0[1]~253_combout\);

-- Location: LCCOMB_X10_Y21_N12
\inst|seven_seg0[1]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~254_combout\ = (\inst|seven_seg0[1]~253_combout\) # ((\inst|seven_seg0[1]~197_combout\ & (\inst32|inst2|inst1|inst5~0_combout\ $ (!\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[1]~197_combout\,
	datad => \inst|seven_seg0[1]~253_combout\,
	combout => \inst|seven_seg0[1]~254_combout\);

-- Location: LCCOMB_X10_Y21_N28
\inst|seven_seg0[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[1]~225_combout\ = (\inst|seven_seg0[1]~208_combout\ & ((\inst|seven_seg0[1]~224_combout\) # ((!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg0[1]~208_combout\ & (((\inst32|inst5|inst1|inst5~combout\ & 
-- \inst|seven_seg0[1]~254_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[1]~208_combout\,
	datab => \inst|seven_seg0[1]~224_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg0[1]~254_combout\,
	combout => \inst|seven_seg0[1]~225_combout\);

-- Location: LCCOMB_X8_Y21_N10
\inst|seven_seg0[0]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~226_combout\ = (\inst|seven_seg0[0]~151_combout\ & (((\inst|seven_seg0[0]~152_combout\)))) # (!\inst|seven_seg0[0]~151_combout\ & ((\inst|seven_seg0[0]~152_combout\ & (\inst|seven_seg0[6]~64_combout\)) # 
-- (!\inst|seven_seg0[0]~152_combout\ & ((\inst|seven_seg0[6]~66_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~64_combout\,
	datab => \inst|seven_seg0[6]~66_combout\,
	datac => \inst|seven_seg0[0]~151_combout\,
	datad => \inst|seven_seg0[0]~152_combout\,
	combout => \inst|seven_seg0[0]~226_combout\);

-- Location: LCCOMB_X5_Y21_N8
\inst|seven_seg0[6]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~227_combout\ = (!\inst32|inst34|inst1|inst5~combout\ & !\inst32|inst3|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~227_combout\);

-- Location: LCCOMB_X5_Y21_N2
\inst|seven_seg0[0]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~228_combout\ = (\inst|seven_seg0[0]~156_combout\ & ((\inst|seven_seg0[0]~155_combout\ & ((\inst|seven_seg0[6]~154_combout\))) # (!\inst|seven_seg0[0]~155_combout\ & (\inst|seven_seg0[6]~227_combout\)))) # 
-- (!\inst|seven_seg0[0]~156_combout\ & (\inst|seven_seg0[0]~155_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[0]~156_combout\,
	datab => \inst|seven_seg0[0]~155_combout\,
	datac => \inst|seven_seg0[6]~227_combout\,
	datad => \inst|seven_seg0[6]~154_combout\,
	combout => \inst|seven_seg0[0]~228_combout\);

-- Location: LCCOMB_X5_Y21_N20
\inst|seven_seg0[0]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~229_combout\ = (\inst|seven_seg0[0]~156_combout\ & (\inst|seven_seg0[0]~228_combout\)) # (!\inst|seven_seg0[0]~156_combout\ & ((\inst|seven_seg0[0]~228_combout\ & (\inst|seven_seg0[6]~71_combout\)) # (!\inst|seven_seg0[0]~228_combout\ 
-- & ((\inst|seven_seg0[6]~243_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[0]~156_combout\,
	datab => \inst|seven_seg0[0]~228_combout\,
	datac => \inst|seven_seg0[6]~71_combout\,
	datad => \inst|seven_seg0[6]~243_combout\,
	combout => \inst|seven_seg0[0]~229_combout\);

-- Location: LCCOMB_X8_Y21_N20
\inst|seven_seg0[0]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~230_combout\ = (\inst|seven_seg0[0]~226_combout\ & (((\inst|seven_seg0[0]~229_combout\) # (!\inst|seven_seg0[0]~151_combout\)))) # (!\inst|seven_seg0[0]~226_combout\ & (\inst|seven_seg0[6]~57_combout\ & 
-- (\inst|seven_seg0[0]~151_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[0]~226_combout\,
	datab => \inst|seven_seg0[6]~57_combout\,
	datac => \inst|seven_seg0[0]~151_combout\,
	datad => \inst|seven_seg0[0]~229_combout\,
	combout => \inst|seven_seg0[0]~230_combout\);

-- Location: LCCOMB_X8_Y21_N6
\inst|seven_seg0[0]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~231_combout\ = (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg0[0]~230_combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[6]~247_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst|seven_seg0[0]~230_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~247_combout\,
	combout => \inst|seven_seg0[0]~231_combout\);

-- Location: LCCOMB_X8_Y21_N16
\inst|seven_seg0[0]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[0]~232_combout\ = (\inst|seven_seg0[3]~171_combout\) # (\inst|seven_seg0[0]~231_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|seven_seg0[3]~171_combout\,
	datad => \inst|seven_seg0[0]~231_combout\,
	combout => \inst|seven_seg0[0]~232_combout\);

-- Location: LCCOMB_X9_Y23_N2
\inst|Equal0~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~18_combout\ = (!\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|Equal0~18_combout\);

-- Location: LCCOMB_X11_Y21_N10
\inst|seven_seg1[6]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~62_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst25|inst34|inst1|inst5~combout\ $ ((\inst32|inst34|inst1|inst~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst|Equal0~18_combout\ & 
-- (\inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst~combout\,
	datad => \inst|Equal0~18_combout\,
	combout => \inst|seven_seg1[6]~62_combout\);

-- Location: LCCOMB_X11_Y21_N20
\inst|seven_seg1[6]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~63_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & 
-- !\inst|seven_seg1[6]~62_combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~62_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[6]~62_combout\,
	combout => \inst|seven_seg1[6]~63_combout\);

-- Location: LCCOMB_X11_Y21_N0
\inst|seven_seg1[6]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~61_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((!\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~61_combout\);

-- Location: LCCOMB_X11_Y21_N30
\inst|seven_seg1[6]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~64_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst~combout\,
	combout => \inst|seven_seg1[6]~64_combout\);

-- Location: LCCOMB_X11_Y21_N16
\inst|seven_seg1[6]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~65_combout\ = (\inst|seven_seg1[6]~63_combout\ & (((\inst|seven_seg1[6]~64_combout\) # (!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg1[6]~63_combout\ & (!\inst|seven_seg1[6]~61_combout\ & 
-- ((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~63_combout\,
	datab => \inst|seven_seg1[6]~61_combout\,
	datac => \inst|seven_seg1[6]~64_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~65_combout\);

-- Location: LCCOMB_X14_Y24_N26
\inst|seven_seg1[6]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~57_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((!\inst32|inst1|inst|inst5~combout\ & !\inst32|inst57|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & 
-- (((\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~57_combout\);

-- Location: LCCOMB_X14_Y24_N28
\inst|seven_seg1[6]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~58_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst5|inst1|inst5~combout\)) # (!\inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (((!\inst|seven_seg1[6]~57_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111100100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~57_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~58_combout\);

-- Location: LCCOMB_X14_Y24_N0
\inst|seven_seg1[6]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~32_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~32_combout\);

-- Location: LCCOMB_X14_Y24_N30
\inst|seven_seg1[6]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~59_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~32_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[6]~58_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~58_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[6]~32_combout\,
	combout => \inst|seven_seg1[6]~59_combout\);

-- Location: LCCOMB_X11_Y21_N24
\inst|seven_seg1[6]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~53_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\) # 
-- (!\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~53_combout\);

-- Location: LCCOMB_X11_Y21_N14
\inst|seven_seg1[6]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~52_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst~combout\ $ (\inst25|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~52_combout\);

-- Location: LCCOMB_X11_Y21_N26
\inst|seven_seg1[6]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~54_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~52_combout\))) # 
-- (!\inst32|inst3|inst1|inst5~combout\ & (!\inst|seven_seg1[6]~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~53_combout\,
	datac => \inst|seven_seg1[6]~52_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~54_combout\);

-- Location: LCCOMB_X11_Y21_N28
\inst|seven_seg1[6]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~55_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst34|inst1|inst~combout\ $ ((\inst25|inst34|inst1|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst5|inst1|inst5~combout\) # 
-- (\inst32|inst34|inst1|inst~combout\ $ (\inst25|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110111101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~55_combout\);

-- Location: LCCOMB_X11_Y21_N12
\inst|seven_seg1[6]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~51_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst5|inst1|inst5~combout\))) # (!\inst32|inst488|inst1|inst5~combout\ & 
-- (((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~51_combout\);

-- Location: LCCOMB_X11_Y21_N22
\inst|seven_seg1[6]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~56_combout\ = (\inst|seven_seg1[6]~54_combout\ & ((\inst|seven_seg1[6]~55_combout\) # ((!\inst32|inst57|inst1|inst5~combout\)))) # (!\inst|seven_seg1[6]~54_combout\ & (((\inst32|inst57|inst1|inst5~combout\ & 
-- \inst|seven_seg1[6]~51_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~54_combout\,
	datab => \inst|seven_seg1[6]~55_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[6]~51_combout\,
	combout => \inst|seven_seg1[6]~56_combout\);

-- Location: LCCOMB_X14_Y24_N8
\inst|seven_seg1[6]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~60_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~56_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[6]~59_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~59_combout\,
	datad => \inst|seven_seg1[6]~56_combout\,
	combout => \inst|seven_seg1[6]~60_combout\);

-- Location: LCCOMB_X10_Y24_N4
\inst|seven_seg1[6]~253\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~253_combout\ = (!\inst32|inst5|inst1|inst5~combout\ & (\inst|Equal0~18_combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst|Equal0~18_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~253_combout\);

-- Location: LCCOMB_X14_Y24_N6
\inst|seven_seg1[6]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~49_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~49_combout\);

-- Location: LCCOMB_X14_Y24_N24
\inst|seven_seg1[6]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~50_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[6]~253_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~49_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~253_combout\,
	datad => \inst|seven_seg1[6]~49_combout\,
	combout => \inst|seven_seg1[6]~50_combout\);

-- Location: LCCOMB_X14_Y24_N2
\inst|seven_seg1[6]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~66_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~60_combout\ & (\inst|seven_seg1[6]~65_combout\)) # (!\inst|seven_seg1[6]~60_combout\ & ((\inst|seven_seg1[6]~50_combout\))))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (((\inst|seven_seg1[6]~60_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~65_combout\,
	datac => \inst|seven_seg1[6]~60_combout\,
	datad => \inst|seven_seg1[6]~50_combout\,
	combout => \inst|seven_seg1[6]~66_combout\);

-- Location: LCCOMB_X12_Y21_N6
\inst|Equal0~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~17_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|Equal0~17_combout\);

-- Location: LCCOMB_X8_Y23_N24
\inst|Equal0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~16_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|Equal0~16_combout\);

-- Location: LCCOMB_X8_Y23_N26
\inst|seven_seg1[6]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~42_combout\ = (\inst|seven_seg2[1]~25_combout\ & (\inst|Equal0~9_combout\ & (\inst|Equal0~17_combout\ $ (\inst|Equal0~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~17_combout\,
	datab => \inst|Equal0~16_combout\,
	datac => \inst|seven_seg2[1]~25_combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|seven_seg1[6]~42_combout\);

-- Location: LCCOMB_X8_Y23_N12
\inst|seven_seg1[6]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~43_combout\ = \inst|seven_seg1[6]~42_combout\ $ (((!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & \inst|Equal0~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst|seven_seg1[6]~42_combout\,
	datad => \inst|Equal0~15_combout\,
	combout => \inst|seven_seg1[6]~43_combout\);

-- Location: LCCOMB_X8_Y23_N20
\inst|seven_seg1[6]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~40_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst3|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & !\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[6]~40_combout\);

-- Location: LCCOMB_X8_Y23_N14
\inst|seven_seg1[6]~252\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~252_combout\ = (\inst|seven_seg1[6]~43_combout\) # ((\inst|seven_seg1[6]~40_combout\ & (\inst|seven_seg2[1]~25_combout\ & \inst|Equal0~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~43_combout\,
	datab => \inst|seven_seg1[6]~40_combout\,
	datac => \inst|seven_seg2[1]~25_combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|seven_seg1[6]~252_combout\);

-- Location: LCCOMB_X8_Y24_N16
\inst|seven_seg1[6]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~44_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~252_combout\) # ((!\inst32|inst1|inst|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (((!\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~252_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~44_combout\);

-- Location: LCCOMB_X8_Y24_N26
\inst|seven_seg1[6]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~45_combout\ = (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~44_combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & (!\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg1[6]~44_combout\,
	combout => \inst|seven_seg1[6]~45_combout\);

-- Location: LCCOMB_X8_Y24_N28
\inst|seven_seg1[6]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~46_combout\ = (\inst|seven_seg1[6]~45_combout\) # ((\inst|seven_seg1[6]~252_combout\ & (\inst32|inst3|inst1|inst5~combout\ & \inst|seven_seg0[0]~152_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~252_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst|seven_seg0[0]~152_combout\,
	datad => \inst|seven_seg1[6]~45_combout\,
	combout => \inst|seven_seg1[6]~46_combout\);

-- Location: LCCOMB_X14_Y24_N10
\inst|seven_seg1[6]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~47_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[6]~46_combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~253_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~46_combout\,
	datac => \inst|seven_seg1[6]~253_combout\,
	combout => \inst|seven_seg1[6]~47_combout\);

-- Location: LCCOMB_X4_Y24_N0
\inst|seven_seg1[6]~29\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~29_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & ((!\inst32|inst57|inst1|inst5~combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111101010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~29_combout\);

-- Location: LCCOMB_X4_Y24_N22
\inst|seven_seg1[6]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~28_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[6]~28_combout\);

-- Location: LCCOMB_X4_Y24_N18
\inst|seven_seg1[6]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~30_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((\inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\) # 
-- ((!\inst32|inst34|inst1|inst5~combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011111100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[6]~30_combout\);

-- Location: LCCOMB_X4_Y24_N28
\inst|seven_seg1[6]~31\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~31_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~29_combout\ & ((\inst|seven_seg1[6]~30_combout\))) # (!\inst|seven_seg1[6]~29_combout\ & (\inst|seven_seg1[6]~28_combout\)))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[6]~29_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~29_combout\,
	datac => \inst|seven_seg1[6]~28_combout\,
	datad => \inst|seven_seg1[6]~30_combout\,
	combout => \inst|seven_seg1[6]~31_combout\);

-- Location: LCCOMB_X14_Y24_N22
\inst|seven_seg1[6]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~37_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ $ ((!\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # 
-- (\inst32|inst34|inst1|inst5~combout\ $ (\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100111110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~37_combout\);

-- Location: LCCOMB_X8_Y24_N4
\inst|seven_seg1[6]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~36_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~36_combout\);

-- Location: LCCOMB_X11_Y23_N4
\inst|seven_seg1[6]~35\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~35_combout\ = (\inst25|inst5|inst1|inst5~combout\ & (\inst25|inst57|inst1|inst5~combout\ $ (((\inst32|inst4|inst2~0_combout\ & \inst25|inst4|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~35_combout\);

-- Location: LCCOMB_X8_Y24_N6
\inst|seven_seg1[6]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~38_combout\ = (\inst|seven_seg1[6]~37_combout\ & (((\inst|seven_seg1[6]~36_combout\ & \inst|seven_seg1[6]~35_combout\)))) # (!\inst|seven_seg1[6]~37_combout\ & (((\inst|seven_seg1[6]~36_combout\ & \inst|seven_seg1[6]~35_combout\)) # 
-- (!\inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~37_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~36_combout\,
	datad => \inst|seven_seg1[6]~35_combout\,
	combout => \inst|seven_seg1[6]~38_combout\);

-- Location: LCCOMB_X14_Y24_N18
\inst|seven_seg1[6]~33\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~33_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~33_combout\);

-- Location: LCCOMB_X14_Y24_N4
\inst|seven_seg1[6]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~34_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[6]~32_combout\)) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~33_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~32_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg1[6]~33_combout\,
	combout => \inst|seven_seg1[6]~34_combout\);

-- Location: LCCOMB_X14_Y24_N16
\inst|seven_seg1[6]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~39_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~34_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[6]~38_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~38_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~34_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[6]~39_combout\);

-- Location: LCCOMB_X14_Y24_N12
\inst|seven_seg1[6]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~48_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~39_combout\ & (\inst|seven_seg1[6]~47_combout\)) # (!\inst|seven_seg1[6]~39_combout\ & ((\inst|seven_seg1[6]~31_combout\))))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[6]~39_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~47_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[6]~31_combout\,
	datad => \inst|seven_seg1[6]~39_combout\,
	combout => \inst|seven_seg1[6]~48_combout\);

-- Location: LCCOMB_X14_Y24_N20
\inst|seven_seg1[6]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~67_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[6]~48_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[6]~66_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg1[6]~66_combout\,
	datad => \inst|seven_seg1[6]~48_combout\,
	combout => \inst|seven_seg1[6]~67_combout\);

-- Location: LCCOMB_X6_Y22_N22
\inst|seven_seg1[5]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~75_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst4|inst1|inst5~combout\ & !\inst32|inst5|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\) # (!\inst32|inst5|inst1|inst5~combout\))))) # (!\inst32|inst57|inst1|inst5~combout\ & (((!\inst32|inst4|inst1|inst5~combout\ & \inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~75_combout\);

-- Location: LCCOMB_X6_Y22_N28
\inst|seven_seg1[5]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~74_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst4|inst1|inst5~combout\ $ (!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst4|inst1|inst5~combout\ $ (!\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~74_combout\);

-- Location: LCCOMB_X6_Y22_N12
\inst|seven_seg1[5]~268\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~268_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (!\inst|seven_seg1[5]~74_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (\inst|seven_seg1[5]~74_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|seven_seg1[5]~74_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~268_combout\);

-- Location: LCCOMB_X6_Y22_N14
\inst|seven_seg1[5]~269\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~269_combout\ = (\inst|seven_seg1[5]~74_combout\ & (\inst|seven_seg1[5]~268_combout\ & (\inst|seven_seg1[5]~75_combout\ $ (!\inst32|inst3|inst1|inst5~combout\)))) # (!\inst|seven_seg1[5]~74_combout\ & (\inst|seven_seg1[5]~75_combout\ $ 
-- (((\inst|seven_seg1[5]~268_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~75_combout\,
	datab => \inst|seven_seg1[5]~74_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~268_combout\,
	combout => \inst|seven_seg1[5]~269_combout\);

-- Location: LCCOMB_X6_Y22_N4
\inst|seven_seg1[5]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~70_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst5|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (((\inst32|inst667|inst1|inst5~combout\) # (\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111101101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~70_combout\);

-- Location: LCCOMB_X6_Y22_N10
\inst|seven_seg1[5]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~69_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~69_combout\);

-- Location: LCCOMB_X6_Y22_N6
\inst|seven_seg1[5]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~71_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~69_combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[5]~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg1[5]~70_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~69_combout\,
	combout => \inst|seven_seg1[5]~71_combout\);

-- Location: LCCOMB_X6_Y22_N16
\inst|seven_seg1[5]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~68_combout\ = (\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~68_combout\);

-- Location: LCCOMB_X6_Y22_N0
\inst|seven_seg1[5]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~72_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (((!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst5|inst1|inst5~combout\) # 
-- (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst5|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~72_combout\);

-- Location: LCCOMB_X6_Y22_N18
\inst|seven_seg1[5]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~73_combout\ = (\inst|seven_seg1[5]~71_combout\ & (((\inst|seven_seg1[5]~72_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst|seven_seg1[5]~71_combout\ & (!\inst|seven_seg1[5]~68_combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~71_combout\,
	datab => \inst|seven_seg1[5]~68_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~72_combout\,
	combout => \inst|seven_seg1[5]~73_combout\);

-- Location: LCCOMB_X6_Y22_N8
\inst|seven_seg1[5]~76\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~76_combout\ = (\inst32|inst1|inst|inst5~combout\ & ((\inst|seven_seg1[5]~73_combout\))) # (!\inst32|inst1|inst|inst5~combout\ & (\inst|seven_seg1[5]~269_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datac => \inst|seven_seg1[5]~269_combout\,
	datad => \inst|seven_seg1[5]~73_combout\,
	combout => \inst|seven_seg1[5]~76_combout\);

-- Location: LCCOMB_X10_Y21_N14
\inst|seven_seg1[5]~79\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~79_combout\ = (\inst|Equal0~13_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|Equal0~13_combout\,
	combout => \inst|seven_seg1[5]~79_combout\);

-- Location: LCCOMB_X9_Y23_N10
\inst|seven_seg1[5]~97\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~97_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~79_combout\) # ((!\inst|Equal0~11_combout\ & \inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (((\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~11_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg1[5]~79_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~97_combout\);

-- Location: LCCOMB_X8_Y24_N30
\inst|Equal0~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~19_combout\ = (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|Equal0~19_combout\);

-- Location: LCCOMB_X8_Y24_N24
\inst|seven_seg1[5]~95\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~95_combout\ = (\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~95_combout\);

-- Location: LCCOMB_X8_Y24_N2
\inst|seven_seg1[5]~96\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~96_combout\ = (\inst|Equal0~19_combout\ & (((\inst|seven_seg1[5]~95_combout\ & !\inst32|inst57|inst1|inst5~combout\)) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst|Equal0~19_combout\ & (\inst|seven_seg1[5]~95_combout\ & 
-- ((!\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~19_combout\,
	datab => \inst|seven_seg1[5]~95_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~96_combout\);

-- Location: LCCOMB_X8_Y24_N12
\inst|seven_seg1[5]~98\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~98_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~96_combout\))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[5]~97_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~97_combout\,
	datab => \inst|seven_seg1[5]~96_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~98_combout\);

-- Location: LCCOMB_X9_Y24_N20
\inst|seven_seg1[5]~90\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~90_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~90_combout\);

-- Location: LCCOMB_X8_Y24_N20
\inst|seven_seg1[5]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~255_combout\ = (\inst|seven_seg1[5]~90_combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datac => \inst|seven_seg1[5]~90_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~255_combout\);

-- Location: LCCOMB_X10_Y23_N0
\inst|seven_seg1[5]~99\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~99_combout\ = (\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110011101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[5]~99_combout\);

-- Location: LCCOMB_X10_Y23_N26
\inst|seven_seg1[5]~100\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~100_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|Equal0~18_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[5]~99_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[5]~99_combout\,
	datad => \inst|Equal0~18_combout\,
	combout => \inst|seven_seg1[5]~100_combout\);

-- Location: LCCOMB_X10_Y23_N20
\inst|seven_seg1[5]~101\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~101_combout\ = (!\inst32|inst1|inst|inst5~combout\ & (\inst|Equal0~18_combout\ & ((\inst|Equal0~27_combout\) # (!\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst|Equal0~27_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|Equal0~18_combout\,
	combout => \inst|seven_seg1[5]~101_combout\);

-- Location: LCCOMB_X10_Y23_N6
\inst|seven_seg1[5]~102\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~102_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~100_combout\ & ((!\inst|seven_seg1[5]~101_combout\))) # (!\inst|seven_seg1[5]~100_combout\ & (\inst|Equal0~13_combout\)))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg1[5]~100_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|Equal0~13_combout\,
	datac => \inst|seven_seg1[5]~100_combout\,
	datad => \inst|seven_seg1[5]~101_combout\,
	combout => \inst|seven_seg1[5]~102_combout\);

-- Location: LCCOMB_X8_Y24_N22
\inst|seven_seg1[5]~103\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~103_combout\ = (\inst|seven_seg1[5]~98_combout\ & (((\inst|seven_seg1[5]~102_combout\) # (!\inst32|inst5|inst1|inst5~combout\)))) # (!\inst|seven_seg1[5]~98_combout\ & (\inst|seven_seg1[5]~255_combout\ & 
-- (\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~98_combout\,
	datab => \inst|seven_seg1[5]~255_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~102_combout\,
	combout => \inst|seven_seg1[5]~103_combout\);

-- Location: LCCOMB_X9_Y23_N12
\inst|seven_seg1[5]~77\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~77_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst57|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst1|inst|inst5~combout\ & 
-- \inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~77_combout\);

-- Location: LCCOMB_X9_Y23_N30
\inst|seven_seg1[5]~78\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~78_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|Equal0~19_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (!\inst|seven_seg1[5]~77_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~77_combout\,
	datab => \inst|Equal0~19_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~78_combout\);

-- Location: LCCOMB_X8_Y24_N18
\inst|seven_seg1[5]~254\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~254_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~254_combout\);

-- Location: LCCOMB_X9_Y23_N24
\inst|seven_seg1[5]~80\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~80_combout\ = (\inst|seven_seg1[5]~78_combout\ & (((\inst|seven_seg1[5]~79_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst|seven_seg1[5]~78_combout\ & (\inst|seven_seg1[5]~254_combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~78_combout\,
	datab => \inst|seven_seg1[5]~254_combout\,
	datac => \inst|seven_seg1[5]~79_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~80_combout\);

-- Location: LCCOMB_X9_Y24_N0
\inst|seven_seg1[5]~84\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~84_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|Equal0~18_combout\) # (!\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|Equal0~18_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~84_combout\);

-- Location: LCCOMB_X9_Y24_N26
\inst|seven_seg1[5]~81\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~81_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg1[5]~81_combout\);

-- Location: LCCOMB_X9_Y24_N4
\inst|seven_seg1[5]~82\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~82_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~82_combout\);

-- Location: LCCOMB_X9_Y24_N14
\inst|seven_seg1[5]~83\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~83_combout\ = (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~81_combout\) # ((\inst|seven_seg1[5]~82_combout\ & !\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~81_combout\,
	datab => \inst|seven_seg1[5]~82_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~83_combout\);

-- Location: LCCOMB_X9_Y24_N10
\inst|seven_seg1[5]~85\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~85_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (((\inst|seven_seg1[5]~84_combout\) # (\inst|seven_seg1[5]~83_combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[5]~80_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~80_combout\,
	datab => \inst|seven_seg1[5]~84_combout\,
	datac => \inst|seven_seg1[5]~83_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~85_combout\);

-- Location: LCCOMB_X11_Y23_N22
\inst|Equal0~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~20_combout\ = (\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & (\inst17|inst4|inst1|inst5~combout\ $ (\inst25|inst4|inst2~0_combout\)))) # (!\inst32|inst4|inst2~0_combout\ & (!\inst25|inst4|inst1|inst5~combout\ & 
-- (\inst17|inst4|inst1|inst5~combout\ $ (!\inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010010000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst|Equal0~20_combout\);

-- Location: LCCOMB_X9_Y24_N12
\inst|seven_seg1[5]~86\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~86_combout\ = (\inst|Equal0~20_combout\ & (((!\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst4|inst1|inst5~combout\)) # (!\inst|Equal0~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~20_combout\,
	datab => \inst|Equal0~11_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~86_combout\);

-- Location: LCCOMB_X9_Y24_N30
\inst|seven_seg1[5]~91\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~91_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~91_combout\);

-- Location: LCCOMB_X9_Y24_N24
\inst|seven_seg1[5]~92\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~92_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst|seven_seg1[5]~90_combout\) # (\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[5]~91_combout\ & 
-- ((\inst32|inst5|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~91_combout\,
	datab => \inst|seven_seg1[5]~90_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~92_combout\);

-- Location: LCCOMB_X9_Y24_N6
\inst|seven_seg1[5]~87\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~87_combout\ = (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst5|inst1|inst5~combout\ & (\inst15|inst57|inst1|inst~combout\ $ (!\inst32|inst667|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst15|inst57|inst1|inst~combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~87_combout\);

-- Location: LCCOMB_X9_Y24_N16
\inst|seven_seg1[5]~88\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~88_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & 
-- !\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst667|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~88_combout\);

-- Location: LCCOMB_X9_Y24_N18
\inst|seven_seg1[5]~89\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~89_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~87_combout\) # ((!\inst32|inst57|inst1|inst5~combout\ & \inst|seven_seg1[5]~88_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~87_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~88_combout\,
	combout => \inst|seven_seg1[5]~89_combout\);

-- Location: LCCOMB_X9_Y24_N2
\inst|seven_seg1[5]~93\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~93_combout\ = (\inst|seven_seg1[5]~86_combout\) # ((\inst|seven_seg1[5]~89_combout\) # ((\inst|seven_seg1[5]~92_combout\ & \inst32|inst57|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~86_combout\,
	datab => \inst|seven_seg1[5]~92_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~89_combout\,
	combout => \inst|seven_seg1[5]~93_combout\);

-- Location: LCCOMB_X9_Y24_N28
\inst|seven_seg1[5]~94\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~94_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[5]~85_combout\)) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~93_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~85_combout\,
	datab => \inst|seven_seg1[5]~93_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[5]~94_combout\);

-- Location: LCCOMB_X9_Y24_N22
\inst|seven_seg1[5]~104\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[5]~104_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~94_combout\ & ((\inst|seven_seg1[5]~103_combout\))) # (!\inst|seven_seg1[5]~94_combout\ & (\inst|seven_seg1[5]~76_combout\)))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[5]~94_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[5]~76_combout\,
	datab => \inst|seven_seg1[5]~103_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~94_combout\,
	combout => \inst|seven_seg1[5]~104_combout\);

-- Location: LCCOMB_X8_Y20_N28
\inst|seven_seg1[4]~266\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~266_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (!\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst3|inst1|inst5~combout\ $ (((\inst32|inst667|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000110100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~266_combout\);

-- Location: LCCOMB_X8_Y23_N6
\inst|seven_seg1[6]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[6]~41_combout\ = (\inst|seven_seg1[6]~40_combout\ & (\inst|seven_seg2[1]~25_combout\ & \inst|Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[6]~40_combout\,
	datac => \inst|seven_seg2[1]~25_combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|seven_seg1[6]~41_combout\);

-- Location: LCCOMB_X8_Y23_N30
\inst|seven_seg1[4]~125\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~125_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst2~0_combout\ $ (((!\inst25|inst4|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~125_combout\);

-- Location: LCCOMB_X8_Y23_N16
\inst|seven_seg1[4]~126\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~126_combout\ = (\inst32|inst2|inst1|inst5~0_combout\) # (((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (\inst32|inst1|inst|inst5~combout\)) # (!\inst32|inst4|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[4]~126_combout\);

-- Location: LCCOMB_X8_Y23_N18
\inst|seven_seg1[4]~127\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~127_combout\ = (\inst|seven_seg1[4]~125_combout\) # (((\inst|seven_seg1[4]~126_combout\ & !\inst32|inst3|inst1|inst5~combout\)) # (!\inst|Equal0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~125_combout\,
	datab => \inst|seven_seg1[4]~126_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|seven_seg1[4]~127_combout\);

-- Location: LCCOMB_X10_Y23_N8
\inst|Equal0~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~21_combout\ = (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & (\inst|Equal0~27_combout\ & (!\inst32|inst1|inst|inst5~combout\ & \inst|Equal0~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst|Equal0~27_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|Equal0~18_combout\,
	combout => \inst|Equal0~21_combout\);

-- Location: LCCOMB_X8_Y23_N28
\inst|seven_seg1[4]~128\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~128_combout\ = (\inst|seven_seg1[4]~127_combout\ & (!\inst|Equal0~21_combout\ & ((\inst|seven_seg1[6]~41_combout\) # (\inst|seven_seg1[6]~43_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~41_combout\,
	datab => \inst|seven_seg1[4]~127_combout\,
	datac => \inst|Equal0~21_combout\,
	datad => \inst|seven_seg1[6]~43_combout\,
	combout => \inst|seven_seg1[4]~128_combout\);

-- Location: LCCOMB_X8_Y20_N30
\inst|seven_seg1[4]~267\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~267_combout\ = (\inst|seven_seg1[4]~266_combout\) # ((\inst|seven_seg1[4]~128_combout\ & \inst32|inst667|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[4]~266_combout\,
	datac => \inst|seven_seg1[4]~128_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~267_combout\);

-- Location: LCCOMB_X8_Y20_N24
\inst|seven_seg1[4]~122\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~122_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst4|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~122_combout\);

-- Location: LCCOMB_X7_Y22_N24
\inst|seven_seg1[4]~110\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~110_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101100000011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~110_combout\);

-- Location: LCCOMB_X8_Y20_N18
\inst|seven_seg1[4]~123\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~123_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\)) # (!\inst32|inst4|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\) # ((\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~123_combout\);

-- Location: LCCOMB_X8_Y20_N4
\inst|seven_seg1[4]~124\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~124_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)) # (!\inst|seven_seg1[4]~110_combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg1[4]~123_combout\ & 
-- !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~110_combout\,
	datab => \inst|seven_seg1[4]~123_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~124_combout\);

-- Location: LCCOMB_X8_Y20_N6
\inst|seven_seg1[4]~129\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~129_combout\ = (\inst|seven_seg1[4]~124_combout\ & ((\inst|seven_seg1[4]~267_combout\) # ((!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst|seven_seg1[4]~124_combout\ & (((!\inst|seven_seg1[4]~122_combout\ & 
-- \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~267_combout\,
	datab => \inst|seven_seg1[4]~122_combout\,
	datac => \inst|seven_seg1[4]~124_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~129_combout\);

-- Location: LCCOMB_X7_Y22_N10
\inst|seven_seg1[4]~111\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~111_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst4|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst4|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~111_combout\);

-- Location: LCCOMB_X7_Y22_N4
\inst|seven_seg1[4]~112\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~112_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst4|inst1|inst5~combout\)) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst4|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~112_combout\);

-- Location: LCCOMB_X7_Y22_N14
\inst|seven_seg1[4]~113\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~113_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)) # (!\inst|seven_seg1[4]~111_combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg1[4]~112_combout\ & 
-- !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~111_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg1[4]~112_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~113_combout\);

-- Location: LCCOMB_X8_Y22_N16
\inst|seven_seg1[4]~114\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~114_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst3|inst1|inst5~combout\) # (!\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # 
-- ((\inst32|inst667|inst1|inst5~combout\) # (\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~114_combout\);

-- Location: LCCOMB_X8_Y22_N26
\inst|seven_seg1[4]~115\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~115_combout\ = (\inst|seven_seg1[4]~113_combout\ & ((\inst|seven_seg1[4]~114_combout\) # ((!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst|seven_seg1[4]~113_combout\ & (((!\inst|seven_seg1[4]~110_combout\ & 
-- \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~113_combout\,
	datab => \inst|seven_seg1[4]~114_combout\,
	datac => \inst|seven_seg1[4]~110_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~115_combout\);

-- Location: LCCOMB_X15_Y23_N18
\inst|seven_seg1[4]~117\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~117_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\ $ (\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~117_combout\);

-- Location: LCCOMB_X15_Y23_N20
\inst|seven_seg1[4]~118\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~118_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst4|inst1|inst5~combout\) # (\inst|seven_seg1[4]~117_combout\)) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (!\inst|seven_seg1[4]~117_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~117_combout\,
	combout => \inst|seven_seg1[4]~118_combout\);

-- Location: LCCOMB_X15_Y23_N14
\inst|seven_seg1[4]~119\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~119_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001001001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~119_combout\);

-- Location: LCCOMB_X15_Y23_N24
\inst|seven_seg1[4]~116\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~116_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~116_combout\);

-- Location: LCCOMB_X15_Y23_N16
\inst|seven_seg1[4]~120\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~120_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~118_combout\ & (!\inst|seven_seg1[4]~119_combout\)) # (!\inst|seven_seg1[4]~118_combout\ & ((!\inst|seven_seg1[4]~116_combout\))))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[4]~118_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110001101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg1[4]~118_combout\,
	datac => \inst|seven_seg1[4]~119_combout\,
	datad => \inst|seven_seg1[4]~116_combout\,
	combout => \inst|seven_seg1[4]~120_combout\);

-- Location: LCCOMB_X7_Y21_N22
\inst|seven_seg1[4]~121\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~121_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~115_combout\) # ((\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (((!\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg1[4]~120_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg1[4]~115_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~120_combout\,
	combout => \inst|seven_seg1[4]~121_combout\);

-- Location: LCCOMB_X12_Y21_N10
\inst|seven_seg1[4]~106\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~106_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\ $ ((\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~106_combout\);

-- Location: LCCOMB_X12_Y21_N0
\inst|seven_seg1[4]~105\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~105_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst34|inst1|inst5~combout\ & \inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~105_combout\);

-- Location: LCCOMB_X12_Y21_N14
\inst|seven_seg1[4]~108\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~108_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (!\inst|seven_seg1[4]~106_combout\ & (\inst32|inst57|inst1|inst5~combout\ $ (!\inst|seven_seg1[4]~105_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- ((\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[4]~106_combout\ & !\inst|seven_seg1[4]~105_combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~105_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~106_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~105_combout\,
	combout => \inst|seven_seg1[4]~108_combout\);

-- Location: LCCOMB_X12_Y21_N12
\inst|seven_seg1[4]~107\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~107_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[4]~106_combout\ $ (!\inst|seven_seg1[4]~105_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- ((\inst|seven_seg1[4]~106_combout\ & ((!\inst|seven_seg1[4]~105_combout\))) # (!\inst|seven_seg1[4]~106_combout\ & (\inst32|inst34|inst1|inst5~combout\ & \inst|seven_seg1[4]~105_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~106_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~105_combout\,
	combout => \inst|seven_seg1[4]~107_combout\);

-- Location: LCCOMB_X12_Y21_N8
\inst|seven_seg1[4]~109\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~109_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~108_combout\) # (!\inst|seven_seg1[4]~107_combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~107_combout\) # 
-- (!\inst|seven_seg1[4]~108_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[4]~108_combout\,
	datad => \inst|seven_seg1[4]~107_combout\,
	combout => \inst|seven_seg1[4]~109_combout\);

-- Location: LCCOMB_X7_Y21_N24
\inst|seven_seg1[4]~130\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~130_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~121_combout\ & (\inst|seven_seg1[4]~129_combout\)) # (!\inst|seven_seg1[4]~121_combout\ & ((\inst|seven_seg1[4]~109_combout\))))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[4]~121_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg1[4]~129_combout\,
	datac => \inst|seven_seg1[4]~121_combout\,
	datad => \inst|seven_seg1[4]~109_combout\,
	combout => \inst|seven_seg1[4]~130_combout\);

-- Location: LCCOMB_X8_Y20_N12
\inst|seven_seg1[4]~139\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~139_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~139_combout\);

-- Location: LCCOMB_X8_Y20_N10
\inst|seven_seg1[4]~138\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~138_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst4|inst1|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\)) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (((!\inst32|inst4|inst1|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010110111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~138_combout\);

-- Location: LCCOMB_X8_Y20_N22
\inst|seven_seg1[4]~140\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~140_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((!\inst|seven_seg1[4]~138_combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[4]~139_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~139_combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~138_combout\,
	combout => \inst|seven_seg1[4]~140_combout\);

-- Location: LCCOMB_X10_Y22_N26
\inst|seven_seg1[4]~141\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~141_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~141_combout\);

-- Location: LCCOMB_X8_Y20_N8
\inst|seven_seg1[4]~137\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~137_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst4|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst4|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110110100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~137_combout\);

-- Location: LCCOMB_X8_Y20_N0
\inst|seven_seg1[4]~142\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~142_combout\ = (\inst|seven_seg1[4]~140_combout\ & (((!\inst32|inst34|inst1|inst5~combout\)) # (!\inst|seven_seg1[4]~141_combout\))) # (!\inst|seven_seg1[4]~140_combout\ & (((!\inst|seven_seg1[4]~137_combout\ & 
-- \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~140_combout\,
	datab => \inst|seven_seg1[4]~141_combout\,
	datac => \inst|seven_seg1[4]~137_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~142_combout\);

-- Location: LCCOMB_X7_Y22_N28
\inst|seven_seg1[4]~143\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~143_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ $ (((!\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst2|inst1|inst5~0_combout\))))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (!\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~143_combout\);

-- Location: LCCOMB_X7_Y22_N20
\inst|seven_seg1[4]~147\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~147_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst34|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~147_combout\);

-- Location: LCCOMB_X7_Y22_N6
\inst|seven_seg1[4]~144\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~144_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((!\inst32|inst667|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010011100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~144_combout\);

-- Location: LCCOMB_X7_Y22_N0
\inst|seven_seg1[4]~145\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~145_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (!\inst32|inst667|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~145_combout\);

-- Location: LCCOMB_X7_Y22_N26
\inst|seven_seg1[4]~146\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~146_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (!\inst|seven_seg1[4]~144_combout\)) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & ((!\inst|seven_seg1[4]~145_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~144_combout\,
	datab => \inst|seven_seg1[4]~145_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~146_combout\);

-- Location: LCCOMB_X7_Y22_N30
\inst|seven_seg1[4]~148\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~148_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~146_combout\ & ((!\inst|seven_seg1[4]~147_combout\))) # (!\inst|seven_seg1[4]~146_combout\ & (!\inst|seven_seg1[4]~143_combout\)))) # 
-- (!\inst32|inst4|inst1|inst5~combout\ & (((\inst|seven_seg1[4]~146_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~143_combout\,
	datab => \inst|seven_seg1[4]~147_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[4]~146_combout\,
	combout => \inst|seven_seg1[4]~148_combout\);

-- Location: LCCOMB_X7_Y21_N18
\inst|seven_seg1[4]~149\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~149_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst5|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[4]~142_combout\)) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~148_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg1[4]~142_combout\,
	datac => \inst|seven_seg1[4]~148_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~149_combout\);

-- Location: LCCOMB_X8_Y20_N26
\inst|seven_seg1[4]~150\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~150_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((!\inst32|inst4|inst1|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101101100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~150_combout\);

-- Location: LCCOMB_X9_Y23_N4
\inst|seven_seg1[4]~153\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~153_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst|Equal0~18_combout\) # (\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|Equal0~11_combout\ & 
-- ((!\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~11_combout\,
	datab => \inst|Equal0~18_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~153_combout\);

-- Location: LCCOMB_X8_Y20_N16
\inst|seven_seg1[4]~154\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~154_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~128_combout\) # ((!\inst|seven_seg1[4]~153_combout\ & !\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (((\inst|seven_seg1[4]~153_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~128_combout\,
	datab => \inst|seven_seg1[4]~153_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~154_combout\);

-- Location: LCCOMB_X8_Y20_N20
\inst|seven_seg1[4]~151\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~151_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst2|inst1|inst5~0_combout\)) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~151_combout\);

-- Location: LCCOMB_X8_Y20_N14
\inst|seven_seg1[4]~152\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~152_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\) # (!\inst|seven_seg1[4]~137_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[4]~151_combout\ & 
-- ((!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~151_combout\,
	datab => \inst|seven_seg1[4]~137_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~152_combout\);

-- Location: LCCOMB_X8_Y20_N2
\inst|seven_seg1[4]~155\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~155_combout\ = (\inst|seven_seg1[4]~152_combout\ & (((\inst|seven_seg1[4]~154_combout\) # (!\inst32|inst34|inst1|inst5~combout\)))) # (!\inst|seven_seg1[4]~152_combout\ & (!\inst|seven_seg1[4]~150_combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~150_combout\,
	datab => \inst|seven_seg1[4]~154_combout\,
	datac => \inst|seven_seg1[4]~152_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~155_combout\);

-- Location: LCCOMB_X7_Y22_N12
\inst|seven_seg1[4]~133\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~133_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst34|inst1|inst5~combout\) # (!\inst32|inst667|inst1|inst5~combout\))))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010110011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~133_combout\);

-- Location: LCCOMB_X7_Y22_N2
\inst|seven_seg1[4]~132\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~132_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (((!\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst34|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst667|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & \inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000110000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~132_combout\);

-- Location: LCCOMB_X7_Y22_N22
\inst|seven_seg1[4]~134\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~134_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~132_combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (!\inst|seven_seg1[4]~133_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~133_combout\,
	datab => \inst|seven_seg1[4]~132_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~134_combout\);

-- Location: LCCOMB_X7_Y22_N16
\inst|seven_seg1[4]~131\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~131_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & ((!\inst32|inst34|inst1|inst5~combout\) # 
-- (!\inst32|inst2|inst1|inst5~0_combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010011101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~131_combout\);

-- Location: LCCOMB_X7_Y22_N8
\inst|seven_seg1[4]~135\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~135_combout\ = (\inst32|inst34|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~135_combout\);

-- Location: LCCOMB_X7_Y22_N18
\inst|seven_seg1[4]~136\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~136_combout\ = (\inst|seven_seg1[4]~134_combout\ & (((\inst|seven_seg1[4]~135_combout\) # (!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst|seven_seg1[4]~134_combout\ & (!\inst|seven_seg1[4]~131_combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[4]~134_combout\,
	datab => \inst|seven_seg1[4]~131_combout\,
	datac => \inst|seven_seg1[4]~135_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[4]~136_combout\);

-- Location: LCCOMB_X7_Y21_N12
\inst|seven_seg1[4]~156\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~156_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[4]~149_combout\ & (\inst|seven_seg1[4]~155_combout\)) # (!\inst|seven_seg1[4]~149_combout\ & ((\inst|seven_seg1[4]~136_combout\))))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg1[4]~149_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg1[4]~149_combout\,
	datac => \inst|seven_seg1[4]~155_combout\,
	datad => \inst|seven_seg1[4]~136_combout\,
	combout => \inst|seven_seg1[4]~156_combout\);

-- Location: LCCOMB_X7_Y21_N14
\inst|seven_seg1[4]~157\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[4]~157_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\inst|seven_seg1[4]~130_combout\)) # (!\inst32|inst1|inst|inst5~combout\ & ((\inst|seven_seg1[4]~156_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[4]~130_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg1[4]~156_combout\,
	combout => \inst|seven_seg1[4]~157_combout\);

-- Location: LCCOMB_X10_Y21_N16
\inst|seven_seg1[3]~159\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~159_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111001011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~159_combout\);

-- Location: LCCOMB_X10_Y21_N4
\inst|seven_seg1[3]~165\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~165_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & \inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg1[3]~165_combout\);

-- Location: LCCOMB_X10_Y21_N6
\inst|seven_seg1[3]~166\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~166_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[3]~159_combout\ & ((!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (((\inst|seven_seg1[3]~165_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~159_combout\,
	datab => \inst|seven_seg1[3]~165_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~166_combout\);

-- Location: LCCOMB_X10_Y23_N2
\inst|seven_seg1[3]~178\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~178_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[5]~99_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[5]~99_combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg1[3]~178_combout\);

-- Location: LCCOMB_X9_Y23_N14
\inst|seven_seg1[3]~179\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~179_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~179_combout\);

-- Location: LCCOMB_X9_Y23_N8
\inst|seven_seg1[3]~180\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~180_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~178_combout\) # ((\inst|seven_seg1[3]~179_combout\ & \inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~178_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~179_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~180_combout\);

-- Location: LCCOMB_X9_Y22_N12
\inst|seven_seg1[3]~256\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~256_combout\ = (\inst|seven_seg1[3]~180_combout\) # ((\inst|seven_seg1[3]~166_combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~166_combout\,
	datad => \inst|seven_seg1[3]~180_combout\,
	combout => \inst|seven_seg1[3]~256_combout\);

-- Location: LCCOMB_X9_Y22_N14
\inst|seven_seg1[3]~184\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~184_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst4|inst1|inst5~combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- ((!\inst32|inst4|inst1|inst5~combout\))))) # (!\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst2|inst1|inst5~0_combout\ & !\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~184_combout\);

-- Location: LCCOMB_X9_Y22_N0
\inst|seven_seg0[6]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~233_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg0[6]~233_combout\);

-- Location: LCCOMB_X10_Y23_N28
\inst|seven_seg0[6]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~237_combout\ = (\inst32|inst667|inst1|inst5~0_combout\ & ((\inst15|inst57|inst1|inst~combout\ & ((\inst32|inst3|inst1|inst5~combout\))) # (!\inst15|inst57|inst1|inst~combout\ & (!\inst|seven_seg1[5]~99_combout\)))) # 
-- (!\inst32|inst667|inst1|inst5~0_combout\ & ((\inst15|inst57|inst1|inst~combout\ & (!\inst|seven_seg1[5]~99_combout\)) # (!\inst15|inst57|inst1|inst~combout\ & ((\inst32|inst3|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000101110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst|seven_seg1[5]~99_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg0[6]~237_combout\);

-- Location: LCCOMB_X9_Y22_N20
\inst|seven_seg1[3]~183\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~183_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~233_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~237_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~233_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~237_combout\,
	combout => \inst|seven_seg1[3]~183_combout\);

-- Location: LCCOMB_X9_Y22_N6
\inst|seven_seg1[3]~257\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~257_combout\ = (\inst|seven_seg1[3]~183_combout\) # ((\inst|seven_seg1[3]~184_combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~184_combout\,
	datad => \inst|seven_seg1[3]~183_combout\,
	combout => \inst|seven_seg1[3]~257_combout\);

-- Location: LCCOMB_X10_Y22_N6
\inst|seven_seg0[6]~255\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~255_combout\ = (\inst32|inst2|inst1|inst5~0_combout\) # ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~255_combout\);

-- Location: LCCOMB_X9_Y23_N26
\inst|seven_seg0[6]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~236_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst667|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst1|inst|inst5~combout\ & 
-- \inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~236_combout\);

-- Location: LCCOMB_X10_Y22_N16
\inst|seven_seg0[6]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~235_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (((!\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # 
-- (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~235_combout\);

-- Location: LCCOMB_X10_Y22_N18
\inst|seven_seg1[3]~181\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~181_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst57|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~235_combout\))) # 
-- (!\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg0[6]~236_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~236_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~235_combout\,
	combout => \inst|seven_seg1[3]~181_combout\);

-- Location: LCCOMB_X10_Y22_N14
\inst|seven_seg0[6]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~234_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (!\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~234_combout\);

-- Location: LCCOMB_X10_Y22_N4
\inst|seven_seg1[3]~182\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~182_combout\ = (\inst|seven_seg1[3]~181_combout\ & (((!\inst32|inst4|inst1|inst5~combout\)) # (!\inst|seven_seg0[6]~255_combout\))) # (!\inst|seven_seg1[3]~181_combout\ & (((\inst|seven_seg0[6]~234_combout\ & 
-- \inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~255_combout\,
	datab => \inst|seven_seg1[3]~181_combout\,
	datac => \inst|seven_seg0[6]~234_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~182_combout\);

-- Location: LCCOMB_X9_Y22_N16
\inst|seven_seg1[3]~185\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~185_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~182_combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[3]~257_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~257_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~182_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~185_combout\);

-- Location: LCCOMB_X10_Y22_N30
\inst|seven_seg0[6]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~238_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\)) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg0[6]~238_combout\);

-- Location: LCCOMB_X10_Y22_N24
\inst|seven_seg0[6]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg0[6]~239_combout\ = (\inst|Equal0~13_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst|Equal0~13_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg0[6]~239_combout\);

-- Location: LCCOMB_X10_Y22_N10
\inst|seven_seg1[3]~186\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~186_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # ((!\inst|seven_seg0[6]~238_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst4|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[6]~239_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101110001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~238_combout\,
	datad => \inst|seven_seg0[6]~239_combout\,
	combout => \inst|seven_seg1[3]~186_combout\);

-- Location: LCCOMB_X10_Y22_N20
\inst|seven_seg1[3]~187\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~187_combout\ = (\inst|seven_seg1[3]~186_combout\ & (((\inst|seven_seg0[6]~237_combout\)) # (!\inst32|inst4|inst1|inst5~combout\))) # (!\inst|seven_seg1[3]~186_combout\ & (\inst32|inst4|inst1|inst5~combout\ & 
-- ((\inst|seven_seg0[6]~235_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~186_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg0[6]~237_combout\,
	datad => \inst|seven_seg0[6]~235_combout\,
	combout => \inst|seven_seg1[3]~187_combout\);

-- Location: LCCOMB_X9_Y22_N18
\inst|seven_seg1[3]~188\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~188_combout\ = (\inst|seven_seg1[3]~185_combout\ & (((\inst|seven_seg1[3]~187_combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg1[3]~185_combout\ & (\inst|seven_seg1[3]~256_combout\ & 
-- ((\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~256_combout\,
	datab => \inst|seven_seg1[3]~185_combout\,
	datac => \inst|seven_seg1[3]~187_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~188_combout\);

-- Location: LCCOMB_X10_Y21_N2
\inst|seven_seg2[4]~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~26_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg2[4]~26_combout\);

-- Location: LCCOMB_X10_Y23_N10
\inst|seven_seg1[3]~164\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~164_combout\ = (\inst|Equal0~18_combout\ & ((\inst|seven_seg1[5]~99_combout\) # (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))) # (!\inst|Equal0~18_combout\ & (\inst|seven_seg1[5]~99_combout\ & 
-- (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~18_combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst|seven_seg1[5]~99_combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg1[3]~164_combout\);

-- Location: LCCOMB_X10_Y21_N0
\inst|seven_seg1[3]~167\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~167_combout\ = (\inst|seven_seg1[3]~166_combout\ & ((\inst32|inst57|inst1|inst5~combout\) # ((\inst|seven_seg2[4]~26_combout\ & \inst|seven_seg1[3]~164_combout\)))) # (!\inst|seven_seg1[3]~166_combout\ & 
-- (\inst|seven_seg2[4]~26_combout\ & ((\inst|seven_seg1[3]~164_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~166_combout\,
	datab => \inst|seven_seg2[4]~26_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~164_combout\,
	combout => \inst|seven_seg1[3]~167_combout\);

-- Location: LCCOMB_X10_Y24_N22
\inst|seven_seg1[3]~158\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~158_combout\ = \inst32|inst667|inst1|inst5~0_combout\ $ (\inst32|inst4|inst1|inst5~combout\ $ (\inst15|inst57|inst1|inst~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg1[3]~158_combout\);

-- Location: LCCOMB_X10_Y22_N28
\inst|seven_seg1[3]~162\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~162_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (((!\inst32|inst1|inst|inst5~combout\ & !\inst32|inst4|inst1|inst5~combout\)) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst4|inst1|inst5~combout\ & ((!\inst32|inst3|inst1|inst5~combout\) # (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001011111111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~162_combout\);

-- Location: LCCOMB_X10_Y24_N8
\inst|seven_seg1[3]~160\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~160_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[3]~159_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[5]~99_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[3]~159_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[5]~99_combout\,
	combout => \inst|seven_seg1[3]~160_combout\);

-- Location: LCCOMB_X10_Y24_N28
\inst|seven_seg1[3]~163\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~163_combout\ = (\inst|seven_seg1[3]~158_combout\ & ((\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[3]~162_combout\)) # (!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~160_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~158_combout\,
	datab => \inst|seven_seg1[3]~162_combout\,
	datac => \inst|seven_seg1[3]~160_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~163_combout\);

-- Location: LCCOMB_X10_Y24_N30
\inst|seven_seg1[3]~168\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~168_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~163_combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[3]~167_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~167_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~163_combout\,
	combout => \inst|seven_seg1[3]~168_combout\);

-- Location: LCCOMB_X10_Y23_N12
\inst|seven_seg1[3]~169\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~169_combout\ = (\inst|Equal0~13_combout\ & (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst|Equal0~13_combout\,
	datac => \inst15|inst57|inst1|inst~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~169_combout\);

-- Location: LCCOMB_X9_Y23_N28
\inst|Equal0~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~30_combout\ = (\inst32|inst1|inst|inst5~combout\ & (\SW[5]~input_o\ & (\inst32|inst2|inst1|inst5~0_combout\ & \SW[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \SW[5]~input_o\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \SW[0]~input_o\,
	combout => \inst|Equal0~30_combout\);

-- Location: LCCOMB_X9_Y23_N22
\inst|seven_seg1[3]~170\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~170_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # ((\inst|Equal0~30_combout\) # (!\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst|Equal0~30_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~170_combout\);

-- Location: LCCOMB_X9_Y23_N0
\inst|seven_seg1[3]~171\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~171_combout\ = (\inst|seven_seg1[3]~170_combout\) # ((\inst|seven_seg2[1]~25_combout\) # ((!\inst32|inst57|inst1|inst5~combout\ & \inst|Equal0~18_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~170_combout\,
	datab => \inst32|inst57|inst1|inst5~combout\,
	datac => \inst|seven_seg2[1]~25_combout\,
	datad => \inst|Equal0~18_combout\,
	combout => \inst|seven_seg1[3]~171_combout\);

-- Location: LCCOMB_X9_Y23_N18
\inst|Equal0~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~22_combout\ = (\inst|Equal0~9_combout\ & (\inst|Equal0~19_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & \inst|Equal0~30_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~9_combout\,
	datab => \inst|Equal0~19_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|Equal0~30_combout\,
	combout => \inst|Equal0~22_combout\);

-- Location: LCCOMB_X9_Y23_N20
\inst|seven_seg1[3]~172\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~172_combout\ = (!\inst|Equal0~22_combout\ & (\inst|seven_seg1[4]~128_combout\ & ((\inst|seven_seg1[3]~171_combout\) # (!\inst|Equal0~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~9_combout\,
	datab => \inst|seven_seg1[3]~171_combout\,
	datac => \inst|Equal0~22_combout\,
	datad => \inst|seven_seg1[4]~128_combout\,
	combout => \inst|seven_seg1[3]~172_combout\);

-- Location: LCCOMB_X10_Y23_N22
\inst|seven_seg1[3]~173\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~173_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~169_combout\) # ((\inst|seven_seg1[3]~172_combout\ & \inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~169_combout\,
	datab => \inst|seven_seg1[3]~172_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~173_combout\);

-- Location: LCCOMB_X10_Y23_N24
\inst|seven_seg1[3]~174\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~174_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst|Equal0~18_combout\) # ((\inst|seven_seg1[3]~172_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (((\inst|seven_seg1[5]~99_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~18_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[5]~99_combout\,
	datad => \inst|seven_seg1[3]~172_combout\,
	combout => \inst|seven_seg1[3]~174_combout\);

-- Location: LCCOMB_X9_Y22_N2
\inst|seven_seg1[3]~175\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~175_combout\ = (!\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~174_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~233_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg0[6]~233_combout\,
	datac => \inst|seven_seg1[3]~174_combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~175_combout\);

-- Location: LCCOMB_X10_Y24_N16
\inst|seven_seg1[3]~176\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~176_combout\ = (\inst|seven_seg1[3]~173_combout\) # (\inst|seven_seg1[3]~175_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[3]~173_combout\,
	datac => \inst|seven_seg1[3]~175_combout\,
	combout => \inst|seven_seg1[3]~176_combout\);

-- Location: LCCOMB_X9_Y22_N4
\inst|seven_seg1[3]~264\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~264_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst1|inst|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- (\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[3]~264_combout\);

-- Location: LCCOMB_X9_Y22_N22
\inst|seven_seg1[3]~265\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~265_combout\ = (\inst|seven_seg1[3]~264_combout\ & (\inst32|inst4|inst1|inst5~combout\ $ ((\inst32|inst667|inst1|inst5~combout\)))) # (!\inst|seven_seg1[3]~264_combout\ & (\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & !\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100001101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg1[3]~264_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[3]~265_combout\);

-- Location: LCCOMB_X10_Y24_N18
\inst|seven_seg1[3]~161\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~161_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[3]~158_combout\ & ((\inst|seven_seg1[3]~160_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (((\inst|seven_seg1[3]~265_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~158_combout\,
	datab => \inst|seven_seg1[3]~265_combout\,
	datac => \inst|seven_seg1[3]~160_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~161_combout\);

-- Location: LCCOMB_X10_Y24_N2
\inst|seven_seg1[3]~177\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~177_combout\ = (\inst|seven_seg1[3]~168_combout\ & ((\inst|seven_seg1[3]~176_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg1[3]~168_combout\ & (((\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg1[3]~161_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~168_combout\,
	datab => \inst|seven_seg1[3]~176_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~161_combout\,
	combout => \inst|seven_seg1[3]~177_combout\);

-- Location: LCCOMB_X10_Y24_N20
\inst|seven_seg1[3]~189\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~189_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~177_combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[3]~188_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~188_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~177_combout\,
	combout => \inst|seven_seg1[3]~189_combout\);

-- Location: LCCOMB_X14_Y21_N30
\inst|seven_seg1[2]~199\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~199_combout\ = (\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~199_combout\);

-- Location: LCCOMB_X14_Y21_N12
\inst|seven_seg1[2]~198\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~198_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ $ (((\inst32|inst667|inst1|inst5~combout\))))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst667|inst1|inst5~combout\ & 
-- ((!\inst32|inst1|inst|inst5~combout\) # (!\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~198_combout\);

-- Location: LCCOMB_X14_Y21_N0
\inst|seven_seg1[2]~200\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~200_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[2]~198_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (!\inst|seven_seg1[2]~199_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~199_combout\,
	datab => \inst32|inst5|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~198_combout\,
	combout => \inst|seven_seg1[2]~200_combout\);

-- Location: LCCOMB_X11_Y23_N18
\inst|seven_seg1[2]~192\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~192_combout\ = \inst32|inst4|inst1|inst5~combout\ $ (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~192_combout\);

-- Location: LCCOMB_X14_Y21_N6
\inst|seven_seg1[2]~258\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~258_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst4|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~258_combout\);

-- Location: LCCOMB_X14_Y21_N2
\inst|seven_seg1[2]~197\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~197_combout\ = (\inst|Equal0~20_combout\ & (!\inst32|inst667|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & \inst|seven_seg1[2]~258_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~20_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg1[2]~258_combout\,
	combout => \inst|seven_seg1[2]~197_combout\);

-- Location: LCCOMB_X14_Y21_N26
\inst|seven_seg1[2]~201\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~201_combout\ = (\inst|seven_seg1[2]~197_combout\) # ((\inst|seven_seg1[2]~200_combout\ & \inst|seven_seg1[2]~192_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[2]~200_combout\,
	datac => \inst|seven_seg1[2]~192_combout\,
	datad => \inst|seven_seg1[2]~197_combout\,
	combout => \inst|seven_seg1[2]~201_combout\);

-- Location: LCCOMB_X14_Y21_N4
\inst|seven_seg1[2]~202\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~202_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst1|inst|inst5~combout\ & 
-- \inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~202_combout\);

-- Location: LCCOMB_X14_Y24_N14
\inst|seven_seg1[2]~203\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~203_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst1|inst|inst5~combout\ & \inst32|inst3|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((!\inst32|inst3|inst1|inst5~combout\))))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~203_combout\);

-- Location: LCCOMB_X14_Y21_N22
\inst|seven_seg1[2]~204\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~204_combout\ = (\inst|Equal0~20_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[2]~202_combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & ((!\inst|seven_seg1[2]~203_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~20_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[2]~202_combout\,
	datad => \inst|seven_seg1[2]~203_combout\,
	combout => \inst|seven_seg1[2]~204_combout\);

-- Location: LCCOMB_X10_Y22_N22
\inst|seven_seg1[2]~205\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~205_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (((\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~205_combout\);

-- Location: LCCOMB_X10_Y22_N8
\inst|seven_seg1[2]~206\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~206_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[2]~205_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\ & \inst|Equal0~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~205_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|Equal0~11_combout\,
	combout => \inst|seven_seg1[2]~206_combout\);

-- Location: LCCOMB_X14_Y21_N16
\inst|seven_seg1[2]~207\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~207_combout\ = (\inst|seven_seg1[2]~204_combout\) # ((\inst|seven_seg1[2]~206_combout\ & \inst|seven_seg0[0]~152_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~204_combout\,
	datac => \inst|seven_seg1[2]~206_combout\,
	datad => \inst|seven_seg0[0]~152_combout\,
	combout => \inst|seven_seg1[2]~207_combout\);

-- Location: LCCOMB_X14_Y21_N10
\inst|seven_seg1[2]~208\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~208_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[2]~201_combout\) # ((\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & (((!\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg1[2]~207_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~201_combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~207_combout\,
	combout => \inst|seven_seg1[2]~208_combout\);

-- Location: LCCOMB_X11_Y23_N16
\inst|Equal0~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~23_combout\ = (\inst32|inst4|inst2~0_combout\ & (!\inst25|inst4|inst1|inst5~combout\ & (\inst17|inst4|inst1|inst5~combout\ $ (!\inst25|inst4|inst2~0_combout\)))) # (!\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & 
-- (\inst17|inst4|inst1|inst5~combout\ $ (!\inst25|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst17|inst4|inst1|inst5~combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst4|inst2~0_combout\,
	combout => \inst|Equal0~23_combout\);

-- Location: LCCOMB_X15_Y21_N18
\inst|seven_seg1[2]~209\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~209_combout\ = (\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ $ (\inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst1|inst|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ $ (((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst3|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~209_combout\);

-- Location: LCCOMB_X15_Y21_N4
\inst|seven_seg1[2]~210\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~210_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & (\inst|Equal0~23_combout\ & \inst|seven_seg1[2]~209_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|Equal0~23_combout\,
	datad => \inst|seven_seg1[2]~209_combout\,
	combout => \inst|seven_seg1[2]~210_combout\);

-- Location: LCCOMB_X14_Y21_N20
\inst|seven_seg1[2]~211\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~211_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~211_combout\);

-- Location: LCCOMB_X14_Y21_N14
\inst|seven_seg1[2]~212\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~212_combout\ = (\inst|seven_seg1[2]~211_combout\ & (\inst|seven_seg0[0]~152_combout\ & ((!\inst|Equal0~17_combout\) # (!\inst|Equal0~27_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~27_combout\,
	datab => \inst|seven_seg1[2]~211_combout\,
	datac => \inst|Equal0~17_combout\,
	datad => \inst|seven_seg0[0]~152_combout\,
	combout => \inst|seven_seg1[2]~212_combout\);

-- Location: LCCOMB_X14_Y21_N24
\inst|seven_seg1[2]~213\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~213_combout\ = (\inst|seven_seg1[2]~212_combout\) # ((\inst|Equal0~20_combout\ & \inst32|inst667|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~20_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[2]~212_combout\,
	combout => \inst|seven_seg1[2]~213_combout\);

-- Location: LCCOMB_X14_Y21_N18
\inst|seven_seg1[2]~214\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~214_combout\ = (\inst|seven_seg1[2]~210_combout\) # ((\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst1|inst5~combout\ & \inst|seven_seg1[2]~213_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~210_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~213_combout\,
	combout => \inst|seven_seg1[2]~214_combout\);

-- Location: LCCOMB_X14_Y21_N8
\inst|seven_seg1[2]~194\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~194_combout\ = (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & !\inst|seven_seg1[2]~192_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst|seven_seg1[2]~192_combout\,
	combout => \inst|seven_seg1[2]~194_combout\);

-- Location: LCCOMB_X15_Y21_N22
\inst|seven_seg1[2]~195\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~195_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[2]~194_combout\) # ((!\inst32|inst57|inst1|inst5~combout\ & !\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~194_combout\,
	combout => \inst|seven_seg1[2]~195_combout\);

-- Location: LCCOMB_X15_Y21_N8
\inst|seven_seg1[2]~190\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~190_combout\ = (\inst|Equal0~23_combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & (!\inst32|inst3|inst1|inst5~combout\ & \inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~23_combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~190_combout\);

-- Location: LCCOMB_X15_Y21_N26
\inst|seven_seg1[2]~191\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~191_combout\ = (!\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & ((!\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[2]~191_combout\);

-- Location: LCCOMB_X15_Y21_N28
\inst|seven_seg1[2]~193\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~193_combout\ = (\inst|seven_seg1[2]~190_combout\) # ((!\inst|seven_seg1[2]~192_combout\ & (\inst32|inst3|inst1|inst5~combout\ & \inst|seven_seg1[2]~191_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~192_combout\,
	datab => \inst|seven_seg1[2]~190_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~191_combout\,
	combout => \inst|seven_seg1[2]~193_combout\);

-- Location: LCCOMB_X15_Y21_N24
\inst|seven_seg1[2]~196\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~196_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst|seven_seg1[2]~193_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[2]~195_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg1[2]~195_combout\,
	datad => \inst|seven_seg1[2]~193_combout\,
	combout => \inst|seven_seg1[2]~196_combout\);

-- Location: LCCOMB_X14_Y21_N28
\inst|seven_seg1[2]~215\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[2]~215_combout\ = (\inst|seven_seg1[2]~208_combout\ & ((\inst|seven_seg1[2]~214_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg1[2]~208_combout\ & (((\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg1[2]~196_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~208_combout\,
	datab => \inst|seven_seg1[2]~214_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~196_combout\,
	combout => \inst|seven_seg1[2]~215_combout\);

-- Location: LCCOMB_X8_Y22_N10
\inst|seven_seg1[1]~227\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~227_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst1|inst|inst5~combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst2|inst1|inst5~0_combout\ & 
-- ((\inst32|inst1|inst|inst5~combout\ & (\inst32|inst3|inst1|inst5~combout\ & !\inst32|inst488|inst1|inst5~combout\)) # (!\inst32|inst1|inst|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & \inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~227_combout\);

-- Location: LCCOMB_X8_Y22_N12
\inst|seven_seg1[1]~228\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~228_combout\ = (\inst|seven_seg2[4]~26_combout\ & ((\inst|seven_seg1[1]~227_combout\ & ((!\inst32|inst3|inst1|inst5~combout\))) # (!\inst|seven_seg1[1]~227_combout\ & (!\inst32|inst667|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[1]~227_combout\,
	datab => \inst|seven_seg2[4]~26_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~228_combout\);

-- Location: LCCOMB_X8_Y22_N22
\inst|seven_seg1[1]~229\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~229_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & 
-- \inst32|inst1|inst|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~229_combout\);

-- Location: LCCOMB_X8_Y22_N24
\inst|seven_seg1[1]~230\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~230_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~229_combout\ & ((!\inst32|inst3|inst1|inst5~combout\) # (!\inst32|inst667|inst1|inst5~combout\))) # (!\inst|seven_seg1[1]~229_combout\ & 
-- ((\inst32|inst667|inst1|inst5~combout\) # (\inst32|inst3|inst1|inst5~combout\))))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~229_combout\) # (\inst32|inst3|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[1]~229_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~230_combout\);

-- Location: LCCOMB_X8_Y22_N2
\inst|seven_seg1[1]~231\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~231_combout\ = (\inst|seven_seg1[1]~228_combout\) # ((\inst|seven_seg1[1]~230_combout\ & (\inst32|inst57|inst1|inst5~combout\ & !\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[1]~228_combout\,
	datab => \inst|seven_seg1[1]~230_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~231_combout\);

-- Location: LCCOMB_X12_Y22_N30
\inst|seven_seg1[1]~224\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~224_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~224_combout\);

-- Location: LCCOMB_X12_Y22_N0
\inst|seven_seg1[1]~225\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~225_combout\ = (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg1[1]~224_combout\ & !\inst32|inst57|inst1|inst5~combout\)) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (!\inst|seven_seg1[1]~224_combout\ & \inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[1]~224_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~225_combout\);

-- Location: LCCOMB_X12_Y22_N26
\inst|seven_seg1[1]~222\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~222_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst1|inst|inst5~combout\) # (!\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst2|inst1|inst5~0_combout\))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~222_combout\);

-- Location: LCCOMB_X12_Y22_N24
\inst|seven_seg1[1]~221\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~221_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~221_combout\);

-- Location: LCCOMB_X12_Y22_N28
\inst|seven_seg1[1]~223\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~223_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~221_combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & 
-- (\inst|seven_seg1[1]~222_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[1]~222_combout\,
	datad => \inst|seven_seg1[1]~221_combout\,
	combout => \inst|seven_seg1[1]~223_combout\);

-- Location: LCCOMB_X12_Y22_N2
\inst|seven_seg1[1]~226\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~226_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~223_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~225_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg1[1]~225_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[1]~223_combout\,
	combout => \inst|seven_seg1[1]~226_combout\);

-- Location: LCCOMB_X8_Y22_N28
\inst|seven_seg1[1]~232\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~232_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (((\inst32|inst5|inst1|inst5~combout\) # (\inst|seven_seg1[1]~226_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[1]~231_combout\ & 
-- (!\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~231_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg1[1]~226_combout\,
	combout => \inst|seven_seg1[1]~232_combout\);

-- Location: LCCOMB_X8_Y22_N14
\inst|seven_seg1[1]~234\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~234_combout\ = (\inst32|inst4|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & 
-- \inst32|inst1|inst|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst2|inst1|inst5~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~234_combout\);

-- Location: LCCOMB_X8_Y22_N8
\inst|seven_seg1[1]~235\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~235_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # ((\inst|seven_seg1[1]~234_combout\ & \inst32|inst3|inst1|inst5~combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~234_combout\ & \inst32|inst3|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[1]~234_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~235_combout\);

-- Location: LCCOMB_X8_Y22_N18
\inst|seven_seg1[1]~236\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~236_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst57|inst1|inst5~combout\ & (\inst|seven_seg1[1]~235_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~235_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~236_combout\);

-- Location: LCCOMB_X8_Y23_N22
\inst|seven_seg1[1]~237\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~237_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\) # (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~237_combout\);

-- Location: LCCOMB_X8_Y23_N0
\inst|seven_seg1[1]~238\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~238_combout\ = (\inst|seven_seg2[4]~26_combout\ & (((\inst|Equal0~16_combout\ & \inst|Equal0~9_combout\)) # (!\inst|seven_seg1[1]~237_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[4]~26_combout\,
	datab => \inst|Equal0~16_combout\,
	datac => \inst|seven_seg1[1]~237_combout\,
	datad => \inst|Equal0~9_combout\,
	combout => \inst|seven_seg1[1]~238_combout\);

-- Location: LCCOMB_X8_Y24_N0
\inst|seven_seg1[1]~233\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~233_combout\ = (\inst32|inst3|inst1|inst5~combout\ & (((!\inst32|inst4|inst1|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\ & ((!\inst32|inst4|inst1|inst5~combout\) # 
-- (!\inst32|inst1|inst|inst5~combout\))) # (!\inst32|inst2|inst1|inst5~0_combout\ & ((\inst32|inst1|inst|inst5~combout\) # (\inst32|inst4|inst1|inst5~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~233_combout\);

-- Location: LCCOMB_X8_Y24_N14
\inst|seven_seg1[1]~260\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~260_combout\ = (\inst|seven_seg1[1]~233_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|seven_seg1[1]~233_combout\,
	combout => \inst|seven_seg1[1]~260_combout\);

-- Location: LCCOMB_X8_Y22_N4
\inst|seven_seg1[1]~239\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~239_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~236_combout\ & (\inst|seven_seg1[1]~238_combout\)) # (!\inst|seven_seg1[1]~236_combout\ & ((\inst|seven_seg1[1]~260_combout\))))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (\inst|seven_seg1[1]~236_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~236_combout\,
	datac => \inst|seven_seg1[1]~238_combout\,
	datad => \inst|seven_seg1[1]~260_combout\,
	combout => \inst|seven_seg1[1]~239_combout\);

-- Location: LCCOMB_X8_Y22_N20
\inst|seven_seg1[1]~218\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~218_combout\ = (\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~218_combout\);

-- Location: LCCOMB_X8_Y22_N6
\inst|seven_seg1[1]~219\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~219_combout\ = (\inst32|inst57|inst1|inst5~combout\ & (\inst32|inst488|inst1|inst5~combout\ & ((!\inst|seven_seg1[1]~218_combout\) # (!\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst57|inst1|inst5~combout\ & 
-- (!\inst32|inst667|inst1|inst5~combout\ & (!\inst|seven_seg1[1]~218_combout\ & !\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~218_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg1[1]~219_combout\);

-- Location: LCCOMB_X10_Y23_N30
\inst|seven_seg1[1]~216\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~216_combout\ = (\inst|Equal0~18_combout\ & (!\inst32|inst1|inst|inst5~combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (!\inst15|inst57|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~18_combout\,
	datab => \inst32|inst667|inst1|inst5~0_combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst15|inst57|inst1|inst~combout\,
	combout => \inst|seven_seg1[1]~216_combout\);

-- Location: LCCOMB_X9_Y22_N28
\inst|seven_seg1[1]~217\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~217_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\ & \inst32|inst1|inst|inst5~combout\)))) # (!\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst1|inst|inst5~combout\,
	combout => \inst|seven_seg1[1]~217_combout\);

-- Location: LCCOMB_X9_Y22_N24
\inst|seven_seg1[1]~259\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~259_combout\ = (\inst32|inst4|inst2~0_combout\ & ((\inst25|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~217_combout\))) # (!\inst25|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~216_combout\)))) # 
-- (!\inst32|inst4|inst2~0_combout\ & ((\inst25|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~216_combout\)) # (!\inst25|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~217_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst2~0_combout\,
	datab => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[1]~216_combout\,
	datad => \inst|seven_seg1[1]~217_combout\,
	combout => \inst|seven_seg1[1]~259_combout\);

-- Location: LCCOMB_X8_Y22_N0
\inst|seven_seg1[1]~220\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~220_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((\inst32|inst488|inst1|inst5~combout\ & \inst|seven_seg1[1]~259_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~219_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[1]~219_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg1[1]~259_combout\,
	combout => \inst|seven_seg1[1]~220_combout\);

-- Location: LCCOMB_X8_Y22_N30
\inst|seven_seg1[1]~240\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[1]~240_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[1]~232_combout\ & (\inst|seven_seg1[1]~239_combout\)) # (!\inst|seven_seg1[1]~232_combout\ & ((\inst|seven_seg1[1]~220_combout\))))) # 
-- (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[1]~232_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~232_combout\,
	datac => \inst|seven_seg1[1]~239_combout\,
	datad => \inst|seven_seg1[1]~220_combout\,
	combout => \inst|seven_seg1[1]~240_combout\);

-- Location: LCCOMB_X9_Y22_N30
\inst|seven_seg1[3]~248\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~248_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg1[1]~216_combout\)) # (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg0[6]~237_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg1[1]~216_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg0[6]~237_combout\,
	combout => \inst|seven_seg1[3]~248_combout\);

-- Location: LCCOMB_X9_Y22_N26
\inst|seven_seg1[3]~263\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~263_combout\ = (\inst|seven_seg1[3]~248_combout\) # ((\inst|seven_seg1[3]~184_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~248_combout\,
	datab => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~184_combout\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst|seven_seg1[3]~263_combout\);

-- Location: LCCOMB_X10_Y22_N2
\inst|seven_seg1[3]~246\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~246_combout\ = (!\inst32|inst667|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & ((!\inst32|inst2|inst1|inst5~0_combout\) # (!\inst32|inst1|inst|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~246_combout\);

-- Location: LCCOMB_X10_Y22_N12
\inst|seven_seg1[3]~247\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~247_combout\ = (!\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~246_combout\))) # (!\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[6]~236_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[6]~236_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~246_combout\,
	combout => \inst|seven_seg1[3]~247_combout\);

-- Location: LCCOMB_X9_Y22_N8
\inst|seven_seg1[0]~249\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[0]~249_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst34|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~247_combout\))) # 
-- (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[3]~263_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~263_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[3]~247_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg1[0]~249_combout\);

-- Location: LCCOMB_X9_Y22_N10
\inst|seven_seg1[0]~250\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[0]~250_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg1[0]~249_combout\ & (\inst|seven_seg1[3]~187_combout\)) # (!\inst|seven_seg1[0]~249_combout\ & ((\inst|seven_seg1[3]~180_combout\))))) # 
-- (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg1[0]~249_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~187_combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg1[0]~249_combout\,
	datad => \inst|seven_seg1[3]~180_combout\,
	combout => \inst|seven_seg1[0]~250_combout\);

-- Location: LCCOMB_X10_Y23_N18
\inst|seven_seg1[3]~243\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~243_combout\ = (!\inst|Equal0~18_combout\ & (\inst|seven_seg1[3]~172_combout\ & (\inst32|inst667|inst1|inst5~combout\ & \inst32|inst4|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~18_combout\,
	datab => \inst|seven_seg1[3]~172_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~243_combout\);

-- Location: LCCOMB_X10_Y23_N16
\inst|seven_seg1[3]~242\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~242_combout\ = (\inst|Equal0~18_combout\ & (!\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst1|inst|inst5~combout\ & !\inst32|inst667|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~18_combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst32|inst1|inst|inst5~combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg1[3]~242_combout\);

-- Location: LCCOMB_X10_Y23_N4
\inst|seven_seg1[3]~244\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~244_combout\ = (\inst|seven_seg1[3]~173_combout\) # ((!\inst32|inst57|inst1|inst5~combout\ & ((\inst|seven_seg1[3]~243_combout\) # (\inst|seven_seg1[3]~242_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst|seven_seg1[3]~243_combout\,
	datac => \inst|seven_seg1[3]~173_combout\,
	datad => \inst|seven_seg1[3]~242_combout\,
	combout => \inst|seven_seg1[3]~244_combout\);

-- Location: LCCOMB_X10_Y21_N22
\inst|seven_seg1[3]~262\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~262_combout\ = (\inst|seven_seg1[3]~166_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (\inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|seven_seg1[3]~166_combout\,
	combout => \inst|seven_seg1[3]~262_combout\);

-- Location: LCCOMB_X10_Y24_N14
\inst|seven_seg1[0]~241\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[0]~241_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (((\inst32|inst488|inst1|inst5~combout\) # (\inst|seven_seg1[3]~163_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg1[3]~262_combout\ & 
-- (!\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|seven_seg1[3]~262_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~163_combout\,
	combout => \inst|seven_seg1[0]~241_combout\);

-- Location: LCCOMB_X10_Y24_N6
\inst|seven_seg1[3]~261\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[3]~261_combout\ = (\inst|seven_seg1[3]~265_combout\ & (\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst32|inst4|inst2~0_combout\,
	datad => \inst|seven_seg1[3]~265_combout\,
	combout => \inst|seven_seg1[3]~261_combout\);

-- Location: LCCOMB_X10_Y24_N0
\inst|seven_seg1[0]~245\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[0]~245_combout\ = (\inst|seven_seg1[0]~241_combout\ & ((\inst|seven_seg1[3]~244_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg1[0]~241_combout\ & (((\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg1[3]~261_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[3]~244_combout\,
	datab => \inst|seven_seg1[0]~241_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg1[3]~261_combout\,
	combout => \inst|seven_seg1[0]~245_combout\);

-- Location: LCCOMB_X10_Y24_N10
\inst|seven_seg1[0]~251\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg1[0]~251_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg1[0]~245_combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & (\inst|seven_seg1[0]~250_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[0]~250_combout\,
	datab => \inst|seven_seg1[0]~245_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg1[0]~251_combout\);

-- Location: LCCOMB_X12_Y21_N2
\inst|seven_seg2[6]~27\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~27_combout\ = (\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst1|inst|inst5~combout\ & (\inst32|inst2|inst1|inst5~0_combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg2[6]~27_combout\);

-- Location: LCCOMB_X11_Y23_N20
\inst|seven_seg2[6]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~28_combout\ = (\inst|seven_seg0[0]~152_combout\ & (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (\inst|seven_seg2[6]~27_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg0[0]~152_combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg2[6]~27_combout\,
	combout => \inst|seven_seg2[6]~28_combout\);

-- Location: LCCOMB_X14_Y23_N16
\inst|seven_seg2[6]~29\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~29_combout\ = (\inst|Equal0~22_combout\ & (((!\inst32|inst34|inst1|inst5~combout\ & \inst|Equal0~20_combout\)))) # (!\inst|Equal0~22_combout\ & ((\inst|seven_seg2[6]~28_combout\) # ((!\inst32|inst34|inst1|inst5~combout\ & 
-- \inst|Equal0~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~22_combout\,
	datab => \inst|seven_seg2[6]~28_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|Equal0~20_combout\,
	combout => \inst|seven_seg2[6]~29_combout\);

-- Location: LCCOMB_X15_Y23_N26
\inst|seven_seg2[6]~31\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~31_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst57|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst34|inst1|inst5~combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg2[6]~31_combout\);

-- Location: LCCOMB_X14_Y23_N26
\inst|seven_seg2[6]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~30_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst5|inst1|inst5~combout\) # ((\inst|seven_seg2[1]~25_combout\ & \inst|Equal0~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg2[1]~25_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|Equal0~11_combout\,
	combout => \inst|seven_seg2[6]~30_combout\);

-- Location: LCCOMB_X14_Y23_N12
\inst|seven_seg2[6]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~32_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (((\inst|seven_seg2[6]~30_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (((!\inst32|inst5|inst1|inst5~combout\)) # (!\inst|seven_seg2[6]~31_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000111110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg2[6]~31_combout\,
	datac => \inst|seven_seg2[6]~30_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg2[6]~32_combout\);

-- Location: LCCOMB_X14_Y23_N14
\inst|seven_seg2[6]~33\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[6]~33_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg2[6]~29_combout\))) # (!\inst32|inst488|inst1|inst5~combout\ & (((\inst|seven_seg2[6]~32_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000111110000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg2[6]~29_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg2[6]~32_combout\,
	combout => \inst|seven_seg2[6]~33_combout\);

-- Location: LCCOMB_X14_Y23_N18
\inst|seven_seg2[5]~39\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~39_combout\ = (((\inst|Equal0~20_combout\ & !\inst32|inst4|inst1|inst5~combout\)) # (!\inst32|inst34|inst1|inst5~combout\)) # (!\inst32|inst488|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|Equal0~20_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg2[5]~39_combout\);

-- Location: LCCOMB_X14_Y23_N0
\inst|Equal0~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~24_combout\ = (\inst|Equal0~12_combout\ & (\inst|Equal0~20_combout\ & (\inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\ & \inst|Equal0~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~12_combout\,
	datab => \inst|Equal0~20_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst|Equal0~10_combout\,
	combout => \inst|Equal0~24_combout\);

-- Location: LCCOMB_X14_Y23_N28
\inst|seven_seg2[5]~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~71_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\SW[5]~input_o\ & (\SW[0]~input_o\ & \inst|Equal0~13_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \SW[5]~input_o\,
	datac => \SW[0]~input_o\,
	datad => \inst|Equal0~13_combout\,
	combout => \inst|seven_seg2[5]~71_combout\);

-- Location: LCCOMB_X14_Y23_N22
\inst|seven_seg2[5]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~37_combout\ = (\inst|Equal0~24_combout\) # ((!\inst|seven_seg2[5]~71_combout\ & (!\inst32|inst34|inst1|inst5~combout\ & \inst|Equal0~20_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~24_combout\,
	datab => \inst|seven_seg2[5]~71_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|Equal0~20_combout\,
	combout => \inst|seven_seg2[5]~37_combout\);

-- Location: LCCOMB_X14_Y23_N20
\inst|seven_seg2[5]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~36_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (((\inst32|inst667|inst1|inst5~combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[6]~30_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|Equal0~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|Equal0~24_combout\,
	datac => \inst|seven_seg2[6]~30_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg2[5]~36_combout\);

-- Location: LCCOMB_X15_Y21_N6
\inst|seven_seg2[5]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~34_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg2[5]~34_combout\);

-- Location: LCCOMB_X14_Y23_N10
\inst|seven_seg2[5]~35\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~35_combout\ = (\inst|Equal0~24_combout\ & ((!\inst|seven_seg2[5]~34_combout\) # (!\inst|seven_seg2[1]~25_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|seven_seg2[1]~25_combout\,
	datac => \inst|seven_seg2[5]~34_combout\,
	datad => \inst|Equal0~24_combout\,
	combout => \inst|seven_seg2[5]~35_combout\);

-- Location: LCCOMB_X14_Y23_N8
\inst|seven_seg2[5]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~38_combout\ = (\inst|seven_seg2[5]~36_combout\ & ((\inst|seven_seg2[5]~37_combout\) # ((!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg2[5]~36_combout\ & (((\inst32|inst488|inst1|inst5~combout\ & 
-- \inst|seven_seg2[5]~35_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[5]~37_combout\,
	datab => \inst|seven_seg2[5]~36_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg2[5]~35_combout\,
	combout => \inst|seven_seg2[5]~38_combout\);

-- Location: LCCOMB_X14_Y23_N6
\inst|seven_seg2[5]~72\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~72_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg0[0]~152_combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg0[0]~152_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg2[5]~72_combout\);

-- Location: LCCOMB_X14_Y23_N4
\inst|seven_seg2[5]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[5]~40_combout\ = (\inst32|inst667|inst1|inst5~combout\ & (((\inst|seven_seg2[5]~38_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[5]~39_combout\) # ((\inst|seven_seg2[5]~38_combout\ & 
-- \inst|seven_seg2[5]~72_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst|seven_seg2[5]~39_combout\,
	datac => \inst|seven_seg2[5]~38_combout\,
	datad => \inst|seven_seg2[5]~72_combout\,
	combout => \inst|seven_seg2[5]~40_combout\);

-- Location: LCCOMB_X9_Y21_N0
\inst|seven_seg2[2]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~46_combout\ = (\inst32|inst5|inst1|inst5~combout\ & (\inst32|inst34|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # (!\inst|seven_seg2[4]~26_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg2[4]~26_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg2[2]~46_combout\);

-- Location: LCCOMB_X11_Y23_N2
\inst|seven_seg2[4]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~44_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (!\inst32|inst3|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst2~0_combout\ $ ((!\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000111000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg2[4]~44_combout\);

-- Location: LCCOMB_X11_Y23_N0
\inst|seven_seg2[4]~43\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~43_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|Equal0~11_combout\ & (\inst32|inst57|inst1|inst5~combout\ & !\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|Equal0~11_combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg2[4]~43_combout\);

-- Location: LCCOMB_X11_Y23_N28
\inst|seven_seg2[4]~45\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~45_combout\ = (\inst|seven_seg2[4]~43_combout\) # ((\inst32|inst5|inst1|inst5~combout\ & ((\inst|seven_seg2[4]~44_combout\) # (\inst32|inst34|inst1|inst5~combout\))) # (!\inst32|inst5|inst1|inst5~combout\ & 
-- ((!\inst32|inst34|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst|seven_seg2[4]~44_combout\,
	datac => \inst32|inst34|inst1|inst5~combout\,
	datad => \inst|seven_seg2[4]~43_combout\,
	combout => \inst|seven_seg2[4]~45_combout\);

-- Location: LCCOMB_X12_Y22_N14
\inst|seven_seg2[4]~47\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~47_combout\ = (\inst32|inst667|inst1|inst5~combout\ & ((\inst32|inst488|inst1|inst5~combout\) # ((\inst|seven_seg2[4]~45_combout\)))) # (!\inst32|inst667|inst1|inst5~combout\ & (!\inst32|inst488|inst1|inst5~combout\ & 
-- (!\inst|seven_seg2[2]~46_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101110001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg2[2]~46_combout\,
	datad => \inst|seven_seg2[4]~45_combout\,
	combout => \inst|seven_seg2[4]~47_combout\);

-- Location: LCCOMB_X11_Y23_N6
\inst|seven_seg2[2]~41\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~41_combout\ = (\inst25|inst5|inst1|inst5~combout\ & (\inst32|inst4|inst2~0_combout\ & (\inst25|inst4|inst1|inst5~combout\ & \inst25|inst57|inst1|inst5~combout\))) # (!\inst25|inst5|inst1|inst5~combout\ & 
-- (!\inst25|inst57|inst1|inst5~combout\ & ((!\inst25|inst4|inst1|inst5~combout\) # (!\inst32|inst4|inst2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst25|inst5|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst25|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg2[2]~41_combout\);

-- Location: LCCOMB_X12_Y22_N4
\inst|seven_seg2[4]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~42_combout\ = ((\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # (\inst|Equal0~11_combout\)))) # (!\inst|seven_seg2[2]~41_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst57|inst1|inst5~combout\,
	datab => \inst32|inst4|inst1|inst5~combout\,
	datac => \inst|Equal0~11_combout\,
	datad => \inst|seven_seg2[2]~41_combout\,
	combout => \inst|seven_seg2[4]~42_combout\);

-- Location: LCCOMB_X5_Y24_N28
\inst|seven_seg2[4]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~14_combout\ = (\SW[9]~input_o\ & \SW[8]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \SW[9]~input_o\,
	datad => \SW[8]~input_o\,
	combout => \inst|seven_seg2[4]~14_combout\);

-- Location: LCCOMB_X5_Y24_N6
\inst|seven_seg2[4]~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~11_combout\ = (\SW[6]~input_o\ & (\inst|seven_seg2[4]~14_combout\ & (\SW[1]~input_o\ & \SW[7]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \inst|seven_seg2[4]~14_combout\,
	datac => \SW[1]~input_o\,
	datad => \SW[7]~input_o\,
	combout => \inst|seven_seg2[4]~11_combout\);

-- Location: LCCOMB_X4_Y24_N14
\inst|seven_seg2[4]~75\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~75_combout\ = (\SW[4]~input_o\ & (\SW[3]~input_o\ & (\SW[2]~input_o\ & \inst|seven_seg2[4]~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[4]~input_o\,
	datab => \SW[3]~input_o\,
	datac => \SW[2]~input_o\,
	datad => \inst|seven_seg2[4]~11_combout\,
	combout => \inst|seven_seg2[4]~75_combout\);

-- Location: LCCOMB_X15_Y23_N30
\inst|seven_seg2[4]~49\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~49_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst|seven_seg2[4]~75_combout\ & ((!\inst|Equal0~20_combout\) # (!\inst|seven_seg1[2]~258_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & (((\inst|Equal0~20_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~258_combout\,
	datab => \inst|Equal0~20_combout\,
	datac => \inst|seven_seg2[4]~75_combout\,
	datad => \inst32|inst34|inst1|inst5~combout\,
	combout => \inst|seven_seg2[4]~49_combout\);

-- Location: LCCOMB_X12_Y22_N8
\inst|seven_seg2[4]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~50_combout\ = (\inst|seven_seg2[4]~47_combout\ & (((\inst|seven_seg2[4]~49_combout\) # (!\inst32|inst488|inst1|inst5~combout\)))) # (!\inst|seven_seg2[4]~47_combout\ & (\inst|seven_seg2[4]~42_combout\ & 
-- ((\inst32|inst488|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[4]~47_combout\,
	datab => \inst|seven_seg2[4]~42_combout\,
	datac => \inst|seven_seg2[4]~49_combout\,
	datad => \inst32|inst488|inst1|inst5~combout\,
	combout => \inst|seven_seg2[4]~50_combout\);

-- Location: LCCOMB_X11_Y23_N14
\inst|seven_seg2[3]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[3]~52_combout\ = ((!\inst32|inst3|inst1|inst5~combout\ & (!\inst32|inst57|inst1|inst5~combout\ & !\inst|seven_seg1[2]~192_combout\))) # (!\inst|seven_seg1[6]~35_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[6]~35_combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst57|inst1|inst5~combout\,
	datad => \inst|seven_seg1[2]~192_combout\,
	combout => \inst|seven_seg2[3]~52_combout\);

-- Location: LCCOMB_X11_Y21_N18
\inst|seven_seg2[3]~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[3]~51_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst25|inst34|inst1|inst5~combout\ $ (\inst32|inst34|inst1|inst~combout\)))) # (!\inst32|inst5|inst1|inst5~combout\ & (!\inst|seven_seg2[4]~26_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[4]~26_combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst32|inst34|inst1|inst~combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg2[3]~51_combout\);

-- Location: LCCOMB_X14_Y23_N30
\inst|seven_seg2[3]~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[3]~53_combout\ = (\inst32|inst34|inst1|inst5~combout\ & ((\inst|seven_seg2[3]~51_combout\) # ((\inst|seven_seg2[3]~52_combout\ & !\inst32|inst488|inst1|inst5~combout\)))) # (!\inst32|inst34|inst1|inst5~combout\ & 
-- (\inst|seven_seg2[3]~52_combout\ & (!\inst32|inst488|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|seven_seg2[3]~52_combout\,
	datac => \inst32|inst488|inst1|inst5~combout\,
	datad => \inst|seven_seg2[3]~51_combout\,
	combout => \inst|seven_seg2[3]~53_combout\);

-- Location: LCCOMB_X14_Y23_N24
\inst|seven_seg2[3]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[3]~54_combout\ = (\inst|seven_seg2[4]~24_combout\ & (\inst|seven_seg0[0]~152_combout\ & (!\inst|Equal0~18_combout\ & !\inst|seven_seg1[2]~192_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[4]~24_combout\,
	datab => \inst|seven_seg0[0]~152_combout\,
	datac => \inst|Equal0~18_combout\,
	datad => \inst|seven_seg1[2]~192_combout\,
	combout => \inst|seven_seg2[3]~54_combout\);

-- Location: LCCOMB_X14_Y23_N2
\inst|seven_seg2[3]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[3]~55_combout\ = (\inst|seven_seg2[3]~54_combout\ & (((\inst|seven_seg2[5]~38_combout\)))) # (!\inst|seven_seg2[3]~54_combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[5]~38_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg2[3]~53_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[3]~53_combout\,
	datab => \inst|seven_seg2[3]~54_combout\,
	datac => \inst|seven_seg2[5]~38_combout\,
	datad => \inst32|inst667|inst1|inst5~combout\,
	combout => \inst|seven_seg2[3]~55_combout\);

-- Location: LCCOMB_X12_Y21_N20
\inst|seven_seg2[2]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~58_combout\ = (\inst32|inst3|inst1|inst5~combout\ & ((\inst32|inst2|inst1|inst5~0_combout\) # ((\inst32|inst1|inst|inst5~combout\ & \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst1|inst|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst33|LPM_MUX_component|auto_generated|result_node[0]~0_combout\,
	datad => \inst32|inst3|inst1|inst5~combout\,
	combout => \inst|seven_seg2[2]~58_combout\);

-- Location: LCCOMB_X12_Y21_N30
\inst|seven_seg2[2]~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~59_combout\ = (!\inst32|inst4|inst1|inst5~combout\ & ((\inst|Equal0~17_combout\) # (!\inst|seven_seg2[2]~58_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|seven_seg2[2]~58_combout\,
	datad => \inst|Equal0~17_combout\,
	combout => \inst|seven_seg2[2]~59_combout\);

-- Location: LCCOMB_X11_Y21_N4
\inst|seven_seg2[2]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~60_combout\ = (\inst|seven_seg2[2]~59_combout\) # ((\inst32|inst34|inst1|inst~combout\ $ (!\inst25|inst34|inst1|inst5~combout\)) # (!\inst32|inst57|inst1|inst5~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst|seven_seg2[2]~59_combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg2[2]~60_combout\);

-- Location: LCCOMB_X12_Y22_N18
\inst|seven_seg2[2]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~56_combout\ = (\inst32|inst57|inst1|inst5~combout\ & ((\inst32|inst3|inst1|inst5~combout\) # ((\inst32|inst2|inst1|inst5~0_combout\) # (\inst32|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst3|inst1|inst5~combout\,
	datab => \inst32|inst2|inst1|inst5~0_combout\,
	datac => \inst32|inst4|inst1|inst5~combout\,
	datad => \inst32|inst57|inst1|inst5~combout\,
	combout => \inst|seven_seg2[2]~56_combout\);

-- Location: LCCOMB_X12_Y22_N12
\inst|seven_seg2[2]~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~57_combout\ = (!\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg2[2]~46_combout\) # ((!\inst32|inst34|inst1|inst5~combout\ & !\inst|seven_seg2[2]~56_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg2[2]~46_combout\,
	datad => \inst|seven_seg2[2]~56_combout\,
	combout => \inst|seven_seg2[2]~57_combout\);

-- Location: LCCOMB_X12_Y22_N6
\inst|seven_seg2[2]~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[2]~61_combout\ = (!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[2]~57_combout\) # ((\inst|seven_seg2[2]~60_combout\ & \inst|seven_seg2[2]~41_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[2]~60_combout\,
	datab => \inst|seven_seg2[2]~41_combout\,
	datac => \inst32|inst667|inst1|inst5~combout\,
	datad => \inst|seven_seg2[2]~57_combout\,
	combout => \inst|seven_seg2[2]~61_combout\);

-- Location: LCCOMB_X15_Y21_N14
\inst|seven_seg2[1]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~64_combout\ = (\inst32|inst5|inst1|inst5~combout\ & ((\inst32|inst4|inst1|inst5~combout\) # ((\inst32|inst3|inst1|inst5~combout\) # (\inst32|inst2|inst1|inst5~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst3|inst1|inst5~combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst32|inst2|inst1|inst5~0_combout\,
	combout => \inst|seven_seg2[1]~64_combout\);

-- Location: LCCOMB_X15_Y21_N30
\inst|seven_seg2[1]~74\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~74_combout\ = (!\inst32|inst34|inst1|inst5~combout\ & ((\inst25|inst4|inst1|inst5~combout\ $ (!\inst32|inst4|inst2~0_combout\)) # (!\inst|seven_seg2[1]~64_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst25|inst4|inst1|inst5~combout\,
	datac => \inst|seven_seg2[1]~64_combout\,
	datad => \inst32|inst4|inst2~0_combout\,
	combout => \inst|seven_seg2[1]~74_combout\);

-- Location: LCCOMB_X11_Y21_N6
\inst|Equal0~25\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~25_combout\ = (\inst32|inst34|inst1|inst5~combout\ & (\inst|Equal0~17_combout\ & (\inst25|inst34|inst1|inst5~combout\ $ (!\inst32|inst34|inst1|inst~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst25|inst34|inst1|inst5~combout\,
	datac => \inst|Equal0~17_combout\,
	datad => \inst32|inst34|inst1|inst~combout\,
	combout => \inst|Equal0~25_combout\);

-- Location: LCCOMB_X15_Y21_N16
\inst|Equal0~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~26_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\ & (\inst|Equal0~23_combout\ & \inst|Equal0~25_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|Equal0~23_combout\,
	datad => \inst|Equal0~25_combout\,
	combout => \inst|Equal0~26_combout\);

-- Location: LCCOMB_X15_Y21_N2
\inst|seven_seg2[1]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~62_combout\ = ((\inst32|inst4|inst2~0_combout\ $ (!\inst25|inst4|inst1|inst5~combout\)) # (!\inst|seven_seg2[2]~58_combout\)) # (!\inst32|inst4|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg2[2]~58_combout\,
	combout => \inst|seven_seg2[1]~62_combout\);

-- Location: LCCOMB_X15_Y21_N20
\inst|seven_seg2[1]~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~63_combout\ = ((!\inst32|inst5|inst1|inst5~combout\ & ((\inst|Equal0~26_combout\) # (\inst|seven_seg2[1]~62_combout\)))) # (!\inst32|inst34|inst1|inst5~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101011101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst34|inst1|inst5~combout\,
	datab => \inst|Equal0~26_combout\,
	datac => \inst32|inst5|inst1|inst5~combout\,
	datad => \inst|seven_seg2[1]~62_combout\,
	combout => \inst|seven_seg2[1]~63_combout\);

-- Location: LCCOMB_X15_Y21_N0
\inst|seven_seg2[1]~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~65_combout\ = (\inst32|inst488|inst1|inst5~combout\ & (\inst32|inst667|inst1|inst5~combout\)) # (!\inst32|inst488|inst1|inst5~combout\ & ((\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[1]~63_combout\))) # 
-- (!\inst32|inst667|inst1|inst5~combout\ & (\inst|seven_seg2[1]~74_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg2[1]~74_combout\,
	datad => \inst|seven_seg2[1]~63_combout\,
	combout => \inst|seven_seg2[1]~65_combout\);

-- Location: LCCOMB_X15_Y21_N12
\inst|seven_seg2[1]~73\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~73_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (\inst|seven_seg2[5]~34_combout\ & (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst25|inst4|inst1|inst5~combout\,
	datad => \inst|seven_seg2[5]~34_combout\,
	combout => \inst|seven_seg2[1]~73_combout\);

-- Location: LCCOMB_X15_Y21_N10
\inst|seven_seg2[1]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[1]~66_combout\ = (\inst32|inst488|inst1|inst5~combout\ & ((\inst|seven_seg2[1]~65_combout\ & ((\inst|Equal0~26_combout\))) # (!\inst|seven_seg2[1]~65_combout\ & (\inst|seven_seg2[1]~73_combout\)))) # (!\inst32|inst488|inst1|inst5~combout\ 
-- & (\inst|seven_seg2[1]~65_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst488|inst1|inst5~combout\,
	datab => \inst|seven_seg2[1]~65_combout\,
	datac => \inst|seven_seg2[1]~73_combout\,
	datad => \inst|Equal0~26_combout\,
	combout => \inst|seven_seg2[1]~66_combout\);

-- Location: LCCOMB_X8_Y23_N10
\inst|seven_seg2[0]~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[0]~68_combout\ = (\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst4|inst2~0_combout\ $ (((\inst25|inst4|inst1|inst5~combout\))))) # (!\inst32|inst2|inst1|inst5~0_combout\ & (\inst32|inst3|inst1|inst5~combout\ & 
-- (\inst32|inst4|inst2~0_combout\ $ (\inst25|inst4|inst1|inst5~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst2|inst1|inst5~0_combout\,
	datab => \inst32|inst4|inst2~0_combout\,
	datac => \inst32|inst3|inst1|inst5~combout\,
	datad => \inst25|inst4|inst1|inst5~combout\,
	combout => \inst|seven_seg2[0]~68_combout\);

-- Location: LCCOMB_X15_Y23_N10
\inst|seven_seg2[0]~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[0]~69_combout\ = (\inst32|inst4|inst1|inst5~combout\ & (((!\inst32|inst5|inst1|inst5~combout\) # (!\inst|seven_seg2[0]~68_combout\)))) # (!\inst32|inst4|inst1|inst5~combout\ & (!\inst|Equal0~20_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101110111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst4|inst1|inst5~combout\,
	datab => \inst|Equal0~20_combout\,
	datac => \inst|seven_seg2[0]~68_combout\,
	datad => \inst32|inst5|inst1|inst5~combout\,
	combout => \inst|seven_seg2[0]~69_combout\);

-- Location: LCCOMB_X15_Y23_N8
\inst|seven_seg2[0]~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[0]~67_combout\ = (!\inst32|inst488|inst1|inst5~combout\ & ((!\inst|seven_seg2[6]~31_combout\) # (!\inst32|inst5|inst1|inst5~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001100010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst5|inst1|inst5~combout\,
	datab => \inst32|inst488|inst1|inst5~combout\,
	datac => \inst|seven_seg2[6]~31_combout\,
	combout => \inst|seven_seg2[0]~67_combout\);

-- Location: LCCOMB_X15_Y23_N4
\inst|seven_seg2[0]~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[0]~70_combout\ = (!\inst32|inst667|inst1|inst5~combout\ & ((\inst|seven_seg2[0]~67_combout\) # ((\inst|seven_seg2[0]~69_combout\ & \inst|seven_seg2[4]~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg2[0]~69_combout\,
	datab => \inst32|inst667|inst1|inst5~combout\,
	datac => \inst|seven_seg2[0]~67_combout\,
	datad => \inst|seven_seg2[4]~24_combout\,
	combout => \inst|seven_seg2[0]~70_combout\);

-- Location: LCCOMB_X15_Y23_N28
\inst|seven_seg2[4]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg2[4]~48_combout\ = (\inst|seven_seg2[4]~24_combout\ & (\inst32|inst667|inst1|inst5~0_combout\ $ (\inst15|inst57|inst1|inst~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|inst667|inst1|inst5~0_combout\,
	datab => \inst15|inst57|inst1|inst~combout\,
	datac => \inst|seven_seg2[4]~24_combout\,
	combout => \inst|seven_seg2[4]~48_combout\);

-- Location: LCCOMB_X15_Y23_N6
\inst|seven_seg3[2]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|seven_seg3[2]~0_combout\ = (\inst|seven_seg2[4]~75_combout\) # (((\inst|seven_seg1[2]~258_combout\ & \inst|Equal0~20_combout\)) # (!\inst|seven_seg2[4]~48_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|seven_seg1[2]~258_combout\,
	datab => \inst|Equal0~20_combout\,
	datac => \inst|seven_seg2[4]~75_combout\,
	datad => \inst|seven_seg2[4]~48_combout\,
	combout => \inst|seven_seg3[2]~0_combout\);

ww_D0(6) <= \D0[6]~output_o\;

ww_D0(5) <= \D0[5]~output_o\;

ww_D0(4) <= \D0[4]~output_o\;

ww_D0(3) <= \D0[3]~output_o\;

ww_D0(2) <= \D0[2]~output_o\;

ww_D0(1) <= \D0[1]~output_o\;

ww_D0(0) <= \D0[0]~output_o\;

ww_D1(6) <= \D1[6]~output_o\;

ww_D1(5) <= \D1[5]~output_o\;

ww_D1(4) <= \D1[4]~output_o\;

ww_D1(3) <= \D1[3]~output_o\;

ww_D1(2) <= \D1[2]~output_o\;

ww_D1(1) <= \D1[1]~output_o\;

ww_D1(0) <= \D1[0]~output_o\;

ww_D2(6) <= \D2[6]~output_o\;

ww_D2(5) <= \D2[5]~output_o\;

ww_D2(4) <= \D2[4]~output_o\;

ww_D2(3) <= \D2[3]~output_o\;

ww_D2(2) <= \D2[2]~output_o\;

ww_D2(1) <= \D2[1]~output_o\;

ww_D2(0) <= \D2[0]~output_o\;

ww_D3(6) <= \D3[6]~output_o\;

ww_D3(5) <= \D3[5]~output_o\;

ww_D3(4) <= \D3[4]~output_o\;

ww_D3(3) <= \D3[3]~output_o\;

ww_D3(2) <= \D3[2]~output_o\;

ww_D3(1) <= \D3[1]~output_o\;

ww_D3(0) <= \D3[0]~output_o\;

ww_OUTPUT(9) <= \OUTPUT[9]~output_o\;

ww_OUTPUT(8) <= \OUTPUT[8]~output_o\;

ww_OUTPUT(7) <= \OUTPUT[7]~output_o\;

ww_OUTPUT(6) <= \OUTPUT[6]~output_o\;

ww_OUTPUT(5) <= \OUTPUT[5]~output_o\;

ww_OUTPUT(4) <= \OUTPUT[4]~output_o\;

ww_OUTPUT(3) <= \OUTPUT[3]~output_o\;

ww_OUTPUT(2) <= \OUTPUT[2]~output_o\;

ww_OUTPUT(1) <= \OUTPUT[1]~output_o\;

ww_OUTPUT(0) <= \OUTPUT[0]~output_o\;
END structure;


