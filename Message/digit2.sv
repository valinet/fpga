module digit2(input clock, output reg [6:0] number);

reg[6:0] mem [0:24] = '{7'b1000111,7'b0001000,7'b1111111,7'b1001100,7'b1011000,7'b1000001,7'b1000111,7'b1111000,7'b1111110,7'b1001111,7'b1111111,7'b0001000,7'b1001000,7'b1110001,7'b1001111,7'b1111111,7'b1000110,7'b0001010,7'b0111100,7'b1001111,7'b0010010,7'b1111000,7'b1111110,7'b1001111,7'b1111111};

reg [5:0] counter;

initial counter = 1;

always @(posedge(clock)) begin
	number = mem[counter];
	if (counter == 5'd25) begin
		counter <= 0;
	end else begin
		counter <= counter + 1;
	end
end
endmodule