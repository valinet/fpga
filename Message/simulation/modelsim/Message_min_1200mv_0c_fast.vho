-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "04/12/2016 15:34:36"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	Message IS
    PORT (
	DIGIT0 : OUT std_logic_vector(0 TO 6);
	CLOCK : IN std_logic;
	DIGIT1 : OUT std_logic_vector(0 TO 6);
	DIGIT2 : OUT std_logic_vector(0 TO 6);
	DIGIT3 : OUT std_logic_vector(0 TO 6)
	);
END Message;

-- Design Ports Information
-- DIGIT0[0]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[1]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[2]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[4]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[5]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT0[6]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[0]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[1]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[2]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[4]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[5]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT1[6]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[0]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[1]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[2]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[4]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[5]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT2[6]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[0]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[1]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[2]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[4]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[5]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- DIGIT3[6]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF Message IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_DIGIT0 : std_logic_vector(0 TO 6);
SIGNAL ww_CLOCK : std_logic;
SIGNAL ww_DIGIT1 : std_logic_vector(0 TO 6);
SIGNAL ww_DIGIT2 : std_logic_vector(0 TO 6);
SIGNAL ww_DIGIT3 : std_logic_vector(0 TO 6);
SIGNAL \inst|reduced~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \DIGIT0[0]~output_o\ : std_logic;
SIGNAL \DIGIT0[1]~output_o\ : std_logic;
SIGNAL \DIGIT0[2]~output_o\ : std_logic;
SIGNAL \DIGIT0[3]~output_o\ : std_logic;
SIGNAL \DIGIT0[4]~output_o\ : std_logic;
SIGNAL \DIGIT0[5]~output_o\ : std_logic;
SIGNAL \DIGIT0[6]~output_o\ : std_logic;
SIGNAL \DIGIT1[0]~output_o\ : std_logic;
SIGNAL \DIGIT1[1]~output_o\ : std_logic;
SIGNAL \DIGIT1[2]~output_o\ : std_logic;
SIGNAL \DIGIT1[3]~output_o\ : std_logic;
SIGNAL \DIGIT1[4]~output_o\ : std_logic;
SIGNAL \DIGIT1[5]~output_o\ : std_logic;
SIGNAL \DIGIT1[6]~output_o\ : std_logic;
SIGNAL \DIGIT2[0]~output_o\ : std_logic;
SIGNAL \DIGIT2[1]~output_o\ : std_logic;
SIGNAL \DIGIT2[2]~output_o\ : std_logic;
SIGNAL \DIGIT2[3]~output_o\ : std_logic;
SIGNAL \DIGIT2[4]~output_o\ : std_logic;
SIGNAL \DIGIT2[5]~output_o\ : std_logic;
SIGNAL \DIGIT2[6]~output_o\ : std_logic;
SIGNAL \DIGIT3[0]~output_o\ : std_logic;
SIGNAL \DIGIT3[1]~output_o\ : std_logic;
SIGNAL \DIGIT3[2]~output_o\ : std_logic;
SIGNAL \DIGIT3[3]~output_o\ : std_logic;
SIGNAL \DIGIT3[4]~output_o\ : std_logic;
SIGNAL \DIGIT3[5]~output_o\ : std_logic;
SIGNAL \DIGIT3[6]~output_o\ : std_logic;
SIGNAL \CLOCK~input_o\ : std_logic;
SIGNAL \CLOCK~inputclkctrl_outclk\ : std_logic;
SIGNAL \inst|Add0~0_combout\ : std_logic;
SIGNAL \inst|counter~1_combout\ : std_logic;
SIGNAL \inst|Add0~1\ : std_logic;
SIGNAL \inst|Add0~2_combout\ : std_logic;
SIGNAL \inst|Add0~3\ : std_logic;
SIGNAL \inst|Add0~4_combout\ : std_logic;
SIGNAL \inst|Add0~5\ : std_logic;
SIGNAL \inst|Add0~6_combout\ : std_logic;
SIGNAL \inst|Add0~7\ : std_logic;
SIGNAL \inst|Add0~8_combout\ : std_logic;
SIGNAL \inst|Add0~9\ : std_logic;
SIGNAL \inst|Add0~10_combout\ : std_logic;
SIGNAL \inst|Add0~11\ : std_logic;
SIGNAL \inst|Add0~12_combout\ : std_logic;
SIGNAL \inst|counter~0_combout\ : std_logic;
SIGNAL \inst|Add0~13\ : std_logic;
SIGNAL \inst|Add0~14_combout\ : std_logic;
SIGNAL \inst|Add0~15\ : std_logic;
SIGNAL \inst|Add0~16_combout\ : std_logic;
SIGNAL \inst|Add0~17\ : std_logic;
SIGNAL \inst|Add0~18_combout\ : std_logic;
SIGNAL \inst|Add0~19\ : std_logic;
SIGNAL \inst|Add0~20_combout\ : std_logic;
SIGNAL \inst|Add0~21\ : std_logic;
SIGNAL \inst|Add0~22_combout\ : std_logic;
SIGNAL \inst|counter~2_combout\ : std_logic;
SIGNAL \inst|Add0~23\ : std_logic;
SIGNAL \inst|Add0~24_combout\ : std_logic;
SIGNAL \inst|counter~3_combout\ : std_logic;
SIGNAL \inst|Add0~25\ : std_logic;
SIGNAL \inst|Add0~26_combout\ : std_logic;
SIGNAL \inst|counter~4_combout\ : std_logic;
SIGNAL \inst|Add0~27\ : std_logic;
SIGNAL \inst|Add0~28_combout\ : std_logic;
SIGNAL \inst|counter~5_combout\ : std_logic;
SIGNAL \inst|Add0~29\ : std_logic;
SIGNAL \inst|Add0~30_combout\ : std_logic;
SIGNAL \inst|Add0~31\ : std_logic;
SIGNAL \inst|Add0~32_combout\ : std_logic;
SIGNAL \inst|counter~6_combout\ : std_logic;
SIGNAL \inst|Add0~33\ : std_logic;
SIGNAL \inst|Add0~34_combout\ : std_logic;
SIGNAL \inst|Add0~35\ : std_logic;
SIGNAL \inst|Add0~36_combout\ : std_logic;
SIGNAL \inst|counter~7_combout\ : std_logic;
SIGNAL \inst|Add0~37\ : std_logic;
SIGNAL \inst|Add0~38_combout\ : std_logic;
SIGNAL \inst|counter~8_combout\ : std_logic;
SIGNAL \inst|Add0~39\ : std_logic;
SIGNAL \inst|Add0~40_combout\ : std_logic;
SIGNAL \inst|counter~10_combout\ : std_logic;
SIGNAL \inst|Add0~41\ : std_logic;
SIGNAL \inst|Add0~42_combout\ : std_logic;
SIGNAL \inst|counter~11_combout\ : std_logic;
SIGNAL \inst|Add0~43\ : std_logic;
SIGNAL \inst|Add0~44_combout\ : std_logic;
SIGNAL \inst|counter~12_combout\ : std_logic;
SIGNAL \inst|Add0~45\ : std_logic;
SIGNAL \inst|Add0~46_combout\ : std_logic;
SIGNAL \inst|Add0~47\ : std_logic;
SIGNAL \inst|Add0~48_combout\ : std_logic;
SIGNAL \inst|counter~9_combout\ : std_logic;
SIGNAL \inst|Equal0~5_combout\ : std_logic;
SIGNAL \inst|Equal0~6_combout\ : std_logic;
SIGNAL \inst|Equal0~1_combout\ : std_logic;
SIGNAL \inst|Equal0~3_combout\ : std_logic;
SIGNAL \inst|Equal0~2_combout\ : std_logic;
SIGNAL \inst|Equal0~0_combout\ : std_logic;
SIGNAL \inst|Equal0~4_combout\ : std_logic;
SIGNAL \inst|Equal0~7_combout\ : std_logic;
SIGNAL \inst|reduced~0_combout\ : std_logic;
SIGNAL \inst|reduced~feeder_combout\ : std_logic;
SIGNAL \inst|reduced~q\ : std_logic;
SIGNAL \inst|reduced~clkctrl_outclk\ : std_logic;
SIGNAL \inst5|Add0~0_combout\ : std_logic;
SIGNAL \inst5|counter[0]~3_combout\ : std_logic;
SIGNAL \inst5|Add0~1\ : std_logic;
SIGNAL \inst5|Add0~2_combout\ : std_logic;
SIGNAL \inst5|counter~0_combout\ : std_logic;
SIGNAL \inst5|Add0~3\ : std_logic;
SIGNAL \inst5|Add0~4_combout\ : std_logic;
SIGNAL \inst5|Equal0~0_combout\ : std_logic;
SIGNAL \inst5|Add0~5\ : std_logic;
SIGNAL \inst5|Add0~7\ : std_logic;
SIGNAL \inst5|Add0~8_combout\ : std_logic;
SIGNAL \inst5|counter~2_combout\ : std_logic;
SIGNAL \inst5|Add0~9\ : std_logic;
SIGNAL \inst5|Add0~10_combout\ : std_logic;
SIGNAL \inst5|Add0~6_combout\ : std_logic;
SIGNAL \inst5|counter~1_combout\ : std_logic;
SIGNAL \inst5|mem~16_combout\ : std_logic;
SIGNAL \inst5|mem~17_combout\ : std_logic;
SIGNAL \inst5|mem~1_combout\ : std_logic;
SIGNAL \inst5|mem~0_combout\ : std_logic;
SIGNAL \inst5|mem~2_combout\ : std_logic;
SIGNAL \inst5|mem~4_combout\ : std_logic;
SIGNAL \inst5|mem~3_combout\ : std_logic;
SIGNAL \inst5|mem~5_combout\ : std_logic;
SIGNAL \inst5|mem~7_combout\ : std_logic;
SIGNAL \inst5|mem~6_combout\ : std_logic;
SIGNAL \inst5|mem~8_combout\ : std_logic;
SIGNAL \inst5|mem~14_combout\ : std_logic;
SIGNAL \inst5|mem~15_combout\ : std_logic;
SIGNAL \inst5|mem~10_combout\ : std_logic;
SIGNAL \inst5|mem~9_combout\ : std_logic;
SIGNAL \inst5|mem~11_combout\ : std_logic;
SIGNAL \inst5|mem~12_combout\ : std_logic;
SIGNAL \inst5|mem~13_combout\ : std_logic;
SIGNAL \inst6|Add0~0_combout\ : std_logic;
SIGNAL \inst6|Add0~5\ : std_logic;
SIGNAL \inst6|Add0~7\ : std_logic;
SIGNAL \inst6|Add0~9\ : std_logic;
SIGNAL \inst6|Add0~10_combout\ : std_logic;
SIGNAL \inst6|Add0~1\ : std_logic;
SIGNAL \inst6|Add0~2_combout\ : std_logic;
SIGNAL \inst6|counter~0_combout\ : std_logic;
SIGNAL \inst6|Add0~3\ : std_logic;
SIGNAL \inst6|Add0~4_combout\ : std_logic;
SIGNAL \inst6|Equal0~0_combout\ : std_logic;
SIGNAL \inst6|Add0~8_combout\ : std_logic;
SIGNAL \inst6|counter~2_combout\ : std_logic;
SIGNAL \inst6|Add0~6_combout\ : std_logic;
SIGNAL \inst6|counter~1_combout\ : std_logic;
SIGNAL \inst6|mem~16_combout\ : std_logic;
SIGNAL \inst6|mem~17_combout\ : std_logic;
SIGNAL \inst6|mem~0_combout\ : std_logic;
SIGNAL \inst6|mem~1_combout\ : std_logic;
SIGNAL \inst6|mem~2_combout\ : std_logic;
SIGNAL \inst6|mem~3_combout\ : std_logic;
SIGNAL \inst6|mem~4_combout\ : std_logic;
SIGNAL \inst6|mem~5_combout\ : std_logic;
SIGNAL \inst6|mem~6_combout\ : std_logic;
SIGNAL \inst6|mem~7_combout\ : std_logic;
SIGNAL \inst6|mem~8_combout\ : std_logic;
SIGNAL \inst6|mem~14_combout\ : std_logic;
SIGNAL \inst6|mem~15_combout\ : std_logic;
SIGNAL \inst6|mem~10_combout\ : std_logic;
SIGNAL \inst6|mem~9_combout\ : std_logic;
SIGNAL \inst6|mem~11_combout\ : std_logic;
SIGNAL \inst6|mem~12_combout\ : std_logic;
SIGNAL \inst6|mem~13_combout\ : std_logic;
SIGNAL \inst7|Add0~0_combout\ : std_logic;
SIGNAL \inst7|counter~0_combout\ : std_logic;
SIGNAL \inst7|Add0~1\ : std_logic;
SIGNAL \inst7|Add0~2_combout\ : std_logic;
SIGNAL \inst7|Equal0~0_combout\ : std_logic;
SIGNAL \inst7|Add0~3\ : std_logic;
SIGNAL \inst7|Add0~5\ : std_logic;
SIGNAL \inst7|Add0~6_combout\ : std_logic;
SIGNAL \inst7|counter~2_combout\ : std_logic;
SIGNAL \inst7|Add0~7\ : std_logic;
SIGNAL \inst7|Add0~8_combout\ : std_logic;
SIGNAL \inst7|Add0~4_combout\ : std_logic;
SIGNAL \inst7|counter~1_combout\ : std_logic;
SIGNAL \inst7|mem~16_combout\ : std_logic;
SIGNAL \inst7|mem~17_combout\ : std_logic;
SIGNAL \inst7|mem~0_combout\ : std_logic;
SIGNAL \inst7|mem~1_combout\ : std_logic;
SIGNAL \inst7|mem~2_combout\ : std_logic;
SIGNAL \inst7|mem~3_combout\ : std_logic;
SIGNAL \inst7|mem~4_combout\ : std_logic;
SIGNAL \inst7|mem~5_combout\ : std_logic;
SIGNAL \inst7|mem~7_combout\ : std_logic;
SIGNAL \inst7|mem~6_combout\ : std_logic;
SIGNAL \inst7|mem~8_combout\ : std_logic;
SIGNAL \inst7|mem~14_combout\ : std_logic;
SIGNAL \inst7|mem~15_combout\ : std_logic;
SIGNAL \inst7|mem~9_combout\ : std_logic;
SIGNAL \inst7|mem~10_combout\ : std_logic;
SIGNAL \inst7|mem~11_combout\ : std_logic;
SIGNAL \inst7|mem~12_combout\ : std_logic;
SIGNAL \inst7|mem~13_combout\ : std_logic;
SIGNAL \inst8|Add0~1\ : std_logic;
SIGNAL \inst8|Add0~2_combout\ : std_logic;
SIGNAL \inst8|Add0~3\ : std_logic;
SIGNAL \inst8|Add0~5\ : std_logic;
SIGNAL \inst8|Add0~7\ : std_logic;
SIGNAL \inst8|Add0~8_combout\ : std_logic;
SIGNAL \inst8|Add0~6_combout\ : std_logic;
SIGNAL \inst8|counter~2_combout\ : std_logic;
SIGNAL \inst8|Add0~0_combout\ : std_logic;
SIGNAL \inst8|counter~0_combout\ : std_logic;
SIGNAL \inst8|Equal0~0_combout\ : std_logic;
SIGNAL \inst8|Add0~4_combout\ : std_logic;
SIGNAL \inst8|counter~1_combout\ : std_logic;
SIGNAL \inst8|mem~16_combout\ : std_logic;
SIGNAL \inst8|mem~17_combout\ : std_logic;
SIGNAL \inst8|mem~0_combout\ : std_logic;
SIGNAL \inst8|mem~1_combout\ : std_logic;
SIGNAL \inst8|mem~2_combout\ : std_logic;
SIGNAL \inst8|mem~3_combout\ : std_logic;
SIGNAL \inst8|mem~4_combout\ : std_logic;
SIGNAL \inst8|mem~5_combout\ : std_logic;
SIGNAL \inst8|mem~7_combout\ : std_logic;
SIGNAL \inst8|mem~6_combout\ : std_logic;
SIGNAL \inst8|mem~8_combout\ : std_logic;
SIGNAL \inst8|mem~14_combout\ : std_logic;
SIGNAL \inst8|mem~15_combout\ : std_logic;
SIGNAL \inst8|mem~10_combout\ : std_logic;
SIGNAL \inst8|mem~9_combout\ : std_logic;
SIGNAL \inst8|mem~11_combout\ : std_logic;
SIGNAL \inst8|mem~12_combout\ : std_logic;
SIGNAL \inst8|mem~13_combout\ : std_logic;
SIGNAL \inst5|number\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst5|counter\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst|counter\ : std_logic_vector(24 DOWNTO 0);
SIGNAL \inst6|number\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst6|counter\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst7|number\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst7|counter\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst8|number\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst8|counter\ : std_logic_vector(5 DOWNTO 0);

BEGIN

DIGIT0 <= ww_DIGIT0;
ww_CLOCK <= CLOCK;
DIGIT1 <= ww_DIGIT1;
DIGIT2 <= ww_DIGIT2;
DIGIT3 <= ww_DIGIT3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst|reduced~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst|reduced~q\);

\CLOCK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCK~input_o\);

-- Location: IOOBUF_X26_Y29_N16
\DIGIT0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(6),
	devoe => ww_devoe,
	o => \DIGIT0[0]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\DIGIT0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(5),
	devoe => ww_devoe,
	o => \DIGIT0[1]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\DIGIT0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(4),
	devoe => ww_devoe,
	o => \DIGIT0[2]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\DIGIT0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(3),
	devoe => ww_devoe,
	o => \DIGIT0[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\DIGIT0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(2),
	devoe => ww_devoe,
	o => \DIGIT0[4]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\DIGIT0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(1),
	devoe => ww_devoe,
	o => \DIGIT0[5]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\DIGIT0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5|number\(0),
	devoe => ww_devoe,
	o => \DIGIT0[6]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\DIGIT1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(6),
	devoe => ww_devoe,
	o => \DIGIT1[0]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\DIGIT1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(5),
	devoe => ww_devoe,
	o => \DIGIT1[1]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\DIGIT1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(4),
	devoe => ww_devoe,
	o => \DIGIT1[2]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\DIGIT1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(3),
	devoe => ww_devoe,
	o => \DIGIT1[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\DIGIT1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(2),
	devoe => ww_devoe,
	o => \DIGIT1[4]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\DIGIT1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(1),
	devoe => ww_devoe,
	o => \DIGIT1[5]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\DIGIT1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|number\(0),
	devoe => ww_devoe,
	o => \DIGIT1[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\DIGIT2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(6),
	devoe => ww_devoe,
	o => \DIGIT2[0]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\DIGIT2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(5),
	devoe => ww_devoe,
	o => \DIGIT2[1]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\DIGIT2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(4),
	devoe => ww_devoe,
	o => \DIGIT2[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\DIGIT2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(3),
	devoe => ww_devoe,
	o => \DIGIT2[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\DIGIT2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(2),
	devoe => ww_devoe,
	o => \DIGIT2[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\DIGIT2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(1),
	devoe => ww_devoe,
	o => \DIGIT2[5]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\DIGIT2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|number\(0),
	devoe => ww_devoe,
	o => \DIGIT2[6]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\DIGIT3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(6),
	devoe => ww_devoe,
	o => \DIGIT3[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\DIGIT3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(5),
	devoe => ww_devoe,
	o => \DIGIT3[1]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\DIGIT3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(4),
	devoe => ww_devoe,
	o => \DIGIT3[2]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\DIGIT3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(3),
	devoe => ww_devoe,
	o => \DIGIT3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\DIGIT3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(2),
	devoe => ww_devoe,
	o => \DIGIT3[4]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\DIGIT3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(1),
	devoe => ww_devoe,
	o => \DIGIT3[5]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\DIGIT3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|number\(0),
	devoe => ww_devoe,
	o => \DIGIT3[6]~output_o\);

-- Location: IOIBUF_X41_Y15_N1
\CLOCK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK,
	o => \CLOCK~input_o\);

-- Location: CLKCTRL_G9
\CLOCK~inputclkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK~inputclkctrl_outclk\);

-- Location: LCCOMB_X32_Y17_N8
\inst|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~0_combout\ = \inst|counter\(0) $ (VCC)
-- \inst|Add0~1\ = CARRY(\inst|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(0),
	datad => VCC,
	combout => \inst|Add0~0_combout\,
	cout => \inst|Add0~1\);

-- Location: LCCOMB_X32_Y17_N2
\inst|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~1_combout\ = (\inst|Add0~0_combout\ & !\inst|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Add0~0_combout\,
	datad => \inst|Equal0~7_combout\,
	combout => \inst|counter~1_combout\);

-- Location: FF_X32_Y17_N3
\inst|counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(0));

-- Location: LCCOMB_X32_Y17_N10
\inst|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~2_combout\ = (\inst|counter\(1) & (!\inst|Add0~1\)) # (!\inst|counter\(1) & ((\inst|Add0~1\) # (GND)))
-- \inst|Add0~3\ = CARRY((!\inst|Add0~1\) # (!\inst|counter\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(1),
	datad => VCC,
	cin => \inst|Add0~1\,
	combout => \inst|Add0~2_combout\,
	cout => \inst|Add0~3\);

-- Location: FF_X32_Y17_N11
\inst|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(1));

-- Location: LCCOMB_X32_Y17_N12
\inst|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~4_combout\ = (\inst|counter\(2) & (\inst|Add0~3\ $ (GND))) # (!\inst|counter\(2) & (!\inst|Add0~3\ & VCC))
-- \inst|Add0~5\ = CARRY((\inst|counter\(2) & !\inst|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(2),
	datad => VCC,
	cin => \inst|Add0~3\,
	combout => \inst|Add0~4_combout\,
	cout => \inst|Add0~5\);

-- Location: FF_X32_Y17_N13
\inst|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(2));

-- Location: LCCOMB_X32_Y17_N14
\inst|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~6_combout\ = (\inst|counter\(3) & (!\inst|Add0~5\)) # (!\inst|counter\(3) & ((\inst|Add0~5\) # (GND)))
-- \inst|Add0~7\ = CARRY((!\inst|Add0~5\) # (!\inst|counter\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(3),
	datad => VCC,
	cin => \inst|Add0~5\,
	combout => \inst|Add0~6_combout\,
	cout => \inst|Add0~7\);

-- Location: FF_X32_Y17_N15
\inst|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(3));

-- Location: LCCOMB_X32_Y17_N16
\inst|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~8_combout\ = (\inst|counter\(4) & (\inst|Add0~7\ $ (GND))) # (!\inst|counter\(4) & (!\inst|Add0~7\ & VCC))
-- \inst|Add0~9\ = CARRY((\inst|counter\(4) & !\inst|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(4),
	datad => VCC,
	cin => \inst|Add0~7\,
	combout => \inst|Add0~8_combout\,
	cout => \inst|Add0~9\);

-- Location: FF_X32_Y17_N17
\inst|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(4));

-- Location: LCCOMB_X32_Y17_N18
\inst|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~10_combout\ = (\inst|counter\(5) & (!\inst|Add0~9\)) # (!\inst|counter\(5) & ((\inst|Add0~9\) # (GND)))
-- \inst|Add0~11\ = CARRY((!\inst|Add0~9\) # (!\inst|counter\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(5),
	datad => VCC,
	cin => \inst|Add0~9\,
	combout => \inst|Add0~10_combout\,
	cout => \inst|Add0~11\);

-- Location: FF_X32_Y17_N19
\inst|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(5));

-- Location: LCCOMB_X32_Y17_N20
\inst|Add0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~12_combout\ = (\inst|counter\(6) & (\inst|Add0~11\ $ (GND))) # (!\inst|counter\(6) & (!\inst|Add0~11\ & VCC))
-- \inst|Add0~13\ = CARRY((\inst|counter\(6) & !\inst|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(6),
	datad => VCC,
	cin => \inst|Add0~11\,
	combout => \inst|Add0~12_combout\,
	cout => \inst|Add0~13\);

-- Location: LCCOMB_X32_Y17_N0
\inst|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~0_combout\ = (\inst|Add0~12_combout\ & !\inst|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|Add0~12_combout\,
	datad => \inst|Equal0~7_combout\,
	combout => \inst|counter~0_combout\);

-- Location: FF_X32_Y17_N1
\inst|counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(6));

-- Location: LCCOMB_X32_Y17_N22
\inst|Add0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~14_combout\ = (\inst|counter\(7) & (!\inst|Add0~13\)) # (!\inst|counter\(7) & ((\inst|Add0~13\) # (GND)))
-- \inst|Add0~15\ = CARRY((!\inst|Add0~13\) # (!\inst|counter\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(7),
	datad => VCC,
	cin => \inst|Add0~13\,
	combout => \inst|Add0~14_combout\,
	cout => \inst|Add0~15\);

-- Location: FF_X32_Y17_N23
\inst|counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(7));

-- Location: LCCOMB_X32_Y17_N24
\inst|Add0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~16_combout\ = (\inst|counter\(8) & (\inst|Add0~15\ $ (GND))) # (!\inst|counter\(8) & (!\inst|Add0~15\ & VCC))
-- \inst|Add0~17\ = CARRY((\inst|counter\(8) & !\inst|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(8),
	datad => VCC,
	cin => \inst|Add0~15\,
	combout => \inst|Add0~16_combout\,
	cout => \inst|Add0~17\);

-- Location: FF_X32_Y17_N25
\inst|counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~16_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(8));

-- Location: LCCOMB_X32_Y17_N26
\inst|Add0~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~18_combout\ = (\inst|counter\(9) & (!\inst|Add0~17\)) # (!\inst|counter\(9) & ((\inst|Add0~17\) # (GND)))
-- \inst|Add0~19\ = CARRY((!\inst|Add0~17\) # (!\inst|counter\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(9),
	datad => VCC,
	cin => \inst|Add0~17\,
	combout => \inst|Add0~18_combout\,
	cout => \inst|Add0~19\);

-- Location: FF_X32_Y17_N27
\inst|counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(9));

-- Location: LCCOMB_X32_Y17_N28
\inst|Add0~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~20_combout\ = (\inst|counter\(10) & (\inst|Add0~19\ $ (GND))) # (!\inst|counter\(10) & (!\inst|Add0~19\ & VCC))
-- \inst|Add0~21\ = CARRY((\inst|counter\(10) & !\inst|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(10),
	datad => VCC,
	cin => \inst|Add0~19\,
	combout => \inst|Add0~20_combout\,
	cout => \inst|Add0~21\);

-- Location: FF_X32_Y17_N29
\inst|counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(10));

-- Location: LCCOMB_X32_Y17_N30
\inst|Add0~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~22_combout\ = (\inst|counter\(11) & (!\inst|Add0~21\)) # (!\inst|counter\(11) & ((\inst|Add0~21\) # (GND)))
-- \inst|Add0~23\ = CARRY((!\inst|Add0~21\) # (!\inst|counter\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(11),
	datad => VCC,
	cin => \inst|Add0~21\,
	combout => \inst|Add0~22_combout\,
	cout => \inst|Add0~23\);

-- Location: LCCOMB_X32_Y17_N4
\inst|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~2_combout\ = (\inst|Add0~22_combout\ & !\inst|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Add0~22_combout\,
	datad => \inst|Equal0~7_combout\,
	combout => \inst|counter~2_combout\);

-- Location: FF_X32_Y17_N5
\inst|counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(11));

-- Location: LCCOMB_X32_Y16_N0
\inst|Add0~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~24_combout\ = (\inst|counter\(12) & (\inst|Add0~23\ $ (GND))) # (!\inst|counter\(12) & (!\inst|Add0~23\ & VCC))
-- \inst|Add0~25\ = CARRY((\inst|counter\(12) & !\inst|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(12),
	datad => VCC,
	cin => \inst|Add0~23\,
	combout => \inst|Add0~24_combout\,
	cout => \inst|Add0~25\);

-- Location: LCCOMB_X33_Y16_N4
\inst|counter~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~3_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~24_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~24_combout\,
	combout => \inst|counter~3_combout\);

-- Location: FF_X33_Y16_N5
\inst|counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(12));

-- Location: LCCOMB_X32_Y16_N2
\inst|Add0~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~26_combout\ = (\inst|counter\(13) & (!\inst|Add0~25\)) # (!\inst|counter\(13) & ((\inst|Add0~25\) # (GND)))
-- \inst|Add0~27\ = CARRY((!\inst|Add0~25\) # (!\inst|counter\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(13),
	datad => VCC,
	cin => \inst|Add0~25\,
	combout => \inst|Add0~26_combout\,
	cout => \inst|Add0~27\);

-- Location: LCCOMB_X33_Y16_N20
\inst|counter~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~4_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~26_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~26_combout\,
	combout => \inst|counter~4_combout\);

-- Location: FF_X33_Y16_N21
\inst|counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(13));

-- Location: LCCOMB_X32_Y16_N4
\inst|Add0~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~28_combout\ = (\inst|counter\(14) & (\inst|Add0~27\ $ (GND))) # (!\inst|counter\(14) & (!\inst|Add0~27\ & VCC))
-- \inst|Add0~29\ = CARRY((\inst|counter\(14) & !\inst|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(14),
	datad => VCC,
	cin => \inst|Add0~27\,
	combout => \inst|Add0~28_combout\,
	cout => \inst|Add0~29\);

-- Location: LCCOMB_X33_Y16_N6
\inst|counter~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~5_combout\ = (\inst|Add0~28_combout\ & !\inst|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Add0~28_combout\,
	datad => \inst|Equal0~7_combout\,
	combout => \inst|counter~5_combout\);

-- Location: FF_X33_Y16_N7
\inst|counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(14));

-- Location: LCCOMB_X32_Y16_N6
\inst|Add0~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~30_combout\ = (\inst|counter\(15) & (!\inst|Add0~29\)) # (!\inst|counter\(15) & ((\inst|Add0~29\) # (GND)))
-- \inst|Add0~31\ = CARRY((!\inst|Add0~29\) # (!\inst|counter\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(15),
	datad => VCC,
	cin => \inst|Add0~29\,
	combout => \inst|Add0~30_combout\,
	cout => \inst|Add0~31\);

-- Location: FF_X32_Y16_N7
\inst|counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(15));

-- Location: LCCOMB_X32_Y16_N8
\inst|Add0~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~32_combout\ = (\inst|counter\(16) & (\inst|Add0~31\ $ (GND))) # (!\inst|counter\(16) & (!\inst|Add0~31\ & VCC))
-- \inst|Add0~33\ = CARRY((\inst|counter\(16) & !\inst|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(16),
	datad => VCC,
	cin => \inst|Add0~31\,
	combout => \inst|Add0~32_combout\,
	cout => \inst|Add0~33\);

-- Location: LCCOMB_X33_Y16_N14
\inst|counter~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~6_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~32_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~32_combout\,
	combout => \inst|counter~6_combout\);

-- Location: FF_X33_Y16_N15
\inst|counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(16));

-- Location: LCCOMB_X32_Y16_N10
\inst|Add0~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~34_combout\ = (\inst|counter\(17) & (!\inst|Add0~33\)) # (!\inst|counter\(17) & ((\inst|Add0~33\) # (GND)))
-- \inst|Add0~35\ = CARRY((!\inst|Add0~33\) # (!\inst|counter\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(17),
	datad => VCC,
	cin => \inst|Add0~33\,
	combout => \inst|Add0~34_combout\,
	cout => \inst|Add0~35\);

-- Location: FF_X32_Y16_N11
\inst|counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~34_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(17));

-- Location: LCCOMB_X32_Y16_N12
\inst|Add0~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~36_combout\ = (\inst|counter\(18) & (\inst|Add0~35\ $ (GND))) # (!\inst|counter\(18) & (!\inst|Add0~35\ & VCC))
-- \inst|Add0~37\ = CARRY((\inst|counter\(18) & !\inst|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(18),
	datad => VCC,
	cin => \inst|Add0~35\,
	combout => \inst|Add0~36_combout\,
	cout => \inst|Add0~37\);

-- Location: LCCOMB_X33_Y16_N0
\inst|counter~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~7_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~36_combout\,
	combout => \inst|counter~7_combout\);

-- Location: FF_X33_Y16_N1
\inst|counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(18));

-- Location: LCCOMB_X32_Y16_N14
\inst|Add0~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~38_combout\ = (\inst|counter\(19) & (!\inst|Add0~37\)) # (!\inst|counter\(19) & ((\inst|Add0~37\) # (GND)))
-- \inst|Add0~39\ = CARRY((!\inst|Add0~37\) # (!\inst|counter\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(19),
	datad => VCC,
	cin => \inst|Add0~37\,
	combout => \inst|Add0~38_combout\,
	cout => \inst|Add0~39\);

-- Location: LCCOMB_X33_Y16_N2
\inst|counter~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~8_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~38_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~38_combout\,
	combout => \inst|counter~8_combout\);

-- Location: FF_X33_Y16_N3
\inst|counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(19));

-- Location: LCCOMB_X32_Y16_N16
\inst|Add0~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~40_combout\ = (\inst|counter\(20) & (\inst|Add0~39\ $ (GND))) # (!\inst|counter\(20) & (!\inst|Add0~39\ & VCC))
-- \inst|Add0~41\ = CARRY((\inst|counter\(20) & !\inst|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(20),
	datad => VCC,
	cin => \inst|Add0~39\,
	combout => \inst|Add0~40_combout\,
	cout => \inst|Add0~41\);

-- Location: LCCOMB_X32_Y16_N30
\inst|counter~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~10_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~40_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Equal0~7_combout\,
	datad => \inst|Add0~40_combout\,
	combout => \inst|counter~10_combout\);

-- Location: FF_X32_Y16_N31
\inst|counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(20));

-- Location: LCCOMB_X32_Y16_N18
\inst|Add0~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~42_combout\ = (\inst|counter\(21) & (!\inst|Add0~41\)) # (!\inst|counter\(21) & ((\inst|Add0~41\) # (GND)))
-- \inst|Add0~43\ = CARRY((!\inst|Add0~41\) # (!\inst|counter\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst|counter\(21),
	datad => VCC,
	cin => \inst|Add0~41\,
	combout => \inst|Add0~42_combout\,
	cout => \inst|Add0~43\);

-- Location: LCCOMB_X32_Y16_N28
\inst|counter~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~11_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~42_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Equal0~7_combout\,
	datad => \inst|Add0~42_combout\,
	combout => \inst|counter~11_combout\);

-- Location: FF_X32_Y16_N29
\inst|counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(21));

-- Location: LCCOMB_X32_Y16_N20
\inst|Add0~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~44_combout\ = (\inst|counter\(22) & (\inst|Add0~43\ $ (GND))) # (!\inst|counter\(22) & (!\inst|Add0~43\ & VCC))
-- \inst|Add0~45\ = CARRY((\inst|counter\(22) & !\inst|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(22),
	datad => VCC,
	cin => \inst|Add0~43\,
	combout => \inst|Add0~44_combout\,
	cout => \inst|Add0~45\);

-- Location: LCCOMB_X32_Y16_N26
\inst|counter~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~12_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~44_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst|Equal0~7_combout\,
	datad => \inst|Add0~44_combout\,
	combout => \inst|counter~12_combout\);

-- Location: FF_X32_Y16_N27
\inst|counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(22));

-- Location: LCCOMB_X32_Y16_N22
\inst|Add0~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~46_combout\ = (\inst|counter\(23) & (!\inst|Add0~45\)) # (!\inst|counter\(23) & ((\inst|Add0~45\) # (GND)))
-- \inst|Add0~47\ = CARRY((!\inst|Add0~45\) # (!\inst|counter\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(23),
	datad => VCC,
	cin => \inst|Add0~45\,
	combout => \inst|Add0~46_combout\,
	cout => \inst|Add0~47\);

-- Location: FF_X32_Y16_N23
\inst|counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|Add0~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(23));

-- Location: LCCOMB_X32_Y16_N24
\inst|Add0~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Add0~48_combout\ = \inst|Add0~47\ $ (!\inst|counter\(24))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst|counter\(24),
	cin => \inst|Add0~47\,
	combout => \inst|Add0~48_combout\);

-- Location: LCCOMB_X33_Y16_N26
\inst|counter~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|counter~9_combout\ = (!\inst|Equal0~7_combout\ & \inst|Add0~48_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~7_combout\,
	datad => \inst|Add0~48_combout\,
	combout => \inst|counter~9_combout\);

-- Location: FF_X33_Y16_N27
\inst|counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|counter~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|counter\(24));

-- Location: LCCOMB_X33_Y16_N18
\inst|Equal0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~5_combout\ = (!\inst|counter\(17) & (\inst|counter\(18) & (\inst|counter\(16) & \inst|counter\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(17),
	datab => \inst|counter\(18),
	datac => \inst|counter\(16),
	datad => \inst|counter\(19),
	combout => \inst|Equal0~5_combout\);

-- Location: LCCOMB_X33_Y16_N22
\inst|Equal0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~6_combout\ = (\inst|counter\(22) & (\inst|counter\(21) & (!\inst|counter\(23) & \inst|counter\(20))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(22),
	datab => \inst|counter\(21),
	datac => \inst|counter\(23),
	datad => \inst|counter\(20),
	combout => \inst|Equal0~6_combout\);

-- Location: LCCOMB_X33_Y17_N30
\inst|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~1_combout\ = (!\inst|counter\(3) & (!\inst|counter\(1) & (!\inst|counter\(2) & !\inst|counter\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(3),
	datab => \inst|counter\(1),
	datac => \inst|counter\(2),
	datad => \inst|counter\(0),
	combout => \inst|Equal0~1_combout\);

-- Location: LCCOMB_X33_Y16_N8
\inst|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~3_combout\ = (\inst|counter\(14) & (\inst|counter\(12) & (!\inst|counter\(15) & \inst|counter\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(14),
	datab => \inst|counter\(12),
	datac => \inst|counter\(15),
	datad => \inst|counter\(13),
	combout => \inst|Equal0~3_combout\);

-- Location: LCCOMB_X32_Y17_N6
\inst|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~2_combout\ = (!\inst|counter\(9) & (!\inst|counter\(8) & (\inst|counter\(11) & !\inst|counter\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(9),
	datab => \inst|counter\(8),
	datac => \inst|counter\(11),
	datad => \inst|counter\(10),
	combout => \inst|Equal0~2_combout\);

-- Location: LCCOMB_X33_Y17_N8
\inst|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~0_combout\ = (!\inst|counter\(4) & (!\inst|counter\(5) & (\inst|counter\(6) & !\inst|counter\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(4),
	datab => \inst|counter\(5),
	datac => \inst|counter\(6),
	datad => \inst|counter\(7),
	combout => \inst|Equal0~0_combout\);

-- Location: LCCOMB_X33_Y16_N24
\inst|Equal0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~4_combout\ = (\inst|Equal0~1_combout\ & (\inst|Equal0~3_combout\ & (\inst|Equal0~2_combout\ & \inst|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|Equal0~1_combout\,
	datab => \inst|Equal0~3_combout\,
	datac => \inst|Equal0~2_combout\,
	datad => \inst|Equal0~0_combout\,
	combout => \inst|Equal0~4_combout\);

-- Location: LCCOMB_X33_Y16_N12
\inst|Equal0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|Equal0~7_combout\ = (\inst|counter\(24) & (\inst|Equal0~5_combout\ & (\inst|Equal0~6_combout\ & \inst|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst|counter\(24),
	datab => \inst|Equal0~5_combout\,
	datac => \inst|Equal0~6_combout\,
	datad => \inst|Equal0~4_combout\,
	combout => \inst|Equal0~7_combout\);

-- Location: LCCOMB_X33_Y16_N16
\inst|reduced~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|reduced~0_combout\ = \inst|reduced~q\ $ (\inst|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|reduced~q\,
	datad => \inst|Equal0~7_combout\,
	combout => \inst|reduced~0_combout\);

-- Location: LCCOMB_X33_Y16_N30
\inst|reduced~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst|reduced~feeder_combout\ = \inst|reduced~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst|reduced~0_combout\,
	combout => \inst|reduced~feeder_combout\);

-- Location: FF_X33_Y16_N31
\inst|reduced\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst|reduced~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst|reduced~q\);

-- Location: CLKCTRL_G7
\inst|reduced~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst|reduced~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst|reduced~clkctrl_outclk\);

-- Location: LCCOMB_X27_Y28_N14
\inst5|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~0_combout\ = \inst5|counter\(0) $ (GND)
-- \inst5|Add0~1\ = CARRY(!\inst5|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(0),
	datad => VCC,
	combout => \inst5|Add0~0_combout\,
	cout => \inst5|Add0~1\);

-- Location: LCCOMB_X27_Y28_N30
\inst5|counter[0]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|counter[0]~3_combout\ = !\inst5|Add0~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst5|Add0~0_combout\,
	combout => \inst5|counter[0]~3_combout\);

-- Location: FF_X27_Y28_N31
\inst5|counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|counter[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(0));

-- Location: LCCOMB_X27_Y28_N16
\inst5|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~2_combout\ = (\inst5|counter\(1) & ((\inst5|Add0~1\) # (GND))) # (!\inst5|counter\(1) & (!\inst5|Add0~1\))
-- \inst5|Add0~3\ = CARRY((\inst5|counter\(1)) # (!\inst5|Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110101111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(1),
	datad => VCC,
	cin => \inst5|Add0~1\,
	combout => \inst5|Add0~2_combout\,
	cout => \inst5|Add0~3\);

-- Location: LCCOMB_X27_Y28_N12
\inst5|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|counter~0_combout\ = ((!\inst5|counter\(5) & (\inst5|counter\(4) & \inst5|Equal0~0_combout\))) # (!\inst5|Add0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(5),
	datab => \inst5|counter\(4),
	datac => \inst5|Equal0~0_combout\,
	datad => \inst5|Add0~2_combout\,
	combout => \inst5|counter~0_combout\);

-- Location: FF_X27_Y28_N13
\inst5|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(1));

-- Location: LCCOMB_X27_Y28_N18
\inst5|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~4_combout\ = (\inst5|counter\(2) & (\inst5|Add0~3\ $ (GND))) # (!\inst5|counter\(2) & (!\inst5|Add0~3\ & VCC))
-- \inst5|Add0~5\ = CARRY((\inst5|counter\(2) & !\inst5|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(2),
	datad => VCC,
	cin => \inst5|Add0~3\,
	combout => \inst5|Add0~4_combout\,
	cout => \inst5|Add0~5\);

-- Location: FF_X27_Y28_N19
\inst5|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(2));

-- Location: LCCOMB_X27_Y28_N2
\inst5|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Equal0~0_combout\ = (!\inst5|counter\(2) & (\inst5|counter\(1) & (\inst5|counter\(3) & !\inst5|counter\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(2),
	datab => \inst5|counter\(1),
	datac => \inst5|counter\(3),
	datad => \inst5|counter\(0),
	combout => \inst5|Equal0~0_combout\);

-- Location: LCCOMB_X27_Y28_N20
\inst5|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~6_combout\ = (\inst5|counter\(3) & (!\inst5|Add0~5\)) # (!\inst5|counter\(3) & ((\inst5|Add0~5\) # (GND)))
-- \inst5|Add0~7\ = CARRY((!\inst5|Add0~5\) # (!\inst5|counter\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datad => VCC,
	cin => \inst5|Add0~5\,
	combout => \inst5|Add0~6_combout\,
	cout => \inst5|Add0~7\);

-- Location: LCCOMB_X27_Y28_N22
\inst5|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~8_combout\ = (\inst5|counter\(4) & (\inst5|Add0~7\ $ (GND))) # (!\inst5|counter\(4) & (!\inst5|Add0~7\ & VCC))
-- \inst5|Add0~9\ = CARRY((\inst5|counter\(4) & !\inst5|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(4),
	datad => VCC,
	cin => \inst5|Add0~7\,
	combout => \inst5|Add0~8_combout\,
	cout => \inst5|Add0~9\);

-- Location: LCCOMB_X27_Y28_N28
\inst5|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|counter~2_combout\ = (\inst5|Add0~8_combout\ & ((\inst5|counter\(5)) # ((!\inst5|counter\(4)) # (!\inst5|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(5),
	datab => \inst5|Equal0~0_combout\,
	datac => \inst5|counter\(4),
	datad => \inst5|Add0~8_combout\,
	combout => \inst5|counter~2_combout\);

-- Location: FF_X27_Y28_N29
\inst5|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(4));

-- Location: LCCOMB_X27_Y28_N24
\inst5|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Add0~10_combout\ = \inst5|Add0~9\ $ (\inst5|counter\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst5|counter\(5),
	cin => \inst5|Add0~9\,
	combout => \inst5|Add0~10_combout\);

-- Location: FF_X27_Y28_N25
\inst5|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(5));

-- Location: LCCOMB_X27_Y28_N26
\inst5|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|counter~1_combout\ = (\inst5|Add0~6_combout\ & ((\inst5|counter\(5)) # ((!\inst5|Equal0~0_combout\) # (!\inst5|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(5),
	datab => \inst5|counter\(4),
	datac => \inst5|Equal0~0_combout\,
	datad => \inst5|Add0~6_combout\,
	combout => \inst5|counter~1_combout\);

-- Location: FF_X27_Y28_N27
\inst5|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|counter\(3));

-- Location: LCCOMB_X28_Y28_N24
\inst5|mem~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~16_combout\ = (\inst5|counter\(2) & (\inst5|counter\(0) & ((\inst5|counter\(3)) # (\inst5|counter\(1))))) # (!\inst5|counter\(2) & (\inst5|counter\(1) $ (((\inst5|counter\(3)) # (\inst5|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000110110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~16_combout\);

-- Location: LCCOMB_X28_Y28_N4
\inst5|mem~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~17_combout\ = (\inst5|counter\(4) & (!\inst5|mem~16_combout\ & ((\inst5|counter\(0)) # (!\inst5|counter\(3))))) # (!\inst5|counter\(4) & (((\inst5|counter\(0)) # (!\inst5|mem~16_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(4),
	datac => \inst5|counter\(0),
	datad => \inst5|mem~16_combout\,
	combout => \inst5|mem~17_combout\);

-- Location: FF_X28_Y28_N5
\inst5|number[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(6));

-- Location: LCCOMB_X28_Y28_N26
\inst5|mem~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~1_combout\ = (\inst5|counter\(3) & (\inst5|counter\(2) $ ((\inst5|counter\(0))))) # (!\inst5|counter\(3) & (!\inst5|counter\(1) & (\inst5|counter\(2) $ (\inst5|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~1_combout\);

-- Location: LCCOMB_X28_Y28_N12
\inst5|mem~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~0_combout\ = (\inst5|counter\(2) & (!\inst5|counter\(3) & (\inst5|counter\(0) $ (\inst5|counter\(1))))) # (!\inst5|counter\(2) & (\inst5|counter\(0) & (\inst5|counter\(3) $ (!\inst5|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~0_combout\);

-- Location: LCCOMB_X28_Y28_N30
\inst5|mem~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~2_combout\ = (\inst5|counter\(4) & ((\inst5|mem~0_combout\))) # (!\inst5|counter\(4) & (\inst5|mem~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(4),
	datac => \inst5|mem~1_combout\,
	datad => \inst5|mem~0_combout\,
	combout => \inst5|mem~2_combout\);

-- Location: FF_X28_Y28_N31
\inst5|number[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(5));

-- Location: LCCOMB_X28_Y28_N18
\inst5|mem~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~4_combout\ = (\inst5|counter\(2) & (\inst5|counter\(4) $ (((\inst5|counter\(3)) # (!\inst5|counter\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(4),
	datac => \inst5|counter\(2),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~4_combout\);

-- Location: LCCOMB_X28_Y28_N16
\inst5|mem~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~3_combout\ = (\inst5|counter\(1) & (\inst5|counter\(3) $ (((\inst5|counter\(2)))))) # (!\inst5|counter\(1) & ((\inst5|counter\(4) & (!\inst5|counter\(3))) # (!\inst5|counter\(4) & ((!\inst5|counter\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010010100111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(4),
	datac => \inst5|counter\(1),
	datad => \inst5|counter\(2),
	combout => \inst5|mem~3_combout\);

-- Location: LCCOMB_X28_Y28_N8
\inst5|mem~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~5_combout\ = (\inst5|counter\(0) & ((\inst5|mem~3_combout\))) # (!\inst5|counter\(0) & (\inst5|mem~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|mem~4_combout\,
	datac => \inst5|counter\(0),
	datad => \inst5|mem~3_combout\,
	combout => \inst5|mem~5_combout\);

-- Location: FF_X28_Y28_N9
\inst5|number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(4));

-- Location: LCCOMB_X28_Y28_N10
\inst5|mem~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~7_combout\ = (\inst5|counter\(0) & ((\inst5|counter\(3)) # (\inst5|counter\(2) $ (!\inst5|counter\(1))))) # (!\inst5|counter\(0) & (((!\inst5|counter\(1)) # (!\inst5|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001110111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~7_combout\);

-- Location: LCCOMB_X28_Y28_N28
\inst5|mem~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~6_combout\ = (\inst5|counter\(3) & (!\inst5|counter\(2) & (\inst5|counter\(0) & \inst5|counter\(1)))) # (!\inst5|counter\(3) & (((!\inst5|counter\(1)) # (!\inst5|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~6_combout\);

-- Location: LCCOMB_X28_Y28_N14
\inst5|mem~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~8_combout\ = (\inst5|counter\(4) & ((\inst5|mem~6_combout\))) # (!\inst5|counter\(4) & (\inst5|mem~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|mem~7_combout\,
	datab => \inst5|counter\(4),
	datad => \inst5|mem~6_combout\,
	combout => \inst5|mem~8_combout\);

-- Location: FF_X28_Y28_N15
\inst5|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(3));

-- Location: LCCOMB_X28_Y28_N2
\inst5|mem~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~14_combout\ = (\inst5|counter\(2) & (!\inst5|counter\(1) & (\inst5|counter\(0) $ (\inst5|counter\(4))))) # (!\inst5|counter\(2) & ((\inst5|counter\(1) $ (!\inst5|counter\(0))) # (!\inst5|counter\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010101110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(1),
	datab => \inst5|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(4),
	combout => \inst5|mem~14_combout\);

-- Location: LCCOMB_X28_Y28_N0
\inst5|mem~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~15_combout\ = (\inst5|mem~14_combout\ & ((\inst5|counter\(0)) # (\inst5|counter\(1) $ (!\inst5|counter\(3))))) # (!\inst5|mem~14_combout\ & (!\inst5|counter\(1) & (\inst5|counter\(0) $ (\inst5|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100111010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(1),
	datab => \inst5|mem~14_combout\,
	datac => \inst5|counter\(0),
	datad => \inst5|counter\(3),
	combout => \inst5|mem~15_combout\);

-- Location: FF_X28_Y28_N1
\inst5|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(2));

-- Location: LCCOMB_X27_Y28_N4
\inst5|mem~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~10_combout\ = (\inst5|counter\(2) & ((\inst5|counter\(4) & ((\inst5|counter\(3)))) # (!\inst5|counter\(4) & (\inst5|counter\(1))))) # (!\inst5|counter\(2) & (!\inst5|counter\(1) & ((\inst5|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(2),
	datab => \inst5|counter\(1),
	datac => \inst5|counter\(3),
	datad => \inst5|counter\(4),
	combout => \inst5|mem~10_combout\);

-- Location: LCCOMB_X27_Y28_N6
\inst5|mem~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~9_combout\ = (\inst5|counter\(3) & (!\inst5|counter\(4) & (\inst5|counter\(2) $ (\inst5|counter\(1))))) # (!\inst5|counter\(3) & (\inst5|counter\(4) & ((!\inst5|counter\(1)) # (!\inst5|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(2),
	datab => \inst5|counter\(1),
	datac => \inst5|counter\(3),
	datad => \inst5|counter\(4),
	combout => \inst5|mem~9_combout\);

-- Location: LCCOMB_X27_Y28_N0
\inst5|mem~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~11_combout\ = (\inst5|counter\(0) & (!\inst5|mem~10_combout\)) # (!\inst5|counter\(0) & ((\inst5|mem~9_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(0),
	datac => \inst5|mem~10_combout\,
	datad => \inst5|mem~9_combout\,
	combout => \inst5|mem~11_combout\);

-- Location: FF_X27_Y28_N1
\inst5|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(1));

-- Location: LCCOMB_X28_Y28_N20
\inst5|mem~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~12_combout\ = (\inst5|counter\(3) & ((\inst5|counter\(2) & (!\inst5|counter\(4))) # (!\inst5|counter\(2) & ((\inst5|counter\(1)))))) # (!\inst5|counter\(3) & ((\inst5|counter\(4) & ((!\inst5|counter\(1)))) # (!\inst5|counter\(4) & 
-- (\inst5|counter\(2) & \inst5|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(3),
	datab => \inst5|counter\(4),
	datac => \inst5|counter\(2),
	datad => \inst5|counter\(1),
	combout => \inst5|mem~12_combout\);

-- Location: LCCOMB_X28_Y28_N22
\inst5|mem~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|mem~13_combout\ = (\inst5|counter\(0) & (\inst5|counter\(4) $ (((!\inst5|mem~12_combout\) # (!\inst5|counter\(1)))))) # (!\inst5|counter\(0) & (\inst5|mem~12_combout\ & ((!\inst5|counter\(4)) # (!\inst5|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(1),
	datab => \inst5|counter\(4),
	datac => \inst5|counter\(0),
	datad => \inst5|mem~12_combout\,
	combout => \inst5|mem~13_combout\);

-- Location: FF_X28_Y28_N23
\inst5|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst5|mem~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst5|number\(0));

-- Location: LCCOMB_X29_Y28_N14
\inst6|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~0_combout\ = \inst6|counter\(0) $ (VCC)
-- \inst6|Add0~1\ = CARRY(\inst6|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(0),
	datad => VCC,
	combout => \inst6|Add0~0_combout\,
	cout => \inst6|Add0~1\);

-- Location: FF_X29_Y28_N15
\inst6|counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|Add0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(0));

-- Location: LCCOMB_X29_Y28_N18
\inst6|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~4_combout\ = (\inst6|counter\(2) & (\inst6|Add0~3\ $ (GND))) # (!\inst6|counter\(2) & (!\inst6|Add0~3\ & VCC))
-- \inst6|Add0~5\ = CARRY((\inst6|counter\(2) & !\inst6|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(2),
	datad => VCC,
	cin => \inst6|Add0~3\,
	combout => \inst6|Add0~4_combout\,
	cout => \inst6|Add0~5\);

-- Location: LCCOMB_X29_Y28_N20
\inst6|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~6_combout\ = (\inst6|counter\(3) & (!\inst6|Add0~5\)) # (!\inst6|counter\(3) & ((\inst6|Add0~5\) # (GND)))
-- \inst6|Add0~7\ = CARRY((!\inst6|Add0~5\) # (!\inst6|counter\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(3),
	datad => VCC,
	cin => \inst6|Add0~5\,
	combout => \inst6|Add0~6_combout\,
	cout => \inst6|Add0~7\);

-- Location: LCCOMB_X29_Y28_N22
\inst6|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~8_combout\ = (\inst6|counter\(4) & (\inst6|Add0~7\ $ (GND))) # (!\inst6|counter\(4) & (!\inst6|Add0~7\ & VCC))
-- \inst6|Add0~9\ = CARRY((\inst6|counter\(4) & !\inst6|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(4),
	datad => VCC,
	cin => \inst6|Add0~7\,
	combout => \inst6|Add0~8_combout\,
	cout => \inst6|Add0~9\);

-- Location: LCCOMB_X29_Y28_N24
\inst6|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~10_combout\ = \inst6|Add0~9\ $ (\inst6|counter\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst6|counter\(5),
	cin => \inst6|Add0~9\,
	combout => \inst6|Add0~10_combout\);

-- Location: FF_X29_Y28_N25
\inst6|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(5));

-- Location: LCCOMB_X29_Y28_N16
\inst6|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Add0~2_combout\ = (\inst6|counter\(1) & ((\inst6|Add0~1\) # (GND))) # (!\inst6|counter\(1) & (!\inst6|Add0~1\))
-- \inst6|Add0~3\ = CARRY((\inst6|counter\(1)) # (!\inst6|Add0~1\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001111001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(1),
	datad => VCC,
	cin => \inst6|Add0~1\,
	combout => \inst6|Add0~2_combout\,
	cout => \inst6|Add0~3\);

-- Location: LCCOMB_X29_Y28_N28
\inst6|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|counter~0_combout\ = ((\inst6|counter\(4) & (!\inst6|counter\(5) & \inst6|Equal0~0_combout\))) # (!\inst6|Add0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(4),
	datab => \inst6|counter\(5),
	datac => \inst6|Equal0~0_combout\,
	datad => \inst6|Add0~2_combout\,
	combout => \inst6|counter~0_combout\);

-- Location: FF_X29_Y28_N29
\inst6|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(1));

-- Location: FF_X29_Y28_N19
\inst6|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(2));

-- Location: LCCOMB_X29_Y28_N10
\inst6|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|Equal0~0_combout\ = (\inst6|counter\(3) & (\inst6|counter\(0) & (!\inst6|counter\(2) & \inst6|counter\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(3),
	datab => \inst6|counter\(0),
	datac => \inst6|counter\(2),
	datad => \inst6|counter\(1),
	combout => \inst6|Equal0~0_combout\);

-- Location: LCCOMB_X29_Y28_N12
\inst6|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|counter~2_combout\ = (\inst6|Add0~8_combout\ & (((\inst6|counter\(5)) # (!\inst6|counter\(4))) # (!\inst6|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|Equal0~0_combout\,
	datab => \inst6|counter\(5),
	datac => \inst6|counter\(4),
	datad => \inst6|Add0~8_combout\,
	combout => \inst6|counter~2_combout\);

-- Location: FF_X29_Y28_N13
\inst6|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(4));

-- Location: LCCOMB_X29_Y28_N30
\inst6|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|counter~1_combout\ = (\inst6|Add0~6_combout\ & (((\inst6|counter\(5)) # (!\inst6|Equal0~0_combout\)) # (!\inst6|counter\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(4),
	datab => \inst6|counter\(5),
	datac => \inst6|Equal0~0_combout\,
	datad => \inst6|Add0~6_combout\,
	combout => \inst6|counter~1_combout\);

-- Location: FF_X29_Y28_N31
\inst6|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|counter\(3));

-- Location: LCCOMB_X24_Y28_N2
\inst6|mem~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~16_combout\ = (\inst6|counter\(2) & (!\inst6|counter\(0) & ((\inst6|counter\(3)) # (\inst6|counter\(1))))) # (!\inst6|counter\(2) & (\inst6|counter\(1) $ (((\inst6|counter\(3)) # (!\inst6|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101001001101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~16_combout\);

-- Location: LCCOMB_X24_Y28_N16
\inst6|mem~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~17_combout\ = (\inst6|counter\(4) & (!\inst6|mem~16_combout\ & ((!\inst6|counter\(0)) # (!\inst6|counter\(3))))) # (!\inst6|counter\(4) & (((!\inst6|mem~16_combout\) # (!\inst6|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(3),
	datab => \inst6|counter\(4),
	datac => \inst6|counter\(0),
	datad => \inst6|mem~16_combout\,
	combout => \inst6|mem~17_combout\);

-- Location: FF_X24_Y28_N17
\inst6|number[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(6));

-- Location: LCCOMB_X24_Y28_N14
\inst6|mem~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~0_combout\ = (\inst6|counter\(2) & (!\inst6|counter\(3) & (\inst6|counter\(0) $ (!\inst6|counter\(1))))) # (!\inst6|counter\(2) & (!\inst6|counter\(0) & (\inst6|counter\(3) $ (!\inst6|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~0_combout\);

-- Location: LCCOMB_X24_Y28_N24
\inst6|mem~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~1_combout\ = (\inst6|counter\(3) & (\inst6|counter\(2) $ ((!\inst6|counter\(0))))) # (!\inst6|counter\(3) & (!\inst6|counter\(1) & (\inst6|counter\(2) $ (!\inst6|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~1_combout\);

-- Location: LCCOMB_X24_Y28_N22
\inst6|mem~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~2_combout\ = (\inst6|counter\(4) & (\inst6|mem~0_combout\)) # (!\inst6|counter\(4) & ((\inst6|mem~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(4),
	datac => \inst6|mem~0_combout\,
	datad => \inst6|mem~1_combout\,
	combout => \inst6|mem~2_combout\);

-- Location: FF_X24_Y28_N23
\inst6|number[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(5));

-- Location: LCCOMB_X24_Y28_N30
\inst6|mem~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~3_combout\ = (\inst6|counter\(2) & (\inst6|counter\(4) $ (((\inst6|counter\(3)) # (!\inst6|counter\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101010000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(1),
	datac => \inst6|counter\(4),
	datad => \inst6|counter\(3),
	combout => \inst6|mem~3_combout\);

-- Location: LCCOMB_X24_Y28_N12
\inst6|mem~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~4_combout\ = (\inst6|counter\(1) & (\inst6|counter\(2) $ ((\inst6|counter\(3))))) # (!\inst6|counter\(1) & ((\inst6|counter\(4) & ((!\inst6|counter\(3)))) # (!\inst6|counter\(4) & (!\inst6|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011000110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(4),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~4_combout\);

-- Location: LCCOMB_X24_Y28_N0
\inst6|mem~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~5_combout\ = (\inst6|counter\(0) & (\inst6|mem~3_combout\)) # (!\inst6|counter\(0) & ((\inst6|mem~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(0),
	datac => \inst6|mem~3_combout\,
	datad => \inst6|mem~4_combout\,
	combout => \inst6|mem~5_combout\);

-- Location: FF_X24_Y28_N1
\inst6|number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(4));

-- Location: LCCOMB_X24_Y28_N10
\inst6|mem~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~6_combout\ = (\inst6|counter\(3) & (!\inst6|counter\(2) & (!\inst6|counter\(0) & \inst6|counter\(1)))) # (!\inst6|counter\(3) & (((\inst6|counter\(0)) # (!\inst6|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~6_combout\);

-- Location: LCCOMB_X24_Y28_N20
\inst6|mem~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~7_combout\ = (\inst6|counter\(0) & (((!\inst6|counter\(1))) # (!\inst6|counter\(2)))) # (!\inst6|counter\(0) & ((\inst6|counter\(3)) # (\inst6|counter\(2) $ (!\inst6|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~7_combout\);

-- Location: LCCOMB_X24_Y28_N26
\inst6|mem~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~8_combout\ = (\inst6|counter\(4) & (\inst6|mem~6_combout\)) # (!\inst6|counter\(4) & ((\inst6|mem~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|mem~6_combout\,
	datac => \inst6|counter\(4),
	datad => \inst6|mem~7_combout\,
	combout => \inst6|mem~8_combout\);

-- Location: FF_X24_Y28_N27
\inst6|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(3));

-- Location: LCCOMB_X29_Y28_N6
\inst6|mem~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~14_combout\ = (\inst6|counter\(0) & ((\inst6|counter\(4) & (!\inst6|counter\(1))) # (!\inst6|counter\(4) & ((!\inst6|counter\(2)))))) # (!\inst6|counter\(0) & ((\inst6|counter\(1) & (!\inst6|counter\(2))) # (!\inst6|counter\(1) & 
-- ((!\inst6|counter\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011000011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(1),
	datab => \inst6|counter\(0),
	datac => \inst6|counter\(2),
	datad => \inst6|counter\(4),
	combout => \inst6|mem~14_combout\);

-- Location: LCCOMB_X29_Y28_N8
\inst6|mem~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~15_combout\ = (\inst6|mem~14_combout\ & ((\inst6|counter\(3) $ (!\inst6|counter\(1))) # (!\inst6|counter\(0)))) # (!\inst6|mem~14_combout\ & (!\inst6|counter\(1) & (\inst6|counter\(3) $ (!\inst6|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011100001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(3),
	datab => \inst6|counter\(0),
	datac => \inst6|counter\(1),
	datad => \inst6|mem~14_combout\,
	combout => \inst6|mem~15_combout\);

-- Location: FF_X29_Y28_N9
\inst6|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(2));

-- Location: LCCOMB_X24_Y28_N8
\inst6|mem~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~10_combout\ = (\inst6|counter\(2) & ((\inst6|counter\(4) & (\inst6|counter\(3))) # (!\inst6|counter\(4) & ((\inst6|counter\(1)))))) # (!\inst6|counter\(2) & (((\inst6|counter\(4) & !\inst6|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(3),
	datac => \inst6|counter\(4),
	datad => \inst6|counter\(1),
	combout => \inst6|mem~10_combout\);

-- Location: LCCOMB_X24_Y28_N18
\inst6|mem~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~9_combout\ = (\inst6|counter\(4) & (!\inst6|counter\(3) & ((!\inst6|counter\(1)) # (!\inst6|counter\(2))))) # (!\inst6|counter\(4) & (\inst6|counter\(3) & (\inst6|counter\(2) $ (\inst6|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011001110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(2),
	datab => \inst6|counter\(1),
	datac => \inst6|counter\(4),
	datad => \inst6|counter\(3),
	combout => \inst6|mem~9_combout\);

-- Location: LCCOMB_X24_Y28_N4
\inst6|mem~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~11_combout\ = (\inst6|counter\(0) & ((\inst6|mem~9_combout\))) # (!\inst6|counter\(0) & (!\inst6|mem~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|counter\(0),
	datac => \inst6|mem~10_combout\,
	datad => \inst6|mem~9_combout\,
	combout => \inst6|mem~11_combout\);

-- Location: FF_X24_Y28_N5
\inst6|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(1));

-- Location: LCCOMB_X29_Y28_N4
\inst6|mem~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~12_combout\ = (\inst6|counter\(1) & (!\inst6|counter\(2) & (\inst6|counter\(3) $ (!\inst6|counter\(4))))) # (!\inst6|counter\(1) & ((\inst6|counter\(3) & (\inst6|counter\(2) & !\inst6|counter\(4))) # (!\inst6|counter\(3) & 
-- ((\inst6|counter\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(3),
	datab => \inst6|counter\(1),
	datac => \inst6|counter\(2),
	datad => \inst6|counter\(4),
	combout => \inst6|mem~12_combout\);

-- Location: LCCOMB_X29_Y28_N26
\inst6|mem~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|mem~13_combout\ = (\inst6|counter\(1) & ((\inst6|mem~12_combout\ & (!\inst6|counter\(0))) # (!\inst6|mem~12_combout\ & (\inst6|counter\(0) & !\inst6|counter\(4))))) # (!\inst6|counter\(1) & ((\inst6|counter\(0) & (\inst6|mem~12_combout\)) # 
-- (!\inst6|counter\(0) & ((!\inst6|counter\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100001101101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(1),
	datab => \inst6|mem~12_combout\,
	datac => \inst6|counter\(0),
	datad => \inst6|counter\(4),
	combout => \inst6|mem~13_combout\);

-- Location: FF_X29_Y28_N27
\inst6|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst6|mem~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|number\(0));

-- Location: LCCOMB_X26_Y28_N18
\inst7|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Add0~0_combout\ = (\inst5|counter\(0) & (\inst7|counter\(1) & VCC)) # (!\inst5|counter\(0) & (\inst7|counter\(1) $ (VCC)))
-- \inst7|Add0~1\ = CARRY((!\inst5|counter\(0) & \inst7|counter\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|counter\(0),
	datab => \inst7|counter\(1),
	datad => VCC,
	combout => \inst7|Add0~0_combout\,
	cout => \inst7|Add0~1\);

-- Location: LCCOMB_X26_Y28_N28
\inst7|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|counter~0_combout\ = (\inst7|Add0~0_combout\ & (((\inst7|counter\(5)) # (!\inst7|Equal0~0_combout\)) # (!\inst7|counter\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(4),
	datab => \inst7|Equal0~0_combout\,
	datac => \inst7|counter\(5),
	datad => \inst7|Add0~0_combout\,
	combout => \inst7|counter~0_combout\);

-- Location: FF_X26_Y28_N29
\inst7|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|counter\(1));

-- Location: LCCOMB_X26_Y28_N20
\inst7|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Add0~2_combout\ = (\inst7|counter\(2) & (!\inst7|Add0~1\)) # (!\inst7|counter\(2) & ((\inst7|Add0~1\) # (GND)))
-- \inst7|Add0~3\ = CARRY((!\inst7|Add0~1\) # (!\inst7|counter\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|counter\(2),
	datad => VCC,
	cin => \inst7|Add0~1\,
	combout => \inst7|Add0~2_combout\,
	cout => \inst7|Add0~3\);

-- Location: FF_X26_Y28_N21
\inst7|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|counter\(2));

-- Location: LCCOMB_X26_Y28_N2
\inst7|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Equal0~0_combout\ = (\inst7|counter\(3) & (!\inst7|counter\(2) & (!\inst5|counter\(0) & !\inst7|counter\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datab => \inst7|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst7|counter\(1),
	combout => \inst7|Equal0~0_combout\);

-- Location: LCCOMB_X26_Y28_N22
\inst7|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Add0~4_combout\ = (\inst7|counter\(3) & (\inst7|Add0~3\ $ (GND))) # (!\inst7|counter\(3) & (!\inst7|Add0~3\ & VCC))
-- \inst7|Add0~5\ = CARRY((\inst7|counter\(3) & !\inst7|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datad => VCC,
	cin => \inst7|Add0~3\,
	combout => \inst7|Add0~4_combout\,
	cout => \inst7|Add0~5\);

-- Location: LCCOMB_X26_Y28_N24
\inst7|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Add0~6_combout\ = (\inst7|counter\(4) & (!\inst7|Add0~5\)) # (!\inst7|counter\(4) & ((\inst7|Add0~5\) # (GND)))
-- \inst7|Add0~7\ = CARRY((!\inst7|Add0~5\) # (!\inst7|counter\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(4),
	datad => VCC,
	cin => \inst7|Add0~5\,
	combout => \inst7|Add0~6_combout\,
	cout => \inst7|Add0~7\);

-- Location: LCCOMB_X26_Y28_N12
\inst7|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|counter~2_combout\ = (\inst7|Add0~6_combout\ & ((\inst7|counter\(5)) # ((!\inst7|counter\(4)) # (!\inst7|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(5),
	datab => \inst7|Equal0~0_combout\,
	datac => \inst7|counter\(4),
	datad => \inst7|Add0~6_combout\,
	combout => \inst7|counter~2_combout\);

-- Location: FF_X26_Y28_N13
\inst7|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|counter\(4));

-- Location: LCCOMB_X26_Y28_N26
\inst7|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|Add0~8_combout\ = \inst7|counter\(5) $ (!\inst7|Add0~7\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(5),
	cin => \inst7|Add0~7\,
	combout => \inst7|Add0~8_combout\);

-- Location: FF_X26_Y28_N27
\inst7|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|counter\(5));

-- Location: LCCOMB_X26_Y28_N10
\inst7|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|counter~1_combout\ = (\inst7|Add0~4_combout\ & ((\inst7|counter\(5)) # ((!\inst7|counter\(4)) # (!\inst7|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(5),
	datab => \inst7|Equal0~0_combout\,
	datac => \inst7|Add0~4_combout\,
	datad => \inst7|counter\(4),
	combout => \inst7|counter~1_combout\);

-- Location: FF_X26_Y28_N11
\inst7|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|counter\(3));

-- Location: LCCOMB_X26_Y28_N8
\inst7|mem~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~16_combout\ = (\inst7|counter\(2) & (\inst5|counter\(0) & ((\inst7|counter\(3)) # (!\inst7|counter\(1))))) # (!\inst7|counter\(2) & (\inst7|counter\(1) $ (((!\inst7|counter\(3) & !\inst5|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011001011000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datab => \inst7|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst7|counter\(1),
	combout => \inst7|mem~16_combout\);

-- Location: LCCOMB_X26_Y28_N0
\inst7|mem~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~17_combout\ = (\inst7|mem~16_combout\ & (((\inst5|counter\(0) & !\inst7|counter\(4))))) # (!\inst7|mem~16_combout\ & (((\inst5|counter\(0)) # (!\inst7|counter\(4))) # (!\inst7|counter\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datab => \inst7|mem~16_combout\,
	datac => \inst5|counter\(0),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~17_combout\);

-- Location: FF_X26_Y28_N1
\inst7|number[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(6));

-- Location: LCCOMB_X26_Y28_N14
\inst7|mem~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~0_combout\ = (\inst7|counter\(2) & (!\inst7|counter\(3) & (\inst7|counter\(1) $ (!\inst5|counter\(0))))) # (!\inst7|counter\(2) & (\inst5|counter\(0) & (\inst7|counter\(3) $ (\inst7|counter\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000101100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datab => \inst7|counter\(1),
	datac => \inst5|counter\(0),
	datad => \inst7|counter\(2),
	combout => \inst7|mem~0_combout\);

-- Location: LCCOMB_X26_Y28_N16
\inst7|mem~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~1_combout\ = (\inst7|counter\(3) & (\inst7|counter\(2) $ ((\inst5|counter\(0))))) # (!\inst7|counter\(3) & (\inst7|counter\(1) & (\inst7|counter\(2) $ (\inst5|counter\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(3),
	datab => \inst7|counter\(2),
	datac => \inst5|counter\(0),
	datad => \inst7|counter\(1),
	combout => \inst7|mem~1_combout\);

-- Location: LCCOMB_X26_Y28_N30
\inst7|mem~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~2_combout\ = (\inst7|counter\(4) & (\inst7|mem~0_combout\)) # (!\inst7|counter\(4) & ((\inst7|mem~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(4),
	datac => \inst7|mem~0_combout\,
	datad => \inst7|mem~1_combout\,
	combout => \inst7|mem~2_combout\);

-- Location: FF_X26_Y28_N31
\inst7|number[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(5));

-- Location: LCCOMB_X23_Y28_N26
\inst7|mem~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~3_combout\ = (\inst7|counter\(1) & ((\inst7|counter\(4) & ((!\inst7|counter\(3)))) # (!\inst7|counter\(4) & (!\inst7|counter\(2))))) # (!\inst7|counter\(1) & (\inst7|counter\(2) $ ((\inst7|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011001010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(2),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(1),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~3_combout\);

-- Location: LCCOMB_X23_Y28_N20
\inst7|mem~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~4_combout\ = (\inst7|counter\(2) & (\inst7|counter\(4) $ (((\inst7|counter\(3)) # (\inst7|counter\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(2),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(1),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~4_combout\);

-- Location: LCCOMB_X23_Y28_N0
\inst7|mem~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~5_combout\ = (\inst5|counter\(0) & (\inst7|mem~3_combout\)) # (!\inst5|counter\(0) & ((\inst7|mem~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(0),
	datac => \inst7|mem~3_combout\,
	datad => \inst7|mem~4_combout\,
	combout => \inst7|mem~5_combout\);

-- Location: FF_X23_Y28_N1
\inst7|number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(4));

-- Location: LCCOMB_X23_Y28_N8
\inst7|mem~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~7_combout\ = (\inst5|counter\(0) & ((\inst7|counter\(3)) # (\inst7|counter\(1) $ (\inst7|counter\(2))))) # (!\inst5|counter\(0) & ((\inst7|counter\(1)) # ((!\inst7|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011011111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(1),
	datab => \inst5|counter\(0),
	datac => \inst7|counter\(3),
	datad => \inst7|counter\(2),
	combout => \inst7|mem~7_combout\);

-- Location: LCCOMB_X23_Y28_N6
\inst7|mem~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~6_combout\ = (\inst7|counter\(1) & (!\inst7|counter\(3))) # (!\inst7|counter\(1) & ((\inst7|counter\(3) & (!\inst7|counter\(2) & \inst5|counter\(0))) # (!\inst7|counter\(3) & ((!\inst5|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(1),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(2),
	datad => \inst5|counter\(0),
	combout => \inst7|mem~6_combout\);

-- Location: LCCOMB_X23_Y28_N2
\inst7|mem~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~8_combout\ = (\inst7|counter\(4) & ((\inst7|mem~6_combout\))) # (!\inst7|counter\(4) & (\inst7|mem~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|counter\(4),
	datac => \inst7|mem~7_combout\,
	datad => \inst7|mem~6_combout\,
	combout => \inst7|mem~8_combout\);

-- Location: FF_X23_Y28_N3
\inst7|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(3));

-- Location: LCCOMB_X23_Y28_N28
\inst7|mem~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~14_combout\ = (\inst5|counter\(0) & ((\inst7|counter\(1) & ((!\inst7|counter\(4)))) # (!\inst7|counter\(1) & (!\inst7|counter\(2))))) # (!\inst5|counter\(0) & ((\inst7|counter\(4) & (\inst7|counter\(1))) # (!\inst7|counter\(4) & 
-- ((!\inst7|counter\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010011010001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(1),
	datab => \inst5|counter\(0),
	datac => \inst7|counter\(2),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~14_combout\);

-- Location: LCCOMB_X23_Y28_N12
\inst7|mem~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~15_combout\ = (\inst7|mem~14_combout\ & ((\inst5|counter\(0)) # (\inst7|counter\(1) $ (\inst7|counter\(3))))) # (!\inst7|mem~14_combout\ & (\inst7|counter\(1) & (\inst5|counter\(0) $ (\inst7|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(1),
	datab => \inst5|counter\(0),
	datac => \inst7|counter\(3),
	datad => \inst7|mem~14_combout\,
	combout => \inst7|mem~15_combout\);

-- Location: FF_X23_Y28_N13
\inst7|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(2));

-- Location: LCCOMB_X23_Y28_N22
\inst7|mem~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~9_combout\ = (\inst7|counter\(3) & (!\inst7|counter\(4) & (\inst7|counter\(2) $ (!\inst7|counter\(1))))) # (!\inst7|counter\(3) & (\inst7|counter\(4) & ((\inst7|counter\(1)) # (!\inst7|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(2),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(1),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~9_combout\);

-- Location: LCCOMB_X23_Y28_N24
\inst7|mem~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~10_combout\ = (\inst7|counter\(2) & ((\inst7|counter\(4) & (\inst7|counter\(3))) # (!\inst7|counter\(4) & ((!\inst7|counter\(1)))))) # (!\inst7|counter\(2) & (((\inst7|counter\(1) & \inst7|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(2),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(1),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~10_combout\);

-- Location: LCCOMB_X23_Y28_N30
\inst7|mem~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~11_combout\ = (\inst5|counter\(0) & ((!\inst7|mem~10_combout\))) # (!\inst5|counter\(0) & (\inst7|mem~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|counter\(0),
	datac => \inst7|mem~9_combout\,
	datad => \inst7|mem~10_combout\,
	combout => \inst7|mem~11_combout\);

-- Location: FF_X23_Y28_N31
\inst7|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(1));

-- Location: LCCOMB_X23_Y28_N10
\inst7|mem~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~12_combout\ = (\inst7|counter\(2) & (\inst7|counter\(4) $ (((\inst7|counter\(3)) # (!\inst7|counter\(1)))))) # (!\inst7|counter\(2) & ((\inst7|counter\(3) & (!\inst7|counter\(1))) # (!\inst7|counter\(3) & (\inst7|counter\(1) & 
-- \inst7|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|counter\(2),
	datab => \inst7|counter\(3),
	datac => \inst7|counter\(1),
	datad => \inst7|counter\(4),
	combout => \inst7|mem~12_combout\);

-- Location: LCCOMB_X23_Y28_N4
\inst7|mem~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst7|mem~13_combout\ = (\inst5|counter\(0) & (\inst7|counter\(4) $ (((\inst7|counter\(1)) # (!\inst7|mem~12_combout\))))) # (!\inst5|counter\(0) & (\inst7|mem~12_combout\ & ((\inst7|counter\(1)) # (!\inst7|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|mem~12_combout\,
	datab => \inst7|counter\(4),
	datac => \inst7|counter\(1),
	datad => \inst5|counter\(0),
	combout => \inst7|mem~13_combout\);

-- Location: FF_X23_Y28_N5
\inst7|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst7|mem~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|number\(0));

-- Location: LCCOMB_X30_Y28_N10
\inst8|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Add0~0_combout\ = (\inst6|counter\(0) & (\inst8|counter\(1) $ (VCC))) # (!\inst6|counter\(0) & (\inst8|counter\(1) & VCC))
-- \inst8|Add0~1\ = CARRY((\inst6|counter\(0) & \inst8|counter\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(0),
	datab => \inst8|counter\(1),
	datad => VCC,
	combout => \inst8|Add0~0_combout\,
	cout => \inst8|Add0~1\);

-- Location: LCCOMB_X30_Y28_N12
\inst8|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Add0~2_combout\ = (\inst8|counter\(2) & (!\inst8|Add0~1\)) # (!\inst8|counter\(2) & ((\inst8|Add0~1\) # (GND)))
-- \inst8|Add0~3\ = CARRY((!\inst8|Add0~1\) # (!\inst8|counter\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(2),
	datad => VCC,
	cin => \inst8|Add0~1\,
	combout => \inst8|Add0~2_combout\,
	cout => \inst8|Add0~3\);

-- Location: FF_X30_Y28_N13
\inst8|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|counter\(2));

-- Location: LCCOMB_X30_Y28_N14
\inst8|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Add0~4_combout\ = (\inst8|counter\(3) & (\inst8|Add0~3\ $ (GND))) # (!\inst8|counter\(3) & (!\inst8|Add0~3\ & VCC))
-- \inst8|Add0~5\ = CARRY((\inst8|counter\(3) & !\inst8|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst8|counter\(3),
	datad => VCC,
	cin => \inst8|Add0~3\,
	combout => \inst8|Add0~4_combout\,
	cout => \inst8|Add0~5\);

-- Location: LCCOMB_X30_Y28_N16
\inst8|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Add0~6_combout\ = (\inst8|counter\(4) & (!\inst8|Add0~5\)) # (!\inst8|counter\(4) & ((\inst8|Add0~5\) # (GND)))
-- \inst8|Add0~7\ = CARRY((!\inst8|Add0~5\) # (!\inst8|counter\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(4),
	datad => VCC,
	cin => \inst8|Add0~5\,
	combout => \inst8|Add0~6_combout\,
	cout => \inst8|Add0~7\);

-- Location: LCCOMB_X30_Y28_N18
\inst8|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Add0~8_combout\ = \inst8|counter\(5) $ (!\inst8|Add0~7\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(5),
	cin => \inst8|Add0~7\,
	combout => \inst8|Add0~8_combout\);

-- Location: FF_X30_Y28_N19
\inst8|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|counter\(5));

-- Location: LCCOMB_X30_Y28_N30
\inst8|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|counter~2_combout\ = (\inst8|Add0~6_combout\ & (((\inst8|counter\(5)) # (!\inst8|counter\(4))) # (!\inst8|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|Equal0~0_combout\,
	datab => \inst8|counter\(5),
	datac => \inst8|counter\(4),
	datad => \inst8|Add0~6_combout\,
	combout => \inst8|counter~2_combout\);

-- Location: FF_X30_Y28_N31
\inst8|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|counter\(4));

-- Location: LCCOMB_X30_Y28_N2
\inst8|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|counter~0_combout\ = (\inst8|Add0~0_combout\ & (((\inst8|counter\(5)) # (!\inst8|Equal0~0_combout\)) # (!\inst8|counter\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(4),
	datab => \inst8|counter\(5),
	datac => \inst8|Equal0~0_combout\,
	datad => \inst8|Add0~0_combout\,
	combout => \inst8|counter~0_combout\);

-- Location: FF_X30_Y28_N3
\inst8|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|counter\(1));

-- Location: LCCOMB_X30_Y28_N26
\inst8|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|Equal0~0_combout\ = (\inst6|counter\(0) & (!\inst8|counter\(1) & (!\inst8|counter\(2) & \inst8|counter\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(0),
	datab => \inst8|counter\(1),
	datac => \inst8|counter\(2),
	datad => \inst8|counter\(3),
	combout => \inst8|Equal0~0_combout\);

-- Location: LCCOMB_X30_Y28_N0
\inst8|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|counter~1_combout\ = (\inst8|Add0~4_combout\ & (((\inst8|counter\(5)) # (!\inst8|counter\(4))) # (!\inst8|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|Equal0~0_combout\,
	datab => \inst8|counter\(5),
	datac => \inst8|Add0~4_combout\,
	datad => \inst8|counter\(4),
	combout => \inst8|counter~1_combout\);

-- Location: FF_X30_Y28_N1
\inst8|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|counter\(3));

-- Location: LCCOMB_X31_Y28_N16
\inst8|mem~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~16_combout\ = (\inst8|counter\(2) & (!\inst6|counter\(0) & ((\inst8|counter\(3)) # (!\inst8|counter\(1))))) # (!\inst8|counter\(2) & (\inst8|counter\(1) $ (((!\inst8|counter\(3) & \inst6|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000111100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst6|counter\(0),
	combout => \inst8|mem~16_combout\);

-- Location: LCCOMB_X31_Y28_N12
\inst8|mem~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~17_combout\ = (\inst6|counter\(0) & (!\inst8|mem~16_combout\ & ((!\inst8|counter\(4)) # (!\inst8|counter\(3))))) # (!\inst6|counter\(0) & (((!\inst8|mem~16_combout\) # (!\inst8|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(3),
	datab => \inst6|counter\(0),
	datac => \inst8|counter\(4),
	datad => \inst8|mem~16_combout\,
	combout => \inst8|mem~17_combout\);

-- Location: FF_X31_Y28_N13
\inst8|number[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~17_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(6));

-- Location: LCCOMB_X30_Y28_N24
\inst8|mem~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~0_combout\ = (\inst8|counter\(2) & (!\inst8|counter\(3) & (\inst6|counter\(0) $ (\inst8|counter\(1))))) # (!\inst8|counter\(2) & (!\inst6|counter\(0) & (\inst8|counter\(1) $ (\inst8|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000101100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(0),
	datab => \inst8|counter\(1),
	datac => \inst8|counter\(2),
	datad => \inst8|counter\(3),
	combout => \inst8|mem~0_combout\);

-- Location: LCCOMB_X30_Y28_N28
\inst8|mem~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~1_combout\ = (\inst8|counter\(1) & (\inst6|counter\(0) $ ((!\inst8|counter\(2))))) # (!\inst8|counter\(1) & (\inst8|counter\(3) & (\inst6|counter\(0) $ (!\inst8|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|counter\(0),
	datab => \inst8|counter\(1),
	datac => \inst8|counter\(2),
	datad => \inst8|counter\(3),
	combout => \inst8|mem~1_combout\);

-- Location: LCCOMB_X30_Y28_N8
\inst8|mem~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~2_combout\ = (\inst8|counter\(4) & (\inst8|mem~0_combout\)) # (!\inst8|counter\(4) & ((\inst8|mem~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst8|mem~0_combout\,
	datac => \inst8|counter\(4),
	datad => \inst8|mem~1_combout\,
	combout => \inst8|mem~2_combout\);

-- Location: FF_X30_Y28_N9
\inst8|number[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(5));

-- Location: LCCOMB_X31_Y28_N0
\inst8|mem~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~3_combout\ = (\inst8|counter\(2) & (\inst8|counter\(4) $ (((\inst8|counter\(1)) # (\inst8|counter\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~3_combout\);

-- Location: LCCOMB_X31_Y28_N2
\inst8|mem~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~4_combout\ = (\inst8|counter\(1) & ((\inst8|counter\(4) & ((!\inst8|counter\(3)))) # (!\inst8|counter\(4) & (!\inst8|counter\(2))))) # (!\inst8|counter\(1) & (\inst8|counter\(2) $ ((\inst8|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~4_combout\);

-- Location: LCCOMB_X31_Y28_N14
\inst8|mem~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~5_combout\ = (\inst6|counter\(0) & (\inst8|mem~3_combout\)) # (!\inst6|counter\(0) & ((\inst8|mem~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst8|mem~3_combout\,
	datac => \inst6|counter\(0),
	datad => \inst8|mem~4_combout\,
	combout => \inst8|mem~5_combout\);

-- Location: FF_X31_Y28_N15
\inst8|number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(4));

-- Location: LCCOMB_X31_Y28_N22
\inst8|mem~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~7_combout\ = (\inst6|counter\(0) & ((\inst8|counter\(1)) # ((!\inst8|counter\(2))))) # (!\inst6|counter\(0) & ((\inst8|counter\(3)) # (\inst8|counter\(1) $ (\inst8|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst6|counter\(0),
	combout => \inst8|mem~7_combout\);

-- Location: LCCOMB_X31_Y28_N28
\inst8|mem~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~6_combout\ = (\inst8|counter\(1) & (((!\inst8|counter\(3))))) # (!\inst8|counter\(1) & ((\inst8|counter\(3) & (!\inst8|counter\(2) & !\inst6|counter\(0))) # (!\inst8|counter\(3) & ((\inst6|counter\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst6|counter\(0),
	combout => \inst8|mem~6_combout\);

-- Location: LCCOMB_X31_Y28_N8
\inst8|mem~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~8_combout\ = (\inst8|counter\(4) & ((\inst8|mem~6_combout\))) # (!\inst8|counter\(4) & (\inst8|mem~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst8|counter\(4),
	datac => \inst8|mem~7_combout\,
	datad => \inst8|mem~6_combout\,
	combout => \inst8|mem~8_combout\);

-- Location: FF_X31_Y28_N9
\inst8|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(3));

-- Location: LCCOMB_X31_Y28_N10
\inst8|mem~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~14_combout\ = (\inst8|counter\(2) & (\inst8|counter\(1) & (\inst6|counter\(0) $ (!\inst8|counter\(4))))) # (!\inst8|counter\(2) & ((\inst8|counter\(1) $ (!\inst6|counter\(0))) # (!\inst8|counter\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst6|counter\(0),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~14_combout\);

-- Location: LCCOMB_X31_Y28_N18
\inst8|mem~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~15_combout\ = (\inst8|mem~14_combout\ & ((\inst8|counter\(3) $ (\inst8|counter\(1))) # (!\inst6|counter\(0)))) # (!\inst8|mem~14_combout\ & (\inst8|counter\(1) & (\inst6|counter\(0) $ (!\inst8|counter\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101110100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|mem~14_combout\,
	datab => \inst6|counter\(0),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(1),
	combout => \inst8|mem~15_combout\);

-- Location: FF_X31_Y28_N19
\inst8|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~15_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(2));

-- Location: LCCOMB_X31_Y28_N6
\inst8|mem~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~10_combout\ = (\inst8|counter\(2) & ((\inst8|counter\(4) & ((\inst8|counter\(3)))) # (!\inst8|counter\(4) & (!\inst8|counter\(1))))) # (!\inst8|counter\(2) & (\inst8|counter\(1) & ((\inst8|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~10_combout\);

-- Location: LCCOMB_X31_Y28_N20
\inst8|mem~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~9_combout\ = (\inst8|counter\(3) & (!\inst8|counter\(4) & (\inst8|counter\(1) $ (!\inst8|counter\(2))))) # (!\inst8|counter\(3) & (\inst8|counter\(4) & ((\inst8|counter\(1)) # (!\inst8|counter\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~9_combout\);

-- Location: LCCOMB_X31_Y28_N4
\inst8|mem~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~11_combout\ = (\inst6|counter\(0) & ((\inst8|mem~9_combout\))) # (!\inst6|counter\(0) & (!\inst8|mem~10_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|mem~10_combout\,
	datac => \inst6|counter\(0),
	datad => \inst8|mem~9_combout\,
	combout => \inst8|mem~11_combout\);

-- Location: FF_X31_Y28_N5
\inst8|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(1));

-- Location: LCCOMB_X31_Y28_N24
\inst8|mem~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~12_combout\ = (\inst8|counter\(1) & ((\inst8|counter\(3) & (\inst8|counter\(2) & !\inst8|counter\(4))) # (!\inst8|counter\(3) & ((\inst8|counter\(4)))))) # (!\inst8|counter\(1) & (!\inst8|counter\(2) & (\inst8|counter\(3) $ 
-- (!\inst8|counter\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001101010000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst8|counter\(2),
	datac => \inst8|counter\(3),
	datad => \inst8|counter\(4),
	combout => \inst8|mem~12_combout\);

-- Location: LCCOMB_X31_Y28_N26
\inst8|mem~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|mem~13_combout\ = (\inst8|counter\(1) & ((\inst6|counter\(0) & ((\inst8|mem~12_combout\))) # (!\inst6|counter\(0) & (!\inst8|counter\(4))))) # (!\inst8|counter\(1) & ((\inst6|counter\(0) & (!\inst8|counter\(4) & !\inst8|mem~12_combout\)) # 
-- (!\inst6|counter\(0) & ((\inst8|mem~12_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101100000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst8|counter\(1),
	datab => \inst6|counter\(0),
	datac => \inst8|counter\(4),
	datad => \inst8|mem~12_combout\,
	combout => \inst8|mem~13_combout\);

-- Location: FF_X31_Y28_N27
\inst8|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst|reduced~clkctrl_outclk\,
	d => \inst8|mem~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|number\(0));

ww_DIGIT0(0) <= \DIGIT0[0]~output_o\;

ww_DIGIT0(1) <= \DIGIT0[1]~output_o\;

ww_DIGIT0(2) <= \DIGIT0[2]~output_o\;

ww_DIGIT0(3) <= \DIGIT0[3]~output_o\;

ww_DIGIT0(4) <= \DIGIT0[4]~output_o\;

ww_DIGIT0(5) <= \DIGIT0[5]~output_o\;

ww_DIGIT0(6) <= \DIGIT0[6]~output_o\;

ww_DIGIT1(0) <= \DIGIT1[0]~output_o\;

ww_DIGIT1(1) <= \DIGIT1[1]~output_o\;

ww_DIGIT1(2) <= \DIGIT1[2]~output_o\;

ww_DIGIT1(3) <= \DIGIT1[3]~output_o\;

ww_DIGIT1(4) <= \DIGIT1[4]~output_o\;

ww_DIGIT1(5) <= \DIGIT1[5]~output_o\;

ww_DIGIT1(6) <= \DIGIT1[6]~output_o\;

ww_DIGIT2(0) <= \DIGIT2[0]~output_o\;

ww_DIGIT2(1) <= \DIGIT2[1]~output_o\;

ww_DIGIT2(2) <= \DIGIT2[2]~output_o\;

ww_DIGIT2(3) <= \DIGIT2[3]~output_o\;

ww_DIGIT2(4) <= \DIGIT2[4]~output_o\;

ww_DIGIT2(5) <= \DIGIT2[5]~output_o\;

ww_DIGIT2(6) <= \DIGIT2[6]~output_o\;

ww_DIGIT3(0) <= \DIGIT3[0]~output_o\;

ww_DIGIT3(1) <= \DIGIT3[1]~output_o\;

ww_DIGIT3(2) <= \DIGIT3[2]~output_o\;

ww_DIGIT3(3) <= \DIGIT3[3]~output_o\;

ww_DIGIT3(4) <= \DIGIT3[4]~output_o\;

ww_DIGIT3(5) <= \DIGIT3[5]~output_o\;

ww_DIGIT3(6) <= \DIGIT3[6]~output_o\;
END structure;


