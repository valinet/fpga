-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Full Version"
-- CREATED		"Tue Mar 15 16:48:59 2016"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY \2clockdivider\ IS 
	PORT
	(
		INPUT :  IN  STD_LOGIC;
		RESET :  IN  STD_LOGIC;
		OUTPUT :  OUT  STD_LOGIC
	);
END \2clockdivider\;

ARCHITECTURE bdf_type OF \2clockdivider\ IS 

SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC;
SIGNAL	DFF_inst :  STD_LOGIC;


BEGIN 
OUTPUT <= SYNTHESIZED_WIRE_0;



PROCESS(INPUT,RESET)
BEGIN
IF (RESET = '0') THEN
	DFF_inst <= '0';
ELSIF (RISING_EDGE(INPUT)) THEN
	DFF_inst <= SYNTHESIZED_WIRE_0;
END IF;
END PROCESS;


SYNTHESIZED_WIRE_0 <= NOT(DFF_inst);



END bdf_type;