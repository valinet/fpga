library verilog;
use verilog.vl_types.all;
entity \24hrclock\ is
    port(
        OUTPUT          : out    vl_logic;
        CLOCK           : in     vl_logic
    );
end \24hrclock\;
