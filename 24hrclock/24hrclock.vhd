-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Full Version"
-- CREATED		"Tue Mar 15 16:43:58 2016"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY \24hrclock\ IS 
	PORT
	(
		CLOCK :  IN  STD_LOGIC;
		RESET :  IN  STD_LOGIC;
		OUTPUT :  OUT  STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END \24hrclock\;

ARCHITECTURE bdf_type OF \24hrclock\ IS 

COMPONENT upcounter
	PORT(clock : IN STD_LOGIC;
		 aclr : IN STD_LOGIC;
		 q : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT \2clockdivider\
	PORT(INPUT : IN STD_LOGIC;
		 RESET : IN STD_LOGIC;
		 OUTPUT : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT \3clockdivider\
	PORT(INPUT : IN STD_LOGIC;
		 RESET : IN STD_LOGIC;
		 OUTPUT : OUT STD_LOGIC
	);
END COMPONENT;

COMPONENT \5clockdivider\
	PORT(INPUT : IN STD_LOGIC;
		 RESET : IN STD_LOGIC;
		 OUTPUT : OUT STD_LOGIC
	);
END COMPONENT;

SIGNAL	Q :  STD_LOGIC_VECTOR(0 TO 5);
SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_5 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_6 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_7 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_8 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_9 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_10 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_11 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_12 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_13 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_14 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_15 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_16 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_17 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_18 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_19 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_20 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_21 :  STD_LOGIC;


BEGIN 



b2v_inst : upcounter
PORT MAP(clock => SYNTHESIZED_WIRE_0,
		 aclr => SYNTHESIZED_WIRE_1,
		 q => OUTPUT);


b2v_inst1 : \2clockdivider\
PORT MAP(INPUT => CLOCK,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_5);




b2v_inst12 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_2,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_3);


b2v_inst13 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_3,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_4);


SYNTHESIZED_WIRE_1 <= NOT(RESET);



b2v_inst15 : \3clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_4,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_0);


b2v_inst2 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_5,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_10);


b2v_inst26 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_6,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_7);


b2v_inst27 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_7,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_8);


b2v_inst28 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_8,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_9);


b2v_inst29 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_9,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_11);


b2v_inst3 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_10,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_16);


b2v_inst30 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_11,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_12);


b2v_inst31 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_12,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_13);


b2v_inst32 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_13,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_14);


b2v_inst33 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_14,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_15);


b2v_inst34 : \5clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_15,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_2);


b2v_inst4 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_16,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_17);


b2v_inst5 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_17,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_18);


b2v_inst6 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_18,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_19);


b2v_inst7 : \2clockdivider\
PORT MAP(INPUT => SYNTHESIZED_WIRE_19,
		 RESET => RESET,
		 OUTPUT => SYNTHESIZED_WIRE_6);



END bdf_type;