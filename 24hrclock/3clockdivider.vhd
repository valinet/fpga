-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Full Version"
-- CREATED		"Tue Mar 15 16:48:44 2016"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY \3clockdivider\ IS 
	PORT
	(
		INPUT :  IN  STD_LOGIC;
		RESET :  IN  STD_LOGIC;
		OUTPUT :  OUT  STD_LOGIC
	);
END \3clockdivider\;

ARCHITECTURE bdf_type OF \3clockdivider\ IS 

SIGNAL	SYNTHESIZED_WIRE_0 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC;
SIGNAL	DFF_inst2 :  STD_LOGIC;


BEGIN 



PROCESS(INPUT,RESET)
BEGIN
IF (RESET = '0') THEN
	SYNTHESIZED_WIRE_3 <= '0';
ELSIF (RISING_EDGE(INPUT)) THEN
	SYNTHESIZED_WIRE_3 <= SYNTHESIZED_WIRE_0;
END IF;
END PROCESS;


PROCESS(INPUT,RESET)
BEGIN
IF (RESET = '0') THEN
	SYNTHESIZED_WIRE_4 <= '0';
ELSIF (RISING_EDGE(INPUT)) THEN
	SYNTHESIZED_WIRE_4 <= SYNTHESIZED_WIRE_3;
END IF;
END PROCESS;


PROCESS(INPUT,RESET)
BEGIN
IF (RESET = '0') THEN
	DFF_inst2 <= '0';
ELSIF (RISING_EDGE(INPUT)) THEN
	DFF_inst2 <= SYNTHESIZED_WIRE_4;
END IF;
END PROCESS;


SYNTHESIZED_WIRE_0 <= SYNTHESIZED_WIRE_1 AND SYNTHESIZED_WIRE_2;


SYNTHESIZED_WIRE_2 <= NOT(SYNTHESIZED_WIRE_3);



SYNTHESIZED_WIRE_1 <= NOT(SYNTHESIZED_WIRE_4);



OUTPUT <= DFF_inst2 OR SYNTHESIZED_WIRE_4;


END bdf_type;