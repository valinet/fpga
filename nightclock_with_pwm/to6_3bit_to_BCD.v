module to6_3bit_to_BCD(input [2:0] bcd, output reg [6:0] seven_seg0);

always @(*)
if (bcd == 4'b000) begin
		seven_seg0 = 7'b1000000;
end else if (bcd == 4'b001) begin
		seven_seg0 = 7'b1111001;
end else if (bcd == 4'b010) begin
		seven_seg0 = 7'b0100100; 
end else if (bcd == 4'b011) begin
		seven_seg0 = 7'b0110000; 
end else if (bcd == 4'b100) begin
		seven_seg0 = 7'b0011001; 
end else if (bcd == 4'b101) begin
		seven_seg0 = 7'b0010010; 
end else if (bcd == 4'b110) begin
		seven_seg0 = 7'b0000010; 
end
endmodule