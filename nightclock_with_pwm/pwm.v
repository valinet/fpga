module pwm(input [6:0] _input, input clock, output reg [6:0] _output);

reg x;

initial x = 0;

always @(posedge(clock)) begin
	if (x == 0) begin
		_output = _input;
		x = 1;
	end else if (x == 1) begin
		_output = 7'b1111111;
		x = 0;
	end
end

endmodule