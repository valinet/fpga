module secondscounter(input clock, input reset, input enabled, output reg[5:0] seconds, output reg ripple);

initial seconds = 6'd0;
initial ripple = 0;

always @(posedge(clock) or posedge(reset)) begin
	if (reset) begin
		seconds = 0;
		ripple = 0;
	end else if (seconds < 6'd59) begin
		seconds = seconds + 1;
		ripple = 0;
	end else begin
		seconds = 0;
		ripple = enabled;
	end
end
endmodule