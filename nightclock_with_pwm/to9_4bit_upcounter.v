module to9_4bit_upcounter(input clock, input reset, input add, output reg[3:0] number, output reg ripple);

always @(posedge(clock) or posedge(reset) or posedge(add)) begin
	if (add) begin
		
	if (reset) begin
		number = 0;
		ripple = 0;
	end else if (number < 9) begin
		number = number + 1;
		ripple = 0;
	end else begin
		number = 0;
		ripple = ~ripple;
	end
end
endmodule