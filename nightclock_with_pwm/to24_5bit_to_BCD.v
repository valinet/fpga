module to24_5bit_to_BCD(input [4:0] bcd, output reg [6:0] seven_seg0, output reg [6:0] seven_seg1);

always @(*)
if (bcd == 5'b00000) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00001) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00010) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00011) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00100) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00101) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00110) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b00111) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b01000) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b01001) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b1111111; 
end else if (bcd == 5'b01010) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b01011) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b01100) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b01101) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b01110) begin
		seven_seg0 = 7'b0011001;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b01111) begin
		seven_seg0 = 7'b0010010;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b10000) begin
		seven_seg0 = 7'b0000010;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b10001) begin
		seven_seg0 = 7'b1111000;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b10010) begin
		seven_seg0 = 7'b0000000;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b10011) begin
		seven_seg0 = 7'b0010000;
		seven_seg1 = 7'b1111001; 
end else if (bcd == 5'b10100) begin
		seven_seg0 = 7'b1000000;
		seven_seg1 = 7'b0100100; 
end else if (bcd == 5'b10101) begin
		seven_seg0 = 7'b1111001;
		seven_seg1 = 7'b0100100; 
end else if (bcd == 5'b10110) begin
		seven_seg0 = 7'b0100100;
		seven_seg1 = 7'b0100100; 
end else if (bcd == 5'b10111) begin
		seven_seg0 = 7'b0110000;
		seven_seg1 = 7'b0100100; 
end
endmodule