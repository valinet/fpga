-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "04/12/2016 14:04:46"

-- 
-- Device: Altera EP3C16F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	nightclock IS
    PORT (
	DECIMAL : OUT std_logic;
	CLOCK : IN std_logic;
	\BEGIN\ : IN std_logic;
	HOURLEFT : OUT std_logic_vector(6 DOWNTO 0);
	INC_HRS : IN std_logic;
	INC_MIN : IN std_logic;
	HOURRIGHT : OUT std_logic_vector(6 DOWNTO 0);
	MINUTESLEFT : OUT std_logic_vector(6 DOWNTO 0);
	OUTPUT : OUT std_logic_vector(6 DOWNTO 0);
	SECONDS : OUT std_logic_vector(9 DOWNTO 0);
	LED_DISPLAY : IN std_logic
	);
END nightclock;

-- Design Ports Information
-- DECIMAL	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[6]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[5]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[4]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[3]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[2]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[1]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURLEFT[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[6]	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[3]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[2]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HOURRIGHT[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[6]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[5]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[4]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[3]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[2]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[1]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- MINUTESLEFT[0]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[6]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[5]	=>  Location: PIN_F12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[4]	=>  Location: PIN_G12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[3]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[2]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[1]	=>  Location: PIN_F11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- OUTPUT[0]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[8]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[7]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[6]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[5]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[3]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[2]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[1]	=>  Location: PIN_J2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SECONDS[0]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LED_DISPLAY	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- BEGIN	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK	=>  Location: PIN_G21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- INC_HRS	=>  Location: PIN_F1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- INC_MIN	=>  Location: PIN_G3,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF nightclock IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_DECIMAL : std_logic;
SIGNAL ww_CLOCK : std_logic;
SIGNAL \ww_BEGIN\ : std_logic;
SIGNAL ww_HOURLEFT : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_INC_HRS : std_logic;
SIGNAL ww_INC_MIN : std_logic;
SIGNAL ww_HOURRIGHT : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_MINUTESLEFT : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_OUTPUT : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_SECONDS : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_LED_DISPLAY : std_logic;
SIGNAL \inst~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst1|reduced~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst20~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \inst19~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \DECIMAL~output_o\ : std_logic;
SIGNAL \HOURLEFT[6]~output_o\ : std_logic;
SIGNAL \HOURLEFT[5]~output_o\ : std_logic;
SIGNAL \HOURLEFT[4]~output_o\ : std_logic;
SIGNAL \HOURLEFT[3]~output_o\ : std_logic;
SIGNAL \HOURLEFT[2]~output_o\ : std_logic;
SIGNAL \HOURLEFT[1]~output_o\ : std_logic;
SIGNAL \HOURLEFT[0]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[6]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[5]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[4]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[3]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[2]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[1]~output_o\ : std_logic;
SIGNAL \HOURRIGHT[0]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[6]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[5]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[4]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[3]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[2]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[1]~output_o\ : std_logic;
SIGNAL \MINUTESLEFT[0]~output_o\ : std_logic;
SIGNAL \OUTPUT[6]~output_o\ : std_logic;
SIGNAL \OUTPUT[5]~output_o\ : std_logic;
SIGNAL \OUTPUT[4]~output_o\ : std_logic;
SIGNAL \OUTPUT[3]~output_o\ : std_logic;
SIGNAL \OUTPUT[2]~output_o\ : std_logic;
SIGNAL \OUTPUT[1]~output_o\ : std_logic;
SIGNAL \OUTPUT[0]~output_o\ : std_logic;
SIGNAL \SECONDS[9]~output_o\ : std_logic;
SIGNAL \SECONDS[8]~output_o\ : std_logic;
SIGNAL \SECONDS[7]~output_o\ : std_logic;
SIGNAL \SECONDS[6]~output_o\ : std_logic;
SIGNAL \SECONDS[5]~output_o\ : std_logic;
SIGNAL \SECONDS[4]~output_o\ : std_logic;
SIGNAL \SECONDS[3]~output_o\ : std_logic;
SIGNAL \SECONDS[2]~output_o\ : std_logic;
SIGNAL \SECONDS[1]~output_o\ : std_logic;
SIGNAL \SECONDS[0]~output_o\ : std_logic;
SIGNAL \CLOCK~input_o\ : std_logic;
SIGNAL \CLOCK~inputclkctrl_outclk\ : std_logic;
SIGNAL \inst37|Add0~0_combout\ : std_logic;
SIGNAL \inst37|counter~1_combout\ : std_logic;
SIGNAL \inst37|Add0~1\ : std_logic;
SIGNAL \inst37|Add0~2_combout\ : std_logic;
SIGNAL \inst37|Add0~3\ : std_logic;
SIGNAL \inst37|Add0~4_combout\ : std_logic;
SIGNAL \inst37|Add0~5\ : std_logic;
SIGNAL \inst37|Add0~6_combout\ : std_logic;
SIGNAL \inst37|Add0~7\ : std_logic;
SIGNAL \inst37|Add0~8_combout\ : std_logic;
SIGNAL \inst37|Add0~9\ : std_logic;
SIGNAL \inst37|Add0~10_combout\ : std_logic;
SIGNAL \inst37|Add0~11\ : std_logic;
SIGNAL \inst37|Add0~12_combout\ : std_logic;
SIGNAL \inst37|counter~0_combout\ : std_logic;
SIGNAL \inst37|Add0~13\ : std_logic;
SIGNAL \inst37|Add0~14_combout\ : std_logic;
SIGNAL \inst37|Add0~15\ : std_logic;
SIGNAL \inst37|Add0~16_combout\ : std_logic;
SIGNAL \inst37|Add0~17\ : std_logic;
SIGNAL \inst37|Add0~18_combout\ : std_logic;
SIGNAL \inst37|Add0~19\ : std_logic;
SIGNAL \inst37|Add0~20_combout\ : std_logic;
SIGNAL \inst37|Add0~21\ : std_logic;
SIGNAL \inst37|Add0~22_combout\ : std_logic;
SIGNAL \inst37|counter~2_combout\ : std_logic;
SIGNAL \inst37|Add0~23\ : std_logic;
SIGNAL \inst37|Add0~24_combout\ : std_logic;
SIGNAL \inst37|counter~3_combout\ : std_logic;
SIGNAL \inst37|Add0~25\ : std_logic;
SIGNAL \inst37|Add0~26_combout\ : std_logic;
SIGNAL \inst37|counter~4_combout\ : std_logic;
SIGNAL \inst37|Add0~27\ : std_logic;
SIGNAL \inst37|Add0~28_combout\ : std_logic;
SIGNAL \inst37|counter~5_combout\ : std_logic;
SIGNAL \inst37|Add0~29\ : std_logic;
SIGNAL \inst37|Add0~30_combout\ : std_logic;
SIGNAL \inst37|Add0~31\ : std_logic;
SIGNAL \inst37|Add0~32_combout\ : std_logic;
SIGNAL \inst37|counter~6_combout\ : std_logic;
SIGNAL \inst37|Add0~33\ : std_logic;
SIGNAL \inst37|Add0~34_combout\ : std_logic;
SIGNAL \inst37|Add0~35\ : std_logic;
SIGNAL \inst37|Add0~36_combout\ : std_logic;
SIGNAL \inst37|counter~7_combout\ : std_logic;
SIGNAL \inst37|Add0~37\ : std_logic;
SIGNAL \inst37|Add0~38_combout\ : std_logic;
SIGNAL \inst37|counter~8_combout\ : std_logic;
SIGNAL \inst37|Equal0~5_combout\ : std_logic;
SIGNAL \inst37|Add0~39\ : std_logic;
SIGNAL \inst37|Add0~40_combout\ : std_logic;
SIGNAL \inst37|counter~10_combout\ : std_logic;
SIGNAL \inst37|Add0~41\ : std_logic;
SIGNAL \inst37|Add0~42_combout\ : std_logic;
SIGNAL \inst37|counter~11_combout\ : std_logic;
SIGNAL \inst37|Add0~43\ : std_logic;
SIGNAL \inst37|Add0~44_combout\ : std_logic;
SIGNAL \inst37|counter~12_combout\ : std_logic;
SIGNAL \inst37|Add0~45\ : std_logic;
SIGNAL \inst37|Add0~46_combout\ : std_logic;
SIGNAL \inst37|Add0~47\ : std_logic;
SIGNAL \inst37|Add0~48_combout\ : std_logic;
SIGNAL \inst37|counter~9_combout\ : std_logic;
SIGNAL \inst37|Equal0~6_combout\ : std_logic;
SIGNAL \inst37|Equal0~1_combout\ : std_logic;
SIGNAL \inst37|Equal0~3_combout\ : std_logic;
SIGNAL \inst37|Equal0~2_combout\ : std_logic;
SIGNAL \inst37|Equal0~0_combout\ : std_logic;
SIGNAL \inst37|Equal0~4_combout\ : std_logic;
SIGNAL \inst37|Equal0~7_combout\ : std_logic;
SIGNAL \inst37|reduced~0_combout\ : std_logic;
SIGNAL \inst37|reduced~feeder_combout\ : std_logic;
SIGNAL \inst37|reduced~q\ : std_logic;
SIGNAL \BEGIN~input_o\ : std_logic;
SIGNAL \inst2~0_combout\ : std_logic;
SIGNAL \inst2~feeder_combout\ : std_logic;
SIGNAL \inst2~q\ : std_logic;
SIGNAL \inst~combout\ : std_logic;
SIGNAL \inst1|Add0~0_combout\ : std_logic;
SIGNAL \inst1|counter~0_combout\ : std_logic;
SIGNAL \inst1|Add0~1\ : std_logic;
SIGNAL \inst1|Add0~2_combout\ : std_logic;
SIGNAL \inst1|Add0~3\ : std_logic;
SIGNAL \inst1|Add0~4_combout\ : std_logic;
SIGNAL \inst1|Add0~5\ : std_logic;
SIGNAL \inst1|Add0~6_combout\ : std_logic;
SIGNAL \inst1|Add0~7\ : std_logic;
SIGNAL \inst1|Add0~8_combout\ : std_logic;
SIGNAL \inst1|Add0~9\ : std_logic;
SIGNAL \inst1|Add0~10_combout\ : std_logic;
SIGNAL \inst1|Add0~11\ : std_logic;
SIGNAL \inst1|Add0~12_combout\ : std_logic;
SIGNAL \inst1|counter~1_combout\ : std_logic;
SIGNAL \inst1|Add0~13\ : std_logic;
SIGNAL \inst1|Add0~14_combout\ : std_logic;
SIGNAL \inst1|Add0~15\ : std_logic;
SIGNAL \inst1|Add0~16_combout\ : std_logic;
SIGNAL \inst1|counter~2_combout\ : std_logic;
SIGNAL \inst1|Add0~17\ : std_logic;
SIGNAL \inst1|Add0~18_combout\ : std_logic;
SIGNAL \inst1|Add0~19\ : std_logic;
SIGNAL \inst1|Add0~20_combout\ : std_logic;
SIGNAL \inst1|counter~3_combout\ : std_logic;
SIGNAL \inst1|Add0~21\ : std_logic;
SIGNAL \inst1|Add0~22_combout\ : std_logic;
SIGNAL \inst1|counter~4_combout\ : std_logic;
SIGNAL \inst1|Add0~23\ : std_logic;
SIGNAL \inst1|Add0~24_combout\ : std_logic;
SIGNAL \inst1|Add0~25\ : std_logic;
SIGNAL \inst1|Add0~26_combout\ : std_logic;
SIGNAL \inst1|Add0~27\ : std_logic;
SIGNAL \inst1|Add0~28_combout\ : std_logic;
SIGNAL \inst1|Add0~29\ : std_logic;
SIGNAL \inst1|Add0~30_combout\ : std_logic;
SIGNAL \inst1|Add0~31\ : std_logic;
SIGNAL \inst1|Add0~32_combout\ : std_logic;
SIGNAL \inst1|counter~5_combout\ : std_logic;
SIGNAL \inst1|Add0~33\ : std_logic;
SIGNAL \inst1|Add0~34_combout\ : std_logic;
SIGNAL \inst1|counter~6_combout\ : std_logic;
SIGNAL \inst1|Add0~35\ : std_logic;
SIGNAL \inst1|Add0~36_combout\ : std_logic;
SIGNAL \inst1|Add0~37\ : std_logic;
SIGNAL \inst1|Add0~38_combout\ : std_logic;
SIGNAL \inst1|Add0~39\ : std_logic;
SIGNAL \inst1|Add0~40_combout\ : std_logic;
SIGNAL \inst1|Add0~41\ : std_logic;
SIGNAL \inst1|Add0~42_combout\ : std_logic;
SIGNAL \inst1|Add0~43\ : std_logic;
SIGNAL \inst1|Add0~44_combout\ : std_logic;
SIGNAL \inst1|Add0~45\ : std_logic;
SIGNAL \inst1|Add0~46_combout\ : std_logic;
SIGNAL \inst1|Add0~47\ : std_logic;
SIGNAL \inst1|Add0~48_combout\ : std_logic;
SIGNAL \inst1|Equal0~6_combout\ : std_logic;
SIGNAL \inst1|Equal0~5_combout\ : std_logic;
SIGNAL \inst1|Equal0~1_combout\ : std_logic;
SIGNAL \inst1|Equal0~2_combout\ : std_logic;
SIGNAL \inst1|Equal0~0_combout\ : std_logic;
SIGNAL \inst1|Equal0~3_combout\ : std_logic;
SIGNAL \inst1|Equal0~4_combout\ : std_logic;
SIGNAL \inst1|Equal0~7_combout\ : std_logic;
SIGNAL \inst1|reduced~0_combout\ : std_logic;
SIGNAL \inst1|reduced~feeder_combout\ : std_logic;
SIGNAL \inst1|reduced~q\ : std_logic;
SIGNAL \inst1|reduced~clkctrl_outclk\ : std_logic;
SIGNAL \INC_HRS~input_o\ : std_logic;
SIGNAL \inst~clkctrl_outclk\ : std_logic;
SIGNAL \inst6|seconds[0]~6_combout\ : std_logic;
SIGNAL \inst6|seconds[0]~7\ : std_logic;
SIGNAL \inst6|seconds[1]~8_combout\ : std_logic;
SIGNAL \inst6|seconds[1]~9\ : std_logic;
SIGNAL \inst6|seconds[2]~10_combout\ : std_logic;
SIGNAL \inst6|seconds[2]~11\ : std_logic;
SIGNAL \inst6|seconds[3]~12_combout\ : std_logic;
SIGNAL \inst6|seconds[3]~13\ : std_logic;
SIGNAL \inst6|seconds[4]~14_combout\ : std_logic;
SIGNAL \inst6|seconds[4]~15\ : std_logic;
SIGNAL \inst6|seconds[5]~16_combout\ : std_logic;
SIGNAL \inst40|LessThan9~0_combout\ : std_logic;
SIGNAL \inst40|LessThan9~1_combout\ : std_logic;
SIGNAL \inst6|ripple~feeder_combout\ : std_logic;
SIGNAL \inst6|ripple~q\ : std_logic;
SIGNAL \INC_MIN~input_o\ : std_logic;
SIGNAL \inst19~combout\ : std_logic;
SIGNAL \inst19~clkctrl_outclk\ : std_logic;
SIGNAL \inst4|seconds[0]~6_combout\ : std_logic;
SIGNAL \inst4|LessThan0~2_combout\ : std_logic;
SIGNAL \inst4|seconds[4]~15\ : std_logic;
SIGNAL \inst4|seconds[5]~16_combout\ : std_logic;
SIGNAL \inst4|LessThan0~3_combout\ : std_logic;
SIGNAL \inst4|seconds[0]~7\ : std_logic;
SIGNAL \inst4|seconds[1]~8_combout\ : std_logic;
SIGNAL \inst4|seconds[1]~9\ : std_logic;
SIGNAL \inst4|seconds[2]~10_combout\ : std_logic;
SIGNAL \inst4|seconds[2]~11\ : std_logic;
SIGNAL \inst4|seconds[3]~12_combout\ : std_logic;
SIGNAL \inst4|seconds[3]~13\ : std_logic;
SIGNAL \inst4|seconds[4]~14_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[1]~9_combout\ : std_logic;
SIGNAL \inst4|ripple~0_combout\ : std_logic;
SIGNAL \inst4|ripple~q\ : std_logic;
SIGNAL \inst20~combout\ : std_logic;
SIGNAL \inst20~clkctrl_outclk\ : std_logic;
SIGNAL \inst11|number[3]~2_combout\ : std_logic;
SIGNAL \inst11|number[3]~4_combout\ : std_logic;
SIGNAL \inst11|Add0~0_combout\ : std_logic;
SIGNAL \inst11|number[0]~8_combout\ : std_logic;
SIGNAL \inst11|number[1]~7_combout\ : std_logic;
SIGNAL \inst11|Add0~1\ : std_logic;
SIGNAL \inst11|Add0~2_combout\ : std_logic;
SIGNAL \inst11|number[1]~9_combout\ : std_logic;
SIGNAL \inst11|Add0~3\ : std_logic;
SIGNAL \inst11|Add0~4_combout\ : std_logic;
SIGNAL \inst11|number[2]~5_combout\ : std_logic;
SIGNAL \inst11|LessThan0~0_combout\ : std_logic;
SIGNAL \inst11|Add0~5\ : std_logic;
SIGNAL \inst11|Add0~7\ : std_logic;
SIGNAL \inst11|Add0~8_combout\ : std_logic;
SIGNAL \inst11|number[4]~3_combout\ : std_logic;
SIGNAL \inst11|LessThan0~1_combout\ : std_logic;
SIGNAL \inst11|Add0~6_combout\ : std_logic;
SIGNAL \inst11|number[3]~6_combout\ : std_logic;
SIGNAL \inst9|comb~51_combout\ : std_logic;
SIGNAL \inst9|comb~52_combout\ : std_logic;
SIGNAL \inst8|x~0_combout\ : std_logic;
SIGNAL \inst8|x~q\ : std_logic;
SIGNAL \inst13|_output~0_combout\ : std_logic;
SIGNAL \inst13|_output~1_combout\ : std_logic;
SIGNAL \inst13|_output~2_combout\ : std_logic;
SIGNAL \inst9|comb~54_combout\ : std_logic;
SIGNAL \inst9|comb~53_combout\ : std_logic;
SIGNAL \inst13|_output~3_combout\ : std_logic;
SIGNAL \inst9|comb~65_combout\ : std_logic;
SIGNAL \inst9|comb~66_combout\ : std_logic;
SIGNAL \inst13|_output~4_combout\ : std_logic;
SIGNAL \inst13|_output~5_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[1]~2_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[6]~13_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[6]~3_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[6]~4_combout\ : std_logic;
SIGNAL \inst9|comb~56_combout\ : std_logic;
SIGNAL \inst9|comb~55_combout\ : std_logic;
SIGNAL \inst12|_output~0_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[5]~5_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[5]~6_combout\ : std_logic;
SIGNAL \inst9|comb~57_combout\ : std_logic;
SIGNAL \inst9|comb~58_combout\ : std_logic;
SIGNAL \inst12|_output~1_combout\ : std_logic;
SIGNAL \inst9|comb~70_combout\ : std_logic;
SIGNAL \inst9|comb~71_combout\ : std_logic;
SIGNAL \inst9|comb~46_combout\ : std_logic;
SIGNAL \inst9|comb~69_combout\ : std_logic;
SIGNAL \inst12|_output~2_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[3]~8_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[3]~7_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[3]~9_combout\ : std_logic;
SIGNAL \inst9|comb~59_combout\ : std_logic;
SIGNAL \inst9|comb~60_combout\ : std_logic;
SIGNAL \inst12|_output~3_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[2]~14_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[2]~15_combout\ : std_logic;
SIGNAL \inst9|comb~62_combout\ : std_logic;
SIGNAL \inst9|comb~61_combout\ : std_logic;
SIGNAL \inst12|_output~4_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[1]~10_combout\ : std_logic;
SIGNAL \inst9|comb~67_combout\ : std_logic;
SIGNAL \inst9|comb~68_combout\ : std_logic;
SIGNAL \inst12|_output~5_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[0]~12_combout\ : std_logic;
SIGNAL \inst9|seven_seg0[0]~11_combout\ : std_logic;
SIGNAL \inst9|comb~64_combout\ : std_logic;
SIGNAL \inst9|comb~63_combout\ : std_logic;
SIGNAL \inst12|_output~6_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~4_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[6]~0_combout\ : std_logic;
SIGNAL \inst5|comb~1_combout\ : std_logic;
SIGNAL \inst5|comb~0_combout\ : std_logic;
SIGNAL \inst10|_output~0_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[5]~1_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[5]~2_combout\ : std_logic;
SIGNAL \inst5|comb~3_combout\ : std_logic;
SIGNAL \inst5|comb~2_combout\ : std_logic;
SIGNAL \inst10|_output~1_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[4]~5_combout\ : std_logic;
SIGNAL \inst5|Equal0~0_combout\ : std_logic;
SIGNAL \inst5|Equal0~1_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[2]~3_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[2]~4_combout\ : std_logic;
SIGNAL \inst5|comb~4_combout\ : std_logic;
SIGNAL \inst5|comb~5_combout\ : std_logic;
SIGNAL \inst10|_output~2_combout\ : std_logic;
SIGNAL \inst5|Equal0~2_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[3]~7_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[3]~6_combout\ : std_logic;
SIGNAL \inst5|comb~7_combout\ : std_logic;
SIGNAL \inst5|comb~6_combout\ : std_logic;
SIGNAL \inst10|_output~3_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[2]~8_combout\ : std_logic;
SIGNAL \inst5|comb~9_combout\ : std_logic;
SIGNAL \inst5|comb~8_combout\ : std_logic;
SIGNAL \inst10|_output~4_combout\ : std_logic;
SIGNAL \inst5|Equal0~3_combout\ : std_logic;
SIGNAL \inst5|seven_seg1[2]~10_combout\ : std_logic;
SIGNAL \inst5|comb~10_combout\ : std_logic;
SIGNAL \inst5|comb~11_combout\ : std_logic;
SIGNAL \inst5|comb~12_combout\ : std_logic;
SIGNAL \inst10|_output~5_combout\ : std_logic;
SIGNAL \inst10|_output~6_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~10_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~9_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~12_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~11_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~6_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~5_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[1]~7_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~8_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~13_combout\ : std_logic;
SIGNAL \inst5|comb~14_combout\ : std_logic;
SIGNAL \inst5|comb~13_combout\ : std_logic;
SIGNAL \inst8|_output~0_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[6]~16_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[5]~17_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[5]~15_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[5]~18_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[5]~14_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[5]~36_combout\ : std_logic;
SIGNAL \inst5|comb~15_combout\ : std_logic;
SIGNAL \inst5|comb~16_combout\ : std_logic;
SIGNAL \inst8|_output~1_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[4]~19_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[4]~20_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[4]~21_combout\ : std_logic;
SIGNAL \inst5|comb~17_combout\ : std_logic;
SIGNAL \inst5|comb~18_combout\ : std_logic;
SIGNAL \inst8|_output~2_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[3]~22_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[3]~23_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[3]~24_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[3]~25_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[3]~26_combout\ : std_logic;
SIGNAL \inst5|comb~20_combout\ : std_logic;
SIGNAL \inst5|comb~19_combout\ : std_logic;
SIGNAL \inst8|_output~3_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[2]~28_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[2]~27_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[2]~29_combout\ : std_logic;
SIGNAL \inst5|comb~22_combout\ : std_logic;
SIGNAL \inst5|comb~21_combout\ : std_logic;
SIGNAL \inst8|_output~4_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[1]~30_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[1]~31_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[1]~37_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[1]~32_combout\ : std_logic;
SIGNAL \inst5|comb~24_combout\ : std_logic;
SIGNAL \inst5|comb~23_combout\ : std_logic;
SIGNAL \inst8|_output~5_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[0]~33_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[0]~34_combout\ : std_logic;
SIGNAL \inst5|seven_seg0[0]~35_combout\ : std_logic;
SIGNAL \inst5|comb~26_combout\ : std_logic;
SIGNAL \inst5|comb~25_combout\ : std_logic;
SIGNAL \inst8|_output~6_combout\ : std_logic;
SIGNAL \LED_DISPLAY~input_o\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[9]~22_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[8]~10_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[7]~13_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[7]~14_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[6]~15_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[5]~16_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[5]~17_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[4]~18_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[4]~19_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[3]~20_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[3]~23_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[2]~21_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[1]~24_combout\ : std_logic;
SIGNAL \inst3|LPM_MUX_component|auto_generated|result_node[0]~25_combout\ : std_logic;
SIGNAL \inst5|seven_seg1\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst13|_output\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst12|_output\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst10|_output\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst4|seconds\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst9|seven_seg1\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst37|counter\ : std_logic_vector(24 DOWNTO 0);
SIGNAL \inst5|seven_seg0\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst11|number\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \inst6|seconds\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \inst9|seven_seg0\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \inst1|counter\ : std_logic_vector(24 DOWNTO 0);
SIGNAL \inst8|_output\ : std_logic_vector(6 DOWNTO 0);

BEGIN

DECIMAL <= ww_DECIMAL;
ww_CLOCK <= CLOCK;
\ww_BEGIN\ <= \BEGIN\;
HOURLEFT <= ww_HOURLEFT;
ww_INC_HRS <= INC_HRS;
ww_INC_MIN <= INC_MIN;
HOURRIGHT <= ww_HOURRIGHT;
MINUTESLEFT <= ww_MINUTESLEFT;
OUTPUT <= ww_OUTPUT;
SECONDS <= ww_SECONDS;
ww_LED_DISPLAY <= LED_DISPLAY;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst~combout\);

\inst1|reduced~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst1|reduced~q\);

\inst20~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst20~combout\);

\inst19~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst19~combout\);

\CLOCK~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCK~input_o\);

-- Location: IOOBUF_X32_Y29_N16
\DECIMAL~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst~combout\,
	devoe => ww_devoe,
	o => \DECIMAL~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\HOURLEFT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(6),
	devoe => ww_devoe,
	o => \HOURLEFT[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N30
\HOURLEFT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HOURLEFT[5]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\HOURLEFT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(4),
	devoe => ww_devoe,
	o => \HOURLEFT[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\HOURLEFT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(3),
	devoe => ww_devoe,
	o => \HOURLEFT[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\HOURLEFT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(2),
	devoe => ww_devoe,
	o => \HOURLEFT[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N16
\HOURLEFT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(1),
	devoe => ww_devoe,
	o => \HOURLEFT[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\HOURLEFT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst13|_output\(0),
	devoe => ww_devoe,
	o => \HOURLEFT[0]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\HOURRIGHT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(6),
	devoe => ww_devoe,
	o => \HOURRIGHT[6]~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\HOURRIGHT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(5),
	devoe => ww_devoe,
	o => \HOURRIGHT[5]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\HOURRIGHT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(4),
	devoe => ww_devoe,
	o => \HOURRIGHT[4]~output_o\);

-- Location: IOOBUF_X30_Y29_N2
\HOURRIGHT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(3),
	devoe => ww_devoe,
	o => \HOURRIGHT[3]~output_o\);

-- Location: IOOBUF_X28_Y29_N2
\HOURRIGHT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(2),
	devoe => ww_devoe,
	o => \HOURRIGHT[2]~output_o\);

-- Location: IOOBUF_X30_Y29_N30
\HOURRIGHT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(1),
	devoe => ww_devoe,
	o => \HOURRIGHT[1]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\HOURRIGHT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst12|_output\(0),
	devoe => ww_devoe,
	o => \HOURRIGHT[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\MINUTESLEFT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(6),
	devoe => ww_devoe,
	o => \MINUTESLEFT[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\MINUTESLEFT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(5),
	devoe => ww_devoe,
	o => \MINUTESLEFT[5]~output_o\);

-- Location: IOOBUF_X23_Y29_N30
\MINUTESLEFT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(4),
	devoe => ww_devoe,
	o => \MINUTESLEFT[4]~output_o\);

-- Location: IOOBUF_X23_Y29_N23
\MINUTESLEFT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(3),
	devoe => ww_devoe,
	o => \MINUTESLEFT[3]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\MINUTESLEFT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(2),
	devoe => ww_devoe,
	o => \MINUTESLEFT[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\MINUTESLEFT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(1),
	devoe => ww_devoe,
	o => \MINUTESLEFT[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N2
\MINUTESLEFT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst10|_output\(0),
	devoe => ww_devoe,
	o => \MINUTESLEFT[0]~output_o\);

-- Location: IOOBUF_X26_Y29_N16
\OUTPUT[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(6),
	devoe => ww_devoe,
	o => \OUTPUT[6]~output_o\);

-- Location: IOOBUF_X28_Y29_N23
\OUTPUT[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(5),
	devoe => ww_devoe,
	o => \OUTPUT[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N9
\OUTPUT[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(4),
	devoe => ww_devoe,
	o => \OUTPUT[4]~output_o\);

-- Location: IOOBUF_X28_Y29_N30
\OUTPUT[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(3),
	devoe => ww_devoe,
	o => \OUTPUT[3]~output_o\);

-- Location: IOOBUF_X26_Y29_N2
\OUTPUT[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(2),
	devoe => ww_devoe,
	o => \OUTPUT[2]~output_o\);

-- Location: IOOBUF_X21_Y29_N30
\OUTPUT[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(1),
	devoe => ww_devoe,
	o => \OUTPUT[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N23
\OUTPUT[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst8|_output\(0),
	devoe => ww_devoe,
	o => \OUTPUT[0]~output_o\);

-- Location: IOOBUF_X0_Y27_N16
\SECONDS[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[9]~22_combout\,
	devoe => ww_devoe,
	o => \SECONDS[9]~output_o\);

-- Location: IOOBUF_X0_Y27_N9
\SECONDS[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[8]~12_combout\,
	devoe => ww_devoe,
	o => \SECONDS[8]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\SECONDS[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[7]~14_combout\,
	devoe => ww_devoe,
	o => \SECONDS[7]~output_o\);

-- Location: IOOBUF_X0_Y26_N23
\SECONDS[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[6]~15_combout\,
	devoe => ww_devoe,
	o => \SECONDS[6]~output_o\);

-- Location: IOOBUF_X0_Y24_N16
\SECONDS[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[5]~17_combout\,
	devoe => ww_devoe,
	o => \SECONDS[5]~output_o\);

-- Location: IOOBUF_X0_Y24_N23
\SECONDS[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[4]~19_combout\,
	devoe => ww_devoe,
	o => \SECONDS[4]~output_o\);

-- Location: IOOBUF_X0_Y21_N16
\SECONDS[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[3]~23_combout\,
	devoe => ww_devoe,
	o => \SECONDS[3]~output_o\);

-- Location: IOOBUF_X0_Y21_N23
\SECONDS[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[2]~21_combout\,
	devoe => ww_devoe,
	o => \SECONDS[2]~output_o\);

-- Location: IOOBUF_X0_Y20_N2
\SECONDS[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[1]~24_combout\,
	devoe => ww_devoe,
	o => \SECONDS[1]~output_o\);

-- Location: IOOBUF_X0_Y20_N9
\SECONDS[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|LPM_MUX_component|auto_generated|result_node[0]~25_combout\,
	devoe => ww_devoe,
	o => \SECONDS[0]~output_o\);

-- Location: IOIBUF_X41_Y15_N1
\CLOCK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK,
	o => \CLOCK~input_o\);

-- Location: CLKCTRL_G9
\CLOCK~inputclkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK~inputclkctrl_outclk\);

-- Location: LCCOMB_X26_Y18_N8
\inst37|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~0_combout\ = \inst37|counter\(0) $ (VCC)
-- \inst37|Add0~1\ = CARRY(\inst37|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(0),
	datad => VCC,
	combout => \inst37|Add0~0_combout\,
	cout => \inst37|Add0~1\);

-- Location: LCCOMB_X26_Y18_N2
\inst37|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~1_combout\ = (\inst37|Add0~0_combout\ & !\inst37|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst37|Add0~0_combout\,
	datac => \inst37|Equal0~7_combout\,
	combout => \inst37|counter~1_combout\);

-- Location: FF_X26_Y18_N3
\inst37|counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(0));

-- Location: LCCOMB_X26_Y18_N10
\inst37|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~2_combout\ = (\inst37|counter\(1) & (!\inst37|Add0~1\)) # (!\inst37|counter\(1) & ((\inst37|Add0~1\) # (GND)))
-- \inst37|Add0~3\ = CARRY((!\inst37|Add0~1\) # (!\inst37|counter\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(1),
	datad => VCC,
	cin => \inst37|Add0~1\,
	combout => \inst37|Add0~2_combout\,
	cout => \inst37|Add0~3\);

-- Location: FF_X26_Y18_N11
\inst37|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(1));

-- Location: LCCOMB_X26_Y18_N12
\inst37|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~4_combout\ = (\inst37|counter\(2) & (\inst37|Add0~3\ $ (GND))) # (!\inst37|counter\(2) & (!\inst37|Add0~3\ & VCC))
-- \inst37|Add0~5\ = CARRY((\inst37|counter\(2) & !\inst37|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(2),
	datad => VCC,
	cin => \inst37|Add0~3\,
	combout => \inst37|Add0~4_combout\,
	cout => \inst37|Add0~5\);

-- Location: FF_X26_Y18_N13
\inst37|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(2));

-- Location: LCCOMB_X26_Y18_N14
\inst37|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~6_combout\ = (\inst37|counter\(3) & (!\inst37|Add0~5\)) # (!\inst37|counter\(3) & ((\inst37|Add0~5\) # (GND)))
-- \inst37|Add0~7\ = CARRY((!\inst37|Add0~5\) # (!\inst37|counter\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(3),
	datad => VCC,
	cin => \inst37|Add0~5\,
	combout => \inst37|Add0~6_combout\,
	cout => \inst37|Add0~7\);

-- Location: FF_X26_Y18_N15
\inst37|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(3));

-- Location: LCCOMB_X26_Y18_N16
\inst37|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~8_combout\ = (\inst37|counter\(4) & (\inst37|Add0~7\ $ (GND))) # (!\inst37|counter\(4) & (!\inst37|Add0~7\ & VCC))
-- \inst37|Add0~9\ = CARRY((\inst37|counter\(4) & !\inst37|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(4),
	datad => VCC,
	cin => \inst37|Add0~7\,
	combout => \inst37|Add0~8_combout\,
	cout => \inst37|Add0~9\);

-- Location: FF_X26_Y18_N17
\inst37|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(4));

-- Location: LCCOMB_X26_Y18_N18
\inst37|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~10_combout\ = (\inst37|counter\(5) & (!\inst37|Add0~9\)) # (!\inst37|counter\(5) & ((\inst37|Add0~9\) # (GND)))
-- \inst37|Add0~11\ = CARRY((!\inst37|Add0~9\) # (!\inst37|counter\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(5),
	datad => VCC,
	cin => \inst37|Add0~9\,
	combout => \inst37|Add0~10_combout\,
	cout => \inst37|Add0~11\);

-- Location: FF_X26_Y18_N19
\inst37|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(5));

-- Location: LCCOMB_X26_Y18_N20
\inst37|Add0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~12_combout\ = (\inst37|counter\(6) & (\inst37|Add0~11\ $ (GND))) # (!\inst37|counter\(6) & (!\inst37|Add0~11\ & VCC))
-- \inst37|Add0~13\ = CARRY((\inst37|counter\(6) & !\inst37|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(6),
	datad => VCC,
	cin => \inst37|Add0~11\,
	combout => \inst37|Add0~12_combout\,
	cout => \inst37|Add0~13\);

-- Location: LCCOMB_X26_Y18_N0
\inst37|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~0_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~12_combout\,
	combout => \inst37|counter~0_combout\);

-- Location: FF_X26_Y18_N1
\inst37|counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(6));

-- Location: LCCOMB_X26_Y18_N22
\inst37|Add0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~14_combout\ = (\inst37|counter\(7) & (!\inst37|Add0~13\)) # (!\inst37|counter\(7) & ((\inst37|Add0~13\) # (GND)))
-- \inst37|Add0~15\ = CARRY((!\inst37|Add0~13\) # (!\inst37|counter\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(7),
	datad => VCC,
	cin => \inst37|Add0~13\,
	combout => \inst37|Add0~14_combout\,
	cout => \inst37|Add0~15\);

-- Location: FF_X26_Y18_N23
\inst37|counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(7));

-- Location: LCCOMB_X26_Y18_N24
\inst37|Add0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~16_combout\ = (\inst37|counter\(8) & (\inst37|Add0~15\ $ (GND))) # (!\inst37|counter\(8) & (!\inst37|Add0~15\ & VCC))
-- \inst37|Add0~17\ = CARRY((\inst37|counter\(8) & !\inst37|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(8),
	datad => VCC,
	cin => \inst37|Add0~15\,
	combout => \inst37|Add0~16_combout\,
	cout => \inst37|Add0~17\);

-- Location: FF_X26_Y18_N25
\inst37|counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~16_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(8));

-- Location: LCCOMB_X26_Y18_N26
\inst37|Add0~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~18_combout\ = (\inst37|counter\(9) & (!\inst37|Add0~17\)) # (!\inst37|counter\(9) & ((\inst37|Add0~17\) # (GND)))
-- \inst37|Add0~19\ = CARRY((!\inst37|Add0~17\) # (!\inst37|counter\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(9),
	datad => VCC,
	cin => \inst37|Add0~17\,
	combout => \inst37|Add0~18_combout\,
	cout => \inst37|Add0~19\);

-- Location: FF_X26_Y18_N27
\inst37|counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(9));

-- Location: LCCOMB_X26_Y18_N28
\inst37|Add0~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~20_combout\ = (\inst37|counter\(10) & (\inst37|Add0~19\ $ (GND))) # (!\inst37|counter\(10) & (!\inst37|Add0~19\ & VCC))
-- \inst37|Add0~21\ = CARRY((\inst37|counter\(10) & !\inst37|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(10),
	datad => VCC,
	cin => \inst37|Add0~19\,
	combout => \inst37|Add0~20_combout\,
	cout => \inst37|Add0~21\);

-- Location: FF_X26_Y18_N29
\inst37|counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(10));

-- Location: LCCOMB_X26_Y18_N30
\inst37|Add0~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~22_combout\ = (\inst37|counter\(11) & (!\inst37|Add0~21\)) # (!\inst37|counter\(11) & ((\inst37|Add0~21\) # (GND)))
-- \inst37|Add0~23\ = CARRY((!\inst37|Add0~21\) # (!\inst37|counter\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(11),
	datad => VCC,
	cin => \inst37|Add0~21\,
	combout => \inst37|Add0~22_combout\,
	cout => \inst37|Add0~23\);

-- Location: LCCOMB_X26_Y18_N4
\inst37|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~2_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~22_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst37|Equal0~7_combout\,
	datac => \inst37|Add0~22_combout\,
	combout => \inst37|counter~2_combout\);

-- Location: FF_X26_Y18_N5
\inst37|counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(11));

-- Location: LCCOMB_X26_Y17_N0
\inst37|Add0~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~24_combout\ = (\inst37|counter\(12) & (\inst37|Add0~23\ $ (GND))) # (!\inst37|counter\(12) & (!\inst37|Add0~23\ & VCC))
-- \inst37|Add0~25\ = CARRY((\inst37|counter\(12) & !\inst37|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(12),
	datad => VCC,
	cin => \inst37|Add0~23\,
	combout => \inst37|Add0~24_combout\,
	cout => \inst37|Add0~25\);

-- Location: LCCOMB_X27_Y17_N24
\inst37|counter~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~3_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~24_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~24_combout\,
	combout => \inst37|counter~3_combout\);

-- Location: FF_X27_Y17_N25
\inst37|counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(12));

-- Location: LCCOMB_X26_Y17_N2
\inst37|Add0~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~26_combout\ = (\inst37|counter\(13) & (!\inst37|Add0~25\)) # (!\inst37|counter\(13) & ((\inst37|Add0~25\) # (GND)))
-- \inst37|Add0~27\ = CARRY((!\inst37|Add0~25\) # (!\inst37|counter\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(13),
	datad => VCC,
	cin => \inst37|Add0~25\,
	combout => \inst37|Add0~26_combout\,
	cout => \inst37|Add0~27\);

-- Location: LCCOMB_X27_Y17_N2
\inst37|counter~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~4_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~26_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~26_combout\,
	combout => \inst37|counter~4_combout\);

-- Location: FF_X27_Y17_N3
\inst37|counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(13));

-- Location: LCCOMB_X26_Y17_N4
\inst37|Add0~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~28_combout\ = (\inst37|counter\(14) & (\inst37|Add0~27\ $ (GND))) # (!\inst37|counter\(14) & (!\inst37|Add0~27\ & VCC))
-- \inst37|Add0~29\ = CARRY((\inst37|counter\(14) & !\inst37|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(14),
	datad => VCC,
	cin => \inst37|Add0~27\,
	combout => \inst37|Add0~28_combout\,
	cout => \inst37|Add0~29\);

-- Location: LCCOMB_X26_Y17_N30
\inst37|counter~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~5_combout\ = (\inst37|Add0~28_combout\ & !\inst37|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Add0~28_combout\,
	datad => \inst37|Equal0~7_combout\,
	combout => \inst37|counter~5_combout\);

-- Location: FF_X26_Y17_N31
\inst37|counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(14));

-- Location: LCCOMB_X26_Y17_N6
\inst37|Add0~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~30_combout\ = (\inst37|counter\(15) & (!\inst37|Add0~29\)) # (!\inst37|counter\(15) & ((\inst37|Add0~29\) # (GND)))
-- \inst37|Add0~31\ = CARRY((!\inst37|Add0~29\) # (!\inst37|counter\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(15),
	datad => VCC,
	cin => \inst37|Add0~29\,
	combout => \inst37|Add0~30_combout\,
	cout => \inst37|Add0~31\);

-- Location: FF_X26_Y17_N7
\inst37|counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(15));

-- Location: LCCOMB_X26_Y17_N8
\inst37|Add0~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~32_combout\ = (\inst37|counter\(16) & (\inst37|Add0~31\ $ (GND))) # (!\inst37|counter\(16) & (!\inst37|Add0~31\ & VCC))
-- \inst37|Add0~33\ = CARRY((\inst37|counter\(16) & !\inst37|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(16),
	datad => VCC,
	cin => \inst37|Add0~31\,
	combout => \inst37|Add0~32_combout\,
	cout => \inst37|Add0~33\);

-- Location: LCCOMB_X27_Y17_N28
\inst37|counter~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~6_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~32_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~32_combout\,
	combout => \inst37|counter~6_combout\);

-- Location: FF_X27_Y17_N29
\inst37|counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(16));

-- Location: LCCOMB_X26_Y17_N10
\inst37|Add0~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~34_combout\ = (\inst37|counter\(17) & (!\inst37|Add0~33\)) # (!\inst37|counter\(17) & ((\inst37|Add0~33\) # (GND)))
-- \inst37|Add0~35\ = CARRY((!\inst37|Add0~33\) # (!\inst37|counter\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(17),
	datad => VCC,
	cin => \inst37|Add0~33\,
	combout => \inst37|Add0~34_combout\,
	cout => \inst37|Add0~35\);

-- Location: FF_X26_Y17_N11
\inst37|counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~34_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(17));

-- Location: LCCOMB_X26_Y17_N12
\inst37|Add0~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~36_combout\ = (\inst37|counter\(18) & (\inst37|Add0~35\ $ (GND))) # (!\inst37|counter\(18) & (!\inst37|Add0~35\ & VCC))
-- \inst37|Add0~37\ = CARRY((\inst37|counter\(18) & !\inst37|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(18),
	datad => VCC,
	cin => \inst37|Add0~35\,
	combout => \inst37|Add0~36_combout\,
	cout => \inst37|Add0~37\);

-- Location: LCCOMB_X27_Y17_N14
\inst37|counter~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~7_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~36_combout\,
	combout => \inst37|counter~7_combout\);

-- Location: FF_X27_Y17_N15
\inst37|counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(18));

-- Location: LCCOMB_X26_Y17_N14
\inst37|Add0~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~38_combout\ = (\inst37|counter\(19) & (!\inst37|Add0~37\)) # (!\inst37|counter\(19) & ((\inst37|Add0~37\) # (GND)))
-- \inst37|Add0~39\ = CARRY((!\inst37|Add0~37\) # (!\inst37|counter\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(19),
	datad => VCC,
	cin => \inst37|Add0~37\,
	combout => \inst37|Add0~38_combout\,
	cout => \inst37|Add0~39\);

-- Location: LCCOMB_X27_Y17_N20
\inst37|counter~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~8_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~38_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~38_combout\,
	combout => \inst37|counter~8_combout\);

-- Location: FF_X27_Y17_N21
\inst37|counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(19));

-- Location: LCCOMB_X27_Y17_N10
\inst37|Equal0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~5_combout\ = (!\inst37|counter\(17) & (\inst37|counter\(16) & (\inst37|counter\(18) & \inst37|counter\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(17),
	datab => \inst37|counter\(16),
	datac => \inst37|counter\(18),
	datad => \inst37|counter\(19),
	combout => \inst37|Equal0~5_combout\);

-- Location: LCCOMB_X26_Y17_N16
\inst37|Add0~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~40_combout\ = (\inst37|counter\(20) & (\inst37|Add0~39\ $ (GND))) # (!\inst37|counter\(20) & (!\inst37|Add0~39\ & VCC))
-- \inst37|Add0~41\ = CARRY((\inst37|counter\(20) & !\inst37|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(20),
	datad => VCC,
	cin => \inst37|Add0~39\,
	combout => \inst37|Add0~40_combout\,
	cout => \inst37|Add0~41\);

-- Location: LCCOMB_X26_Y17_N28
\inst37|counter~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~10_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~40_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~40_combout\,
	combout => \inst37|counter~10_combout\);

-- Location: FF_X26_Y17_N29
\inst37|counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(20));

-- Location: LCCOMB_X26_Y17_N18
\inst37|Add0~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~42_combout\ = (\inst37|counter\(21) & (!\inst37|Add0~41\)) # (!\inst37|counter\(21) & ((\inst37|Add0~41\) # (GND)))
-- \inst37|Add0~43\ = CARRY((!\inst37|Add0~41\) # (!\inst37|counter\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(21),
	datad => VCC,
	cin => \inst37|Add0~41\,
	combout => \inst37|Add0~42_combout\,
	cout => \inst37|Add0~43\);

-- Location: LCCOMB_X26_Y17_N26
\inst37|counter~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~11_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~42_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~42_combout\,
	combout => \inst37|counter~11_combout\);

-- Location: FF_X26_Y17_N27
\inst37|counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(21));

-- Location: LCCOMB_X26_Y17_N20
\inst37|Add0~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~44_combout\ = (\inst37|counter\(22) & (\inst37|Add0~43\ $ (GND))) # (!\inst37|counter\(22) & (!\inst37|Add0~43\ & VCC))
-- \inst37|Add0~45\ = CARRY((\inst37|counter\(22) & !\inst37|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst37|counter\(22),
	datad => VCC,
	cin => \inst37|Add0~43\,
	combout => \inst37|Add0~44_combout\,
	cout => \inst37|Add0~45\);

-- Location: LCCOMB_X27_Y17_N22
\inst37|counter~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~12_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~44_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~44_combout\,
	combout => \inst37|counter~12_combout\);

-- Location: FF_X27_Y17_N23
\inst37|counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(22));

-- Location: LCCOMB_X26_Y17_N22
\inst37|Add0~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~46_combout\ = (\inst37|counter\(23) & (!\inst37|Add0~45\)) # (!\inst37|counter\(23) & ((\inst37|Add0~45\) # (GND)))
-- \inst37|Add0~47\ = CARRY((!\inst37|Add0~45\) # (!\inst37|counter\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(23),
	datad => VCC,
	cin => \inst37|Add0~45\,
	combout => \inst37|Add0~46_combout\,
	cout => \inst37|Add0~47\);

-- Location: FF_X26_Y17_N23
\inst37|counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|Add0~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(23));

-- Location: LCCOMB_X26_Y17_N24
\inst37|Add0~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Add0~48_combout\ = \inst37|Add0~47\ $ (!\inst37|counter\(24))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst37|counter\(24),
	cin => \inst37|Add0~47\,
	combout => \inst37|Add0~48_combout\);

-- Location: LCCOMB_X27_Y17_N16
\inst37|counter~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|counter~9_combout\ = (!\inst37|Equal0~7_combout\ & \inst37|Add0~48_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|Equal0~7_combout\,
	datad => \inst37|Add0~48_combout\,
	combout => \inst37|counter~9_combout\);

-- Location: FF_X27_Y17_N17
\inst37|counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|counter~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|counter\(24));

-- Location: LCCOMB_X27_Y17_N8
\inst37|Equal0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~6_combout\ = (\inst37|counter\(22) & (!\inst37|counter\(23) & (\inst37|counter\(21) & \inst37|counter\(20))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(22),
	datab => \inst37|counter\(23),
	datac => \inst37|counter\(21),
	datad => \inst37|counter\(20),
	combout => \inst37|Equal0~6_combout\);

-- Location: LCCOMB_X27_Y17_N30
\inst37|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~1_combout\ = (!\inst37|counter\(2) & (!\inst37|counter\(1) & (!\inst37|counter\(3) & !\inst37|counter\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(2),
	datab => \inst37|counter\(1),
	datac => \inst37|counter\(3),
	datad => \inst37|counter\(0),
	combout => \inst37|Equal0~1_combout\);

-- Location: LCCOMB_X27_Y17_N4
\inst37|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~3_combout\ = (\inst37|counter\(12) & (!\inst37|counter\(15) & (\inst37|counter\(14) & \inst37|counter\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(12),
	datab => \inst37|counter\(15),
	datac => \inst37|counter\(14),
	datad => \inst37|counter\(13),
	combout => \inst37|Equal0~3_combout\);

-- Location: LCCOMB_X26_Y18_N6
\inst37|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~2_combout\ = (!\inst37|counter\(9) & (!\inst37|counter\(10) & (\inst37|counter\(11) & !\inst37|counter\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(9),
	datab => \inst37|counter\(10),
	datac => \inst37|counter\(11),
	datad => \inst37|counter\(8),
	combout => \inst37|Equal0~2_combout\);

-- Location: LCCOMB_X27_Y17_N12
\inst37|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~0_combout\ = (!\inst37|counter\(4) & (!\inst37|counter\(7) & (\inst37|counter\(6) & !\inst37|counter\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|counter\(4),
	datab => \inst37|counter\(7),
	datac => \inst37|counter\(6),
	datad => \inst37|counter\(5),
	combout => \inst37|Equal0~0_combout\);

-- Location: LCCOMB_X27_Y17_N18
\inst37|Equal0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~4_combout\ = (\inst37|Equal0~1_combout\ & (\inst37|Equal0~3_combout\ & (\inst37|Equal0~2_combout\ & \inst37|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|Equal0~1_combout\,
	datab => \inst37|Equal0~3_combout\,
	datac => \inst37|Equal0~2_combout\,
	datad => \inst37|Equal0~0_combout\,
	combout => \inst37|Equal0~4_combout\);

-- Location: LCCOMB_X27_Y17_N26
\inst37|Equal0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|Equal0~7_combout\ = (\inst37|Equal0~5_combout\ & (\inst37|counter\(24) & (\inst37|Equal0~6_combout\ & \inst37|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|Equal0~5_combout\,
	datab => \inst37|counter\(24),
	datac => \inst37|Equal0~6_combout\,
	datad => \inst37|Equal0~4_combout\,
	combout => \inst37|Equal0~7_combout\);

-- Location: LCCOMB_X26_Y21_N26
\inst37|reduced~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|reduced~0_combout\ = \inst37|reduced~q\ $ (\inst37|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|reduced~q\,
	datad => \inst37|Equal0~7_combout\,
	combout => \inst37|reduced~0_combout\);

-- Location: LCCOMB_X26_Y21_N30
\inst37|reduced~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst37|reduced~feeder_combout\ = \inst37|reduced~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst37|reduced~0_combout\,
	combout => \inst37|reduced~feeder_combout\);

-- Location: FF_X26_Y21_N31
\inst37|reduced\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst37|reduced~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst37|reduced~q\);

-- Location: IOIBUF_X0_Y21_N8
\BEGIN~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => \ww_BEGIN\,
	o => \BEGIN~input_o\);

-- Location: LCCOMB_X26_Y21_N2
\inst2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst2~0_combout\ = \BEGIN~input_o\ $ (!\inst2~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \BEGIN~input_o\,
	datad => \inst2~q\,
	combout => \inst2~0_combout\);

-- Location: LCCOMB_X26_Y21_N6
\inst2~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst2~feeder_combout\ = \inst2~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst2~0_combout\,
	combout => \inst2~feeder_combout\);

-- Location: FF_X26_Y21_N7
inst2 : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \BEGIN~input_o\,
	d => \inst2~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst2~q\);

-- Location: LCCOMB_X26_Y21_N22
inst : cycloneiii_lcell_comb
-- Equation(s):
-- \inst~combout\ = LCELL((\inst37|reduced~q\ & \inst2~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst37|reduced~q\,
	datad => \inst2~q\,
	combout => \inst~combout\);

-- Location: LCCOMB_X3_Y22_N8
\inst1|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~0_combout\ = \inst1|counter\(0) $ (VCC)
-- \inst1|Add0~1\ = CARRY(\inst1|counter\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(0),
	datad => VCC,
	combout => \inst1|Add0~0_combout\,
	cout => \inst1|Add0~1\);

-- Location: LCCOMB_X3_Y22_N4
\inst1|counter~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~0_combout\ = (\inst1|Add0~0_combout\ & !\inst1|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~0_combout\,
	datad => \inst1|Equal0~7_combout\,
	combout => \inst1|counter~0_combout\);

-- Location: FF_X3_Y22_N5
\inst1|counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(0));

-- Location: LCCOMB_X3_Y22_N10
\inst1|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~2_combout\ = (\inst1|counter\(1) & (!\inst1|Add0~1\)) # (!\inst1|counter\(1) & ((\inst1|Add0~1\) # (GND)))
-- \inst1|Add0~3\ = CARRY((!\inst1|Add0~1\) # (!\inst1|counter\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(1),
	datad => VCC,
	cin => \inst1|Add0~1\,
	combout => \inst1|Add0~2_combout\,
	cout => \inst1|Add0~3\);

-- Location: FF_X3_Y22_N11
\inst1|counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(1));

-- Location: LCCOMB_X3_Y22_N12
\inst1|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~4_combout\ = (\inst1|counter\(2) & (\inst1|Add0~3\ $ (GND))) # (!\inst1|counter\(2) & (!\inst1|Add0~3\ & VCC))
-- \inst1|Add0~5\ = CARRY((\inst1|counter\(2) & !\inst1|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(2),
	datad => VCC,
	cin => \inst1|Add0~3\,
	combout => \inst1|Add0~4_combout\,
	cout => \inst1|Add0~5\);

-- Location: FF_X3_Y22_N13
\inst1|counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(2));

-- Location: LCCOMB_X3_Y22_N14
\inst1|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~6_combout\ = (\inst1|counter\(3) & (!\inst1|Add0~5\)) # (!\inst1|counter\(3) & ((\inst1|Add0~5\) # (GND)))
-- \inst1|Add0~7\ = CARRY((!\inst1|Add0~5\) # (!\inst1|counter\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(3),
	datad => VCC,
	cin => \inst1|Add0~5\,
	combout => \inst1|Add0~6_combout\,
	cout => \inst1|Add0~7\);

-- Location: FF_X3_Y22_N15
\inst1|counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(3));

-- Location: LCCOMB_X3_Y22_N16
\inst1|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~8_combout\ = (\inst1|counter\(4) & (\inst1|Add0~7\ $ (GND))) # (!\inst1|counter\(4) & (!\inst1|Add0~7\ & VCC))
-- \inst1|Add0~9\ = CARRY((\inst1|counter\(4) & !\inst1|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(4),
	datad => VCC,
	cin => \inst1|Add0~7\,
	combout => \inst1|Add0~8_combout\,
	cout => \inst1|Add0~9\);

-- Location: FF_X3_Y22_N17
\inst1|counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(4));

-- Location: LCCOMB_X3_Y22_N18
\inst1|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~10_combout\ = (\inst1|counter\(5) & (!\inst1|Add0~9\)) # (!\inst1|counter\(5) & ((\inst1|Add0~9\) # (GND)))
-- \inst1|Add0~11\ = CARRY((!\inst1|Add0~9\) # (!\inst1|counter\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(5),
	datad => VCC,
	cin => \inst1|Add0~9\,
	combout => \inst1|Add0~10_combout\,
	cout => \inst1|Add0~11\);

-- Location: FF_X3_Y22_N19
\inst1|counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(5));

-- Location: LCCOMB_X3_Y22_N20
\inst1|Add0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~12_combout\ = (\inst1|counter\(6) & (\inst1|Add0~11\ $ (GND))) # (!\inst1|counter\(6) & (!\inst1|Add0~11\ & VCC))
-- \inst1|Add0~13\ = CARRY((\inst1|counter\(6) & !\inst1|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(6),
	datad => VCC,
	cin => \inst1|Add0~11\,
	combout => \inst1|Add0~12_combout\,
	cout => \inst1|Add0~13\);

-- Location: LCCOMB_X3_Y22_N0
\inst1|counter~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~1_combout\ = (\inst1|Add0~12_combout\ & !\inst1|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~12_combout\,
	datad => \inst1|Equal0~7_combout\,
	combout => \inst1|counter~1_combout\);

-- Location: FF_X3_Y22_N1
\inst1|counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(6));

-- Location: LCCOMB_X3_Y22_N22
\inst1|Add0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~14_combout\ = (\inst1|counter\(7) & (!\inst1|Add0~13\)) # (!\inst1|counter\(7) & ((\inst1|Add0~13\) # (GND)))
-- \inst1|Add0~15\ = CARRY((!\inst1|Add0~13\) # (!\inst1|counter\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(7),
	datad => VCC,
	cin => \inst1|Add0~13\,
	combout => \inst1|Add0~14_combout\,
	cout => \inst1|Add0~15\);

-- Location: FF_X3_Y22_N23
\inst1|counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(7));

-- Location: LCCOMB_X3_Y22_N24
\inst1|Add0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~16_combout\ = (\inst1|counter\(8) & (\inst1|Add0~15\ $ (GND))) # (!\inst1|counter\(8) & (!\inst1|Add0~15\ & VCC))
-- \inst1|Add0~17\ = CARRY((\inst1|counter\(8) & !\inst1|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(8),
	datad => VCC,
	cin => \inst1|Add0~15\,
	combout => \inst1|Add0~16_combout\,
	cout => \inst1|Add0~17\);

-- Location: LCCOMB_X2_Y22_N8
\inst1|counter~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~2_combout\ = (!\inst1|Equal0~7_combout\ & \inst1|Add0~16_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Equal0~7_combout\,
	datad => \inst1|Add0~16_combout\,
	combout => \inst1|counter~2_combout\);

-- Location: FF_X2_Y22_N9
\inst1|counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(8));

-- Location: LCCOMB_X3_Y22_N26
\inst1|Add0~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~18_combout\ = (\inst1|counter\(9) & (!\inst1|Add0~17\)) # (!\inst1|counter\(9) & ((\inst1|Add0~17\) # (GND)))
-- \inst1|Add0~19\ = CARRY((!\inst1|Add0~17\) # (!\inst1|counter\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(9),
	datad => VCC,
	cin => \inst1|Add0~17\,
	combout => \inst1|Add0~18_combout\,
	cout => \inst1|Add0~19\);

-- Location: FF_X3_Y22_N27
\inst1|counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(9));

-- Location: LCCOMB_X3_Y22_N28
\inst1|Add0~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~20_combout\ = (\inst1|counter\(10) & (\inst1|Add0~19\ $ (GND))) # (!\inst1|counter\(10) & (!\inst1|Add0~19\ & VCC))
-- \inst1|Add0~21\ = CARRY((\inst1|counter\(10) & !\inst1|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(10),
	datad => VCC,
	cin => \inst1|Add0~19\,
	combout => \inst1|Add0~20_combout\,
	cout => \inst1|Add0~21\);

-- Location: LCCOMB_X2_Y22_N16
\inst1|counter~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~3_combout\ = (!\inst1|Equal0~7_combout\ & \inst1|Add0~20_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Equal0~7_combout\,
	datad => \inst1|Add0~20_combout\,
	combout => \inst1|counter~3_combout\);

-- Location: FF_X2_Y22_N17
\inst1|counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(10));

-- Location: LCCOMB_X3_Y22_N30
\inst1|Add0~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~22_combout\ = (\inst1|counter\(11) & (!\inst1|Add0~21\)) # (!\inst1|counter\(11) & ((\inst1|Add0~21\) # (GND)))
-- \inst1|Add0~23\ = CARRY((!\inst1|Add0~21\) # (!\inst1|counter\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(11),
	datad => VCC,
	cin => \inst1|Add0~21\,
	combout => \inst1|Add0~22_combout\,
	cout => \inst1|Add0~23\);

-- Location: LCCOMB_X2_Y22_N14
\inst1|counter~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~4_combout\ = (!\inst1|Equal0~7_combout\ & \inst1|Add0~22_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|Equal0~7_combout\,
	datac => \inst1|Add0~22_combout\,
	combout => \inst1|counter~4_combout\);

-- Location: FF_X2_Y22_N15
\inst1|counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(11));

-- Location: LCCOMB_X3_Y21_N0
\inst1|Add0~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~24_combout\ = (\inst1|counter\(12) & (\inst1|Add0~23\ $ (GND))) # (!\inst1|counter\(12) & (!\inst1|Add0~23\ & VCC))
-- \inst1|Add0~25\ = CARRY((\inst1|counter\(12) & !\inst1|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(12),
	datad => VCC,
	cin => \inst1|Add0~23\,
	combout => \inst1|Add0~24_combout\,
	cout => \inst1|Add0~25\);

-- Location: FF_X3_Y21_N1
\inst1|counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~24_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(12));

-- Location: LCCOMB_X3_Y21_N2
\inst1|Add0~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~26_combout\ = (\inst1|counter\(13) & (!\inst1|Add0~25\)) # (!\inst1|counter\(13) & ((\inst1|Add0~25\) # (GND)))
-- \inst1|Add0~27\ = CARRY((!\inst1|Add0~25\) # (!\inst1|counter\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(13),
	datad => VCC,
	cin => \inst1|Add0~25\,
	combout => \inst1|Add0~26_combout\,
	cout => \inst1|Add0~27\);

-- Location: FF_X3_Y21_N3
\inst1|counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~26_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(13));

-- Location: LCCOMB_X3_Y21_N4
\inst1|Add0~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~28_combout\ = (\inst1|counter\(14) & (\inst1|Add0~27\ $ (GND))) # (!\inst1|counter\(14) & (!\inst1|Add0~27\ & VCC))
-- \inst1|Add0~29\ = CARRY((\inst1|counter\(14) & !\inst1|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(14),
	datad => VCC,
	cin => \inst1|Add0~27\,
	combout => \inst1|Add0~28_combout\,
	cout => \inst1|Add0~29\);

-- Location: FF_X3_Y21_N5
\inst1|counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(14));

-- Location: LCCOMB_X3_Y21_N6
\inst1|Add0~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~30_combout\ = (\inst1|counter\(15) & (!\inst1|Add0~29\)) # (!\inst1|counter\(15) & ((\inst1|Add0~29\) # (GND)))
-- \inst1|Add0~31\ = CARRY((!\inst1|Add0~29\) # (!\inst1|counter\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(15),
	datad => VCC,
	cin => \inst1|Add0~29\,
	combout => \inst1|Add0~30_combout\,
	cout => \inst1|Add0~31\);

-- Location: FF_X3_Y21_N7
\inst1|counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(15));

-- Location: LCCOMB_X3_Y21_N8
\inst1|Add0~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~32_combout\ = (\inst1|counter\(16) & (\inst1|Add0~31\ $ (GND))) # (!\inst1|counter\(16) & (!\inst1|Add0~31\ & VCC))
-- \inst1|Add0~33\ = CARRY((\inst1|counter\(16) & !\inst1|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(16),
	datad => VCC,
	cin => \inst1|Add0~31\,
	combout => \inst1|Add0~32_combout\,
	cout => \inst1|Add0~33\);

-- Location: LCCOMB_X3_Y21_N30
\inst1|counter~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~5_combout\ = (\inst1|Add0~32_combout\ & !\inst1|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Add0~32_combout\,
	datad => \inst1|Equal0~7_combout\,
	combout => \inst1|counter~5_combout\);

-- Location: FF_X3_Y21_N31
\inst1|counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(16));

-- Location: LCCOMB_X3_Y21_N10
\inst1|Add0~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~34_combout\ = (\inst1|counter\(17) & (!\inst1|Add0~33\)) # (!\inst1|counter\(17) & ((\inst1|Add0~33\) # (GND)))
-- \inst1|Add0~35\ = CARRY((!\inst1|Add0~33\) # (!\inst1|counter\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(17),
	datad => VCC,
	cin => \inst1|Add0~33\,
	combout => \inst1|Add0~34_combout\,
	cout => \inst1|Add0~35\);

-- Location: LCCOMB_X3_Y21_N28
\inst1|counter~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|counter~6_combout\ = (!\inst1|Equal0~7_combout\ & \inst1|Add0~34_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst1|Equal0~7_combout\,
	datad => \inst1|Add0~34_combout\,
	combout => \inst1|counter~6_combout\);

-- Location: FF_X3_Y21_N29
\inst1|counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|counter~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(17));

-- Location: LCCOMB_X3_Y21_N12
\inst1|Add0~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~36_combout\ = (\inst1|counter\(18) & (\inst1|Add0~35\ $ (GND))) # (!\inst1|counter\(18) & (!\inst1|Add0~35\ & VCC))
-- \inst1|Add0~37\ = CARRY((\inst1|counter\(18) & !\inst1|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(18),
	datad => VCC,
	cin => \inst1|Add0~35\,
	combout => \inst1|Add0~36_combout\,
	cout => \inst1|Add0~37\);

-- Location: FF_X3_Y21_N13
\inst1|counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(18));

-- Location: LCCOMB_X3_Y21_N14
\inst1|Add0~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~38_combout\ = (\inst1|counter\(19) & (!\inst1|Add0~37\)) # (!\inst1|counter\(19) & ((\inst1|Add0~37\) # (GND)))
-- \inst1|Add0~39\ = CARRY((!\inst1|Add0~37\) # (!\inst1|counter\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(19),
	datad => VCC,
	cin => \inst1|Add0~37\,
	combout => \inst1|Add0~38_combout\,
	cout => \inst1|Add0~39\);

-- Location: FF_X3_Y21_N15
\inst1|counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(19));

-- Location: LCCOMB_X3_Y21_N16
\inst1|Add0~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~40_combout\ = (\inst1|counter\(20) & (\inst1|Add0~39\ $ (GND))) # (!\inst1|counter\(20) & (!\inst1|Add0~39\ & VCC))
-- \inst1|Add0~41\ = CARRY((\inst1|counter\(20) & !\inst1|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(20),
	datad => VCC,
	cin => \inst1|Add0~39\,
	combout => \inst1|Add0~40_combout\,
	cout => \inst1|Add0~41\);

-- Location: FF_X3_Y21_N17
\inst1|counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(20));

-- Location: LCCOMB_X3_Y21_N18
\inst1|Add0~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~42_combout\ = (\inst1|counter\(21) & (!\inst1|Add0~41\)) # (!\inst1|counter\(21) & ((\inst1|Add0~41\) # (GND)))
-- \inst1|Add0~43\ = CARRY((!\inst1|Add0~41\) # (!\inst1|counter\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(21),
	datad => VCC,
	cin => \inst1|Add0~41\,
	combout => \inst1|Add0~42_combout\,
	cout => \inst1|Add0~43\);

-- Location: FF_X3_Y21_N19
\inst1|counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(21));

-- Location: LCCOMB_X3_Y21_N20
\inst1|Add0~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~44_combout\ = (\inst1|counter\(22) & (\inst1|Add0~43\ $ (GND))) # (!\inst1|counter\(22) & (!\inst1|Add0~43\ & VCC))
-- \inst1|Add0~45\ = CARRY((\inst1|counter\(22) & !\inst1|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst1|counter\(22),
	datad => VCC,
	cin => \inst1|Add0~43\,
	combout => \inst1|Add0~44_combout\,
	cout => \inst1|Add0~45\);

-- Location: FF_X3_Y21_N21
\inst1|counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(22));

-- Location: LCCOMB_X3_Y21_N22
\inst1|Add0~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~46_combout\ = (\inst1|counter\(23) & (!\inst1|Add0~45\)) # (!\inst1|counter\(23) & ((\inst1|Add0~45\) # (GND)))
-- \inst1|Add0~47\ = CARRY((!\inst1|Add0~45\) # (!\inst1|counter\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(23),
	datad => VCC,
	cin => \inst1|Add0~45\,
	combout => \inst1|Add0~46_combout\,
	cout => \inst1|Add0~47\);

-- Location: FF_X3_Y21_N23
\inst1|counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(23));

-- Location: LCCOMB_X3_Y21_N24
\inst1|Add0~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Add0~48_combout\ = \inst1|Add0~47\ $ (!\inst1|counter\(24))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst1|counter\(24),
	cin => \inst1|Add0~47\,
	combout => \inst1|Add0~48_combout\);

-- Location: FF_X3_Y21_N25
\inst1|counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|Add0~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|counter\(24));

-- Location: LCCOMB_X2_Y21_N0
\inst1|Equal0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~6_combout\ = (!\inst1|counter\(22) & (!\inst1|counter\(20) & (!\inst1|counter\(21) & !\inst1|counter\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(22),
	datab => \inst1|counter\(20),
	datac => \inst1|counter\(21),
	datad => \inst1|counter\(23),
	combout => \inst1|Equal0~6_combout\);

-- Location: LCCOMB_X3_Y21_N26
\inst1|Equal0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~5_combout\ = (!\inst1|counter\(18) & (!\inst1|counter\(19) & (\inst1|counter\(16) & \inst1|counter\(17))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(18),
	datab => \inst1|counter\(19),
	datac => \inst1|counter\(16),
	datad => \inst1|counter\(17),
	combout => \inst1|Equal0~5_combout\);

-- Location: LCCOMB_X3_Y22_N2
\inst1|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~1_combout\ = (!\inst1|counter\(7) & (!\inst1|counter\(4) & (!\inst1|counter\(5) & \inst1|counter\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(7),
	datab => \inst1|counter\(4),
	datac => \inst1|counter\(5),
	datad => \inst1|counter\(6),
	combout => \inst1|Equal0~1_combout\);

-- Location: LCCOMB_X2_Y22_N20
\inst1|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~2_combout\ = (\inst1|counter\(11) & (\inst1|counter\(10) & (\inst1|counter\(8) & !\inst1|counter\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(11),
	datab => \inst1|counter\(10),
	datac => \inst1|counter\(8),
	datad => \inst1|counter\(9),
	combout => \inst1|Equal0~2_combout\);

-- Location: LCCOMB_X3_Y22_N6
\inst1|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~0_combout\ = (!\inst1|counter\(2) & (!\inst1|counter\(3) & (!\inst1|counter\(0) & !\inst1|counter\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(2),
	datab => \inst1|counter\(3),
	datac => \inst1|counter\(0),
	datad => \inst1|counter\(1),
	combout => \inst1|Equal0~0_combout\);

-- Location: LCCOMB_X2_Y22_N28
\inst1|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~3_combout\ = (!\inst1|counter\(14) & (!\inst1|counter\(12) & (!\inst1|counter\(15) & !\inst1|counter\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(14),
	datab => \inst1|counter\(12),
	datac => \inst1|counter\(15),
	datad => \inst1|counter\(13),
	combout => \inst1|Equal0~3_combout\);

-- Location: LCCOMB_X2_Y22_N24
\inst1|Equal0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~4_combout\ = (\inst1|Equal0~1_combout\ & (\inst1|Equal0~2_combout\ & (\inst1|Equal0~0_combout\ & \inst1|Equal0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|Equal0~1_combout\,
	datab => \inst1|Equal0~2_combout\,
	datac => \inst1|Equal0~0_combout\,
	datad => \inst1|Equal0~3_combout\,
	combout => \inst1|Equal0~4_combout\);

-- Location: LCCOMB_X2_Y22_N12
\inst1|Equal0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|Equal0~7_combout\ = (!\inst1|counter\(24) & (\inst1|Equal0~6_combout\ & (\inst1|Equal0~5_combout\ & \inst1|Equal0~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|counter\(24),
	datab => \inst1|Equal0~6_combout\,
	datac => \inst1|Equal0~5_combout\,
	datad => \inst1|Equal0~4_combout\,
	combout => \inst1|Equal0~7_combout\);

-- Location: LCCOMB_X2_Y22_N22
\inst1|reduced~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|reduced~0_combout\ = \inst1|reduced~q\ $ (\inst1|Equal0~7_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst1|reduced~q\,
	datad => \inst1|Equal0~7_combout\,
	combout => \inst1|reduced~0_combout\);

-- Location: LCCOMB_X2_Y22_N30
\inst1|reduced~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst1|reduced~feeder_combout\ = \inst1|reduced~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst1|reduced~0_combout\,
	combout => \inst1|reduced~feeder_combout\);

-- Location: FF_X2_Y22_N31
\inst1|reduced\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK~inputclkctrl_outclk\,
	d => \inst1|reduced~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst1|reduced~q\);

-- Location: CLKCTRL_G2
\inst1|reduced~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst1|reduced~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst1|reduced~clkctrl_outclk\);

-- Location: IOIBUF_X0_Y23_N1
\INC_HRS~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_INC_HRS,
	o => \INC_HRS~input_o\);

-- Location: CLKCTRL_G10
\inst~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst~clkctrl_outclk\);

-- Location: LCCOMB_X1_Y21_N10
\inst6|seconds[0]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[0]~6_combout\ = \inst6|seconds\(0) $ (VCC)
-- \inst6|seconds[0]~7\ = CARRY(\inst6|seconds\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(0),
	datad => VCC,
	combout => \inst6|seconds[0]~6_combout\,
	cout => \inst6|seconds[0]~7\);

-- Location: FF_X1_Y21_N11
\inst6|seconds[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[0]~6_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(0));

-- Location: LCCOMB_X1_Y21_N12
\inst6|seconds[1]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[1]~8_combout\ = (\inst6|seconds\(1) & (!\inst6|seconds[0]~7\)) # (!\inst6|seconds\(1) & ((\inst6|seconds[0]~7\) # (GND)))
-- \inst6|seconds[1]~9\ = CARRY((!\inst6|seconds[0]~7\) # (!\inst6|seconds\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(1),
	datad => VCC,
	cin => \inst6|seconds[0]~7\,
	combout => \inst6|seconds[1]~8_combout\,
	cout => \inst6|seconds[1]~9\);

-- Location: FF_X1_Y21_N13
\inst6|seconds[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[1]~8_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(1));

-- Location: LCCOMB_X1_Y21_N14
\inst6|seconds[2]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[2]~10_combout\ = (\inst6|seconds\(2) & (\inst6|seconds[1]~9\ $ (GND))) # (!\inst6|seconds\(2) & (!\inst6|seconds[1]~9\ & VCC))
-- \inst6|seconds[2]~11\ = CARRY((\inst6|seconds\(2) & !\inst6|seconds[1]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(2),
	datad => VCC,
	cin => \inst6|seconds[1]~9\,
	combout => \inst6|seconds[2]~10_combout\,
	cout => \inst6|seconds[2]~11\);

-- Location: FF_X1_Y21_N15
\inst6|seconds[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[2]~10_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(2));

-- Location: LCCOMB_X1_Y21_N16
\inst6|seconds[3]~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[3]~12_combout\ = (\inst6|seconds\(3) & (!\inst6|seconds[2]~11\)) # (!\inst6|seconds\(3) & ((\inst6|seconds[2]~11\) # (GND)))
-- \inst6|seconds[3]~13\ = CARRY((!\inst6|seconds[2]~11\) # (!\inst6|seconds\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(3),
	datad => VCC,
	cin => \inst6|seconds[2]~11\,
	combout => \inst6|seconds[3]~12_combout\,
	cout => \inst6|seconds[3]~13\);

-- Location: FF_X1_Y21_N17
\inst6|seconds[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[3]~12_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(3));

-- Location: LCCOMB_X1_Y21_N18
\inst6|seconds[4]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[4]~14_combout\ = (\inst6|seconds\(4) & (\inst6|seconds[3]~13\ $ (GND))) # (!\inst6|seconds\(4) & (!\inst6|seconds[3]~13\ & VCC))
-- \inst6|seconds[4]~15\ = CARRY((\inst6|seconds\(4) & !\inst6|seconds[3]~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(4),
	datad => VCC,
	cin => \inst6|seconds[3]~13\,
	combout => \inst6|seconds[4]~14_combout\,
	cout => \inst6|seconds[4]~15\);

-- Location: FF_X1_Y21_N19
\inst6|seconds[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[4]~14_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(4));

-- Location: LCCOMB_X1_Y21_N20
\inst6|seconds[5]~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|seconds[5]~16_combout\ = \inst6|seconds[4]~15\ $ (\inst6|seconds\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst6|seconds\(5),
	cin => \inst6|seconds[4]~15\,
	combout => \inst6|seconds[5]~16_combout\);

-- Location: FF_X1_Y21_N21
\inst6|seconds[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~clkctrl_outclk\,
	d => \inst6|seconds[5]~16_combout\,
	sclr => \inst40|LessThan9~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|seconds\(5));

-- Location: LCCOMB_X1_Y21_N24
\inst40|LessThan9~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst40|LessThan9~0_combout\ = ((!\inst6|seconds\(2) & ((!\inst6|seconds\(0)) # (!\inst6|seconds\(1))))) # (!\inst6|seconds\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(1),
	datab => \inst6|seconds\(3),
	datac => \inst6|seconds\(2),
	datad => \inst6|seconds\(0),
	combout => \inst40|LessThan9~0_combout\);

-- Location: LCCOMB_X1_Y21_N2
\inst40|LessThan9~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst40|LessThan9~1_combout\ = (\inst6|seconds\(5) & (\inst6|seconds\(4) & !\inst40|LessThan9~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(5),
	datac => \inst6|seconds\(4),
	datad => \inst40|LessThan9~0_combout\,
	combout => \inst40|LessThan9~1_combout\);

-- Location: LCCOMB_X1_Y21_N28
\inst6|ripple~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst6|ripple~feeder_combout\ = \inst40|LessThan9~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst40|LessThan9~1_combout\,
	combout => \inst6|ripple~feeder_combout\);

-- Location: FF_X1_Y21_N29
\inst6|ripple\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst~combout\,
	d => \inst6|ripple~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst6|ripple~q\);

-- Location: IOIBUF_X0_Y23_N15
\INC_MIN~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_INC_MIN,
	o => \INC_MIN~input_o\);

-- Location: LCCOMB_X26_Y21_N24
inst19 : cycloneiii_lcell_comb
-- Equation(s):
-- \inst19~combout\ = LCELL((\inst6|ripple~q\) # ((!\INC_MIN~input_o\ & (\inst37|reduced~q\ & !\inst2~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|ripple~q\,
	datab => \INC_MIN~input_o\,
	datac => \inst37|reduced~q\,
	datad => \inst2~q\,
	combout => \inst19~combout\);

-- Location: CLKCTRL_G13
\inst19~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst19~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst19~clkctrl_outclk\);

-- Location: LCCOMB_X27_Y21_N0
\inst4|seconds[0]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[0]~6_combout\ = \inst4|seconds\(0) $ (VCC)
-- \inst4|seconds[0]~7\ = CARRY(\inst4|seconds\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(0),
	datad => VCC,
	combout => \inst4|seconds[0]~6_combout\,
	cout => \inst4|seconds[0]~7\);

-- Location: LCCOMB_X27_Y21_N26
\inst4|LessThan0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~2_combout\ = ((!\inst4|seconds\(2) & ((!\inst4|seconds\(0)) # (!\inst4|seconds\(1))))) # (!\inst4|seconds\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(3),
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(2),
	datad => \inst4|seconds\(0),
	combout => \inst4|LessThan0~2_combout\);

-- Location: LCCOMB_X27_Y21_N8
\inst4|seconds[4]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[4]~14_combout\ = (\inst4|seconds\(4) & (\inst4|seconds[3]~13\ $ (GND))) # (!\inst4|seconds\(4) & (!\inst4|seconds[3]~13\ & VCC))
-- \inst4|seconds[4]~15\ = CARRY((\inst4|seconds\(4) & !\inst4|seconds[3]~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(4),
	datad => VCC,
	cin => \inst4|seconds[3]~13\,
	combout => \inst4|seconds[4]~14_combout\,
	cout => \inst4|seconds[4]~15\);

-- Location: LCCOMB_X27_Y21_N10
\inst4|seconds[5]~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[5]~16_combout\ = \inst4|seconds\(5) $ (\inst4|seconds[4]~15\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	cin => \inst4|seconds[4]~15\,
	combout => \inst4|seconds[5]~16_combout\);

-- Location: FF_X27_Y21_N11
\inst4|seconds[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[5]~16_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(5));

-- Location: LCCOMB_X27_Y21_N20
\inst4|LessThan0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|LessThan0~3_combout\ = (\inst4|seconds\(4) & (!\inst4|LessThan0~2_combout\ & \inst4|seconds\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(4),
	datac => \inst4|LessThan0~2_combout\,
	datad => \inst4|seconds\(5),
	combout => \inst4|LessThan0~3_combout\);

-- Location: FF_X27_Y21_N1
\inst4|seconds[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[0]~6_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(0));

-- Location: LCCOMB_X27_Y21_N2
\inst4|seconds[1]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[1]~8_combout\ = (\inst4|seconds\(1) & (!\inst4|seconds[0]~7\)) # (!\inst4|seconds\(1) & ((\inst4|seconds[0]~7\) # (GND)))
-- \inst4|seconds[1]~9\ = CARRY((!\inst4|seconds[0]~7\) # (!\inst4|seconds\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(1),
	datad => VCC,
	cin => \inst4|seconds[0]~7\,
	combout => \inst4|seconds[1]~8_combout\,
	cout => \inst4|seconds[1]~9\);

-- Location: FF_X27_Y21_N3
\inst4|seconds[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[1]~8_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(1));

-- Location: LCCOMB_X27_Y21_N4
\inst4|seconds[2]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[2]~10_combout\ = (\inst4|seconds\(2) & (\inst4|seconds[1]~9\ $ (GND))) # (!\inst4|seconds\(2) & (!\inst4|seconds[1]~9\ & VCC))
-- \inst4|seconds[2]~11\ = CARRY((\inst4|seconds\(2) & !\inst4|seconds[1]~9\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(2),
	datad => VCC,
	cin => \inst4|seconds[1]~9\,
	combout => \inst4|seconds[2]~10_combout\,
	cout => \inst4|seconds[2]~11\);

-- Location: FF_X27_Y21_N5
\inst4|seconds[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[2]~10_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(2));

-- Location: LCCOMB_X27_Y21_N6
\inst4|seconds[3]~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|seconds[3]~12_combout\ = (\inst4|seconds\(3) & (!\inst4|seconds[2]~11\)) # (!\inst4|seconds\(3) & ((\inst4|seconds[2]~11\) # (GND)))
-- \inst4|seconds[3]~13\ = CARRY((!\inst4|seconds[2]~11\) # (!\inst4|seconds\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(3),
	datad => VCC,
	cin => \inst4|seconds[2]~11\,
	combout => \inst4|seconds[3]~12_combout\,
	cout => \inst4|seconds[3]~13\);

-- Location: FF_X27_Y21_N7
\inst4|seconds[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[3]~12_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(3));

-- Location: FF_X27_Y21_N9
\inst4|seconds[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~clkctrl_outclk\,
	d => \inst4|seconds[4]~14_combout\,
	sclr => \inst4|LessThan0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|seconds\(4));

-- Location: LCCOMB_X27_Y21_N28
\inst5|seven_seg1[1]~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[1]~9_combout\ = (\inst4|seconds\(4) & \inst4|seconds\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|seconds\(4),
	datad => \inst4|seconds\(5),
	combout => \inst5|seven_seg1[1]~9_combout\);

-- Location: LCCOMB_X27_Y21_N22
\inst4|ripple~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst4|ripple~0_combout\ = (\inst5|seven_seg1[1]~9_combout\ & (!\inst4|LessThan0~2_combout\ & ((\INC_MIN~input_o\) # (\inst2~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \INC_MIN~input_o\,
	datab => \inst5|seven_seg1[1]~9_combout\,
	datac => \inst2~q\,
	datad => \inst4|LessThan0~2_combout\,
	combout => \inst4|ripple~0_combout\);

-- Location: FF_X27_Y21_N23
\inst4|ripple\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst19~combout\,
	d => \inst4|ripple~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ripple~q\);

-- Location: LCCOMB_X26_Y21_N28
inst20 : cycloneiii_lcell_comb
-- Equation(s):
-- \inst20~combout\ = LCELL((\inst4|ripple~q\) # ((!\INC_HRS~input_o\ & (\inst37|reduced~q\ & !\inst2~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \INC_HRS~input_o\,
	datab => \inst4|ripple~q\,
	datac => \inst37|reduced~q\,
	datad => \inst2~q\,
	combout => \inst20~combout\);

-- Location: CLKCTRL_G11
\inst20~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst20~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst20~clkctrl_outclk\);

-- Location: LCCOMB_X29_Y28_N0
\inst11|number[3]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[3]~2_combout\ = (!\INC_HRS~input_o\ & !\inst2~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \INC_HRS~input_o\,
	datad => \inst2~q\,
	combout => \inst11|number[3]~2_combout\);

-- Location: LCCOMB_X29_Y28_N4
\inst11|number[3]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[3]~4_combout\ = (\inst11|number\(4) & (!\inst11|LessThan0~0_combout\ & ((\inst2~q\) # (\INC_HRS~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst2~q\,
	datab => \inst11|number\(4),
	datac => \INC_HRS~input_o\,
	datad => \inst11|LessThan0~0_combout\,
	combout => \inst11|number[3]~4_combout\);

-- Location: LCCOMB_X29_Y28_N20
\inst11|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|Add0~0_combout\ = \inst11|number\(0) $ (VCC)
-- \inst11|Add0~1\ = CARRY(\inst11|number\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(0),
	datad => VCC,
	combout => \inst11|Add0~0_combout\,
	cout => \inst11|Add0~1\);

-- Location: LCCOMB_X29_Y28_N12
\inst11|number[0]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[0]~8_combout\ = (\inst11|LessThan0~1_combout\ & ((\inst11|Add0~0_combout\) # ((\inst11|number[3]~4_combout\ & \inst11|number\(0))))) # (!\inst11|LessThan0~1_combout\ & (\inst11|number[3]~4_combout\ & (\inst11|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|LessThan0~1_combout\,
	datab => \inst11|number[3]~4_combout\,
	datac => \inst11|number\(0),
	datad => \inst11|Add0~0_combout\,
	combout => \inst11|number[0]~8_combout\);

-- Location: FF_X29_Y28_N13
\inst11|number[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst20~clkctrl_outclk\,
	d => \inst11|number[0]~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst11|number\(0));

-- Location: LCCOMB_X29_Y28_N6
\inst11|number[1]~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[1]~7_combout\ = (\inst11|number\(1) & (\inst11|number\(4) & ((\inst2~q\) # (\INC_HRS~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst2~q\,
	datab => \INC_HRS~input_o\,
	datac => \inst11|number\(1),
	datad => \inst11|number\(4),
	combout => \inst11|number[1]~7_combout\);

-- Location: LCCOMB_X29_Y28_N22
\inst11|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|Add0~2_combout\ = (\inst11|number\(1) & (!\inst11|Add0~1\)) # (!\inst11|number\(1) & ((\inst11|Add0~1\) # (GND)))
-- \inst11|Add0~3\ = CARRY((!\inst11|Add0~1\) # (!\inst11|number\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(1),
	datad => VCC,
	cin => \inst11|Add0~1\,
	combout => \inst11|Add0~2_combout\,
	cout => \inst11|Add0~3\);

-- Location: LCCOMB_X29_Y28_N14
\inst11|number[1]~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[1]~9_combout\ = (\inst11|LessThan0~0_combout\ & (((\inst11|Add0~2_combout\)))) # (!\inst11|LessThan0~0_combout\ & ((\inst11|number[1]~7_combout\) # ((\inst11|Add0~2_combout\ & !\inst11|number\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number[1]~7_combout\,
	datab => \inst11|LessThan0~0_combout\,
	datac => \inst11|Add0~2_combout\,
	datad => \inst11|number\(4),
	combout => \inst11|number[1]~9_combout\);

-- Location: FF_X29_Y28_N15
\inst11|number[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst20~clkctrl_outclk\,
	d => \inst11|number[1]~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst11|number\(1));

-- Location: LCCOMB_X29_Y28_N24
\inst11|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|Add0~4_combout\ = (\inst11|number\(2) & (\inst11|Add0~3\ $ (GND))) # (!\inst11|number\(2) & (!\inst11|Add0~3\ & VCC))
-- \inst11|Add0~5\ = CARRY((\inst11|number\(2) & !\inst11|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(2),
	datad => VCC,
	cin => \inst11|Add0~3\,
	combout => \inst11|Add0~4_combout\,
	cout => \inst11|Add0~5\);

-- Location: LCCOMB_X29_Y28_N2
\inst11|number[2]~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[2]~5_combout\ = (\inst11|LessThan0~1_combout\ & ((\inst11|Add0~4_combout\) # ((\inst11|number[3]~4_combout\ & \inst11|number\(2))))) # (!\inst11|LessThan0~1_combout\ & (\inst11|number[3]~4_combout\ & (\inst11|number\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|LessThan0~1_combout\,
	datab => \inst11|number[3]~4_combout\,
	datac => \inst11|number\(2),
	datad => \inst11|Add0~4_combout\,
	combout => \inst11|number[2]~5_combout\);

-- Location: FF_X29_Y28_N3
\inst11|number[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst20~clkctrl_outclk\,
	d => \inst11|number[2]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst11|number\(2));

-- Location: LCCOMB_X29_Y28_N18
\inst11|LessThan0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|LessThan0~0_combout\ = (!\inst11|number\(3) & (((!\inst11|number\(2)) # (!\inst11|number\(1))) # (!\inst11|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(0),
	datab => \inst11|number\(1),
	datac => \inst11|number\(3),
	datad => \inst11|number\(2),
	combout => \inst11|LessThan0~0_combout\);

-- Location: LCCOMB_X29_Y28_N26
\inst11|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|Add0~6_combout\ = (\inst11|number\(3) & (!\inst11|Add0~5\)) # (!\inst11|number\(3) & ((\inst11|Add0~5\) # (GND)))
-- \inst11|Add0~7\ = CARRY((!\inst11|Add0~5\) # (!\inst11|number\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(3),
	datad => VCC,
	cin => \inst11|Add0~5\,
	combout => \inst11|Add0~6_combout\,
	cout => \inst11|Add0~7\);

-- Location: LCCOMB_X29_Y28_N28
\inst11|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|Add0~8_combout\ = \inst11|Add0~7\ $ (!\inst11|number\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \inst11|number\(4),
	cin => \inst11|Add0~7\,
	combout => \inst11|Add0~8_combout\);

-- Location: LCCOMB_X29_Y28_N16
\inst11|number[4]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[4]~3_combout\ = (\inst11|LessThan0~0_combout\ & (((\inst11|Add0~8_combout\)))) # (!\inst11|LessThan0~0_combout\ & ((\inst11|number\(4) & (!\inst11|number[3]~2_combout\)) # (!\inst11|number\(4) & ((\inst11|Add0~8_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number[3]~2_combout\,
	datab => \inst11|LessThan0~0_combout\,
	datac => \inst11|number\(4),
	datad => \inst11|Add0~8_combout\,
	combout => \inst11|number[4]~3_combout\);

-- Location: FF_X29_Y28_N17
\inst11|number[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst20~clkctrl_outclk\,
	d => \inst11|number[4]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst11|number\(4));

-- Location: LCCOMB_X29_Y28_N30
\inst11|LessThan0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|LessThan0~1_combout\ = (\inst11|LessThan0~0_combout\) # (!\inst11|number\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(4),
	datad => \inst11|LessThan0~0_combout\,
	combout => \inst11|LessThan0~1_combout\);

-- Location: LCCOMB_X29_Y28_N8
\inst11|number[3]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst11|number[3]~6_combout\ = (\inst11|LessThan0~1_combout\ & ((\inst11|Add0~6_combout\) # ((\inst11|number[3]~4_combout\ & \inst11|number\(3))))) # (!\inst11|LessThan0~1_combout\ & (\inst11|number[3]~4_combout\ & (\inst11|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|LessThan0~1_combout\,
	datab => \inst11|number[3]~4_combout\,
	datac => \inst11|number\(3),
	datad => \inst11|Add0~6_combout\,
	combout => \inst11|number[3]~6_combout\);

-- Location: FF_X29_Y28_N9
\inst11|number[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst20~clkctrl_outclk\,
	d => \inst11|number[3]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst11|number\(3));

-- Location: LCCOMB_X30_Y27_N16
\inst9|comb~51\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~51_combout\ = (!\inst11|number\(3) & (\inst11|number\(2) & \inst11|number\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(3),
	datab => \inst11|number\(2),
	datad => \inst11|number\(4),
	combout => \inst9|comb~51_combout\);

-- Location: LCCOMB_X30_Y27_N30
\inst9|comb~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~52_combout\ = ((!\inst11|number\(3) & !\inst11|number\(2))) # (!\inst11|number\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(3),
	datab => \inst11|number\(2),
	datad => \inst11|number\(4),
	combout => \inst9|comb~52_combout\);

-- Location: LCCOMB_X30_Y27_N0
\inst9|seven_seg1[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(6) = (!\inst9|comb~51_combout\ & ((\inst9|comb~52_combout\) # (\inst9|seven_seg1\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~51_combout\,
	datac => \inst9|comb~52_combout\,
	datad => \inst9|seven_seg1\(6),
	combout => \inst9|seven_seg1\(6));

-- Location: LCCOMB_X28_Y26_N18
\inst8|x~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|x~0_combout\ = !\inst8|x~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst8|x~q\,
	combout => \inst8|x~0_combout\);

-- Location: FF_X28_Y26_N19
\inst8|x\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|x~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|x~q\);

-- Location: LCCOMB_X29_Y26_N4
\inst13|_output~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~0_combout\ = (\inst9|seven_seg1\(6)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg1\(6),
	datad => \inst8|x~q\,
	combout => \inst13|_output~0_combout\);

-- Location: FF_X29_Y26_N5
\inst13|_output[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(6));

-- Location: LCCOMB_X30_Y27_N18
\inst9|seven_seg1[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(4) = (!\inst9|comb~51_combout\ & ((\inst9|comb~52_combout\) # (\inst9|seven_seg1\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~51_combout\,
	datac => \inst9|comb~52_combout\,
	datad => \inst9|seven_seg1\(4),
	combout => \inst9|seven_seg1\(4));

-- Location: LCCOMB_X28_Y26_N16
\inst13|_output~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~1_combout\ = (\inst9|seven_seg1\(4)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg1\(4),
	datad => \inst8|x~q\,
	combout => \inst13|_output~1_combout\);

-- Location: FF_X28_Y26_N17
\inst13|_output[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(4));

-- Location: LCCOMB_X30_Y27_N28
\inst9|seven_seg1[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(3) = (!\inst9|comb~51_combout\ & ((\inst9|comb~52_combout\) # (\inst9|seven_seg1\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~51_combout\,
	datac => \inst9|comb~52_combout\,
	datad => \inst9|seven_seg1\(3),
	combout => \inst9|seven_seg1\(3));

-- Location: LCCOMB_X29_Y26_N2
\inst13|_output~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~2_combout\ = (\inst9|seven_seg1\(3)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg1\(3),
	datad => \inst8|x~q\,
	combout => \inst13|_output~2_combout\);

-- Location: FF_X29_Y26_N3
\inst13|_output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(3));

-- Location: LCCOMB_X30_Y28_N18
\inst9|comb~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~54_combout\ = (\inst11|number\(2) & (((!\inst11|number\(3))))) # (!\inst11|number\(2) & (!\inst11|number\(4) & ((!\inst11|number\(1)) # (!\inst11|number\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(3),
	datac => \inst11|number\(1),
	datad => \inst11|number\(2),
	combout => \inst9|comb~54_combout\);

-- Location: LCCOMB_X30_Y28_N8
\inst9|comb~53\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~53_combout\ = (\inst11|number\(4) & (!\inst11|number\(3) & ((!\inst11|number\(2))))) # (!\inst11|number\(4) & (\inst11|number\(3) & ((\inst11|number\(1)) # (\inst11|number\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(3),
	datac => \inst11|number\(1),
	datad => \inst11|number\(2),
	combout => \inst9|comb~53_combout\);

-- Location: LCCOMB_X30_Y28_N12
\inst9|seven_seg1[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(2) = (!\inst9|comb~53_combout\ & ((\inst9|comb~54_combout\) # (\inst9|seven_seg1\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~54_combout\,
	datac => \inst9|comb~53_combout\,
	datad => \inst9|seven_seg1\(2),
	combout => \inst9|seven_seg1\(2));

-- Location: LCCOMB_X28_Y26_N14
\inst13|_output~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~3_combout\ = (\inst9|seven_seg1\(2)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg1\(2),
	datad => \inst8|x~q\,
	combout => \inst13|_output~3_combout\);

-- Location: FF_X28_Y26_N15
\inst13|_output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(2));

-- Location: LCCOMB_X30_Y28_N28
\inst9|comb~65\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~65_combout\ = (\inst11|number\(4) & (!\inst11|number\(3))) # (!\inst11|number\(4) & (\inst11|number\(3) & ((\inst11|number\(1)) # (\inst11|number\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(3),
	datac => \inst11|number\(1),
	datad => \inst11|number\(2),
	combout => \inst9|comb~65_combout\);

-- Location: LCCOMB_X30_Y28_N14
\inst9|comb~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~66_combout\ = (!\inst11|number\(4) & (((!\inst11|number\(1) & !\inst11|number\(2))) # (!\inst11|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(3),
	datac => \inst11|number\(1),
	datad => \inst11|number\(2),
	combout => \inst9|comb~66_combout\);

-- Location: LCCOMB_X30_Y28_N10
\inst9|seven_seg1[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(1) = (!\inst9|comb~65_combout\ & ((\inst9|comb~66_combout\) # (\inst9|seven_seg1\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~65_combout\,
	datac => \inst9|comb~66_combout\,
	datad => \inst9|seven_seg1\(1),
	combout => \inst9|seven_seg1\(1));

-- Location: LCCOMB_X28_Y26_N12
\inst13|_output~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~4_combout\ = (\inst9|seven_seg1\(1)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg1\(1),
	datad => \inst8|x~q\,
	combout => \inst13|_output~4_combout\);

-- Location: FF_X28_Y26_N13
\inst13|_output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(1));

-- Location: LCCOMB_X30_Y27_N2
\inst9|seven_seg1[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg1\(0) = (!\inst9|comb~51_combout\ & ((\inst9|comb~52_combout\) # (\inst9|seven_seg1\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~51_combout\,
	datac => \inst9|comb~52_combout\,
	datad => \inst9|seven_seg1\(0),
	combout => \inst9|seven_seg1\(0));

-- Location: LCCOMB_X28_Y26_N6
\inst13|_output~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst13|_output~5_combout\ = (\inst9|seven_seg1\(0)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|seven_seg1\(0),
	datad => \inst8|x~q\,
	combout => \inst13|_output~5_combout\);

-- Location: FF_X28_Y26_N7
\inst13|_output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst13|_output~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst13|_output\(0));

-- Location: LCCOMB_X32_Y28_N4
\inst9|seven_seg0[1]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[1]~2_combout\ = (!\inst11|number\(1) & !\inst11|number\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datad => \inst11|number\(3),
	combout => \inst9|seven_seg0[1]~2_combout\);

-- Location: LCCOMB_X31_Y28_N10
\inst9|seven_seg0[6]~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[6]~13_combout\ = (\inst9|seven_seg0[1]~2_combout\ & ((\inst11|number\(4) & ((\inst11|number\(0)) # (\inst11|number\(2)))) # (!\inst11|number\(4) & ((!\inst11|number\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(0),
	datab => \inst11|number\(4),
	datac => \inst11|number\(2),
	datad => \inst9|seven_seg0[1]~2_combout\,
	combout => \inst9|seven_seg0[6]~13_combout\);

-- Location: LCCOMB_X31_Y28_N24
\inst9|seven_seg0[6]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[6]~3_combout\ = (\inst11|number\(3) & (!\inst11|number\(2))) # (!\inst11|number\(3) & (\inst11|number\(2) & \inst11|number\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(3),
	datac => \inst11|number\(2),
	datad => \inst11|number\(0),
	combout => \inst9|seven_seg0[6]~3_combout\);

-- Location: LCCOMB_X31_Y28_N6
\inst9|seven_seg0[6]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[6]~4_combout\ = (\inst9|seven_seg0[6]~13_combout\) # ((!\inst11|number\(4) & (\inst11|number\(1) & \inst9|seven_seg0[6]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|seven_seg0[6]~13_combout\,
	datab => \inst11|number\(4),
	datac => \inst11|number\(1),
	datad => \inst9|seven_seg0[6]~3_combout\,
	combout => \inst9|seven_seg0[6]~4_combout\);

-- Location: LCCOMB_X30_Y28_N22
\inst9|comb~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~56_combout\ = (\inst9|seven_seg0[6]~4_combout\ & ((!\inst11|number\(3)) # (!\inst11|number\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[6]~4_combout\,
	combout => \inst9|comb~56_combout\);

-- Location: LCCOMB_X30_Y28_N4
\inst9|comb~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~55_combout\ = (!\inst9|seven_seg0[6]~4_combout\ & ((!\inst11|number\(3)) # (!\inst11|number\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[6]~4_combout\,
	combout => \inst9|comb~55_combout\);

-- Location: LCCOMB_X30_Y28_N16
\inst9|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(6) = (!\inst9|comb~55_combout\ & ((\inst9|comb~56_combout\) # (\inst9|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|comb~56_combout\,
	datac => \inst9|comb~55_combout\,
	datad => \inst9|seven_seg0\(6),
	combout => \inst9|seven_seg0\(6));

-- Location: LCCOMB_X28_Y26_N4
\inst12|_output~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~0_combout\ = (\inst9|seven_seg0\(6)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg0\(6),
	datad => \inst8|x~q\,
	combout => \inst12|_output~0_combout\);

-- Location: FF_X28_Y26_N5
\inst12|_output[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(6));

-- Location: LCCOMB_X31_Y28_N12
\inst9|seven_seg0[5]~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[5]~5_combout\ = (\inst11|number\(1) & ((\inst11|number\(2) & (!\inst11|number\(3) & \inst11|number\(0))) # (!\inst11|number\(2) & ((\inst11|number\(0)) # (!\inst11|number\(3)))))) # (!\inst11|number\(1) & ((\inst11|number\(2) & 
-- (\inst11|number\(3))) # (!\inst11|number\(2) & (!\inst11|number\(3) & \inst11|number\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110101101000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(2),
	datac => \inst11|number\(3),
	datad => \inst11|number\(0),
	combout => \inst9|seven_seg0[5]~5_combout\);

-- Location: LCCOMB_X30_Y28_N24
\inst9|seven_seg0[5]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[5]~6_combout\ = (\inst11|number\(3)) # ((\inst11|number\(1) & ((\inst11|number\(2)))) # (!\inst11|number\(1) & (\inst11|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(0),
	datac => \inst11|number\(3),
	datad => \inst11|number\(2),
	combout => \inst9|seven_seg0[5]~6_combout\);

-- Location: LCCOMB_X31_Y28_N16
\inst9|comb~57\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~57_combout\ = (\inst11|number\(4) & (((!\inst9|seven_seg0[5]~6_combout\ & !\inst11|number\(3))))) # (!\inst11|number\(4) & (!\inst9|seven_seg0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|seven_seg0[5]~5_combout\,
	datab => \inst9|seven_seg0[5]~6_combout\,
	datac => \inst11|number\(3),
	datad => \inst11|number\(4),
	combout => \inst9|comb~57_combout\);

-- Location: LCCOMB_X31_Y28_N14
\inst9|comb~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~58_combout\ = (\inst11|number\(4) & (((\inst9|seven_seg0[5]~6_combout\ & !\inst11|number\(3))))) # (!\inst11|number\(4) & (\inst9|seven_seg0[5]~5_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|seven_seg0[5]~5_combout\,
	datab => \inst9|seven_seg0[5]~6_combout\,
	datac => \inst11|number\(3),
	datad => \inst11|number\(4),
	combout => \inst9|comb~58_combout\);

-- Location: LCCOMB_X31_Y28_N20
\inst9|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(5) = (!\inst9|comb~57_combout\ & ((\inst9|comb~58_combout\) # (\inst9|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~57_combout\,
	datac => \inst9|comb~58_combout\,
	datad => \inst9|seven_seg0\(5),
	combout => \inst9|seven_seg0\(5));

-- Location: LCCOMB_X29_Y26_N0
\inst12|_output~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~1_combout\ = (\inst9|seven_seg0\(5)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg0\(5),
	datad => \inst8|x~q\,
	combout => \inst12|_output~1_combout\);

-- Location: FF_X29_Y26_N1
\inst12|_output[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(5));

-- Location: LCCOMB_X32_Y28_N0
\inst9|comb~70\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~70_combout\ = (\inst11|number\(0) & (((!\inst11|number\(3))))) # (!\inst11|number\(0) & (\inst11|number\(2) & (\inst11|number\(1) $ (!\inst11|number\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100011110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(2),
	datac => \inst11|number\(0),
	datad => \inst11|number\(3),
	combout => \inst9|comb~70_combout\);

-- Location: LCCOMB_X32_Y28_N10
\inst9|comb~71\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~71_combout\ = (\inst11|number\(4) & (((\inst11|number\(0) & \inst9|comb~70_combout\)))) # (!\inst11|number\(4) & ((\inst11|number\(0)) # ((\inst11|number\(2) & \inst9|comb~70_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(2),
	datac => \inst11|number\(0),
	datad => \inst9|comb~70_combout\,
	combout => \inst9|comb~71_combout\);

-- Location: LCCOMB_X32_Y28_N24
\inst9|comb~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~46_combout\ = (\inst11|number\(4) & (((!\inst11|number\(3))))) # (!\inst11|number\(4) & ((\inst11|number\(1) $ (\inst11|number\(3))) # (!\inst11|number\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(2),
	datac => \inst11|number\(4),
	datad => \inst11|number\(3),
	combout => \inst9|comb~46_combout\);

-- Location: LCCOMB_X32_Y28_N22
\inst9|comb~69\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~69_combout\ = (!\inst11|number\(0) & \inst9|comb~46_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst11|number\(0),
	datad => \inst9|comb~46_combout\,
	combout => \inst9|comb~69_combout\);

-- Location: LCCOMB_X32_Y28_N12
\inst9|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(4) = (!\inst9|comb~69_combout\ & ((\inst9|comb~71_combout\) # (\inst9|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|comb~71_combout\,
	datac => \inst9|comb~69_combout\,
	datad => \inst9|seven_seg0\(4),
	combout => \inst9|seven_seg0\(4));

-- Location: LCCOMB_X28_Y26_N22
\inst12|_output~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~2_combout\ = (\inst9|seven_seg0\(4)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|seven_seg0\(4),
	datad => \inst8|x~q\,
	combout => \inst12|_output~2_combout\);

-- Location: FF_X28_Y26_N23
\inst12|_output[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(4));

-- Location: LCCOMB_X32_Y28_N28
\inst9|seven_seg0[3]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[3]~8_combout\ = (\inst11|number\(2) & (\inst11|number\(0) $ (\inst11|number\(3)))) # (!\inst11|number\(2) & (\inst11|number\(0) & \inst11|number\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(2),
	datac => \inst11|number\(0),
	datad => \inst11|number\(3),
	combout => \inst9|seven_seg0[3]~8_combout\);

-- Location: LCCOMB_X32_Y28_N2
\inst9|seven_seg0[3]~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[3]~7_combout\ = (\inst9|seven_seg0[1]~2_combout\ & (\inst11|number\(0) $ (((!\inst11|number\(4) & \inst11|number\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst11|number\(2),
	datac => \inst11|number\(0),
	datad => \inst9|seven_seg0[1]~2_combout\,
	combout => \inst9|seven_seg0[3]~7_combout\);

-- Location: LCCOMB_X32_Y28_N6
\inst9|seven_seg0[3]~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[3]~9_combout\ = (\inst9|seven_seg0[3]~7_combout\) # ((!\inst11|number\(4) & (\inst9|seven_seg0[3]~8_combout\ & \inst11|number\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(4),
	datab => \inst9|seven_seg0[3]~8_combout\,
	datac => \inst11|number\(1),
	datad => \inst9|seven_seg0[3]~7_combout\,
	combout => \inst9|seven_seg0[3]~9_combout\);

-- Location: LCCOMB_X31_Y28_N28
\inst9|comb~59\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~59_combout\ = (!\inst9|seven_seg0[3]~9_combout\ & ((!\inst11|number\(3)) # (!\inst11|number\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[3]~9_combout\,
	combout => \inst9|comb~59_combout\);

-- Location: LCCOMB_X31_Y28_N22
\inst9|comb~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~60_combout\ = (\inst9|seven_seg0[3]~9_combout\ & ((!\inst11|number\(3)) # (!\inst11|number\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[3]~9_combout\,
	combout => \inst9|comb~60_combout\);

-- Location: LCCOMB_X31_Y28_N18
\inst9|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(3) = (!\inst9|comb~59_combout\ & ((\inst9|comb~60_combout\) # (\inst9|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~59_combout\,
	datac => \inst9|comb~60_combout\,
	datad => \inst9|seven_seg0\(3),
	combout => \inst9|seven_seg0\(3));

-- Location: LCCOMB_X28_Y26_N28
\inst12|_output~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~3_combout\ = (\inst9|seven_seg0\(3)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|seven_seg0\(3),
	datad => \inst8|x~q\,
	combout => \inst12|_output~3_combout\);

-- Location: FF_X28_Y26_N29
\inst12|_output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(3));

-- Location: LCCOMB_X30_Y28_N0
\inst9|seven_seg0[2]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[2]~14_combout\ = (!\inst11|number\(0) & ((\inst11|number\(1) & (\inst11|number\(4) $ (!\inst11|number\(2)))) # (!\inst11|number\(1) & (!\inst11|number\(4) & \inst11|number\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(0),
	datac => \inst11|number\(4),
	datad => \inst11|number\(2),
	combout => \inst9|seven_seg0[2]~14_combout\);

-- Location: LCCOMB_X30_Y28_N30
\inst9|seven_seg0[2]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[2]~15_combout\ = (\inst9|seven_seg0[2]~14_combout\ & ((\inst11|number\(1) & (!\inst11|number\(3))) # (!\inst11|number\(1) & (\inst11|number\(3) & \inst11|number\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst9|seven_seg0[2]~14_combout\,
	datac => \inst11|number\(3),
	datad => \inst11|number\(2),
	combout => \inst9|seven_seg0[2]~15_combout\);

-- Location: LCCOMB_X30_Y27_N26
\inst9|comb~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~62_combout\ = (\inst9|seven_seg0[2]~15_combout\ & ((!\inst11|number\(4)) # (!\inst11|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(3),
	datac => \inst9|seven_seg0[2]~15_combout\,
	datad => \inst11|number\(4),
	combout => \inst9|comb~62_combout\);

-- Location: LCCOMB_X30_Y27_N24
\inst9|comb~61\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~61_combout\ = (!\inst9|seven_seg0[2]~15_combout\ & ((!\inst11|number\(4)) # (!\inst11|number\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(3),
	datac => \inst9|seven_seg0[2]~15_combout\,
	datad => \inst11|number\(4),
	combout => \inst9|comb~61_combout\);

-- Location: LCCOMB_X30_Y27_N20
\inst9|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(2) = (!\inst9|comb~61_combout\ & ((\inst9|comb~62_combout\) # (\inst9|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|comb~62_combout\,
	datab => \inst9|comb~61_combout\,
	datad => \inst9|seven_seg0\(2),
	combout => \inst9|seven_seg0\(2));

-- Location: LCCOMB_X29_Y26_N22
\inst12|_output~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~4_combout\ = (\inst9|seven_seg0\(2)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|seven_seg0\(2),
	datad => \inst8|x~q\,
	combout => \inst12|_output~4_combout\);

-- Location: FF_X29_Y26_N23
\inst12|_output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(2));

-- Location: LCCOMB_X30_Y28_N2
\inst9|seven_seg0[1]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[1]~10_combout\ = (\inst11|number\(1) & ((\inst11|number\(4)) # (\inst11|number\(3) $ (!\inst11|number\(0))))) # (!\inst11|number\(1) & ((\inst11|number\(3) & (\inst11|number\(4))) # (!\inst11|number\(3) & ((\inst11|number\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100111100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(3),
	datac => \inst11|number\(4),
	datad => \inst11|number\(0),
	combout => \inst9|seven_seg0[1]~10_combout\);

-- Location: LCCOMB_X30_Y28_N20
\inst9|comb~67\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~67_combout\ = (\inst11|number\(4) & (!\inst11|number\(3) & ((\inst11|number\(2)) # (\inst9|seven_seg0[1]~10_combout\)))) # (!\inst11|number\(4) & (((!\inst9|seven_seg0[1]~10_combout\)) # (!\inst11|number\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011010100101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(2),
	datab => \inst11|number\(3),
	datac => \inst11|number\(4),
	datad => \inst9|seven_seg0[1]~10_combout\,
	combout => \inst9|comb~67_combout\);

-- Location: LCCOMB_X30_Y28_N26
\inst9|comb~68\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~68_combout\ = (\inst11|number\(2) & (((!\inst11|number\(4) & \inst9|seven_seg0[1]~10_combout\)))) # (!\inst11|number\(2) & (!\inst11|number\(3) & (\inst11|number\(4) & !\inst9|seven_seg0[1]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(2),
	datab => \inst11|number\(3),
	datac => \inst11|number\(4),
	datad => \inst9|seven_seg0[1]~10_combout\,
	combout => \inst9|comb~68_combout\);

-- Location: LCCOMB_X30_Y28_N6
\inst9|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(1) = (!\inst9|comb~67_combout\ & ((\inst9|comb~68_combout\) # (\inst9|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|comb~67_combout\,
	datac => \inst9|comb~68_combout\,
	datad => \inst9|seven_seg0\(1),
	combout => \inst9|seven_seg0\(1));

-- Location: LCCOMB_X29_Y26_N12
\inst12|_output~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~5_combout\ = (\inst9|seven_seg0\(1)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst9|seven_seg0\(1),
	datad => \inst8|x~q\,
	combout => \inst12|_output~5_combout\);

-- Location: FF_X29_Y26_N13
\inst12|_output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(1));

-- Location: LCCOMB_X31_Y28_N4
\inst9|seven_seg0[0]~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[0]~12_combout\ = (!\inst11|number\(1) & (\inst11|number\(2) & (!\inst11|number\(3) & \inst11|number\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(2),
	datac => \inst11|number\(3),
	datad => \inst11|number\(0),
	combout => \inst9|seven_seg0[0]~12_combout\);

-- Location: LCCOMB_X31_Y28_N2
\inst9|seven_seg0[0]~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0[0]~11_combout\ = (\inst11|number\(1) & (\inst11|number\(3) & (\inst11|number\(2) $ (\inst11|number\(0))))) # (!\inst11|number\(1) & (!\inst11|number\(3) & (\inst11|number\(2) $ (\inst11|number\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst11|number\(1),
	datab => \inst11|number\(2),
	datac => \inst11|number\(3),
	datad => \inst11|number\(0),
	combout => \inst9|seven_seg0[0]~11_combout\);

-- Location: LCCOMB_X31_Y28_N26
\inst9|comb~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~64_combout\ = (\inst11|number\(4) & (\inst9|seven_seg0[0]~12_combout\ & (!\inst11|number\(3)))) # (!\inst11|number\(4) & (((\inst9|seven_seg0[0]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|seven_seg0[0]~12_combout\,
	datab => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[0]~11_combout\,
	combout => \inst9|comb~64_combout\);

-- Location: LCCOMB_X31_Y28_N8
\inst9|comb~63\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|comb~63_combout\ = (\inst11|number\(4) & (!\inst9|seven_seg0[0]~12_combout\ & (!\inst11|number\(3)))) # (!\inst11|number\(4) & (((!\inst9|seven_seg0[0]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|seven_seg0[0]~12_combout\,
	datab => \inst11|number\(4),
	datac => \inst11|number\(3),
	datad => \inst9|seven_seg0[0]~11_combout\,
	combout => \inst9|comb~63_combout\);

-- Location: LCCOMB_X31_Y28_N0
\inst9|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst9|seven_seg0\(0) = (!\inst9|comb~63_combout\ & ((\inst9|comb~64_combout\) # (\inst9|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst9|comb~64_combout\,
	datac => \inst9|comb~63_combout\,
	datad => \inst9|seven_seg0\(0),
	combout => \inst9|seven_seg0\(0));

-- Location: LCCOMB_X28_Y26_N30
\inst12|_output~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst12|_output~6_combout\ = (\inst9|seven_seg0\(0)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst9|seven_seg0\(0),
	datad => \inst8|x~q\,
	combout => \inst12|_output~6_combout\);

-- Location: FF_X28_Y26_N31
\inst12|_output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst12|_output~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst12|_output\(0));

-- Location: LCCOMB_X26_Y23_N24
\inst5|seven_seg0[6]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~4_combout\ = (((!\inst4|seconds\(3)) # (!\inst4|seconds\(2))) # (!\inst4|seconds\(5))) # (!\inst4|seconds\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst4|seconds\(5),
	datac => \inst4|seconds\(2),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[6]~4_combout\);

-- Location: LCCOMB_X27_Y25_N0
\inst5|seven_seg1[6]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[6]~0_combout\ = (\inst4|seconds\(5)) # ((\inst4|seconds\(4) & ((\inst4|seconds\(2)) # (\inst4|seconds\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(4),
	datad => \inst4|seconds\(5),
	combout => \inst5|seven_seg1[6]~0_combout\);

-- Location: LCCOMB_X27_Y26_N28
\inst5|comb~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~1_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg1[6]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg1[6]~0_combout\,
	combout => \inst5|comb~1_combout\);

-- Location: LCCOMB_X27_Y26_N14
\inst5|comb~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~0_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg1[6]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg1[6]~0_combout\,
	combout => \inst5|comb~0_combout\);

-- Location: LCCOMB_X27_Y26_N10
\inst5|seven_seg1[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(6) = (!\inst5|comb~0_combout\ & ((\inst5|comb~1_combout\) # (\inst5|seven_seg1\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~1_combout\,
	datac => \inst5|comb~0_combout\,
	datad => \inst5|seven_seg1\(6),
	combout => \inst5|seven_seg1\(6));

-- Location: LCCOMB_X27_Y26_N0
\inst10|_output~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~0_combout\ = (\inst5|seven_seg1\(6)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg1\(6),
	datad => \inst8|x~q\,
	combout => \inst10|_output~0_combout\);

-- Location: FF_X27_Y26_N1
\inst10|_output[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(6));

-- Location: LCCOMB_X26_Y25_N0
\inst5|seven_seg1[5]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[5]~1_combout\ = (\inst4|seconds\(4)) # ((\inst4|seconds\(3) & ((\inst4|seconds\(2)) # (\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg1[5]~1_combout\);

-- Location: LCCOMB_X26_Y25_N30
\inst5|seven_seg1[5]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[5]~2_combout\ = (\inst4|seconds\(5) & (((!\inst4|seconds\(3) & !\inst4|seconds\(4))))) # (!\inst4|seconds\(5) & (\inst5|seven_seg1[5]~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst5|seven_seg1[5]~1_combout\,
	datac => \inst4|seconds\(3),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg1[5]~2_combout\);

-- Location: LCCOMB_X26_Y26_N26
\inst5|comb~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~3_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg1[5]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg1[5]~2_combout\,
	combout => \inst5|comb~3_combout\);

-- Location: LCCOMB_X26_Y26_N4
\inst5|comb~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~2_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg1[5]~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg1[5]~2_combout\,
	combout => \inst5|comb~2_combout\);

-- Location: LCCOMB_X26_Y26_N24
\inst5|seven_seg1[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(5) = (!\inst5|comb~2_combout\ & ((\inst5|comb~3_combout\) # (\inst5|seven_seg1\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~3_combout\,
	datac => \inst5|comb~2_combout\,
	datad => \inst5|seven_seg1\(5),
	combout => \inst5|seven_seg1\(5));

-- Location: LCCOMB_X28_Y26_N0
\inst10|_output~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~1_combout\ = (\inst5|seven_seg1\(5)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg1\(5),
	datad => \inst8|x~q\,
	combout => \inst10|_output~1_combout\);

-- Location: FF_X28_Y26_N1
\inst10|_output[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(5));

-- Location: LCCOMB_X26_Y23_N6
\inst5|seven_seg1[4]~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[4]~5_combout\ = (\inst4|seconds\(4) & (!\inst4|seconds\(3) & (!\inst4|seconds\(2)))) # (!\inst4|seconds\(4) & (\inst4|seconds\(3) & ((\inst4|seconds\(2)) # (\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011001000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(2),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg1[4]~5_combout\);

-- Location: LCCOMB_X26_Y23_N18
\inst5|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Equal0~0_combout\ = (\inst4|seconds\(4) & (\inst4|seconds\(0) & (!\inst4|seconds\(5) & !\inst4|seconds\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst4|seconds\(0),
	datac => \inst4|seconds\(5),
	datad => \inst4|seconds\(1),
	combout => \inst5|Equal0~0_combout\);

-- Location: LCCOMB_X26_Y23_N0
\inst5|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Equal0~1_combout\ = (\inst4|seconds\(3) & (\inst4|seconds\(2) & \inst5|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(2),
	datad => \inst5|Equal0~0_combout\,
	combout => \inst5|Equal0~1_combout\);

-- Location: LCCOMB_X26_Y23_N30
\inst5|seven_seg1[2]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[2]~3_combout\ = (\inst4|seconds\(4) & (\inst4|seconds\(3) & \inst4|seconds\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(2),
	combout => \inst5|seven_seg1[2]~3_combout\);

-- Location: LCCOMB_X26_Y23_N4
\inst5|seven_seg1[2]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[2]~4_combout\ = (\inst4|seconds\(5)) # ((\inst5|seven_seg1[2]~3_combout\ & ((\inst4|seconds\(0)) # (\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg1[2]~3_combout\,
	datab => \inst4|seconds\(0),
	datac => \inst4|seconds\(5),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg1[2]~4_combout\);

-- Location: LCCOMB_X26_Y23_N20
\inst5|comb~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~4_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst5|seven_seg1[2]~4_combout\ & ((\inst5|Equal0~1_combout\))) # (!\inst5|seven_seg1[2]~4_combout\ & (!\inst5|seven_seg1[4]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg1[4]~5_combout\,
	datab => \inst5|Equal0~1_combout\,
	datac => \inst5|seven_seg1[2]~4_combout\,
	datad => \inst5|seven_seg0[6]~4_combout\,
	combout => \inst5|comb~4_combout\);

-- Location: LCCOMB_X26_Y23_N22
\inst5|comb~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~5_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst5|seven_seg1[2]~4_combout\ & ((!\inst5|Equal0~1_combout\))) # (!\inst5|seven_seg1[2]~4_combout\ & (\inst5|seven_seg1[4]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg1[4]~5_combout\,
	datab => \inst5|Equal0~1_combout\,
	datac => \inst5|seven_seg1[2]~4_combout\,
	datad => \inst5|seven_seg0[6]~4_combout\,
	combout => \inst5|comb~5_combout\);

-- Location: LCCOMB_X26_Y23_N10
\inst5|seven_seg1[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(4) = (!\inst5|comb~4_combout\ & ((\inst5|comb~5_combout\) # (\inst5|seven_seg1\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~4_combout\,
	datac => \inst5|comb~5_combout\,
	datad => \inst5|seven_seg1\(4),
	combout => \inst5|seven_seg1\(4));

-- Location: LCCOMB_X27_Y26_N22
\inst10|_output~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~2_combout\ = (\inst5|seven_seg1\(4)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg1\(4),
	datad => \inst8|x~q\,
	combout => \inst10|_output~2_combout\);

-- Location: FF_X27_Y26_N23
\inst10|_output[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(4));

-- Location: LCCOMB_X27_Y21_N16
\inst5|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Equal0~2_combout\ = (!\inst4|seconds\(2) & !\inst4|seconds\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|seconds\(2),
	datad => \inst4|seconds\(3),
	combout => \inst5|Equal0~2_combout\);

-- Location: LCCOMB_X27_Y23_N26
\inst5|seven_seg1[3]~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[3]~7_combout\ = (\inst5|Equal0~2_combout\ & (\inst4|seconds\(4) & ((!\inst4|seconds\(1)) # (!\inst4|seconds\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst5|Equal0~2_combout\,
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg1[3]~7_combout\);

-- Location: LCCOMB_X27_Y23_N28
\inst5|seven_seg1[3]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[3]~6_combout\ = (\inst4|seconds\(3) & ((\inst4|seconds\(5)) # ((\inst4|seconds\(2)) # (\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(2),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg1[3]~6_combout\);

-- Location: LCCOMB_X27_Y23_N22
\inst5|comb~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~7_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst5|seven_seg1[3]~7_combout\) # ((!\inst4|seconds\(4) & \inst5|seven_seg1[3]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst5|seven_seg0[6]~4_combout\,
	datac => \inst5|seven_seg1[3]~7_combout\,
	datad => \inst5|seven_seg1[3]~6_combout\,
	combout => \inst5|comb~7_combout\);

-- Location: LCCOMB_X27_Y23_N4
\inst5|comb~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~6_combout\ = (\inst5|seven_seg0[6]~4_combout\ & (!\inst5|seven_seg1[3]~7_combout\ & ((\inst4|seconds\(4)) # (!\inst5|seven_seg1[3]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst5|seven_seg0[6]~4_combout\,
	datac => \inst5|seven_seg1[3]~7_combout\,
	datad => \inst5|seven_seg1[3]~6_combout\,
	combout => \inst5|comb~6_combout\);

-- Location: LCCOMB_X27_Y23_N10
\inst5|seven_seg1[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(3) = (!\inst5|comb~6_combout\ & ((\inst5|comb~7_combout\) # (\inst5|seven_seg1\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~7_combout\,
	datac => \inst5|comb~6_combout\,
	datad => \inst5|seven_seg1\(3),
	combout => \inst5|seven_seg1\(3));

-- Location: LCCOMB_X27_Y26_N24
\inst10|_output~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~3_combout\ = (\inst5|seven_seg1\(3)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg1\(3),
	datad => \inst8|x~q\,
	combout => \inst10|_output~3_combout\);

-- Location: FF_X27_Y26_N25
\inst10|_output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(3));

-- Location: LCCOMB_X26_Y23_N16
\inst5|seven_seg1[2]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[2]~8_combout\ = (\inst5|seven_seg1[2]~4_combout\ & (((\inst5|Equal0~1_combout\)))) # (!\inst5|seven_seg1[2]~4_combout\ & (\inst4|seconds\(4) & ((!\inst5|Equal0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst5|Equal0~1_combout\,
	datac => \inst5|seven_seg1[2]~4_combout\,
	datad => \inst5|Equal0~2_combout\,
	combout => \inst5|seven_seg1[2]~8_combout\);

-- Location: LCCOMB_X26_Y23_N12
\inst5|comb~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~9_combout\ = (\inst5|seven_seg1[2]~8_combout\ & \inst5|seven_seg0[6]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg1[2]~8_combout\,
	datad => \inst5|seven_seg0[6]~4_combout\,
	combout => \inst5|comb~9_combout\);

-- Location: LCCOMB_X26_Y23_N26
\inst5|comb~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~8_combout\ = (!\inst5|seven_seg1[2]~8_combout\ & \inst5|seven_seg0[6]~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg1[2]~8_combout\,
	datad => \inst5|seven_seg0[6]~4_combout\,
	combout => \inst5|comb~8_combout\);

-- Location: LCCOMB_X26_Y23_N28
\inst5|seven_seg1[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(2) = (!\inst5|comb~8_combout\ & ((\inst5|comb~9_combout\) # (\inst5|seven_seg1\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~9_combout\,
	datac => \inst5|comb~8_combout\,
	datad => \inst5|seven_seg1\(2),
	combout => \inst5|seven_seg1\(2));

-- Location: LCCOMB_X27_Y26_N6
\inst10|_output~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~4_combout\ = (\inst5|seven_seg1\(2)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg1\(2),
	datad => \inst8|x~q\,
	combout => \inst10|_output~4_combout\);

-- Location: FF_X27_Y26_N7
\inst10|_output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(2));

-- Location: LCCOMB_X27_Y21_N24
\inst5|Equal0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|Equal0~3_combout\ = (\inst4|seconds\(0) & (\inst4|seconds\(5) & (!\inst4|seconds\(1) & \inst4|seconds\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(0),
	datab => \inst4|seconds\(5),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(4),
	combout => \inst5|Equal0~3_combout\);

-- Location: LCCOMB_X27_Y21_N30
\inst5|seven_seg1[2]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1[2]~10_combout\ = (!\inst4|seconds\(1) & !\inst4|seconds\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(0),
	combout => \inst5|seven_seg1[2]~10_combout\);

-- Location: LCCOMB_X27_Y21_N18
\inst5|comb~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~10_combout\ = (\inst5|seven_seg1[1]~9_combout\ & (((!\inst5|Equal0~3_combout\ & !\inst5|seven_seg1[2]~10_combout\)) # (!\inst5|Equal0~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|Equal0~3_combout\,
	datab => \inst5|seven_seg1[1]~9_combout\,
	datac => \inst5|seven_seg1[2]~10_combout\,
	datad => \inst5|Equal0~2_combout\,
	combout => \inst5|comb~10_combout\);

-- Location: LCCOMB_X28_Y25_N0
\inst5|comb~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~11_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|comb~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|comb~10_combout\,
	combout => \inst5|comb~11_combout\);

-- Location: LCCOMB_X28_Y25_N26
\inst5|comb~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~12_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|comb~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|comb~10_combout\,
	combout => \inst5|comb~12_combout\);

-- Location: LCCOMB_X28_Y25_N20
\inst5|seven_seg1[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(1) = (!\inst5|comb~11_combout\ & ((\inst5|comb~12_combout\) # (\inst5|seven_seg1\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~11_combout\,
	datac => \inst5|comb~12_combout\,
	datad => \inst5|seven_seg1\(1),
	combout => \inst5|seven_seg1\(1));

-- Location: LCCOMB_X27_Y26_N12
\inst10|_output~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~5_combout\ = (\inst5|seven_seg1\(1)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg1\(1),
	datad => \inst8|x~q\,
	combout => \inst10|_output~5_combout\);

-- Location: FF_X27_Y26_N13
\inst10|_output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(1));

-- Location: LCCOMB_X27_Y23_N20
\inst5|seven_seg1[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg1\(0) = (!\inst5|comb~6_combout\ & ((\inst5|comb~7_combout\) # (\inst5|seven_seg1\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~7_combout\,
	datac => \inst5|comb~6_combout\,
	datad => \inst5|seven_seg1\(0),
	combout => \inst5|seven_seg1\(0));

-- Location: LCCOMB_X28_Y26_N26
\inst10|_output~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst10|_output~6_combout\ = (\inst5|seven_seg1\(0)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg1\(0),
	datad => \inst8|x~q\,
	combout => \inst10|_output~6_combout\);

-- Location: FF_X28_Y26_N27
\inst10|_output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst10|_output~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst10|_output\(0));

-- Location: LCCOMB_X27_Y23_N2
\inst5|seven_seg0[6]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~10_combout\ = (\inst4|seconds\(5) & ((\inst4|seconds\(3) & (\inst4|seconds\(1) $ (\inst4|seconds\(4)))) # (!\inst4|seconds\(3) & ((\inst4|seconds\(1)) # (!\inst4|seconds\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[6]~10_combout\);

-- Location: LCCOMB_X27_Y23_N24
\inst5|seven_seg0[6]~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~9_combout\ = (\inst4|seconds\(5) & (\inst4|seconds\(4) & ((\inst4|seconds\(1)) # (!\inst4|seconds\(3))))) # (!\inst4|seconds\(5) & (\inst4|seconds\(3) $ ((\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[6]~9_combout\);

-- Location: LCCOMB_X27_Y23_N30
\inst5|seven_seg0[6]~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~12_combout\ = (\inst5|seven_seg0[6]~10_combout\ & ((\inst4|seconds\(2) & (\inst4|seconds\(4))) # (!\inst4|seconds\(2) & ((!\inst5|seven_seg0[6]~9_combout\))))) # (!\inst5|seven_seg0[6]~10_combout\ & (\inst4|seconds\(4) & 
-- ((\inst5|seven_seg0[6]~9_combout\) # (!\inst4|seconds\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst5|seven_seg0[6]~10_combout\,
	datac => \inst4|seconds\(2),
	datad => \inst5|seven_seg0[6]~9_combout\,
	combout => \inst5|seven_seg0[6]~12_combout\);

-- Location: LCCOMB_X27_Y23_N16
\inst5|seven_seg0[6]~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~11_combout\ = (\inst4|seconds\(4) & ((\inst5|seven_seg0[6]~10_combout\ & (\inst4|seconds\(2))) # (!\inst5|seven_seg0[6]~10_combout\ & ((\inst5|seven_seg0[6]~9_combout\))))) # (!\inst4|seconds\(4) & ((\inst4|seconds\(2)) # 
-- (\inst5|seven_seg0[6]~10_combout\ $ (\inst5|seven_seg0[6]~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst5|seven_seg0[6]~10_combout\,
	datac => \inst4|seconds\(2),
	datad => \inst5|seven_seg0[6]~9_combout\,
	combout => \inst5|seven_seg0[6]~11_combout\);

-- Location: LCCOMB_X27_Y25_N20
\inst5|seven_seg0[6]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~6_combout\ = \inst4|seconds\(4) $ (\inst4|seconds\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|seconds\(4),
	datad => \inst4|seconds\(2),
	combout => \inst5|seven_seg0[6]~6_combout\);

-- Location: LCCOMB_X27_Y25_N26
\inst5|seven_seg0[6]~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~5_combout\ = (\inst4|seconds\(0) & (\inst4|seconds\(1) $ (!\inst4|seconds\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[6]~5_combout\);

-- Location: LCCOMB_X27_Y25_N6
\inst5|seven_seg0[1]~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[1]~7_combout\ = (\inst4|seconds\(1) & (\inst4|seconds\(0) & !\inst4|seconds\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[1]~7_combout\);

-- Location: LCCOMB_X27_Y25_N4
\inst5|seven_seg0[6]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~8_combout\ = (\inst4|seconds\(5) & (((\inst5|seven_seg0[6]~5_combout\)))) # (!\inst4|seconds\(5) & (\inst5|seven_seg0[6]~6_combout\ & ((\inst5|seven_seg0[1]~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst5|seven_seg0[6]~6_combout\,
	datac => \inst5|seven_seg0[6]~5_combout\,
	datad => \inst5|seven_seg0[1]~7_combout\,
	combout => \inst5|seven_seg0[6]~8_combout\);

-- Location: LCCOMB_X27_Y23_N12
\inst5|seven_seg0[6]~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~13_combout\ = (\inst5|seven_seg0[6]~12_combout\ & (!\inst5|seven_seg0[6]~11_combout\ & (\inst4|seconds\(0)))) # (!\inst5|seven_seg0[6]~12_combout\ & (((\inst5|seven_seg0[6]~8_combout\)) # (!\inst5|seven_seg0[6]~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~12_combout\,
	datab => \inst5|seven_seg0[6]~11_combout\,
	datac => \inst4|seconds\(0),
	datad => \inst5|seven_seg0[6]~8_combout\,
	combout => \inst5|seven_seg0[6]~13_combout\);

-- Location: LCCOMB_X27_Y23_N0
\inst5|comb~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~14_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg0[6]~13_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[6]~13_combout\,
	combout => \inst5|comb~14_combout\);

-- Location: LCCOMB_X27_Y23_N18
\inst5|comb~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~13_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg0[6]~13_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[6]~13_combout\,
	combout => \inst5|comb~13_combout\);

-- Location: LCCOMB_X27_Y24_N24
\inst5|seven_seg0[6]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(6) = (!\inst5|comb~13_combout\ & ((\inst5|comb~14_combout\) # (\inst5|seven_seg0\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~14_combout\,
	datac => \inst5|comb~13_combout\,
	datad => \inst5|seven_seg0\(6),
	combout => \inst5|seven_seg0\(6));

-- Location: LCCOMB_X27_Y26_N2
\inst8|_output~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~0_combout\ = (\inst5|seven_seg0\(6)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0\(6),
	datad => \inst8|x~q\,
	combout => \inst8|_output~0_combout\);

-- Location: FF_X27_Y26_N3
\inst8|_output[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(6));

-- Location: LCCOMB_X27_Y25_N12
\inst5|seven_seg0[6]~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[6]~16_combout\ = (\inst5|seven_seg0[1]~7_combout\) # ((!\inst4|seconds\(2) & (\inst5|Equal0~3_combout\ & \inst4|seconds\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst5|Equal0~3_combout\,
	datac => \inst4|seconds\(3),
	datad => \inst5|seven_seg0[1]~7_combout\,
	combout => \inst5|seven_seg0[6]~16_combout\);

-- Location: LCCOMB_X27_Y25_N18
\inst5|seven_seg0[5]~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[5]~17_combout\ = (\inst4|seconds\(2) & (\inst5|seven_seg0[6]~5_combout\)) # (!\inst4|seconds\(2) & ((\inst5|seven_seg0[6]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datac => \inst5|seven_seg0[6]~5_combout\,
	datad => \inst5|seven_seg0[6]~16_combout\,
	combout => \inst5|seven_seg0[5]~17_combout\);

-- Location: LCCOMB_X26_Y25_N8
\inst5|seven_seg0[5]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[5]~15_combout\ = (\inst4|seconds\(3) & ((\inst4|seconds\(4)) # ((!\inst4|seconds\(0) & !\inst4|seconds\(1))))) # (!\inst4|seconds\(3) & (((\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101110001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(4),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg0[5]~15_combout\);

-- Location: LCCOMB_X27_Y25_N8
\inst5|seven_seg0[5]~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[5]~18_combout\ = (\inst4|seconds\(2) & ((\inst4|seconds\(4) & ((!\inst5|seven_seg0[5]~15_combout\))) # (!\inst4|seconds\(4) & (\inst5|seven_seg0[5]~17_combout\)))) # (!\inst4|seconds\(2) & ((\inst4|seconds\(4) & 
-- (\inst5|seven_seg0[5]~17_combout\)) # (!\inst4|seconds\(4) & ((!\inst5|seven_seg0[5]~15_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100011101101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst5|seven_seg0[5]~17_combout\,
	datac => \inst4|seconds\(4),
	datad => \inst5|seven_seg0[5]~15_combout\,
	combout => \inst5|seven_seg0[5]~18_combout\);

-- Location: LCCOMB_X27_Y25_N2
\inst5|seven_seg0[5]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[5]~14_combout\ = (\inst4|seconds\(1) & (!\inst4|seconds\(3) & ((\inst4|seconds\(0)) # (!\inst5|seven_seg0[6]~6_combout\)))) # (!\inst4|seconds\(1) & (((\inst4|seconds\(3) & \inst5|seven_seg0[6]~6_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011100000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(0),
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(3),
	datad => \inst5|seven_seg0[6]~6_combout\,
	combout => \inst5|seven_seg0[5]~14_combout\);

-- Location: LCCOMB_X27_Y25_N14
\inst5|seven_seg0[5]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[5]~36_combout\ = (\inst4|seconds\(2) & ((\inst5|seven_seg0[5]~14_combout\) # ((\inst4|seconds\(4) & \inst5|seven_seg0[6]~5_combout\)))) # (!\inst4|seconds\(2) & ((\inst5|seven_seg0[6]~5_combout\) # ((!\inst4|seconds\(4) & 
-- \inst5|seven_seg0[5]~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(4),
	datac => \inst5|seven_seg0[6]~5_combout\,
	datad => \inst5|seven_seg0[5]~14_combout\,
	combout => \inst5|seven_seg0[5]~36_combout\);

-- Location: LCCOMB_X27_Y26_N26
\inst5|comb~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~15_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst4|seconds\(5) & (!\inst5|seven_seg0[5]~18_combout\)) # (!\inst4|seconds\(5) & ((!\inst5|seven_seg0[5]~36_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datab => \inst5|seven_seg0[5]~18_combout\,
	datac => \inst4|seconds\(5),
	datad => \inst5|seven_seg0[5]~36_combout\,
	combout => \inst5|comb~15_combout\);

-- Location: LCCOMB_X27_Y26_N4
\inst5|comb~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~16_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst4|seconds\(5) & (\inst5|seven_seg0[5]~18_combout\)) # (!\inst4|seconds\(5) & ((\inst5|seven_seg0[5]~36_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datab => \inst5|seven_seg0[5]~18_combout\,
	datac => \inst4|seconds\(5),
	datad => \inst5|seven_seg0[5]~36_combout\,
	combout => \inst5|comb~16_combout\);

-- Location: LCCOMB_X27_Y26_N20
\inst5|seven_seg0[5]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(5) = (!\inst5|comb~15_combout\ & ((\inst5|comb~16_combout\) # (\inst5|seven_seg0\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~15_combout\,
	datac => \inst5|comb~16_combout\,
	datad => \inst5|seven_seg0\(5),
	combout => \inst5|seven_seg0\(5));

-- Location: LCCOMB_X28_Y26_N8
\inst8|_output~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~1_combout\ = (\inst5|seven_seg0\(5)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0\(5),
	datad => \inst8|x~q\,
	combout => \inst8|_output~1_combout\);

-- Location: FF_X28_Y26_N9
\inst8|_output[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(5));

-- Location: LCCOMB_X26_Y25_N22
\inst5|seven_seg0[4]~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[4]~19_combout\ = (\inst4|seconds\(5) & (\inst4|seconds\(1) & (!\inst4|seconds\(3) & !\inst4|seconds\(4)))) # (!\inst4|seconds\(5) & (!\inst4|seconds\(1) & (\inst4|seconds\(3) & \inst4|seconds\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(3),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[4]~19_combout\);

-- Location: LCCOMB_X26_Y25_N28
\inst5|seven_seg0[4]~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[4]~20_combout\ = (\inst4|seconds\(5) & ((\inst4|seconds\(1) & ((!\inst4|seconds\(4)))) # (!\inst4|seconds\(1) & (!\inst4|seconds\(3))))) # (!\inst4|seconds\(5) & ((\inst4|seconds\(4)) # (\inst4|seconds\(1) $ (\inst4|seconds\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011110011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(1),
	datac => \inst4|seconds\(3),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[4]~20_combout\);

-- Location: LCCOMB_X26_Y25_N2
\inst5|seven_seg0[4]~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[4]~21_combout\ = (\inst4|seconds\(0)) # ((\inst4|seconds\(2) & ((!\inst5|seven_seg0[4]~20_combout\))) # (!\inst4|seconds\(2) & (\inst5|seven_seg0[4]~19_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(0),
	datac => \inst5|seven_seg0[4]~19_combout\,
	datad => \inst5|seven_seg0[4]~20_combout\,
	combout => \inst5|seven_seg0[4]~21_combout\);

-- Location: LCCOMB_X26_Y26_N0
\inst5|comb~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~17_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg0[4]~21_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[4]~21_combout\,
	combout => \inst5|comb~17_combout\);

-- Location: LCCOMB_X26_Y26_N22
\inst5|comb~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~18_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg0[4]~21_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[4]~21_combout\,
	combout => \inst5|comb~18_combout\);

-- Location: LCCOMB_X26_Y26_N10
\inst5|seven_seg0[4]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(4) = (!\inst5|comb~17_combout\ & ((\inst5|comb~18_combout\) # (\inst5|seven_seg0\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~17_combout\,
	datac => \inst5|comb~18_combout\,
	datad => \inst5|seven_seg0\(4),
	combout => \inst5|seven_seg0\(4));

-- Location: LCCOMB_X28_Y26_N10
\inst8|_output~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~2_combout\ = (\inst5|seven_seg0\(4)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0\(4),
	datad => \inst8|x~q\,
	combout => \inst8|_output~2_combout\);

-- Location: FF_X28_Y26_N11
\inst8|_output[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(4));

-- Location: LCCOMB_X27_Y25_N10
\inst5|seven_seg0[3]~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[3]~22_combout\ = (\inst4|seconds\(2) & (!\inst4|seconds\(4) & (\inst4|seconds\(1) $ (!\inst4|seconds\(3))))) # (!\inst4|seconds\(2) & ((\inst4|seconds\(4)) # ((!\inst4|seconds\(1) & \inst4|seconds\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010101000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(4),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[3]~22_combout\);

-- Location: LCCOMB_X27_Y25_N16
\inst5|seven_seg0[3]~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[3]~23_combout\ = (\inst4|seconds\(2) & ((\inst4|seconds\(4) & (\inst4|seconds\(1) & !\inst4|seconds\(3))) # (!\inst4|seconds\(4) & (!\inst4|seconds\(1) & \inst4|seconds\(3))))) # (!\inst4|seconds\(2) & ((\inst4|seconds\(4)) # 
-- ((\inst4|seconds\(1) & !\inst4|seconds\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100011011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(4),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[3]~23_combout\);

-- Location: LCCOMB_X27_Y25_N22
\inst5|seven_seg0[3]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[3]~24_combout\ = (\inst5|seven_seg0[3]~22_combout\ & ((\inst5|seven_seg0[3]~23_combout\ & ((\inst5|seven_seg0[6]~16_combout\))) # (!\inst5|seven_seg0[3]~23_combout\ & (\inst4|seconds\(0))))) # (!\inst5|seven_seg0[3]~22_combout\ & 
-- (\inst5|seven_seg0[3]~23_combout\ & (!\inst4|seconds\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110000100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[3]~22_combout\,
	datab => \inst5|seven_seg0[3]~23_combout\,
	datac => \inst4|seconds\(0),
	datad => \inst5|seven_seg0[6]~16_combout\,
	combout => \inst5|seven_seg0[3]~24_combout\);

-- Location: LCCOMB_X27_Y25_N28
\inst5|seven_seg0[3]~25\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[3]~25_combout\ = (\inst4|seconds\(0) & ((\inst4|seconds\(4) & (\inst4|seconds\(1) $ (!\inst4|seconds\(3)))) # (!\inst4|seconds\(4) & (\inst4|seconds\(1) & !\inst4|seconds\(3))))) # (!\inst4|seconds\(0) & ((\inst4|seconds\(4) & 
-- (!\inst4|seconds\(1) & \inst4|seconds\(3))) # (!\inst4|seconds\(4) & (\inst4|seconds\(1) $ (!\inst4|seconds\(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010000101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(0),
	datab => \inst4|seconds\(4),
	datac => \inst4|seconds\(1),
	datad => \inst4|seconds\(3),
	combout => \inst5|seven_seg0[3]~25_combout\);

-- Location: LCCOMB_X27_Y25_N30
\inst5|seven_seg0[3]~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[3]~26_combout\ = (!\inst4|seconds\(5) & ((\inst5|seven_seg0[6]~6_combout\ & ((\inst5|seven_seg0[3]~25_combout\))) # (!\inst5|seven_seg0[6]~6_combout\ & (\inst5|seven_seg0[6]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst5|seven_seg0[6]~6_combout\,
	datac => \inst5|seven_seg0[6]~5_combout\,
	datad => \inst5|seven_seg0[3]~25_combout\,
	combout => \inst5|seven_seg0[3]~26_combout\);

-- Location: LCCOMB_X27_Y26_N16
\inst5|comb~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~20_combout\ = (\inst5|seven_seg0[6]~4_combout\ & ((\inst5|seven_seg0[3]~26_combout\) # ((\inst5|seven_seg0[3]~24_combout\ & \inst4|seconds\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datab => \inst5|seven_seg0[3]~24_combout\,
	datac => \inst4|seconds\(5),
	datad => \inst5|seven_seg0[3]~26_combout\,
	combout => \inst5|comb~20_combout\);

-- Location: LCCOMB_X27_Y26_N30
\inst5|comb~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~19_combout\ = (\inst5|seven_seg0[6]~4_combout\ & (!\inst5|seven_seg0[3]~26_combout\ & ((!\inst4|seconds\(5)) # (!\inst5|seven_seg0[3]~24_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datab => \inst5|seven_seg0[3]~24_combout\,
	datac => \inst4|seconds\(5),
	datad => \inst5|seven_seg0[3]~26_combout\,
	combout => \inst5|comb~19_combout\);

-- Location: LCCOMB_X27_Y26_N18
\inst5|seven_seg0[3]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(3) = (!\inst5|comb~19_combout\ & ((\inst5|comb~20_combout\) # (\inst5|seven_seg0\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|comb~20_combout\,
	datac => \inst5|comb~19_combout\,
	datad => \inst5|seven_seg0\(3),
	combout => \inst5|seven_seg0\(3));

-- Location: LCCOMB_X28_Y26_N24
\inst8|_output~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~3_combout\ = (\inst5|seven_seg0\(3)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0\(3),
	datad => \inst8|x~q\,
	combout => \inst8|_output~3_combout\);

-- Location: FF_X28_Y26_N25
\inst8|_output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(3));

-- Location: LCCOMB_X26_Y25_N10
\inst5|seven_seg0[2]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[2]~28_combout\ = (\inst4|seconds\(2) & (\inst4|seconds\(3) & (!\inst4|seconds\(5) & !\inst4|seconds\(1)))) # (!\inst4|seconds\(2) & ((\inst4|seconds\(3) & (\inst4|seconds\(5) & \inst4|seconds\(1))) # (!\inst4|seconds\(3) & 
-- (\inst4|seconds\(5) $ (\inst4|seconds\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(5),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg0[2]~28_combout\);

-- Location: LCCOMB_X26_Y25_N12
\inst5|seven_seg0[2]~27\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[2]~27_combout\ = (\inst4|seconds\(2) & (!\inst4|seconds\(3) & (\inst4|seconds\(5) $ (\inst4|seconds\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(5),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg0[2]~27_combout\);

-- Location: LCCOMB_X26_Y25_N4
\inst5|seven_seg0[2]~29\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[2]~29_combout\ = (!\inst4|seconds\(0) & ((\inst4|seconds\(4) & ((\inst5|seven_seg0[2]~27_combout\))) # (!\inst4|seconds\(4) & (\inst5|seven_seg0[2]~28_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[2]~28_combout\,
	datab => \inst4|seconds\(0),
	datac => \inst5|seven_seg0[2]~27_combout\,
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[2]~29_combout\);

-- Location: LCCOMB_X28_Y25_N22
\inst5|comb~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~22_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg0[2]~29_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[2]~29_combout\,
	combout => \inst5|comb~22_combout\);

-- Location: LCCOMB_X28_Y25_N4
\inst5|comb~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~21_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg0[2]~29_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[2]~29_combout\,
	combout => \inst5|comb~21_combout\);

-- Location: LCCOMB_X28_Y25_N18
\inst5|seven_seg0[2]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(2) = (!\inst5|comb~21_combout\ & ((\inst5|comb~22_combout\) # (\inst5|seven_seg0\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~22_combout\,
	datac => \inst5|comb~21_combout\,
	datad => \inst5|seven_seg0\(2),
	combout => \inst5|seven_seg0\(2));

-- Location: LCCOMB_X28_Y26_N2
\inst8|_output~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~4_combout\ = (\inst5|seven_seg0\(2)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0\(2),
	datad => \inst8|x~q\,
	combout => \inst8|_output~4_combout\);

-- Location: FF_X28_Y26_N3
\inst8|_output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(2));

-- Location: LCCOMB_X26_Y25_N18
\inst5|seven_seg0[1]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[1]~30_combout\ = (\inst4|seconds\(5) & ((\inst4|seconds\(3) & (\inst4|seconds\(0) $ (\inst4|seconds\(1)))) # (!\inst4|seconds\(3) & (!\inst4|seconds\(0) & !\inst4|seconds\(1))))) # (!\inst4|seconds\(5) & ((\inst4|seconds\(3) & 
-- (\inst4|seconds\(0) & \inst4|seconds\(1))) # (!\inst4|seconds\(3) & (\inst4|seconds\(0) $ (\inst4|seconds\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100110010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg0[1]~30_combout\);

-- Location: LCCOMB_X26_Y25_N20
\inst5|seven_seg0[1]~31\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[1]~31_combout\ = (\inst4|seconds\(5) & (((\inst4|seconds\(0)) # (\inst4|seconds\(1))) # (!\inst4|seconds\(3)))) # (!\inst4|seconds\(5) & ((\inst4|seconds\(3) & (\inst4|seconds\(0) $ (!\inst4|seconds\(1)))) # (!\inst4|seconds\(3) & 
-- ((\inst4|seconds\(0)) # (\inst4|seconds\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst4|seconds\(3),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(1),
	combout => \inst5|seven_seg0[1]~31_combout\);

-- Location: LCCOMB_X26_Y25_N16
\inst5|seven_seg0[1]~37\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[1]~37_combout\ = (\inst4|seconds\(2) & (\inst5|seven_seg0[1]~30_combout\ & ((!\inst4|seconds\(4))))) # (!\inst4|seconds\(2) & (((!\inst5|seven_seg0[1]~31_combout\ & \inst4|seconds\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst5|seven_seg0[1]~30_combout\,
	datac => \inst5|seven_seg0[1]~31_combout\,
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[1]~37_combout\);

-- Location: LCCOMB_X27_Y25_N24
\inst5|seven_seg0[1]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[1]~32_combout\ = (\inst5|seven_seg0[1]~37_combout\) # ((\inst4|seconds\(5) & (!\inst5|seven_seg0[6]~6_combout\ & \inst5|seven_seg0[1]~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(5),
	datab => \inst5|seven_seg0[6]~6_combout\,
	datac => \inst5|seven_seg0[1]~37_combout\,
	datad => \inst5|seven_seg0[1]~7_combout\,
	combout => \inst5|seven_seg0[1]~32_combout\);

-- Location: LCCOMB_X28_Y25_N10
\inst5|comb~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~24_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg0[1]~32_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[1]~32_combout\,
	combout => \inst5|comb~24_combout\);

-- Location: LCCOMB_X28_Y25_N8
\inst5|comb~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~23_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg0[1]~32_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[1]~32_combout\,
	combout => \inst5|comb~23_combout\);

-- Location: LCCOMB_X28_Y25_N12
\inst5|seven_seg0[1]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(1) = (!\inst5|comb~23_combout\ & ((\inst5|comb~24_combout\) # (\inst5|seven_seg0\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~24_combout\,
	datac => \inst5|comb~23_combout\,
	datad => \inst5|seven_seg0\(1),
	combout => \inst5|seven_seg0\(1));

-- Location: LCCOMB_X27_Y26_N8
\inst8|_output~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~5_combout\ = (\inst5|seven_seg0\(1)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0\(1),
	datad => \inst8|x~q\,
	combout => \inst8|_output~5_combout\);

-- Location: FF_X27_Y26_N9
\inst8|_output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(1));

-- Location: LCCOMB_X26_Y25_N26
\inst5|seven_seg0[0]~33\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[0]~33_combout\ = (\inst4|seconds\(4) & (!\inst4|seconds\(5) & (\inst4|seconds\(2) $ (!\inst4|seconds\(0))))) # (!\inst4|seconds\(4) & (\inst4|seconds\(2) $ (((\inst4|seconds\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(5),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[0]~33_combout\);

-- Location: LCCOMB_X26_Y25_N24
\inst5|seven_seg0[0]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[0]~34_combout\ = (\inst4|seconds\(2) & (\inst4|seconds\(5) & (!\inst4|seconds\(0)))) # (!\inst4|seconds\(2) & (\inst4|seconds\(5) $ (((!\inst4|seconds\(0) & \inst4|seconds\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(2),
	datab => \inst4|seconds\(5),
	datac => \inst4|seconds\(0),
	datad => \inst4|seconds\(4),
	combout => \inst5|seven_seg0[0]~34_combout\);

-- Location: LCCOMB_X26_Y25_N14
\inst5|seven_seg0[0]~35\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0[0]~35_combout\ = (\inst4|seconds\(1) & ((\inst4|seconds\(3) & (\inst5|seven_seg0[0]~33_combout\ & !\inst5|seven_seg0[0]~34_combout\)) # (!\inst4|seconds\(3) & (!\inst5|seven_seg0[0]~33_combout\ & \inst5|seven_seg0[0]~34_combout\)))) # 
-- (!\inst4|seconds\(1) & (\inst5|seven_seg0[0]~33_combout\ & (\inst4|seconds\(3) $ (!\inst5|seven_seg0[0]~34_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001010010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|seconds\(1),
	datab => \inst4|seconds\(3),
	datac => \inst5|seven_seg0[0]~33_combout\,
	datad => \inst5|seven_seg0[0]~34_combout\,
	combout => \inst5|seven_seg0[0]~35_combout\);

-- Location: LCCOMB_X26_Y26_N6
\inst5|comb~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~26_combout\ = (\inst5|seven_seg0[6]~4_combout\ & \inst5|seven_seg0[0]~35_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[0]~35_combout\,
	combout => \inst5|comb~26_combout\);

-- Location: LCCOMB_X26_Y26_N8
\inst5|comb~25\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|comb~25_combout\ = (\inst5|seven_seg0[6]~4_combout\ & !\inst5|seven_seg0[0]~35_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst5|seven_seg0[6]~4_combout\,
	datad => \inst5|seven_seg0[0]~35_combout\,
	combout => \inst5|comb~25_combout\);

-- Location: LCCOMB_X26_Y26_N12
\inst5|seven_seg0[0]\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst5|seven_seg0\(0) = (!\inst5|comb~25_combout\ & ((\inst5|comb~26_combout\) # (\inst5|seven_seg0\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst5|comb~26_combout\,
	datac => \inst5|comb~25_combout\,
	datad => \inst5|seven_seg0\(0),
	combout => \inst5|seven_seg0\(0));

-- Location: LCCOMB_X28_Y26_N20
\inst8|_output~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst8|_output~6_combout\ = (\inst5|seven_seg0\(0)) # (\inst8|x~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst5|seven_seg0\(0),
	datad => \inst8|x~q\,
	combout => \inst8|_output~6_combout\);

-- Location: FF_X28_Y26_N21
\inst8|_output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst1|reduced~clkctrl_outclk\,
	d => \inst8|_output~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst8|_output\(0));

-- Location: IOIBUF_X0_Y25_N1
\LED_DISPLAY~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_LED_DISPLAY,
	o => \LED_DISPLAY~input_o\);

-- Location: LCCOMB_X1_Y21_N22
\inst3|LPM_MUX_component|auto_generated|result_node[1]~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\ = (!\inst6|seconds\(3) & ((!\inst6|seconds\(1)) # (!\inst6|seconds\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(2),
	datab => \inst6|seconds\(1),
	datac => \inst6|seconds\(3),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\);

-- Location: LCCOMB_X1_Y24_N10
\inst3|LPM_MUX_component|auto_generated|result_node[9]~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[9]~22_combout\ = (\LED_DISPLAY~input_o\ & ((\inst6|seconds\(5)) # ((\inst6|seconds\(4)) # (!\inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LED_DISPLAY~input_o\,
	datab => \inst6|seconds\(5),
	datac => \inst6|seconds\(4),
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[9]~22_combout\);

-- Location: LCCOMB_X1_Y24_N12
\inst3|LPM_MUX_component|auto_generated|result_node[8]~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[8]~10_combout\ = (!\inst6|seconds\(4) & !\inst6|seconds\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(5),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[8]~10_combout\);

-- Location: LCCOMB_X1_Y24_N22
\inst3|LPM_MUX_component|auto_generated|result_node[8]~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[8]~12_combout\ = (\LED_DISPLAY~input_o\ & (((\inst6|seconds\(3) & \inst6|seconds\(2))) # (!\inst3|LPM_MUX_component|auto_generated|result_node[8]~10_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LED_DISPLAY~input_o\,
	datab => \inst6|seconds\(3),
	datac => \inst6|seconds\(2),
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[8]~10_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[8]~12_combout\);

-- Location: LCCOMB_X1_Y24_N20
\inst3|LPM_MUX_component|auto_generated|result_node[7]~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[7]~13_combout\ = (\inst6|seconds\(4) & ((\inst6|seconds\(1)) # ((\inst6|seconds\(2)) # (\inst6|seconds\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(1),
	datab => \inst6|seconds\(2),
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(3),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[7]~13_combout\);

-- Location: LCCOMB_X1_Y24_N2
\inst3|LPM_MUX_component|auto_generated|result_node[7]~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[7]~14_combout\ = (\LED_DISPLAY~input_o\ & ((\inst6|seconds\(5)) # (\inst3|LPM_MUX_component|auto_generated|result_node[7]~13_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(5),
	datac => \LED_DISPLAY~input_o\,
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[7]~13_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[7]~14_combout\);

-- Location: LCCOMB_X1_Y24_N4
\inst3|LPM_MUX_component|auto_generated|result_node[6]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[6]~15_combout\ = (\LED_DISPLAY~input_o\ & ((\inst6|seconds\(5)) # ((\inst6|seconds\(3) & \inst6|seconds\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LED_DISPLAY~input_o\,
	datab => \inst6|seconds\(3),
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(5),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[6]~15_combout\);

-- Location: LCCOMB_X1_Y24_N18
\inst3|LPM_MUX_component|auto_generated|result_node[5]~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[5]~16_combout\ = (\inst6|seconds\(1) & (\inst6|seconds\(2) & (\inst6|seconds\(4) & \inst6|seconds\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(1),
	datab => \inst6|seconds\(2),
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(3),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[5]~16_combout\);

-- Location: LCCOMB_X1_Y24_N16
\inst3|LPM_MUX_component|auto_generated|result_node[5]~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[5]~17_combout\ = (\LED_DISPLAY~input_o\ & ((\inst6|seconds\(5)) # (\inst3|LPM_MUX_component|auto_generated|result_node[5]~16_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(5),
	datac => \LED_DISPLAY~input_o\,
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[5]~16_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[5]~17_combout\);

-- Location: LCCOMB_X1_Y24_N6
\inst3|LPM_MUX_component|auto_generated|result_node[4]~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[4]~18_combout\ = (\LED_DISPLAY~input_o\ & \inst6|seconds\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \LED_DISPLAY~input_o\,
	datad => \inst6|seconds\(5),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[4]~18_combout\);

-- Location: LCCOMB_X1_Y24_N0
\inst3|LPM_MUX_component|auto_generated|result_node[4]~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[4]~19_combout\ = (\inst3|LPM_MUX_component|auto_generated|result_node[4]~18_combout\ & ((\inst6|seconds\(4)) # ((\inst6|seconds\(3)) # (\inst6|seconds\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(4),
	datab => \inst6|seconds\(3),
	datac => \inst6|seconds\(2),
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[4]~18_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[4]~19_combout\);

-- Location: LCCOMB_X1_Y21_N0
\inst3|LPM_MUX_component|auto_generated|result_node[3]~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[3]~20_combout\ = (\inst6|seconds\(4)) # ((\inst6|seconds\(3) & ((\inst6|seconds\(1)) # (\inst6|seconds\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(3),
	datab => \inst6|seconds\(1),
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(2),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[3]~20_combout\);

-- Location: LCCOMB_X1_Y21_N8
\inst3|LPM_MUX_component|auto_generated|result_node[3]~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[3]~23_combout\ = (\inst6|seconds\(5) & (\LED_DISPLAY~input_o\ & \inst3|LPM_MUX_component|auto_generated|result_node[3]~20_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst6|seconds\(5),
	datac => \LED_DISPLAY~input_o\,
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[3]~20_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[3]~23_combout\);

-- Location: LCCOMB_X1_Y21_N6
\inst3|LPM_MUX_component|auto_generated|result_node[2]~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[2]~21_combout\ = (\inst6|seconds\(4) & (\LED_DISPLAY~input_o\ & \inst6|seconds\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(4),
	datac => \LED_DISPLAY~input_o\,
	datad => \inst6|seconds\(5),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[2]~21_combout\);

-- Location: LCCOMB_X1_Y21_N30
\inst3|LPM_MUX_component|auto_generated|result_node[1]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[1]~24_combout\ = (\inst6|seconds\(4) & (\inst6|seconds\(5) & (\LED_DISPLAY~input_o\ & !\inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|seconds\(4),
	datab => \inst6|seconds\(5),
	datac => \LED_DISPLAY~input_o\,
	datad => \inst3|LPM_MUX_component|auto_generated|result_node[1]~11_combout\,
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[1]~24_combout\);

-- Location: LCCOMB_X1_Y21_N26
\inst3|LPM_MUX_component|auto_generated|result_node[0]~25\ : cycloneiii_lcell_comb
-- Equation(s):
-- \inst3|LPM_MUX_component|auto_generated|result_node[0]~25_combout\ = (!\inst40|LessThan9~0_combout\ & (\LED_DISPLAY~input_o\ & (\inst6|seconds\(4) & \inst6|seconds\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst40|LessThan9~0_combout\,
	datab => \LED_DISPLAY~input_o\,
	datac => \inst6|seconds\(4),
	datad => \inst6|seconds\(5),
	combout => \inst3|LPM_MUX_component|auto_generated|result_node[0]~25_combout\);

ww_DECIMAL <= \DECIMAL~output_o\;

ww_HOURLEFT(6) <= \HOURLEFT[6]~output_o\;

ww_HOURLEFT(5) <= \HOURLEFT[5]~output_o\;

ww_HOURLEFT(4) <= \HOURLEFT[4]~output_o\;

ww_HOURLEFT(3) <= \HOURLEFT[3]~output_o\;

ww_HOURLEFT(2) <= \HOURLEFT[2]~output_o\;

ww_HOURLEFT(1) <= \HOURLEFT[1]~output_o\;

ww_HOURLEFT(0) <= \HOURLEFT[0]~output_o\;

ww_HOURRIGHT(6) <= \HOURRIGHT[6]~output_o\;

ww_HOURRIGHT(5) <= \HOURRIGHT[5]~output_o\;

ww_HOURRIGHT(4) <= \HOURRIGHT[4]~output_o\;

ww_HOURRIGHT(3) <= \HOURRIGHT[3]~output_o\;

ww_HOURRIGHT(2) <= \HOURRIGHT[2]~output_o\;

ww_HOURRIGHT(1) <= \HOURRIGHT[1]~output_o\;

ww_HOURRIGHT(0) <= \HOURRIGHT[0]~output_o\;

ww_MINUTESLEFT(6) <= \MINUTESLEFT[6]~output_o\;

ww_MINUTESLEFT(5) <= \MINUTESLEFT[5]~output_o\;

ww_MINUTESLEFT(4) <= \MINUTESLEFT[4]~output_o\;

ww_MINUTESLEFT(3) <= \MINUTESLEFT[3]~output_o\;

ww_MINUTESLEFT(2) <= \MINUTESLEFT[2]~output_o\;

ww_MINUTESLEFT(1) <= \MINUTESLEFT[1]~output_o\;

ww_MINUTESLEFT(0) <= \MINUTESLEFT[0]~output_o\;

ww_OUTPUT(6) <= \OUTPUT[6]~output_o\;

ww_OUTPUT(5) <= \OUTPUT[5]~output_o\;

ww_OUTPUT(4) <= \OUTPUT[4]~output_o\;

ww_OUTPUT(3) <= \OUTPUT[3]~output_o\;

ww_OUTPUT(2) <= \OUTPUT[2]~output_o\;

ww_OUTPUT(1) <= \OUTPUT[1]~output_o\;

ww_OUTPUT(0) <= \OUTPUT[0]~output_o\;

ww_SECONDS(9) <= \SECONDS[9]~output_o\;

ww_SECONDS(8) <= \SECONDS[8]~output_o\;

ww_SECONDS(7) <= \SECONDS[7]~output_o\;

ww_SECONDS(6) <= \SECONDS[6]~output_o\;

ww_SECONDS(5) <= \SECONDS[5]~output_o\;

ww_SECONDS(4) <= \SECONDS[4]~output_o\;

ww_SECONDS(3) <= \SECONDS[3]~output_o\;

ww_SECONDS(2) <= \SECONDS[2]~output_o\;

ww_SECONDS(1) <= \SECONDS[1]~output_o\;

ww_SECONDS(0) <= \SECONDS[0]~output_o\;
END structure;


