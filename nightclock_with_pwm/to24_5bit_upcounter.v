module to24_5bit_upcounter(input clock, input reset, input enabled, output reg[4:0] number, output reg ripple);

always @(posedge(clock) or posedge(reset)) begin
	if (reset) begin
		number = 0;
		ripple = 0;
	end else if (number < 23) begin
		number = number + 1;
		ripple = 0;
	end else if (enabled) begin
		ripple = 0;
	end else begin
		number = 0;
		ripple = ~ripple;
	end
end
endmodule
	