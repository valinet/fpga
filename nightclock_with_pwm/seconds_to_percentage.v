module seconds_to_percentages(input [5:0] seconds, output reg [9:0] percentage);

always @* begin
	if (seconds < 6) begin
		percentage = 10'b0000000000;
	end else if (seconds < 12) begin
		percentage = 10'b1000000000;
	end else if (seconds < 18) begin
		percentage = 10'b1100000000;
	end else if (seconds < 24) begin
		percentage = 10'b1110000000;
	end else if (seconds < 30) begin
		percentage = 10'b1111000000;
	end else if (seconds < 36) begin
		percentage = 10'b1111100000;
	end else if (seconds < 42) begin
		percentage = 10'b1111110000;
	end else if (seconds < 48) begin
		percentage = 10'b1111111000;
	end else if (seconds < 54) begin
		percentage = 10'b1111111100;
	end else if (seconds < 59) begin
		percentage = 10'b1111111110;
	end else begin
		percentage = 10'b1111111111;
	end
end
endmodule