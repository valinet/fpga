module pwm_clockreducer(input clock, output reg reduced);

reg [24:0] counter;

initial counter = 0;
initial reduced = 0;

always @(posedge(clock)) begin
	if (counter == 26'd200000) begin
		counter <= 0;
		reduced <= ~reduced;
	end else begin
		counter <= counter + 1;
	end
end
endmodule